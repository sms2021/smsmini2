﻿#pragma warning disable CS0246 // The type or namespace name 'Ionic' could not be found (are you missing a using directive or an assembly reference?)
using Ionic.Zip;
#pragma warning restore CS0246 // The type or namespace name 'Ionic' could not be found (are you missing a using directive or an assembly reference?)
#pragma warning disable CS0246 // The type or namespace name 'Janawat' could not be found (are you missing a using directive or an assembly reference?)
using Janawat.Application.Data;
using Newtonsoft.Json;
#pragma warning restore CS0246 // The type or namespace name 'Janawat' could not be found (are you missing a using directive or an assembly reference?)
using SMSMINI.DAL;
using SMSMINI.Transaction;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Windows.Forms;


namespace SMSMINI
{
    public partial class Main : Form
    {
        private ProgressBar.FormProgress m_fmProgress = null;

        string strConnectionMode = "";
        DateTime Local_date1 = DateTime.Now;//Local
        DateTime Server_date2 = DateTime.Now;//Server

        //UserInfo user = null;
        bool isDownloadDBComplete = false;

        string strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;

        public Main()
        {
            InitializeComponent();


            string _mode = (UserInfo.ConnectMode == "0") ? " [Off line]" : " [On line]";
            object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
            if (attributes.Length == 0)
            {
                this.Text = "SMS MINI [v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "] " + _mode;

            }
            this.Text = ((AssemblyProductAttribute)attributes[0]).Product + " [v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "] " + _mode;

            this.Text = this.Text + " User: " + UserInfo.UserId + " " + UserInfo.FullName;

        }

        private static void GetAndDisplayRights()
        {
            WindowsPrincipal pricipal = new WindowsPrincipal(WindowsIdentity.GetCurrent());
            bool hasAdministrativeRight = pricipal.IsInRole(WindowsBuiltInRole.Administrator);
        }


        [DllImport("Kernel32.dll")]
        public static extern bool SetLocalTime(ref SYSTEMTIME Time);

        public struct SYSTEMTIME
        {
            public ushort wYear;
            public ushort wMonth;
            public ushort wDayOfWeek;
            public ushort wDay;
            public ushort wHour;
            public ushort wMinute;
            public ushort wSecond;
            public ushort wMilliseconds;

            public void FromDateTime(DateTime time)
            {
                wYear = (ushort)time.Year;
                wMonth = (ushort)time.Month;
                wDayOfWeek = (ushort)time.DayOfWeek;
                wDay = (ushort)time.Day;
                wHour = (ushort)time.Hour;
                wMinute = (ushort)time.Minute;
                wSecond = (ushort)time.Second;
                wMilliseconds = (ushort)time.Millisecond;
            }
        }


        private void Main_Load(object sender, EventArgs e)
        {



            ////if (UserInfo.ConnectMode == "0")//Offline
            ////{
            ////    this.toolStripLabel_Mode.Image = global::SMSMINI.Properties.Resources.netshell_Ico4;
            ////    strConnectionMode = "Off line";
            ////    toolStripLabel_Mode.Text = "Off line";
            ////    timer1.Stop();
            ////    toolStripDropDownButton_MasterData.Enabled = true;
            ////    toolStripBt_SMSWeb.Enabled = true;
            ////    toolStripButton_DownloadJOB.Enabled = true;
            ////    toolStripButton4.Enabled = true;

            ////    uploadMonitoringToolStripMenuItem.Enabled = true;


            ////    //20170530
            ////    EditJOBOnlineToolStripMenuItem.Enabled = false;


            ////}
            ////else//Online
            ////{
            ////    toolStripLabel_Mode.Text = "On line";
            ////    strConnectionMode = "netshell_Ico4";
            ////    timer1.Start();


            ////    toolStripBt_SMSWeb.Enabled = true;

            ////    toolStripButton_DownloadJOB.Enabled = false;
            ////    toolStripButton4.Enabled = false;
            ////    //20170530 toolStripDropDownButton_MasterData.Enabled = false;
            ////    //--==============================
            ////    toolStripDropDownButton_MasterData.Enabled = true;
            ////    enabledSubMenuOnline_MasterData();
            ////    //--==============================




            ////    uploadMonitoringToolStripMenuItem.Enabled = false;



            ////}



            uploadMonitoringToolStripMenuItem.Enabled = true;


            //20170530
            EditJOBOnlineToolStripMenuItem.Enabled = false;


            toolStripButton_CloseJOB.Enabled = true;

        }



        private void enabledSubMenuOnline_MasterData()
        {
            บนทกขอมลFixedAssetToolStripMenuItem.Enabled = false;
            ยนยนขอมลFixedAssetToolStripMenuItem.Enabled = false;
            aaaToolStripMenuItem.Enabled = false;
            vvvvToolStripMenuItem.Enabled = false;
            เปลยนPasswordToolStripMenuItem1.Enabled = false;
            fAMToolStripMenuItem.Enabled = false;
            เบกอะใหลToolStripMenuItem.Enabled = false;
            downloadฐานขอมลToolStripMenuItem.Enabled = false;
            อปเดตฐานขอมลToolStripMenuItem.Enabled = false;
            updateSQLScriptToolStripMenuItem.Enabled = false;
            เปดJOBToolStripMenuItem.Enabled = false;
            uploadJOBPhotoToolStripMenuItem.Enabled = false;
            uploadJOBPhotoToolStripMenuItem1.Enabled = false;
            SurveyDataToolStripMenuItem.Enabled = false;
            EditJOBOnlineToolStripMenuItem.Enabled = true;
        }

        private void UpdateSpareParts(SqlConnection conn_svr, string spa_id)
        {
            List<AutoPart> autoPart = null;
            #region Get data Online
            string sql = " SELECT * FROM SpareParts  Where SPA_ID ='" + spa_id + "'";

            using (SqlDataReader dr = new SqlCommand(sql, conn_svr).ExecuteReader())
            {
                if (dr.HasRows)
                {
                    autoPart = new List<AutoPart>();
                    while (dr.Read())
                    {
                        autoPart.Add(new AutoPart
                        {
                            SPA_ID = dr["SPA_ID"].ToString(),
                            BRA_ID = dr["BRA_ID"] == DBNull.Value ? null : dr["BRA_ID"].ToString(),
                            Cost = dr["Cost"] == DBNull.Value ? null : (decimal?)dr["Cost"],
                            Description = dr["Description"].ToString(),
                            InStock = dr["InStock"] == DBNull.Value ? null : (int?)dr["InStock"],
                            IsDelete = dr["IsDelete"] == DBNull.Value ? null : (bool?)dr["IsDelete"],
                            IsDiscount = dr["IsDiscount"] == DBNull.Value ? null : (bool?)dr["IsDiscount"],
                            IsGlobalDiscount = dr["IsGlobalDiscount"] == DBNull.Value ? null : (bool?)dr["IsGlobalDiscount"],
                            Minimum = dr["InStock"] == DBNull.Value ? null : (int?)dr["InStock"],
                            PartPhoto = dr["PartPhoto"] == DBNull.Value ? null : (System.Data.Linq.Binary)dr["PartPhoto"],
                            PricePerUnit = dr["PricePerUnit"] == DBNull.Value ? null : (decimal?)dr["PricePerUnit"],
                            SparePart = dr["SparePart"].ToString(),
                            Styp_id = dr["Styp_id"] == DBNull.Value ? null : dr["Styp_id"].ToString(),
                            SUP_ID = dr["SUP_ID"] == DBNull.Value ? null : dr["SUP_ID"].ToString(),
                            TranType = dr["TranType"].ToString(),
                            UnitID = dr["UnitID"] == DBNull.Value ? null : dr["UnitID"].ToString(),
                            WartDate = dr["WartDate"] == DBNull.Value ? null : (DateTime?)dr["WartDate"],
                            WH = dr["WH"].ToString()

                        });
                    }
                }
            }// using (SqlDataReader....

            //DAL.SMSManage.SparePart aa = new DAL.SMSManage.SparePart();
            //aa.p
            #endregion

            if (autoPart == null) return;

            #region Get data Offline


            string strConn = SMSMINI.Properties.Settings.Default["ServicesMSDBConnectionString_Offline"].ToString();
            using (System.Data.SqlClient.SqlConnection Conn_client = new SqlConnection(strConn))
            {
                if (Conn_client.State == ConnectionState.Closed)
                    Conn_client.Open();

                SqlCommand cmd = Conn_client.CreateCommand();
                cmd.CommandType = CommandType.Text;

                string mode = "";
                foreach (var item in autoPart)
                {
                    sql = "select * from SpareParts where SPA_ID ='" + item.SPA_ID + "'";
                    using (SqlDataReader dr = new SqlCommand(sql, Conn_client).ExecuteReader())
                    {
                        if (dr.HasRows)//Update
                        {
                            dr.Read();
                            mode = "Edit";
                        }
                        else//Add New 
                        {
                            mode = "Add";

                        }
                    }
                    if (mode == "Edit")
                    {
                        int isdel = 0;
                        if (item.IsDelete != null)
                            isdel = item.IsDelete == true ? 1 : 0;


                        sql = " UPDATE  SpareParts " +
                               " SET SUP_ID = '" + item.SUP_ID + "'" +
                               "    ,Styp_id = '" + item.Styp_id + "'" +
                               "    ,BRA_ID = '" + item.BRA_ID + "'" +
                               "    ,SparePart = '" + item.SparePart + "'" +
                               "    ,PricePerUnit = " + item.PricePerUnit + "" +
                               "    ,Cost = " + item.Cost + "" +
                               "    ,WartDate = '" + item.WartDate + "'" +
                               "    ,InStock = " + item.InStock + "" +
                               "    ,Minimum = " + item.Minimum + "" +
                               "    ,UnitID = '" + item.UnitID + "'" +
                               "    ,Description = '" + item.Description + "'" +
                               "    ,TranType = '" + item.TranType + "'" +
                               "    ,WH = '" + item.WH.Trim() + "'" +
                            //"    ,PartPhoto = '" + item.PartPhoto + "'" +
                            //"    ,IsDiscount = " + item.IsDiscount + "" +
                            //"    ,IsGlobalDiscount = " + item.IsGlobalDiscount + "" +
                            "    ,IsDelete = " + isdel + "" +
                               " WHERE  SPA_ID = '" + item.SPA_ID + "'";

                        cmd.CommandText = sql;
                        cmd.ExecuteNonQuery();
                    }
                    else
                    {
                        int isdel = 0;
                        if (item.IsDelete != null)
                            isdel = item.IsDelete == true ? 1 : 0;

                        sql = " INSERT INTO  SpareParts " +
                               "    (SPA_ID " +
                               "    ,SUP_ID " +
                               "    ,Styp_id " +
                               "    ,BRA_ID " +
                               "    ,SparePart " +
                               "    ,PricePerUnit " +
                               "    ,Cost " +
                               "    ,WartDate " +
                               "    ,InStock " +
                               "    ,Minimum " +
                               "    ,UnitID " +
                               "    ,Description " +
                               "    ,TranType " +
                               "    ,WH " +
                               //"    ,PartPhoto " +
                               //"    ,IsDiscount " +
                               //"    ,IsGlobalDiscount " +
                               "    ,IsDelete) " +
                               "    VALUES " +
                               "    ('" + item.SPA_ID + "'" +
                               "    ,'" + item.SUP_ID + "'" +
                               "    ,'" + item.Styp_id + "'" +
                               "    ,'" + item.BRA_ID + "'" +
                               "    ,'" + item.SparePart + "'" +
                               "    ," + item.PricePerUnit + "" +
                               "    ," + item.Cost + "" +
                               "    ,'" + item.WartDate + "'" +
                               "    ," + item.InStock + "" +
                               "    ," + item.Minimum + "" +
                               "    ,'" + item.UnitID + "'" +
                               "    ,'" + item.Description + "'" +
                               "    ,'" + item.TranType + "'" +
                               "    ,'" + item.WH.Trim() + "'" +
                               //"    ,'" + item.PartPhoto + "'" +
                               //"    ," + item.IsDiscount + "" +
                               //"    ," + item.IsGlobalDiscount + "" +
                               "    ," + isdel + ")";

                        cmd.CommandText = sql;
                        cmd.ExecuteNonQuery();
                    }
                }

            }

            #endregion
        }

        private void UpdateSparePart_TranType(SqlConnection conn_svr, string spa_id)
        {
            List<AutoSparePartTranType> spType = null;

            #region Get data Online
            string sql = " SELECT distinct SpareParts.SPA_ID, SparePart_TranType.TRA_ID, SparePart_TranType.TransferName " +
                        " FROM SpareParts INNER JOIN " +
                        " SparePart_TranType ON SpareParts.TranType = SparePart_TranType.TRA_ID " +
                        " Where SPA_ID ='" + spa_id + "'";

            using (SqlDataReader dr = new SqlCommand(sql, conn_svr).ExecuteReader())
            {
                if (dr.HasRows)
                {
                    spType = new List<AutoSparePartTranType>();
                    while (dr.Read())
                    {
                        spType.Add(new AutoSparePartTranType
                        {
                            TRA_ID = dr["TRA_ID"].ToString(),
                            TransferName = dr["TransferName"].ToString()
                        });
                    }
                }
            }// using (SqlDataReader....

            #endregion

            if (spType == null) return;

            #region Get data Offline


            string strConn = SMSMINI.Properties.Settings.Default["ServicesMSDBConnectionString_Offline"].ToString();
            using (System.Data.SqlClient.SqlConnection Conn_client = new SqlConnection(strConn))
            {
                if (Conn_client.State == ConnectionState.Closed)
                    Conn_client.Open();

                SqlCommand cmd = Conn_client.CreateCommand();
                cmd.CommandType = CommandType.Text;

                string mode = "";
                foreach (var item in spType)
                {
                    sql = "select * from SparePart_TranType where TRA_ID ='" + item.TRA_ID + "'";
                    using (SqlDataReader dr = new SqlCommand(sql, Conn_client).ExecuteReader())
                    {
                        if (dr.HasRows)//Update
                        {
                            dr.Read();
                            mode = "Edit";
                        }
                        else//Add New 
                        {
                            mode = "Add";
                        }
                    }

                    if (mode == "Edit")
                    {
                        sql = "Update SparePart_TranType set TransferName ='" + item.TransferName + "' where TRA_ID ='" + item.TRA_ID + "'";
                        cmd.CommandText = sql;
                        cmd.ExecuteNonQuery();
                    }
                    else
                    {
                        sql = "Insert into SparePart_TranType values ('" + item.TRA_ID + "','" + item.TransferName + "')";
                        cmd.CommandText = sql;
                        cmd.ExecuteNonQuery();
                    }
                }

            }

            #endregion

        }




        DateTime? srv = null;
        string conn = Properties.Settings.Default["ServicesMSDBConnectionString"].ToString();

        private bool AutoUpdateSystemDateTime()
        {
            try
            {

                invoke_Progress("ติดต่อ SMS Server");

                if (getServerDateTime())
                {
                    invoke_Progress("เริ่มปรับเวลาเครื่อง Client...");

                    SYSTEMTIME systemDateLocal = new SYSTEMTIME();
                    systemDateLocal.FromDateTime(Server_date2);
                    return SetLocalTime(ref systemDateLocal);

                }
                else
                    return false;
            }
            catch (System.Exception)
            {
                return false;
            }
        }

        private bool getServerDateTime()
        {
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext(conn))
            {
                srv = dc
                    .ExecuteQuery<DateTime>("SELECT GETDATE()")
                    .First();

                Local_date1 = DateTime.Now;
            }

            if (srv != null)
            {
                Server_date2 = srv.Value;
                return true;
            }
            else
                return false;
        }


        private void toolStripBt_StartIn_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.AppStarting;

                newProgressbar();
                invoke_Progress("กำลังติดต่อขอเวลาจาก SMS Server");
                bool isSetDateTime = AutoUpdateSystemDateTime();
                closeProgress();


                if (isSetDateTime)
                {
                    MessageBox.Show("ทำการปรับปรุงเวลาเรียบร้อย" + Environment.NewLine +
                    "Server(manage):\t\t" + Server_date2 + Environment.NewLine +
                    "Client(mini):ก่อน\t\t" + Local_date1.ToString() + Environment.NewLine +
                    "Client(mini):หลัง\t\t" + DateTime.Now + Environment.NewLine, "ผลการทำงาน...",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    if (getServerDateTime())
                    {
                        commandUpdateDate();
                        commandUpdateTime();
                    }
                    MessageBox.Show("ไม่สามารถทำการปรับปรุงเวลา" + Environment.NewLine +
                       "Server(manage):\t\t" + Server_date2 + Environment.NewLine, "ข้อผิดพลาด...",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);

                }


                UserInfo.SMSServerDateTime = Server_date2;

                Cursor.Current = Cursors.Default;
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("เกิดผิดพลาดในการใช้งานเมนู (1) Time Sync " + ex.Message, "ข้อผิดพลาด");
                this.Show();
            }
        }

        private void commandUpdateDate()
        {
            try
            {

                var processDate = new Process
                {
                    StartInfo = new ProcessStartInfo("cmd", "/c " + "date " + Server_date2.ToString("MM-dd-yy"))
                    {

                        //Verb = "runas",
                        CreateNoWindow = true,
                        RedirectStandardOutput = false,
                        UseShellExecute = true,
                    }
                };


                processDate.Start();
            }
            catch (Exception)
            { }
        }

        private void commandUpdateTime()
        {
            try
            {
                var processTime = new Process
                {
                    StartInfo = new ProcessStartInfo("cmd", "/c " + "time " + Server_date2.ToString("HH:mm:ss"))
                    {
                        //Verb = "runas",
                        CreateNoWindow = true,
                        RedirectStandardOutput = false,
                        UseShellExecute = true,
                    }
                };

                processTime.Start();
            }
            catch (Exception)
            {
            }
        }

        private void newProgressbar()
        {
            m_fmProgress = new ProgressBar.FormProgress();
            backgroundWorker1.RunWorkerAsync();
            System.Threading.Thread.Sleep(3000);
        }

        private void invoke_Progress(string strMsg)
        {
            try
            {
                if (m_fmProgress != null)
                {
                    m_fmProgress.lblDescription.Invoke(
                       (MethodInvoker)delegate ()
                       {
                           m_fmProgress.lblDescription.Text = strMsg;
                       }
                       );
                }
            }
            catch (Exception)
            { }
        }

        private void closeProgress()
        {
            if (m_fmProgress != null)
            {
                m_fmProgress.Invoke(
                    (MethodInvoker)delegate ()
                    {
                        m_fmProgress.Hide();
                        m_fmProgress.Dispose();
                        m_fmProgress = null;
                    }
                );
            }
        }


        private bool IsUpdatePart(ref bool update_MasterData_Part, ref bool update_MasterData_Contract)
        {
            //bool isUpdatePart = false;

            string strConn = SMSMINI.Properties.Settings.Default["ServicesMSDBConnectionString"].ToString();

            #region SP_Get_SpareParts_LogUpdate
            try
            {
                using (System.Data.SqlClient.SqlConnection Conn_svr = new SqlConnection(strConn))
                {
                    if (Conn_svr.State == ConnectionState.Closed)
                        Conn_svr.Open();

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = Conn_svr;
                    cmd.CommandText = "SP_Get_SpareParts_LogUpdate";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Clear();
                    cmd.Parameters.Add(new SqlParameter("@VAN_ID", UserInfo.UserId));
                    cmd.Parameters.Add(new SqlParameter("@Status", ""));//3 return distinct Table Name, 2 = return delete 0,1 = retun InsertUpdate


                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.HasRows)
                            update_MasterData_Part = true;
                        else
                            update_MasterData_Part = false;
                    }

                }
            }
            catch (System.Exception)
            {
            }

            #endregion

            #region Conf_Contract_LogVANUpdates
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn))
            {
                try
                {
                    var maxDate = (from t in dc.Conf_Contract_LogVANUpdates
                                   orderby t.UpdateDateTime descending
                                   where t.VAN_ID == UserInfo.UserId
                                   select t.UpdateDateTime).FirstOrDefault();

                    if (maxDate == null || maxDate == new DateTime(0001, 1, 1))
                        maxDate = DateTime.Now.AddDays(-7);

                    var qvan = from t in dc.Conf_Contract_LogUpdates
                               orderby t.TrTableName
                               where (t.TrDateTime > maxDate)
                               select t;

                    if (qvan.Count() > 0)
                        update_MasterData_Contract = true;
                    else
                        update_MasterData_Contract = false;
                }
                catch (System.Exception)
                {
                }// try

            }// using (DAL.SMSManage.SMSManageDataContext 
            #endregion

            return true;
        }

        private bool IsFAM()
        {
            if (!UserInfo.UserId.ToLower().StartsWith("v"))
                return true;

            //using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            //{
            //    var f = from t in dc.Car_CheckOurCar_Details
            //            where t.DateCheck.Date == DateTime.Now.Date && t.VAN_ID == UserInfo.UserId
            //            select t;

            //    if (f.Count() > 0)
            //        return true;
            //    else
            //        return false;
            //}

            return true;

        }

        private void toolStripBt_DownloadJob_Click(object sender, EventArgs e)
        {

            ////////JOB_Detail_CheckServiceReportAlert();

            //////JOB_MessageAlert();

            //////VANUpdateLog();

            //////checkSystemDate();

            //////#region checkDBVersion
            //////string _conMini = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
            //////string _conmanage = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;

            //////if (!ConnectionManager.checkDBVersion(_conMini, _conmanage))
            //////{
            //////    MessageBox.Show("คุณไม่สามารถ Download JOB...เนื่องจาก" + Environment.NewLine +
            //////        "ตรวจสอบพบ!...ฐานข้อมูลเก่า..." + Environment.NewLine +
            //////        "คุณยังไม่ทำการเปลี่ยนฐานข้อมูล เวอร์ชั่นใหม่..." + Environment.NewLine +
            //////        "กรุณาอัปเดต ฐานข้อมูล เวอร์ชั่นใหม่...ก่อน Download หรือ Upload JOB..." + Environment.NewLine +
            //////         "#--------------------------------------------------------------------#" + Environment.NewLine +
            //////         "กรุณาต่อเนต แล้วทำการโหลดฐานข้อมูลได้ที่:" + Environment.NewLine +
            //////         "เข้าเมนู ปรับปรุงข้อมูล => เข้าเมนู (1) Download ฐานข้อมูล... เพื่อ" + Environment.NewLine +
            //////         "ทำการ Download ฐานข้อมูล และ Update ฐานข้อมูล Auto",
            //////         "ผลการตรวจสอบ...ฐานข้อมูลไม่ถูกต้อง ???...", MessageBoxButtons.OK, MessageBoxIcon.Error);

            //////    toolStripDropDownButton_MasterData.ShowDropDown();
            //////    downloadฐานขอมลToolStripMenuItem.Select();
            //////    return;

            //////}

            ////////if ()
            ////////{}
            //////#endregion 

            //////#region Update Script Online

            //////updateScriptOnline();

            //////#endregion

            //////if (!IsFAM())//ตรวจสอบสถาพรถยนต์ แล้ว
            //////{
            //////    MessageBox.Show("คุณไม่สามารถทำการดาวน์โหลด JOB ได้ เนื่องจากคุณยังไม่ตรวจสอบสภาพรถยนต์" + Environment.NewLine +
            //////        "กรุณา เข้าโปรแกรม FAM เพื่อเข้าดำเนินการ ตรวจสภาพรถยนต์", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Question);
            //////    showFAN();
            //////}
            //////else
            //////{
            //////    //CheckUpdateApp.checkUpdate(false);
            //////    try
            //////    {
            //////        //ตรวจสอบ JOB ที่ยังไม่ Upload
            //////        Cursor.Current = Cursors.AppStarting;
            //////        Transaction.JobUpload f0 = new SMSMINI.Transaction.JobUpload();
            //////        f0.StartPosition = FormStartPosition.CenterScreen;
            //////        f0.checkUpload = true;
            //////        if (f0.ShowDialog() == DialogResult.Abort)
            //////        {
            //////            if (f0.jobValid)
            //////            {
            //////                Cursor.Current = Cursors.Default;
            //////                return;
            //////            }
            //////        }


            //////        //////ตรวจสอบ Auto Update Master Data
            //////        ////bool update_MasterData_Part = false;
            //////        ////bool update_MasterData_Contract = false;

            //////        ////if (IsUpdatePart(ref update_MasterData_Part, ref update_MasterData_Contract))
            //////        ////{
            //////        ////    //if (update_MasterData_Part || update_MasterData_Contract)
            //////        ////    //{
            //////        ////    //    MessageBox.Show("Update Master Data..." + Environment.NewLine +
            //////        ////    //        "กรุณา Update Master Data ก่อนการโหลด JOB", "Auto update Master Data ",
            //////        ////    //        MessageBoxButtons.OK, MessageBoxIcon.Information);

            //////        ////    //    if (update_MasterData_Part)
            //////        ////    //    {
            //////        ////    //        MasterData.AutoUpdateMasterData fa = new SMSMINI.MasterData.AutoUpdateMasterData();
            //////        ////    //        fa.StartPosition = FormStartPosition.CenterScreen;

            //////        ////    //        if (fa.ShowDialog() == DialogResult.Abort)
            //////        ////    //            fa.Close();
            //////        ////    //    }

            //////        ////    //    if (update_MasterData_Contract)
            //////        ////    //    {
            //////        ////    //        MasterData.AutoUpdateMasterData_Contract fcon = new SMSMINI.MasterData.AutoUpdateMasterData_Contract();
            //////        ////    //        fcon.StartPosition = FormStartPosition.CenterScreen;

            //////        ////    //        if (fcon.ShowDialog() == DialogResult.Abort)
            //////        ////    //            fcon.Close();
            //////        ////    //    }
            //////        ////    //    goto DownloadJOB;

            //////        ////    //}
            //////        ////    //else
            //////        ////    //{
            //////        ////    //    goto DownloadJOB;
            //////        ////    //}

            //////        //////DownloadJOB:
            //////        //////    Cursor.Current = Cursors.AppStarting;
            //////        //////    Transaction.JobDownload f = new SMSMINI.Transaction.JobDownload();
            //////        //////    f.StartPosition = FormStartPosition.CenterScreen;
            //////        //////    if (f.ShowDialog() == DialogResult.Abort)
            //////        //////    {
            //////        //////        if (f.downSucc && f.lsSMSJob.Count() > 0)
            //////        //////        {
            //////        //////            LoadFailure_WiWp(f.lsSMSJob);
            //////        //////        }

            //////        //////        Cursor.Current = Cursors.Default;
            //////        //////    }
            //////        //////    //====
            //////        //////    if (f.IsFFAOK == true)
            //////        //////    {
            //////        //////        toolStripDropDownButton_MasterData.ShowDropDown();
            //////        //////        ยนยนขอมลFixedAssetToolStripMenuItem.Select();

            //////        //////    }


            //////        ////}
            //////        ///

            //////    }

            //////    catch (System.Exception ex)
            //////    {
            //////        MessageBox.Show("เกิดผิดพลาดในการใช้งานเมนู (2) Download JOB Error " + ex.Message, "ข้อผิดพลาด");
            //////        this.Show();
            //////    }
            //////}
        
        
        }


        private void JOB_MessageAlert()
        {
            using (var dc = new DAL.SMSManage.SMSManageDataContext())
            {
                //Alert By VAN
                var maVAN = dc.vw_JOB_MessageAlert_by_VANs
                    .Where(t => t.VAN == UserInfo.Van_ID)
                    .ToList();

                //Alert By Station
                var maStation = dc.vw_JOB_MessageAlert_by_SATs
                  .Where(t => t.VAN == UserInfo.Van_ID)
                  .ToList();

                if (maVAN.Count() > 0 || maStation.Count() > 0)
                {
                    var f = new SMSMINI.Transaction.PopUp_JOB_MessageAlert();
                    f.StartPosition = FormStartPosition.CenterParent;
                    f.ShowDialog();
                }
            }
        }

        private void LoadFailure_WiWp(List<SMSMINI.DAL.SMSManage.JOB> jlist)
        {
        }


        private void VANUpdateLog()
        {
            string titel;
            object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
            if (attributes.Length == 0)
                titel = "SMS MINI [v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "]";
            else
                titel = ((AssemblyProductAttribute)attributes[0]).Product + " [v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "]";

            DAL.SMSManage.SMSDBVersion dbVersion = null;
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn))
            {
                var q = dc.SMSDBVersions.FirstOrDefault();
                if (q != null)
                    dbVersion = q;
            }


            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            {
                var q = dc.Conf_Curent_SMSMiniVersions
                    .Where(t => t.VAN_ID == UserInfo.Van_ID)
                    .FirstOrDefault();

                if (q != null)
                {
                    q.SMSMiniVersion = titel;
                    q.DataBaseVersion = dbVersion == null ? null : dbVersion.DBVersion;
                }
                else
                {
                    var newLog = new DAL.SMSManage.Conf_Curent_SMSMiniVersion
                    {
                        VAN_ID = UserInfo.Van_ID,
                        SMSMiniVersion = titel,
                        DataBaseVersion = dbVersion == null ? null : dbVersion.DBVersion
                    };

                    dc.Conf_Curent_SMSMiniVersions.InsertOnSubmit(newLog);
                }

                dc.SubmitChanges();
            }
        }

        private static void updateScriptOnline()
        {

            string id = "";
            string errID = "";
            try
            {
                string _Offline = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
                string _Online = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;

                //MessageBox.Show("Start...updateScriptOnline()"+
                //    " _Offline:" + _Offline + " _Online:" + _Online);


                using (SqlConnection connection = new SqlConnection(_Online))
                {
                    using (SqlCommand command = new SqlCommand("SELECT * FROM  SQLUpdate_SMSMINI", connection))
                    {
                        connection.Open();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                try
                                {
                                    id = reader["ID"].ToString();
                                    //if (id == "463")
                                    //MessageBox.Show("id: " + id + "" );

                                    string sql = reader["SQLScript"].ToString();

                                    //MessageBox.Show("id: " + id + " sql:" + sql);

                                    JaSqlHelper.ExecuteNonQuery(_Offline, System.Data.CommandType.Text, sql);
                                    //MessageBox.Show("ExecuteNonQuery...Complete...");
                                }
                                catch (Exception ex0)
                                {
                                    errID += id + " Err:" + ex0.Message + ",";
                                    //MessageBox.Show("Update Script Online ID:=" + id + " Error.1:" + ex0.Message);
                                }
                            }
                            //reader.Close();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                //throw;
                MessageBox.Show("Update Script Online ID:=" + id + " Error.2:" + ex.Message);
            }

            if (!string.IsNullOrEmpty(errID))
            {
                MessageBox.Show("Update Script Online  Error.1:" + errID);
            }
        }

        private bool IsAutoUpdateView()
        {
            ////string fdateName = "ViewUpdateDate.txt";//Log วันที่ Update

            ////try
            ////{
            ////    //Load ViewUpdateDate.txt
            ////    string remotefdateUri = "http://sms.flowco.co.th/sms/SMSMini/AutoUpdateView/";
            ////    string myWebResource = string.Empty;
            ////    string fdate = string.Empty;
            ////    using (WebClient client = new WebClient())
            ////    {
            ////        myWebResource = remotefdateUri + fdateName;
            ////        fdate = client.DownloadString(myWebResource);

            ////        DateTime date1 = Convert.ToDateTime(fdate);
            ////        DateTime date2;

            ////        if (File.Exists(Application.StartupPath + "//" + fdateName))
            ////        {
            ////            using (StreamReader sr = new StreamReader(Application.StartupPath + "//" + fdateName))
            ////            {
            ////                String line;
            ////                while ((line = sr.ReadLine()) != null)
            ////                {
            ////                    date2 = Convert.ToDateTime(line);
            ////                    //ถ้าวันที่เคย update ครั้งล่าสุดมากกว่า หรือเท่ากับ วันที่ update บน server
            ////                    if (DateTime.Compare(date2, date1) >= 0)
            ////                        return false;//ไม่ต้อง update

            ////                }
            ////            }
            ////        }
            ////    }


            ////    string fileName = "AutoUpdateView.zip";
            ////    myWebResource = string.Empty;

            ////    string downloadPath = @"C:\(7) AttachDB\AutoUpdateView\" + fileName;
            ////    string destinationDir = @"C:\(7) AttachDB\AutoUpdateView";
            ////    DirectoryInfo dirif = new DirectoryInfo(destinationDir);
            ////    if (!dirif.Exists)
            ////        dirif.Create();

            ////    var doi = new DirectoryInfo(destinationDir).GetFiles("*.*");
            ////    foreach (var item in doi)
            ////    {
            ////        if (item.Extension != ".zip")
            ////            item.Delete();
            ////    }



            ////    Download.DownloadManager f = new SMSMINI.Download.DownloadManager();
            ////    f.StartPosition = FormStartPosition.CenterParent;
            ////    f.downloadPath = downloadPath;
            ////    f.downloadUrl = @"http://sms.flowco.co.th/sms/SMSMini/AutoUpdateView/" + fileName;
            ////    f.btnDownload_Click(null, null);
            ////    if (f.ShowDialog() == DialogResult.Abort)
            ////    {
            ////        MessageBox.Show("ดาวน์โหลดเรียบร้อย กรุณากดปุ่ม OK เพื่อทำการแตก zip ไฟล์" + Environment.NewLine +
            ////            "เพื่อ update ระบบต่อไป", "ผลการทำงาน", MessageBoxButtons.OK);
            ////    }
            ////    else
            ////        return false;

            ////    //Extract Script file
            ////    invoke_Progress("กำลัง Extract Script file...");
            ////    if (File.Exists(downloadPath))
            ////    {
            ////        using (ZipFile zip1 = ZipFile.Read(downloadPath))
            ////        {
            ////            foreach (ZipEntry e in zip1)
            ////            {
            ////                e.Extract(destinationDir, ExtractExistingFileAction.OverwriteSilently);
            ////            }
            ////        }
            ////    }

            ////    //Delete Zip file
            ////    var doc = new DirectoryInfo(destinationDir).GetFiles("*.zip");
            ////    foreach (var item in doc)
            ////    {
            ////        if (item.Exists)
            ////            item.Delete();
            ////    }

            ////    //Exe bat file Update Script
            ////    invoke_Progress("กำลัง Update script...");
            ////    var batfile = new DirectoryInfo(destinationDir).GetFiles("*.bat");
            ////    foreach (var item in batfile)
            ////    {
            ////        if (batfile.Length > 0)
            ////        {
            ////            ////string path = Application.StartupPath + "\\SMSexe.exe";

            ////            ////ProcessStartInfo psi = new ProcessStartInfo();
            ////            //////psi.WorkingDirectory = destinationDir;
            ////            ////psi.FileName = path;
            ////            ////psi.Arguments = " v";//View update
            ////            ////psi.WindowStyle = ProcessWindowStyle.Normal;
            ////            ////psi.Verb = "runas";
            ////            //////psi.UseShellExecute = true;
            ////            ////Process.Start(psi);

            ////            Process.Start(item.FullName);
            ////        }
            ////    }
            ////    closeProgress();


            ////    using (WebClient client = new WebClient())
            ////    {
            ////        myWebResource = remotefdateUri + fdateName;
            ////        fdate = client.DownloadString(myWebResource);
            ////        client.DownloadFile(myWebResource, Application.StartupPath + "//" + fdateName);
            ////    }
            ////    Cursor.Current = Cursors.Default;
            ////}
            ////catch (Exception)
            ////{
            ////    //ถ้าเกิด Exception ให้ลบ UpdateDate.txt 
            ////    closeProgress();
            ////    Cursor.Current = Cursors.Default;
            ////    var fidate = new FileInfo(Application.StartupPath + "//" + fdateName);
            ////    if (fidate.Exists)
            ////        fidate.Delete();

            ////    return false;

            ////}

            return true;
        }

        private bool IsAutoUpdateScript()
        {
            ////string fdateName = "UpdateDate.txt";//Log วันที่ Update

            ////try
            ////{
            ////    //Load UpdateDate.txt
            ////    string remotefdateUri = "http://sms.flowco.co.th/sms/SMSMini/AutoUpdateScript/";
            ////    string myWebResource = string.Empty;
            ////    string fdate = string.Empty;
            ////    using (WebClient client = new WebClient())
            ////    {
            ////        myWebResource = remotefdateUri + fdateName;
            ////        fdate = client.DownloadString(myWebResource);

            ////        DateTime date1 = Convert.ToDateTime(fdate);
            ////        DateTime date2;

            ////        if (File.Exists(Application.StartupPath + "//" + fdateName))
            ////        {
            ////            using (StreamReader sr = new StreamReader(Application.StartupPath + "//" + fdateName))
            ////            {
            ////                String line;
            ////                while ((line = sr.ReadLine()) != null)
            ////                {
            ////                    date2 = Convert.ToDateTime(line);
            ////                    //ถ้าวันที่เคย update ครั้งล่าสุดมากกว่า หรือเท่ากับ วันที่ update บน server
            ////                    if (DateTime.Compare(date2, date1) >= 0)
            ////                        return false;//ไม่ต้อง update

            ////                }
            ////            }
            ////        }

            ////    }


            ////    string fileName = "AutoUpdateScript.zip";
            ////    myWebResource = string.Empty;

            ////    string downloadPath = @"C:\(7) AttachDB\AutoUpdateScript\" + fileName;
            ////    string destinationDir = @"C:\(7) AttachDB\AutoUpdateScript";
            ////    DirectoryInfo dirif = new DirectoryInfo(destinationDir);
            ////    if (!dirif.Exists)
            ////        dirif.Create();

            ////    var doi = new DirectoryInfo(destinationDir).GetFiles("*.*");
            ////    foreach (var item in doi)
            ////    {
            ////        if (item.Extension != ".zip")
            ////            item.Delete();
            ////    }





            ////    Download.DownloadManager f = new SMSMINI.Download.DownloadManager();
            ////    f.StartPosition = FormStartPosition.CenterParent;
            ////    f.downloadPath = downloadPath;
            ////    f.downloadUrl = @"http://sms.flowco.co.th/sms/SMSMini/AutoUpdateScript/" + fileName;
            ////    f.btnDownload_Click(null, null);
            ////    if (f.ShowDialog() == DialogResult.Abort)
            ////    {
            ////        MessageBox.Show("ดาวน์โหลดเรียบร้อย กรุณากดปุ่ม OK เพื่อทำการแตก zip ไฟล์" + Environment.NewLine +
            ////            "เพื่อ update ระบบต่อไป", "ผลการทำงาน", MessageBoxButtons.OK);
            ////    }
            ////    else
            ////        return false;

            ////    //Extract Script file
            ////    invoke_Progress("กำลัง Extract Script file...");
            ////    if (File.Exists(downloadPath))
            ////    {
            ////        using (ZipFile zip1 = ZipFile.Read(downloadPath))
            ////        {
            ////            foreach (ZipEntry e in zip1)
            ////            {
            ////                e.Extract(destinationDir, ExtractExistingFileAction.OverwriteSilently);
            ////            }
            ////        }
            ////    }

            ////    //Delete Zip file
            ////    var doc = new DirectoryInfo(destinationDir).GetFiles("*.zip");
            ////    foreach (var item in doc)
            ////    {
            ////        if (item.Exists)
            ////            item.Delete();
            ////    }

            ////    //Exe bat file Update Script
            ////    invoke_Progress("กำลัง Update script...");
            ////    var batfile = new DirectoryInfo(destinationDir).GetFiles("*.bat");
            ////    foreach (var item in batfile)
            ////    {
            ////        if (batfile.Length > 0)
            ////        {


            ////            Process.Start(item.FullName);

            ////        }
            ////    }
            ////    closeProgress();


            ////    using (WebClient client = new WebClient())
            ////    {
            ////        myWebResource = remotefdateUri + fdateName;
            ////        fdate = client.DownloadString(myWebResource);
            ////        client.DownloadFile(myWebResource, Application.StartupPath + "//" + fdateName);
            ////    }
            ////    Cursor.Current = Cursors.Default;
            ////}
            ////catch (Exception)
            ////{
            ////    //ถ้าเกิด Exception ให้ลบ UpdateDate.txt 
            ////    closeProgress();
            ////    Cursor.Current = Cursors.Default;
            ////    var fidate = new FileInfo(Application.StartupPath + "//" + fdateName);
            ////    if (fidate.Exists)
            ////        fidate.Delete();

            ////    return false;

            ////}

            return true;

        }

        private void toolStripBt_CloseJOB_Click(object sender, EventArgs e)
        {
            //CheckUpdateApp.checkUpdate(false);
            CloseJOBMethod();
        }

        private void CloseJOBMethod()
        {
            try
            {
                Cursor.Current = Cursors.AppStarting;
                Transaction.JobClose f = new SMSMINI.Transaction.JobClose();

                f.WindowState = FormWindowState.Maximized;
                if (f.ShowDialog() == DialogResult.Abort)
                    Cursor.Current = Cursors.Default;
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("เกิดผิดพลาดในการใช้งานเมนู (3) บันทึกออกงาน Error " + Environment.NewLine
                    + ex.Message, "ข้อผิดพลาด");
                this.Show();
            }
        }

        //bool isMaxPrice = false;

        private void toolStripBt_UploadJOB_Click(object sender, EventArgs e)
        {
            //////checkSystemDate();

            //////#region Check Database Version

            //////string _conMini = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
            //////string _conmanage = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;

            //////if (!ConnectionManager.checkDBVersion(_conMini, _conmanage))
            //////{
            //////    MessageBox.Show("คุณไม่สามารถ Upload JOB...เนื่อจาก" + Environment.NewLine +
            //////        "ตรวจสอบพบ!...ฐานข้อมูลเก่า..." + Environment.NewLine +
            //////        "คุณยังไม่ทำการเปลี่ยนฐานข้อมูล เวอร์ชั่นใหม่..." + Environment.NewLine +
            //////        "กรุณาอัปเดต ฐานข้อมูล เวอร์ชั่นใหม่...ก่อน Download หรือ Upload JOB..." + Environment.NewLine +
            //////        "#--------------------------------------------------------------------#" + Environment.NewLine +
            //////        "กรุณาต่อเนต แล้วทำการโหลดฐานข้อมูลได้ที่:" + Environment.NewLine +

            //////        "เข้าเมนู ปรับปรุงข้อมูล => เข้าเมนู (1) Download ฐานข้อมูล... เพื่อ" + Environment.NewLine +
            //////        "ทำการ Download ฐานข้อมูล และ Update ฐานข้อมูล Auto",
            //////        "ผลการตรวจสอบ...ฐานข้อมูลไม่ถูกต้อง ???...", MessageBoxButtons.OK, MessageBoxIcon.Error);

            //////    toolStripDropDownButton_MasterData.ShowDropDown();
            //////    downloadฐานขอมลToolStripMenuItem.Select();
            //////    return;
            //////}

            //////#endregion


            //////#region Check JOB Photo Report

            ////////    bool IsPartIN = false;

            ////////    DAL.SMSManage.JOB _job = null;
            ////////    DAL.SMSManage.Station _station = null;
            ////////    DAL.SMSManage.JOB_WorkID _wid = null;
            ////////    DAL.SMSManage.JOB_Detail_Spare _jobd = null;

            ////////    DAL.SMSManage.Conf_Upload_JOB_Photo_Report _conf_jphot = null;

            ////////    string strMaxPrice = "";
            ////////    decimal? maxPrice = 0;
            ////////    string _JOBId = "";

            ////////    List<DAL.SMSManage.JOB_PhotoReport_Detail> lsJSP = new List<SMSMINI.DAL.SMSManage.JOB_PhotoReport_Detail>();
            ////////    using (DAL.SMSManage.SMSManageDataContext dcMini = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            ////////    {
            ////////        var item = dcMini.JOBs
            ////////              .Where(t => t.EMP_ID3 == UserInfo.Van_ID &&
            ////////                      (t.IsUpload == "0"[0] || t.IsUpload == null) &&
            ////////                          new string[] { "07", "99" }.Contains(t.JOBS_ID))
            ////////              .FirstOrDefault();

            ////////        if (item != null)//มี JOB พร้อม Upload
            ////////        {
            ////////            _job = item;
            ////////            _JOBId = item.JOB_ID;

            ////////            var st = dcMini.Stations.Where(t => t.STA_ID == item.STA_ID).FirstOrDefault();
            ////////            _station = st;
            ////////            var stg = dcMini.Station_Groups.Where(t => t.GOP_ID == st.GOP_ID).FirstOrDefault();


            ////////            #region Conf_Upload_JOB_Photo_Report
            ////////            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            ////////            {
            ////////                _conf_jphot = dc.Conf_Upload_JOB_Photo_Reports
            ////////                    .Where(t => t.GOP_ID == st.GOP_ID && t.TYP_ID == item.TYP_ID && t.TYP_ID1 == item.TYP_ID1)
            ////////                    .FirstOrDefault();
            ////////                if (_conf_jphot != null)
            ////////                {//บังคับ Upload รูป
            ////////                    strMaxPrice = "กลุ่มบริษัท: " + stg.TradeName + "-" + stg.GroupStation + Environment.NewLine +
            ////////                       "ราคา JOB เกิน: " + _conf_jphot.MaxPrice.Value.ToString("#,#00.00") + " [บังคับ Upload รูปภาพอะไหล่] ";
            ////////                }
            ////////                else
            ////////                {
            ////////                    IsPartIN = false;
            ////////                    strMaxPrice = "";
            ////////                    //break;
            ////////                }

            ////////            }
            ////////            #endregion



            ////////            var wid = dcMini.JOB_WorkIDs.Where(t => t.JOB_ID == item.JOB_ID).FirstOrDefault();
            ////////            _wid = wid;

            ////////            var jsp = dcMini.JOB_Detail_Spares.Where(t => t.JOB_ID == item.JOB_ID && t.TRA_ID == "IN");
            ////////            foreach (var itemx in jsp)
            ////////            {
            ////////                lsJSP.Add(new DAL.SMSManage.JOB_PhotoReport_Detail
            ////////                {
            ////////                    Item = itemx.NO,
            ////////                    JOB_ID = itemx.JOB_ID,
            ////////                    StartDate = itemx.StartDate,
            ////////                    Enddate = (itemx.Enddate == null ? DateTime.Now : itemx.Enddate.Value),
            ////////                    ImgBefore = null,
            ////////                    ImgAfter = null,
            ////////                    Equipment = itemx.SPA_ID,
            ////////                    Decription = item.JobFailure_Detail,
            ////////                    Remark = "",
            ////////                    ImgBeforeRepair = null,
            ////////                    ImgAfterRepair = null,
            ////////                    TranDate = itemx.StartDate
            ////////                });
            ////////            }

            ////////            //ตรวจสอบ อะไหล่ IN
            ////////            //var maxPrice = 0;
            ////////            //////////////////////////

            ////////            var _jdp0 = dcMini.JOB_Detail_Spares
            ////////                .Where(t => t.JOB_ID == item.JOB_ID && t.TRA_ID == "IN");

            ////////            if (_jdp0.Count() > 0)
            ////////            {
            ////////                maxPrice = _jdp0.Sum(t => t.SumTotalPrices).Value;
            ////////            }

            ////////            if (_conf_jphot == null || _conf_jphot.MaxPrice == 0)//ไม่มีคอนฟิก
            ////////            {
            ////////                checkIsUploadJOBPhotoReport(ref IsPartIN, _job, _station, _wid, ref _jobd, dcMini, item);
            ////////                isMaxPrice = false;
            ////////            }
            ////////            else//มีคอนฟิก
            ////////            {
            ////////                if (maxPrice >= _conf_jphot.MaxPrice)//ราคา Job => ราคาที่คอนฟิก
            ////////                {
            ////////                    isMaxPrice = true;
            ////////                    checkIsUploadJOBPhotoReport(ref IsPartIN, _job, _station, _wid, ref _jobd, dcMini, item);
            ////////                }
            ////////            }
            ////////        }
            ////////    }

            ////////loopIsMaxPrice:

            ////////    if (IsPartIN)// ถ้า TranType = IN ให้ Upload JOB Photo
            ////////    {
            ////////        if (MessageBox.Show("ตรวจสอบอะไหล่  IN ใน JOB " + _JOBId + " ราคารวม =" + maxPrice.Value.ToString("#,#00.00") +
            ////////            " กรุณาทำการ Upload รูปภาพอะไหล่" + Environment.NewLine +
            ////////            Environment.NewLine +
            ////////            strMaxPrice + Environment.NewLine +
            ////////            Environment.NewLine +
            ////////            "รูปภาพอะไหล่ มีผลกับการเก็บเงินลูกค้า และ" + Environment.NewLine +
            ////////            "ค่าคอมมิชชั่นของ VAN โดยตรง" + Environment.NewLine +
            ////////            "คุณต้องการ Upload รูปภาพอะไหล่ ใช่หรือไม่ ?...", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            ////////        {
            ////////            try
            ////////            {

            ////////                Cursor.Current = Cursors.AppStarting;


            ////////                Transaction.JobUploadPhoto f = new JobUploadPhoto();
            ////////                f.StartPosition = FormStartPosition.CenterScreen;

            ////////                f._jOBNo = _job.JOB_ID;

            ////////                f._failureDetail = _job.JobFailure_Detail;
            ////////                f._startDate = _jobd.StartDate;
            ////////                f._endDate = _jobd.Enddate.Value;
            ////////                f._workID = (_wid == null ? "" : _wid.Work_ID);
            ////////                f._stationNo = _station.STA_ID;
            ////////                f._stationName = _station.StationSys;


            ////////                f.lsJPart = lsJSP.ToList();

            ////////                var _ok = f.ShowDialog();
            ////////                if (_ok == DialogResult.OK)
            ////////                {
            ////////                    isMaxPrice = f._IsMaxPrice;
            ////////                    Cursor.Current = Cursors.Default;
            ////////                }
            ////////            }
            ////////            catch (System.Exception ex)
            ////////            {
            ////////                MessageBox.Show("เกิดผิดพลาดในการใช้งานเมนู (4) Upload JOB Error " + ex.Message, "ข้อผิดพลาด");
            ////////                this.Show();
            ////////            }
            ////////        }

            ////////        if (isMaxPrice)
            ////////        {
            ////////            goto loopIsMaxPrice;
            ////////        }
            ////////    }

            //////#endregion


            //////#region Popup ยืนยัน Fixed asset
            ////////Popup ยืนยัน Fixed asset 
            ////////using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            ////////{
            ////////    var fIsConf = (from t in dc.FFixedAsset_Station_Details
            ////////                   join s in dc.Stations on t.STA_ID equals s.STA_ID
            ////////                   where t.STA_ID == vSTAConf && t.IsConfirm == false
            ////////                   select new
            ////////                   {
            ////////                       STA_ID = t.STA_ID,
            ////////                       StationSys = s.StationSys
            ////////                   }).FirstOrDefault();

            ////////    if (fIsConf != null)
            ////////    {
            ////////        if (MessageBox.Show("สถานี: " + fIsConf.STA_ID.ToString() + " " + fIsConf.StationSys.ToString() + Environment.NewLine +
            ////////       "ยังไม่บันทึก ยืนยัน ความถูกต้องของ Fixed Asset" + Environment.NewLine +
            ////////       "กรุณา เข้าไป บันทึกยืนยันความถูกต้อง", "ผลการตรวจสอบ...ยืนยัน Fixed Asset", MessageBoxButtons.OK, MessageBoxIcon.Error) == DialogResult.OK)
            ////////        {
            ////////            toolStripDropDownButton_MasterData.ShowDropDown();
            ////////            ยนยนขอมลFixedAssetToolStripMenuItem.Select();
            ////////        }//if (MessageBox
            ////////    }//if (fIsConf.Count() > 0)

            ////////}




            //////#endregion


            //////#region Upload Form
            //////try
            //////{
            //////    Cursor.Current = Cursors.AppStarting;

            //////    Transaction.JobUpload f = new SMSMINI.Transaction.JobUpload();
            //////    f.StartPosition = FormStartPosition.CenterScreen;
            //////    f.checkUpload = false;
            //////    if (f.ShowDialog() == DialogResult.Abort)
            //////        Cursor.Current = Cursors.Default;

            //////    if (f.IsFFAOK == true)
            //////    {
            //////        //toolStripDropDownButton_MasterData.ShowDropDown();
            //////        //ยนยนขอมลFixedAssetToolStripMenuItem.Select();


            //////        Cursor.Current = Cursors.AppStarting;
            //////        FFA.Input_FFA ffa = new SMSMINI.FFA.Input_FFA();
            //////        ffa.IsConfirmFFA = true;
            //////        // ffa.tst_txtFind.Text = "";
            //////        ffa.statID = f.statID;

            //////        ffa.WindowState = FormWindowState.Maximized;
            //////        if (ffa.ShowDialog() == DialogResult.Abort)
            //////            Cursor.Current = Cursors.Default;

            //////    }
            //////}
            //////catch (System.Exception ex)
            //////{
            //////    MessageBox.Show("เกิดผิดพลาดในการใช้งานเมนู (4) Upload JOB Error " + ex.Message, "ข้อผิดพลาด");
            //////    this.Show();
            //////}
            //////#endregion

        }

        //private void checkIsUploadJOBPhotoReport(ref bool IsPartIN, DAL.SMSManage.JOB _job, DAL.SMSManage.Station _station, DAL.SMSManage.JOB_WorkID _wid, ref DAL.SMSManage.JOB_Detail_Spare _jobd, DAL.SMSManage.SMSManageDataContext dcMini, SMSMINI.DAL.SMSManage.JOB item)
        //{
        //    //////////////////////////
        //    var jdp = dcMini.JOB_Detail_Spares
        //        .Where(t => t.JOB_ID == item.JOB_ID && t.TRA_ID == "IN")
        //        .FirstOrDefault();
        //    if (jdp != null)//มีอะไหล่ IN
        //    {
        //        //maxPrice = jdp.
        //        _jobd = jdp;
        //        var j2 = dcMini.JOB_PhotoReports
        //            .Where(t => t.JOB_ID == item.JOB_ID// &&
        //            // t.StartDate == jdp.StartDate &&
        //            // t.Enddate == jdp.Enddate                                  
        //            ).FirstOrDefault();

        //        if (j2 == null)//ยังไม่มีข้อมูล Upload JOB Photo
        //        {
        //            IsPartIN = true;//ให้ Upload JOB Photo
        //            isMaxPrice = true;
        //        }
        //        else
        //        {
        //            try
        //            {

        //                Cursor.Current = Cursors.AppStarting;

        //                Transaction.JobUploadPhoto f = new JobUploadPhoto();
        //                f.StartPosition = FormStartPosition.CenterScreen;

        //                f._jOBNo = jdp.JOB_ID;

        //                f._failureDetail = _job.JobFailure_Detail;
        //                f._startDate = jdp.StartDate;
        //                f._endDate = jdp.Enddate.Value;
        //                f._workID = (_wid == null ? "" : _wid.Work_ID);
        //                f._stationNo = _station.STA_ID;
        //                f._stationName = _station.StationSys;

        //                var _ok = f.ShowDialog();
        //                if (_ok == DialogResult.OK)
        //                {
        //                    isMaxPrice = f._IsMaxPrice;
        //                    Cursor.Current = Cursors.Default;
        //                }
        //            }
        //            catch (System.Exception ex)
        //            {
        //                MessageBox.Show("เกิดผิดพลาดในการใช้งานเมนู (4) Upload JOB Error " + ex.Message, "ข้อผิดพลาด");
        //                this.Show();
        //            }

        //        }
        //    }
        //}



        private void toolStripBt_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!isDownloadDBComplete)
            {
                if (MessageBox.Show("คุณต้องการออกจากโปรแกรม ใช่หรือไม่ ? !!!", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    e.Cancel = true;
            }
        }

        private void toolStripBt_LogOff_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.No;
            this.Close();
        }

        private void Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (DialogResult != DialogResult.No)
            {
                Application.Exit();
            }
        }

        private void toolStripButton_About_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.AppStarting;
                About f = new About();
                f.ShowDialog();
                f.StartPosition = FormStartPosition.CenterScreen;
                Cursor.Current = Cursors.Default;
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("เกิดผิดพลาดในการใช้งานเมนูDownload Job Error " + ex.Message, "ข้อผิดพลาด");
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            m_fmProgress.Activate();
            m_fmProgress.BringToFront();
            m_fmProgress.TopMost = true;
            m_fmProgress.ShowDialog();
        }

        private void ขอมลสถานStationToolStripMenuItem_Click(object sender, EventArgs e)
        {

            //try
            //{
            //    Cursor.Current = Cursors.AppStarting;
            //    Transaction.Local_JobDeletel f = new Transaction.Local_JobDeletel();
            //    f.StartPosition = FormStartPosition.CenterScreen;
            //    f.ShowDialog();
            //    Cursor.Current = Cursors.Default;
            //}
            //catch (System.Exception ex)
            //{
            //    MessageBox.Show("เกิดผิดพลาดในการใช้งานเมนูDownload Job Error " + ex.Message, "ข้อผิดพลาด");
            //}

        }


        private void เปลยนPasswordToolStripMenuItem_Click(object sender, EventArgs e)
        {

            try
            {
                Cursor.Current = Cursors.AppStarting;
                this.Hide();
                MasterData.ChangPassword f = new SMSMINI.MasterData.ChangPassword();
                f.StartPosition = FormStartPosition.CenterScreen;
                if (f.ShowDialog() == DialogResult.Abort)
                    this.Show();

                Cursor.Current = Cursors.Default;
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("เกิดผิดพลาดในการใช้งานเมนู เปลี่ยน Password Error " + ex.Message, "ข้อผิดพลาด");
            }
        }



        private void toolStripMenuItem6_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    Cursor.Current = Cursors.AppStarting;
            //    this.Hide();
            //    SMSMINI.Transaction.Local_JobDeletel f = new SMSMINI.Transaction.Local_JobDeletel();
            //    f.StartPosition = FormStartPosition.CenterScreen;
            //    if (f.ShowDialog() == DialogResult.Abort)
            //        this.Show();

            //    Cursor.Current = Cursors.Default;
            //}
            //catch (System.Exception ex)
            //{
            //    MessageBox.Show("เกิดผิดพลาดในการใช้งานเมนู ล้างข้อมูล JOB " + ex.Message, "ข้อผิดพลาด");
            //}
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("IEXPLORE.EXE", "http://janawat.spaces.live.com/blog/cns!7D608959D854CB28!3006.entry");
        }


        private void pictureBox3_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("IEXPLORE.EXE", "http://www.flowco.co.th");
        }

        private void sMSWebToolStripMenuItem_Click(object sender, EventArgs e)
        {
            /* //20190916
            string url = SMSMINI.Properties.Settings.Default.SMSweb;
            string strUrl = url + "/Login.aspx?req_user=" + UserInfo.UserName + "&req_pass=" + UserInfo.Password + "";
            System.Diagnostics.Process.Start("IEXPLORE.EXE", strUrl);
             */
            var url = GetURL.smsURL("SMSService");
            if (url != "")
                System.Diagnostics.Process.Start(url + "/Login.aspx?req_user=" + UserInfo.UserName + "&req_pass=" + UserInfo.Password + "");
            else
                System.Diagnostics.Process.Start("http://sms.flowco.co.th/sms/Login.aspx?req_user=" + UserInfo.UserName + "&req_pass=" + UserInfo.Password + "");

        }

        private void uploadMonitoringToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    Cursor.Current = Cursors.AppStarting;
            //    Transaction.JobUploadMonitor f = new SMSMINI.Transaction.JobUploadMonitor();
            //    f.StartPosition = FormStartPosition.CenterScreen;
            //    if (f.ShowDialog() == DialogResult.Abort)
            //        Cursor.Current = Cursors.Default;
            //}
            //catch (System.Exception ex)
            //{
            //    MessageBox.Show("เกิดผิดพลาดในการใช้งานเมนู UploadMonitoring Error " + ex.Message, "ข้อผิดพลาด");
            //    this.Show();
            //}
        }



        private void ขอมลTranTypeToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    Cursor.Current = Cursors.AppStarting;
            //    MasterData.PartTranType f = new SMSMINI.MasterData.PartTranType();
            //    f.StartPosition = FormStartPosition.CenterScreen;
            //    if (f.ShowDialog() == DialogResult.Abort)

            //        Cursor.Current = Cursors.Default;
            //}
            //catch (System.Exception ex)
            //{
            //    MessageBox.Show("เกิดผิดพลาดในการใช้งานเมนู ข้อมูล TranType " + ex.Message, "ข้อผิดพลาด");
            //}
        }

        private void ขอมลจดเสยToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    Cursor.Current = Cursors.AppStarting;
            //    MasterData.PointFailure f = new SMSMINI.MasterData.PointFailure();
            //    f.StartPosition = FormStartPosition.CenterScreen;
            //    if (f.ShowDialog() == DialogResult.Abort)

            //        Cursor.Current = Cursors.Default;
            //}
            //catch (System.Exception ex)
            //{
            //    MessageBox.Show("เกิดผิดพลาดในการใช้งานเมนู ข้อมูลจุดเสีย " + ex.Message, "ข้อผิดพลาด");
            //}
        }

        private void ขอมลผใชงานVANToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.AppStarting;
                MasterData.Van f = new SMSMINI.MasterData.Van();
                f.vMode = "login";
                f.StartPosition = FormStartPosition.CenterScreen;
                if (f.ShowDialog() == DialogResult.Abort)

                    Cursor.Current = Cursors.Default;
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("เกิดผิดพลาดในการใช้งานเมนู ข้อมูลผู้ใช้งาน (VAN) Error " + ex.Message, "ข้อผิดพลาด");
            }
        }

        private void fffToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.AppStarting;
                MasterData.Station f = new SMSMINI.MasterData.Station();
                f.StartPosition = FormStartPosition.CenterScreen;
                if (f.ShowDialog() == DialogResult.Abort)

                    Cursor.Current = Cursors.Default;
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("เกิดผิดพลาดในการใช้งานเมนู ข้อมูลสถานี (Station) Error " + ex.Message, "ข้อผิดพลาด");
            }
        }

        private void ddddToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.AppStarting;
                MasterData.Part f = new SMSMINI.MasterData.Part();
                f.StartPosition = FormStartPosition.CenterScreen;
                if (f.ShowDialog() == DialogResult.Abort)

                    Cursor.Current = Cursors.Default;
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("เกิดผิดพลาดในการใช้งานเมนู Manual Update Part Error " + ex.Message, "ข้อผิดพลาด");
            }

        }

        private void vvvvToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    Cursor.Current = Cursors.AppStarting;
            //    SMSMINI.Transaction.Local_JobDeletel f = new SMSMINI.Transaction.Local_JobDeletel();
            //    f.StartPosition = FormStartPosition.CenterScreen;
            //    if (f.ShowDialog() == DialogResult.Abort)

            //        Cursor.Current = Cursors.Default;
            //}
            //catch (System.Exception ex)
            //{
            //    MessageBox.Show("เกิดผิดพลาดในการใช้งานเมนู ล้างข้อมูล JOB " + ex.Message, "ข้อผิดพลาด");
            //}
        }

        private void เปลยนPasswordToolStripMenuItem1_Click(object sender, EventArgs e)
        {

            try
            {
                Cursor.Current = Cursors.AppStarting;
                MasterData.ChangPassword f = new SMSMINI.MasterData.ChangPassword();
                f.StartPosition = FormStartPosition.CenterScreen;
                if (f.ShowDialog() == DialogResult.Abort)

                    Cursor.Current = Cursors.Default;
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("เกิดผิดพลาดในการใช้งานเมนู เปลี่ยน Password Error " + ex.Message, "ข้อผิดพลาด");
            }
        }

        private void networkConnectionTestToolStripMenuItem_Click(object sender, EventArgs e)
        {

            try
            {
                Cursor.Current = Cursors.AppStarting;

                NetworkConnectionTest f = new NetworkConnectionTest();
                f.StartPosition = FormStartPosition.CenterScreen;
                f.WindowState = FormWindowState.Normal;
                f.ShowDialog();
                Cursor.Current = Cursors.Default;
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("เกิดผิดพลาดในการใช้งานเมนู Network Connection Test " + ex.Message, "ข้อผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void รจกผพฒนาToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("IEXPLORE.EXE", "http://janawat.spaces.live.com/blog/cns!7D608959D854CB28!4262.entry");
        }

        private void pricesListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                //Cursor.Current = Cursors.AppStarting;

                //Report.Report_PartPricesList f = new SMSMINI.Report.Report_PartPricesList();
                //f.StartPosition = FormStartPosition.CenterScreen;

                //f.ShowDialog();
                //Cursor.Current = Cursors.Default;

                //--=====
                //20190916 System.Diagnostics.Process.Start("IEXPLORE.EXE", " http://sms.flowco.co.th/sms/Reports/RptPriceList.aspx");
                var url = GetURL.smsURL("SMSService");
                if (url != "")
                    System.Diagnostics.Process.Start(url + "/Reports/RptPriceList.aspx");
                else
                    System.Diagnostics.Process.Start("http://sms.flowco.co.th/sms/Reports/RptPriceList.aspx");
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("เกิดผิดพลาดในการใช้งานเมนู Part Prices List " + ex.Message, "ข้อผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void รายงานTAGPrintToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.AppStarting;

                Transaction.ReturnPartsRoutingTagReport f = new Transaction.ReturnPartsRoutingTagReport();
                f.TmpJobID = null;
                if (f.ShowDialog() == DialogResult.Abort)
                    Cursor.Current = Cursors.Default;
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("ข้อผิดพลาด " + ex.Message, "เกิดข้อผิดพลาด");
            }
        }

        private void manualUpdatePartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.AppStarting;
                MasterData.Part f = new SMSMINI.MasterData.Part();
                f.StartPosition = FormStartPosition.CenterScreen;
                if (f.ShowDialog() == DialogResult.Abort)

                    Cursor.Current = Cursors.Default;
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("เกิดผิดพลาดในการใช้งานเมนู Manual Update Part Error " + ex.Message, "ข้อผิดพลาด");
            }
        }

        private void autoUpdatePartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("ขออภัย ยังไม่สามารถใช้งานเมนูนี้ เนื่องจาก อยู่ในช่วงกำลังพัฒนา", "ผลการตรวจสอบ");
            return;
        }

        private void updatePriceListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    Cursor.Current = Cursors.AppStarting;
            //    MasterData.frmUpdatePartPriceList f = new SMSMINI.MasterData.frmUpdatePartPriceList();
            //    f.StartPosition = FormStartPosition.CenterScreen;
            //    f.toolStripLabel_cobby.SelectedIndex = 3;
            //    f.toolStripComboBox_PriceDiscount.SelectedIndex = 1;
            //    if (f.ShowDialog() == DialogResult.Abort)


            //        Cursor.Current = Cursors.Default;
            //}
            //catch (System.Exception ex)
            //{
            //    MessageBox.Show("เกิดผิดพลาดในการใช้งานเมนู Update PriseLis/ส่วนลดตามไตรมาส" + ex.Message, "ข้อผิดพลาด");
            //}
        }

        private void fAMToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            checkSystemDate();

            showFAN();
        }

        private void checkSystemDate()
        {
            string conn = Properties.Settings.Default["ServicesMSDBConnectionString"].ToString();
            try
            {
                DateTime? srv = null;
                DateTime localDate = DateTime.Now;
                var curDate = DateTime.Now;

                using (DAL.SMSManage.SMSManageDataContext dc =
                    new SMSMINI.DAL.SMSManage.SMSManageDataContext(conn))
                {

                    srv = dc
                        .ExecuteQuery<DateTime>("SELECT GETDATE()")
                        .First();
                }

                localDate = DateTime.Now;
                TimeSpan s = localDate - curDate;
                double numberofdays = s.TotalMinutes;

                localDate = localDate.AddMinutes(numberofdays);

                TimeSpan diffResult = localDate.Subtract(srv.Value);

                if (srv.Value.Date != localDate.Date)
                {
                    MessageBox.Show("วันที่บนเครื่อง Client(mini) ไม่ตรงกับ Server(manage) กรุณาตรวจสอบ..." + Environment.NewLine +
                        "Server(manage):\t\t" + srv.Value + Environment.NewLine +
                        "Client(mini):\t\t" + localDate, "ผลการตรวจสอบ...",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if ((diffResult.Hours > 0) || (diffResult.Minutes >= 5))// if ((srv.Value.Hour != localDate.Hour) || (srv.Value.Minute != localDate.Minute))
                {
                    MessageBox.Show("เวลาบนเครื่อง Client(mini) ไม่ตรงกับ Server(manage) กรุณาตรวจสอบ..." + Environment.NewLine +
                        "Server(manage):\t\t" + srv.Value + Environment.NewLine +
                        "Client(mini):\t\t" + localDate, "ผลการตรวจสอบ...",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception)
            {

            }
        }


        private static void showFAN()
        {

        }

        private void เบกอะใหลToolStripMenuItem_Click(object sender, EventArgs e)
        {

            try
            {
                Cursor.Current = Cursors.AppStarting;

                Transaction.PartsRequisition f = new SMSMINI.Transaction.PartsRequisition();
                f.WindowState = FormWindowState.Maximized;

                if (f.ShowDialog() == DialogResult.Abort)

                    Cursor.Current = Cursors.Default;
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("เกิดผิดพลาดในการใช้งานเมนู Update PriseLis/ส่วนลดตามไตรมาส" + ex.Message, "ข้อผิดพลาด");
            }
        }

        private void updateเงอนไขสญญาToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    Cursor.Current = Cursors.AppStarting;

            //    MasterData.AutoUpdateMasterData_Contract f = new SMSMINI.MasterData.AutoUpdateMasterData_Contract();
            //    f.StartPosition = FormStartPosition.CenterScreen;

            //    if (f.ShowDialog() == DialogResult.Abort)
            //        Cursor.Current = Cursors.Default;
            //}
            //catch (System.Exception ex)
            //{
            //    MessageBox.Show("เกิดผิดพลาดในการใช้งานเมนู Update เงื่อนไขสัญญา" + ex.Message, "ข้อผิดพลาด");
            //}

        }

        private void อปเดตฐานขอมลToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    Cursor.Current = Cursors.AppStarting;

            //    MasterData.SMSminiRestoreDB f = new SMSMINI.MasterData.SMSminiRestoreDB();
            //    f.StartPosition = FormStartPosition.CenterScreen;

            //    if (f.ShowDialog() == DialogResult.Abort)
            //    {
            //        Cursor.Current = Cursors.Default;
            //        toolStripBt_LogOff_Click(null, null);
            //    }
            //}
            //catch (System.Exception ex)
            //{
            //    MessageBox.Show("เกิดผิดพลาดในการใช้งานเมนู Update ฐานข้อมูล" + ex.Message, "ข้อผิดพลาด");
            //}

        }

        private void downloadฐานขอมลToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    Cursor.Current = Cursors.AppStarting;

            //    Download.DownloadManager f = new SMSMINI.Download.DownloadManager();
            //    f.StartPosition = FormStartPosition.CenterScreen;

            //    if (f.ShowDialog() == DialogResult.Abort)
            //    {
            //        if (f.isDownloadComplete)
            //        {
            //            isDownloadDBComplete = f.isDownloadComplete;
            //            Cursor.Current = Cursors.Default;

            //            Application.Exit();
            //        }
            //    }
            //}
            //catch (System.Exception ex)
            //{
            //    MessageBox.Show("เกิดผิดพลาดในการใช้งานเมนู Download ฐานข้อมูล" + ex.Message, "ข้อผิดพลาด");
            //}
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://janawat.wordpress.com/about/");
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.flowco.co.th/");
        }

        private void updateSQLScriptToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    Cursor.Current = Cursors.AppStarting;

            //    MasterData.AutoUpdateSQLScript f = new SMSMINI.MasterData.AutoUpdateSQLScript();
            //    f.StartPosition = FormStartPosition.CenterScreen;

            //    if (f.ShowDialog() == DialogResult.Abort)
            //    {
            //        if (f.isDownloadComplete)
            //        {
            //            isDownloadDBComplete = f.isDownloadComplete;
            //            Cursor.Current = Cursors.Default;

            //            Application.Exit();
            //        }
            //    }
            //}
            //catch (System.Exception ex)
            //{
            //    MessageBox.Show("เกิดผิดพลาดในการใช้งานเมนู Update SQL Script" + ex.Message, "ข้อผิดพลาด");
            //}
        }

        private void เปดJOBToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //if (MessageBox.Show("คุณต้องการเปิด JOB เบิกค่า ใช้จ่ายพิเศษ/Mileage ใช่หรือไม่?...", "คำยืนยัน", 
            //    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            //{
            //    Cursor = Cursors.AppStarting;
            //    JOBMileage.JOBMileageClient svc = new SMSMINI.JOBMileage.JOBMileageClient();
            //    string mess = svc.OpenJOBMileage(UserInfo.UserId, "");

            //    Cursor = Cursors.Default;
            //    MessageBox.Show(mess,"ผลการเปิด JOB",MessageBoxButtons.OK,MessageBoxIcon.Information );
            //}

            //20190916 System.Diagnostics.Process.Start("http://sms.flowco.co.th/sms/jml.aspx");
            var url = GetURL.smsURL("SMSService");
            if (url != "")
                System.Diagnostics.Process.Start(url + "/jml.aspx");
            else
                System.Diagnostics.Process.Start("http://sms.flowco.co.th/sms/jml.aspx");

        }

        private void พมพรายงานFailureCodeWIWPonlineToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int xPos = this.Width / 2;
            int yPos = this.Height / 2;
            string defaultJobID = "";
            string _jobID = "";
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn))
            {
                var q = dc.JOBs.OrderByDescending(t => t.JOB_ID).FirstOrDefault();
                if (q != null)
                    defaultJobID = q.JOB_ID;
            }
            _jobID = Microsoft.VisualBasic.Interaction.InputBox("กรุณา ป้อนหมายเลข JOB:?" + Environment.NewLine + "[Default JOB ล่าสุด]", "พิมพ์ FailureCode WI/WP", defaultJobID, xPos, yPos);
            if (!string.IsNullOrEmpty(_jobID))
            {
                Report.PrintFailureCodeWIWP f = new SMSMINI.Report.PrintFailureCodeWIWP();
                f.tmpJOBID = _jobID;
                f.mPrint = true;
                f.ShowDialog();
            }
        }

        private void พมพรายงานServicesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void พมพJOBPhotoReportsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {

                string _JOBNo = "";
                Cursor.Current = Cursors.AppStarting;

                Transaction.Finding.FindJOB_PhotoReport f = new SMSMINI.Transaction.Finding.FindJOB_PhotoReport();
                f.StartPosition = FormStartPosition.CenterScreen;

                if (f.ShowDialog() == DialogResult.OK)
                {
                    _JOBNo = f.JOBNo;
                    if (UserInfo.IsSVPrintOnline == 1)//20210817
                    {
                        var xuser = new xUserInfo();

                        xuser.UserId = UserInfo.UserId;
                        xuser.UserName = UserInfo.UserName;
                        xuser.Password = UserInfo.Password;
                        xuser.UserLevID = UserInfo.UserLevID;
                        xuser.UserLevel = UserInfo.UserLevel;
                        xuser.FullName = UserInfo.FullName;
                        xuser.Van_ID = UserInfo.Van_ID;
                        xuser.SUP_VAN = UserInfo.SUP_VAN;
                        xuser.Email = UserInfo.Email;

                        xuser.Van_Type = UserInfo.Van_Type;
                        xuser.VehicleRegis = UserInfo.VehicleRegis;


                        xuser.ConnectMode = UserInfo.ConnectMode;

                        xuser.eDepID = UserInfo.eDepID;
                        xuser.IsSVPrintOnline = UserInfo.IsSVPrintOnline;

                        var json = JsonConvert.SerializeObject(xuser);


                        //http://sms.flowco.co.th/smssvprint/

                        //var mainUrl = "http://localhost:1069/";
                        var mainUrl = "http://sms.flowco.co.th/smssvprint/";
                        var Photoprint = mainUrl + "TAGPrint.aspx?JOB_ID=" + _JOBNo.Trim() + "&EndDate=xx&EndTime=xx&chkUpload=1&xUserInfo=" + json + "";
                        System.Diagnostics.Process.Start(Photoprint);
                    }
                    else
                    {
                       
                        Transaction.ServiceJOB_PhotoReport fr = new ServiceJOB_PhotoReport();
                        fr.StartPosition = FormStartPosition.CenterScreen;
                        fr._JOBNo = _JOBNo;
                        fr.ShowDialog();
                        Cursor.Current = Cursors.Default;
                    }
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("เกิดผิดพลาดในการใช้งานเมนู Update เงื่อนไขสัญญา" + ex.Message, "ข้อผิดพลาด");
            }
        }

        private void uploadJOBPhotoToolStripMenuItem_Click(object sender, EventArgs e)
        {


            ReUploadPhotoMethod();

        }

        private static void ReUploadPhotoMethod()
        {
            try
            {
                clGetJOBPhoto _jPhoto = null;

                Cursor.Current = Cursors.AppStarting;

                Transaction.Finding.Find_JOBPhoto f0 = new SMSMINI.Transaction.Finding.Find_JOBPhoto();
                f0.StartPosition = FormStartPosition.CenterScreen;

                if (f0.ShowDialog() == DialogResult.OK)
                {
                    _jPhoto = f0.jPhoto;

                    Transaction.JobUploadPhoto f1 = new JobUploadPhoto();
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1._MODE = "1";
                    f1._jOBNo = _jPhoto.JOB_ID;

                    f1._failureDetail = _jPhoto.JobFailure_Detail;
                    f1._startDate = _jPhoto.StartDate;
                    f1._endDate = _jPhoto.EndDate == null ? DateTime.Now : _jPhoto.EndDate.Value;
                    f1._workID = (_jPhoto.Work_ID == null ? "" : _jPhoto.Work_ID);
                    f1._stationNo = _jPhoto.STA_ID;
                    f1._stationName = _jPhoto.StationSys;

                    //==20201026==
                    //f.lsJPart = lsJSP.ToList();
                    //f._LConfTRA = LConfTRA.ToList();
                    //============

                    if (f1.ShowDialog() == DialogResult.Abort)
                        Cursor.Current = Cursors.Default;









                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("เกิดผิดพลาดในการใช้งานเมนู Re-Upload JOB Photo..." + ex.Message, "ข้อผิดพลาด");
            }
        }

        private void uploadJOBPhotoToolStripMenuItem1_Click(object sender, EventArgs e)
        {

            //try
            //{
            //    clGetJOBPhoto _jPhoto = null;

            //    Cursor.Current = Cursors.AppStarting;

            //    var f0 = new SMSMINI.Transaction.Finding.Find_JOBPhoto();
            //    f0.StartPosition = FormStartPosition.CenterScreen;

            //    if (f0.ShowDialog() == DialogResult.OK)
            //    {
            //        _jPhoto = f0.jPhoto;

            //        JOBSlipUpload f1 = new JOBSlipUpload();
            //        f1.StartPosition = FormStartPosition.CenterScreen;

            //        f1._jOBNo = _jPhoto.JOB_ID;

            //        f1._failureDetail = _jPhoto.JobFailure_Detail;
            //        f1._startDate = _jPhoto.StartDate;
            //        f1._endDate = _jPhoto.EndDate == null ? DateTime.Now : _jPhoto.EndDate.Value;
            //        f1._stationNo = _jPhoto.STA_ID;
            //        f1._stationName = _jPhoto.StationSys;

            //        if (f1.ShowDialog() == DialogResult.Abort)
            //            Cursor.Current = Cursors.Default;

            //    }
            //}
            //catch (System.Exception ex)
            //{
            //    MessageBox.Show("เกิดผิดพลาดในการใช้งานเมนู Re-Upload JOB Photo..." + ex.Message, "ข้อผิดพลาด");
            //}

        }

        private void บนทกขอมลFixedAssetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFFAMethod();
        }

        private void SaveFFAMethod()
        {
            try
            {
                Cursor.Current = Cursors.AppStarting;
                FFA.Input_FFA f = new SMSMINI.FFA.Input_FFA();
                f.IsConfirmFFA = false;
                f.WindowState = FormWindowState.Maximized;
                if (f.ShowDialog() == DialogResult.Abort)
                    Cursor.Current = Cursors.Default;
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("เกิดผิดพลาดในการใช้งานเมนู บันทึกข้อมูล  Fixed Asset " + Environment.NewLine
                    + ex.Message, "ข้อผิดพลาด");
                this.Show();
            }
        }



        private void ยนยนขอมลFixedAssetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConfirmFFAMethod();
        }

        private void ConfirmFFAMethod()
        {
            try
            {
                Cursor.Current = Cursors.AppStarting;
                FFA.Input_FFA f = new SMSMINI.FFA.Input_FFA();
                f.IsConfirmFFA = true;
                f.WindowState = FormWindowState.Maximized;
                if (f.ShowDialog() == DialogResult.Abort)
                    Cursor.Current = Cursors.Default;
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("เกิดผิดพลาดในการใช้งานเมนู ยืนยัน ข้อมูล  Fixed Asset " + Environment.NewLine
                    + ex.Message, "ข้อผิดพลาด");
                this.Show();
            }
        }

        private void ขอมลFixedAssetToolStripMenuItem_Click(object sender, EventArgs e)
        {/*20210308
            try
            {
                Cursor.Current = Cursors.AppStarting;
                var f = new SMSMINI.MasterData.AutoUpdateMasterData_SNTracking ();

                f.STAID = null;
                f.MODE = "Click";

                if (f.ShowDialog() == DialogResult.Abort)
                    Cursor.Current = Cursors.Default;
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("เกิดผิดพลาดในการใช้งานเมนู บันทึกข้อมูล  Fixed Asset " + Environment.NewLine
                    + ex.Message, "ข้อผิดพลาด");
                this.Show();
            }*/
        }

        private void ขอมลToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.AppStarting;
                var f = new SMSMINI.MasterData.FixedAsset_SerialNumber_Master();


                if (f.ShowDialog() == DialogResult.Abort)
                    Cursor.Current = Cursors.Default;
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("เกิดผิดพลาดในการใช้งานเมนู ข้อมูล Fixed Asset SerialNumber Master " + Environment.NewLine
                    + ex.Message, "ข้อผิดพลาด");
                this.Show();
            }
        }

        private void SurveyDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            /* 
            string url = "http://sms.flowco.co.th/surv";
          //  string strUrl = url + "/Login.aspx?req_user=" + UserInfo.UserName + "&req_pass=" + UserInfo.Password + "";
            string strUrl = url + "/Login.aspx?req_user=" + UserInfo.UserName + "&req_pass=" + UserInfo.Password + "";
            System.Diagnostics.Process.Start("IEXPLORE.EXE", strUrl);

            */
            var url = GetURL.smsURL("SMSSurvey");
            if (url != "")
                //SMSService
                System.Diagnostics.Process.Start(url + "/Login.aspx?req_user=" + UserInfo.UserName + "&req_pass=" + UserInfo.Password + "");
            else
                System.Diagnostics.Process.Start("http://sms.flowco.co.th/surv/Login.aspx?req_user=" + UserInfo.UserName + "&req_pass=" + UserInfo.Password + "");



            //System.Diagnostics.Process.Start("http://sms.flowco.co.th/sms/jml.aspx");
            //string url = SMSMINI.Properties.Settings.Default.SMSweb;
            //string strUrl = url + "/Login.aspx?req_user=" + UserInfo.UserName + "&req_pass=" + UserInfo.Password + "";
            //System.Diagnostics.Process.Start("IEXPLORE.EXE", strUrl);

        }

        private void พมพใบปะหนาSOToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.AppStarting;
                Report.PrintReportCoverSO f = new SMSMINI.Report.PrintReportCoverSO();

                f.WindowState = FormWindowState.Maximized;

                if (f.ShowDialog() == DialogResult.Abort)
                    Cursor.Current = Cursors.Default;
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("เกิดผิดพลาดในการใช้งานเมนู พิมพ์ใบปะหน้า SO Error " + Environment.NewLine
                    + ex.Message, "ข้อผิดพลาด");
                this.Show();
            }
        }

        private void EditJOBOnlineToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.AppStarting;
                Transaction.JOBOnline f = new SMSMINI.Transaction.JOBOnline();

                f.WindowState = FormWindowState.Maximized;

                if (f.ShowDialog() == DialogResult.Abort)
                    Cursor.Current = Cursors.Default;
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("เกิดผิดพลาดในการใช้งานเมนู แก้ไขข้อมูล JOB ย้อนหลัง " + Environment.NewLine
                    + ex.Message, "ข้อผิดพลาด");
                this.Show();
            }
        }

        private void btSMSweb_Click(object sender, EventArgs e)
        {
            var url = GetURL.smsURL("SMSService");
            if (url != "")
                System.Diagnostics.Process.Start(url + "/Login.aspx?req_user=" + UserInfo.UserName + "&req_pass=" + UserInfo.Password + "");
            else
                System.Diagnostics.Process.Start("http://sms.flowco.co.th/sms/Login.aspx?req_user=" + UserInfo.UserName + "&req_pass=" + UserInfo.Password + "");

        }

        private void btMileageStatement_Click(object sender, EventArgs e)
        {
            var url = GetURL.smsURL("MileageStatement");
            if (url != "")
                System.Diagnostics.Process.Start(url + "/Login.aspx?req_user=" + UserInfo.UserName + "&req_pass=" + UserInfo.Password + "");
            else
                System.Diagnostics.Process.Start("http://sms.flowco.co.th/sms/Mileage/MLReportByVAN.aspx?req_user=" + UserInfo.UserName + "&req_pass=" + UserInfo.Password + "");

        }

        private void btSTART_STOP_Mileage_Click(object sender, EventArgs e)
        {
            var url = GetURL.smsURL("START_STOP_Mileage");
            if (url != "")
                System.Diagnostics.Process.Start(url + "/Login.aspx?req_user=" + UserInfo.UserName + "&req_pass=" + UserInfo.Password + "");
            else
                System.Diagnostics.Process.Start("http://ams.flowco.co.th/sms/Mileage/ChkML.aspx?req_user=" + UserInfo.UserName + "&req_pass=" + UserInfo.Password + "");

        }

        private void btSMSSurvey_Click(object sender, EventArgs e)
        {
            var url = GetURL.smsURL("SMSSurvey");
            if (url != "")
                System.Diagnostics.Process.Start(url + "/Login.aspx?req_user=" + UserInfo.UserName + "&req_pass=" + UserInfo.Password + "");
            else
                System.Diagnostics.Process.Start("http://sms.flowco.co.th/surv/Login.aspx?req_user=" + UserInfo.UserName + "&req_pass=" + UserInfo.Password + "");


        }

        private void btCloseJOB_Click(object sender, EventArgs e)
        {
            CloseJOBMethod();
        }

        private void btFFA_Save_Click(object sender, EventArgs e)
        {
            SaveFFAMethod();
        }

        private void btFFA_Confirm_Click(object sender, EventArgs e)
        {
            ConfirmFFAMethod();
        }

        private void btReUploadPhoto_Click(object sender, EventArgs e)
        {
            ReUploadPhotoMethod();
        }


    }
}
