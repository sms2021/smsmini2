﻿namespace SMSMINI.MasterData
{
    partial class Van
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Van));
            this.dataGridView_Users = new System.Windows.Forms.DataGridView();
            this.toolStrip_Mnu = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel_cobby = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripText_txtFind = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_FindStation = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_Check = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_Cancel = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_Exit = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel_Dateime = new System.Windows.Forms.ToolStripLabel();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.dataGridView_Type = new System.Windows.Forms.DataGridView();
            this.dataGridView_VAN = new System.Windows.Forms.DataGridView();
            this.dataGridView_Users_Level = new System.Windows.Forms.DataGridView();
            this.dataGridView_Employees = new System.Windows.Forms.DataGridView();
            this.toolStripButton_Update = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Users)).BeginInit();
            this.toolStrip_Mnu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Type)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_VAN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Users_Level)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Employees)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView_Users
            // 
            this.dataGridView_Users.AllowUserToAddRows = false;
            this.dataGridView_Users.AllowUserToDeleteRows = false;
            this.dataGridView_Users.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_Users.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_Users.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_Users.Location = new System.Drawing.Point(3, 42);
            this.dataGridView_Users.Name = "dataGridView_Users";
            this.dataGridView_Users.ReadOnly = true;
            this.dataGridView_Users.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView_Users.ShowCellErrors = false;
            this.dataGridView_Users.Size = new System.Drawing.Size(734, 480);
            this.dataGridView_Users.TabIndex = 7;
            // 
            // toolStrip_Mnu
            // 
            this.toolStrip_Mnu.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip_Mnu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel_cobby,
            this.toolStripSeparator4,
            this.toolStripText_txtFind,
            this.toolStripSeparator2,
            this.toolStripButton_FindStation,
            this.toolStripButton_Check,
            this.toolStripSeparator3,
            this.toolStripButton_Update,
            this.toolStripButton_Cancel,
            this.toolStripSeparator1,
            this.toolStripButton_Exit,
            this.toolStripLabel_Dateime});
            this.toolStrip_Mnu.Location = new System.Drawing.Point(0, 0);
            this.toolStrip_Mnu.Name = "toolStrip_Mnu";
            this.toolStrip_Mnu.Size = new System.Drawing.Size(741, 39);
            this.toolStrip_Mnu.TabIndex = 6;
            // 
            // toolStripLabel_cobby
            // 
            this.toolStripLabel_cobby.AutoSize = false;
            this.toolStripLabel_cobby.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.toolStripLabel_cobby.ForeColor = System.Drawing.SystemColors.Desktop;
            this.toolStripLabel_cobby.Items.AddRange(new object[] {
            "ตาม Van no",
            "ตาม ชื่อ ฺVAN",
            "ตาม UserName",
            "ทั้งหมด"});
            this.toolStripLabel_cobby.Name = "toolStripLabel_cobby";
            this.toolStripLabel_cobby.Size = new System.Drawing.Size(150, 21);
            this.toolStripLabel_cobby.Text = "ค้นหาข้อมูล...";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripText_txtFind
            // 
            this.toolStripText_txtFind.AutoSize = false;
            this.toolStripText_txtFind.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.toolStripText_txtFind.ForeColor = System.Drawing.SystemColors.Desktop;
            this.toolStripText_txtFind.Name = "toolStripText_txtFind";
            this.toolStripText_txtFind.Size = new System.Drawing.Size(150, 20);
            this.toolStripText_txtFind.KeyDown += new System.Windows.Forms.KeyEventHandler(this.toolStripText_txtFind_KeyDown);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripButton_FindStation
            // 
            this.toolStripButton_FindStation.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_FindStation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.toolStripButton_FindStation.Image = global::SMSMINI.Properties.Resources.Find48;
            this.toolStripButton_FindStation.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_FindStation.Name = "toolStripButton_FindStation";
            this.toolStripButton_FindStation.Size = new System.Drawing.Size(36, 36);
            this.toolStripButton_FindStation.ToolTipText = "ค้นหาข้อมูล";
            this.toolStripButton_FindStation.Click += new System.EventHandler(this.toolStripButton_FindStation_Click);
            // 
            // toolStripButton_Check
            // 
            this.toolStripButton_Check.Enabled = false;
            this.toolStripButton_Check.Image = global::SMSMINI.Properties.Resources.stockicons;
            this.toolStripButton_Check.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Check.Name = "toolStripButton_Check";
            this.toolStripButton_Check.Size = new System.Drawing.Size(87, 36);
            this.toolStripButton_Check.Text = "ตรวจสอบ";
            this.toolStripButton_Check.ToolTipText = "ตรวจสอบ ผลต่าง";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripButton_Cancel
            // 
            this.toolStripButton_Cancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.toolStripButton_Cancel.ForeColor = System.Drawing.SystemColors.Desktop;
            this.toolStripButton_Cancel.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Cancel.Image")));
            this.toolStripButton_Cancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Cancel.Name = "toolStripButton_Cancel";
            this.toolStripButton_Cancel.Size = new System.Drawing.Size(76, 36);
            this.toolStripButton_Cancel.Text = "Cancel";
            this.toolStripButton_Cancel.ToolTipText = "ยกเลิกข้อมูล";
            this.toolStripButton_Cancel.Click += new System.EventHandler(this.toolStripButton_Cancel_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripButton_Exit
            // 
            this.toolStripButton_Exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.toolStripButton_Exit.ForeColor = System.Drawing.SystemColors.Desktop;
            this.toolStripButton_Exit.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Exit.Image")));
            this.toolStripButton_Exit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Exit.Name = "toolStripButton_Exit";
            this.toolStripButton_Exit.Size = new System.Drawing.Size(60, 36);
            this.toolStripButton_Exit.Text = "Exit";
            this.toolStripButton_Exit.ToolTipText = "ออกจากหน้าจอ";
            this.toolStripButton_Exit.Click += new System.EventHandler(this.toolStripButton_Exit_Click);
            // 
            // toolStripLabel_Dateime
            // 
            this.toolStripLabel_Dateime.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripLabel_Dateime.Font = new System.Drawing.Font("Tahoma", 10F);
            this.toolStripLabel_Dateime.ForeColor = System.Drawing.SystemColors.Desktop;
            this.toolStripLabel_Dateime.Name = "toolStripLabel_Dateime";
            this.toolStripLabel_Dateime.Size = new System.Drawing.Size(0, 36);
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // dataGridView_Type
            // 
            this.dataGridView_Type.AllowUserToAddRows = false;
            this.dataGridView_Type.AllowUserToDeleteRows = false;
            this.dataGridView_Type.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_Type.Location = new System.Drawing.Point(323, 332);
            this.dataGridView_Type.Name = "dataGridView_Type";
            this.dataGridView_Type.ReadOnly = true;
            this.dataGridView_Type.Size = new System.Drawing.Size(235, 78);
            this.dataGridView_Type.TabIndex = 8;
            // 
            // dataGridView_VAN
            // 
            this.dataGridView_VAN.AllowUserToAddRows = false;
            this.dataGridView_VAN.AllowUserToDeleteRows = false;
            this.dataGridView_VAN.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_VAN.Location = new System.Drawing.Point(323, 248);
            this.dataGridView_VAN.Name = "dataGridView_VAN";
            this.dataGridView_VAN.ReadOnly = true;
            this.dataGridView_VAN.Size = new System.Drawing.Size(235, 78);
            this.dataGridView_VAN.TabIndex = 9;
            // 
            // dataGridView_Users_Level
            // 
            this.dataGridView_Users_Level.AllowUserToAddRows = false;
            this.dataGridView_Users_Level.AllowUserToDeleteRows = false;
            this.dataGridView_Users_Level.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_Users_Level.Location = new System.Drawing.Point(323, 80);
            this.dataGridView_Users_Level.Name = "dataGridView_Users_Level";
            this.dataGridView_Users_Level.ReadOnly = true;
            this.dataGridView_Users_Level.Size = new System.Drawing.Size(235, 78);
            this.dataGridView_Users_Level.TabIndex = 10;
            // 
            // dataGridView_Employees
            // 
            this.dataGridView_Employees.AllowUserToAddRows = false;
            this.dataGridView_Employees.AllowUserToDeleteRows = false;
            this.dataGridView_Employees.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_Employees.Location = new System.Drawing.Point(323, 164);
            this.dataGridView_Employees.Name = "dataGridView_Employees";
            this.dataGridView_Employees.ReadOnly = true;
            this.dataGridView_Employees.Size = new System.Drawing.Size(235, 78);
            this.dataGridView_Employees.TabIndex = 11;
            // 
            // toolStripButton_Update
            // 
            this.toolStripButton_Update.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.toolStripButton_Update.ForeColor = System.Drawing.SystemColors.Desktop;
            this.toolStripButton_Update.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Update.Image")));
            this.toolStripButton_Update.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Update.Name = "toolStripButton_Update";
            this.toolStripButton_Update.Size = new System.Drawing.Size(78, 36);
            this.toolStripButton_Update.Text = "Update";
            this.toolStripButton_Update.ToolTipText = "Update VAN";
            this.toolStripButton_Update.Click += new System.EventHandler(this.toolStripButton_Update_Click);
            // 
            // Van
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(741, 526);
            this.Controls.Add(this.dataGridView_Users);
            this.Controls.Add(this.dataGridView_Employees);
            this.Controls.Add(this.dataGridView_Users_Level);
            this.Controls.Add(this.dataGridView_VAN);
            this.Controls.Add(this.dataGridView_Type);
            this.Controls.Add(this.toolStrip_Mnu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Van";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "เจ้าหน้าที่ (Van)";
            this.Load += new System.EventHandler(this.Van_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Van_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Users)).EndInit();
            this.toolStrip_Mnu.ResumeLayout(false);
            this.toolStrip_Mnu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Type)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_VAN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Users_Level)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Employees)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView_Users;
        private System.Windows.Forms.ToolStrip toolStrip_Mnu;
        private System.Windows.Forms.ToolStripComboBox toolStripLabel_cobby;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripTextBox toolStripText_txtFind;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripButton_FindStation;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton toolStripButton_Cancel;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButton_Exit;
        private System.Windows.Forms.ToolStripLabel toolStripLabel_Dateime;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.DataGridView dataGridView_Type;
        private System.Windows.Forms.DataGridView dataGridView_VAN;
        private System.Windows.Forms.DataGridView dataGridView_Users_Level;
        private System.Windows.Forms.DataGridView dataGridView_Employees;
        private System.Windows.Forms.ToolStripButton toolStripButton_Check;
        private System.Windows.Forms.ToolStripButton toolStripButton_Update;
    }
}