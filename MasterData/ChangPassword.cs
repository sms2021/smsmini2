﻿using System;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Windows.Forms;


namespace SMSMINI.MasterData
{
    public partial class ChangPassword : Form
    {
#pragma warning disable CS0246 // The type or namespace name 'Encryption' could not be found (are you missing a using directive or an assembly reference?)
        Encryption.Encryption dCryp = new Encryption.Encryption();
#pragma warning restore CS0246 // The type or namespace name 'Encryption' could not be found (are you missing a using directive or an assembly reference?)

        //string strConn = "";
        //string oldPass = "";
        private ProgressBar.FormProgress m_fmProgress = null;

        public ChangPassword()
        {
            InitializeComponent();

            object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
            if (attributes.Length == 0)
            {
                this.Text = "SMS MINI [v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "]";

            }
            this.Text = ((AssemblyProductAttribute)attributes[0]).Product + " [v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "]";

        }


        private void ChangPassword_FormClosed(object sender, FormClosedEventArgs e)
        {
            DialogResult = DialogResult.Abort;
        }

        private void ChangPassword_Load(object sender, EventArgs e)
        {
            clearText();
            txtFullName.Text = UserInfo.FullName;
            txtUserName.Text = UserInfo.UserName;
        }

        private void clearText()
        {
            txtPassword1.Text = string.Empty;
            txtPassword2.Text = string.Empty;
            txtPassword3.Text = string.Empty;

            txtPassword1.Focus();
        }


        private void button1_Click(object sender, EventArgs e)
        {
            bool upPass = false;
            if (UserInfo.Password != txtPassword1.Text.Trim())
            {
                MessageBox.Show("ป้อน Password ไม่ถูกต้อง กรุณาป้อนใหม่...", "ผลการตรวจสอบ");
                txtPassword1.Focus();
                return;
            }

            if (txtPassword2.Text.Trim() != txtPassword3.Text.Trim())
            {
                MessageBox.Show("ป้อน Password ไม่ตรงกั กรุณาป้อนใหม่...", "ผลการตรวจสอบ");
                txtPassword3.Focus();
                return;
            }

            if (MessageBox.Show("คุณต้องการเปลี่ยน Password ใช่หรือไม่", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                newProgressbar();


                try
                {
                    invoke_Progress("เริ่มตรวจสอบและเข้ารหัส Password");
                    string _Password = txtPassword2.Text.Trim();
                    string _User = UserInfo.UserName;
                    string key = SMSMINI.Properties.Settings.Default["CryptionKey"].ToString(); //ConfigurationManager.AppSettings["CryptionKey"].ToString();
                    string xpass = dCryp.Encrypt(_Password, key);

                    string emEmail = "";
                    string FName = "";

                    invoke_Progress("กำลังเปลี่ยน Password...");

                    using (DAL.SMSManage.SMSManageDataContext db = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
                    {
                        if ((from t in db.Users where t.UserName.Trim() == _User.Trim() select t).Count() > 0)
                        {
                            var emp = (from us in db.Users
                                       from em in db.Employees
                                       where us.EMP_ID == em.EMP_ID &&
                                       us.UserName == _User
                                       select em)
                                       .FirstOrDefault();
                            if (emp != null)
                            {
                                emEmail = emp.EMail;
                                FName = emp.FName;

                                var u = (from t in db.Users where t.UserName.Trim() == _User.Trim() select t)
                                    .FirstOrDefault();
                                if (u != null)
                                {
                                    u.Password = xpass;
                                    db.SubmitChanges();
                                    upPass = true;
                                }
                                else
                                    upPass = false;
                            }
                        }

                    }

                    if (upPass)
                    {

                        using (DAL.SMSManage.SMSManageDataContext db = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
                        {
                            if ((from t in db.Users where t.UserName.Trim() == _User.Trim() select t).Count() > 0)
                            {
                                var u = (from t in db.Users where t.UserName.Trim() == _User.Trim() select t)
                                    .FirstOrDefault();
                                if (u != null)
                                {
                                    u.Password = xpass;
                                    db.SubmitChanges();
                                }
                            }

                        }

                        try
                        {
                            //invoke_Progress("Auto Send mail แจ้งการเปลี่ยน Password");
                            //smsServices.IsmsServiceClient svc = new SMSMINI.smsServices.IsmsServiceClient();

                            //svc.ClientCredentials.Windows.ClientCredential.UserName = "smswcf";
                            //svc.ClientCredentials.Windows.ClientCredential.Password = "smswcf";

                            //string mess = svc.autoSendMail(UserInfo.Email, UserInfo.UserName, txtPassword3.Text.Trim());

                            using (DAL.SMSManage.SMSManageDataContext db = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
                            {
                                if ((from t in db.Users where t.UserName.Trim() == _User.Trim() select t).Count() > 0)
                                {
                                    var u = (from t in db.Users where t.UserName.Trim() == _User.Trim() select t)
                                        .FirstOrDefault();

                                    if (u != null)
                                    {
                                        u.Password = xpass;
                                        db.SubmitChanges();
                                    }
                                }

                            }

                            MailMessage mail = new MailMessage();
                            SmtpClient mailClient = new SmtpClient("mail.flowco.co.th");

                            mail.From = new MailAddress("SMSMileageStatement@flowco.co.th", "SMS Automail");
                            //mail.From = new MailAddress("ATGMonitoringOffice@flowco.co.th", "AMO Automail");

                            mail.To.Add(emEmail);
                            mail.Subject = "Change Password";
                            mail.Body = "<h2>เรียนคุณ " + FName + "</h2>" +
                                "<h2>Change Password</h2>User:=" + _User + "<BR/>Password:=" + _Password;
                            mail.IsBodyHtml = true;

                            mailClient.Port = 25;
                            //mailClient.Credentials = new System.Net.NetworkCredential("ATGMonitoringOffice@flowco.co.th", "AMO@2013");
                            mailClient.Credentials = new System.Net.NetworkCredential("SMSMileageStatement@flowco.co.th", "SMS@2013");
                            mailClient.EnableSsl = true;

                            ServicePointManager.ServerCertificateValidationCallback =
                                delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
                                { return true; };

                            mailClient.Send(mail);


                            closeProgress();

                            MessageBox.Show("เปลี่ยน Password เรียบร้อย" + System.Environment.NewLine +
                                "กรุณาเช็ค Password ใน mail: " + emEmail, "ผลการทำงาน",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);

                            clearText();

                        }
                        catch (System.Exception ex)
                        {
                            closeProgress();
                            MessageBox.Show(ex.Message, "ผลการทำงาน");
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    closeProgress();
                    MessageBox.Show(ex.Message, "ผลการทำงาน");
                }

            }
        }

        #region Progressbar

        private void newProgressbar()
        {
            m_fmProgress = new ProgressBar.FormProgress();
            backgroundWorker1.RunWorkerAsync();
            System.Threading.Thread.Sleep(3000);
        }

        private void invoke_Progress(string strMsg)
        {
            try
            {
                m_fmProgress.lblDescription.Invoke(
                   (MethodInvoker)delegate ()
                   {
                       m_fmProgress.lblDescription.Text = strMsg;
                   }
                   );
            }
            catch (System.Exception)
            {
            }
        }

        private void closeProgress()
        {
            if (m_fmProgress != null)
            {
                m_fmProgress.Invoke(
                    (MethodInvoker)delegate ()
                    {
                        m_fmProgress.Hide();
                        m_fmProgress.Dispose();
                        m_fmProgress = null;
                    }
                );
            }
        }

        #endregion

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            m_fmProgress.Activate();
            m_fmProgress.BringToFront();
            m_fmProgress.TopMost = true;
            m_fmProgress.ShowDialog();
        }


    }
}
