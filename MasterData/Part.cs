﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace SMSMINI.MasterData
{
    public partial class Part : Form
    {
        string strConn = "";
        private ProgressBar.FormProgress m_fmProgress = null;

        List<string> listPartID;
        public Part()
        {
            InitializeComponent();

            object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
            if (attributes.Length == 0)
            {
                this.Text = "SMS MINI [v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "]";

            }
            this.Text = ((AssemblyProductAttribute)attributes[0]).Product + " [v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "]";


        }

        private void Part_Load(object sender, EventArgs e)
        {

        }


        #region Progressbar

        private void newProgressbar()
        {
            m_fmProgress = new ProgressBar.FormProgress();
            backgroundWorker1.RunWorkerAsync();
            System.Threading.Thread.Sleep(3000);
        }

        private void invoke_Progress(string strMsg)
        {
            try
            {
                m_fmProgress.lblDescription.Invoke(
                   (MethodInvoker)delegate ()
                   {
                       m_fmProgress.lblDescription.Text = strMsg;
                   }
                   );
            }
            catch (System.Exception)
            { }
        }

        private void closeProgress()
        {
            if (m_fmProgress != null)
            {
                m_fmProgress.Invoke(
                    (MethodInvoker)delegate ()
                    {
                        m_fmProgress.Hide();
                        m_fmProgress.Dispose();
                        m_fmProgress = null;
                    }
                );
            }
        }

        #endregion


        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            m_fmProgress.Activate();
            m_fmProgress.BringToFront();
            m_fmProgress.TopMost = true;
            m_fmProgress.ShowDialog();
        }

        private void Part_FormClosed(object sender, FormClosedEventArgs e)
        {
            DialogResult = DialogResult.Abort;
        }

        private void toolStripButton_FindStation_Click(object sender, EventArgs e)
        {
            if (toolStripText_txtFind.Text != string.Empty)
            {
                //strConn = ConnectionManager.GetConnectionString("1");
                //using (DAL.SMSManage.SMSManageDataContext db = new DAL.SMSManage.SMSManageDataContext(strConn))
                using (DAL.SMSManage.SMSManageDataContext db = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
                {
                    ShowPartData(db);
                }
            }
            else
            {
                toolStripText_txtFind.Focus();
                return;
            }
        }

        private void ShowPartData(DAL.SMSManage.SMSManageDataContext db)
        {
            newProgressbar();
            invoke_Progress("ตรวจสอบ ข้อมูล อะไหล่");

            try
            {
                if (toolStripLabel_cobby.Text.Trim() == "ตาม Part no")
                {
                    if ((from t in db.SpareParts where t.SPA_ID.Contains(toolStripText_txtFind.Text.Trim()) select t).Count() > 0)
                    {
                        var q = from t in db.SpareParts where t.SPA_ID.Contains(toolStripText_txtFind.Text.Trim()) select t;
                        dataGridView_Part.DataSource = q.ToList();
                        dataGridView_Part.Refresh();
                    }
                    else
                    {
                        dataGridView_Part.DataSource = null;
                        dataGridView_Part.Refresh();

                        closeProgress();
                        MessageBox.Show("ไม่มีข้อมูล อะไหล่ ตามที่ต้องการกรุณาระบุใหม่", "กรุณาติดต่อผู้ดูแลระบบ...", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else if (toolStripLabel_cobby.Text.Trim() == "ตาม Part name")
                {
                    if ((from t in db.SpareParts where t.SparePart1.Contains(toolStripText_txtFind.Text.Trim()) select t).Count() > 0)
                    {
                        var q = from t in db.SpareParts where t.SparePart1.Contains(toolStripText_txtFind.Text.Trim()) select t;
                        dataGridView_Part.DataSource = q.ToList();
                        dataGridView_Part.Refresh();
                    }
                    else
                    {
                        dataGridView_Part.DataSource = null;
                        dataGridView_Part.Refresh();
                        closeProgress();
                        MessageBox.Show("ไม่มีข้อมูล อะไหล่ ตามที่ต้องการกรุณาระบุใหม่", "กรุณาติดต่อผู้ดูแลระบบ...", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                }
                else if (toolStripLabel_cobby.Text.Trim() == "ทั้งหมด")
                {
                    if ((from t in db.SpareParts select t).Count() > 0)
                    {
                        var q = from t in db.SpareParts select t;
                        dataGridView_Part.DataSource = q.ToList();
                        dataGridView_Part.Refresh();
                    }
                    else
                    {
                        dataGridView_Part.DataSource = null;
                        dataGridView_Part.Refresh();

                        closeProgress();
                        MessageBox.Show("ไม่มีข้อมูล อะไหล่ ตามที่ต้องการกรุณาระบุใหม่", "กรุณาติดต่อผู้ดูแลระบบ...", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                }
                closeProgress();
            }
            catch (System.Exception ex)
            {
                closeProgress();

                MessageBox.Show("เกิดข้อผิดพลาด Error: ในการตรวจสอบข้อมูล อะไหล่ " + Environment.NewLine +
                    "Error: " + ex.Message, "กรุณาติดต่อผู้ดูแลระบบ...",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void toolStripButton_Update_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("คุณต้องการ Update ข้อมูล Part ใช่หรือไม่?...", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                newProgressbar();
                invoke_Progress("เริ่มปรับปรุง ข้อมูล อะไหล่");

                //strConn = ConnectionManager.GetConnectionString("0");
                string partNo = "";
                using (DAL.SMSManage.SMSManageDataContext db = new SMSMINI.DAL.SMSManage.SMSManageDataContext()) //DAL.SMSManage.SMSManageDataContext(strConn))
                {
                    db.Connection.Open();
                    db.Transaction = db.Connection.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted);

                    try
                    {
                        for (int i = 0; i <= dataGridView_Part.RowCount - 1; i++)
                        {
                            partNo = dataGridView_Part["SPA_ID", i].Value == null ? "" : dataGridView_Part["SPA_ID", i].Value.ToString().Trim();

                            if ((from tmp in db.SpareParts where tmp.SPA_ID.Trim() == partNo.Trim() select tmp).Count() > 0)
                            {
                                //==============================================
                                //Update SpareParts
                                //==============================================
                                var p = (from t in db.SpareParts where t.SPA_ID.Trim() == partNo.Trim() select t).FirstOrDefault();
                                p.SUP_ID = dataGridView_Part["SUP_ID", i].Value == null ? "" : dataGridView_Part["SUP_ID", i].Value.ToString().Trim();
                                p.Styp_id = dataGridView_Part["Styp_id", i].Value == null ? "" : dataGridView_Part["Styp_id", i].Value.ToString().Trim();
                                //p.BRA_ID = 0;
                                p.SparePart1 = dataGridView_Part["SparePart1", i].Value == null ? "" : dataGridView_Part["SparePart1", i].Value.ToString().Trim();

                                p.PricePerUnit = dataGridView_Part["PricePerUnit", i].Value == null ? 0 : Convert.ToDecimal(dataGridView_Part["PricePerUnit", i].Value);
                                p.Cost = dataGridView_Part["Cost", i].Value == null ? 0 : Convert.ToDecimal(dataGridView_Part["Cost", i].Value);
                                p.TranType = dataGridView_Part["TranType", i].Value == null ? "" : dataGridView_Part["TranType", i].Value.ToString().Trim();
                                p.WH = dataGridView_Part["WH", i].Value == null ? "" : dataGridView_Part["WH", i].Value.ToString().Trim();

                                db.SubmitChanges();
                            }

                            else
                            {
                                //==============================================
                                //Insert SpareParts
                                //==============================================
                                DAL.SMSManage.SparePart sp = new DAL.SMSManage.SparePart()
                                {
                                    SPA_ID = partNo,
                                    SUP_ID = dataGridView_Part["SUP_ID", i].Value == null ? "" : dataGridView_Part["SUP_ID", i].Value.ToString().Trim(),
                                    Styp_id = dataGridView_Part["Styp_id", i].Value == null ? "" : dataGridView_Part["Styp_id", i].Value.ToString().Trim(),
                                    //BRA_ID = 0,//dataGridView_Part["BRA_ID", i].Value == null ? "" : dataGridView_Part["BRA_ID", i].Value.ToString().Trim(),
                                    SparePart1 = dataGridView_Part["SparePart1", i].Value == null ? "" : dataGridView_Part["SparePart1", i].Value.ToString().Trim(),

                                    PricePerUnit = dataGridView_Part["PricePerUnit", i].Value == null ? 0 : Convert.ToDecimal(dataGridView_Part["PricePerUnit", i].Value),
                                    Cost = dataGridView_Part["Cost", i].Value == null ? 0 : Convert.ToDecimal(dataGridView_Part["Cost", i].Value),
                                    TranType = dataGridView_Part["TranType", i].Value == null ? "" : dataGridView_Part["TranType", i].Value.ToString().Trim(),
                                    WH = dataGridView_Part["WH", i].Value == null ? "" : dataGridView_Part["WH", i].Value.ToString().Trim()

                                };

                                db.SpareParts.InsertOnSubmit(sp);
                                db.SubmitChanges();
                            }
                        }

                        db.Transaction.Commit();

                        dataGridView_Part.DataSource = null;
                        dataGridView_Part.Refresh();
                        closeProgress();

                        MessageBox.Show("ปรับปรุงข้อมูล อะไหล่ เรียบร้อย", "ผลการทำงาน", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (System.Exception ex)
                    {
                        closeProgress();
                        db.Transaction.Rollback();
                        MessageBox.Show("เกิดข้อผิดพลาด Error: ในการ Update ข้อมูล Part  Error: " + ex.Message, " กรุณาติดต่อผู้ดูแลระบบ...",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }

        private void toolStripButton_Cancel_Click(object sender, EventArgs e)
        {

        }

        private void toolStripButton_Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void toolStripButton_Check_Click(object sender, EventArgs e)
        {
            newProgressbar();
            invoke_Progress("กำลังโหลดข้อมูล อะไหล่ บนเครื่อง VAN (Client)");

            try
            {
                listPartID = new List<string>();

                // strConn = ConnectionManager.GetConnectionString("0");//Offline
                using (DAL.SMSManage.SMSManageDataContext db = new SMSMINI.DAL.SMSManage.SMSManageDataContext()) //DAL.SMSManage.SMSManageDataContext(strConn))
                {
                    var sp = from t in db.SpareParts select t.SPA_ID;
                    foreach (var p in sp)
                        listPartID.Add(p);
                }

                if (listPartID.Count() > 0)
                {
                    invoke_Progress("กำลังตรวจสอบข้อข้อมูล อะไหล่ี ระหว่างเครื่อง VAN และ SMS Server");
                    strConn = Properties.Settings.Default["ServicesMSDBConnectionString"].ToString();//ConnectionManager.GetConnectionString("1");//Online


                    string sql = "select *  from SpareParts where SPA_ID not in(@SPA_ID)";//  (" + SQLArrayToInString(listPartID.ToArray()) + ") ";

                    DataTable dt = new DataTable();
                    DataSet ds = new DataSet();
                    using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(strConn))
                    {
                        if (conn.State == ConnectionState.Closed) conn.Open();
                        using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand())
                        {
                            cmd.Connection = conn;
                            cmd.Parameters.Clear();
                            cmd.CommandText = sql;
                            cmd.CommandTimeout = 60000;
                            cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@SPA_ID", SQLArrayToInString(listPartID.ToArray())));


                            System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter();
                            da.SelectCommand = cmd;

                            da.Fill(ds);
                            dataGridView_Part.DataSource = ds;
                            dataGridView_Part.Refresh();
                        }
                    }

                }

                closeProgress();
            }
            catch (System.Exception ex)
            {

                closeProgress();
                MessageBox.Show("เกิดข้อผิดพลาด Error: ในการตรวจสอบข้อมูล อะไหล่" + Environment.NewLine +
                    "Error: " + ex.Message, "กรุณาติดต่อผู้ดูแลระบบ...",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private string SQLArrayToInString(Array a)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i <= a.GetUpperBound(0); i++)
                sb.AppendFormat("'{0}',", a.GetValue(i).ToString().Replace("\"", "").Replace("\'", ""));
            string retVal = sb.ToString();
            return retVal.Substring(0, retVal.Length - 1);
        }

    }
}
