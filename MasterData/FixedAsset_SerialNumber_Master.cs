﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace SMSMINI.MasterData
{
    public partial class FixedAsset_SerialNumber_Master : Form
    {
        List<DAL.SMSManage.FFixedAsset_Model> ffModel = null;
        List<DAL.SMSManage.FFixedAsset_Brand> ffBrand = null;
        List<DAL.SMSManage.FFixedAsset_Category> ffCategory = null;
        List<DAL.SMSManage.FFixedAsset_SerialNumber_Pool> ffSerialNumberPool = null;

        public string FFSN { get; set; }

        public FixedAsset_SerialNumber_Master()
        {
            InitializeComponent();
        }

        private void FixedAsset_SerialNumber_Master_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(FFSN))
            {
                toolStripText_txtFind.Text = FFSN;
                toolStripButton_FindStation_Click(null, null);
            }

        }

        private void toolStripButton_Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void toolStripButton_FindStation_Click(object sender, EventArgs e)
        {
            ffModel = new List<SMSMINI.DAL.SMSManage.FFixedAsset_Model>();
            ffBrand = new List<SMSMINI.DAL.SMSManage.FFixedAsset_Brand>();
            ffCategory = new List<SMSMINI.DAL.SMSManage.FFixedAsset_Category>();

            ffSerialNumberPool = new List<SMSMINI.DAL.SMSManage.FFixedAsset_SerialNumber_Pool>();

            string SN = toolStripText_txtFind.Text.Trim();

            //Remote SMS Server
            string strConn = SMSMINI.Properties.Settings.Default["ServicesMSDBConnectionString"].ToString();
            using (var dc = new DAL.SMSManage.SMSManageDataContext(strConn))
            {
                try
                {

                    #region Load FFixedAsset_SerialNumber_Pools

                    var _snPool = dc.FFixedAsset_SerialNumber_Pools
                        .Where(t =>
                                (new string[] { "00", "66", "76" }.Contains(t.StatusUse) &&
                                (t.SerialNumber.Contains(SN))
                              ))
                              .Distinct()
                              .ToList();

                    if (_snPool.Count() > 0)
                    {
                        dataGridView1.DataSource = _snPool.ToList();
                    }
                    else
                    {
                        MessageBox.Show("ไม่มีข้อมูล กรุณาป้อนเงื่อนไขใหม่...", "ผลการค้นหา", MessageBoxButtons.OK);
                    }

                    foreach (var item1 in _snPool)
                    {
                        ffSerialNumberPool.Add(new DAL.SMSManage.FFixedAsset_SerialNumber_Pool
                        {
                            SerialNumber = item1.SerialNumber,
                            SerialNumberBarcode = item1.SerialNumberBarcode,
                            ControlFixAssetNo = item1.ControlFixAssetNo,
                            FCAT_ID = item1.FCAT_ID,
                            FMD_ID = item1.FMD_ID,

                            FixedAssetName = item1.FixedAssetName,
                            EMP_ID = item1.EMP_ID,
                            RegisDate = item1.RegisDate,
                            StatusUse = item1.StatusUse,
                            Detail = item1.Detail,

                            InstallDate = item1.InstallDate,
                            ExpiryDate = item1.ExpiryDate,
                            SupExpiryDate = item1.SupExpiryDate,
                            WADDate = item1.WADDate,

                            PDIDate = item1.PDIDate
                        });
                    }



                    #endregion

                    //Load SN Master data
                    #region Load FFixedAsset_Station_Detail

                    //1.
                    //Load FFixedAsset_Station_Detail

                    if (ffSerialNumberPool.Count() > 0)
                    {

                        foreach (var item0 in ffSerialNumberPool)
                        {
                            #region Load FFixedAsset_Brands
                            //4.FFixedAsset_Brand             

                            var _brand = dc.FFixedAsset_Brands.ToList();

                            if (_brand.Count() > 0)
                            {
                                foreach (var item1 in _brand)
                                {
                                    ffBrand.Add(new DAL.SMSManage.FFixedAsset_Brand
                                    {
                                        FBA_ID = item1.FBA_ID,
                                        FBrand = item1.FBrand,
                                        FBDetail = item1.FBDetail

                                    });
                                }
                            }
                            #endregion

                            #region Load FFixedAsset_Models
                            //5.FFixedAsset_Model
                            var _model = dc.FFixedAsset_Models
                                .Where(t => t.FMD_ID == item0.FMD_ID ||
                                            t.FMD_ID == 999 ||
                                            (_snPool.Select(o => o.FMD_ID).Contains(t.FMD_ID))
                                            ).ToList();
                            if (_model.Count() > 0)
                            {
                                foreach (var item1 in _model)
                                {
                                    ffModel.Add(new DAL.SMSManage.FFixedAsset_Model
                                    {
                                        FMD_ID = item1.FMD_ID,
                                        FBA_ID = item1.FBA_ID,
                                        FModel = item1.FModel,
                                        FMDetail = item1.FMDetail,
                                        Photo = item1.Photo
                                    });
                                }
                            }

                            #endregion


                            #region Load FFixedAsset_Category
                            //5.FFixedAsset_Category
                            var _cate = dc.FFixedAsset_Categories
                                          .Where(t => t.FCAT_ID == item0.FCAT_ID ||
                                            t.FCAT_ID == 99)
                                            .ToList();
                            if (_cate.Count() > 0)
                            {
                                foreach (var item1 in _cate)
                                {
                                    ffCategory.Add(new DAL.SMSManage.FFixedAsset_Category
                                    {
                                        FCAT_ID = item1.FCAT_ID,
                                        FixedAsset_CateName = item1.FixedAsset_CateName,
                                        FCATNumber = item1.FCATNumber,
                                        FCatDetail = item1.FCatDetail
                                    });
                                }
                            }

                            #endregion

                        }

                    }
                    #endregion


                }
                catch (Exception)
                {
                    throw;
                }
            }

        }


        public void toolStripButton_Update_Click(object sender, EventArgs e)
        {
            if (ffSerialNumberPool.Count() <= 0)
                return;

            if (MessageBox.Show("คุณต้องการ Update ข้อมูล Fixed Asset SerialNumber Master ใช่หรือไม่?...", "คำยืนยัน",
                MessageBoxButtons.YesNo) == DialogResult.No)
            {
                return;
            }

            string strConn = SMSMINI.Properties.Settings.Default["ServicesMSDBConnectionString"].ToString();
            using (var dc = new DAL.SMSManage.SMSManageDataContext(strConn))
            {
                try
                {
                    dc.Connection.Open();
                    dc.Transaction = dc.Connection.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted);

                    //1. FFixedAsset_Category
                    #region FFixedAsset_Category

                    if (ffCategory.Count() > 0)
                    {
                        foreach (var item in ffCategory)
                        {
                            var _cate = dc.FFixedAsset_Categories
                                .Where(t => t.FCAT_ID == item.FCAT_ID).FirstOrDefault();
                            if (_cate != null)
                            {
                                _cate.FCatDetail = item.FCatDetail;
                                _cate.FixedAsset_CateName = item.FixedAsset_CateName;

                                dc.SubmitChanges();
                            }
                            else
                            {
                                var newCate = new DAL.SMSManage.FFixedAsset_Category()
                                {
                                    FCAT_ID = item.FCAT_ID,
                                    FCatDetail = item.FCatDetail,
                                    FixedAsset_CateName = item.FixedAsset_CateName
                                };

                                dc.FFixedAsset_Categories.InsertOnSubmit(newCate);
                                dc.SubmitChanges();
                            }
                        }


                    }
                    #endregion

                    //2. FFixedAsset_Brand
                    #region FFixedAsset_Brand

                    if (ffBrand.Count() > 0)
                    {
                        foreach (var item in ffBrand)
                        {
                            var _brand = dc.FFixedAsset_Brands
                                .Where(t => t.FBA_ID == item.FBA_ID).FirstOrDefault();
                            if (_brand != null)
                            {
                                //_brand.FCAT_ID = item.FCAT_ID;
                                _brand.FBrand = item.FBrand;
                                _brand.FBDetail = item.FBDetail;
                                dc.SubmitChanges();
                            }
                            else
                            {
                                var newBrand = new DAL.SMSManage.FFixedAsset_Brand()
                                {
                                    FBA_ID = item.FBA_ID,
                                    //FCAT_ID = item.FCAT_ID,
                                    FBrand = item.FBrand,
                                    FBDetail = item.FBDetail
                                };


                                dc.FFixedAsset_Brands.InsertOnSubmit(newBrand);
                                dc.SubmitChanges();
                            }
                        }

                    }
                    #endregion

                    //3. FFixedAsset_Models
                    #region FFixedAsset_Model

                    if (ffModel.Count() > 0)
                    {
                        foreach (var item in ffModel)
                        {
                            var _mdel = dc.FFixedAsset_Models
                                .Where(t => t.FMD_ID == item.FMD_ID).FirstOrDefault();
                            if (_mdel != null)
                            {
                                _mdel.FBA_ID = item.FBA_ID;
                                _mdel.FModel = item.FModel;
                                _mdel.FMDetail = item.FMDetail;
                                _mdel.Photo = item.Photo;

                                dc.SubmitChanges();
                            }
                            else
                            {
                                var newMdel = new DAL.SMSManage.FFixedAsset_Model()
                                {
                                    FMD_ID = item.FMD_ID,
                                    FBA_ID = item.FBA_ID,
                                    FModel = item.FModel,
                                    FMDetail = item.FMDetail,
                                    Photo = item.Photo
                                };

                                dc.FFixedAsset_Models.InsertOnSubmit(newMdel);
                                dc.SubmitChanges();
                            }
                        }

                        //
                    }

                    #endregion                    

                    //5. FFixedAsset_SerialNumber_Pools
                    #region FFixedAsset_SerialNumber_Pool
                    //Clear FFixedAsset_Station_Detail,
                    //FFixedAsset_SerialNumber_Pool
                    //dc.ExecuteCommand("Truncate table FFixedAsset_Station_Detail;");
                    //dc.ExecuteCommand("Delete FFixedAsset_SerialNumber_Pool;");

                    if (ffSerialNumberPool.Count() > 0)
                    {
                        foreach (var item in ffSerialNumberPool)
                        {
                            var _snPool = dc.FFixedAsset_SerialNumber_Pools
                                .Where(t => t.SerialNumber == item.SerialNumber)
                                .FirstOrDefault();

                            if (_snPool != null)
                            {
                                _snPool.SerialNumberBarcode = item.SerialNumberBarcode;
                                _snPool.FCAT_ID = item.FCAT_ID;
                                _snPool.FMD_ID = item.FMD_ID;
                                _snPool.FixedAssetName = item.FixedAssetName;
                                _snPool.EMP_ID = item.EMP_ID;
                                _snPool.RegisDate = item.RegisDate;
                                _snPool.StatusUse = item.StatusUse;
                                _snPool.Detail = item.Detail;
                                _snPool.ExpiryDate = item.ExpiryDate;
                                _snPool.InstallDate = item.InstallDate;

                                _snPool.SupExpiryDate = item.SupExpiryDate;

                                dc.SubmitChanges();
                            }
                            else
                            {

                                var newsnPool = new DAL.SMSManage.FFixedAsset_SerialNumber_Pool()
                                {
                                    SerialNumber = item.SerialNumber,
                                    SerialNumberBarcode = item.SerialNumberBarcode,
                                    FCAT_ID = item.FCAT_ID,
                                    FMD_ID = item.FMD_ID,
                                    FixedAssetName = item.FixedAssetName,
                                    EMP_ID = item.EMP_ID,
                                    RegisDate = item.RegisDate,
                                    StatusUse = item.StatusUse,
                                    Detail = item.Detail,
                                    ExpiryDate = item.ExpiryDate,
                                    InstallDate = item.InstallDate,

                                    SupExpiryDate = item.InstallDate

                                };

                                dc.FFixedAsset_SerialNumber_Pools.InsertOnSubmit(newsnPool);
                                dc.SubmitChanges();
                            }

                        }
                    }
                    #endregion


                    dc.Transaction.Commit();


                    MessageBox.Show("Update FixedAsset เรียบร้อย...", "ผลการทำงาน",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);

                    DialogResult = DialogResult.OK;
                }
                catch (Exception ex)
                {
                    dc.Transaction.Rollback();
                    MessageBox.Show(ex.Message);
                }

            }

        }


    }
}
