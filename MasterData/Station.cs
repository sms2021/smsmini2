﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace SMSMINI.MasterData
{
    public partial class Station : Form
    {
        string strConn = "";
        private ProgressBar.FormProgress m_fmProgress = null;

        List<string> listJobID;

        public Station()
        {
            InitializeComponent();
            //this.Text = this.Text + " [v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "]";
            object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
            if (attributes.Length == 0)
            {
                this.Text = "SMS MINI [v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "]";

            }
            this.Text = ((AssemblyProductAttribute)attributes[0]).Product + " [v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "]";


        }

        private void Station_Load(object sender, EventArgs e)
        {
            toolStripButton_FindStation.Enabled = true;
            toolStripButton_Check.Enabled = true;
            toolStripButton_Update.Enabled = false;
            toolStripButton_Cancel.Enabled = true;
        }


        #region Progressbar

        private void newProgressbar()
        {
            m_fmProgress = new ProgressBar.FormProgress();
            backgroundWorker1.RunWorkerAsync();
            System.Threading.Thread.Sleep(3000);
        }

        private void invoke_Progress(string strMsg)
        {
            try
            {
                m_fmProgress.lblDescription.Invoke(
                   (MethodInvoker)delegate ()
                   {
                       m_fmProgress.lblDescription.Text = strMsg;
                   }
                   );
            }
            catch (System.Exception)
            { }
        }

        private void closeProgress()
        {
            if (m_fmProgress != null)
            {
                m_fmProgress.Invoke(
                    (MethodInvoker)delegate ()
                    {
                        m_fmProgress.Hide();
                        m_fmProgress.Dispose();
                        m_fmProgress = null;
                    }
                );
            }
        }

        #endregion


        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            m_fmProgress.Activate();
            m_fmProgress.BringToFront();
            m_fmProgress.TopMost = true;
            m_fmProgress.ShowDialog();
        }

        private void Station_FormClosed(object sender, FormClosedEventArgs e)
        {
            DialogResult = DialogResult.Abort;
        }

        private void toolStripButton_FindStation_Click(object sender, EventArgs e)
        {
            if (toolStripText_txtFind.Text != string.Empty)
            {
                searchData();
                toolStripButton_FindStation.Enabled = false;
                toolStripButton_Check.Enabled = false;
                toolStripButton_Update.Enabled = true;
                toolStripButton_Cancel.Enabled = true;
            }
            else
            {
                toolStripText_txtFind.Focus();
                return;
            }


        }

        private void searchData()
        {
            //strConn = ConnectionManager.GetConnectionString("1");// On line only
            newProgressbar();
            invoke_Progress("ตรวจสอบ ข้อมูล สถานี");

            try
            {
                using (DAL.SMSManage.SMSManageDataContext db = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
                {
                    switch (toolStripLabel_cobby.Text.Trim())
                    {
                        case "ตาม รหัสสถานี":
                            dataGridView_Station.DataSource = (from t in db.Stations
                                                               where t.STA_ID1.Contains(toolStripText_txtFind.Text.Trim())
                                                               select new
                                                               {
                                                                   t.STA_ID,
                                                                   t.STA_ID1,
                                                                   t.StationSys,
                                                                   t.StationRegis,
                                                                   t.GOP_ID,
                                                                   t.Address,
                                                                   t.FaxNo,
                                                                   t.TelNo,
                                                                   t.SUB_ZONE,
                                                                   t.PRO_ID,
                                                                   t.IsContract
                                                               }).Distinct().ToList();
                            dataGridView_Station.Refresh();

                            break;

                        case "ตาม ชื่อสถานี":
                            dataGridView_Station.DataSource = (from t in db.Stations
                                                               where t.StationSys.Contains(toolStripText_txtFind.Text.Trim())
                                                               select new
                                                               {
                                                                   t.STA_ID,
                                                                   t.STA_ID1,
                                                                   t.StationSys,
                                                                   t.StationRegis,
                                                                   t.GOP_ID,
                                                                   t.Address,
                                                                   t.FaxNo,
                                                                   t.TelNo,
                                                                   t.SUB_ZONE,
                                                                   t.PRO_ID,
                                                                   t.IsContract
                                                               }).Distinct().ToList();
                            dataGridView_Station.Refresh();
                            break;

                        default:
                            break;
                    }
                }

                closeProgress();
            }

            catch (System.Exception ex)
            {
                closeProgress();

                MessageBox.Show("เกิดข้อผิดพลาด Error: ในการตรวจสอบข้อมูล สถานี " + Environment.NewLine +
                    "Error: " + ex.Message, "กรุณาติดต่อผู้ดูแลระบบ...",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void toolStripButton_Update_Click(object sender, EventArgs e)
        {
            strConn = Properties.Settings.Default["ServicesMSDBConnectionString"].ToString(); //ConnectionManager.GetConnectionString("0");//Offline
            if (MessageBox.Show("คุณต้องการปรับปรุง ข้อมูลสถานี ใช่หรือไม่???...", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                newProgressbar();
                invoke_Progress("เริ่มปรับปรุง ข้อมูล สถานี");

                string strSTAID = "";
                string tmpMess = "";


                using (DAL.SMSManage.SMSManageDataContext db = new DAL.SMSManage.SMSManageDataContext())
                {

                    db.Connection.Open();
                    db.Transaction = db.Connection.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted);


                    try
                    {

                        //=====================================================================
                        //Station_Group
                        //=====================================================================

                        for (int i = 0; i <= dataGridView_Station_Group.RowCount - 1; i++)
                        {
                            tmpMess = "Station_Group";
                            if ((from t in db.Station_Groups
                                 where t.GOP_ID.Trim() == dataGridView_Station_Group["GOP_ID", i].Value.ToString().Trim()
                                 select t).Count() > 0)
                            {//Update
                                var stg = (from t in db.Station_Groups
                                           where t.GOP_ID.Trim() == dataGridView_Station_Group["GOP_ID", i].Value.ToString().Trim()
                                           select t).FirstOrDefault();

                                stg.GroupStation = dataGridView_Station_Group["GroupStation", i].Value == null ? null : dataGridView_Station_Group["GroupStation", i].Value.ToString();
                                stg.Group_Add1 = dataGridView_Station_Group["Group_Add1", i].Value == null ? null : dataGridView_Station_Group["Group_Add1", i].Value.ToString();
                                stg.Group_Add2 = dataGridView_Station_Group["Group_Add2", i].Value == null ? null : dataGridView_Station_Group["Group_Add2", i].Value.ToString();
                                stg.Group_Zip = dataGridView_Station_Group["Group_Zip", i].Value == null ? null : dataGridView_Station_Group["Group_Zip", i].Value.ToString();
                                stg.Group_Tel = dataGridView_Station_Group["Group_Tel", i].Value == null ? null : dataGridView_Station_Group["Group_Tel", i].Value.ToString();
                                stg.Group_Fax = dataGridView_Station_Group["Group_Fax", i].Value == null ? null : dataGridView_Station_Group["Group_Fax", i].Value.ToString();
                                stg.Group_Zone = dataGridView_Station_Group["Group_Zone", i].Value == null ? null : dataGridView_Station_Group["Group_Zone", i].Value.ToString();
                                stg.PRO_ID = dataGridView_Station_Group["PRO_ID", i].Value == null ? null : dataGridView_Station_Group["PRO_ID", i].Value.ToString();
                                stg.Group_Contact = dataGridView_Station_Group["Group_Contact", i].Value == null ? null : dataGridView_Station_Group["Group_Contact", i].Value.ToString();
                                stg.Group_Type = dataGridView_Station_Group["Group_Type", i].Value == null ? null : dataGridView_Station_Group["Group_Type", i].Value.ToString();

                                if (dataGridView_Station_Group["Group_Dict", i].Value == DBNull.Value)
                                    stg.Group_Dict = 0;
                                else
                                    stg.Group_Dict = Convert.ToDecimal(dataGridView_Station_Group["Group_Dict", i].Value);


                                stg.Description = dataGridView_Station_Group["Description", i].Value == null ? null : dataGridView_Station_Group["Description", i].Value.ToString();
                                //DBNull
                                if (dataGridView_Station_Group["Contract_Price", i].Value == DBNull.Value)
                                    stg.Contract_Price = 0;
                                else
                                    stg.Contract_Price = Convert.ToDecimal(dataGridView_Station_Group["Contract_Price", i].Value);


                                stg.WH = dataGridView_Station_Group["WH", i].Value == null ? null : dataGridView_Station_Group["WH", i].Value.ToString();

                                db.SubmitChanges();
                            }
                            else
                            {//Add
                                var stg = new DAL.SMSManage.Station_Group();
                                stg.GOP_ID = dataGridView_Station_Group["GOP_ID", i].Value.ToString();
                                stg.GroupStation = dataGridView_Station_Group["GroupStation", i].Value == null ? null : dataGridView_Station_Group["GroupStation", i].Value.ToString();
                                stg.Group_Add1 = dataGridView_Station_Group["Group_Add1", i].Value == null ? null : dataGridView_Station_Group["Group_Add1", i].Value.ToString();
                                stg.Group_Add2 = dataGridView_Station_Group["Group_Add2", i].Value == null ? null : dataGridView_Station_Group["Group_Add2", i].Value.ToString();
                                stg.Group_Zip = dataGridView_Station_Group["Group_Zip", i].Value == null ? null : dataGridView_Station_Group["Group_Zip", i].Value.ToString();
                                stg.Group_Tel = dataGridView_Station_Group["Group_Tel", i].Value == null ? null : dataGridView_Station_Group["Group_Tel", i].Value.ToString();
                                stg.Group_Fax = dataGridView_Station_Group["Group_Fax", i].Value == null ? null : dataGridView_Station_Group["Group_Fax", i].Value.ToString();
                                stg.Group_Zone = dataGridView_Station_Group["Group_Zone", i].Value == null ? null : dataGridView_Station_Group["Group_Zone", i].Value.ToString();
                                stg.PRO_ID = dataGridView_Station_Group["PRO_ID", i].Value == null ? null : dataGridView_Station_Group["PRO_ID", i].Value.ToString();
                                stg.Group_Contact = dataGridView_Station_Group["Group_Contact", i].Value == null ? null : dataGridView_Station_Group["Group_Contact", i].Value.ToString();
                                stg.Group_Type = dataGridView_Station_Group["Group_Type", i].Value == null ? null : dataGridView_Station_Group["Group_Type", i].Value.ToString();
                                //stg.Group_Dict = dataGridView_Station_Group["Group_Dict", i].Value == null ? 0 : Convert.ToDecimal(dataGridView_Station_Group["Group_Dict", i].Value.ToString());
                                if (dataGridView_Station_Group["Group_Dict", i].Value == null)
                                    stg.Group_Dict = Convert.ToDecimal(dataGridView_Station_Group["Group_Dict", i].Value);
                                else
                                    stg.Group_Dict = 0;

                                stg.Description = dataGridView_Station_Group["Description", i].Value == null ? null : dataGridView_Station_Group["Description", i].Value.ToString();
                                //stg.Contract_Price = dataGridView_Station_Group["Contract_Price", i].Value == null ? 0 : Convert.ToDecimal(dataGridView_Station_Group["Contract_Price", i].Value.ToString());
                                if (dataGridView_Station_Group["Contract_Price", i].Value == null)
                                    stg.Contract_Price = 0;
                                else
                                    stg.Contract_Price = Convert.ToDecimal(dataGridView_Station_Group["Contract_Price", i].Value);



                                stg.WH = dataGridView_Station_Group["WH", i].Value == null ? null : dataGridView_Station_Group["WH", i].Value.ToString();

                                db.Station_Groups.InsertOnSubmit(stg);
                                db.SubmitChanges();

                            }
                        }


                        //=====================================================================
                        //Stations
                        //=====================================================================
                        for (int i = 0; i <= dataGridView_Station.RowCount - 1; i++)
                        {
                            tmpMess = "Stations";
                            strSTAID = dataGridView_Station["STA_ID1", i].Value == null ? "" : dataGridView_Station["STA_ID1", i].Value.ToString();

                            if ((from t in db.Stations
                                 where t.STA_ID.Trim() ==
                                     dataGridView_Station["STA_ID", i].Value.ToString().Trim()
                                 select t).Count() > 0)
                            {
                                var st = (from t in db.Stations
                                          where t.STA_ID.Trim() ==
                                              dataGridView_Station["STA_ID", i].Value.ToString().Trim()
                                          select t).FirstOrDefault();


                                st.STA_ID1 = dataGridView_Station["STA_ID1", i].Value == null ? "" : dataGridView_Station["STA_ID1", i].Value.ToString();
                                st.StationSys = dataGridView_Station["StationSys", i].Value == null ? "" : dataGridView_Station["StationSys", i].Value.ToString();
                                st.StationRegis = dataGridView_Station["StationRegis", i].Value == null ? "" : dataGridView_Station["StationRegis", i].Value.ToString();
                                st.GOP_ID = dataGridView_Station["GOP_ID", i].Value == null ? "" : dataGridView_Station["GOP_ID", i].Value.ToString();
                                st.Address = dataGridView_Station["Address", i].Value == null ? "" : dataGridView_Station["Address", i].Value.ToString();
                                st.FaxNo = dataGridView_Station["FaxNo", i].Value == null ? "" : dataGridView_Station["FaxNo", i].Value.ToString();
                                st.TelNo = dataGridView_Station["TelNo", i].Value == null ? "" : dataGridView_Station["TelNo", i].Value.ToString();
                                st.SUB_ZONE = dataGridView_Station["SUB_ZONE", i].Value == null ? "" : dataGridView_Station["SUB_ZONE", i].Value.ToString();
                                st.PRO_ID = dataGridView_Station["PRO_ID", i].Value == null ? 99 : int.Parse(dataGridView_Station["PRO_ID", i].Value.ToString());
                                st.IsContract = dataGridView_Station["IsContract", i].Value == null ? false : Convert.ToBoolean(dataGridView_Station["IsContract", i].Value);

                                db.SubmitChanges();
                            }
                            else
                            {
                                var st = new DAL.SMSManage.Station();
                                st.STA_ID = dataGridView_Station["STA_ID", i].Value == null ? "" : dataGridView_Station["STA_ID", i].Value.ToString();
                                st.STA_ID1 = dataGridView_Station["STA_ID1", i].Value == null ? "" : dataGridView_Station["STA_ID1", i].Value.ToString();
                                st.StationSys = dataGridView_Station["StationSys", i].Value == null ? "" : dataGridView_Station["StationSys", i].Value.ToString();
                                st.StationRegis = dataGridView_Station["StationRegis", i].Value == null ? "" : dataGridView_Station["StationRegis", i].Value.ToString();
                                st.GOP_ID = dataGridView_Station["GOP_ID", i].Value == null ? "" : dataGridView_Station["GOP_ID", i].Value.ToString();
                                st.Address = dataGridView_Station["Address", i].Value == null ? "" : dataGridView_Station["Address", i].Value.ToString();
                                st.FaxNo = dataGridView_Station["FaxNo", i].Value == null ? "" : dataGridView_Station["FaxNo", i].Value.ToString();
                                st.TelNo = dataGridView_Station["TelNo", i].Value == null ? "" : dataGridView_Station["TelNo", i].Value.ToString();
                                st.SUB_ZONE = dataGridView_Station["SUB_ZONE", i].Value == null ? "" : dataGridView_Station["SUB_ZONE", i].Value.ToString();
                                st.PRO_ID = dataGridView_Station["PRO_ID", i].Value == null ? 99 : int.Parse(dataGridView_Station["PRO_ID", i].Value.ToString());
                                st.IsContract = dataGridView_Station["IsContract", i].Value == null ? false : Convert.ToBoolean(dataGridView_Station["IsContract", i].Value);

                                db.Stations.InsertOnSubmit(st);
                                db.SubmitChanges();

                            }


                        }


                        db.Transaction.Commit();

                        dataGridView_Station.DataSource = null;
                        dataGridView_Station.Refresh();

                        dataGridView_Station_Group.DataSource = null;
                        dataGridView_Station_Group.Refresh();

                        closeProgress();

                        MessageBox.Show("ปรับปรุงข้อมูล สถานี เรียบร้อย", "ผลการทำงาน", MessageBoxButtons.OK, MessageBoxIcon.Information);


                        toolStripButton_FindStation.Enabled = true;
                        toolStripButton_Check.Enabled = true;
                        toolStripButton_Update.Enabled = false;
                        toolStripButton_Cancel.Enabled = true;

                    }
                    catch (System.Exception ex)
                    {
                        db.Transaction.Rollback();

                        closeProgress();
                        MessageBox.Show("เกิดข้อผิดพลาด Error: ในการปรับปรุงข้อมูล สถานี" + Environment.NewLine +
                            "Section " + tmpMess + " STA_ID1 " + strSTAID + " Error: " + ex.Message, "กรุณาติดต่อผู้ดูแลระบบ...",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }

            }
        }

        private void toolStripButton_Cancel_Click(object sender, EventArgs e)
        {
            toolStripButton_FindStation.Enabled = true;
            toolStripButton_Check.Enabled = true;
            toolStripButton_Update.Enabled = false;
            toolStripButton_Cancel.Enabled = true;
        }

        private void toolStripButton_Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void toolStripButton_Check_Click(object sender, EventArgs e)
        {


            newProgressbar();
            invoke_Progress("กำลังโหลดข้อมูลสถานี บนเครื่อง VAN (Client)");

            try
            {
                listJobID = new List<string>();

                strConn = Properties.Settings.Default["ServicesMSDBConnectionString"].ToString(); //ConnectionManager.GetConnectionString("0");//Offline
                using (DAL.SMSManage.SMSManageDataContext db = new DAL.SMSManage.SMSManageDataContext())
                {
                    var st = from t in db.Stations select t.STA_ID;
                    foreach (var s in st)
                        listJobID.Add(s);
                }

                if (listJobID.Count() > 0)
                {
                    invoke_Progress("กำลังตรวจสอบข้อมูลสถานี ระหว่างเครื่อง VAN และ SMS Server");
                    strConn = Properties.Settings.Default["ServicesMSDBConnectionString"].ToString(); //ConnectionManager.GetConnectionString("1");//Online                   

                    DataTable dt = null;

                    using (SqlConnection conn = new SqlConnection(strConn))
                    {
                        if (conn.State == ConnectionState.Closed)
                            conn.Open();
                        //Station_Group
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Connection = conn;
                            cmd.CommandText = " SELECT DISTINCT  Station_Group.GOP_ID, GroupStation, Group_Add1, Group_Add2, Group_Zip, Group_Tel, Group_Fax, Group_Zone, Station_Group.PRO_ID, Group_Contact, Group_Type,Group_Dict, [Description], Contract_Price, WH " +
                                                " from dbo.Station INNER JOIN Station_Group ON Station.GOP_ID = Station_Group.GOP_ID " +
                                                "   where Station.STA_ID not in  (" + SQLArrayToInString(listJobID.ToArray()) + ") and Station_Group.GOP_ID not in ('FLB01','FLR01','FLW01','F001')";
                            cmd.CommandType = CommandType.Text;
                            cmd.CommandTimeout = 60000;

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                dt = new DataTable();
                                dt.Load(dr);
                                dataGridView_Station_Group.DataSource = dt;
                                dataGridView_Station_Group.Refresh();

                            }

                            //Station
                            cmd.Connection = conn;
                            cmd.CommandText = "select STA_ID, STA_ID1, StationSys, StationRegis, GOP_ID, [Address], FaxNo, TelNo, SUB_ZONE, PRO_ID,  IsContract from Station " +
                                                " where STA_ID not in  (" + SQLArrayToInString(listJobID.ToArray()) + ")  and GOP_ID not in ('FLB01','FLR01','FLW01')"; ;
                            cmd.CommandType = CommandType.Text;
                            cmd.CommandTimeout = 60000;

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                dt = new DataTable();
                                dt.Load(dr);
                                dataGridView_Station.DataSource = dt;
                                dataGridView_Station.Refresh();

                            }

                            if (0 >= dataGridView_Station.RowCount)
                            {
                                closeProgress();
                                dataGridView_Station.DataSource = null;
                                dataGridView_Station.Refresh();

                                MessageBox.Show("ข้อมูลสถานี ตรงกัน", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);

                            }
                        }//using (SqlCommand cmd =  new SqlCommand ())



                    }//using (SqlConnection conn = new SqlConnection(strConn))

                }

                closeProgress();

                toolStripButton_FindStation.Enabled = false;
                toolStripButton_Check.Enabled = false;
                toolStripButton_Update.Enabled = true;
                toolStripButton_Cancel.Enabled = true;

            }
            catch (System.Exception ex)
            {

                closeProgress();
                MessageBox.Show("เกิดข้อผิดพลาด Error: ในการตรวจสอบข้อมูล สถานี" + Environment.NewLine +
                    "Error: " + ex.Message, "กรุณาติดต่อผู้ดูแลระบบ...",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private string SQLArrayToInString(Array a)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i <= a.GetUpperBound(0); i++)
                sb.AppendFormat("{0},", a.GetValue(i));//sb.AppendFormat("{0},", "'"+a.GetValue(i)+"'");
            string retVal = sb.ToString();
            return retVal.Substring(0, retVal.Length - 1);
        }


    }
}
