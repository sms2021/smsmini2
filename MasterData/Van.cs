﻿using SMSMINI.DAL.SMSManage;
using System;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace SMSMINI.MasterData
{
    public partial class Van : Form
    {
        //string strConn = "";
        private ProgressBar.FormProgress m_fmProgress = null;

        private static string vmode = "";
        public string vMode { get { return vmode; } set { vmode = value; } }

        public Van()
        {
            InitializeComponent();
            //this.Text = this.Text + " [v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "]";

            object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
            if (attributes.Length == 0)
            {
                this.Text = "SMS MINI [v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "]";

            }
            this.Text = ((AssemblyProductAttribute)attributes[0]).Product + " [v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "]";

        }

        private void Van_Load(object sender, EventArgs e)
        {
            if (vMode == "unlog")//ไม่ผ่านการ Login
            {
                toolStripLabel_cobby.SelectedIndex = 0;
                toolStripLabel_cobby.Enabled = false;
            }
            else
                toolStripLabel_cobby.Enabled = true;
        }


        #region Progressbar

        private void newProgressbar()
        {
            m_fmProgress = new ProgressBar.FormProgress();
            backgroundWorker1.RunWorkerAsync();
            System.Threading.Thread.Sleep(3000);
        }

        private void invoke_Progress(string strMsg)
        {
            try
            {
                m_fmProgress.lblDescription.Invoke(
                   (MethodInvoker)delegate ()
                   {
                       m_fmProgress.lblDescription.Text = strMsg;
                   }
                   );
            }
            catch (System.Exception)
            { }
        }

        private void closeProgress()
        {
            if (m_fmProgress != null)
            {
                m_fmProgress.Invoke(
                    (MethodInvoker)delegate ()
                    {
                        m_fmProgress.Hide();
                        m_fmProgress.Dispose();
                        m_fmProgress = null;
                    }
                );
            }
        }

        #endregion


        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            m_fmProgress.Activate();
            m_fmProgress.BringToFront();
            m_fmProgress.TopMost = true;
            m_fmProgress.ShowDialog();
        }

        private void Van_FormClosed(object sender, FormClosedEventArgs e)
        {
            DialogResult = DialogResult.Abort;
        }

        private void toolStripButton_FindStation_Click(object sender, EventArgs e)
        {
            if (toolStripText_txtFind.Text != String.Empty)
            {
                SearchVAN();
            }
            else
            {
                toolStripText_txtFind.Focus();
                return;
            }
        }

        private void SearchVAN()
        {
            //strConn = ConnectionManager.GetConnectionString("1");// On line only
            newProgressbar();
            invoke_Progress("ตรวจสอบ ข้อมูล VAN");

            try
            {
                using (SMSMINI.DAL.SMSManage.SMSManageDataContext db = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
                {
                    /**ตาม Van no
                    ตาม ชื่อ ฺVAN
                    ตาม UserName
                    ทั้งหมด*/

                    switch (toolStripLabel_cobby.Text.Trim())
                    {
                        case "ตาม Van no":
                            var u = (from t in db.Users where t.UserName == toolStripText_txtFind.Text.Trim() select t).FirstOrDefault();
                            if (u == null)
                            {
                                closeProgress();
                                MessageBox.Show("VAN " + toolStripText_txtFind.Text + " ไม่มีข้อมูล User", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return;
                            }

                            dataGridView_Employees.DataSource = (from t in db.Employees
                                                                 where t.EMP_ID.Contains(toolStripText_txtFind.Text.Trim())
                                                                 select new
                                                                 {
                                                                     t.EMP_ID,
                                                                     t.EMP_ID1,
                                                                     t.FName,
                                                                     t.LName,
                                                                     t.EMail
                                                                     ,
                                                                     t.Phone//20190719
                                                                 }).Distinct().ToList();
                            dataGridView_Employees.Refresh();

                            dataGridView_VAN.DataSource = (from t in db.VANs
                                                           where t.VAN_ID.Contains(toolStripText_txtFind.Text.Trim())
                                                           select new { t.VAN_ID, t.EMP_ID, t.LicensePlate, t.TEC_SUPER, t.SUP_ID, t.ADM_ID }).Distinct().ToList();
                            dataGridView_VAN.Refresh();

                            dataGridView_Users_Level.DataSource = (from t in db.Users_Levels
                                                                   where u.LEV_ID == t.LEV_ID
                                                                   select new { t.LEV_ID, t.levelName, t.Permiss }).Distinct().ToList();
                            dataGridView_Users_Level.Refresh();


                            dataGridView_Type.DataSource = (from t in db.Types
                                                            where u.TYP_ID == t.TYP_ID
                                                            select new { t.TYP_ID, t.Type1, t.StatType }).Distinct().ToList();
                            dataGridView_Type.Refresh();

                            dataGridView_Users.DataSource = (from t in db.Users
                                                             where t.UserName == toolStripText_txtFind.Text.Trim()
                                                             select new { t.UserName, t.Password, t.LEV_ID, t.EMP_ID, t.TYP_ID }).Distinct().ToList();
                            dataGridView_Users.Refresh();

                            break;

                        case "ตาม ชื่อ ฺVAN":
                            var tmp_u = (from t in db.Users
                                         from e in db.Employees
                                         where t.EMP_ID == e.EMP_ID &&
                                             e.FName.Contains(toolStripText_txtFind.Text.Trim())
                                         select t).FirstOrDefault();
                            if (tmp_u == null)
                            {
                                closeProgress();
                                MessageBox.Show("VAN " + toolStripText_txtFind.Text + " ไม่มีข้อมูล User", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return;
                            }

                            dataGridView_Employees.DataSource = (from t in db.Employees
                                                                 where t.EMP_ID.Contains(toolStripText_txtFind.Text.Trim())
                                                                 select new
                                                                 {
                                                                     t.EMP_ID,
                                                                     t.EMP_ID1,
                                                                     t.FName,
                                                                     t.LName,
                                                                     t.EMail
                                                                  ,
                                                                     t.Phone//20190719
                                                                 }).Distinct().ToList();
                            dataGridView_Employees.Refresh();

                            dataGridView_VAN.DataSource = (from t in db.VANs
                                                           where t.VAN_ID.Contains(toolStripText_txtFind.Text.Trim())
                                                           select new { t.VAN_ID, t.EMP_ID, t.LicensePlate, t.TEC_SUPER, t.SUP_ID, t.ADM_ID }).Distinct().ToList();
                            dataGridView_VAN.Refresh();

                            dataGridView_Users_Level.DataSource = (from t in db.Users_Levels
                                                                   where tmp_u.LEV_ID == t.LEV_ID
                                                                   select new { t.LEV_ID, t.levelName, t.Permiss }).Distinct().ToList();
                            dataGridView_Users_Level.Refresh();


                            dataGridView_Type.DataSource = (from t in db.Types
                                                            where tmp_u.TYP_ID == t.TYP_ID
                                                            select new { t.TYP_ID, t.Type1, t.StatType }).Distinct().ToList();
                            dataGridView_Type.Refresh();

                            dataGridView_Users.DataSource = (from t in db.Users
                                                             from e in db.Employees
                                                             where t.EMP_ID == e.EMP_ID &&
                                                                 e.FName.Contains(toolStripText_txtFind.Text.Trim())
                                                             select new { t.UserName, t.Password, t.LEV_ID, t.EMP_ID, t.TYP_ID }).Distinct().ToList();
                            dataGridView_Users.Refresh();


                            break;

                        case "ตาม UserName":
                            var tmp_u1 = (from t in db.Users
                                          from e in db.Employees
                                          where t.EMP_ID == e.EMP_ID &&
                                             t.UserName.Contains(toolStripText_txtFind.Text.Trim())
                                          select t).FirstOrDefault();

                            if (tmp_u1 == null)
                            {
                                closeProgress();
                                MessageBox.Show("VAN " + toolStripText_txtFind.Text + " ไม่มีข้อมูล User", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return;
                            }
                            dataGridView_Employees.DataSource = (from t in db.Employees
                                                                 where t.EMP_ID.Contains(toolStripText_txtFind.Text.Trim())
                                                                 select new
                                                                 {
                                                                     t.EMP_ID,
                                                                     t.EMP_ID1,
                                                                     t.FName,
                                                                     t.LName,
                                                                     t.EMail
                                                                 ,
                                                                     t.Phone //20100719
                                                                 }).Distinct().ToList();
                            dataGridView_Employees.Refresh();

                            dataGridView_VAN.DataSource = (from t in db.VANs
                                                           where t.VAN_ID.Contains(toolStripText_txtFind.Text.Trim())
                                                           select new { t.VAN_ID, t.EMP_ID, t.LicensePlate, t.TEC_SUPER, t.SUP_ID, t.ADM_ID }).Distinct().ToList();
                            dataGridView_VAN.Refresh();

                            dataGridView_Users_Level.DataSource = (from t in db.Users_Levels
                                                                   where tmp_u1.LEV_ID == t.LEV_ID
                                                                   select new { t.LEV_ID, t.levelName, t.Permiss }).Distinct().ToList();
                            dataGridView_Users_Level.Refresh();


                            dataGridView_Type.DataSource = (from t in db.Types
                                                            where tmp_u1.TYP_ID == t.TYP_ID
                                                            select new { t.TYP_ID, t.Type1, t.StatType }).Distinct().ToList();
                            dataGridView_Type.Refresh();

                            dataGridView_Users.DataSource = (from t in db.Users
                                                             from e in db.Employees
                                                             where t.EMP_ID == e.EMP_ID &&
                                                                t.UserName.Contains(toolStripText_txtFind.Text.Trim())
                                                             select new { t.UserName, t.Password, t.LEV_ID, t.EMP_ID, t.TYP_ID }).Distinct().ToList();
                            dataGridView_Users.Refresh();

                            break;

                        default:
                            break;

                    }
                }
                closeProgress();
            }
            catch (System.Exception ex)
            {
                closeProgress();

                MessageBox.Show("เกิดข้อผิดพลาด Error: ในการตรวจสอบข้อมูล VAN " + Environment.NewLine +
                    "Error: " + ex.Message, "กรุณาติดต่อผู้ดูแลระบบ...",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void toolStripButton_Update_Click(object sender, EventArgs e)
        {
            //strConn = ConnectionManager.GetConnectionString("0");//Offline
            if (MessageBox.Show("คุณต้องการ Load VAN (ผู้ใช้งาน) ใช่หรือไม่ ???...", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                newProgressbar();
                invoke_Progress("เริ่มปรับปรุง ข้อมูล VAN");

                using (SMSManageDataContext db = new SMSManageDataContext())
                {
                    db.Connection.Open();
                    db.Transaction = db.Connection.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted);


                    try
                    {
                        //=====================================================================
                        //Employees
                        //=====================================================================
                        for (int i = 0; i <= dataGridView_Employees.RowCount - 1; i++)
                        {
                            if ((from t in db.Employees
                                 where t.EMP_ID.Trim() ==
              dataGridView_Employees["EMP_ID", i].Value.ToString().Trim()
                                 select t).Count() > 0)
                            {
                                var em = (from t in db.Employees
                                          where t.EMP_ID.Trim() ==
                                              dataGridView_Employees["EMP_ID", i].Value.ToString().Trim()
                                          select t).FirstOrDefault();


                                em.EMP_ID1 = dataGridView_Employees["EMP_ID1", i].Value == null ? "" : dataGridView_Employees["EMP_ID1", i].Value.ToString();
                                em.FName = dataGridView_Employees["FName", i].Value == null ? "" : dataGridView_Employees["FName", i].Value.ToString();
                                em.LName = dataGridView_Employees["LName", i].Value == null ? "" : dataGridView_Employees["LName", i].Value.ToString();
                                em.EMail = dataGridView_Employees["EMail", i].Value == null ? "" : dataGridView_Employees["EMail", i].Value.ToString();
                                //20190719
                                em.Phone = dataGridView_Employees["Phone", i].Value == null ? "" : dataGridView_Employees["Phone", i].Value.ToString();
                                db.SubmitChanges();
                            }
                            else
                            {
                                var em = new DAL.SMSManage.Employee();
                                em.EMP_ID = dataGridView_Employees["EMP_ID", i].Value == null ? "" : dataGridView_Employees["EMP_ID", i].Value.ToString();
                                em.EMP_ID1 = dataGridView_Employees["EMP_ID1", i].Value == null ? "" : dataGridView_Employees["EMP_ID1", i].Value.ToString();
                                em.FName = dataGridView_Employees["FName", i].Value == null ? "" : dataGridView_Employees["FName", i].Value.ToString();
                                em.LName = dataGridView_Employees["LName", i].Value == null ? "" : dataGridView_Employees["LName", i].Value.ToString();
                                em.EMail = dataGridView_Employees["EMail", i].Value == null ? "" : dataGridView_Employees["EMail", i].Value.ToString();
                                //20190719
                                em.Phone = dataGridView_Employees["Phone", i].Value == null ? "" : dataGridView_Employees["Phone", i].Value.ToString();

                                db.Employees.InsertOnSubmit(em);
                                db.SubmitChanges();

                            }

                        }

                        //=====================================================================
                        //VAN                        
                        //=====================================================================
                        for (int i = 0; i <= dataGridView_VAN.RowCount - 1; i++)
                        {
                            if ((from t in db.VANs
                                 where t.VAN_ID.Trim() ==
                                     dataGridView_VAN["VAN_ID", i].Value.ToString().Trim()
                                 select t).Count() > 0)
                            {
                                //t.VAN_ID, t.EMP_ID, t.LicensePlate, t.TEC_SUPER, t.SUP_ID, t.ADM_ID
                                var van = (from t in db.VANs
                                           where t.VAN_ID.Trim() ==
                                               dataGridView_VAN["VAN_ID", i].Value.ToString().Trim()
                                           select t).FirstOrDefault();

                                van.EMP_ID = dataGridView_VAN["EMP_ID", i].Value == null ? "" : dataGridView_VAN["EMP_ID", i].Value.ToString();
                                van.LicensePlate = dataGridView_VAN["LicensePlate", i].Value == null ? "" : dataGridView_VAN["LicensePlate", i].Value.ToString();
                                van.TEC_SUPER = dataGridView_VAN["TEC_SUPER", i].Value == null ? "" : dataGridView_VAN["TEC_SUPER", i].Value.ToString();
                                van.SUP_ID = dataGridView_VAN["SUP_ID", i].Value == null ? "" : dataGridView_VAN["SUP_ID", i].Value.ToString();
                                van.ADM_ID = dataGridView_VAN["ADM_ID", i].Value == null ? "" : dataGridView_VAN["ADM_ID", i].Value.ToString();

                                db.SubmitChanges();
                            }
                            else
                            {
                                var van = new DAL.SMSManage.VAN();

                                van.VAN_ID = dataGridView_VAN["VAN_ID", i].Value == null ? "" : dataGridView_VAN["VAN_ID", i].Value.ToString();
                                van.EMP_ID = dataGridView_VAN["EMP_ID", i].Value == null ? "" : dataGridView_VAN["EMP_ID", i].Value.ToString();
                                van.LicensePlate = dataGridView_VAN["LicensePlate", i].Value == null ? "" : dataGridView_VAN["LicensePlate", i].Value.ToString();
                                van.TEC_SUPER = dataGridView_VAN["TEC_SUPER", i].Value == null ? "" : dataGridView_VAN["TEC_SUPER", i].Value.ToString();
                                van.SUP_ID = dataGridView_VAN["SUP_ID", i].Value == null ? "" : dataGridView_VAN["SUP_ID", i].Value.ToString();
                                van.ADM_ID = dataGridView_VAN["ADM_ID", i].Value == null ? "" : dataGridView_VAN["ADM_ID", i].Value.ToString();

                                db.VANs.InsertOnSubmit(van);
                                db.SubmitChanges();

                            }

                        }

                        //=====================================================================
                        //Users_Level
                        //=====================================================================
                        for (int i = 0; i <= dataGridView_Users_Level.RowCount - 1; i++)
                        {
                            if ((from t in db.Users_Levels
                                 where t.LEV_ID.Trim() ==
                                     dataGridView_Users_Level["LEV_ID", i].Value.ToString().Trim()
                                 select t).Count() > 0)
                            {
                                //t.VAN_ID, t.EMP_ID, t.LicensePlate, t.TEC_SUPER, t.SUP_ID, t.ADM_ID
                                var van = (from t in db.Users_Levels
                                           where t.LEV_ID.Trim() ==
                                               dataGridView_Users_Level["LEV_ID", i].Value.ToString().Trim()
                                           select t).FirstOrDefault();

                                van.levelName = dataGridView_Users_Level["levelName", i].Value == null ? "" : dataGridView_Users_Level["levelName", i].Value.ToString();
                                van.Permiss = dataGridView_Users_Level["Permiss", i].Value == null ? "" : dataGridView_Users_Level["Permiss", i].Value.ToString();
                                db.SubmitChanges();
                            }
                            else
                            {
                                var van = new DAL.SMSManage.Users_Level();

                                van.levelName = dataGridView_Users_Level["levelName", i].Value == null ? "" : dataGridView_Users_Level["levelName", i].Value.ToString();
                                van.Permiss = dataGridView_Users_Level["Permiss", i].Value == null ? "" : dataGridView_Users_Level["Permiss", i].Value.ToString();

                                db.Users_Levels.InsertOnSubmit(van);
                                db.SubmitChanges();

                            }
                        }

                        //=====================================================================
                        //Type
                        //=====================================================================
                        //t.TYP_ID, t.Type1, t.StatType
                        for (int i = 0; i <= dataGridView_Type.RowCount - 1; i++)
                        {
                            if ((from t in db.Types
                                 where t.TYP_ID.Trim() ==
                                     dataGridView_Type["TYP_ID", i].Value.ToString().Trim()
                                 select t).Count() > 0)
                            {
                                //t.VAN_ID, t.EMP_ID, t.LicensePlate, t.TEC_SUPER, t.SUP_ID, t.ADM_ID
                                var van = (from t in db.Types
                                           where t.TYP_ID.Trim() ==
                                               dataGridView_Type["TYP_ID", i].Value.ToString().Trim()
                                           select t).FirstOrDefault();

                                van.Type1 = dataGridView_Type["Type1", i].Value == null ? "" : dataGridView_Type["Type1", i].Value.ToString();
                                van.StatType = dataGridView_Type["StatType", i].Value == null ? "" : dataGridView_Type["StatType", i].Value.ToString();
                                db.SubmitChanges();
                            }
                            else
                            {
                                var van = new DAL.SMSManage.Type();

                                van.Type1 = dataGridView_Type["Type1", i].Value == null ? "" : dataGridView_Type["Type1", i].Value.ToString();
                                van.StatType = dataGridView_Type["StatType", i].Value == null ? "" : dataGridView_Type["StatType", i].Value.ToString();

                                db.Types.InsertOnSubmit(van);
                                db.SubmitChanges();

                            }
                        }

                        //=====================================================================
                        //Users
                        //=====================================================================
                        // t.UserName, t.Password, t.LEV_ID, t.EMP_ID, t.TYP_ID
                        for (int i = 0; i <= dataGridView_Users.RowCount - 1; i++)
                        {
                            if ((from t in db.Users
                                 where t.UserName.Trim() ==
                                     dataGridView_Users["UserName", i].Value.ToString().Trim()
                                 select t).Count() > 0)
                            {
                                //t.VAN_ID, t.EMP_ID, t.LicensePlate, t.TEC_SUPER, t.SUP_ID, t.ADM_ID
                                var van = (from t in db.Users
                                           where t.UserName.Trim() ==
                                               dataGridView_Users["UserName", i].Value.ToString().Trim()
                                           select t).FirstOrDefault();

                                van.UserName = dataGridView_Users["UserName", i].Value == null ? "" : dataGridView_Users["UserName", i].Value.ToString();
                                van.Password = dataGridView_Users["Password", i].Value == null ? "" : dataGridView_Users["Password", i].Value.ToString();
                                van.LEV_ID = dataGridView_Users["LEV_ID", i].Value == null ? "" : dataGridView_Users["LEV_ID", i].Value.ToString();
                                van.EMP_ID = dataGridView_Users["EMP_ID", i].Value == null ? "" : dataGridView_Users["EMP_ID", i].Value.ToString();
                                van.TYP_ID = dataGridView_Users["TYP_ID", i].Value == null ? "" : dataGridView_Users["TYP_ID", i].Value.ToString();
                                db.SubmitChanges();
                            }
                            else
                            {
                                var van = new DAL.SMSManage.User();

                                van.UserName = dataGridView_Users["UserName", i].Value == null ? "" : dataGridView_Users["UserName", i].Value.ToString();
                                van.Password = dataGridView_Users["Password", i].Value == null ? "" : dataGridView_Users["Password", i].Value.ToString();
                                van.LEV_ID = dataGridView_Users["LEV_ID", i].Value == null ? "" : dataGridView_Users["LEV_ID", i].Value.ToString();
                                van.EMP_ID = dataGridView_Users["EMP_ID", i].Value == null ? "" : dataGridView_Users["EMP_ID", i].Value.ToString();
                                van.TYP_ID = dataGridView_Users["TYP_ID", i].Value == null ? "" : dataGridView_Users["TYP_ID", i].Value.ToString();

                                db.Users.InsertOnSubmit(van);
                                db.SubmitChanges();

                            }
                        }

                        db.Transaction.Commit();


                        dataGridView_Users.DataSource = null;
                        dataGridView_Users.Refresh();
                        closeProgress();

                        MessageBox.Show("ปรับปรุงข้อมูล VAN เรียบร้อย", "ผลการทำงาน", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                    catch (System.Exception ex)
                    {
                        closeProgress();
                        db.Transaction.Rollback();
                        MessageBox.Show("เกิดข้อผิดพลาด Error: ในการปรับปรุงข้อมูล VAN" + Environment.NewLine +
                            "Error: " + ex.Message, "กรุณาติดต่อผู้ดูแลระบบ...",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }

            }
        }

        private void toolStripButton_Cancel_Click(object sender, EventArgs e)
        {

        }

        private void toolStripButton_Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void toolStripText_txtFind_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                toolStripButton_FindStation_Click(null, null);
        }



    }
}
