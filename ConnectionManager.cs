﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace SMSMINI
{
    /// <summary>
    /// ConnectionManager
    /// </summary>
    public class ConnectionManager
    {

        /// <summary>
        /// getServerTime
        /// </summary>
        /// <returns></returns>
        public static DateTime getServerTime()
        {
            DateTime _dateServer = DateTime.Now;

            string conn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
            try
            {

                using (SqlConnection cn = new SqlConnection(conn))
                {
                    if (cn.State != ConnectionState.Open)
                        cn.Open();
                    SqlCommand cmd = new SqlCommand("select GetDate()", cn);
                    SqlDataReader dr = cmd.ExecuteReader();
                    if (dr.HasRows)
                    {
                        dr.Read();
                        _dateServer = Convert.ToDateTime(dr[0]);
                        dr.Close();
                    }
                }

            }
            catch (System.Exception ex)
            {
                throw ex;
            }

            return _dateServer;
        }


        /// <summary>
        /// checkVersion
        /// </summary>
        /// <param name="db"></param>
        /// <param name="version"></param>
        /// <param name="serverVersion"></param>
        /// <returns></returns>
        public static bool checkVersion(DAL.SMSManage.SMSManageDataContext db, string version, ref string serverVersion)
        {
            var q = (from t in db.Conf_SMSminiVersions orderby t.VersionDate descending where t.Type == "3" select t).FirstOrDefault();
            if (q != null)
            {
                string[] vServ = q.SMSVersion.Split('.');
                string[] vClient = version.Split('.');

                if (vServ.Length > 0 && vClient.Length > 0)
                {
                    if (int.Parse(vServ[0]) > int.Parse(vClient[0]))
                    {
                        serverVersion = "New version: SMS MINI v." + q.SMSVersion + Environment.NewLine +
                                       "วันที่ Publish: " + q.VersionDate + Environment.NewLine +
                                       "ปรับปรุง: " + Environment.NewLine +
                                       q.Detail;
                        return false;
                    }
                    else if ((int.Parse(vServ[0]) == int.Parse(vClient[0])) && (int.Parse(vServ[1]) > int.Parse(vClient[1])))
                    {
                        serverVersion = "New version: SMS MINI v." + q.SMSVersion + Environment.NewLine +
                                       "วันที่ Publish: " + q.VersionDate + Environment.NewLine +
                                       "ปรับปรุง: " + Environment.NewLine +
                                       q.Detail;
                        return false;
                    }
                    else if (((int.Parse(vServ[0]) == int.Parse(vClient[0])) && (int.Parse(vServ[1]) == int.Parse(vClient[1]))) && (int.Parse(vServ[2]) > int.Parse(vClient[2])))
                    {
                        serverVersion = "New version: SMS MINI v." + q.SMSVersion + Environment.NewLine +
                                       "วันที่ Publish: " + q.VersionDate + Environment.NewLine +
                                       "ปรับปรุง: " + Environment.NewLine +
                                       q.Detail;
                        return false;
                    }
                    else if (((int.Parse(vServ[0]) == int.Parse(vClient[0])) && (int.Parse(vServ[1]) == int.Parse(vClient[1]))) && (int.Parse(vServ[2]) == int.Parse(vClient[2])) && (int.Parse(vServ[3]) > int.Parse(vClient[3])))
                    {
                        serverVersion = "New version: SMS MINI v." + q.SMSVersion + Environment.NewLine +
                                       "วันที่ Publish: " + q.VersionDate + Environment.NewLine +
                                       "ปรับปรุง: " + Environment.NewLine +
                                       q.Detail;
                        return false;
                    }
                }


            }
            return true;
        }


        //IsNumeric
        /// <summary>
        /// IsNumeric
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        public static bool IsNumeric(object arg)
        {
            float number;
            if (float.TryParse(arg.ToString(), out number))
                return true;
            return false;
        }

        //IsDate()
        /// <summary>
        /// IsDate
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        public static bool IsDate(object arg)
        {
            DateTime dt;
            if (DateTime.TryParse(arg.ToString(), out dt))
                return true;
            return false;
        }



        public static string dates_Quarter(DateTime jobOpen)
        {
            string smsQuarter = "";
            if (jobOpen.Month >= 1 && jobOpen.Month <= 3)
            {
                smsQuarter = "1";
            }
            else if (jobOpen.Month >= 4 && jobOpen.Month <= 6)
            {

                smsQuarter = "2";
            }
            else if (jobOpen.Month >= 7 && jobOpen.Month <= 9)
            {

                smsQuarter = "3";
            }
            else if (jobOpen.Month >= 10 && jobOpen.Month <= 12)
            {

                smsQuarter = "4";
            }
            return smsQuarter;
        }



        public static bool checkDBVersion(string strConn_Mini, string strConn_Mange)
        {
            DateTime? dbcreateTime_Manage = null;
            DateTime? dbcreateTime_mini = null;

            try
            {
                //Manage SMSDBVersions
                using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn_Mange))
                {
                    var q = dc.SMSDBVersions
                        .OrderByDescending(t => t.VersionDate)
                        .FirstOrDefault();

                    if (q != null)
                        dbcreateTime_Manage = q.VersionDate.Value;
                }

                //Mini SMSDBVersions
                using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn_Mini))
                {
                    var q = dc.SMSDBVersions
                        .OrderByDescending(t => t.VersionDate)
                        .FirstOrDefault();

                    if (q != null)
                        dbcreateTime_mini = q.VersionDate.Value;
                }

                if (dbcreateTime_mini == null)
                    return false;
                else if (dbcreateTime_Manage.Value.Date == dbcreateTime_mini.Value.Date)
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                return true;
            }
        }


    }
}
