﻿using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace SMSMINI.Report
{
    public partial class PrintFailureCodeWIWP : Form
    {
        public string tmpJOBID { get; set; }
        public bool mPrint { get; set; }
        List<SetFailureCodeWIWP> lsfWIWP = null;
        Report.rptFailureCodeWIWP rpt = null;

        public PrintFailureCodeWIWP()
        {
            InitializeComponent();
        }

        private void btExportPDF_Click(object sender, EventArgs e)
        {
            showReport("PDF");
        }

        private void PrintFailureCodeWIWP_Load(object sender, EventArgs e)
        {
            rpt = new rptFailureCodeWIWP();
            showReport("");
        }


        private void showReport(string mode)
        {

            if (mode == "")
            {
                string strConn = Properties.Settings.Default["ServicesMSDBConnectionString"].ToString();

                using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
                {
                    //var q = (from j in dc.JOBs
                    //         from jd in dc.JOB_Details
                    //         from s in dc.Stations
                    //         from f in dc.Failures
                    //         from fw in dc.Failure_WiWps
                    //         where
                    //            (j.JOB_ID == jd.JOB_ID &&
                    //            j.STA_ID == s.STA_ID &&
                    //            j.FAI_ID == f.FAI_ID &&
                    //            j.FAI_ID == fw.FAI_ID) &&
                    //            j.JOB_ID == tmpJOBID
                    //         select new SetFailureCodeWIWP
                    //         {
                    //             JOB_ID = j.JOB_ID,
                    //             JobFailure_Detail = j.JobFailure_Detail,
                    //             Failure_th = f.Failure_th,
                    //             FAI_ID = j.FAI_ID,
                    //             EMP_ID3 = j.EMP_ID3,
                    //             StartDate = jd.StartDate,
                    //             WIWP_No = fw.WIWP_No,
                    //             WIWP_Detail = (fw.WIWP_Detail == null ? "" : fw.WIWP_Detail),
                    //             Station = s.StationSys
                    //         }).Distinct();

                    var q = dc.vw_mini_PrintFailureCodeWIWPs.Where(t => t.JOB_ID == tmpJOBID)
                        .Select(t => new SetFailureCodeWIWP
                        {
                            JOB_ID = t.JOB_ID,
                            JobFailure_Detail = t.JobFailure_Detail,
                            Failure_th = t.Failure_th,
                            FAI_ID = t.FAI_ID,
                            EMP_ID3 = t.EMP_ID3,
                            StartDate = t.StartDate,
                            WIWP_No = t.WIWP_No,
                            WIWP_Detail = (t.WIWP_Detail == null ? "" : t.WIWP_Detail),
                            Station = t.StationSys

                        }).Distinct();
                    if (q.Count() > 0)
                    {
                        lsfWIWP = new List<SetFailureCodeWIWP>();
                        lsfWIWP = q.ToList();

                        rpt.SetDatabaseLogon("sa", "enabler", "sms.flowco.co.th,1414", "ServicesMSDB");
                        rpt.SetDataSource(lsfWIWP.ToList());
                        rpt.SetParameterValue("miniVersion", "SMS MINI v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString());

                        crystalReportViewer1.ReportSource = rpt;

                    }
                    else
                    {
                        if (mPrint)
                            MessageBox.Show("ไม่มีข้อมูล WI/WP");
                        this.Close();
                    }
                }
            }
            else if (lsfWIWP.Count() > 0)
            {
                //rpt.SetDataSource(lsfWIWP.ToList());
                // rpt.SetParameterValue("miniVersion", "SMS MINI v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString());

                Export2PDF(rpt);
            }
        }

        private void Export2PDF(rptFailureCodeWIWP rpt)
        {
            string f = "";
            SaveFileDialog DialogSave = new SaveFileDialog();
            DialogSave.DefaultExt = "xls";
            DialogSave.Filter = "Adobe Acrobat  (*.pdf)|*.pdf|All files (*.*)|*.*";
            DialogSave.AddExtension = true;
            DialogSave.RestoreDirectory = true;
            DialogSave.Title = "Where do you want to save the file?";
            DialogSave.InitialDirectory = @"C:/";


            var sv = "FailureCodeWIWP_" + tmpJOBID + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".pdf";

            DialogSave.FileName = sv;

            if (DialogSave.ShowDialog() == DialogResult.OK)
            {
                f = DialogSave.FileName.ToString();
                DialogSave.Dispose();
                DialogSave = null;

                try
                {
                    ExportOptions CrExportOptions;
                    DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                    PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();
                    CrDiskFileDestinationOptions.DiskFileName = f;


                    CrExportOptions = rpt.ExportOptions;
                    {
                        CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                        CrExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                        CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
                        CrExportOptions.FormatOptions = CrFormatTypeOptions;

                    }
                    rpt.Export();
                    rpt.Close();

                    if (System.IO.File.Exists(f))
                    {
                        System.Diagnostics.Process.Start(f);
                    }
                }

                catch (Exception ex)
                {
                    MessageBox.Show("ข้อผิดพลาด " + ex.ToString(), "เกิดข้อผิดพลาดในการ Export ไฟล์ SV PDF");
                }
            }// if (DialogSave.ShowDialog() == DialogResult.OK)
        }
    }

    public class SetFailureCodeWIWP
    {
        public string FAI_ID { get; set; }
        public string WIWP_No { get; set; }
        public string WIWP_Detail { get; set; }
        public string JOB_ID { get; set; }
        public string JobFailure_Detail { get; set; }
        public DateTime StartDate { get; set; }
        public string Failure_th { get; set; }
        public string EMP_ID3 { get; set; }
        public string Station { get; set; }
    }
}
