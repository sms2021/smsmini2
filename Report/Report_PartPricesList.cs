﻿using System;
using System.Data;
using System.Reflection;
using System.Windows.Forms;

namespace SMSMINI.Report
{
    public partial class Report_PartPricesList : Form
    {
        private string strConn = "";

        string strUsr = "sa";
        string strPwd = "enabler";
        string strServer = ".\\SQLEXPRESS";
        string strDB = "SMSminiDB";



        public Report_PartPricesList()
        {
            InitializeComponent();

            object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
            if (attributes.Length == 0)
            {
                this.Text = "SMS MINI [v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "]";

            }
            this.Text = ((AssemblyProductAttribute)attributes[0]).Product + " [v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "]";

        }

        private void getDBName()
        {
            string _dbName = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
            //ConnectionManager.GetConnectionString("0");

            try
            {
                strDB = _dbName.Split('=')[2].Split('=')[0].Split(';')[0].ToString();
            }
            catch (System.Exception)
            {
                strDB = "SMSminiDB";
            }
        }

        private void Report_PartPricesList_Load(object sender, EventArgs e)
        {
            toolStripLabel_cobby.SelectedIndex = 0;
            toolStrip_txtTQDate.Text = DateTime.Now.ToString("yyyyMMdd", new System.Globalization.CultureInfo("en-US"));
        }

        private void toolStripButton_FindStation_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;

            strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;//ConnectionManager.GetConnectionString((UserInfo.ConnectMode ?? "0"));
            rptPartPricesList rpt = new rptPartPricesList();

            using (System.Data.SqlClient.SqlConnection Conn = new System.Data.SqlClient.SqlConnection(strConn))
            {
                if (Conn.State == ConnectionState.Closed)
                    Conn.Open();

                DataTable tmpTable = new DataTable();

                /*ตาม Part no
                ตาม Part name
                ตาม กลุ่มบริษัท
                ตาม ไตรมาส (QT)
                ตาม ไตรมาส (yyyy-mm-dd)
                ทั้งหมด*/
                string sql = "";

                if (toolStrip_txtTQDate.Text.Trim() == string.Empty)
                {
                    if (toolStripLabel_cobby.Text == "ตาม Part no")
                        sql = "select * from v_rpt_PartPricesList where SPA_ID like '%" + toolStripText_txtFind.Text + "%' order by GOP_ID ;";
                    else if (toolStripLabel_cobby.Text == "ตาม Part name")
                        sql = "select * from v_rpt_PartPricesList  where SparePart like '%" + toolStripText_txtFind.Text + "%' order by GOP_ID;";
                    else if (toolStripLabel_cobby.Text == "ตาม กลุ่มบริษัท")
                        sql = "select * from v_rpt_PartPricesList   where GroupStation like '%" + toolStripText_txtFind.Text + "%' order by GOP_ID;";
                    else if (toolStripLabel_cobby.Text == "ตาม ไตรมาส (QT)")
                        sql = "select * from v_rpt_PartPricesList   where Quarter like '%" + toolStripText_txtFind.Text + "%' order by GOP_ID ;";
                    else
                        sql = "select * from v_rpt_PartPricesList order by GOP_ID;";

                }
                else
                {
                    if (toolStripLabel_cobby.Text == "ตาม Part no")
                        sql = "select * from v_rpt_PartPricesList where SPA_ID like '%" + toolStripText_txtFind.Text + "%' and Convert(nvarchar,StartDate,112)<='" + toolStrip_txtTQDate.Text + "' and Convert(nvarchar,[EndDate]+1,112)> '" + toolStrip_txtTQDate.Text + "' order by GOP_ID ;";
                    else if (toolStripLabel_cobby.Text == "ตาม Part name")
                        sql = "select * from v_rpt_PartPricesList  where SparePart like '%" + toolStripText_txtFind.Text + "%' and Convert(nvarchar,StartDate,112)<='" + toolStrip_txtTQDate.Text + "' and Convert(nvarchar,[EndDate]+1,112)> '" + toolStrip_txtTQDate.Text + "' order by GOP_ID;";
                    else if (toolStripLabel_cobby.Text == "ตาม กลุ่มบริษัท")
                        sql = "select * from v_rpt_PartPricesList   where GroupStation like '%" + toolStripText_txtFind.Text + "%' and Convert(nvarchar,StartDate,112)<='" + toolStrip_txtTQDate.Text + "' and Convert(nvarchar,[EndDate]+1,112)> '" + toolStrip_txtTQDate.Text + "' order by GOP_ID;";
                    else if (toolStripLabel_cobby.Text == "ตาม ไตรมาส (QT)")
                        sql = "select * from v_rpt_PartPricesList   where Quarter like '%" + toolStripText_txtFind.Text + "%' and Convert(nvarchar,StartDate,112)<='" + toolStrip_txtTQDate.Text + "' and Convert(nvarchar,[EndDate]+1,112)> '" + toolStrip_txtTQDate.Text + "' order by GOP_ID ;";
                    else
                        sql = "select * from v_rpt_PartPricesList where Convert(nvarchar,StartDate,112)<='" + toolStrip_txtTQDate.Text + "' and Convert(nvarchar,[EndDate]+1,112)> '" + toolStrip_txtTQDate.Text + "' order by GOP_ID;";

                }
                System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(sql, Conn);
                DataSet ds = new DataSet();
                da.Fill(ds);


                if (ds.Tables[0].Rows.Count > 0)
                {
                    rpt.SetDatabaseLogon(strUsr, strPwd, strServer, strDB);

                    rpt.SetDataSource(ds.Tables[0].DefaultView);
                    crystalReportViewer1.ReportSource = rpt;
                }
                else
                {
                    crystalReportViewer1.ReportSource = null;
                    MessageBox.Show("ไม่มีข้อมูล กรุณาป้อนเงื่อนไขใหม่ ", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }

            Cursor.Current = Cursors.Default;
        }

        private void toolStripButton_Save_Click(object sender, EventArgs e)
        {

        }

        private void toolStripButton_Cancel_Click(object sender, EventArgs e)
        {
            crystalReportViewer1.ReportSource = null;
        }

        private void toolStripButton_Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Report_PartPricesList_FormClosed(object sender, FormClosedEventArgs e)
        {
            DialogResult = DialogResult.Abort;
        }
    }
}
