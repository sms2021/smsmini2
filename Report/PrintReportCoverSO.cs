﻿using CrystalDecisions.Shared;
#pragma warning disable CS0246 // The type or namespace name 'Janawat' could not be found (are you missing a using directive or an assembly reference?)
using Janawat.Application.Data;
#pragma warning restore CS0246 // The type or namespace name 'Janawat' could not be found (are you missing a using directive or an assembly reference?)
using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;


namespace SMSMINI.Report
{
    public partial class PrintReportCoverSO : Form
    {
        private ProgressBar.FormProgress m_fmProgress = null;


        DAL.ThaiPath thaiBath;
        string strConn = "";

        string strUsr = "sa";
        string strPwd = "enabler";
        string strServer = "203.107.158.164,1414";
        string strDB = "ServicesMSDB";

        string fMode = "Load";

        rptReportCoverSO rptSO;
        rptReportCoverSO_SumPart rptSO_SumPart;


        public PrintReportCoverSO()
        {
            InitializeComponent();
        }

        private void PrintReportCoverSO_Load(object sender, EventArgs e)
        {

            Thread.CurrentThread.CurrentCulture = new CultureInfo("th-TH");
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("th-TH");

            rptSO = new SMSMINI.Report.rptReportCoverSO();
            rptSO_SumPart = new SMSMINI.Report.rptReportCoverSO_SumPart();

            strConn = Properties.Settings.Default["ServicesMSDBConnectionString"].ToString();
            SetComboBox();

            fMode = "";

            btReport.Enabled = false;
            btExportPDF.Enabled = false;
            button1.Enabled = false;
        }

        private void SetComboBox()
        {
            string sql = " SELECT [ZON_ID] ,[ServiceZone]  FROM [ServiceZone]  where [ZON_ID] in (select distinct ZON_ID from VAN_ServiceZone) Order by  ServiceZone;" +
                        "SELECT  [GOP_ID]  ,[TradeName]+ ': '+[GroupStation] as GroupStation FROM [Station_Group]   where TradeName is not null and [GOP_ID] <> '00000' Order by TradeName;";

            using (SqlDataReader dr = JaSqlHelper.ExecuteReader(strConn, CommandType.Text, sql))
            {
                DataTable dt;

                if (dr.HasRows)
                {

                    dt = new DataTable();
                    dt.Load(dr);

                    comboBox1.ValueMember = "ZON_ID";
                    comboBox1.DisplayMember = "ServiceZone";
                    comboBox1.DataSource = dt;

                    dt = new DataTable();
                    dt.Load(dr);

                    comboBox_Group.ValueMember = "GOP_ID";
                    comboBox_Group.DisplayMember = "GroupStation";
                    comboBox_Group.DataSource = dt;

                }
            }


            comboBox_Group.SelectedValue = "P003";
        }


        private void btReport_Click(object sender, EventArgs e)
        {
            //btReport.Enabled = true;
            btExportPDF.Enabled = true;
            button1.Enabled = true;

            newProgressbar();
            invoke_Progress("กำลังประมวลผลข้อมูล...กรุณารอสักครู่...");

            prpDateSend = dateTimePicker_SendDate.Value.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("th-TH"));


            string van = txtVAN.Text.Trim();
            string[] parts = van.Split(',');

            string xsql = "delete tmp_xVAN_SUM_SO_JOB";
            JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, xsql);
            foreach (string part in parts)
            {

                xsql = "Insert into tmp_xVAN_SUM_SO_JOB values('" + part + "')";
                JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, xsql);
            }


            string sql = "SP_Report_JOBCoverSO";
            try
            {

                using (SqlConnection connection = new SqlConnection(strConn))
                {
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Connection.Open();
                        command.CommandTimeout = 6000;
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Clear();
                        command.Parameters.AddWithValue("@MMYYCloseDate", dateTimePicker1.Text);

                        command.ExecuteNonQuery();
                    }
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR..." + ex.Message);
            }


            ShowReport1("");
            ShowReport2("");

            closeProgress();
        }

        string prPTTContact = "";
        string prpDateSend = "";

        string prpThaiBath = "";
        string pSumPrices = "";
        string pVAT = "";
        string pTaotalPrice = "";

        string ppTotalJOB = "";

        private void ShowReport1(string mode)
        {

            string sql = "";

            string van = "";
            string[] parts = null;
            string van2 = "";


            if (mode == "")
            {

                invoke_Progress("ดึงข้อมูลสรุป ใบเสอนราคา");

                #region Query data to CrystalReport
                sql = " Select  count(*) cTotalJob, " +
                             "      SUM(sSumPrices )sSumPrices, " +//------------------------------------------------มูลค่าสินค้า
                             "      (SUM(sSumPrices )* 0.07) sVAT, " +//ภาษี
                             "      SUM(sSumPrices )+(SUM(sSumPrices )* 0.07) sTotalPrices" +//---------------------มูลค่าสินค้าทั้งสิ้น
                             " From TMP_vw_Print_SUM_SO_JOB_Part_View";
                SqlDataReader dr2 = JaSqlHelper.ExecuteReader(strConn, CommandType.Text, sql);
                {


                    dr2.Read();

                    thaiBath = new SMSMINI.DAL.ThaiPath();

                    ppTotalJOB = dr2["cTotalJob"].ToString();//-----------------------------------------------------ส่งสรุปงานทั้งหมด
                    prpThaiBath = thaiBath.ThaiBaht(dr2["sTotalPrices"].ToString());//------------------------------มูลค่าสินค้าทั้งสิ้น...ภาษาไทย
                    pSumPrices = double.Parse(dr2["sSumPrices"].ToString()).ToString("C").Replace("฿", "");//-------มูลค่าสินค้า

                    pVAT = double.Parse(dr2["sVAT"].ToString()).ToString("C").Replace("฿", "");//-------------------ภาษี
                    pTaotalPrice = double.Parse(dr2["sTotalPrices"].ToString()).ToString("C").Replace("฿", "");//---มูลค่าสินค้าทั้งสิ้น

                };

                van = txtVAN.Text.Trim();
                parts = van.Split(',');
                van2 = "";
                foreach (string part in parts)
                {
                    van2 += "'" + part + "',";
                }

                van2 = van2.Substring(0, van2.Length - 1);






                using (System.Data.SqlClient.SqlConnection Conn = new System.Data.SqlClient.SqlConnection(strConn))
                {

                    sql = " select * from TMP_vw_Print_SUM_SO_JOB_Part_View";

                    using (SqlDataAdapter da = new SqlDataAdapter(sql, Conn))
                    {
                        DataSet ds = new DataSet();

                        da.SelectCommand.CommandTimeout = 300;
                        da.Fill(ds);


                        rptSO.SetDatabaseLogon(strUsr, strPwd, strServer, strDB);
                        rptSO.SetDataSource(ds.Tables[0].DefaultView);

                        rptSO.SetParameterValue("PTTContact", prPTTContact);//----------ผู้ติดต่อ
                        rptSO.SetParameterValue("pDateSend", prpDateSend);//------------ส่งเอกสารวันที่

                        rptSO.SetParameterValue("pTotalJOB", ppTotalJOB);//-------------สรุปงานทั้งหมด
                        rptSO.SetParameterValue("pThaiBath", prpThaiBath);//------------เงินภาษาไทย

                        rptSO.SetParameterValue("pSumPrices", pSumPrices);//------------มูลค่าสินค้า
                        rptSO.SetParameterValue("pVAT", pVAT);//------------------------ภาษี
                        rptSO.SetParameterValue("pTaotalPrice", pTaotalPrice);//--------มูลค่าสินค้าทั้งสิ้น

                        crystalReportViewer1.ReportSource = rptSO;
                        crystalReportViewer1.Refresh();





                        //if (mode == "")
                        //{
                        //    crystalReportViewer1.ReportSource = rpt;
                        //    crystalReportViewer1.Refresh();
                        //}
                        //else
                        //{

                        //    Export2PDF(mode, rpt);


                        //}

                    }


                }

                #endregion

                invoke_Progress("ดึงข้อมูลสรุป ใบเสอนราคา เรียบร้อย...");
            }
            else
            {

                Export2PDF(mode, rptSO);


            }


        }

        private void ShowReport2(string mode)
        {

            string van = "";
            string[] parts = null;
            string van2 = "";

            string pMonth = "";
            string pTotalThai = "";
            string pSumTotalPart = "";

            string sql = "";


            if (mode == "")
            {
                invoke_Progress("ดึงข้อมูลสรุป อะไหล่...");

                #region Query data to CrystalReport

                van = txtVAN.Text.Trim();
                parts = van.Split(',');
                van2 = "";

                foreach (string part in parts)
                {
                    van2 += "'" + part + "',";
                }

                van2 = van2.Substring(0, van2.Length - 1);

                pMonth = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dateTimePicker1.Value.Month) + " " +
                    dateTimePicker1.Value.ToString("yyyy", new CultureInfo("th-TH"));

                pTotalThai = "";
                pSumTotalPart = "";

                sql = " select sum(sPrice)as sumsPrice from TMP_vw_Print_SUM_SO_JOB2_ByPart_Part;";

                SqlDataReader dr2 = JaSqlHelper.ExecuteReader(strConn, CommandType.Text, sql);
                {

                    dr2.Read();

                    thaiBath = new SMSMINI.DAL.ThaiPath();


                    pTotalThai = thaiBath.ThaiBaht(dr2["sumsPrice"].ToString());//------------------------------มูลค่าสินค้าทั้งสิ้น...ภาษาไทย
                    pSumTotalPart = double.Parse(dr2["sumsPrice"].ToString()).ToString("C").Replace("฿", "");//-------มูลค่าสินค้า


                };






                using (System.Data.SqlClient.SqlConnection Conn = new System.Data.SqlClient.SqlConnection(strConn))
                {

                    sql = " select * from TMP_vw_Print_SUM_SO_JOB2_ByPart;" +
                          " select * from TMP_vw_Print_SUM_SO_JOB2_ByPart_Part;";


                    using (SqlDataAdapter da = new SqlDataAdapter(sql, Conn))
                    {
                        da.SelectCommand.CommandTimeout = 300;
                        DataSet ds = new DataSet();
                        da.Fill(ds);



                        rptSO_SumPart.SetDatabaseLogon(strUsr, strPwd, strServer, strDB);

                        rptSO_SumPart.SetDataSource(ds.Tables[0].DefaultView);
                        rptSO_SumPart.Subreports["suSO_Part"].Database.Tables[0].SetDataSource(ds.Tables[1].DefaultView);

                        rptSO_SumPart.SetParameterValue("pZoneService", comboBox1.Text);//---------------ผู้ติดต่อ
                        rptSO_SumPart.SetParameterValue("pMonth", pMonth);//-----------------------------ผู้ติดต่อ
                        rptSO_SumPart.SetParameterValue("pTotalThai", pTotalThai);//---------------------ผู้ติดต่อ

                        rptSO_SumPart.SetParameterValue("pSumTotalPart", pSumTotalPart);//---------------ผู้ติดต่อ
                        rptSO_SumPart.SetParameterValue("pUserCreateRep", UserInfo.FullName);//----------ผู้ติดต่อ

                        rptSO_SumPart.SetParameterValue("pSMG", "นายจตุรภัทร รัตนจริยาคุณ");//------------------ผู้ติดต่อ

                        rptSO_SumPart.SetParameterValue("pPTTUser", prPTTContact);//---------------------ผู้ติดต่อ

                        crystalReportViewer2.ReportSource = rptSO_SumPart;
                        crystalReportViewer2.Refresh();

                        //if (mode == "")
                        //{
                        //    crystalReportViewer2.ReportSource = rpt_part;
                        //    crystalReportViewer2.Refresh();
                        //}
                        //else
                        //{

                        //    Export2PDF2(mode, rpt_part);
                        //}



                    }

                }

                #endregion

                invoke_Progress("ดึงข้อมูลสรุป อะไหล่ เรียบร้อย...");

            }
            else
            {

                Export2PDF2(mode, rptSO_SumPart);
            }


        }

        string sv = "";
        private void Export2PDF(string mode, Report.rptReportCoverSO rpt)
        {
            string f = "";
            SaveFileDialog DialogSave = new SaveFileDialog();
            DialogSave.DefaultExt = "pdf";
            DialogSave.Filter = "Adobe Acrobat  (*.pdf)|*.pdf|All files (*.*)|*.*";
            DialogSave.AddExtension = true;
            DialogSave.RestoreDirectory = true;
            DialogSave.Title = "Where do you want to save the file?";
            DialogSave.InitialDirectory = @"C:/";


            sv = "ReportCoverSO_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".pdf";

            DialogSave.FileName = sv;

            if (DialogSave.ShowDialog() == DialogResult.OK)
            {
                f = DialogSave.FileName.ToString();
                DialogSave.Dispose();
                DialogSave = null;

                try
                {
                    ExportOptions CrExportOptions = new ExportOptions();
                    DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                    PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();
                    CrDiskFileDestinationOptions.DiskFileName = f;// "c:\\ServicesReport_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".pdf";


                    CrExportOptions = rpt.ExportOptions;
                    {
                        CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                        CrExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                        CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
                        CrExportOptions.FormatOptions = CrFormatTypeOptions;

                    }
                    rpt.Export();
                    //rpt.Close();

                    if (System.IO.File.Exists(f))
                    {
                        System.Diagnostics.Process.Start(f);
                    }

                    //this.Close();
                }

                catch (Exception ex)
                {
                    MessageBox.Show("ข้อผิดพลาด " + ex.ToString(), "เกิดข้อผิดพลาดในการ Export ไฟล์ SV PDF");
                }
            }// if (DialogSave.ShowDialog() == DialogResult.OK)

        }

        private void Export2PDF2(string mode, Report.rptReportCoverSO_SumPart rpt_part)
        {
            string f = "";
            SaveFileDialog DialogSave = new SaveFileDialog();
            DialogSave.DefaultExt = "pdf";
            DialogSave.Filter = "Adobe Acrobat  (*.pdf)|*.pdf|All files (*.*)|*.*";
            DialogSave.AddExtension = true;
            DialogSave.RestoreDirectory = true;
            DialogSave.Title = "Where do you want to save the file?";
            DialogSave.InitialDirectory = @"C:/";


            sv = "ReportCoverSO_SumPart_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".pdf";

            DialogSave.FileName = sv;

            if (DialogSave.ShowDialog() == DialogResult.OK)
            {
                f = DialogSave.FileName.ToString();
                DialogSave.Dispose();
                DialogSave = null;

                try
                {
                    ExportOptions CrExportOptions = new ExportOptions();
                    DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                    PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();
                    CrDiskFileDestinationOptions.DiskFileName = f;// "c:\\ServicesReport_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".pdf";


                    CrExportOptions = rpt_part.ExportOptions;
                    {
                        CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                        CrExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                        CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
                        CrExportOptions.FormatOptions = CrFormatTypeOptions;

                    }
                    rpt_part.Export();
                    //rpt_part.Close();

                    if (System.IO.File.Exists(f))
                    {
                        System.Diagnostics.Process.Start(f);
                    }

                    //this.Close();
                }

                catch (Exception ex)
                {
                    MessageBox.Show("ข้อผิดพลาด " + ex.ToString(), "เกิดข้อผิดพลาดในการ Export ไฟล์ SV PDF");
                }
            }// if (DialogSave.ShowDialog() == DialogResult.OK)

        }



        private void btExportPDF_Click(object sender, EventArgs e)
        {
            ShowReport1("PDF");
            ShowReport2("PDF");
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (fMode == "")
            {
                btReport.Enabled = true;
                btExportPDF.Enabled = false;

                string sql = " SELECT v.[VAN_ID] " +
                             "   FROM [VAN_ServiceZone] v join [ServiceZone] s " +
                             "      On v.[ZON_ID] = s.[ZON_ID] " +
                             "      where s.[ZON_ID] = " + comboBox1.SelectedValue.ToString() + "";

                SqlDataReader dr = JaSqlHelper.ExecuteReader(strConn, CommandType.Text, sql);
                {

                    string van = "";
                    //dr.Read();
                    //foreach (DbDataRecord c in dr)
                    //{
                    //    van += c["VAN_ID"] + ",";
                    //}

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            van += dr["VAN_ID"] + ",";
                        }
                    }



                    string[] parts = van.Split(',');
                    string van2 = "";
                    foreach (string part in parts)
                    {
                        if (part != "")
                            van2 += part + ",";
                    }

                    txtVAN.Text = van2.Substring(0, van2.Length - 1);

                }

                sql = "SELECT top 1 [ZON_ID] ,[PTTContact] FROM  [ServiceZone] where [ZON_ID] =" + comboBox1.SelectedValue.ToString() + "";
                SqlDataReader dr2 = JaSqlHelper.ExecuteReader(strConn, CommandType.Text, sql);
                {

                    dr2.Read();
                    prPTTContact = dr2["PTTContact"].ToString();
                };
            }



        }


        private void button1_Click(object sender, EventArgs e)
        {
            /* rptReportCoverSO rptSO;
        rptReportCoverSO_SumPart rptSO_SumPart;
*/
            ExportExcel1();
            ExportExcel2();


        }

        private void ExportExcel1()
        {
            string f = "";
            SaveFileDialog DialogSave = new SaveFileDialog();
            DialogSave.DefaultExt = "xls";
            DialogSave.Filter = "Execl 2003 (*.xls)|*.xls|All files (*.*)|*.*";
            DialogSave.AddExtension = true;
            DialogSave.RestoreDirectory = true;
            DialogSave.Title = "Where do you want to save the file?";
            DialogSave.InitialDirectory = @"C:/";


            sv = "ReportCoverSO_SumPart_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".xls";

            DialogSave.FileName = sv;

            if (DialogSave.ShowDialog() == DialogResult.OK)
            {
                f = DialogSave.FileName.ToString();
                DialogSave.Dispose();
                DialogSave = null;

                try
                {


                    ExportOptions CrExportOptions = new ExportOptions();

                    DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                    ExcelFormatOptions CrFormatTypeOptions = new ExcelFormatOptions();
                    CrDiskFileDestinationOptions.DiskFileName = f;

                    CrExportOptions = rptSO.ExportOptions;
                    {
                        CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                        CrExportOptions.ExportFormatType = ExportFormatType.Excel;
                        CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
                        CrExportOptions.FormatOptions = CrFormatTypeOptions;
                    }
                    rptSO.Export();
                    //rptSO.Close();

                    if (System.IO.File.Exists(f))
                    {
                        System.Diagnostics.Process.Start(f);
                    }

                    //this.Close();
                }

                catch (Exception ex)
                {
                    MessageBox.Show("ข้อผิดพลาด " + ex.ToString(), "เกิดข้อผิดพลาดในการ Export ไฟล์ Excel");
                }
            }// if (DialogSave.ShowDialog() == DialogResult.OK)
        }


        private void ExportExcel2()
        {
            string f = "";
            SaveFileDialog DialogSave = new SaveFileDialog();
            DialogSave.DefaultExt = "xls";
            DialogSave.Filter = "Execl 2003 (*.xls)|*.xls|All files (*.*)|*.*";
            DialogSave.AddExtension = true;
            DialogSave.RestoreDirectory = true;
            DialogSave.Title = "Where do you want to save the file?";
            DialogSave.InitialDirectory = @"C:/";


            sv = "ReportCoverSO_SumPart_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".xls";

            DialogSave.FileName = sv;

            if (DialogSave.ShowDialog() == DialogResult.OK)
            {
                f = DialogSave.FileName.ToString();
                DialogSave.Dispose();
                DialogSave = null;

                try
                {


                    ExportOptions CrExportOptions = new ExportOptions();


                    DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                    ExcelFormatOptions CrFormatTypeOptions = new ExcelFormatOptions();
                    CrDiskFileDestinationOptions.DiskFileName = f;

                    CrExportOptions = rptSO_SumPart.ExportOptions;
                    {
                        CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                        CrExportOptions.ExportFormatType = ExportFormatType.Excel;
                        CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
                        CrExportOptions.FormatOptions = CrFormatTypeOptions;
                    }

                    rptSO_SumPart.Export();
                    //rptSO_SumPart.Close();

                    if (System.IO.File.Exists(f))
                    {
                        System.Diagnostics.Process.Start(f);
                    }

                    //this.Close();
                }

                catch (Exception ex)
                {
                    MessageBox.Show("ข้อผิดพลาด " + ex.ToString(), "เกิดข้อผิดพลาดในการ Export ไฟล์ Excel");
                }
            }// if (DialogSave.ShowDialog() == DialogResult.OK)
        }

        private void invoke_Progress(string strMsg)
        {
            try
            {
                m_fmProgress.lblDescription.Invoke(
                   (MethodInvoker)delegate ()
                   {
                       m_fmProgress.lblDescription.Text = strMsg;
                   }
                   );
            }
            catch (System.Exception)
            { }
        }

        private void newProgressbar()
        {
            m_fmProgress = new ProgressBar.FormProgress();
            backgroundWorker1.RunWorkerAsync();
            System.Threading.Thread.Sleep(3000);
        }

        private void closeProgress()
        {
            if (m_fmProgress != null)
            {
                m_fmProgress.Invoke(
                    (MethodInvoker)delegate ()
                    {
                        m_fmProgress.Hide();
                        m_fmProgress.Dispose();
                        m_fmProgress = null;
                    }
                );
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            m_fmProgress.Activate();
            m_fmProgress.BringToFront();
            m_fmProgress.TopMost = true;
            m_fmProgress.ShowDialog();
        }


        private void PrintReportCoverSO_FormClosing(object sender, FormClosingEventArgs e)
        {
            rptSO.Close();
            rptSO_SumPart.Close();
        }



    }
}
