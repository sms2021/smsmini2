﻿namespace SMSMINI.Report
{
    partial class Report_PartPricesList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Report_PartPricesList));
            this.crystalReportViewer1 = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.toolStrip_Mnu = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel_cobby = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripText_txtFind = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStrip_txtTQDate = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_FindStation = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_Save = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_Cancel = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_Exit = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel_Dateime = new System.Windows.Forms.ToolStripLabel();
            this.toolStrip_Mnu.SuspendLayout();
            this.SuspendLayout();
            // 
            // crystalReportViewer1
            // 
            this.crystalReportViewer1.ActiveViewIndex = -1;
            this.crystalReportViewer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.crystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crystalReportViewer1.Cursor = System.Windows.Forms.Cursors.Default;
            this.crystalReportViewer1.Location = new System.Drawing.Point(3, 52);
            this.crystalReportViewer1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.crystalReportViewer1.Name = "crystalReportViewer1";
            this.crystalReportViewer1.SelectionFormula = "";
            this.crystalReportViewer1.Size = new System.Drawing.Size(1415, 526);
            this.crystalReportViewer1.TabIndex = 0;
            this.crystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            this.crystalReportViewer1.ToolPanelWidth = 267;
            this.crystalReportViewer1.ViewTimeSelectionFormula = "";
            // 
            // toolStrip_Mnu
            // 
            this.toolStrip_Mnu.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip_Mnu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel_cobby,
            this.toolStripSeparator4,
            this.toolStripText_txtFind,
            this.toolStripSeparator2,
            this.toolStripLabel1,
            this.toolStrip_txtTQDate,
            this.toolStripSeparator5,
            this.toolStripButton_FindStation,
            this.toolStripSeparator3,
            this.toolStripButton_Save,
            this.toolStripButton_Cancel,
            this.toolStripSeparator1,
            this.toolStripButton_Exit,
            this.toolStripLabel_Dateime});
            this.toolStrip_Mnu.Location = new System.Drawing.Point(0, 0);
            this.toolStrip_Mnu.Name = "toolStrip_Mnu";
            this.toolStrip_Mnu.Size = new System.Drawing.Size(1421, 39);
            this.toolStrip_Mnu.TabIndex = 3;
            // 
            // toolStripLabel_cobby
            // 
            this.toolStripLabel_cobby.AutoSize = false;
            this.toolStripLabel_cobby.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.toolStripLabel_cobby.ForeColor = System.Drawing.SystemColors.Desktop;
            this.toolStripLabel_cobby.Items.AddRange(new object[] {
            "ตาม Part no",
            "ตาม Part name",
            "ตาม กลุ่มบริษัท",
            "ตาม ไตรมาส (QT)",
            "ทั้งหมด"});
            this.toolStripLabel_cobby.Name = "toolStripLabel_cobby";
            this.toolStripLabel_cobby.Size = new System.Drawing.Size(239, 24);
            this.toolStripLabel_cobby.Text = "ค้นหาข้อมูล...";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripText_txtFind
            // 
            this.toolStripText_txtFind.AutoSize = false;
            this.toolStripText_txtFind.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.toolStripText_txtFind.ForeColor = System.Drawing.SystemColors.Desktop;
            this.toolStripText_txtFind.Name = "toolStripText_txtFind";
            this.toolStripText_txtFind.Size = new System.Drawing.Size(199, 24);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(123, 36);
            this.toolStripLabel1.Text = "วันที่ (yyyymmdd):";
            // 
            // toolStrip_txtTQDate
            // 
            this.toolStrip_txtTQDate.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.toolStrip_txtTQDate.Name = "toolStrip_txtTQDate";
            this.toolStrip_txtTQDate.Size = new System.Drawing.Size(199, 39);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripButton_FindStation
            // 
            this.toolStripButton_FindStation.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_FindStation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.toolStripButton_FindStation.Image = global::SMSMINI.Properties.Resources.Find48;
            this.toolStripButton_FindStation.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_FindStation.Name = "toolStripButton_FindStation";
            this.toolStripButton_FindStation.Size = new System.Drawing.Size(36, 36);
            this.toolStripButton_FindStation.ToolTipText = "ค้นหาข้อมูล";
            this.toolStripButton_FindStation.Click += new System.EventHandler(this.toolStripButton_FindStation_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripButton_Save
            // 
            this.toolStripButton_Save.Enabled = false;
            this.toolStripButton_Save.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.toolStripButton_Save.ForeColor = System.Drawing.SystemColors.Desktop;
            this.toolStripButton_Save.Image = global::SMSMINI.Properties.Resources.print_f2;
            this.toolStripButton_Save.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Save.Name = "toolStripButton_Save";
            this.toolStripButton_Save.Size = new System.Drawing.Size(154, 36);
            this.toolStripButton_Save.Text = "รายงาน Prices List";
            this.toolStripButton_Save.ToolTipText = "รายงาน Prices List";
            this.toolStripButton_Save.Click += new System.EventHandler(this.toolStripButton_Save_Click);
            // 
            // toolStripButton_Cancel
            // 
            this.toolStripButton_Cancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.toolStripButton_Cancel.ForeColor = System.Drawing.SystemColors.Desktop;
            this.toolStripButton_Cancel.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Cancel.Image")));
            this.toolStripButton_Cancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Cancel.Name = "toolStripButton_Cancel";
            this.toolStripButton_Cancel.Size = new System.Drawing.Size(87, 36);
            this.toolStripButton_Cancel.Text = "Cancel";
            this.toolStripButton_Cancel.ToolTipText = "ยกเลิกข้อมูล";
            this.toolStripButton_Cancel.Click += new System.EventHandler(this.toolStripButton_Cancel_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripButton_Exit
            // 
            this.toolStripButton_Exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.toolStripButton_Exit.ForeColor = System.Drawing.SystemColors.Desktop;
            this.toolStripButton_Exit.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Exit.Image")));
            this.toolStripButton_Exit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Exit.Name = "toolStripButton_Exit";
            this.toolStripButton_Exit.Size = new System.Drawing.Size(66, 36);
            this.toolStripButton_Exit.Text = "Exit";
            this.toolStripButton_Exit.ToolTipText = "ออกจากหน้าจอ";
            this.toolStripButton_Exit.Click += new System.EventHandler(this.toolStripButton_Exit_Click);
            // 
            // toolStripLabel_Dateime
            // 
            this.toolStripLabel_Dateime.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripLabel_Dateime.Font = new System.Drawing.Font("Tahoma", 10F);
            this.toolStripLabel_Dateime.ForeColor = System.Drawing.SystemColors.Desktop;
            this.toolStripLabel_Dateime.Name = "toolStripLabel_Dateime";
            this.toolStripLabel_Dateime.Size = new System.Drawing.Size(0, 36);
            // 
            // Report_PartPricesList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1421, 581);
            this.Controls.Add(this.toolStrip_Mnu);
            this.Controls.Add(this.crystalReportViewer1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Report_PartPricesList";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Part Prices List";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Report_PartPricesList_FormClosed);
            this.Load += new System.EventHandler(this.Report_PartPricesList_Load);
            this.toolStrip_Mnu.ResumeLayout(false);
            this.toolStrip_Mnu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CrystalDecisions.Windows.Forms.CrystalReportViewer crystalReportViewer1;
        private System.Windows.Forms.ToolStrip toolStrip_Mnu;
        private System.Windows.Forms.ToolStripComboBox toolStripLabel_cobby;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripTextBox toolStripText_txtFind;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripButton_FindStation;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton toolStripButton_Save;
        private System.Windows.Forms.ToolStripButton toolStripButton_Cancel;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButton_Exit;
        private System.Windows.Forms.ToolStripLabel toolStripLabel_Dateime;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripTextBox toolStrip_txtTQDate;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
    }
}