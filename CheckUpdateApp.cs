﻿using System;
using System.Deployment.Application;
using System.Windows.Forms;

namespace SMSMINI
{
    public static class CheckUpdateApp
    {
        public static void checkUpdate(bool formLoad)
        {
            UpdateCheckInfo info = null;

            if (!ApplicationDeployment.IsNetworkDeployed)
            {
                if (formLoad) return;
                MessageBox.Show("This application was not deployed using ClickOnce", "Check for updates", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                ApplicationDeployment updateCheck = ApplicationDeployment.CurrentDeployment;

                try
                {
                    info = updateCheck.CheckForDetailedUpdate();

                }
                catch (DeploymentDownloadException dde)
                {
                    MessageBox.Show("The new version of the application cannot be downloaded at this time. \n\nPlease check your network connection, or try again later. Error: " + dde.Message);
                    return;
                }
                catch (InvalidDeploymentException ide)
                {
                    MessageBox.Show("Cannot check for a new version of the application. The ClickOnce deployment is corrupt. Please redeploy the application and try again. Error: " + ide.Message);
                    return;
                }
                catch (InvalidOperationException ioe)
                {
                    MessageBox.Show("This application cannot be updated. It is likely not a ClickOnce application. Error: " + ioe.Message);
                    return;
                }


                if (info.UpdateAvailable)
                {
                    try
                    {
                        updateCheck.Update();
                        MessageBox.Show("The application has been upgraded, and will now restart.");
                        Application.Restart();
                    }
                    catch (DeploymentDownloadException dde)
                    {
                        MessageBox.Show("Cannot install the latest version of the application. \n\nPlease check your network connection, or try again later. Error: " + dde);
                        return;
                    }
                }
                else
                {
                    if (formLoad) return;
                    MessageBox.Show("No update is currently available", "Check for updates", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }
    }
}
