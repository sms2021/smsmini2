﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace SMSMINI.FFA
{
    public partial class FindStation : Form
    {
        public string findType { get; set; }

        public string jobID { get; set; }

        public string staID { get; set; }
        public string staName { get; set; }

        public FindStation()
        {
            InitializeComponent();
        }

        private void FindStation_Load(object sender, EventArgs e)
        {
            using (var dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            {
                if (findType == "JOB")
                {
                    var job = dc.JOBs
                          .Where(t => t.JOB_ID.Contains(jobID))
                          .OrderBy(t => t.JOB_ID)
                          .ToList();
                    dataGridView1.DataSource = job;
                }
                else
                {

                    var st = dc.vw_Station_Details
                        .Where(t => t.STA_ID.Contains(staID) || t.StationSys.Contains(staID))
                        .OrderBy(t => t.STA_ID)
                        .ToList();
                    dataGridView1.DataSource = st;
                }

            }
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex != -1)
                {
                    if (findType == "JOB")
                    {
                        jobID = dataGridView1["JOB_ID", e.RowIndex].Value.ToString();
                    }
                    else
                    {
                        staID = dataGridView1["STA_ID", e.RowIndex].Value.ToString();
                        staName = dataGridView1["StationSys", e.RowIndex].Value.ToString();
                    }
                }

                DialogResult = DialogResult.OK;
            }
            catch (Exception)
            {

                DialogResult = DialogResult.Cancel;
            }
        }
    }
}
