﻿namespace SMSMINI.FFA
{
    partial class ControlFFA
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ControlFFA));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel_Dateime = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.toolStrip_Mnu = new System.Windows.Forms.ToolStrip();
            this.saveToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.exitToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btCanReserv = new System.Windows.Forms.Button();
            this.lbSN = new System.Windows.Forms.Label();
            this.txtRemark = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTranEmp = new System.Windows.Forms.TextBox();
            this.lbReserv = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtStation = new System.Windows.Forms.TextBox();
            this.cbActStatus = new System.Windows.Forms.ComboBox();
            this.lbStatus = new System.Windows.Forms.Label();
            this.btSelect = new System.Windows.Forms.Button();
            this.txtSN = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.toolStrip_Mnu.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripLabel_Dateime
            // 
            this.toolStripLabel_Dateime.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripLabel_Dateime.Font = new System.Drawing.Font("Tahoma", 10F);
            this.toolStripLabel_Dateime.ForeColor = System.Drawing.SystemColors.Desktop;
            this.toolStripLabel_Dateime.Name = "toolStripLabel_Dateime";
            this.toolStripLabel_Dateime.Size = new System.Drawing.Size(0, 36);
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStrip_Mnu
            // 
            this.toolStrip_Mnu.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip_Mnu.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip_Mnu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator2,
            this.toolStripLabel_Dateime,
            this.saveToolStripButton,
            this.toolStripSeparator,
            this.exitToolStripButton});
            this.toolStrip_Mnu.Location = new System.Drawing.Point(2, 1);
            this.toolStrip_Mnu.Name = "toolStrip_Mnu";
            this.toolStrip_Mnu.Size = new System.Drawing.Size(152, 39);
            this.toolStrip_Mnu.TabIndex = 41;
            // 
            // saveToolStripButton
            // 
            this.saveToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("saveToolStripButton.Image")));
            this.saveToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveToolStripButton.Name = "saveToolStripButton";
            this.saveToolStripButton.Size = new System.Drawing.Size(67, 36);
            this.saveToolStripButton.Text = "&Save";
            this.saveToolStripButton.Click += new System.EventHandler(this.saveToolStripButton_Click);
            // 
            // exitToolStripButton
            // 
            this.exitToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("exitToolStripButton.Image")));
            this.exitToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.exitToolStripButton.Name = "exitToolStripButton";
            this.exitToolStripButton.Size = new System.Drawing.Size(61, 36);
            this.exitToolStripButton.Text = "E&xit";
            this.exitToolStripButton.Click += new System.EventHandler(this.exitToolStripButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btCanReserv);
            this.groupBox1.Controls.Add(this.lbSN);
            this.groupBox1.Controls.Add(this.txtRemark);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtTranEmp);
            this.groupBox1.Controls.Add(this.lbReserv);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtStation);
            this.groupBox1.Controls.Add(this.cbActStatus);
            this.groupBox1.Controls.Add(this.lbStatus);
            this.groupBox1.Controls.Add(this.btSelect);
            this.groupBox1.Controls.Add(this.txtSN);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.groupBox1.Location = new System.Drawing.Point(7, 43);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(850, 117);
            this.groupBox1.TabIndex = 42;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ระบุรายละเอียด";
            // 
            // btCanReserv
            // 
            this.btCanReserv.BackColor = System.Drawing.Color.SandyBrown;
            this.btCanReserv.Location = new System.Drawing.Point(441, 59);
            this.btCanReserv.Name = "btCanReserv";
            this.btCanReserv.Size = new System.Drawing.Size(99, 23);
            this.btCanReserv.TabIndex = 49;
            this.btCanReserv.Text = "ยกเลิกการจอง";
            this.btCanReserv.UseVisualStyleBackColor = false;
            this.btCanReserv.Click += new System.EventHandler(this.btCanReserv_Click);
            // 
            // lbSN
            // 
            this.lbSN.AutoSize = true;
            this.lbSN.ForeColor = System.Drawing.Color.Red;
            this.lbSN.Location = new System.Drawing.Point(37, 18);
            this.lbSN.Name = "lbSN";
            this.lbSN.Size = new System.Drawing.Size(82, 13);
            this.lbSN.TabIndex = 48;
            this.lbSN.Text = "SerialNumber";
            // 
            // txtRemark
            // 
            this.txtRemark.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtRemark.Location = new System.Drawing.Point(356, 34);
            this.txtRemark.Name = "txtRemark";
            this.txtRemark.Size = new System.Drawing.Size(277, 20);
            this.txtRemark.TabIndex = 46;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label4.Location = new System.Drawing.Point(353, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(110, 16);
            this.label4.TabIndex = 47;
            this.label4.Text = "หมายเหตุ/รายละเอียด:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label2.Location = new System.Drawing.Point(542, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 16);
            this.label2.TabIndex = 45;
            this.label2.Text = "ผู้บันทึก:";
            // 
            // txtTranEmp
            // 
            this.txtTranEmp.Enabled = false;
            this.txtTranEmp.Location = new System.Drawing.Point(546, 83);
            this.txtTranEmp.Name = "txtTranEmp";
            this.txtTranEmp.Size = new System.Drawing.Size(87, 20);
            this.txtTranEmp.TabIndex = 44;
            // 
            // lbReserv
            // 
            this.lbReserv.AutoSize = true;
            this.lbReserv.ForeColor = System.Drawing.Color.Red;
            this.lbReserv.Location = new System.Drawing.Point(78, 66);
            this.lbReserv.Name = "lbReserv";
            this.lbReserv.Size = new System.Drawing.Size(42, 13);
            this.lbReserv.TabIndex = 43;
            this.lbReserv.Text = "ไม่จอง";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label1.Location = new System.Drawing.Point(5, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 16);
            this.label1.TabIndex = 42;
            this.label1.Text = "จองเพื่อสถานี :";
            // 
            // txtStation
            // 
            this.txtStation.Location = new System.Drawing.Point(8, 83);
            this.txtStation.Name = "txtStation";
            this.txtStation.Size = new System.Drawing.Size(532, 20);
            this.txtStation.TabIndex = 41;
            this.txtStation.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtStation_KeyDown);
            // 
            // cbActStatus
            // 
            this.cbActStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.cbActStatus.FormattingEnabled = true;
            this.cbActStatus.Location = new System.Drawing.Point(207, 33);
            this.cbActStatus.Name = "cbActStatus";
            this.cbActStatus.Size = new System.Drawing.Size(141, 21);
            this.cbActStatus.TabIndex = 36;
            // 
            // lbStatus
            // 
            this.lbStatus.AutoSize = true;
            this.lbStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbStatus.Location = new System.Drawing.Point(204, 15);
            this.lbStatus.Name = "lbStatus";
            this.lbStatus.Size = new System.Drawing.Size(45, 16);
            this.lbStatus.TabIndex = 40;
            this.lbStatus.Text = "สถานะ :";
            // 
            // btSelect
            // 
            this.btSelect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btSelect.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.btSelect.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btSelect.Location = new System.Drawing.Point(663, 35);
            this.btSelect.Name = "btSelect";
            this.btSelect.Size = new System.Drawing.Size(157, 54);
            this.btSelect.TabIndex = 34;
            this.btSelect.Text = "เลือกรายการ";
            this.btSelect.UseVisualStyleBackColor = false;
            this.btSelect.Click += new System.EventHandler(this.btSelect_Click);
            // 
            // txtSN
            // 
            this.txtSN.Location = new System.Drawing.Point(8, 34);
            this.txtSN.Name = "txtSN";
            this.txtSN.Size = new System.Drawing.Size(188, 20);
            this.txtSN.TabIndex = 31;
            this.txtSN.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSN_KeyDown);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label7.Location = new System.Drawing.Point(5, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 16);
            this.label7.TabIndex = 4;
            this.label7.Text = "S/N :";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.Location = new System.Drawing.Point(7, 166);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridView1.Size = new System.Drawing.Size(850, 257);
            this.dataGridView1.TabIndex = 43;
            this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
            // 
            // ControlFFA
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(864, 435);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.toolStrip_Mnu);
            this.Name = "ControlFFA";
            this.ShowIcon = false;
            this.Text = "ควบคุม Fixed Asset";
            this.Load += new System.EventHandler(this.ControlFFA_Load);
            this.toolStrip_Mnu.ResumeLayout(false);
            this.toolStrip_Mnu.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripLabel toolStripLabel_Dateime;
        private System.Windows.Forms.ToolStripButton saveToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripButton exitToolStripButton;
        private System.Windows.Forms.ToolStrip toolStrip_Mnu;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cbActStatus;
        private System.Windows.Forms.Label lbStatus;
        private System.Windows.Forms.Button btSelect;
        private System.Windows.Forms.Label lbReserv;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtStation;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox txtSN;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtTranEmp;
        private System.Windows.Forms.TextBox txtRemark;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbSN;
        private System.Windows.Forms.Button btCanReserv;

    }
}