﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;


namespace SMSMINI.FFA
{
    public partial class ChangeModelFFA : Form
    {
        List<FFASelect> LstFFASelect = null;
        DateTime servDate = DateTime.Now;


        public ChangeModelFFA()
        {
            InitializeComponent();
        }

        private void ChangeModelFFA_Load(object sender, EventArgs e)
        {
            LstFFASelect = new List<FFASelect>();
            if (string.IsNullOrEmpty(txtSNo.Text))
            {
                btnDelete.Enabled = false;
            }
            else
            {
                btnDelete.Enabled = true;
            }

            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            {
                cbxFCATIDo.DataSource = dc.FFixedAsset_Categories
                         .Where(t => t.IsCancel == false)
                         .Select(t => new
                         {
                             cateID = t.FCAT_ID,
                             cateName = t.FCAT_ID.ToString() + ": " + t.FixedAsset_CateName
                         })
                         .ToList();
                cbxFCATIDo.DisplayMember = "cateName";
                cbxFCATIDo.ValueMember = "cateID";

                cbxFBAIDo.DataSource = dc.FFixedAsset_Brands

                    .Select(t => new
                    {
                        brandID = t.FBA_ID,
                        brand = t.FBA_ID.ToString() + ": " + t.FBrand
                    })
                    .ToList();
                cbxFBAIDo.DisplayMember = "brand";
                cbxFBAIDo.ValueMember = "brandID";

                cbxFMDIDo.DataSource = dc.FFixedAsset_Models
                    .Select(t => new
                    {
                        modelID = t.FMD_ID,
                        FModel = t.FMD_ID.ToString() + ": " + t.FModel
                    })
                    .ToList();
                cbxFMDIDo.DisplayMember = "FModel";
                cbxFMDIDo.ValueMember = "modelID";
                //==============================================
                var c = dc.FFixedAsset_Categories
                .Where(t => t.IsCancel == false)
               .Select(t => new
               {
                   cateID = t.FCAT_ID.ToString(),
                   cateName = t.FCAT_ID.ToString() + ": " + t.FixedAsset_CateName
               }).ToList();

                c.Insert(0, new
                {
                    cateID = "0",
                    cateName = ""
                });

                cbxFCATIDc.DataSource = c;
                cbxFCATIDc.DisplayMember = "cateName";
                cbxFCATIDc.ValueMember = "cateID";

                //cbxFBAIDc.DataSource = dc.FFixedAsset_Brands

                //    .Select(t => new
                //    {
                //        brandID = t.FBA_ID,
                //        brand = t.FBA_ID.ToString() + ": " + t.FBrand
                //    })
                //    .ToList();
                //cbxFBAIDc.DisplayMember = "brand";
                //cbxFBAIDc.ValueMember = "brandID";

                //cbxFMDIDc.DataSource = dc.FFixedAsset_Models
                //    .Select(t => new
                //    {
                //        modelID = t.FMD_ID,
                //        FModel = t.FMD_ID.ToString() + ": " + t.FModel
                //    })
                //    .ToList();
                //cbxFMDIDc.DisplayMember = "FModel";
                //cbxFMDIDc.ValueMember = "modelID";

            }
        }

        private void toolStripButton_FindStation_Click(object sender, EventArgs e)
        {

            var f = new Find_changeModel();
            f.SNo = txtSNFind.Text.Trim();
            f.WindowState = FormWindowState.Normal;

            var result = f.ShowDialog();
            if (result == DialogResult.OK)
            {
                try
                {
                    txtSNo.Text = f.SNo;
                    cbxFCATIDo.SelectedValue = f.FCATIDo;
                    cbxFBAIDo.SelectedValue = f.FBAIDo;
                    cbxFMDIDo.SelectedValue = f.FMDIDo;

                    txtSNc.Text = f.SNo;
                    cbxFCATIDc.SelectedValue = f.FCATIDo.ToString();
                    cbxFBAIDc.SelectedValue = f.FBAIDo.ToString();
                    cbxFMDIDc.SelectedValue = f.FMDIDo.ToString();

                    if (string.IsNullOrEmpty(txtSNo.Text))
                    {
                        btnDelete.Enabled = false;
                    }
                    else
                    {
                        btnDelete.Enabled = true;
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("ข้อมูลประเภทอุปกรณ์ Fixed Asset ไม่ถูกต้อง  " + Environment.NewLine +
                        " อาจเกิดปัญหาตั้งแต่ขั้นตอนการ Import ข้อมูล Fixed Asset เข้าระบบ   " + Environment.NewLine +
                        " รบกวนตรวจสอบข้อมูลอีกครั้ง", "เกิดข้อผิดพลาด");

                }





            }

        }

        private void txtSNc_KeyDown(object sender, KeyEventArgs e)
        {
            var f = new Find_changeModel();
            f.SNo = txtSNc.Text.Trim();
            f.WindowState = FormWindowState.Normal;

            var result = f.ShowDialog();
            if (result == DialogResult.OK)
            {
                txtSNo.Text = f.SNo;
                cbxFCATIDo.SelectedValue = f.FCATIDo;
                cbxFBAIDo.SelectedValue = f.FBAIDo;
                cbxFMDIDo.SelectedValue = f.FMDIDo;


                txtSNc.Text = f.SNo;
                cbxFCATIDc.SelectedValue = f.FCATIDo.ToString();
                cbxFBAIDc.SelectedValue = f.FBAIDo.ToString();
                cbxFMDIDc.SelectedValue = f.FMDIDo.ToString();

            }
        }

        private void cbxFCATIDc_SelectedIndexChanged(object sender, EventArgs e)
        {
            //cbxFBAIDc.DataSource = dc.FFixedAsset_Brands

            //    .Select(t => new
            //    {
            //        brandID = t.FBA_ID,
            //        brand = t.FBA_ID.ToString() + ": " + t.FBrand
            //    })
            //    .ToList();
            //cbxFBAIDc.DisplayMember = "brand";
            //cbxFBAIDc.ValueMember = "brandID";

            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            {
                var b = dc.FFixedAsset_Brands
                             //.Where(t => (t.FCAT_ID == int.Parse(cbxFCATIDc.SelectedValue.ToString())))
                             .Where(t => (t.FCAT_ID.ToString() == cbxFCATIDc.SelectedValue.ToString()))
                              .Select(t => new
                              {
                                  brandID = t.FBA_ID.ToString(),
                                  brand = t.FBA_ID.ToString() + ": " + t.FBrand
                              }).ToList();
                b.Insert(0, new
                {
                    brandID = "0",
                    brand = ""
                });

                cbxFBAIDc.DataSource = b;
                cbxFBAIDc.DisplayMember = "brand";
                cbxFBAIDc.ValueMember = "brandID";
            }


        }

        private void cbxFCATIDc_SelectionChangeCommitted(object sender, EventArgs e)
        {
            //using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            //{

            //    cbxFBAIDc.DataSource = dc.FFixedAsset_Brands
            //        // .Where(t => (t.FCAT_ID == int.Parse(cbxFCATIDc.SelectedValue.ToString())
            //        .Where(t=>(t.FCAT_ID == 50
            //       // .Where(t => (t.FCAT_ID == vb
            //            ))
            //      .Select(t => new
            //      {
            //          brandID = t.FBA_ID,
            //          brand = t.FBA_ID.ToString() + ": " + t.FBrand
            //      })
            //      .ToList();
            //    cbxFBAIDc.DisplayMember = "brand";
            //    cbxFBAIDc.ValueMember = "brandID";

            //}
        }

        private void cbxFCATIDc_SelectedValueChanged(object sender, EventArgs e)
        {
            //using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            //{

            //    cbxFBAIDc.DataSource = dc.FFixedAsset_Brands
            //         .Where(t => (t.FCAT_ID == int.Parse(cbxFCATIDc.SelectedValue.ToString())
            //             .Where(t=>(t.FCAT_ID == 50
            //              .Where(t => (t.FCAT_ID == vb
            //            ))
            //      .Select(t => new
            //      {
            //          brandID = t.FBA_ID,
            //          brand = t.FBA_ID.ToString() + ": " + t.FBrand
            //      })
            //      .ToList();
            //    cbxFBAIDc.DisplayMember = "brand";
            //    cbxFBAIDc.ValueMember = "brandID";

            //}
        }

        private void cbxFBAIDc_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            {
                var m = dc.FFixedAsset_Models
                            // .Where(t => (t.FBA_ID == int.Parse(cbxFBAIDc.SelectedValue.ToString())))
                            .Where(t => (t.FBA_ID.ToString() == cbxFBAIDc.SelectedValue.ToString()))
                              .Select(t => new
                              {
                                  modelID = t.FMD_ID.ToString(),
                                  FModel = t.FMD_ID.ToString() + ": " + t.FModel
                              }).ToList();
                m.Insert(0, new
                {
                    modelID = "0",
                    FModel = ""
                });

                cbxFMDIDc.DataSource = m;
                cbxFMDIDc.DisplayMember = "FModel";
                cbxFMDIDc.ValueMember = "modelID";
            }
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            if (cbxFCATIDc.SelectedValue.ToString() == "0")
            {
                MessageBox.Show("กรุณาระบุ ประเภทอุปกรณ์ ", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (cbxFBAIDc.SelectedValue.ToString() == "0")
            {
                MessageBox.Show("กรุณาระบุ ยี่ห้อ ", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (cbxFMDIDc.SelectedValue.ToString() == "0")
            {
                MessageBox.Show("กรุณาระบุ รุ่น ", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (string.IsNullOrEmpty(txtSNc.Text.Trim()))
            {
                MessageBox.Show("กรุณาตรวจสอบ Serial number ", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (string.IsNullOrEmpty(txtRemark.Text.Trim()))
            {
                MessageBox.Show("กรุณาระบุ หมายเหตุการแก้ไข ", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (checkDupSN(txtSNc.Text.Trim().ToString()))
            {
                MessageBox.Show("คุณเลือก Serialnumber ซ้ำ  กรุณาเลือกใหม่", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            {
                var qRechk = (from t in dc.FFixedAsset_SerialNumber_Pools
                                  //join m in dc.FFixedAsset_Models on t.FMD_ID equals m.FMD_ID
                                  //join b in dc.FFixedAsset_Brands on m.FBA_ID equals b.FBA_ID
                                  //join c in dc.FFixedAsset_Categories on b.FCAT_ID equals c.FCAT_ID
                              join s in dc.FFixedAsset_SNStatus on t.StatusUse equals s.FSTUS_ID
                              where t.SerialNumber == txtSNc.Text.Trim()
                              select new
                              {
                                  //NO = 0,
                                  //t.FCAT_ID,
                                  //m.FMD_ID,
                                  //m.FBA_ID,
                                  t.SerialNumber,
                                  //c.FixedAsset_CateName,
                                  //b.FBrand,
                                  //m.FModel,
                                  s.FFAStatus
                              }).FirstOrDefault();
                if (qRechk != null)
                {
                    var newFFa = new FFASelect();
                    newFFa.SerialNumber = txtSNc.Text.Trim();
                    newFFa.รหัสอุปกรณ์ = int.Parse(cbxFCATIDc.SelectedValue.ToString());
                    newFFa.ประเภทอุปกรณ์ = cbxFCATIDc.Text;
                    newFFa.รหัสยี่ห้อ = int.Parse(cbxFBAIDc.SelectedValue.ToString());
                    newFFa.ยี่ห้อ = cbxFBAIDc.Text;
                    newFFa.รหัสรุ่น = int.Parse(cbxFMDIDc.SelectedValue.ToString());
                    newFFa.รุ่น = cbxFMDIDc.Text;
                    newFFa.หมายเหตุการแก้ไข = txtRemark.Text;
                    //newFFa.FCAT = int.Parse(cbxFCATIDc.SelectedValue.ToString());
                    //newFFa.FBA = int.Parse(cbxFBAIDc.SelectedValue.ToString());
                    //newFFa.FMD = int.Parse(cbxFMDIDc.SelectedValue.ToString());




                    LstFFASelect.Add(newFFa);
                    dataGridView_ChgModel.DataSource = LstFFASelect.ToList();
                }
            }
        }

        private bool checkDupSN(string gvSN)
        {
            for (int i = 0; i <= dataGridView_ChgModel.Rows.Count - 1; i++)
            {
                if (gvSN == dataGridView_ChgModel["SerialNumber", i].Value.ToString()) return true;
            }
            return false;
        }

        private void toolStripButton_Cancel_Click(object sender, EventArgs e)
        {
            ClearText();
            ChangeModelFFA_Load(sender, e);
        }
        private void ClearText()
        {
            dataGridView_ChgModel.DataSource = null; ;
            dataGridView_ChgModel.Refresh();
            txtSNFind.Text = "";
            txtSNo.Text = "";
            txtSNc.Text = "";
            txtRemark.Text = "";

        }

        private void dataGridView_ChgModel_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                if (MessageBox.Show("คุณต้องการ ยกเลิก  Serialnumber : " + dataGridView_ChgModel["Serialnumber", e.RowIndex].Value.ToString() + " ใช่หรือไม่???...", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    var qcanR = LstFFASelect.Where(t => t.SerialNumber == dataGridView_ChgModel["Serialnumber", e.RowIndex].Value.ToString()).FirstOrDefault();
                    if (qcanR != null)
                    {
                        LstFFASelect.Remove(qcanR);
                        dataGridView_ChgModel.DataSource = LstFFASelect.ToList();
                    }
                }
            }
        }

        private void toolStripButton_Save_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("คุณต้องการ บันทึกข้อมูล Fixed Asset ใช่หรือไม่???...", "คำยืนยัน",
                         MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
                {
                    dc.Connection.Open();
                    dc.Transaction = dc.Connection.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted);
                    try
                    {

                        DateTime? _serverTime = null;
                        _serverTime = dc
                               .ExecuteQuery<DateTime>("SELECT GETDATE()")
                               .First();

                        if (_serverTime == null)
                        {
                            MessageBox.Show("ไม่สามารถดึง เวลา จากเซิร์ฟเวอร์ ได้..." + Environment.NewLine +
                            "กรุณาตรวจสอบ...", "ผลการตรวจสอบ...", MessageBoxButtons.OK);
                            return;
                        }

                        foreach (var item2 in LstFFASelect)
                        {
                            var poolChn = dc.FFixedAsset_SerialNumber_Pools
                               .Where(t => t.SerialNumber == item2.SerialNumber)
                               .FirstOrDefault();

                            if (poolChn != null)
                            {
                                //==log change
                                var qOldFBA = dc.FFixedAsset_Models
                               .Where(t => t.FMD_ID == poolChn.FMD_ID)
                               .FirstOrDefault();
                                if (qOldFBA != null)
                                {
                                    int oldFCAT = poolChn.FCAT_ID;
                                    int oldFBA = qOldFBA.FBA_ID;
                                    int oldFMD = poolChn.FMD_ID;

                                    DateTime Trackgetdate = dc.ExecuteQuery<DateTime>("select getdate() ")
                                        .FirstOrDefault();

                                    //ถ้า 11 ต้องระบุสถานี

                                    //var newSNTrack = new DAL.SMSManage.FFixedAsset_SerialNumber_Tracking
                                    var newSNTrack1 = new DAL.SMSManage.FFixedAsset_SerialNumber_Tracking
                                    {
                                        TrackingDate = Trackgetdate,
                                        SerialNumber_Old = item2.SerialNumber,
                                        SerialNumber_New = item2.SerialNumber,
                                        JOB_ID = "",
                                        STA_ID_Old = "",
                                        STA_ID_New = "",
                                        VAN_ID = UserInfo.UserId,
                                        SerialNumber_Status = "94",
                                        Remark = "Change Model FixedAsset Tracking",
                                        RegistDate = _serverTime
                                    };
                                    dc.FFixedAsset_SerialNumber_Trackings.InsertOnSubmit(newSNTrack1);
                                    dc.SubmitChanges();


                                    var chgModelLog = new DAL.SMSManage.FFixedAsset_ChangeModel();
                                    chgModelLog.SerialNumber = item2.SerialNumber;
                                    chgModelLog.Trandate = Trackgetdate;
                                    chgModelLog.FCAT_ID_Old = oldFCAT;
                                    chgModelLog.FBA_ID_Old = oldFBA;
                                    chgModelLog.FMD_ID_Old = oldFMD;
                                    chgModelLog.FCAT_ID_New = item2.รหัสอุปกรณ์;
                                    chgModelLog.FBA_ID_New = item2.รหัสยี่ห้อ;
                                    chgModelLog.FMD_ID_New = item2.รหัสรุ่น;
                                    chgModelLog.EmpChgModel = UserInfo.UserId;
                                    chgModelLog.FrmProcess = "94";
                                    chgModelLog.Remark = item2.หมายเหตุการแก้ไข;

                                    dc.FFixedAsset_ChangeModels.InsertOnSubmit(chgModelLog);
                                    dc.SubmitChanges();

                                }

                                //==

                                poolChn.FCAT_ID = item2.รหัสอุปกรณ์;
                                poolChn.FMD_ID = item2.รหัสรุ่น;
                                dc.SubmitChanges();

                                //=== 
                            }

                        }
                        dc.Transaction.Commit();

                        MessageBox.Show("บันทึกการเปลี่ยนแปลงข้อมูล Fixed Asset เรียบร้อย ", "ผลการทำงาน",
                               MessageBoxButtons.OK, MessageBoxIcon.Information);

                        ClearText();
                        ChangeModelFFA_Load(sender, e);


                    }
                    catch (Exception ex)
                    {
                        dc.Transaction.Rollback();

                        MessageBox.Show("ไม่สามารถ บันทึกข้อมูล Fixed Asset  กรุณาตรวจสอบ" + Environment.NewLine +
                          "1.สัญญาณ อินเตอร์เนต ปกติ หรือไม่?..." + Environment.NewLine +
                          "2.เข้าเว็บไซต์ JOB Monitor ได้ปกติ หรือไม่?..." + Environment.NewLine +
                          "Error: " + ex.Message, "ผลการบันทึกข้อมูล",
                           MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }

        private void toolStripButton_Exit_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {

            string xMenu = null;

            xMenu = "เปลี่ยนแปลงข้อมูล Fixed Asset";


            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            {

                //DateTime? _serverTime = null;
                DateTime _serverTime = DateTime.Now;
                _serverTime = dc
                       .ExecuteQuery<DateTime>("SELECT GETDATE()")
                       .First();

                //if (_serverTime == null)
                //{
                //    MessageBox.Show("ไม่สามารถดึง เวลา จากเซิร์ฟเวอร์ ได้..." + Environment.NewLine +
                //    "กรุณาตรวจสอบ...", "ผลการตรวจสอบ...", MessageBoxButtons.OK);
                //    return;
                //}

                var srv = dc.ExecuteQuery<DateTime>("SELECT GETDATE()").First();
                if (srv != null)
                {
                    servDate = srv;
                }

                var poolChkDel = dc.FFixedAsset_SerialNumber_Pools
                                 .Where(t => t.SerialNumber == txtSNo.Text)
                                 .FirstOrDefault();

                if (poolChkDel != null)
                {
                    bool SNFSam = false;
                    if (!string.IsNullOrEmpty(poolChkDel.EMP_ID))
                    {
                        var poolChkDep = dc.Employees
                                   .Where(t => new string[] { "11", "24", "25", "14"
                               }.Contains(t.DEP_ID) && t.EMP_ID == poolChkDel.EMP_ID)
                                   .FirstOrDefault();

                        if (poolChkDep != null)
                        {
                            SNFSam = true;
                        }
                    }
                    //van ลบได้แค่ภายในวัน 
                    //if (!new string[] { "11", "24", "25"//, "14" 
                    //}.Contains(UserInfo.eDepID) && (poolChkDel.RegisDate < DateTime.Now || SNFSam == true))  //--เปิดให้ sam ลบได้ตลอดก่อนชั่วคราว //14 bdd
                    //// hold if ( poolChkDel.RegisDate < DateTime.Now && SNFSam == false)  //--ปิดให้ sam ลบได้ตลอด
                    //{

                    //    MessageBox.Show("ระบบห้ามลบ Serailnumber  ข้ามวัน ", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //    return;
                    //}

                    if (!new string[] { "11", "24", "25", "14" }.Contains(UserInfo.eDepID))  //--เปิดให้ sam ลบได้ตลอดก่อนชั่วคราว //14 bdd
                    {
                        if (string.IsNullOrEmpty(poolChkDel.RegisDate.ToString()) || poolChkDel.RegisDate.Value.Date < DateTime.Now.Date)
                        {
                            MessageBox.Show("ระบบห้ามลบ Serailnumber  ที่บันทึก ข้ามวัน ", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        if (SNFSam == true || string.IsNullOrEmpty(poolChkDel.EMP_ID))
                        {
                            MessageBox.Show("ระบบห้ามลบ Serailnumber  ที่บันทึกจากส่วนกลาง ", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                    }//dep ต่อไปถ้าเป็น sam จะเช็คเรื่องข้ามวัน และลบได้เฉพาะของแผนกตัวเอง  SNFSam == false





                    //check สถานีก่อน
                    var qChkDetail = dc.FFixedAsset_Station_Details
                               .Where(t => t.SerialNumber == txtSNo.Text)
                               .FirstOrDefault();

                    if (qChkDetail != null)
                    {
                        var qStation = dc.Stations.Where(t => t.STA_ID == qChkDetail.STA_ID).FirstOrDefault();
                        if (
                            MessageBox.Show("Serial Number: [" + qChkDetail.SerialNumber + "] ถูกใช้งานแล้ว " + Environment.NewLine +
                             " " + Environment.NewLine +
                             " ที่สถานี : " + qStation.STA_ID + " : " + qStation.StationSys + " " + Environment.NewLine +
                             " ที่อยู่ : " + qStation.Address + Environment.NewLine +
                             " " + Environment.NewLine +
                             "คุณต้องการ ลบ Serialnumber นี้ ออกจากสถานี และ ลบทิ้งออกจากระบบเลย ใช่หรือไม่???...", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question
                            ) == DialogResult.Yes)
                        {
                            //เก็บ log ลบข้อมูลสถานี
                            DateTime TrackgetdateDetail = dc.ExecuteQuery<DateTime>("select getdate() ")
                                .FirstOrDefault();

                            var newSNTrack1 = new DAL.SMSManage.FFixedAsset_SerialNumber_Tracking
                            {
                                TrackingDate = TrackgetdateDetail,
                                SerialNumber_Old = qChkDetail.SerialNumber,
                                SerialNumber_New = qChkDetail.SerialNumber,
                                JOB_ID = "",
                                STA_ID_Old = qChkDetail.STA_ID,
                                STA_ID_New = qChkDetail.STA_ID,
                                VAN_ID = UserInfo.UserId,
                                SerialNumber_Status = "95",
                                Remark = "Delete FixedAsset Station Detail Tracking",
                                RegistDate = _serverTime
                            };
                            dc.FFixedAsset_SerialNumber_Trackings.InsertOnSubmit(newSNTrack1);
                            dc.SubmitChanges();


                            var ldDetail = new DAL.SMSManage.Log_DeleteDatabyProgram
                            {
                                Trandate = _serverTime,
                                DataID = qChkDetail.SerialNumber,
                                TableName = "FFixedAsset_Station_Detail",
                                Menu = xMenu,
                                Detail = "Delete SN " + qChkDetail.SerialNumber + " STA_ID " + qChkDetail.STA_ID,
                                UserID = UserInfo.UserId,
                                Apps = "SMSMINI"

                            };

                            dc.Log_DeleteDatabyPrograms.InsertOnSubmit(ldDetail);
                            dc.SubmitChanges();

                            //ลบ                           
                            dc.FFixedAsset_Station_Details.DeleteOnSubmit(qChkDetail);
                            dc.SubmitChanges();
                            //==============================================

                            //เก็บ log ลบข้อมูล pool
                            DateTime TrackgetdatePool = dc.ExecuteQuery<DateTime>("select getdate() ")
                            .FirstOrDefault();

                            var newSNTrack2 = new DAL.SMSManage.FFixedAsset_SerialNumber_Tracking
                            {
                                TrackingDate = TrackgetdatePool,
                                SerialNumber_Old = poolChkDel.SerialNumber,
                                SerialNumber_New = poolChkDel.SerialNumber,
                                JOB_ID = "",
                                STA_ID_Old = "",
                                STA_ID_New = "",
                                VAN_ID = UserInfo.UserId,
                                SerialNumber_Status = "95",
                                Remark = "Delete FixedAsset Pool Tracking",
                                RegistDate = _serverTime
                            };
                            dc.FFixedAsset_SerialNumber_Trackings.InsertOnSubmit(newSNTrack2);
                            dc.SubmitChanges();

                            var ldpool = new DAL.SMSManage.Log_DeleteDatabyProgram
                            {
                                Trandate = _serverTime,
                                DataID = poolChkDel.SerialNumber,
                                TableName = "FFixedAsset_SerialNumber_Pool",
                                Menu = xMenu,
                                Detail = "Delete SN " + poolChkDel.SerialNumber,
                                UserID = UserInfo.UserId,
                                Apps = "SMSMINI"

                            };

                            dc.Log_DeleteDatabyPrograms.InsertOnSubmit(ldpool);
                            dc.SubmitChanges();


                            //ลบ
                            dc.FFixedAsset_SerialNumber_Pools.DeleteOnSubmit(poolChkDel);
                            dc.SubmitChanges();

                            MessageBox.Show("ลบข้อมูล Fixed Asset เรียบร้อย ", "ผลการทำงาน",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);

                            ClearText();

                        }//yes
                    }
                    else
                    {
                        if (MessageBox.Show("คุณต้องการ ลบ Serialnumber : " + poolChkDel.SerialNumber + " ออกจากระบบ ใช่หรือไม่???...", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            //เก็บ log ลบข้อมูล pool
                            DateTime TrackgetdatePool = dc.ExecuteQuery<DateTime>("select getdate() ")
                            .FirstOrDefault();

                            var newSNTrack2 = new DAL.SMSManage.FFixedAsset_SerialNumber_Tracking
                            {
                                TrackingDate = TrackgetdatePool,
                                SerialNumber_Old = poolChkDel.SerialNumber,
                                SerialNumber_New = poolChkDel.SerialNumber,
                                JOB_ID = "",
                                STA_ID_Old = "",
                                STA_ID_New = "",
                                VAN_ID = UserInfo.UserId,
                                SerialNumber_Status = "95",
                                Remark = "Delete FixedAsset Pool Tracking",
                                RegistDate = _serverTime
                            };
                            dc.FFixedAsset_SerialNumber_Trackings.InsertOnSubmit(newSNTrack2);
                            dc.SubmitChanges();

                            var ldpool = new DAL.SMSManage.Log_DeleteDatabyProgram
                            {
                                Trandate = _serverTime,
                                DataID = poolChkDel.SerialNumber,
                                TableName = "FFixedAsset_SerialNumber_Pool",
                                Menu = xMenu,
                                Detail = "Delete SN " + poolChkDel.SerialNumber,
                                UserID = UserInfo.UserId,
                                Apps = "SMSMINI"

                            };

                            dc.Log_DeleteDatabyPrograms.InsertOnSubmit(ldpool);
                            dc.SubmitChanges();


                            //ลบ
                            dc.FFixedAsset_SerialNumber_Pools.DeleteOnSubmit(poolChkDel);
                            dc.SubmitChanges();

                            MessageBox.Show("ลบข้อมูล Fixed Asset เรียบร้อย ", "ผลการทำงาน",
                             MessageBoxButtons.OK, MessageBoxIcon.Information);

                            ClearText();
                        }

                    }//nodetail
                }//pool
            }//dal

            if (string.IsNullOrEmpty(txtSNo.Text))
            {
                btnDelete.Enabled = false;
            }
            else
            {
                btnDelete.Enabled = true;
            }









        }


    }

    public class FFASelect
    {
        public string SerialNumber { get; set; }
        public int รหัสอุปกรณ์ { get; set; }
        public string ประเภทอุปกรณ์ { get; set; }
        public int รหัสยี่ห้อ { get; set; }
        public string ยี่ห้อ { get; set; }
        public int รหัสรุ่น { get; set; }
        public string รุ่น { get; set; }
        public string หมายเหตุการแก้ไข { get; set; }
        //public int FCAT { get; set; }
        //public int FBA { get; set; }
        //public int FMD { get; set; }



    }
}
