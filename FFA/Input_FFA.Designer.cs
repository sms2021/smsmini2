﻿namespace SMSMINI.FFA
{
    partial class Input_FFA
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Input_FFA));
            this.toolStrip_Mnu = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel_cobby = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.tst_txtFind = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_FindStation = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_Save = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_Cancel = new System.Windows.Forms.ToolStripButton();
            this.toolStripBt_PrintFFA = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_Exit = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel_Dateime = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.Group1 = new System.Windows.Forms.GroupBox();
            this.txtStaName = new System.Windows.Forms.TextBox();
            this.txtSTA_ID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comb_cat = new System.Windows.Forms.ComboBox();
            this.comb_model = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dtInstall = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.dtExpireDate = new System.Windows.Forms.DateTimePicker();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rdMonth = new System.Windows.Forms.RadioButton();
            this.rdYear = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.txtTranEmp = new System.Windows.Forms.TextBox();
            this.chkInstallDate = new System.Windows.Forms.CheckBox();
            this.txtYearWarranty = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.comb_cus_flo = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.comb_brand = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtSN = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtRemark = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btAdd = new System.Windows.Forms.Button();
            this.dgFFA = new System.Windows.Forms.DataGridView();
            this.cDelSN = new System.Windows.Forms.DataGridViewImageColumn();
            this.SerialNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cIsConfirm = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.cLocation = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.cDetail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cFCAT_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cFixedAsset_CateName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cFBA_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cFBrand = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cFMD_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cFModel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cOwnerShip = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cInstallDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cWarrantDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cSTA_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cStationSys = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cTelNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cFaxNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cProvince = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cPartType1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cPartType2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cPartType3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cCheckGauge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cControlFixAssetNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cTranDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cTranEMP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cConfirmEMP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cConfirmDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cIsLifetime = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.cIsSpecialPrice = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.label11 = new System.Windows.Forms.Label();
            this.txtPartTypeWt1 = new System.Windows.Forms.TextBox();
            this.txtPartTypeWt2 = new System.Windows.Forms.TextBox();
            this.txtPartTypeWt3 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtCheckGauge = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtFixedAssetCust = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.lblIsConfirmFFA = new System.Windows.Forms.Label();
            this.chxFFAConf = new System.Windows.Forms.CheckBox();
            this.cbxLocation = new System.Windows.Forms.ComboBox();
            this.ControlFFA = new System.Windows.Forms.GroupBox();
            this.btControlFFA = new System.Windows.Forms.Button();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.chxLifetime = new System.Windows.Forms.CheckBox();
            this.chxSpecialPrice = new System.Windows.Forms.CheckBox();
            this.btnChangeModelFFA = new System.Windows.Forms.Button();
            this.toolStrip_Mnu.SuspendLayout();
            this.Group1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgFFA)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.ControlFFA.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip_Mnu
            // 
            this.toolStrip_Mnu.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip_Mnu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel_cobby,
            this.toolStripSeparator4,
            this.tst_txtFind,
            this.toolStripSeparator2,
            this.toolStripButton_FindStation,
            this.toolStripSeparator3,
            this.toolStripButton_Save,
            this.toolStripButton_Cancel,
            this.toolStripBt_PrintFFA,
            this.toolStripSeparator1,
            this.toolStripButton_Exit,
            this.toolStripLabel_Dateime,
            this.toolStripSeparator5});
            this.toolStrip_Mnu.Location = new System.Drawing.Point(0, 0);
            this.toolStrip_Mnu.Name = "toolStrip_Mnu";
            this.toolStrip_Mnu.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.toolStrip_Mnu.Size = new System.Drawing.Size(1174, 39);
            this.toolStrip_Mnu.TabIndex = 0;
            // 
            // toolStripLabel_cobby
            // 
            this.toolStripLabel_cobby.AutoSize = false;
            this.toolStripLabel_cobby.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.toolStripLabel_cobby.ForeColor = System.Drawing.SystemColors.Desktop;
            this.toolStripLabel_cobby.Items.AddRange(new object[] {
            "ตาม JOB ID",
            "ตาม สถานี"});
            this.toolStripLabel_cobby.Name = "toolStripLabel_cobby";
            this.toolStripLabel_cobby.Size = new System.Drawing.Size(100, 21);
            this.toolStripLabel_cobby.Text = "ค้นหาข้อมูล...";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 39);
            // 
            // tst_txtFind
            // 
            this.tst_txtFind.AutoSize = false;
            this.tst_txtFind.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.tst_txtFind.ForeColor = System.Drawing.SystemColors.Desktop;
            this.tst_txtFind.Name = "tst_txtFind";
            this.tst_txtFind.Size = new System.Drawing.Size(250, 20);
            this.tst_txtFind.KeyDown += new System.Windows.Forms.KeyEventHandler(this.toolStripText_txtFind_KeyDown);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripButton_FindStation
            // 
            this.toolStripButton_FindStation.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_FindStation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.toolStripButton_FindStation.Image = global::SMSMINI.Properties.Resources.Find48;
            this.toolStripButton_FindStation.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_FindStation.Name = "toolStripButton_FindStation";
            this.toolStripButton_FindStation.Size = new System.Drawing.Size(36, 36);
            this.toolStripButton_FindStation.ToolTipText = "ค้นหาข้อมูล";
            this.toolStripButton_FindStation.Click += new System.EventHandler(this.toolStripButton_FindStation_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripButton_Save
            // 
            this.toolStripButton_Save.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.toolStripButton_Save.ForeColor = System.Drawing.SystemColors.Desktop;
            this.toolStripButton_Save.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Save.Image")));
            this.toolStripButton_Save.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Save.Name = "toolStripButton_Save";
            this.toolStripButton_Save.Size = new System.Drawing.Size(72, 36);
            this.toolStripButton_Save.Text = "บันทึก";
            this.toolStripButton_Save.ToolTipText = "Upload JOB";
            this.toolStripButton_Save.Click += new System.EventHandler(this.toolStripButton_Save_Click);
            // 
            // toolStripButton_Cancel
            // 
            this.toolStripButton_Cancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.toolStripButton_Cancel.ForeColor = System.Drawing.SystemColors.Desktop;
            this.toolStripButton_Cancel.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Cancel.Image")));
            this.toolStripButton_Cancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Cancel.Name = "toolStripButton_Cancel";
            this.toolStripButton_Cancel.Size = new System.Drawing.Size(75, 36);
            this.toolStripButton_Cancel.Text = "ยกเลิก";
            this.toolStripButton_Cancel.ToolTipText = "ยกเลิกข้อมูล";
            this.toolStripButton_Cancel.Click += new System.EventHandler(this.toolStripButton_Cancel_Click);
            // 
            // toolStripBt_PrintFFA
            // 
            this.toolStripBt_PrintFFA.Image = global::SMSMINI.Properties.Resources.print_f2;
            this.toolStripBt_PrintFFA.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripBt_PrintFFA.Name = "toolStripBt_PrintFFA";
            this.toolStripBt_PrintFFA.Size = new System.Drawing.Size(88, 36);
            this.toolStripBt_PrintFFA.Text = "พิมพ์ FFA";
            this.toolStripBt_PrintFFA.Click += new System.EventHandler(this.toolStripBt_PrintFFA_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripButton_Exit
            // 
            this.toolStripButton_Exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.toolStripButton_Exit.ForeColor = System.Drawing.SystemColors.Desktop;
            this.toolStripButton_Exit.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Exit.Image")));
            this.toolStripButton_Exit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Exit.Name = "toolStripButton_Exit";
            this.toolStripButton_Exit.Size = new System.Drawing.Size(108, 36);
            this.toolStripButton_Exit.Text = "ออกโปรแกรม";
            this.toolStripButton_Exit.ToolTipText = "ออกจากหน้าจอ";
            this.toolStripButton_Exit.Click += new System.EventHandler(this.toolStripButton_Exit_Click);
            // 
            // toolStripLabel_Dateime
            // 
            this.toolStripLabel_Dateime.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripLabel_Dateime.Font = new System.Drawing.Font("Tahoma", 10F);
            this.toolStripLabel_Dateime.ForeColor = System.Drawing.SystemColors.Desktop;
            this.toolStripLabel_Dateime.Name = "toolStripLabel_Dateime";
            this.toolStripLabel_Dateime.Size = new System.Drawing.Size(0, 36);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 39);
            // 
            // Group1
            // 
            this.Group1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Group1.Controls.Add(this.txtStaName);
            this.Group1.Controls.Add(this.txtSTA_ID);
            this.Group1.Location = new System.Drawing.Point(12, 51);
            this.Group1.Name = "Group1";
            this.Group1.Size = new System.Drawing.Size(2436, 49);
            this.Group1.TabIndex = 1;
            this.Group1.TabStop = false;
            this.Group1.Text = "ข้อมูลสถานี:";
            // 
            // txtStaName
            // 
            this.txtStaName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStaName.Enabled = false;
            this.txtStaName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtStaName.Location = new System.Drawing.Point(128, 19);
            this.txtStaName.Name = "txtStaName";
            this.txtStaName.Size = new System.Drawing.Size(2299, 23);
            this.txtStaName.TabIndex = 1;
            // 
            // txtSTA_ID
            // 
            this.txtSTA_ID.Enabled = false;
            this.txtSTA_ID.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtSTA_ID.Location = new System.Drawing.Point(6, 19);
            this.txtSTA_ID.Name = "txtSTA_ID";
            this.txtSTA_ID.Size = new System.Drawing.Size(116, 23);
            this.txtSTA_ID.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "อุปกรณ์:";
            // 
            // comb_cat
            // 
            this.comb_cat.FormattingEnabled = true;
            this.comb_cat.Location = new System.Drawing.Point(9, 40);
            this.comb_cat.Name = "comb_cat";
            this.comb_cat.Size = new System.Drawing.Size(135, 21);
            this.comb_cat.TabIndex = 0;
            this.comb_cat.SelectionChangeCommitted += new System.EventHandler(this.comb_cat_SelectionChangeCommitted);
            // 
            // comb_model
            // 
            this.comb_model.FormattingEnabled = true;
            this.comb_model.Location = new System.Drawing.Point(312, 40);
            this.comb_model.Name = "comb_model";
            this.comb_model.Size = new System.Drawing.Size(163, 21);
            this.comb_model.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(309, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "รุ่น:";
            // 
            // dtInstall
            // 
            this.dtInstall.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtInstall.Location = new System.Drawing.Point(481, 41);
            this.dtInstall.Name = "dtInstall";
            this.dtInstall.Size = new System.Drawing.Size(101, 20);
            this.dtInstall.TabIndex = 3;
            this.dtInstall.ValueChanged += new System.EventHandler(this.dtInstall_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(585, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "รับประกัน:";
            // 
            // dtExpireDate
            // 
            this.dtExpireDate.Enabled = false;
            this.dtExpireDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtExpireDate.Location = new System.Drawing.Point(588, 2);
            this.dtExpireDate.Name = "dtExpireDate";
            this.dtExpireDate.Size = new System.Drawing.Size(101, 20);
            this.dtExpireDate.TabIndex = 4;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rdMonth);
            this.groupBox2.Controls.Add(this.rdYear);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.txtTranEmp);
            this.groupBox2.Controls.Add(this.chkInstallDate);
            this.groupBox2.Controls.Add(this.txtYearWarranty);
            this.groupBox2.Controls.Add(this.dtExpireDate);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.comb_cus_flo);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.comb_brand);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.comb_cat);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.dtInstall);
            this.groupBox2.Controls.Add(this.comb_model);
            this.groupBox2.Location = new System.Drawing.Point(12, 101);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(903, 78);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "อุปกรณ์:";
            // 
            // rdMonth
            // 
            this.rdMonth.AutoSize = true;
            this.rdMonth.Location = new System.Drawing.Point(644, 46);
            this.rdMonth.Name = "rdMonth";
            this.rdMonth.Size = new System.Drawing.Size(51, 17);
            this.rdMonth.TabIndex = 22;
            this.rdMonth.Text = "เดือน";
            this.rdMonth.UseVisualStyleBackColor = true;
            this.rdMonth.Click += new System.EventHandler(this.rdMonth_Click);
            // 
            // rdYear
            // 
            this.rdYear.AutoSize = true;
            this.rdYear.Checked = true;
            this.rdYear.Location = new System.Drawing.Point(644, 31);
            this.rdYear.Name = "rdYear";
            this.rdYear.Size = new System.Drawing.Size(32, 17);
            this.rdYear.TabIndex = 21;
            this.rdYear.TabStop = true;
            this.rdYear.Text = "ปี";
            this.rdYear.UseVisualStyleBackColor = true;
            this.rdYear.Click += new System.EventHandler(this.rdYear_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(807, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 20;
            this.label3.Text = "ผู้บันทึก:";
            // 
            // txtTranEmp
            // 
            this.txtTranEmp.Enabled = false;
            this.txtTranEmp.Location = new System.Drawing.Point(810, 40);
            this.txtTranEmp.Name = "txtTranEmp";
            this.txtTranEmp.Size = new System.Drawing.Size(84, 20);
            this.txtTranEmp.TabIndex = 19;
            // 
            // chkInstallDate
            // 
            this.chkInstallDate.AutoSize = true;
            this.chkInstallDate.Location = new System.Drawing.Point(481, 23);
            this.chkInstallDate.Name = "chkInstallDate";
            this.chkInstallDate.Size = new System.Drawing.Size(76, 17);
            this.chkInstallDate.TabIndex = 18;
            this.chkInstallDate.Text = "วันที่ติดตั้ง:";
            this.chkInstallDate.UseVisualStyleBackColor = true;
            this.chkInstallDate.Click += new System.EventHandler(this.chkInstallDate_Click);
            // 
            // txtYearWarranty
            // 
            this.txtYearWarranty.Location = new System.Drawing.Point(588, 41);
            this.txtYearWarranty.Name = "txtYearWarranty";
            this.txtYearWarranty.Size = new System.Drawing.Size(53, 20);
            this.txtYearWarranty.TabIndex = 16;
            this.txtYearWarranty.Text = "2";
            this.txtYearWarranty.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtYearWarranty.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtYearWarranty_KeyDown);
            this.txtYearWarranty.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtYearWarranty_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(692, 24);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(43, 13);
            this.label9.TabIndex = 15;
            this.label9.Text = "เจ้าของ:";
            // 
            // comb_cus_flo
            // 
            this.comb_cus_flo.FormattingEnabled = true;
            this.comb_cus_flo.Items.AddRange(new object[] {
            "Flowco",
            "Customer",
            "Uniwave"});
            this.comb_cus_flo.Location = new System.Drawing.Point(695, 40);
            this.comb_cus_flo.Name = "comb_cus_flo";
            this.comb_cus_flo.Size = new System.Drawing.Size(107, 21);
            this.comb_cus_flo.TabIndex = 14;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(147, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(30, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "ยี่ห้อ:";
            // 
            // comb_brand
            // 
            this.comb_brand.FormattingEnabled = true;
            this.comb_brand.Location = new System.Drawing.Point(150, 40);
            this.comb_brand.Name = "comb_brand";
            this.comb_brand.Size = new System.Drawing.Size(156, 21);
            this.comb_brand.TabIndex = 1;
            this.comb_brand.SelectionChangeCommitted += new System.EventHandler(this.comb_brand_SelectionChangeCommitted);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 183);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(116, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Serial number/ Part no:";
            // 
            // txtSN
            // 
            this.txtSN.Location = new System.Drawing.Point(12, 199);
            this.txtSN.Name = "txtSN";
            this.txtSN.Size = new System.Drawing.Size(180, 20);
            this.txtSN.TabIndex = 3;
            this.txtSN.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSN_KeyDown);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(418, 183);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "ตำแหน่ง (Location):";
            // 
            // txtRemark
            // 
            this.txtRemark.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtRemark.Location = new System.Drawing.Point(573, 199);
            this.txtRemark.Name = "txtRemark";
            this.txtRemark.Size = new System.Drawing.Size(1866, 20);
            this.txtRemark.TabIndex = 5;
            this.txtRemark.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtRemark_KeyDown);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(570, 182);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(114, 13);
            this.label7.TabIndex = 18;
            this.label7.Text = "หมายเหตุ/รายละเอียด:";
            // 
            // btAdd
            // 
            this.btAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btAdd.Location = new System.Drawing.Point(808, 226);
            this.btAdd.Name = "btAdd";
            this.btAdd.Size = new System.Drawing.Size(98, 71);
            this.btAdd.TabIndex = 6;
            this.btAdd.Text = "เลือกรายการ";
            this.btAdd.UseVisualStyleBackColor = true;
            this.btAdd.Click += new System.EventHandler(this.btAdd_Click);
            // 
            // dgFFA
            // 
            this.dgFFA.AllowUserToAddRows = false;
            this.dgFFA.AllowUserToDeleteRows = false;
            this.dgFFA.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgFFA.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgFFA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgFFA.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cDelSN,
            this.SerialNumber,
            this.cIsConfirm,
            this.cLocation,
            this.cDetail,
            this.cFCAT_ID,
            this.cFixedAsset_CateName,
            this.cFBA_ID,
            this.cFBrand,
            this.cFMD_ID,
            this.cFModel,
            this.cOwnerShip,
            this.cInstallDate,
            this.cWarrantDate,
            this.cSTA_ID,
            this.cStationSys,
            this.cAddress,
            this.cTelNo,
            this.cFaxNo,
            this.cProvince,
            this.cPartType1,
            this.cPartType2,
            this.cPartType3,
            this.cCheckGauge,
            this.cControlFixAssetNo,
            this.cTranDate,
            this.cTranEMP,
            this.cConfirmEMP,
            this.cConfirmDate,
            this.cIsLifetime,
            this.cIsSpecialPrice});
            this.dgFFA.Location = new System.Drawing.Point(0, 303);
            this.dgFFA.Name = "dgFFA";
            this.dgFFA.RowHeadersWidth = 123;
            this.dgFFA.Size = new System.Drawing.Size(1174, 156);
            this.dgFFA.TabIndex = 21;
            this.dgFFA.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgFFA_CellClick);
            this.dgFFA.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgFFA_CellDoubleClick);
            this.dgFFA.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgFFA_CellValueChanged);
            this.dgFFA.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgFFA_CurrentCellDirtyStateChanged);
            // 
            // cDelSN
            // 
            this.cDelSN.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.cDelSN.DataPropertyName = "DelSN";
            this.cDelSN.FillWeight = 30F;
            this.cDelSN.HeaderText = "ลบSN";
            this.cDelSN.Image = global::SMSMINI.Properties.Resources.del;
            this.cDelSN.MinimumWidth = 15;
            this.cDelSN.Name = "cDelSN";
            this.cDelSN.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cDelSN.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.cDelSN.Width = 40;
            // 
            // SerialNumber
            // 
            this.SerialNumber.DataPropertyName = "SerialNumber";
            this.SerialNumber.HeaderText = "SerialNumber";
            this.SerialNumber.MinimumWidth = 15;
            this.SerialNumber.Name = "SerialNumber";
            this.SerialNumber.Width = 95;
            // 
            // cIsConfirm
            // 
            this.cIsConfirm.DataPropertyName = "IsConfirm";
            this.cIsConfirm.HeaderText = "Confirm";
            this.cIsConfirm.MinimumWidth = 15;
            this.cIsConfirm.Name = "cIsConfirm";
            this.cIsConfirm.Width = 48;
            // 
            // cLocation
            // 
            this.cLocation.DataPropertyName = "Location";
            this.cLocation.HeaderText = "Location";
            this.cLocation.MinimumWidth = 15;
            this.cLocation.Name = "cLocation";
            this.cLocation.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cLocation.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.cLocation.Width = 73;
            // 
            // cDetail
            // 
            this.cDetail.DataPropertyName = "Detail";
            this.cDetail.HeaderText = "Detail";
            this.cDetail.MinimumWidth = 15;
            this.cDetail.Name = "cDetail";
            this.cDetail.Width = 59;
            // 
            // cFCAT_ID
            // 
            this.cFCAT_ID.DataPropertyName = "FCAT_ID";
            this.cFCAT_ID.HeaderText = "FCAT_ID";
            this.cFCAT_ID.MinimumWidth = 15;
            this.cFCAT_ID.Name = "cFCAT_ID";
            this.cFCAT_ID.Width = 76;
            // 
            // cFixedAsset_CateName
            // 
            this.cFixedAsset_CateName.DataPropertyName = "FixedAsset_CateName";
            this.cFixedAsset_CateName.HeaderText = "FixedAsset_CateName";
            this.cFixedAsset_CateName.MinimumWidth = 15;
            this.cFixedAsset_CateName.Name = "cFixedAsset_CateName";
            this.cFixedAsset_CateName.Width = 139;
            // 
            // cFBA_ID
            // 
            this.cFBA_ID.DataPropertyName = "FBA_ID";
            this.cFBA_ID.HeaderText = "FBA_ID";
            this.cFBA_ID.MinimumWidth = 15;
            this.cFBA_ID.Name = "cFBA_ID";
            this.cFBA_ID.Width = 69;
            // 
            // cFBrand
            // 
            this.cFBrand.DataPropertyName = "FBrand";
            this.cFBrand.HeaderText = "FBrand";
            this.cFBrand.MinimumWidth = 15;
            this.cFBrand.Name = "cFBrand";
            this.cFBrand.Width = 66;
            // 
            // cFMD_ID
            // 
            this.cFMD_ID.DataPropertyName = "FMD_ID";
            this.cFMD_ID.HeaderText = "FMD_ID";
            this.cFMD_ID.MinimumWidth = 15;
            this.cFMD_ID.Name = "cFMD_ID";
            this.cFMD_ID.Width = 72;
            // 
            // cFModel
            // 
            this.cFModel.DataPropertyName = "FModel";
            this.cFModel.HeaderText = "FModel";
            this.cFModel.MinimumWidth = 15;
            this.cFModel.Name = "cFModel";
            this.cFModel.Width = 67;
            // 
            // cOwnerShip
            // 
            this.cOwnerShip.DataPropertyName = "OwnerShip";
            this.cOwnerShip.HeaderText = "OwnerShip";
            this.cOwnerShip.MinimumWidth = 15;
            this.cOwnerShip.Name = "cOwnerShip";
            this.cOwnerShip.Width = 84;
            // 
            // cInstallDate
            // 
            this.cInstallDate.DataPropertyName = "InstallDate";
            this.cInstallDate.HeaderText = "InstallDate";
            this.cInstallDate.MinimumWidth = 15;
            this.cInstallDate.Name = "cInstallDate";
            this.cInstallDate.Width = 82;
            // 
            // cWarrantDate
            // 
            this.cWarrantDate.DataPropertyName = "WarrantDate";
            this.cWarrantDate.HeaderText = "WarrantDate";
            this.cWarrantDate.MinimumWidth = 15;
            this.cWarrantDate.Name = "cWarrantDate";
            this.cWarrantDate.Width = 93;
            // 
            // cSTA_ID
            // 
            this.cSTA_ID.DataPropertyName = "STA_ID";
            this.cSTA_ID.HeaderText = "STA_ID";
            this.cSTA_ID.MinimumWidth = 15;
            this.cSTA_ID.Name = "cSTA_ID";
            this.cSTA_ID.Width = 70;
            // 
            // cStationSys
            // 
            this.cStationSys.DataPropertyName = "StationSys";
            this.cStationSys.HeaderText = "StationSys";
            this.cStationSys.MinimumWidth = 15;
            this.cStationSys.Name = "cStationSys";
            this.cStationSys.Width = 82;
            // 
            // cAddress
            // 
            this.cAddress.DataPropertyName = "Address";
            this.cAddress.HeaderText = "Address";
            this.cAddress.MinimumWidth = 15;
            this.cAddress.Name = "cAddress";
            this.cAddress.Width = 70;
            // 
            // cTelNo
            // 
            this.cTelNo.DataPropertyName = "TelNo";
            this.cTelNo.HeaderText = "TelNo";
            this.cTelNo.MinimumWidth = 15;
            this.cTelNo.Name = "cTelNo";
            this.cTelNo.Width = 61;
            // 
            // cFaxNo
            // 
            this.cFaxNo.DataPropertyName = "FaxNo";
            this.cFaxNo.HeaderText = "FaxNo";
            this.cFaxNo.MinimumWidth = 15;
            this.cFaxNo.Name = "cFaxNo";
            this.cFaxNo.Width = 63;
            // 
            // cProvince
            // 
            this.cProvince.DataPropertyName = "Province";
            this.cProvince.HeaderText = "Province";
            this.cProvince.MinimumWidth = 15;
            this.cProvince.Name = "cProvince";
            this.cProvince.Width = 74;
            // 
            // cPartType1
            // 
            this.cPartType1.DataPropertyName = "PartType1";
            this.cPartType1.HeaderText = "PartType1";
            this.cPartType1.MinimumWidth = 15;
            this.cPartType1.Name = "cPartType1";
            this.cPartType1.Width = 81;
            // 
            // cPartType2
            // 
            this.cPartType2.DataPropertyName = "PartType2";
            this.cPartType2.HeaderText = "PartType2";
            this.cPartType2.MinimumWidth = 15;
            this.cPartType2.Name = "cPartType2";
            this.cPartType2.Width = 81;
            // 
            // cPartType3
            // 
            this.cPartType3.DataPropertyName = "PartType3";
            this.cPartType3.HeaderText = "PartType3";
            this.cPartType3.MinimumWidth = 15;
            this.cPartType3.Name = "cPartType3";
            this.cPartType3.Width = 81;
            // 
            // cCheckGauge
            // 
            this.cCheckGauge.DataPropertyName = "CheckGauge";
            this.cCheckGauge.HeaderText = "CheckGauge";
            this.cCheckGauge.MinimumWidth = 15;
            this.cCheckGauge.Name = "cCheckGauge";
            this.cCheckGauge.Width = 95;
            // 
            // cControlFixAssetNo
            // 
            this.cControlFixAssetNo.DataPropertyName = "ControlFixAssetNo";
            this.cControlFixAssetNo.HeaderText = "ControlFixAssetNo";
            this.cControlFixAssetNo.MinimumWidth = 15;
            this.cControlFixAssetNo.Name = "cControlFixAssetNo";
            this.cControlFixAssetNo.Width = 118;
            // 
            // cTranDate
            // 
            this.cTranDate.DataPropertyName = "TranDate";
            this.cTranDate.HeaderText = "TranDate";
            this.cTranDate.MinimumWidth = 15;
            this.cTranDate.Name = "cTranDate";
            this.cTranDate.Width = 77;
            // 
            // cTranEMP
            // 
            this.cTranEMP.DataPropertyName = "TranEMP";
            this.cTranEMP.HeaderText = "TranEMP";
            this.cTranEMP.MinimumWidth = 15;
            this.cTranEMP.Name = "cTranEMP";
            this.cTranEMP.Width = 77;
            // 
            // cConfirmEMP
            // 
            this.cConfirmEMP.DataPropertyName = "ConfirmEMP";
            this.cConfirmEMP.HeaderText = "ConfirmEMP";
            this.cConfirmEMP.MinimumWidth = 15;
            this.cConfirmEMP.Name = "cConfirmEMP";
            this.cConfirmEMP.Width = 90;
            // 
            // cConfirmDate
            // 
            this.cConfirmDate.DataPropertyName = "ConfirmDate";
            this.cConfirmDate.HeaderText = "ConfirmDate";
            this.cConfirmDate.MinimumWidth = 15;
            this.cConfirmDate.Name = "cConfirmDate";
            this.cConfirmDate.Width = 90;
            // 
            // cIsLifetime
            // 
            this.cIsLifetime.DataPropertyName = "IsLifetime";
            this.cIsLifetime.HeaderText = "Lifetime";
            this.cIsLifetime.MinimumWidth = 15;
            this.cIsLifetime.Name = "cIsLifetime";
            this.cIsLifetime.ReadOnly = true;
            this.cIsLifetime.Width = 49;
            // 
            // cIsSpecialPrice
            // 
            this.cIsSpecialPrice.DataPropertyName = "IsSpecialPrice";
            this.cIsSpecialPrice.HeaderText = "SpecialPrice";
            this.cIsSpecialPrice.MinimumWidth = 15;
            this.cIsSpecialPrice.Name = "cIsSpecialPrice";
            this.cIsSpecialPrice.ReadOnly = true;
            this.cIsSpecialPrice.Width = 72;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label11.Location = new System.Drawing.Point(10, 35);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(384, 17);
            this.label11.TabIndex = 22;
            this.label11.Text = "บันทึกข้อมูล Fixed Asset:=> ใช้งานขณะ เชื่อมต่อ อินเตอร์เน็ต เท่านั้น";
            // 
            // txtPartTypeWt1
            // 
            this.txtPartTypeWt1.Location = new System.Drawing.Point(121, 19);
            this.txtPartTypeWt1.Name = "txtPartTypeWt1";
            this.txtPartTypeWt1.Size = new System.Drawing.Size(53, 20);
            this.txtPartTypeWt1.TabIndex = 26;
            this.txtPartTypeWt1.Text = "0";
            this.txtPartTypeWt1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtPartTypeWt1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPartTypeWt1_KeyPress);
            // 
            // txtPartTypeWt2
            // 
            this.txtPartTypeWt2.Location = new System.Drawing.Point(121, 42);
            this.txtPartTypeWt2.Name = "txtPartTypeWt2";
            this.txtPartTypeWt2.Size = new System.Drawing.Size(53, 20);
            this.txtPartTypeWt2.TabIndex = 27;
            this.txtPartTypeWt2.Text = "0";
            this.txtPartTypeWt2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtPartTypeWt2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPartTypeWt2_KeyPress);
            // 
            // txtPartTypeWt3
            // 
            this.txtPartTypeWt3.Location = new System.Drawing.Point(349, 19);
            this.txtPartTypeWt3.Name = "txtPartTypeWt3";
            this.txtPartTypeWt3.Size = new System.Drawing.Size(53, 20);
            this.txtPartTypeWt3.TabIndex = 28;
            this.txtPartTypeWt3.Text = "0";
            this.txtPartTypeWt3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtPartTypeWt3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPartTypeWt3_KeyPress);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(179, 24);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(14, 13);
            this.label12.TabIndex = 27;
            this.label12.Text = "ปี";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(179, 47);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(14, 13);
            this.label13.TabIndex = 28;
            this.label13.Text = "ปี";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(406, 22);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(14, 13);
            this.label14.TabIndex = 29;
            this.label14.Text = "ปี";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label19);
            this.groupBox5.Controls.Add(this.txtCheckGauge);
            this.groupBox5.Controls.Add(this.label18);
            this.groupBox5.Controls.Add(this.label14);
            this.groupBox5.Controls.Add(this.label13);
            this.groupBox5.Controls.Add(this.txtPartTypeWt3);
            this.groupBox5.Controls.Add(this.label12);
            this.groupBox5.Controls.Add(this.txtPartTypeWt2);
            this.groupBox5.Controls.Add(this.txtPartTypeWt1);
            this.groupBox5.Controls.Add(this.label17);
            this.groupBox5.Controls.Add(this.label16);
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Location = new System.Drawing.Point(216, 225);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(449, 72);
            this.groupBox5.TabIndex = 31;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "รับประกันอะไหล่";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(406, 47);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(14, 13);
            this.label19.TabIndex = 32;
            this.label19.Text = "ปี";
            // 
            // txtCheckGauge
            // 
            this.txtCheckGauge.Location = new System.Drawing.Point(349, 42);
            this.txtCheckGauge.Name = "txtCheckGauge";
            this.txtCheckGauge.Size = new System.Drawing.Size(53, 20);
            this.txtCheckGauge.TabIndex = 31;
            this.txtCheckGauge.Text = "0";
            this.txtCheckGauge.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtCheckGauge.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCheckGauge_KeyPress);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(199, 47);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(150, 13);
            this.label18.TabIndex = 30;
            this.label18.Text = "รับประกันค่าตรวจสอบมาตรวัด:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(231, 24);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(118, 13);
            this.label17.TabIndex = 2;
            this.label17.Text = "PartType 3 (เบรคอะเว):";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(8, 47);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(107, 13);
            this.label16.TabIndex = 1;
            this.label16.Text = "PartType 2 (สายจ่าย):";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(13, 24);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(102, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "PartType 1 (มือจ่าย):";
            // 
            // txtFixedAssetCust
            // 
            this.txtFixedAssetCust.Location = new System.Drawing.Point(198, 199);
            this.txtFixedAssetCust.Name = "txtFixedAssetCust";
            this.txtFixedAssetCust.Size = new System.Drawing.Size(147, 20);
            this.txtFixedAssetCust.TabIndex = 32;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(195, 183);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(130, 13);
            this.label20.TabIndex = 33;
            this.label20.Text = "Control FixAsset ของลูกค้า:";
            // 
            // lblIsConfirmFFA
            // 
            this.lblIsConfirmFFA.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblIsConfirmFFA.AutoSize = true;
            this.lblIsConfirmFFA.BackColor = System.Drawing.Color.Transparent;
            this.lblIsConfirmFFA.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblIsConfirmFFA.ForeColor = System.Drawing.Color.Red;
            this.lblIsConfirmFFA.Location = new System.Drawing.Point(400, 35);
            this.lblIsConfirmFFA.Name = "lblIsConfirmFFA";
            this.lblIsConfirmFFA.Size = new System.Drawing.Size(138, 19);
            this.lblIsConfirmFFA.TabIndex = 34;
            this.lblIsConfirmFFA.Text = "lblIsConfirmFFA";
            // 
            // chxFFAConf
            // 
            this.chxFFAConf.AutoSize = true;
            this.chxFFAConf.Location = new System.Drawing.Point(352, 199);
            this.chxFFAConf.Name = "chxFFAConf";
            this.chxFFAConf.Size = new System.Drawing.Size(56, 17);
            this.chxFFAConf.TabIndex = 35;
            this.chxFFAConf.Text = "ยืนยัน";
            this.chxFFAConf.UseVisualStyleBackColor = true;
            // 
            // cbxLocation
            // 
            this.cbxLocation.FormattingEnabled = true;
            this.cbxLocation.Location = new System.Drawing.Point(421, 198);
            this.cbxLocation.Name = "cbxLocation";
            this.cbxLocation.Size = new System.Drawing.Size(146, 21);
            this.cbxLocation.TabIndex = 57;
            // 
            // ControlFFA
            // 
            this.ControlFFA.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ControlFFA.Controls.Add(this.btControlFFA);
            this.ControlFFA.Location = new System.Drawing.Point(921, 51);
            this.ControlFFA.Name = "ControlFFA";
            this.ControlFFA.Size = new System.Drawing.Size(1792, 168);
            this.ControlFFA.TabIndex = 58;
            this.ControlFFA.TabStop = false;
            // 
            // btControlFFA
            // 
            this.btControlFFA.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btControlFFA.Location = new System.Drawing.Point(50, 52);
            this.btControlFFA.Name = "btControlFFA";
            this.btControlFFA.Size = new System.Drawing.Size(157, 61);
            this.btControlFFA.TabIndex = 0;
            this.btControlFFA.Text = "ควบคุม Fixed Asset";
            this.btControlFFA.UseVisualStyleBackColor = true;
            this.btControlFFA.Click += new System.EventHandler(this.btControlFFA_Click);
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewImageColumn1.DataPropertyName = "DelSN";
            this.dataGridViewImageColumn1.FillWeight = 30F;
            this.dataGridViewImageColumn1.HeaderText = "ลบSN";
            this.dataGridViewImageColumn1.Image = global::SMSMINI.Properties.Resources.delete;
            this.dataGridViewImageColumn1.MinimumWidth = 15;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewImageColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewImageColumn1.Width = 60;
            // 
            // chxLifetime
            // 
            this.chxLifetime.AutoSize = true;
            this.chxLifetime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.chxLifetime.Location = new System.Drawing.Point(702, 242);
            this.chxLifetime.Name = "chxLifetime";
            this.chxLifetime.Size = new System.Drawing.Size(70, 17);
            this.chxLifetime.TabIndex = 59;
            this.chxLifetime.Text = "Lifetime";
            this.chxLifetime.UseVisualStyleBackColor = true;
            // 
            // chxSpecialPrice
            // 
            this.chxSpecialPrice.AutoSize = true;
            this.chxSpecialPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.chxSpecialPrice.Location = new System.Drawing.Point(702, 270);
            this.chxSpecialPrice.Name = "chxSpecialPrice";
            this.chxSpecialPrice.Size = new System.Drawing.Size(86, 17);
            this.chxSpecialPrice.TabIndex = 60;
            this.chxSpecialPrice.Text = "ราคาพิเศษ";
            this.chxSpecialPrice.UseVisualStyleBackColor = true;
            // 
            // btnChangeModelFFA
            // 
            this.btnChangeModelFFA.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btnChangeModelFFA.ForeColor = System.Drawing.Color.Firebrick;
            this.btnChangeModelFFA.Location = new System.Drawing.Point(13, 241);
            this.btnChangeModelFFA.Name = "btnChangeModelFFA";
            this.btnChangeModelFFA.Size = new System.Drawing.Size(185, 45);
            this.btnChangeModelFFA.TabIndex = 61;
            this.btnChangeModelFFA.Text = "เปลี่ยนแปลงข้อมูล Fixed Asset";
            this.btnChangeModelFFA.UseVisualStyleBackColor = true;
            this.btnChangeModelFFA.Click += new System.EventHandler(this.btnChangeModelFFA_Click);
            // 
            // Input_FFA
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1174, 459);
            this.Controls.Add(this.btnChangeModelFFA);
            this.Controls.Add(this.chxSpecialPrice);
            this.Controls.Add(this.chxLifetime);
            this.Controls.Add(this.ControlFFA);
            this.Controls.Add(this.cbxLocation);
            this.Controls.Add(this.chxFFAConf);
            this.Controls.Add(this.lblIsConfirmFFA);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.txtFixedAssetCust);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.dgFFA);
            this.Controls.Add(this.btAdd);
            this.Controls.Add(this.txtRemark);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtSN);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.Group1);
            this.Controls.Add(this.toolStrip_Mnu);
            this.Name = "Input_FFA";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "บันทึกข้อมูล Fixed Asset";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Input_FFA_FormClosed);
            this.Load += new System.EventHandler(this.Input_FFA_Load);
            this.toolStrip_Mnu.ResumeLayout(false);
            this.toolStrip_Mnu.PerformLayout();
            this.Group1.ResumeLayout(false);
            this.Group1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgFFA)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ControlFFA.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip_Mnu;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        public System.Windows.Forms.ToolStripTextBox tst_txtFind;

        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripButton_FindStation;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton toolStripButton_Save;
        private System.Windows.Forms.ToolStripButton toolStripButton_Cancel;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButton_Exit;
        private System.Windows.Forms.ToolStripLabel toolStripLabel_Dateime;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.GroupBox Group1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comb_cat;
        private System.Windows.Forms.ComboBox comb_model;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtInstall;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtExpireDate;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtSN;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtRemark;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btAdd;
        private System.Windows.Forms.TextBox txtStaName;
        private System.Windows.Forms.TextBox txtSTA_ID;
        private System.Windows.Forms.DataGridView dgFFA;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox comb_brand;
        private System.Windows.Forms.ToolStripComboBox toolStripLabel_cobby;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comb_cus_flo;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.ToolStripButton toolStripBt_PrintFFA;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtPartTypeWt1;
        private System.Windows.Forms.TextBox txtPartTypeWt3;
        private System.Windows.Forms.TextBox txtPartTypeWt2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtYearWarranty;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtCheckGauge;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtFixedAssetCust;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.CheckBox chkInstallDate;
        private System.Windows.Forms.Label lblIsConfirmFFA;
        private System.Windows.Forms.CheckBox chxFFAConf;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtTranEmp;
        private System.Windows.Forms.ComboBox cbxLocation;
        private System.Windows.Forms.GroupBox ControlFFA;
        private System.Windows.Forms.Button btControlFFA;
        private System.Windows.Forms.RadioButton rdMonth;
        private System.Windows.Forms.RadioButton rdYear;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.CheckBox chxLifetime;
        private System.Windows.Forms.CheckBox chxSpecialPrice;
        private System.Windows.Forms.DataGridViewImageColumn cDelSN;
        private System.Windows.Forms.DataGridViewTextBoxColumn SerialNumber;
        private System.Windows.Forms.DataGridViewCheckBoxColumn cIsConfirm;
        private System.Windows.Forms.DataGridViewComboBoxColumn cLocation;
        private System.Windows.Forms.DataGridViewTextBoxColumn cDetail;
        private System.Windows.Forms.DataGridViewTextBoxColumn cFCAT_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn cFixedAsset_CateName;
        private System.Windows.Forms.DataGridViewTextBoxColumn cFBA_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn cFBrand;
        private System.Windows.Forms.DataGridViewTextBoxColumn cFMD_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn cFModel;
        private System.Windows.Forms.DataGridViewTextBoxColumn cOwnerShip;
        private System.Windows.Forms.DataGridViewTextBoxColumn cInstallDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn cWarrantDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn cSTA_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn cStationSys;
        private System.Windows.Forms.DataGridViewTextBoxColumn cAddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn cTelNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn cFaxNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn cProvince;
        private System.Windows.Forms.DataGridViewTextBoxColumn cPartType1;
        private System.Windows.Forms.DataGridViewTextBoxColumn cPartType2;
        private System.Windows.Forms.DataGridViewTextBoxColumn cPartType3;
        private System.Windows.Forms.DataGridViewTextBoxColumn cCheckGauge;
        private System.Windows.Forms.DataGridViewTextBoxColumn cControlFixAssetNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn cTranDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn cTranEMP;
        private System.Windows.Forms.DataGridViewTextBoxColumn cConfirmEMP;
        private System.Windows.Forms.DataGridViewTextBoxColumn cConfirmDate;
        private System.Windows.Forms.DataGridViewCheckBoxColumn cIsLifetime;
        private System.Windows.Forms.DataGridViewCheckBoxColumn cIsSpecialPrice;
        private System.Windows.Forms.Button btnChangeModelFFA;
    }
}