﻿using CrystalDecisions.Shared;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;


namespace SMSMINI.FFA
{
    public partial class Input_FFA : Form
    {
        public bool IsConfirmFFA { get; set; }//ยืนยัน FFA หน้าสถานี โดย VAN 

        private ProgressBar.FormProgress m_fmProgress = null;
        DateTime servDate = DateTime.Now;
        List<DAL.SMSManage.vw_FFA_Station_Detail> ffStD = new List<DAL.SMSManage.vw_FFA_Station_Detail>();

        //===20180328
        public List<string> vLJOBChkTypFFA = new List<string>();
        public string vFrmUpload { get; set; }
        //===


        public string statID { get; set; }

        string mode = "";
        public Input_FFA()
        {
            InitializeComponent();
        }

        private void Input_FFA_Load(object sender, EventArgs e)
        {



            //System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

            //-==20161222 มีคนเอาข้างบนออกแต่ลองเอาใส่ดูซิว่าจะได้เป่า
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("fr-FR");

            mode = "LOAD";
            SetControlEnable(false, false, false, false, false);
            CleareSNText();

            //===20170202==group controlFFA==
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            {
                //string vEmp = UserInfo.UserId;
                var mnFFA = dc.Employees.Where(mn => mn.EMP_ID == UserInfo.UserId && new string[] { "07", "11", "14" }.Contains(mn.DEP_ID)).FirstOrDefault();
                if (mnFFA != null)
                {
                    ControlFFA.Enabled = true;
                }
                else
                {
                    ControlFFA.Enabled = false;
                }

            }
            //UserInfo.UserId

            //=============


            try
            {
                //20141110 dtExpireDate.Value = dtInstall.Value.AddYears(2); 
                if (chkInstallDate.Checked == true)
                {

                    dtExpireDate.Value = dtInstall.Value.AddYears(2);

                }
                else
                {
                    dtInstall.Format = DateTimePickerFormat.Custom;
                    dtExpireDate.Format = DateTimePickerFormat.Custom;
                    dtInstall.CustomFormat = "";
                    dtExpireDate.CustomFormat = "";
                }


                using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
                {
                    var srv = dc
                         .ExecuteQuery<DateTime>("SELECT GETDATE()")
                         .First();
                    if (srv != null)
                    {
                        servDate = srv;
                    }

                    comb_cat.DataSource = dc.FFixedAsset_Categories
                        .Where(t => t.IsCancel == false)
                        .Select(t => new
                        {
                            cateID = t.FCAT_ID,
                            cateName = t.FCAT_ID.ToString() + ": " + t.FixedAsset_CateName
                        })
                        .ToList();
                    comb_cat.DisplayMember = "cateName";
                    comb_cat.ValueMember = "cateID";

                    comb_brand.DataSource = dc.FFixedAsset_Brands

                        .Select(t => new
                        {
                            brandID = t.FBA_ID,
                            brand = t.FBA_ID.ToString() + ": " + t.FBrand
                        })
                        .ToList();
                    comb_brand.DisplayMember = "brand";
                    comb_brand.ValueMember = "brandID";

                    comb_model.DataSource = dc.FFixedAsset_Models
                        .Select(t => new
                        {
                            modelID = t.FMD_ID,
                            FModel = t.FMD_ID.ToString() + ": " + t.FModel
                        })
                        .ToList();
                    comb_model.DisplayMember = "FModel";
                    comb_model.ValueMember = "modelID";

                    //21041208

                    cbxLocation.DataSource = dc.Locations
                        //20210201 .Where(t => t.LocType.Contains("D") || t.LOC_ID == 0)
                        .Where(t => t.IsCancel == false)
                        .Select(t => new
                        {
                            LOC_ID = t.LOC_ID,
                            Location = t.LOC_ID.ToString() + ": " + t.Location1
                        })
                       .OrderBy(t => t.LOC_ID).ToList();

                    cbxLocation.DisplayMember = "Location";
                    cbxLocation.ValueMember = "LOC_ID";



                    /* //20170116
                     **/
                    /*20210201*/
                    cLocation.DataSource = dc.Locations
                        //20210201 .Where(t => t.LocType.Contains("D") || t.LOC_ID == 0)
                        .Where(t => t.IsCancel == false)
                        .Select(t => new
                        {
                            Location = t.LOC_ID,
                            LocationDetail = t.LOC_ID.ToString() + ": " + t.Location1
                        })
                       .OrderBy(t => t.Location).ToList();

                    cLocation.DisplayMember = "LocationDetail";
                    cLocation.ValueMember = "Location";


                    //20170216 ลองกลับไปใช้ข้างบน
                    //cLocation.DataSource = dc.Locations
                    //    .Where(t => t.LocType.Contains("D") || t.LOC_ID == 0)
                    //    .Select(t => new
                    //    {
                    //        gvLocation = t.LOC_ID,
                    //        gvLocationDetail = t.LOC_ID.ToString() + ": " + t.Location1
                    //    })
                    //   .OrderBy(t => t.gvLocation).ToList();

                    //cLocation.DisplayMember = "gvLocationDetail";
                    //cLocation.ValueMember = "gvLocation";

                }
                ////20141209
                comb_cus_flo.SelectedItem = "Customer";
                //var ownerShip = "";
                //switch (comb_cus_flo.SelectedItem.ToString())
                //{
                //    case "Flowco": ownerShip = "FLO"; break;
                //    case "Customer": ownerShip = "CUS"; break;
                //    case "Uniwave": ownerShip = "UNI"; break;
                //    //default:
                //    default: ownerShip = "Customer";
                //        break;
                //}

            }
            catch (Exception)
            {
            }



            if (IsConfirmFFA)
            {
                lblIsConfirmFFA.Text = "[ยืนยัน ความถูกต้อง ของข้อมูล Fixed Asset]";
                this.Text = "[ยืนยัน ความถูกต้อง ของข้อมูล Fixed Asset]";
                chxFFAConf.Enabled = true;
                txtTranEmp.Text = "";
                tst_txtFind.Text = statID;

                toolStripLabel_cobby.SelectedIndex = 1;
                //toolStripButton_FindStation_Click(null,null);

                //20141003
                txtTranEmp.Text = UserInfo.UserId;

                //20170704
                //toolStripButton_FindStation_Click(null, null);
                if (statID != null)
                {
                    using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
                    {

                        tst_txtFind.Text = statID;

                        var ss = (from st in dc.Stations
                                  where st.STA_ID == tst_txtFind.Text.Trim()
                                  select st).FirstOrDefault();

                        tmpStation = ss;


                        if (tmpStation != null)
                        {
                            txtSTA_ID.Text = tmpStation.STA_ID;
                            txtStaName.Text = "ชื่อ: " + tmpStation.StationSys + " ที่อยู่: " + tmpStation.Address;

                            getFFA_Detail(dc, tmpStation);

                            SetControlEnable(false, true, true, false, false);
                            //20141002
                            setDataGridViewStyle();


                            //20141110

                            txtYearWarranty.Text = "0";
                            dtInstall.CustomFormat = " ";
                            chkInstallDate.Checked = false;
                            dtExpireDate.CustomFormat = " ";
                        }
                    }
                } //if (statID != "")

            }
            else
            {
                lblIsConfirmFFA.Text = "[บันทึก ข้อมูล Fixed Asset]";
                this.Text = "[บันทึก ข้อมูล Fixed Asset]";
                chxFFAConf.Enabled = false;
                //20140924
                txtTranEmp.Text = UserInfo.UserId;
            }

            toolStripLabel_cobby.SelectedIndex = 1;

        }

        private void SetControlEnable(bool btadd, bool groupBox, bool txtsn, bool Location, bool remark)
        {
            btAdd.Enabled = btadd;
            groupBox5.Enabled = btadd;
            groupBox2.Enabled = groupBox;
            txtSN.Enabled = txtsn;
            //20141208 txtLocation.Enabled = Location;
            txtRemark.Enabled = remark;

            //20141208
            txtFixedAssetCust.Enabled = Location;
            cbxLocation.Enabled = Location;
        }

        DAL.SMSManage.Station tmpStation = null;

        private void toolStripButton_FindStation_Click(object sender, EventArgs e)
        {
            mode = "FIND";

            if (toolStripLabel_cobby.Text == "ค้นหาข้อมูล...")
            {
                MessageBox.Show("กรุณา เลือก เงื่อนไข การค้นหา...", "ผลการตรวจสอบ...", MessageBoxButtons.OK);
                toolStripLabel_cobby.Focus();
                toolStripLabel_cobby.Select();
                return;

            }

            if (tst_txtFind.Text != string.Empty)
            {
                var f = new FFA.FindStation();
                if (toolStripLabel_cobby.Text == "ตาม JOB ID")
                {
                    f.findType = "JOB";
                    f.jobID = tst_txtFind.Text.Trim();
                }
                else
                {
                    f.findType = "STA";
                    f.staID = tst_txtFind.Text.Trim();
                }

                if (f.ShowDialog() == DialogResult.OK)
                {


                    using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
                    {


                        if (toolStripLabel_cobby.Text == "ตาม JOB ID")
                        {
                            tst_txtFind.Text = f.jobID;

                            var jj = (from j in dc.JOBs
                                      from st in dc.Stations
                                      where j.STA_ID == st.STA_ID &&
                                      j.JOB_ID.Contains(tst_txtFind.Text)
                                      select st).FirstOrDefault();

                            tmpStation = jj;
                        }
                        else if (toolStripLabel_cobby.Text == "ตาม สถานี")
                        {
                            tst_txtFind.Text = f.staID;

                            var ss = (from st in dc.Stations
                                      where st.STA_ID == tst_txtFind.Text.Trim()
                                      select st).FirstOrDefault();

                            tmpStation = ss;
                        }

                        if (tmpStation != null)
                        {
                            txtSTA_ID.Text = tmpStation.STA_ID;
                            txtStaName.Text = "ชื่อ: " + tmpStation.StationSys + " ที่อยู่: " + tmpStation.Address;

                            getFFA_Detail(dc, tmpStation);

                            SetControlEnable(false, true, true, false, false);
                            //20141002
                            setDataGridViewStyle();


                            //20141110

                            txtYearWarranty.Text = "0";
                            dtInstall.CustomFormat = " ";
                            chkInstallDate.Checked = false;
                            dtExpireDate.CustomFormat = " ";
                        }
                        else
                        {
                            MessageBox.Show("ไม่มีข้อมูล กรุณาป้อนใหม่...", "ผลการค้นหา", MessageBoxButtons.OK);
                            tst_txtFind.Focus();
                        }
                    }

                }
            }
        }

        private void getFFA_Detail(SMSMINI.DAL.SMSManage.SMSManageDataContext dc, SMSMINI.DAL.SMSManage.Station station)
        {
            //==========20180328
            //if (vFrmUpload == "1")
            //{
            //    var  q = dc.vw_FFA_Station_Details
            //        .Where(t => t.STA_ID == station.STA_ID


            //         && vLJOBChkTypFFA.Contains(t.FCAT_ID.ToString()))
            //        .ToList();

            //    ffStD = q;
            //    dgFFA.DataSource = ffStD.ToList();
            //}

            //else {
            //    var q = dc.vw_FFA_Station_Details
            //            .Where(t => t.STA_ID == station.STA_ID)
            //             .ToList();

            //    ffStD = q;
            //    dgFFA.DataSource = ffStD.ToList();
            //}
            //=================

            var q = dc.vw_FFA_Station_Details
                      .Where(t => t.STA_ID == station.STA_ID)
                       .ToList();

            ffStD = q;
            dgFFA.DataSource = ffStD.ToList();
            //dgFFA.Columns["cDelSN"].Width = 10;

        }





        private void toolStripText_txtFind_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                toolStripButton_FindStation_Click(null, null);
            }
        }

        private void toolStripButton_Save_Click(object sender, EventArgs e)
        {

            if (MessageBox.Show("คุณต้องการ บันทึกข้อมูล Fixed Asset ใช่หรือไม่???...", "คำยืนยัน",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                //===20141111
                if (IsConfirmFFA)//20180409
                {
                    if (vLJOBChkTypFFA.Count() != 0)
                    {
                        //ยืนยัน ความถูกต้อง ของข้อมูล Fixed Asset
                        foreach (var item2 in ffStD)
                        {

                            var chkTypeSD = vLJOBChkTypFFA.Where(g => g.Contains(item2.FCAT_ID.ToString())).FirstOrDefault(); //20180409
                            if (chkTypeSD != null)//20180409
                            {//20180409
                             //20171124======================================

                                if (item2.IsConfirm.ToString() != "false" && !string.IsNullOrEmpty(item2.IsConfirm.ToString()))
                                {
                                    if ((item2.IsConfirm.Value == true && item2.Location.Value.ToString() == "0") || (item2.IsConfirm.Value == true && string.IsNullOrEmpty(item2.Location.Value.ToString())))
                                    {
                                        MessageBox.Show("SN : " + item2.SerialNumber + " ห้ามเลือก Location เป็น ไม่ระบุ... " + Environment.NewLine +
                                        " เนื่องจากจะมีผลต่อการ upload JOB ", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        return;
                                    }
                                }

                                //20171124======================================

                                //20141209  if (string.IsNullOrEmpty(item2.Location) || string.IsNullOrEmpty(item2.Detail))
                                if (string.IsNullOrEmpty(item2.Location.ToString()) || string.IsNullOrEmpty(item2.Detail))
                                {
                                    MessageBox.Show("SN : " + item2.SerialNumber + " กรุณา ระบุ Location และ Detail ", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return;
                                }

                            }//20180409

                        }
                    }
                }//20141208
                //else
                //{
                //    foreach (var item2 in ffStD)
                //    {
                //        if (string.IsNullOrEmpty(item2.Location.ToString()))//201412109 if (string.IsNullOrEmpty(item2.Location))
                //        {
                //            MessageBox.Show("กรุณา ระบุ Location  ", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //            return;
                //        }
                //    }
                //}
                //=========

                newProgressbar();
                invoke_Progress("กำลังดึงเวลาบันทึกข้อมูลจาก Server SMS");

                servDate = getServerDate();
                using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
                {
                    //dc.Connection.Open();

                    dc.Connection.Open();
                    dc.Transaction = dc.Connection.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted);

                    try
                    {
                        //บันทึก FFixedAsset_Station_Category 
                        var fsc = (from t1 in ffStD
                                   group t1 by new { t1.STA_ID, t1.FCAT_ID } into grp
                                   select new DAL.SMSManage.FFixedAsset_Station_Category
                                   {
                                       STA_ID = grp.Key.STA_ID,
                                       FCAT_ID = grp.Key.FCAT_ID,
                                       FQuantity = 0

                                   }).Distinct()
                          .ToList();

                        //Update FQuantity
                        foreach (var item0 in fsc)
                        {
                            foreach (var item1 in ffStD)
                            {
                                if (txtSTA_ID.Text.Trim() == item1.STA_ID &&
                                    item0.FCAT_ID == item1.FCAT_ID)
                                {
                                    item0.FQuantity = item0.FQuantity + 1;
                                }
                            }
                        }
                        /*//==20190430 เปลี่ยน process แทนการลบทิ้ง
                        //===================================//
                        //Delete FFixedAsset_Station_Detail old Data                        
                        //===================================//
                        var delffa = dc.FFixedAsset_Station_Details
                            .Where(t => t.STA_ID == txtSTA_ID.Text.Trim())
                            .ToList();
                            if (delffa != null)
                            {
                                dc.FFixedAsset_Station_Details.DeleteAllOnSubmit(delffa);
                                dc.SubmitChanges();
                            }
                        */
                        //===================================//
                        //Delete FFixedAsset_Station_Categories old Data
                        //===================================//
                        /* st 20210308 ยกเลิกการใช้งาน   var delffc = dc.FFixedAsset_Station_Categories
                                 .Where(t => t.STA_ID == txtSTA_ID.Text.Trim())
                                 .ToList();
                         if (delffc != null)
                         {
                           dc.FFixedAsset_Station_Categories.DeleteAllOnSubmit(delffc);                         
                           dc.SubmitChanges();
                         }

                         ////Insert New 
                         dc.FFixedAsset_Station_Categories.InsertAllOnSubmit(fsc);
                          dc.SubmitChanges();*/

                        //===================================//
                        //บันทึก FFixedAsset_Station_Detail                      
                        //===================================//
                        //Insert New FFixedAsset_Station_Detail Data

                        //20140924 DateTime Confirmdate = dc.ExecuteQuery<DateTime>("SELECT GETDATE()").First();

                        DateTime Confirmdate;


                        //====20180903====
                        DateTime vTranMoveTo;
                        DateTime? srvTime = null;
                        //using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
                        //{

                        srvTime = dc
                           .ExecuteQuery<DateTime>("SELECT GETDATE()")
                           .First();

                        if (srvTime != null)
                        {
                            vTranMoveTo = srvTime.Value;
                        }
                        else
                        {
                            MessageBox.Show("กรุณาเชื่อมต่อ Internet ", "ผลการตรวจสอบ");
                            return;
                        }
                        //}
                        //====20180903====

                        if (IsConfirmFFA)
                        {

                            //ยืนยัน ความถูกต้อง ของข้อมูล Fixed Asset
                            foreach (var item2 in ffStD)
                            {

                                //20180903 ==begin=== ลบ auto ข้อมูลกรณีย้าย fixed asset ไป site อื่น
                                //select หาข้อมูลว่ามีอยู่จริงที่ที่สถานีเก่า
                                /*st 20210308 
                                var vchkD = dc.FFixedAsset_Station_Details
                                   .Where(t => t.SerialNumber == item2.SerialNumber 

                                           &&
                                           t.FCAT_ID == item2.FCAT_ID &&
                                           t.FMD_ID == item2.FMD_ID



                                           && t.STA_ID != item2.STA_ID)
                                   .FirstOrDefault();
                                   */
                                //var vchkD = dc.vw_FFA_Station_Details

                                //            .Where(t => t.SerialNumber == item2.SerialNumber                            
                                //             && t.STA_ID != item2.STA_ID)
                                //            .FirstOrDefault();





                                var vchkD = dc.FFixedAsset_Station_Details
                                            .Where(t => t.SerialNumber == item2.SerialNumber
                                             && t.STA_ID != item2.STA_ID)
                                            .FirstOrDefault();

                                if (vchkD != null)
                                {
                                    //==20210308
                                    var vchkD1 = (from t in dc.vw_FFA_Station_Details
                                                  join l in dc.Locations on t.Location equals l.LOC_ID
                                                  where t.SerialNumber == item2.SerialNumber
                                                  && t.STA_ID != item2.STA_ID
                                                  select new
                                                  {
                                                      t.SerialNumber,
                                                      t.FCAT_ID,
                                                      t.FBA_ID,
                                                      t.FMD_ID,
                                                      t.STA_ID,
                                                      t.OwnerShip,
                                                      t.InstallDate,
                                                      t.WarrantDate,
                                                      Location = l.Location1,
                                                      l.LOC_ID,
                                                      t.Detail,
                                                      t.TranDate,
                                                      t.PartType1,
                                                      t.PartType2,
                                                      t.PartType3,
                                                      t.CheckGauge,
                                                      t.TranEMP,
                                                      t.ControlFixAssetNo,
                                                      t.IsConfirm,
                                                      t.ConfirmEMP,
                                                      t.ConfirmDate,

                                                  })
                                              .FirstOrDefault();
                                    //==
                                    //เก็บ log Fixed Asset ของสถานีเก่า
                                    var newLog = new DAL.SMSManage.FFixedAsset_Station_Detail_logMove
                                    {

                                        SerialNumber = vchkD1.SerialNumber,
                                        FCAT_ID = vchkD1.FCAT_ID,
                                        FBA_ID = vchkD1.FBA_ID,
                                        FMD_ID = vchkD1.FMD_ID,
                                        STA_ID = vchkD1.STA_ID,
                                        OwnerShip = vchkD1.OwnerShip,
                                        InstallDate = vchkD1.InstallDate,
                                        WarrantDate = vchkD1.WarrantDate,
                                        Location = vchkD1.Location,
                                        LOC_ID = vchkD1.LOC_ID,
                                        Detail = vchkD1.Detail,
                                        TranDate = vchkD1.TranDate,
                                        PartType1 = vchkD1.PartType1,
                                        PartType2 = vchkD1.PartType2,
                                        PartType3 = vchkD1.PartType3,
                                        CheckGauge = vchkD1.CheckGauge,
                                        TranEMP = vchkD1.TranEMP,
                                        ControlFixAssetNo = vchkD1.ControlFixAssetNo,
                                        IsConfirm = vchkD1.IsConfirm,
                                        ConfirmEMP = vchkD1.ConfirmEMP,
                                        ConfirmDate = vchkD1.ConfirmDate,
                                        STA_ID_MoveTo = item2.STA_ID,
                                        TranDate_MoveTo = vTranMoveTo,
                                        TranEMP_MoveTo = UserInfo.UserId
                                    };

                                    dc.FFixedAsset_Station_Detail_logMoves.InsertOnSubmit(newLog);
                                    dc.SubmitChanges();

                                    //===20190430==
                                    var ld = new DAL.SMSManage.Log_DeleteDatabyProgram
                                    {
                                        Trandate = servDate,
                                        DataID = vchkD.SerialNumber,//txtJOB_ID.Text.Trim(),
                                        TableName = "FFixedAsset_Station_Detail",
                                        Menu = "บันทึก_ยืนยัน Fixed Asset",
                                        Detail = "Move SN " + vchkD.SerialNumber + " STA_ID " + vchkD.STA_ID + " to " + item2.STA_ID,
                                        UserID = UserInfo.UserId,
                                        Apps = "SMSMINI"

                                    };

                                    dc.Log_DeleteDatabyPrograms.InsertOnSubmit(ld);
                                    dc.SubmitChanges();
                                    //=============

                                    //ลด f_station_category ที่สถานีเดิม
                                    //บันทึก FFixedAsset_Station_Category 

                                    /* st 20210308 ยกเลิกการใช้งาน    
                                    int vcount = 0;
                                  
                                    var vfs_cat = dc.FFixedAsset_Station_Categories
                                                .Where(t => t.STA_ID == vchkD.STA_ID &&
                                                        t.FCAT_ID == vchkD.FCAT_ID 
                                                        
                                                ).FirstOrDefault();

                                    if (vfs_cat != null)
                                      {
                                          vcount = int.Parse(vfs_cat.FQuantity.ToString()) - 1;

                                          vfs_cat.FQuantity = vcount;
                                          dc.SubmitChanges();

                                      }
                                    */

                                    //ลบข้อมูลออกจากสถานีเก่า
                                    dc.FFixedAsset_Station_Details.DeleteOnSubmit(vchkD);
                                    dc.SubmitChanges();

                                    //20210121
                                    var newSNTrack = new DAL.SMSManage.FFixedAsset_SerialNumber_Tracking
                                    {
                                        TrackingDate = vTranMoveTo,
                                        SerialNumber_Old = vchkD.SerialNumber,
                                        SerialNumber_New = vchkD.SerialNumber,
                                        JOB_ID = "",
                                        STA_ID_Old = vchkD.STA_ID,
                                        STA_ID_New = item2.STA_ID,
                                        VAN_ID = UserInfo.UserId,
                                        SerialNumber_Status = "11",
                                        Remark = "Used (Station Move SN) FixedAsset Tracking",
                                        RegistDate = vTranMoveTo
                                    };

                                    dc.FFixedAsset_SerialNumber_Trackings.InsertOnSubmit(newSNTrack);
                                    dc.SubmitChanges();

                                    //====

                                }//มีข้อมูลใน fDetail สถานีเก่า

                                var vchkDSave = dc.FFixedAsset_Station_Details
                                        .Where(t => t.SerialNumber == item2.SerialNumber &&
                                                /*20210308 t.FCAT_ID == item2.FCAT_ID &&
                                                 t.FMD_ID == item2.FMD_ID && */
                                                t.STA_ID == item2.STA_ID)
                                        .FirstOrDefault();

                                if (vchkDSave != null)
                                {
                                    //==20190430 เปลี่ยน process แทนการลบทิ้ง
                                    //มีแล้ว Update
                                    /* 20210308 
                                    vchkDSave.FCAT_ID = item2.FCAT_ID;
                                    vchkDSave.FBA_ID = item2.FBA_ID;
                                    vchkDSave.FMD_ID = item2.FMD_ID; 
                                     */
                                    vchkDSave.STA_ID = item2.STA_ID;
                                    vchkDSave.OwnerShip = item2.OwnerShip;
                                    vchkDSave.InstallDate = item2.InstallDate;
                                    vchkDSave.WarrantDate = item2.WarrantDate;
                                    vchkDSave.Location = null;
                                    vchkDSave.LOC_ID = item2.Location;
                                    vchkDSave.Detail = item2.Detail;
                                    vchkDSave.TranDate = item2.TranDate;
                                    vchkDSave.PartType1 = item2.PartType1;
                                    vchkDSave.PartType2 = item2.PartType2;
                                    vchkDSave.PartType3 = item2.PartType3;
                                    vchkDSave.CheckGauge = item2.CheckGauge;
                                    vchkDSave.TranEMP = item2.TranEMP;
                                    vchkDSave.ControlFixAssetNo = item2.ControlFixAssetNo;
                                    vchkDSave.IsConfirm = item2.IsConfirm;
                                    vchkDSave.ConfirmEMP = item2.ConfirmEMP;
                                    vchkDSave.ConfirmDate = item2.ConfirmDate;
                                    //20190513
                                    vchkDSave.IsLifeTime = item2.IsLifeTime;
                                    vchkDSave.IsSpecialPrice = item2.IsSpecialPrice;
                                    dc.SubmitChanges();
                                }
                                else
                                {
                                    //20190430 เปลี่ยน process แทนการลบทิ้ง
                                    var newFFa = new DAL.SMSManage.FFixedAsset_Station_Detail
                                    {
                                        SerialNumber = item2.SerialNumber,
                                        ControlFixAssetNo = item2.ControlFixAssetNo,

                                        /* 20210308FCAT_ID = item2.FCAT_ID,
                                        FBA_ID = item2.FBA_ID,
                                        FMD_ID = item2.FMD_ID,
                                        STA_ID = item2.STA_ID,*/
                                        OwnerShip = item2.OwnerShip,
                                        InstallDate = item2.InstallDate,
                                        WarrantDate = item2.WarrantDate,

                                        //20141208Location = item2.Location,
                                        LOC_ID = item2.Location, //20141209 int.Parse(item2.Location),
                                        Location = null,

                                        Detail = item2.Detail,
                                        //20141003 TranDate = servDate,
                                        TranDate = item2.TranDate,

                                        PartType1 = item2.PartType1,
                                        PartType2 = item2.PartType2,
                                        PartType3 = item2.PartType3,
                                        //20140522
                                        // TranEMP = UserInfo.UserId

                                        TranEMP = item2.TranEMP, //แยกตามรายการที่แก้ไข และบันทึกจริง

                                        //20141003
                                        CheckGauge = item2.CheckGauge,
                                        IsConfirm = item2.IsConfirm,
                                        ConfirmEMP = item2.ConfirmEMP,
                                        ConfirmDate = item2.ConfirmDate,

                                        //20190513
                                        IsLifeTime = item2.IsLifeTime,
                                        IsSpecialPrice = item2.IsSpecialPrice

                                    };

                                    dc.FFixedAsset_Station_Details.InsertOnSubmit(newFFa);
                                    dc.SubmitChanges();


                                }


                                //====20180903======end===============
                                /* //20190430 เปลี่ยน process แทนการลบทิ้ง
                                var newFFa = new DAL.SMSManage.FFixedAsset_Station_Detail
                                {
                                    SerialNumber = item2.SerialNumber,
                                    ControlFixAssetNo = item2.ControlFixAssetNo,

                                    FCAT_ID = item2.FCAT_ID,
                                    FBA_ID = item2.FBA_ID,
                                    FMD_ID = item2.FMD_ID,
                                    STA_ID = item2.STA_ID,
                                    OwnerShip = item2.OwnerShip,
                                    InstallDate = item2.InstallDate,
                                    WarrantDate = item2.WarrantDate,
                                    //20141208 Location = item2.Location,
                                    LOC_ID = item2.Location,//20141209 int.Parse(item2.Location),
                                    Location =null,

                                    Detail = item2.Detail,
                                    //20141003 TranDate = servDate,
                                    TranDate = item2.TranDate,

                                    PartType1 = item2.PartType1,
                                    PartType2 = item2.PartType2,
                                    PartType3 = item2.PartType3,


                                    TranEMP = item2.TranEMP, 
                                    //ConfirmDate = Confirmdate,
                                    //ConfirmEMP = UserInfo.UserId,

                                    //20141003
                                    CheckGauge = item2.CheckGauge,

                                    //20140924
                                    ConfirmDate = item2.ConfirmDate,
                                    ConfirmEMP = item2.ConfirmEMP,
                                    IsConfirm = item2.IsConfirm


                                };

                                dc.FFixedAsset_Station_Details.InsertOnSubmit(newFFa);
                                dc.SubmitChanges();

                               */

                                var pool = dc.FFixedAsset_SerialNumber_Pools
                                    .Where(t => t.SerialNumber == item2.SerialNumber &&
                                            t.FCAT_ID == item2.FCAT_ID &&
                                            t.FMD_ID == item2.FMD_ID)
                                    .FirstOrDefault();

                                if (pool != null)
                                {
                                    //20210121 
                                    if (pool.StatusUse != "11")
                                    {
                                        var newSNTrack = new DAL.SMSManage.FFixedAsset_SerialNumber_Tracking
                                        {
                                            TrackingDate = vTranMoveTo,
                                            SerialNumber_Old = item2.SerialNumber,
                                            SerialNumber_New = item2.SerialNumber,
                                            JOB_ID = "",
                                            STA_ID_Old = item2.STA_ID,
                                            STA_ID_New = item2.STA_ID,
                                            VAN_ID = UserInfo.UserId,
                                            SerialNumber_Status = "11",
                                            Remark = "Used (Station) FixedAsset Tracking",
                                            RegistDate = vTranMoveTo
                                        };

                                        dc.FFixedAsset_SerialNumber_Trackings.InsertOnSubmit(newSNTrack);
                                        dc.SubmitChanges();

                                    }


                                    //========
                                    pool.StatusUse = "11";//ถูกใช้งานแล้ว
                                    pool.InstallDate = dtInstall.Value;

                                    dc.SubmitChanges();
                                }
                            }//end for

                        }
                        else
                        {
                            //บันทึก ข้อมูล Fixed Asset
                            foreach (var item2 in ffStD)
                            {

                                //20180903 ==begin=== ลบ auto ข้อมูลกรณีย้าย fixed asset ไป site อื่น
                                //select หาข้อมูลว่ามีอยู่จริงที่ที่สถานีเก่า
                                var vchkD = dc.FFixedAsset_Station_Details
                                   .Where(t => t.SerialNumber == item2.SerialNumber
                                           /* 20210308
                                              &&
                                               t.FCAT_ID == item2.FCAT_ID &&
                                               t.FMD_ID == item2.FMD_ID */
                                           && t.STA_ID != item2.STA_ID)
                                   .FirstOrDefault();

                                if (vchkD != null)
                                {
                                    //==20210308
                                    var vchkD1 = (from t in dc.vw_FFA_Station_Details
                                                  join l in dc.Locations on t.Location equals l.LOC_ID
                                                  where t.SerialNumber == item2.SerialNumber
                                                  && t.STA_ID != item2.STA_ID
                                                  select new
                                                  {
                                                      t.SerialNumber,
                                                      t.FCAT_ID,
                                                      t.FBA_ID,
                                                      t.FMD_ID,
                                                      t.STA_ID,
                                                      t.OwnerShip,
                                                      t.InstallDate,
                                                      t.WarrantDate,
                                                      Location = l.Location1,
                                                      l.LOC_ID,
                                                      t.Detail,
                                                      t.TranDate,
                                                      t.PartType1,
                                                      t.PartType2,
                                                      t.PartType3,
                                                      t.CheckGauge,
                                                      t.TranEMP,
                                                      t.ControlFixAssetNo,
                                                      t.IsConfirm,
                                                      t.ConfirmEMP,
                                                      t.ConfirmDate,

                                                  })
                                              .FirstOrDefault();
                                    //==
                                    //เก็บ log Fixed Asset ของสถานีเก่า
                                    var newLog = new DAL.SMSManage.FFixedAsset_Station_Detail_logMove
                                    {

                                        SerialNumber = vchkD1.SerialNumber,
                                        FCAT_ID = vchkD1.FCAT_ID,
                                        FBA_ID = vchkD1.FBA_ID,
                                        FMD_ID = vchkD1.FMD_ID,
                                        STA_ID = vchkD1.STA_ID,
                                        OwnerShip = vchkD1.OwnerShip,
                                        InstallDate = vchkD1.InstallDate,
                                        WarrantDate = vchkD1.WarrantDate,
                                        Location = vchkD1.Location,
                                        LOC_ID = vchkD1.LOC_ID,
                                        Detail = vchkD1.Detail,
                                        TranDate = vchkD1.TranDate,
                                        PartType1 = vchkD1.PartType1,
                                        PartType2 = vchkD1.PartType2,
                                        PartType3 = vchkD1.PartType3,
                                        CheckGauge = vchkD1.CheckGauge,
                                        TranEMP = vchkD1.TranEMP,
                                        ControlFixAssetNo = vchkD1.ControlFixAssetNo,
                                        IsConfirm = vchkD1.IsConfirm,
                                        ConfirmEMP = vchkD1.ConfirmEMP,
                                        ConfirmDate = vchkD1.ConfirmDate,
                                        STA_ID_MoveTo = item2.STA_ID,
                                        TranDate_MoveTo = vTranMoveTo,
                                        TranEMP_MoveTo = UserInfo.UserId
                                    };

                                    dc.FFixedAsset_Station_Detail_logMoves.InsertOnSubmit(newLog);
                                    dc.SubmitChanges();


                                    //===20190430==
                                    var ld = new DAL.SMSManage.Log_DeleteDatabyProgram
                                    {
                                        Trandate = servDate,
                                        DataID = vchkD.SerialNumber,//txtJOB_ID.Text.Trim(),
                                        TableName = "FFixedAsset_Station_Detail",
                                        Menu = "บันทึก_ยืนยัน Fixed Asset",
                                        Detail = "Move SN " + vchkD.SerialNumber + " STA_ID " + vchkD.STA_ID + " to " + item2.STA_ID,
                                        UserID = UserInfo.UserId,
                                        Apps = "SMSMINI"

                                    };

                                    dc.Log_DeleteDatabyPrograms.InsertOnSubmit(ld);
                                    dc.SubmitChanges();
                                    //=============

                                    //ลด f_station_category ที่สถานีเดิม
                                    //บันทึก FFixedAsset_Station_Category 

                                    /* st 20210308 ยกเลิกการใช้งาน  int vcount = 0;
                                    var vfs_cat = dc.FFixedAsset_Station_Categories
                                                .Where(t => t.STA_ID == vchkD.STA_ID &&
                                                        t.FCAT_ID == vchkD.FCAT_ID

                                                ).FirstOrDefault();

                                    if (vfs_cat != null)
                                    {
                                        vcount = int.Parse(vfs_cat.FQuantity.ToString()) - 1;

                                        vfs_cat.FQuantity = vcount;
                                        dc.SubmitChanges();

                                    }*/

                                    //ลบข้อมูลออกจากสถานีเก่า
                                    dc.FFixedAsset_Station_Details.DeleteOnSubmit(vchkD);
                                    dc.SubmitChanges();


                                    //20210121
                                    var newSNTrack = new DAL.SMSManage.FFixedAsset_SerialNumber_Tracking
                                    {
                                        TrackingDate = vTranMoveTo,
                                        SerialNumber_Old = vchkD.SerialNumber,
                                        SerialNumber_New = vchkD.SerialNumber,
                                        JOB_ID = "",
                                        STA_ID_Old = vchkD.STA_ID,
                                        STA_ID_New = item2.STA_ID,
                                        VAN_ID = UserInfo.UserId,
                                        SerialNumber_Status = "11",
                                        Remark = "Used (Station Move SN) FixedAsset Tracking",
                                        RegistDate = vTranMoveTo
                                    };

                                    dc.FFixedAsset_SerialNumber_Trackings.InsertOnSubmit(newSNTrack);
                                    dc.SubmitChanges();

                                    //====


                                }//มีข้อมูลใน fDetail
                                var vchkDSave = dc.FFixedAsset_Station_Details
                                               .Where(t => t.SerialNumber == item2.SerialNumber &&
                                                       /*20210308
                                                       t.FCAT_ID == item2.FCAT_ID &&
                                                       t.FMD_ID == item2.FMD_ID && 
                                                        */
                                                       t.STA_ID == item2.STA_ID)
                                               .FirstOrDefault();

                                if (vchkDSave != null)
                                {
                                    //==20190430 เปลี่ยน process แทนการลบทิ้ง
                                    //มีแล้ว Update
                                    /*20210308
                                    vchkDSave.FCAT_ID = item2.FCAT_ID;
                                    vchkDSave.FBA_ID = item2.FBA_ID;
                                    vchkDSave.FMD_ID = item2.FMD_ID;*/
                                    vchkDSave.STA_ID = item2.STA_ID;
                                    vchkDSave.OwnerShip = item2.OwnerShip;
                                    vchkDSave.InstallDate = item2.InstallDate;
                                    vchkDSave.WarrantDate = item2.WarrantDate;
                                    vchkDSave.Location = null;
                                    vchkDSave.LOC_ID = item2.Location;
                                    vchkDSave.Detail = item2.Detail;
                                    vchkDSave.TranDate = item2.TranDate;
                                    vchkDSave.PartType1 = item2.PartType1;
                                    vchkDSave.PartType2 = item2.PartType2;
                                    vchkDSave.PartType3 = item2.PartType3;
                                    vchkDSave.CheckGauge = item2.CheckGauge;
                                    vchkDSave.TranEMP = item2.TranEMP;
                                    vchkDSave.ControlFixAssetNo = item2.ControlFixAssetNo;
                                    vchkDSave.IsConfirm = item2.IsConfirm;
                                    vchkDSave.ConfirmEMP = item2.ConfirmEMP;
                                    vchkDSave.ConfirmDate = item2.ConfirmDate;
                                    //20190513
                                    vchkDSave.IsLifeTime = item2.IsLifeTime;
                                    vchkDSave.IsSpecialPrice = item2.IsSpecialPrice;
                                    dc.SubmitChanges();
                                }
                                else
                                {
                                    //20190430 เปลี่ยน process แทนการลบทิ้ง
                                    var newFFa = new DAL.SMSManage.FFixedAsset_Station_Detail
                                    {
                                        SerialNumber = item2.SerialNumber,
                                        ControlFixAssetNo = item2.ControlFixAssetNo,

                                        /*20210308 
                                        FCAT_ID = item2.FCAT_ID,
                                        FBA_ID = item2.FBA_ID,
                                        FMD_ID = item2.FMD_ID,
                                        */
                                        STA_ID = item2.STA_ID,
                                        OwnerShip = item2.OwnerShip,
                                        InstallDate = item2.InstallDate,
                                        WarrantDate = item2.WarrantDate,

                                        //20141208Location = item2.Location,
                                        LOC_ID = item2.Location, //20141209 int.Parse(item2.Location),
                                        Location = null,

                                        Detail = item2.Detail,
                                        //20141003 TranDate = servDate,
                                        TranDate = item2.TranDate,

                                        PartType1 = item2.PartType1,
                                        PartType2 = item2.PartType2,
                                        PartType3 = item2.PartType3,
                                        //20140522
                                        // TranEMP = UserInfo.UserId

                                        TranEMP = item2.TranEMP, //แยกตามรายการที่แก้ไข และบีนทึกจริง

                                        //20141003
                                        CheckGauge = item2.CheckGauge,
                                        IsConfirm = item2.IsConfirm,
                                        ConfirmEMP = item2.ConfirmEMP,
                                        ConfirmDate = item2.ConfirmDate,
                                        //20190513
                                        IsLifeTime = item2.IsLifeTime,
                                        IsSpecialPrice = item2.IsSpecialPrice

                                    };

                                    dc.FFixedAsset_Station_Details.InsertOnSubmit(newFFa);
                                    dc.SubmitChanges();


                                }

                                //====20180903======end===============

                                /* //20190430 เปลี่ยน process แทนการลบทิ้ง
                                var newFFa = new DAL.SMSManage.FFixedAsset_Station_Detail
                                {
                                    SerialNumber = item2.SerialNumber,
                                    ControlFixAssetNo = item2.ControlFixAssetNo,

                                    FCAT_ID = item2.FCAT_ID,
                                    FBA_ID = item2.FBA_ID,
                                    FMD_ID = item2.FMD_ID,
                                    STA_ID = item2.STA_ID,
                                    OwnerShip = item2.OwnerShip,
                                    InstallDate = item2.InstallDate,
                                    WarrantDate = item2.WarrantDate,

                                    //20141208Location = item2.Location,
                                    LOC_ID = item2.Location, //20141209 int.Parse(item2.Location),
                                    Location = null,

                                    Detail = item2.Detail,
                                    //20141003 TranDate = servDate,
                                    TranDate = item2.TranDate,

                                    PartType1 = item2.PartType1,
                                    PartType2 = item2.PartType2,
                                    PartType3 = item2.PartType3,
                                    //20140522
                                    // TranEMP = UserInfo.UserId

                                    TranEMP = item2.TranEMP, //แยกตามรายการที่แก้ไข และบีนทึกจริง

                                    //20141003
                                    CheckGauge = item2.CheckGauge,
                                    IsConfirm = item2.IsConfirm,
                                    ConfirmEMP = item2.ConfirmEMP,
                                    ConfirmDate=item2.ConfirmDate

                                };

                                dc.FFixedAsset_Station_Details.InsertOnSubmit(newFFa);
                                dc.SubmitChanges();
                                */

                                var pool = dc.FFixedAsset_SerialNumber_Pools
                                    .Where(t => t.SerialNumber == item2.SerialNumber &&
                                            t.FCAT_ID == item2.FCAT_ID &&
                                            t.FMD_ID == item2.FMD_ID)
                                    .FirstOrDefault();

                                if (pool != null)
                                {
                                    //20210121 
                                    if (pool.StatusUse != "11")
                                    {
                                        var newSNTrack = new DAL.SMSManage.FFixedAsset_SerialNumber_Tracking
                                        {
                                            TrackingDate = vTranMoveTo,
                                            SerialNumber_Old = item2.SerialNumber,
                                            SerialNumber_New = item2.SerialNumber,
                                            JOB_ID = "",
                                            STA_ID_Old = item2.STA_ID,
                                            STA_ID_New = item2.STA_ID,
                                            VAN_ID = UserInfo.UserId,
                                            SerialNumber_Status = "11",
                                            Remark = "Used (Station) FixedAsset Tracking",
                                            RegistDate = vTranMoveTo
                                        };

                                        dc.FFixedAsset_SerialNumber_Trackings.InsertOnSubmit(newSNTrack);
                                        dc.SubmitChanges();

                                    }


                                    //========

                                    pool.StatusUse = "11";//ถูกใช้งานแล้ว
                                    pool.InstallDate = dtInstall.Value;
                                    dc.SubmitChanges();
                                }



                            }//end for

                        }


                        dc.Transaction.Commit();
                        closeProgress();

                        MessageBox.Show("บันทึกข้อมูล Fixed Asset เรียบร้อย ", "ผลการทำงาน",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);

                        //20190507
                        getFFA_Detail(dc, tmpStation);
                        setDataGridViewStyle(); //20210804

                    }
                    catch (Exception ex)
                    {
                        closeProgress();
                        dc.Transaction.Rollback();

                        MessageBox.Show("ไม่สามารถ บันทึกข้อมูล Fixed Asset  กรุณาตรวจสอบ" + Environment.NewLine +
                          "1.สัญญาณ อินเตอร์เนต ปกติ หรือไม่?..." + Environment.NewLine +
                          "2.เข้าเว็บไซต์ JOB Monitor ได้ปกติ หรือไม่?..." + Environment.NewLine +
                          "Error: " + ex.Message, "ผลการบันทึกข้อมูล",
                           MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }



            }

        }

        private DateTime getServerDate()
        {
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            {
                var srv = dc
                     .ExecuteQuery<DateTime>("SELECT GETDATE()")
                     .First();
                if (srv != null)
                    return srv;
                else
                    return DateTime.Now;

            }
        }

        private void toolStripButton_Cancel_Click(object sender, EventArgs e)
        {
            Input_FFA_Load(null, null);
        }

        private void toolStripButton_Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void btAdd_Click(object sender, EventArgs e)
        {
            //20140507
            if (comb_model.SelectedValue.ToString() == null || comb_model.SelectedValue.ToString() == "")
            {
                MessageBox.Show("กรุณา เลือก รุ่น...", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            //======20160831=========

            if (chkInstallDate.Checked == true)
            {
                if (dtInstall.Value.ToString() == dtExpireDate.Value.ToString())
                {
                    MessageBox.Show("กรุณาตรวจสอบ วันที่ติดตั้ง และ วันรับประกัน ...", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }
            //======-======--=========

            //20140507 Recheck S/N 
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            {
                var qRechk = dc.FFixedAsset_SerialNumber_Pools
                                   .Where(t => t.SerialNumber == txtSN.Text &&
                                           t.FCAT_ID == int.Parse(comb_cat.SelectedValue.ToString()) &&
                                           t.FMD_ID == int.Parse(comb_model.SelectedValue.ToString()) &&
                                            //20180831 new string[] { "00", "66", "76" }.Contains(t.StatusUse))
                                            new string[] { "00", "66", "76", "11" }.Contains(t.StatusUse))
                                   .FirstOrDefault();

                //var q = (from r in Room
                //         join v in Reservation on r.RoomID equals v.RoomID into outer
                //         from o in outer.DefaultIfEmpty()
                //         where !(o.Date_Check_Out <= startdate || o.Date_Check_In >= endDate)
                //               && v.cancel == 0 && v.ReservationID == null
                //         select r);

                //var q = (from p in dc.FFixedAsset_SerialNumber_Pools
                //         join d in dc.FFixedAsset_Station_Details on p.SerialNumber equals d.SerialNumber into outer
                //         from o in outer.DefaultIfEmpty()
                //         where (o.SerialNumber == txtSN.Text && o.FCAT_ID ==  int.Parse(comb_cat.SelectedValue.ToString()) &&
                //          o.FMD_ID == int.Parse(comb_model.SelectedValue.ToString()) //&&
                //          //new string[] { "00", "66", "76", "11" }.Contains(o.)
                //         select p);

                if (qRechk != null)
                {
                    var qchkDelAuto = dc.FFixedAsset_Station_Details
                                     .Where(t => t.SerialNumber == qRechk.SerialNumber)
                                   .FirstOrDefault();

                    //20180831 //ลบออกจากสถานีเดิมอัตโนมัติ
                    //20190507 if (qRechk.StatusUse == "11")
                    if (qRechk.StatusUse == "11" && qchkDelAuto.STA_ID != tmpStation.STA_ID)//20190508
                    {
                        var qStation = dc.Stations.Where(t => t.STA_ID == qchkDelAuto.STA_ID).FirstOrDefault();
                        if ((MessageBox.Show("Serial Number: [" + txtSN.Text + "] ถูกใช้งานในสถานีอื่นแล้ว" + Environment.NewLine +

                                 //==20210525
                                 " " + Environment.NewLine +
                                 " ใช้งานที่สถานี : " + qStation.STA_ID + " : " + qStation.StationSys + " " + Environment.NewLine +
                                 " ที่อยู่ : " + qStation.Address + Environment.NewLine +
                                 " " + Environment.NewLine +
                            //==

                            "ระบบจะทำการ ลบข้อมูล ออกจากสถานีเก่า โดยอัติโนมัติ เพื่อให้นำมาบันทึกลงสถานีนี้ได้" + Environment.NewLine +
                            " " + Environment.NewLine +
                            "คุณต้องการ บันทึก Serial Number: [" + txtSN.Text + "] นี้ ลงสถานีนี้ ใช่หรือไม่", "คำยืนยัน", MessageBoxButtons.YesNo
                            , MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)) == DialogResult.No)
                        {
                            return;
                        }

                    }

                    //===================================

                    var qRechkB = dc.FFixedAsset_Models
                                   .Where(t =>
                                           t.FMD_ID == qRechk.FMD_ID &&
                                           t.FBA_ID == int.Parse(comb_brand.SelectedValue.ToString())
                                           )
                                   .FirstOrDefault();

                    if (qRechkB != null)
                    {


                        //เช็คข้อมูลซ้ำ 
                        foreach (var item in ffStD)
                        {
                            if (txtSN.Text.Trim() == item.SerialNumber)
                            {
                                MessageBox.Show("คุณป้อน Serial number [" + txtSN.Text + "] ซ้ำ กรุณาป้อนใหม่", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                txtSN.Focus();
                                txtSN.SelectAll();
                                return;
                            }

                        }





                        var newFFa = new DAL.SMSManage.vw_FFA_Station_Detail();
                        newFFa.STA_ID = txtSTA_ID.Text.Trim();
                        newFFa.StationSys = tmpStation.StationSys;

                        newFFa.SerialNumber = txtSN.Text.Trim();

                        newFFa.FCAT_ID = int.Parse(comb_cat.SelectedValue.ToString());
                        newFFa.FixedAsset_CateName = comb_cat.Text;

                        newFFa.FBA_ID = int.Parse(comb_brand.SelectedValue.ToString());
                        newFFa.FBrand = comb_brand.Text;

                        newFFa.FMD_ID = int.Parse(comb_model.SelectedValue.ToString());
                        newFFa.FModel = comb_model.Text;

                        var ownerShip = "";
                        //20141106switch (comb_cus_flo.SelectedText)
                        switch (comb_cus_flo.SelectedItem.ToString())
                        {
                            case "Flowco": ownerShip = "FLO"; break;
                            case "Customer": ownerShip = "CUS"; break;
                            case "Uniwave": ownerShip = "UNI"; break;
                            //default:
                            default:
                                ownerShip = "Customer";
                                break;
                        }
                        newFFa.OwnerShip = ownerShip;

                        if (chkInstallDate.Checked == true)
                        {
                            newFFa.InstallDate = dtInstall.Value;
                            //20141003
                            newFFa.WarrantDate = dtExpireDate.Value;
                        }
                        else
                        {
                            newFFa.InstallDate = null;
                            //20141003
                            newFFa.WarrantDate = null;
                        }

                        //20141003 newFFa.WarrantDate = dtExpireDate.Value

                        //20141208 newFFa.Location = txtLocation.Text.Trim();

                        //20141208
                        newFFa.Location = int.Parse(cbxLocation.SelectedValue.ToString());//20141209 cbxLocation.SelectedValue.ToString();

                        newFFa.Detail = txtRemark.Text.Trim();
                        newFFa.TranDate = servDate;

                        if (!string.IsNullOrEmpty(txtFixedAssetCust.Text))
                            newFFa.ControlFixAssetNo = txtFixedAssetCust.Text;


                        newFFa.PartType1 = int.Parse(txtPartTypeWt1.Text == "" ? "0" : txtPartTypeWt1.Text);
                        newFFa.PartType2 = int.Parse(txtPartTypeWt2.Text == "" ? "0" : txtPartTypeWt2.Text);
                        newFFa.PartType3 = int.Parse(txtPartTypeWt3.Text == "" ? "0" : txtPartTypeWt3.Text);

                        newFFa.CheckGauge = int.Parse(txtCheckGauge.Text == "" ? "0" : txtCheckGauge.Text);

                        //newFFa.TranEMP = UserInfo.UserId;



                        //====================================
                        //20140923
                        DateTime vConfirmdate = dc.ExecuteQuery<DateTime>("SELECT GETDATE()").First();
                        if (IsConfirmFFA == true)
                        {//ยืนยัน
                            newFFa.ConfirmEMP = UserInfo.UserId;
                            if (chxFFAConf.Checked == true)
                            {
                                newFFa.IsConfirm = true;
                            }
                            else
                            {
                                newFFa.IsConfirm = false;
                            }
                            newFFa.ConfirmDate = vConfirmdate;
                            newFFa.ControlFixAssetNo = txtFixedAssetCust.Text;

                            //แสดงคน mapping site  newFFa.TranEMP
                            newFFa.TranEMP = txtTranEmp.Text;
                        }
                        else
                        {//บันทึก
                            newFFa.TranEMP = UserInfo.UserId;//txtTranEmp.Text;
                        }
                        //====================================

                        //===20190513===
                        if (chxLifetime.Checked == true)
                        {
                            newFFa.IsLifeTime = true;
                        }
                        else
                        {
                            newFFa.IsLifeTime = false;
                        }

                        if (chxSpecialPrice.Checked == true)
                        {
                            newFFa.IsSpecialPrice = true;
                        }
                        else
                        {
                            newFFa.IsSpecialPrice = false;
                        }
                        //======

                        //chkPartType1
                        ffStD.Add(newFFa);
                        dgFFA.DataSource = ffStD.ToList();

                        SetControlEnable(false, true, true, false, false);
                        CleareSNText();


                    }// if (qRechkB != null)
                    else
                    {

                        MessageBox.Show("ข้อมูลรายการนี้ ไม่ตรงกับข้อมูลจริงที่อยู่ในคลัง", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;

                    }


                }//if (qRechk != null)
                else
                {

                    MessageBox.Show("ข้อมูลรายการนี้ ไม่ตรงกับข้อมูลจริงที่อยุ่ในคลัง", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;

                }
                //20141002
                setDataGridViewStyle();
            }


        }
        private void CleareSNText()
        {
            txtSN.Text = string.Empty;
            //20141208 txtLocation.Text = string.Empty;
            //cbxLocation.SelectedIndex = -1;

            txtRemark.Text = string.Empty;

            txtPartTypeWt1.Text = "0";
            txtPartTypeWt2.Text = "0";
            txtPartTypeWt3.Text = "0";

            chxFFAConf.Checked = false;
            chxLifetime.Checked = false;
            chxSpecialPrice.Checked = false;
        }

        private void comb_brand_SelectionChangeCommitted(object sender, EventArgs e)
        {
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            {
                comb_model.DataSource = dc.FFixedAsset_Models
                    .Where(t => t.FBA_ID == int.Parse(comb_brand.SelectedValue.ToString())
                        //20160422 || t.FMD_ID == 999
                        )
                         .Select(t => new
                         {
                             modelID = t.FMD_ID,
                             FModel = t.FMD_ID.ToString() + ": " + t.FModel,
                         })
                    .ToList();

                comb_model.DisplayMember = "FModel";
                comb_model.ValueMember = "modelID";
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            m_fmProgress.Activate();
            m_fmProgress.BringToFront();
            m_fmProgress.TopMost = true;
            m_fmProgress.ShowDialog();
        }

        private void invoke_Progress(string strMsg)
        {
            try
            {
                m_fmProgress.lblDescription.Invoke(
                   (MethodInvoker)delegate ()
                   {
                       m_fmProgress.lblDescription.Text = strMsg;
                   }
                   );
            }
            catch (System.Exception)
            { }
        }

        private void newProgressbar()
        {
            m_fmProgress = new ProgressBar.FormProgress();
            backgroundWorker1.RunWorkerAsync();
            System.Threading.Thread.Sleep(3000);
        }

        private void closeProgress()
        {
            if (m_fmProgress != null)
            {
                m_fmProgress.Invoke(
                    (MethodInvoker)delegate ()
                    {
                        m_fmProgress.Hide();
                        m_fmProgress.Dispose();
                        m_fmProgress = null;
                    }
                );
            }
        }

        private void dtInstall_ValueChanged(object sender, EventArgs e)
        {
            if (mode != "LOAD")
            {
                var iYear = int.Parse(txtYearWarranty.Text);
                dtExpireDate.Value = dtInstall.Value.AddYears(iYear);
            }
        }

        private void txtSN_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                //20140430 
                if (comb_cat.SelectedValue.ToString() == "")
                {
                    MessageBox.Show("กรุณา เลือก อุปกรณ์...", "ผลการตรวจสอบ...", MessageBoxButtons.OK);
                    return;
                }
                if (comb_brand.SelectedValue.ToString() == "")
                {
                    MessageBox.Show("กรุณา เลือก ยี่ห้อ...", "ผลการตรวจสอบ...", MessageBoxButtons.OK);
                    return;
                }
                if (comb_model.SelectedValue.ToString() == "")
                {
                    MessageBox.Show("กรุณา เลือก รุ่น...", "ผลการตรวจสอบ...", MessageBoxButtons.OK);
                    return;
                }




                var f = new Find_SNPool();

                f.SN = txtSN.Text.Trim();
                f.FCATID = int.Parse(comb_cat.SelectedValue.ToString());
                f.FMDID = int.Parse(comb_model.SelectedValue.ToString());
                //20140430
                f.FBAID = int.Parse(comb_brand.SelectedValue.ToString());

                //20170202
                f.STA_ID = txtSTA_ID.Text.Trim();


                var result = f.ShowDialog();
                if (result == DialogResult.OK)
                {
                    SetControlEnable(true, true, true, true, true);
                    txtSN.Text = f.SN;
                    //20141208 txtLocation.Focus();
                    cbxLocation.Focus();

                    setcob_LocatFCAT(f.FCATID);//20210201

                    //==20200716 เปลี่ยนไปใช้ดึงข้อมูลวันที่ InvoiceDate มา default เป็นวันที่ติดตั้ง

                    if (dtInstall.CustomFormat == " ")
                    {
                        if (!string.IsNullOrEmpty(f.InvDate.ToString()))
                        {
                            dtInstall.Format = DateTimePickerFormat.Custom;
                            chkInstallDate.Checked = true;
                            dtInstall.Value = DateTime.Parse(f.InvDate).Date;
                            dtInstall.CustomFormat = "d/M/yyyy";

                            dtExpireDate.Format = DateTimePickerFormat.Custom;
                            dtExpireDate.Value = DateTime.Parse(f.InvDate).Date;
                            dtExpireDate.CustomFormat = "d/M/yyyy";

                            txtYearWarranty.Enabled = true;
                        }
                    }

                    dtInstall.Enabled = true;//20200721
                    //==

                }
                //20141107 
                //else if (result == DialogResult.Cancel)
                else if (result == DialogResult.Ignore)
                {
                    //Add S/N Pool

                    //ถ้าเป็น van ไม่สามารถ Add S/N Pool
                    //ถ้าเป็น SUP ขึ้นไปสามารถ Add S/N Pool
                    //=======================//
                    //UserInfo.UserLevID
                    //=======================//                   
                    //06	Supervisor -VAN
                    //07	Supervisor - Admin
                    //08	Supervisor Board,Recon
                    //09	Section Manager- Board,Recon
                    //12	Section Manager-ตู้จ่าย
                    //13	Section Manager-ระบบ
                    //99	Administrator

                    //if (UserInfo.SUP_VAN == UserInfo.Van_ID && new string[] { "06", "07", "08", "09", "12", "13", "99" }.Contains(UserInfo.UserLevID))
                    //{
                    //    //Add S/N Pool
                    //    AddSNPool();
                    //}                     
                    //else
                    //{
                    //    //ไม่สามารถ Add S/N Pool
                    //    MessageBox.Show("จากการตรวจสอบ Serial Number: [" + txtSN.Text + "] นี้" + Environment.NewLine +
                    //    "ไม่มีอยู่ในระบบ กรุณาติดต่อ SUP, SMG ของท่าน", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //}     

                    AddSNPool();
                }
                else if (result == DialogResult.Cancel)
                {
                    txtSN.Focus();

                    //20170214
                    SetControlEnable(false, true, true, true, true);
                }
            }
        }

        private void AddSNPool()
        {
            if ((MessageBox.Show("คุณต้องการ บันทึก Serial Number: [" + txtSN.Text + "] นี้ ลง Pool ใช่หรือไม่", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)) == DialogResult.Yes)
            {
                servDate = getServerDate();

                //20140430
                if (txtSN.Text == "")
                {
                    MessageBox.Show("กรุณา ระบุ SerialNumber...", "ผลการตรวจสอบ...", MessageBoxButtons.OK);
                    return;
                }




                //Add S/N Pool
                using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
                {
                    //20140520
                    var qDup = dc.FFixedAsset_SerialNumber_Pools.Where(t => t.SerialNumber == txtSN.Text.Trim()).FirstOrDefault();

                    if (qDup != null)
                    {

                        var qAlertDup = (from t in dc.FFixedAsset_SerialNumber_Pools
                                         from c in dc.FFixedAsset_Categories
                                         from b in dc.FFixedAsset_Brands
                                         from m in dc.FFixedAsset_Models

                                         where (t.FMD_ID == m.FMD_ID && t.FCAT_ID == c.FCAT_ID) &&
                                                (m.FBA_ID == b.FBA_ID) &&
                                                //(t.FCAT_ID == FCATID && t.FMD_ID == FMDID && m.FBA_ID == FBAID) &&
                                                (t.SerialNumber == txtSN.Text.Trim())

                                         select new
                                         {
                                             SerialNumber = t.SerialNumber,
                                             FCAT_NAME = c.FixedAsset_CateName,
                                             FBrand = b.FBrand,
                                             FModel = m.FModel

                                         }).Distinct().FirstOrDefault();


                        MessageBox.Show("Serial Number: [" + qAlertDup.SerialNumber + "] มีข้อมูลแล้ว \n ในอุปกรณ์ :  " + qAlertDup.FCAT_NAME + "  ยี่ห้อ : " + qAlertDup.FBrand + " รุ่น : " + qAlertDup.FModel + "  ", "ผลการค้นหา", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //DialogResult = DialogResult.No;
                        return;

                    }
                    else
                    {
                        try
                        {

                            var newsnPool = new DAL.SMSManage.FFixedAsset_SerialNumber_Pool()
                            {
                                SerialNumber = txtSN.Text.Trim(),
                                SerialNumberBarcode = txtSN.Text.Trim(),
                                ControlFixAssetNo = null,
                                FCAT_ID = int.Parse(comb_cat.SelectedValue.ToString()),
                                FMD_ID = int.Parse(comb_model.SelectedValue.ToString()),
                                FixedAssetName = comb_cat.Text,
                                //20160901 EMP_ID = "",
                                EMP_ID = UserInfo.UserId,
                                RegisDate = servDate,
                                StatusUse = "00",
                                Detail = "Add Pool by SMS MINI",
                                InstallDate = dtInstall.Value,
                                ExpiryDate = dtExpireDate.Value
                            };

                            dc.FFixedAsset_SerialNumber_Pools.InsertOnSubmit(newsnPool);
                            dc.SubmitChanges();

                            //20210120                                  
                            var newSNTrack = new DAL.SMSManage.FFixedAsset_SerialNumber_Tracking
                            {
                                TrackingDate = servDate,
                                SerialNumber_Old = txtSN.Text.Trim(),
                                SerialNumber_New = txtSN.Text.Trim(),
                                JOB_ID = "",
                                STA_ID_Old = "",
                                STA_ID_New = "",
                                VAN_ID = UserInfo.UserId,
                                SerialNumber_Status = "00",
                                Remark = "Ready (Key) FixedAsset Tracking",
                                RegistDate = servDate
                            };

                            dc.FFixedAsset_SerialNumber_Trackings.InsertOnSubmit(newSNTrack);
                            dc.SubmitChanges();

                            //=== 

                            MessageBox.Show("บันทึกข้อมูล Serial number [" + txtSN.Text + "] ลง Pool เรียบร้อย ", "ผลการทำงาน", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            SetControlEnable(true, true, true, true, true);
                            //20141208 txtLocation.Focus();

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("ไม่สามารถบันทึกข้อมูล Serial number [" + txtSN.Text + "] กรุณาตรวจสอบ Serial number ใหม่..." + Environment.NewLine +
                            "ERROR " + ex.Message, "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
        }

        private void txtLocation_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                txtRemark.Focus();
        }

        private void txtRemark_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btAdd.Focus();
        }

        private void dgFFA_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

            if (e.RowIndex != -1 && (e.ColumnIndex == 0 || e.ColumnIndex == 1))
            {
                if (MessageBox.Show("คุณต้องการ ยกเลิกรายการ เพื่อทำการแก้ไขข้อมูล ใช่หรือไม่???...", "คำยืนยัน",
              MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    var SN = dgFFA["SerialNumber", e.RowIndex].Value.ToString();

                    var qcan = ffStD.Where(t => t.SerialNumber == SN).FirstOrDefault();
                    if (qcan != null)
                    {

                        setcob_FCAT_FBA_FMD(qcan.FCAT_ID, qcan.FBA_ID);//20210201

                        comb_cat.SelectedValue = qcan.FCAT_ID;
                        comb_brand.SelectedValue = qcan.FBA_ID;
                        //comb_model.SelectedValue = qcan.FMD_ID;

                        comb_model.SelectedValue = qcan.FMD_ID;//20210201



                        //===20210201
                        setcob_LocatFCAT(qcan.FCAT_ID);
                        if (qcan.Location == null)
                        {
                            cbxLocation.SelectedValue = 0;
                        }
                        else
                        {
                            cbxLocation.SelectedValue = qcan.Location;//20141209 int.Parse(qcan.Location);
                        }


                        //==========



                        /*20210201
                        //20140507
                        using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
                        {
                            comb_model.DataSource = dc.FFixedAsset_Models
                          .Where(t => t.FBA_ID == int.Parse(qcan.FBA_ID.ToString())
                                //20160422 || t.FMD_ID == 999
                              )
                               .Select(t => new
                               {
                                   modelID = t.FMD_ID,
                                   FModel = t.FMD_ID.ToString() + ": " + t.FModel,
                               })
                          .ToList();

                            comb_model.DisplayMember = "FModel";
                            comb_model.ValueMember = "modelID";
                            comb_model.SelectedValue = qcan.FMD_ID;

                        }
                         */


                        /* //20140924
                        var iYear = qcan.WarrantDate.Value.Year - qcan.InstallDate.Value.Year;
                        txtYearWarranty.Text = iYear.ToString();
                         */
                        DateTime vInstallDate;

                        /*
                        //--ก่อนเพิ่มการกำหนดเดือน 20180905
                        //20140924
                        var iYear = 0;
                        if (qcan.InstallDate != null)

                        {
                            iYear = qcan.WarrantDate.Value.Year - qcan.InstallDate.Value.Year;
                            txtYearWarranty.Text = iYear.ToString();

                            //20141111
                            dtExpireDate.Format = DateTimePickerFormat.Custom;
                            dtInstall.Format = DateTimePickerFormat.Custom;
                            txtYearWarranty.Enabled = true;


                            chkInstallDate.Checked = true;
                            dtInstall.Value = qcan.InstallDate.Value;

                            //20141003
                            dtExpireDate.Value = qcan.WarrantDate.Value;

                            //20141111
                            dtInstall.CustomFormat = "d/M/yyyy";
                            dtExpireDate.CustomFormat = "d/M/yyyy";
                        }
                        else
                        {
                            txtYearWarranty.Text = "0";
                            dtInstall.CustomFormat = " ";
                            chkInstallDate.Checked = false;
                            //20141003
                            dtExpireDate.CustomFormat = " ";
                        }
                        //--ก่อนเพิ่มการกำหนดเดือน 20180905
                         */

                        //--เพิ่มการกำหนดเดือน 20180905
                        //20140924
                        var iYear = 0;
                        var iMonth = 0;
                        if (qcan.InstallDate != null)
                        {

                            if (qcan.WarrantDate == null) //20200605
                            {
                                qcan.WarrantDate = qcan.InstallDate;
                            }

                            //-- 05/09/2018 - 05/09/2019
                            var vchkM = (qcan.WarrantDate.Value.Month - qcan.InstallDate.Value.Month) + 12 * (qcan.WarrantDate.Value.Year - qcan.InstallDate.Value.Year);//qcan.WarrantDate.Value.Day - qcan.InstallDate.Value.Day;

                            var vm = vchkM % 12;
                            if (vm == 0)
                            {
                                iYear = qcan.WarrantDate.Value.Year - qcan.InstallDate.Value.Year;
                                txtYearWarranty.Text = iYear.ToString();

                                //20141111
                                dtExpireDate.Format = DateTimePickerFormat.Custom;
                                dtInstall.Format = DateTimePickerFormat.Custom;
                                txtYearWarranty.Enabled = true;


                                chkInstallDate.Checked = true;
                                dtInstall.Value = qcan.InstallDate.Value;


                                //20141003
                                dtExpireDate.Value = qcan.WarrantDate.Value;

                                //20141111
                                dtInstall.CustomFormat = "d/M/yyyy";
                                dtExpireDate.CustomFormat = "d/M/yyyy";

                                rdYear.Checked = true;
                                rdMonth.Checked = false;

                                dtInstall.Enabled = true;//20200721

                            }
                            else
                            {
                                iMonth = vchkM;//qcan.WarrantDate.Value.Month - qcan.InstallDate.Value.Month; //----
                                txtYearWarranty.Text = iMonth.ToString();

                                //20141111
                                dtExpireDate.Format = DateTimePickerFormat.Custom;
                                dtInstall.Format = DateTimePickerFormat.Custom;
                                txtYearWarranty.Enabled = true;


                                chkInstallDate.Checked = true;
                                dtInstall.Value = qcan.InstallDate.Value;

                                //20141003
                                dtExpireDate.Value = qcan.WarrantDate.Value;

                                //20141111
                                dtInstall.CustomFormat = "d/M/yyyy";
                                dtExpireDate.CustomFormat = "d/M/yyyy";

                                rdYear.Checked = false;
                                rdMonth.Checked = true;

                                dtInstall.Enabled = true;//20200721

                            }

                        }
                        else
                        {
                            txtYearWarranty.Text = "0";
                            dtInstall.CustomFormat = " ";
                            chkInstallDate.Checked = false;
                            //20141003
                            dtExpireDate.CustomFormat = " ";
                        }
                        //--เพิ่มการกำหนดเดือน 20180905









                        //20141003 dtExpireDate.Value = qcan.WarrantDate.Value;

                        //20141107 comb_cus_flo.SelectedText = qcan.OwnerShip;

                        var _ownerShip = "";
                        switch (qcan.OwnerShip)
                        {
                            case "FLO": _ownerShip = "Flowco"; break;
                            case "CUS": _ownerShip = "Customer"; break;
                            case "UNI": _ownerShip = "Uniwave"; break;
                            //default:
                            default:
                                _ownerShip = "Customer";
                                break;
                        }

                        comb_cus_flo.SelectedItem = _ownerShip;
                        /*20210201
                        //20141208 txtLocation.Text = qcan.Location;
                        //20141208
                        if (qcan.Location == null)
                        {
                            cbxLocation.SelectedValue = 0;
                        }
                        else {
                            cbxLocation.SelectedValue = qcan.Location;//20141209 int.Parse(qcan.Location);
                        }
                         */

                        txtSN.Text = qcan.SerialNumber;

                        txtRemark.Text = qcan.Detail;

                        txtPartTypeWt1.Text = qcan.PartType1 == null ? "0" : qcan.PartType1.ToString();
                        txtPartTypeWt2.Text = qcan.PartType2 == null ? "0" : qcan.PartType2.ToString();
                        txtPartTypeWt3.Text = qcan.PartType3 == null ? "0" : qcan.PartType3.ToString();

                        txtCheckGauge.Text = qcan.CheckGauge == null ? "0" : qcan.CheckGauge.ToString();


                        //20140923
                        txtFixedAssetCust.Text = qcan.ControlFixAssetNo == null ? "" : qcan.ControlFixAssetNo.ToString();
                        if (IsConfirmFFA == true)
                        { //ยืนยัน
                            chxFFAConf.Enabled = true;
                            if (qcan.IsConfirm == true)
                            {
                                chxFFAConf.Checked = true;
                            }
                            else
                            {
                                chxFFAConf.Checked = false;
                            }
                            //=======TranEmp==========
                            if (!string.IsNullOrEmpty(qcan.TranEMP))
                            {
                                txtTranEmp.Text = qcan.TranEMP.ToString();
                            }
                            else
                            {
                                txtTranEmp.Text = "";
                            }
                        }
                        else
                        {//บันทึก
                            chxFFAConf.Enabled = false;
                            txtTranEmp.Text = UserInfo.UserId;

                        }


                        //20190507 ClearPoolStatusUse(qcan.SerialNumber, qcan.FCAT_ID, qcan.FMD_ID);

                        //===20190513===
                        if (qcan.IsLifeTime == true)
                        {
                            chxLifetime.Checked = true;
                        }
                        else
                        {
                            chxLifetime.Checked = false;
                        }

                        if (qcan.IsSpecialPrice == true)
                        {
                            chxSpecialPrice.Checked = true;
                        }
                        else
                        {
                            chxSpecialPrice.Checked = false;
                        }


                        //===========

                        ffStD.Remove(qcan);
                        dgFFA.DataSource = ffStD.ToList();
                        SetControlEnable(true, true, true, true, true);
                    }
                    //20141002
                    setDataGridViewStyle();
                }

            }
            //if (e.ColumnIndex == 3)
            //{
            //    using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            //    {
            //        //int rasd = int.Parse(dgFFA["cFCAT_ID", e.RowIndex].Value.ToString());
            //        //int r = dgFFA[]
            //        cLocation.DataSource = dc.Locations
            //            .Where(t => (t.FCAT_ID == int.Parse(dgFFA["cFCAT_ID", e.RowIndex].Value.ToString()) || t.FCAT_ID == null) && t.IsCancel == false)
            //                 .Select(t => new
            //                 {
            //                     Location = t.LOC_ID,
            //                     LocationDetail = t.LOC_ID.ToString() + ": " + t.Location1
            //                 })
            //                .OrderBy(t => t.Location).ToList();

            //        cLocation.DisplayMember = "LocationDetail";
            //        cLocation.ValueMember = "Location";
            //    }
            //    setDataGridViewStyle();
            //}
        }

        private void ClearPoolStatusUse(string sn, int fcat_id, int fmd_id)
        {
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            {
                var pool = dc.FFixedAsset_SerialNumber_Pools
                    .Where(t => t.SerialNumber == sn && t.FCAT_ID == fcat_id && t.FMD_ID == fmd_id)
                    .FirstOrDefault();
                if (pool != null)
                {
                    pool.StatusUse = "00";
                    //20140522
                    pool.Cancel_EMP = UserInfo.UserId;
                    dc.SubmitChanges();


                }

            }
        }

        private void Input_FFA_FormClosed(object sender, FormClosedEventArgs e)
        {
            //202000305 DialogResult = DialogResult.Abort;
            this.Close();
            //System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("th-TH");
        }

        private void toolStripBt_PrintFFA_Click(object sender, EventArgs e)
        {
            string strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
            string sql = "";


            sql = "SELECT * FROM  [vw_FFA_Station_Detail] Where [STA_ID] ='" + txtSTA_ID.Text.Trim() + "' order by [SerialNumber]  ;";

            using (SqlConnection conn = new SqlConnection(strConn))
            {
                conn.Open();

                using (SqlDataAdapter da = new SqlDataAdapter(sql, conn))
                {
                    DataSet ds = new DataSet();
                    da.Fill(ds);

                    if (ds.Tables[0].Rows.Count > 0)
                    {

                        var rpt = new FFA.FFAReport();

                        rpt.SetDatabaseLogon("sa", "enabler", "sms.flowco.co.th,1414", "ServicesMSDB");
                        rpt.SetDataSource(ds.Tables[0].DefaultView);

                        var add = ds.Tables[0].Rows[0]["Address"].ToString() +
                            " โทร." + ds.Tables[0].Rows[0]["TelNo"].ToString() +
                            " แฟกซ์." + ds.Tables[0].Rows[0]["FaxNo"].ToString() +
                            " จ." + ds.Tables[0].Rows[0]["Province"].ToString();


                        rpt.SetParameterValue("pStation", ds.Tables[0].Rows[0]["StationSys"].ToString());
                        rpt.SetParameterValue("pStaID", ds.Tables[0].Rows[0]["STA_ID"].ToString());
                        rpt.SetParameterValue("pStAdd", add);
                        rpt.SetParameterValue("pVan", UserInfo.Van_ID + ": " + UserInfo.FullName);
                        rpt.SetParameterValue("pSmsVersion", "SMS MINI v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString());

                        string f = "";
                        SaveFileDialog DialogSave = new SaveFileDialog();
                        DialogSave.DefaultExt = "xls";
                        DialogSave.Filter = "Adobe Acrobat  (*.pdf)|*.pdf|All files (*.*)|*.*";
                        DialogSave.AddExtension = true;
                        DialogSave.RestoreDirectory = true;
                        DialogSave.Title = "Where do you want to save the file?";
                        DialogSave.InitialDirectory = @"C:/";


                        string sv = "Fixed_Asset_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".pdf";

                        DialogSave.FileName = sv;

                        if (DialogSave.ShowDialog() == DialogResult.OK)
                        {
                            f = DialogSave.FileName.ToString();
                            DialogSave.Dispose();
                            DialogSave = null;

                            try
                            {
                                ExportOptions CrExportOptions;
                                DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                                PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();
                                CrDiskFileDestinationOptions.DiskFileName = f;


                                CrExportOptions = rpt.ExportOptions;
                                {
                                    CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                                    CrExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                                    CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
                                    CrExportOptions.FormatOptions = CrFormatTypeOptions;

                                }
                                rpt.Export();
                                rpt.Close();

                                if (System.IO.File.Exists(f))
                                {
                                    System.Diagnostics.Process.Start(f);
                                }
                            }

                            catch (Exception ex)
                            {
                                MessageBox.Show("ข้อผิดพลาด " + ex.ToString(), "เกิดข้อผิดพลาดในการ Export ไฟล์ SV PDF");
                            }
                        }

                    }
                }
            }
        }

        private void txtPartTypeWt1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < "0"[0] || e.KeyChar > "9"[0]) && e.KeyChar != ControlChars.Back && e.KeyChar != "."[0])
                e.Handled = true;
        }

        private void txtPartTypeWt2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < "0"[0] || e.KeyChar > "9"[0]) && e.KeyChar != ControlChars.Back && e.KeyChar != "."[0])
                e.Handled = true;
        }

        private void txtPartTypeWt3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < "0"[0] || e.KeyChar > "9"[0]) && e.KeyChar != ControlChars.Back && e.KeyChar != "."[0])
                e.Handled = true;
        }

        private void txtYearWarranty_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < "0"[0] || e.KeyChar > "9"[0]) && e.KeyChar != ControlChars.Back && e.KeyChar != "."[0])
                e.Handled = true;

        }

        private void txtYearWarranty_KeyDown(object sender, KeyEventArgs e)
        {
            /* //20180905 ก่อนเพิ่ม month
            //if (e.KeyCode == Keys.Enter)
            //{
            //    dtInstall.Format = DateTimePickerFormat.Custom; //20141110
            //    var iYear = int.Parse(txtYearWarranty.Text);

            //    dtExpireDate.Format = DateTimePickerFormat.Custom;
            //    dtExpireDate.Value = dtInstall.Value.AddYears(iYear);
            //}
            */
            if (e.KeyCode == Keys.Enter)
            {
                if (rdYear.Checked == true)
                {
                    dtInstall.Format = DateTimePickerFormat.Custom;
                    var iYear = int.Parse(txtYearWarranty.Text);

                    dtExpireDate.Format = DateTimePickerFormat.Custom;
                    dtExpireDate.Value = dtInstall.Value.AddYears(iYear);
                }
                else
                {

                    dtInstall.Format = DateTimePickerFormat.Custom;
                    var iMonth = int.Parse(txtYearWarranty.Text);

                    dtExpireDate.Format = DateTimePickerFormat.Custom;
                    dtExpireDate.Value = dtInstall.Value.AddMonths(iMonth);
                }
            }
        }

        private void txtCheckGauge_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < "0"[0] || e.KeyChar > "9"[0]) && e.KeyChar != ControlChars.Back && e.KeyChar != "."[0])
                e.Handled = true;
        }

        private void chkInstallDate_Click(object sender, EventArgs e)
        {
            DateTime nullDate = new DateTime(1900, 1, 1);

            //dtInstall.Format = DateTimePickerFormat.Custom;
            //dtExpireDate.Format = DateTimePickerFormat.Custom;
            if (chkInstallDate.Checked == true)
            {
                dtInstall.Enabled = true;
                txtYearWarranty.Enabled = true;

                dtInstall.Format = DateTimePickerFormat.Custom;
                dtExpireDate.Format = DateTimePickerFormat.Custom;
                dtInstall.CustomFormat = "d/M/yyyy";
                dtExpireDate.CustomFormat = "d/M/yyyy";


            }
            else
            {
                //dtInstall.Tag = DBNull.Value;
                //dtInstall.Value = nullDate; 
                //dtInstall.DataBindings[0].WriteValue();
                //dtInstall.Format = DateTimePickerFormat.Custom;

                dtInstall.CustomFormat = " ";

                dtInstall.Enabled = false;
                txtYearWarranty.Enabled = false;

                txtYearWarranty.Text = "0";

                //20141110
                dtExpireDate.Format = DateTimePickerFormat.Custom;



                //20200721 dtExpireDate.CustomFormat = "d/M/yyyy";
                dtExpireDate.CustomFormat = " ";//20200721
            }
        }

        //private void chkPartType1_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (chkPartType1.Checked == true)
        //    {
        //        chkPartType2.Checked = false;
        //        chkPartType3.Checked = false;
        //    }
        //}

        //private void chkPartType2_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (chkPartType2.Checked == true)
        //    {
        //        chkPartType1.Checked = false;
        //        chkPartType3.Checked = false;
        //    }
        //}

        //private void chkPartType3_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (chkPartType3.Checked == true)
        //    {
        //        chkPartType2.Checked = false;
        //        chkPartType1.Checked = false;
        //    }
        //}

        private void setDataGridViewStyle()
        {
            for (int r = 0; r <= dgFFA.RowCount - 1; r++)
            {
                dgFFA["SerialNumber", r].Style.BackColor = Color.Gray;
                if (IsConfirmFFA)
                {
                    dgFFA["cIsConfirm", r].Style.BackColor = Color.White;
                    dgFFA["cIsConfirm", r].ReadOnly = false;

                    dgFFA["cLocation", r].Style.BackColor = Color.White;
                    dgFFA["cDetail", r].Style.BackColor = Color.White;
                }
                else
                {
                    dgFFA["cIsConfirm", r].Style.BackColor = Color.Gray;
                    dgFFA["cIsConfirm", r].ReadOnly = true;

                    dgFFA["cLocation", r].Style.BackColor = Color.Gray;
                    dgFFA["cLocation", r].ReadOnly = true; //20141209

                    dgFFA["cDetail", r].Style.BackColor = Color.Gray;
                    dgFFA["cDetail", r].ReadOnly = true;  //20141209
                }

                dgFFA["cFCAT_ID", r].Style.BackColor = Color.Gray;
                dgFFA["cFixedAsset_CateName", r].Style.BackColor = Color.Gray;
                dgFFA["cFBA_ID", r].Style.BackColor = Color.Gray;
                dgFFA["cFBrand", r].Style.BackColor = Color.Gray;
                dgFFA["cFMD_ID", r].Style.BackColor = Color.Gray;
                dgFFA["cFModel", r].Style.BackColor = Color.Gray;
                dgFFA["cOwnerShip", r].Style.BackColor = Color.Gray;
                dgFFA["cInstallDate", r].Style.BackColor = Color.Gray;
                dgFFA["cWarrantDate", r].Style.BackColor = Color.Gray;
                dgFFA["cSTA_ID", r].Style.BackColor = Color.Gray;
                dgFFA["cStationSys", r].Style.BackColor = Color.Gray;
                dgFFA["cAddress", r].Style.BackColor = Color.Gray;
                dgFFA["cTelNo", r].Style.BackColor = Color.Gray;
                dgFFA["cFaxNo", r].Style.BackColor = Color.Gray;
                dgFFA["cProvince", r].Style.BackColor = Color.Gray;
                dgFFA["cPartType1", r].Style.BackColor = Color.Gray;
                dgFFA["cPartType2", r].Style.BackColor = Color.Gray;
                dgFFA["cPartType3", r].Style.BackColor = Color.Gray;
                dgFFA["cCheckGauge", r].Style.BackColor = Color.Gray;
                dgFFA["cControlFixAssetNo", r].Style.BackColor = Color.Gray;
                dgFFA["cTranDate", r].Style.BackColor = Color.Gray;
                dgFFA["cTranEMP", r].Style.BackColor = Color.Gray;
                dgFFA["cConfirmEMP", r].Style.BackColor = Color.Gray;
                dgFFA["cConfirmDate", r].Style.BackColor = Color.Gray;
                dgFFA["cIsLifetime", r].Style.BackColor = Color.Gray;
                dgFFA["cIsSpecialPrice", r].Style.BackColor = Color.Gray;



            }
        }

        private void dgFFA_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dgFFA.CurrentCell is DataGridViewCheckBoxCell)
                dgFFA.CommitEdit(DataGridViewDataErrorContexts.Commit);

        }

        private void dgFFA_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {


            if (e.RowIndex != -1 && (e.ColumnIndex == 1 || e.ColumnIndex == 2 || e.ColumnIndex == 3))
            {
                using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
                {
                    DateTime Confirmdate = dc.ExecuteQuery<DateTime>("SELECT GETDATE()").First();
                    dgFFA["cConfirmEMP", e.RowIndex].Value = UserInfo.UserId;
                    dgFFA["cConfirmDate", e.RowIndex].Value = Confirmdate;

                }
            }

        }

        private void comb_cat_SelectionChangeCommitted(object sender, EventArgs e)
        {
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            {
                comb_brand.DataSource = dc.FFixedAsset_Brands
                    .Where(t => (t.FCAT_ID == int.Parse(comb_cat.SelectedValue.ToString())
                        //20160422 || t.FBA_ID == 999
                        ))
                         .Select(t => new
                         {
                             FBA_ID = t.FBA_ID,
                             FBrand = t.FBA_ID.ToString() + ": " + t.FBrand,
                         })
                    .ToList();

                comb_brand.DisplayMember = "FBrand";
                comb_brand.ValueMember = "FBA_ID";
            }
        }

        private void btControlFFA_Click(object sender, EventArgs e)
        {
            //if (JanawatEmployee.ClsAccount.GetEmpID() == string.Empty)
            //{
            //    MessageBox.Show("กรุณา Login เข้าระบบ", "ผลการตรวจสอบ", MessageBoxButtons.OK,
            //        MessageBoxIcon.Information);
            //    xpServicesMenu(false, false, false, false, true, false);
            //    return;
            //}

            Cursor.Current = Cursors.AppStarting;
            foreach (Form currentForm in this.MdiChildren)
            {
                if (currentForm is FFA.ControlFFA)
                    return;
            }
            var f = new FFA.ControlFFA();
            f.WindowState = FormWindowState.Normal;

            f.ShowDialog();
            Cursor.Current = Cursors.Default;
        }

        private void rdMonth_Click(object sender, EventArgs e)
        {
            rdYear.Checked = false;
        }

        private void rdYear_Click(object sender, EventArgs e)
        {
            rdMonth.Checked = false;
        }

        private void dgFFA_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                string xMenu = null;
                if (IsConfirmFFA)
                {
                    xMenu = "ยืนยันข้อมูล Fixed Asset";
                }
                else
                {
                    xMenu = "บันทึกข้อมูล Fixed Asset";
                }

                #region ลบข้อมูล
                if (e.ColumnIndex == 0)
                {
                    if (MessageBox.Show("คุณต้องการ ลบ Serialnumber : " + dgFFA["Serialnumber", e.RowIndex].Value.ToString() + " ออกจากสถานีนี้ ใช่หรือไม่???...", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        try
                        {

                            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
                            {

                                var srv = dc
                                         .ExecuteQuery<DateTime>("SELECT GETDATE()")
                                         .First();
                                if (srv != null)
                                {
                                    servDate = srv;
                                }


                                var DetailChkDel = dc.FFixedAsset_Station_Details
                                      .Where(t => t.SerialNumber == dgFFA["Serialnumber", e.RowIndex].Value.ToString())
                                      .FirstOrDefault();

                                if (DetailChkDel != null)
                                {
                                    bool SNFSam = false;
                                    if (!string.IsNullOrEmpty(DetailChkDel.TranEMP))
                                    {
                                        var detailChkDep = dc.Employees
                                                   .Where(t => new string[] { "11", "24", "25", "14","10","29","30"
                                            }.Contains(t.DEP_ID) && t.EMP_ID == DetailChkDel.TranEMP)
                                                   .FirstOrDefault();

                                        if (detailChkDep != null)
                                        {
                                            SNFSam = true;
                                        }
                                    }

                                    if (!new string[] { "11", "24", "25", "14", "10", "29", "30" }.Contains(UserInfo.eDepID))  //--เปิดให้ sam ลบได้ตลอดก่อนชั่วคราว //14 bdd
                                    {
                                        if (string.IsNullOrEmpty(DetailChkDel.TranDate.ToString()) || DetailChkDel.TranDate.Value.Date < DateTime.Now.Date)
                                        {
                                            MessageBox.Show("ระบบห้ามลบ Serailnumber  ที่บันทึก ข้ามวัน ", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                            return;
                                        }
                                        if (SNFSam == true || string.IsNullOrEmpty(DetailChkDel.TranEMP))
                                        {
                                            MessageBox.Show("ระบบห้ามลบ Serailnumber  ที่บันทึกจากส่วนกลาง ", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                            return;
                                        }
                                    }//dep ต่อไปถ้าเป็น sam จะเช็คเรื่องข้ามวัน และลบได้เฉพาะของแผนกตัวเอง  SNFSam == false

                                }



                                //===20190502==
                                var ld = new DAL.SMSManage.Log_DeleteDatabyProgram
                                {
                                    Trandate = servDate,
                                    DataID = dgFFA["Serialnumber", e.RowIndex].Value.ToString(),//txtJOB_ID.Text.Trim(),
                                    TableName = "FFixedAsset_Station_Detail",
                                    Menu = xMenu,
                                    Detail = "Delete SN " + dgFFA["Serialnumber", e.RowIndex].Value.ToString() + " STA_ID " + dgFFA["cSTA_ID", e.RowIndex].Value.ToString(),
                                    UserID = UserInfo.UserId,
                                    Apps = "SMSMINI"

                                };

                                dc.Log_DeleteDatabyPrograms.InsertOnSubmit(ld);
                                dc.SubmitChanges();
                                //=============
                                string a = dgFFA["Serialnumber", e.RowIndex].Value.ToString();
                                //string b = dgFFA["FCAT_ID", e.RowIndex].Value.ToString();


                                ClearPoolStatusUse(dgFFA["Serialnumber", e.RowIndex].Value.ToString(), int.Parse(dgFFA["cFCAT_ID", e.RowIndex].Value.ToString()), int.Parse(dgFFA["cFMD_ID", e.RowIndex].Value.ToString()));
                                //20210120                                  
                                var newSNTrack = new DAL.SMSManage.FFixedAsset_SerialNumber_Tracking
                                {
                                    TrackingDate = servDate,
                                    SerialNumber_Old = dgFFA["Serialnumber", e.RowIndex].Value.ToString(),
                                    SerialNumber_New = dgFFA["Serialnumber", e.RowIndex].Value.ToString(),
                                    JOB_ID = "",
                                    STA_ID_Old = dgFFA["cSTA_ID", e.RowIndex].Value.ToString(),
                                    STA_ID_New = dgFFA["cSTA_ID", e.RowIndex].Value.ToString(),
                                    VAN_ID = UserInfo.UserId,
                                    SerialNumber_Status = "00",
                                    Remark = "Ready (Station Cancel) FixedAsset Tracking",
                                    RegistDate = servDate
                                };

                                dc.FFixedAsset_SerialNumber_Trackings.InsertOnSubmit(newSNTrack);
                                dc.SubmitChanges();

                                //=== 
                                var qdel = (from t in dc.FFixedAsset_Station_Details
                                            where t.SerialNumber.Trim() == dgFFA["Serialnumber", e.RowIndex].Value.ToString() &&
                                            t.STA_ID == dgFFA["cSTA_ID", e.RowIndex].Value.ToString()
                                            select t).FirstOrDefault();
                                if (qdel != null)
                                {
                                    dc.FFixedAsset_Station_Details.DeleteOnSubmit(qdel);
                                    dc.SubmitChanges();
                                }



                                var qcanR = ffStD.Where(t => t.SerialNumber == dgFFA["Serialnumber", e.RowIndex].Value.ToString() &&
                                            t.STA_ID == dgFFA["cSTA_ID", e.RowIndex].Value.ToString()).FirstOrDefault();
                                if (qcanR != null)
                                {
                                    ffStD.Remove(qcanR);
                                    dgFFA.DataSource = ffStD.ToList();
                                    setDataGridViewStyle();
                                }

                            }

                        }
                        catch (System.Exception)
                        {
                        }
                    }
                }
                #endregion
            }
            //if (e.ColumnIndex == 3)
            //{
            //    //using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            //    //{

            //    //    //cLocation.DataSource = null;
            //    //    //cLocation.DataSource = dc.Locations
            //    //    //    .Where(t => (t.FCAT_ID == int.Parse(dgFFA["cFCAT_ID", e.RowIndex].Value.ToString()) || t.FCAT_ID == null) && t.IsCancel == false)
            //    //    //         .Select(t => new
            //    //    //         {
            //    //    //             Location = t.LOC_ID,
            //    //    //             LocationDetail = t.LOC_ID.ToString() + ": " + t.Location1
            //    //    //         })
            //    //    //        .OrderBy(t => t.Location).ToList();

            //    //    //cLocation.DisplayMember = "LocationDetail";
            //    //    //cLocation.ValueMember = "Location";
            //    //    //setDataGridViewStyle();
            //    //}
            //}
        }

        private void setcob_LocatFCAT(int fFCATID)
        {
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            {
                var chkPFFCAT = dc.Locations
                                    .Where(t => (t.FCAT_ID == fFCATID || t.FCAT_ID == null) && t.IsCancel == false)

                                    .Select(t => new
                                    {
                                        LOC_ID = t.LOC_ID,
                                        Location = t.Location1
                                    })//.OrderBy(t => t.POI_ID)
                                    .ToList();
                cbxLocation.DataSource = chkPFFCAT;
                cbxLocation.DisplayMember = "Location";
                cbxLocation.ValueMember = "LOC_ID";
            }
        }

        private void setcob_FCAT_FBA_FMD(int gFCATID, int gFBA)
        {
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            {
                comb_cat.DataSource = dc.FFixedAsset_Categories
                     .Where(t => t.IsCancel == false //&& t.FCAT_ID == gFCATID
                     )
                     .Select(t => new
                     {
                         cateID = t.FCAT_ID,
                         cateName = t.FCAT_ID.ToString() + ": " + t.FixedAsset_CateName
                     })
                     .ToList();
                comb_cat.DisplayMember = "cateName";
                comb_cat.ValueMember = "cateID";

                comb_brand.DataSource = dc.FFixedAsset_Brands
                    .Where(t => t.FCAT_ID == gFCATID)
                    .Select(t => new
                    {
                        brandID = t.FBA_ID,
                        brand = t.FBA_ID.ToString() + ": " + t.FBrand
                    })
                    .ToList();
                comb_brand.DisplayMember = "brand";
                comb_brand.ValueMember = "brandID";

                comb_model.DataSource = dc.FFixedAsset_Models
                     .Where(t => t.FBA_ID == gFBA)
                    .Select(t => new
                    {
                        modelID = t.FMD_ID,
                        FModel = t.FMD_ID.ToString() + ": " + t.FModel
                    })
                    .ToList();
                comb_model.DisplayMember = "FModel";
                comb_model.ValueMember = "modelID";
            }
        }

        private void btnChangeModelFFA_Click(object sender, EventArgs e)
        {

            var f = new ChangeModelFFA();
            f.WindowState = FormWindowState.Normal;

            var result = f.ShowDialog();
            if (result == DialogResult.OK)
            {


            }
        }




    }
}
