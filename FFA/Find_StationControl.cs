﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace SMSMINI.FFA
{
    public partial class Find_StationControl : Form
    {
        public string Station { get; set; }
        public string StationSys { get; set; }
        public string STA_ID { get; set; }
        public Find_StationControl()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex != -1)
                {
                    STA_ID = dataGridView1["รหัส", e.RowIndex].Value.ToString();
                    StationSys = dataGridView1["Station", e.RowIndex].Value.ToString();
                }

                DialogResult = DialogResult.OK;
            }
            catch (Exception)
            {

                DialogResult = DialogResult.Cancel;
            }
        }

        private void Find_StationControl_Load(object sender, EventArgs e)
        {
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            {
                var result = (from st in dc.Stations
                              where (st.StationSys.Contains(Station.Trim()) || st.STA_ID.Contains(Station.Trim()))
                              select new
                              {
                                  รหัส = st.STA_ID,
                                  //SiteID = st.STA_ID1,
                                  Station = st.StationSys,
                                  ที่อยู่ = st.Address,
                                  เบอร์โทร = st.TelNo

                              }).Distinct();
                dataGridView1.DataSource = result;
                dataGridView1.Refresh();
            }
        }
    }
}
