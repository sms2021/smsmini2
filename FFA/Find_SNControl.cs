﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace SMSMINI.FFA
{
    public partial class Find_SNControl : Form
    {
        public string SN { get; set; }
        public string fActStatus { get; set; }
        public string fRemark { get; set; }
        public string fReserv { get; set; }
        public string fStation { get; set; }

        List<FFA_Find_Control> lsFFA_Find_Control = new List<FFA_Find_Control>();

        public Find_SNControl()
        {
            InitializeComponent();
        }

        private void Find_SNControl_Load(object sender, EventArgs e)
        {
            if (SN != null)
            {
                using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
                {


                    //var q = (from t in dc.FFixedAsset_SerialNumber_Pools
                    //         from c in dc.FFixedAsset_Categories
                    //         from b in dc.FFixedAsset_Brands
                    //         from m in dc.FFixedAsset_Models
                    //         from s in dc.FFixedAsset_SNStatus

                    //         where (t.FMD_ID == m.FMD_ID && t.FCAT_ID == c.FCAT_ID) &&
                    //                (m.FBA_ID == b.FBA_ID) && t.StatusUse == s.FSTUS_ID &&
                    //                (
                    //                    t.SerialNumber.Contains(SN) &&
                    //                    new string[] { "00", "66", "76", "77" }.Contains(t.StatusUse)
                    //                )

                    //         join fs in dc.FFixedAsset_ControlTrans on t.SerialNumber equals fs.SerialNumber into tmpffa1
                    //         from ffa1 in tmpffa1.DefaultIfEmpty()
                    //         join ac in dc.FFixedAsset_Actions on ffa1.FACT_ID equals ac.FACT_ID into tmpffa
                    //         from ffa in tmpffa.DefaultIfEmpty()
                    //         join st in dc.Stations on ffa1.STA_ID equals st.STA_ID into tmpffa2
                    //         from ffa2 in tmpffa2.DefaultIfEmpty()
                    //         select new
                    //         {
                    //             SerialNumber = t.SerialNumber,
                    //             สถานะ = ffa.FACT_ID + ":" + ffa.FACT_Status,
                    //             หมายเหตุ = ffa1.Remark,
                    //             รหัสสถานี = ffa2.STA_ID,
                    //             สถานี = ffa2.StationSys,
                    //             วันที่ = (DateTime?)ffa1.TranDate,
                    //             ประเภทอุปกรณ์ = c.FixedAsset_CateName,
                    //             ยี่ห้อ = b.FBrand,
                    //             // FMD_ID = t.FMD_ID,
                    //             รุ่น = m.FModel,
                    //             สถานะการใช้งาน = s.FFAStatus
                    //         })
                    //                .Distinct()
                    //               .OrderBy(t => t.SerialNumber)
                    //                .ToList();

                    //if (q.Count() > 0)
                    //{
                    //    dataGridView1.DataSource = q;
                    //}




                    //var q = (from t in dc.FFixedAsset_SerialNumber_Pools
                    //         from c in dc.FFixedAsset_Categories
                    //         from b in dc.FFixedAsset_Brands
                    //         from m in dc.FFixedAsset_Models
                    //         from s in dc.FFixedAsset_SNStatus

                    //         where (t.FMD_ID == m.FMD_ID && t.FCAT_ID == c.FCAT_ID) &&
                    //                (m.FBA_ID == b.FBA_ID) && t.StatusUse == s.FSTUS_ID &&
                    //                (
                    //                    t.SerialNumber.Contains(SN) &&
                    //                    new string[] { "00", "66", "76", "77" }.Contains(t.StatusUse)
                    //                )

                    //         join fs in dc.FFixedAsset_ControlTrans on t.SerialNumber equals fs.SerialNumber into tmpffa1
                    //         from ffa1 in tmpffa1.DefaultIfEmpty()
                    //         join ac in dc.FFixedAsset_Actions on ffa1.FACT_ID equals ac.FACT_ID into tmpffa
                    //         from ffa in tmpffa.DefaultIfEmpty()
                    //         join st in dc.Stations on ffa1.STA_ID equals st.STA_ID into tmpffa2
                    //         from ffa2 in tmpffa2.DefaultIfEmpty()
                    //         select new
                    //         {
                    //             SerialNumber = t.SerialNumber,
                    //             สถานะ = ffa.FACT_ID + ":" + ffa.FACT_Status,
                    //             หมายเหตุ = ffa1.Remark,
                    //             รหัสสถานี = ffa2.STA_ID,
                    //             สถานี = ffa2.StationSys,
                    //             วันที่ = (DateTime?)ffa1.TranDate,
                    //             ประเภทอุปกรณ์ = c.FixedAsset_CateName,
                    //             ยี่ห้อ = b.FBrand,
                    //             // FMD_ID = t.FMD_ID,
                    //             รุ่น = m.FModel,
                    //             สถานะการใช้งาน = s.FFAStatus
                    //         })
                    //                .Distinct()
                    //               .OrderBy(t => t.SerialNumber)
                    //     .ToList();
                    //              // .FirstOrDefault();

                    //if (q.Count() > 0)
                    //{
                    //    ////var qmax = (from qm in dc.Conf_Conditions select t).Max();

                    //    //var qmax = q.Where(qm => qm.SerialNumber.Contains(SN))

                    //    //   //.OrderByDescending(qm => qm.วันที่).Take(1).ToList();
                    //    //   .OrderByDescending(qm => qm.วันที่).ToList();

                    //    ////var qmax = q.Where(qm => qm.SerialNumber.Contains(q.SerialNumber))

                    //    ////  // .OrderByDescending(qm => qm.วันที่).Take(1).ToList();
                    //    ////   .OrderByDescending(qm => qm.วันที่).ToList();

                    //    //dataGridView1.DataSource = qmax;

                    //    // lsFFA_Find_Control.Add(new FFA_Find_Control
                    //    //{

                    //    //});
                    //    foreach (var x1 in q)
                    //    {
                    //        if (x1.วันที่ == null)
                    //        {
                    //            lsFFA_Find_Control.Add(new FFA_Find_Control
                    //           {
                    //               //DataRow dr = dtPart.NewRow();
                    //               SerialNumber = x1.SerialNumber,
                    //               สถานะ = x1.สถานะ,
                    //               หมายเหตุ = x1.หมายเหตุ,
                    //               รหัสสถานี = x1.รหัสสถานี,
                    //               สถานี = x1.สถานี,
                    //               วันที่ = x1.วันที่,
                    //               ประเภทอุปกรณ์ = x1.ประเภทอุปกรณ์,
                    //               ยี่ห้อ = x1.ยี่ห้อ,
                    //               รุ่น = x1.รุ่น,
                    //               สถานะการใช้งาน = x1.สถานะการใช้งาน
                    //           });
                    //        }
                    //    }
                    //            var qmax = q.Where(qm => qm.SerialNumber == x1.SerialNumber)
                    //                .OrderByDescending(qm => qm.วันที่).Take(1).FirstOrDefault();
                    //            lsFFA_Find_Control.Add(new FFA_Find_Control
                    //            {
                    //                //DataRow dr = dtPart.NewRow();
                    //                SerialNumber = qmax.SerialNumber,
                    //                สถานะ = qmax.สถานะ,
                    //                หมายเหตุ = qmax.หมายเหตุ,
                    //                รหัสสถานี = qmax.รหัสสถานี,
                    //                สถานี = qmax.สถานี,
                    //                วันที่ = qmax.วันที่,
                    //                ประเภทอุปกรณ์ = qmax.ประเภทอุปกรณ์,
                    //                ยี่ห้อ = qmax.ยี่ห้อ,
                    //                รุ่น = qmax.รุ่น,
                    //                สถานะการใช้งาน = qmax.สถานะการใช้งาน
                    //            });


                    //        dataGridView1.DataSource = lsFFA_Find_Control.ToList();

                    //    //dataGridView1.DataSource = q;
                    //}



                    //--============================================

                    var q = (from t in dc.vw_Find_FFAControls
                             where t.SerialNumber.Contains(SN)

                             select new
                             {
                                 SerialNumber = t.SerialNumber,
                                 สถานะ = t.FACT_Status,
                                 หมายเหตุ = t.Remark,
                                 รหัสสถานี = t.STA_ID,
                                 สถานี = t.StationSys,
                                 ผู้บันทึก = t.FName,
                                 วันที่ = (DateTime?)t.TranDate,
                                 ประเภทอุปกรณ์ = t.FixedAsset_CateName,
                                 ยี่ห้อ = t.FBrand,
                                 รุ่น = t.FModel,
                                 สถานะการใช้งาน = t.FFAStatus
                             })
                                    .Distinct()
                                   .OrderBy(t => t.SerialNumber)
                                    .ToList();

                    if (q.Count() > 0)
                    {
                        dataGridView1.DataSource = q;
                    }
                    //else
                    //{
                    //    //this.Hide();
                    //    DialogResult = DialogResult.Ignore;
                    //    this.Close();

                    //    var qDetail = dc.FFixedAsset_Station_Details.Where(t => t.SerialNumber == SN).FirstOrDefault();

                    //    if (qDetail != null)
                    //    {
                    //        var qStation = dc.Stations.Where(t => t.STA_ID == qDetail.STA_ID).FirstOrDefault();

                    //        MessageBox.Show("Serial Number: [" + SN + "] ถูกใช้งานแล้ว ที่สถานี  " + qStation.STA_ID + " : " + qStation.StationSys + " ", "ผลการค้นหา", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //        //DialogResult = DialogResult.Ignore;
                    //        //this.Close();
                    //    }
                    //    else
                    //    {//11 แต่ไม่มีใน detail ถูกนำไปปิดjob  
                    //        var qDup = dc.FFixedAsset_SerialNumber_Pools.Where(t => t.SerialNumber == SN && t.StatusUse == "11").FirstOrDefault();
                    //        if (qDup != null)
                    //        {
                    //            var qAlertCloseJOB = (from jfd in dc.JOB_Failure_Details
                    //                                  from j in dc.JOBs
                    //                                  from s in dc.Stations
                    //                                  from t in dc.FFixedAsset_SerialNumber_Pools
                    //                                  from c in dc.FFixedAsset_Categories
                    //                                  from b in dc.FFixedAsset_Brands
                    //                                  from m in dc.FFixedAsset_Models

                    //                                  where (t.FMD_ID == m.FMD_ID && t.FCAT_ID == c.FCAT_ID) &&
                    //                                           (m.FBA_ID == b.FBA_ID) && (t.SerialNumber == jfd.SerialNumber) &&
                    //                                           (jfd.JOB_ID == j.JOB_ID && j.STA_ID == s.STA_ID) &&
                    //                                           ((jfd.SerialNumber_New == SN) || (jfd.SerialNumber == SN))

                    //                                  select new
                    //                                  {
                    //                                      SerialNumber = jfd.SerialNumber,
                    //                                      FCAT_NAME = c.FixedAsset_CateName,
                    //                                      FBrand = b.FBrand,
                    //                                      FModel = m.FModel,
                    //                                      StatusUse = t.StatusUse,
                    //                                      Station = s.STA_ID + " " + s.StationSys,
                    //                                      Trandate = jfd.trandate

                    //                                      //20170116}).OrderByDescending(t =>t.Trandate).Distinct().First();//.Distinct().First();
                    //                                  }).OrderByDescending(t => t.Trandate).Distinct().FirstOrDefault();

                    //            if (qAlertCloseJOB != null)
                    //            {
                    //                MessageBox.Show("Serial Number: [" + SN + "] มีข้อมูลแล้ว \n ในอุปกรณ์ :  " + qAlertCloseJOB.FCAT_NAME + "  ยี่ห้อ : " + qAlertCloseJOB.FBrand + " รุ่น : " + qAlertCloseJOB.FModel + " " + Environment.NewLine +
                    //                    "สถานะ : ถูกนำไปปิดงาน ก่อนบันทึกลงสถานี " + Environment.NewLine +
                    //                    "สถานี : " + qAlertCloseJOB.Station + "  ", "ผลการค้นหา", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    //                 //DialogResult = DialogResult.Ignore;
                    //                 //this.Close();
                    //            }

                    //            //----========
                    //        }
                    //    }
                    //}//else








                }//using

            }
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex != -1)
                {
                    SN = dataGridView1["SerialNumber", e.RowIndex].Value.ToString();
                    fActStatus = dataGridView1["สถานะ", e.RowIndex].Value == null ? "00" : dataGridView1["สถานะ", e.RowIndex].Value.ToString();
                    fRemark = dataGridView1["หมายเหตุ", e.RowIndex].Value == null ? "" : dataGridView1["หมายเหตุ", e.RowIndex].Value.ToString();
                    fReserv = dataGridView1["รหัสสถานี", e.RowIndex].Value == null ? "ไม่จอง" : dataGridView1["รหัสสถานี", e.RowIndex].Value.ToString();
                    fStation = dataGridView1["สถานี", e.RowIndex].Value == null ? "" : dataGridView1["สถานี", e.RowIndex].Value.ToString();
                }

                DialogResult = DialogResult.OK;
            }
            catch (Exception)
            {

                DialogResult = DialogResult.Cancel;
            }
        }

        public class FFA_Find_Control
        {
            //public int NO { get; set; }
            public string SerialNumber { get; set; }
            public string สถานะ { get; set; }
            public string หมายเหตุ { get; set; }
            public string รหัสสถานี { get; set; }
            public string สถานี { get; set; }
            public DateTime? วันที่ { get; set; }

            public string ประเภทอุปกรณ์ { get; set; }
            public string ยี่ห้อ { get; set; }
            public string รุ่น { get; set; }
            public string สถานะการใช้งาน { get; set; }


        }

    }
}
