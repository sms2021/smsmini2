﻿namespace SMSMINI.FFA
{
    partial class ChangeModelFFA
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChangeModelFFA));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.toolStrip_Mnu = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.txtSNFind = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_FindStation = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_Save = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_Cancel = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_Exit = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel_Dateime = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnSelect = new System.Windows.Forms.Button();
            this.txtRemark = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtSNc = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cbxFBAIDc = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cbxFCATIDc = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cbxFMDIDc = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.txtSNo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbxFBAIDo = new System.Windows.Forms.ComboBox();
            this.cbxFMDIDo = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbxFCATIDo = new System.Windows.Forms.ComboBox();
            this.dataGridView_ChgModel = new System.Windows.Forms.DataGridView();
            this.clCancel = new System.Windows.Forms.DataGridViewImageColumn();
            this.toolStrip_Mnu.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_ChgModel)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip_Mnu
            // 
            this.toolStrip_Mnu.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip_Mnu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.toolStripSeparator4,
            this.txtSNFind,
            this.toolStripSeparator2,
            this.toolStripButton_FindStation,
            this.toolStripSeparator3,
            this.toolStripButton_Save,
            this.toolStripButton_Cancel,
            this.toolStripSeparator1,
            this.toolStripButton_Exit,
            this.toolStripLabel_Dateime,
            this.toolStripSeparator5});
            this.toolStrip_Mnu.Location = new System.Drawing.Point(0, 0);
            this.toolStrip_Mnu.Name = "toolStrip_Mnu";
            this.toolStrip_Mnu.Size = new System.Drawing.Size(741, 39);
            this.toolStrip_Mnu.TabIndex = 1;
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(85, 36);
            this.toolStripLabel1.Text = "Serial number";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 39);
            // 
            // txtSNFind
            // 
            this.txtSNFind.AutoSize = false;
            this.txtSNFind.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.txtSNFind.ForeColor = System.Drawing.SystemColors.Desktop;
            this.txtSNFind.Name = "txtSNFind";
            this.txtSNFind.Size = new System.Drawing.Size(250, 20);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripButton_FindStation
            // 
            this.toolStripButton_FindStation.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_FindStation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.toolStripButton_FindStation.Image = global::SMSMINI.Properties.Resources.Find48;
            this.toolStripButton_FindStation.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_FindStation.Name = "toolStripButton_FindStation";
            this.toolStripButton_FindStation.Size = new System.Drawing.Size(36, 36);
            this.toolStripButton_FindStation.ToolTipText = "ค้นหาข้อมูล";
            this.toolStripButton_FindStation.Click += new System.EventHandler(this.toolStripButton_FindStation_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripButton_Save
            // 
            this.toolStripButton_Save.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.toolStripButton_Save.ForeColor = System.Drawing.SystemColors.Desktop;
            this.toolStripButton_Save.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Save.Image")));
            this.toolStripButton_Save.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Save.Name = "toolStripButton_Save";
            this.toolStripButton_Save.Size = new System.Drawing.Size(72, 36);
            this.toolStripButton_Save.Text = "บันทึก";
            this.toolStripButton_Save.ToolTipText = "Upload JOB";
            this.toolStripButton_Save.Click += new System.EventHandler(this.toolStripButton_Save_Click);
            // 
            // toolStripButton_Cancel
            // 
            this.toolStripButton_Cancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.toolStripButton_Cancel.ForeColor = System.Drawing.SystemColors.Desktop;
            this.toolStripButton_Cancel.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Cancel.Image")));
            this.toolStripButton_Cancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Cancel.Name = "toolStripButton_Cancel";
            this.toolStripButton_Cancel.Size = new System.Drawing.Size(75, 36);
            this.toolStripButton_Cancel.Text = "ยกเลิก";
            this.toolStripButton_Cancel.ToolTipText = "ยกเลิกข้อมูล";
            this.toolStripButton_Cancel.Click += new System.EventHandler(this.toolStripButton_Cancel_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripButton_Exit
            // 
            this.toolStripButton_Exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.toolStripButton_Exit.ForeColor = System.Drawing.SystemColors.Desktop;
            this.toolStripButton_Exit.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Exit.Image")));
            this.toolStripButton_Exit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Exit.Name = "toolStripButton_Exit";
            this.toolStripButton_Exit.Size = new System.Drawing.Size(108, 36);
            this.toolStripButton_Exit.Text = "ออกโปรแกรม";
            this.toolStripButton_Exit.ToolTipText = "ออกจากหน้าจอ";
            this.toolStripButton_Exit.Click += new System.EventHandler(this.toolStripButton_Exit_Click);
            // 
            // toolStripLabel_Dateime
            // 
            this.toolStripLabel_Dateime.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripLabel_Dateime.Font = new System.Drawing.Font("Tahoma", 10F);
            this.toolStripLabel_Dateime.ForeColor = System.Drawing.SystemColors.Desktop;
            this.toolStripLabel_Dateime.Name = "toolStripLabel_Dateime";
            this.toolStripLabel_Dateime.Size = new System.Drawing.Size(0, 36);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 39);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.groupBox2.Controls.Add(this.btnSelect);
            this.groupBox2.Controls.Add(this.txtRemark);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.txtSNc);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.cbxFBAIDc);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.cbxFCATIDc);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.cbxFMDIDc);
            this.groupBox2.Location = new System.Drawing.Point(0, 99);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(741, 105);
            this.groupBox2.TabIndex = 27;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "ข้อมูลใหม่";
            // 
            // btnSelect
            // 
            this.btnSelect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelect.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.btnSelect.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btnSelect.Location = new System.Drawing.Point(579, 31);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(149, 60);
            this.btnSelect.TabIndex = 42;
            this.btnSelect.Text = "เลือกรายการ";
            this.btnSelect.UseVisualStyleBackColor = false;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // txtRemark
            // 
            this.txtRemark.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtRemark.Location = new System.Drawing.Point(196, 72);
            this.txtRemark.Name = "txtRemark";
            this.txtRemark.Size = new System.Drawing.Size(369, 20);
            this.txtRemark.TabIndex = 21;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(193, 55);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 13);
            this.label7.TabIndex = 22;
            this.label7.Text = "หมายเหตุการแก้ไข:";
            // 
            // txtSNc
            // 
            this.txtSNc.Enabled = false;
            this.txtSNc.Location = new System.Drawing.Point(10, 72);
            this.txtSNc.Name = "txtSNc";
            this.txtSNc.Size = new System.Drawing.Size(180, 20);
            this.txtSNc.TabIndex = 17;
            this.txtSNc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSNc_KeyDown);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 57);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "Serial number:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(147, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "ยี่ห้อ:";
            // 
            // cbxFBAIDc
            // 
            this.cbxFBAIDc.FormattingEnabled = true;
            this.cbxFBAIDc.Location = new System.Drawing.Point(150, 32);
            this.cbxFBAIDc.Name = "cbxFBAIDc";
            this.cbxFBAIDc.Size = new System.Drawing.Size(156, 21);
            this.cbxFBAIDc.TabIndex = 1;
            this.cbxFBAIDc.SelectedIndexChanged += new System.EventHandler(this.cbxFBAIDc_SelectedIndexChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 16);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(45, 13);
            this.label10.TabIndex = 5;
            this.label10.Text = "อุปกรณ์:";
            // 
            // cbxFCATIDc
            // 
            this.cbxFCATIDc.FormattingEnabled = true;
            this.cbxFCATIDc.Location = new System.Drawing.Point(9, 32);
            this.cbxFCATIDc.Name = "cbxFCATIDc";
            this.cbxFCATIDc.Size = new System.Drawing.Size(135, 21);
            this.cbxFCATIDc.TabIndex = 0;
            this.cbxFCATIDc.SelectionChangeCommitted += new System.EventHandler(this.cbxFCATIDc_SelectionChangeCommitted);
            this.cbxFCATIDc.SelectedIndexChanged += new System.EventHandler(this.cbxFCATIDc_SelectedIndexChanged);
            this.cbxFCATIDc.SelectedValueChanged += new System.EventHandler(this.cbxFCATIDc_SelectedValueChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(309, 17);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(24, 13);
            this.label11.TabIndex = 7;
            this.label11.Text = "รุ่น:";
            // 
            // cbxFMDIDc
            // 
            this.cbxFMDIDc.FormattingEnabled = true;
            this.cbxFMDIDc.Location = new System.Drawing.Point(312, 32);
            this.cbxFMDIDc.Name = "cbxFMDIDc";
            this.cbxFMDIDc.Size = new System.Drawing.Size(163, 21);
            this.cbxFMDIDc.TabIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.groupBox1.Controls.Add(this.btnDelete);
            this.groupBox1.Controls.Add(this.txtSNo);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cbxFBAIDo);
            this.groupBox1.Controls.Add(this.cbxFMDIDo);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.cbxFCATIDo);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.groupBox1.ForeColor = System.Drawing.Color.Red;
            this.groupBox1.Location = new System.Drawing.Point(0, 42);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(741, 58);
            this.groupBox1.TabIndex = 28;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ข้อมูลเดิมก่อนเปลี่ยนแปลง";
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.BackColor = System.Drawing.Color.Tomato;
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btnDelete.ForeColor = System.Drawing.Color.Black;
            this.btnDelete.Location = new System.Drawing.Point(655, 11);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(80, 40);
            this.btnDelete.TabIndex = 43;
            this.btnDelete.Text = "ลบข้อมูล";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // txtSNo
            // 
            this.txtSNo.Enabled = false;
            this.txtSNo.Location = new System.Drawing.Point(10, 29);
            this.txtSNo.Name = "txtSNo";
            this.txtSNo.Size = new System.Drawing.Size(163, 20);
            this.txtSNo.TabIndex = 43;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(317, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 48;
            this.label1.Text = "ยี่ห้อ:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 14);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 13);
            this.label6.TabIndex = 44;
            this.label6.Text = "Serial number:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(176, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 46;
            this.label2.Text = "อุปกรณ์:";
            // 
            // cbxFBAIDo
            // 
            this.cbxFBAIDo.Enabled = false;
            this.cbxFBAIDo.FormattingEnabled = true;
            this.cbxFBAIDo.Location = new System.Drawing.Point(320, 29);
            this.cbxFBAIDo.Name = "cbxFBAIDo";
            this.cbxFBAIDo.Size = new System.Drawing.Size(156, 21);
            this.cbxFBAIDo.TabIndex = 44;
            // 
            // cbxFMDIDo
            // 
            this.cbxFMDIDo.Enabled = false;
            this.cbxFMDIDo.FormattingEnabled = true;
            this.cbxFMDIDo.Location = new System.Drawing.Point(482, 29);
            this.cbxFMDIDo.Name = "cbxFMDIDo";
            this.cbxFMDIDo.Size = new System.Drawing.Size(163, 21);
            this.cbxFMDIDo.TabIndex = 45;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(479, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(24, 13);
            this.label3.TabIndex = 47;
            this.label3.Text = "รุ่น:";
            // 
            // cbxFCATIDo
            // 
            this.cbxFCATIDo.Enabled = false;
            this.cbxFCATIDo.FormattingEnabled = true;
            this.cbxFCATIDo.Location = new System.Drawing.Point(179, 29);
            this.cbxFCATIDo.Name = "cbxFCATIDo";
            this.cbxFCATIDo.Size = new System.Drawing.Size(135, 21);
            this.cbxFCATIDo.TabIndex = 43;
            // 
            // dataGridView_ChgModel
            // 
            this.dataGridView_ChgModel.AllowUserToAddRows = false;
            this.dataGridView_ChgModel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_ChgModel.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clCancel});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView_ChgModel.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView_ChgModel.Location = new System.Drawing.Point(0, 205);
            this.dataGridView_ChgModel.Name = "dataGridView_ChgModel";
            this.dataGridView_ChgModel.ReadOnly = true;
            this.dataGridView_ChgModel.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridView_ChgModel.Size = new System.Drawing.Size(741, 257);
            this.dataGridView_ChgModel.TabIndex = 38;
            this.dataGridView_ChgModel.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_ChgModel_CellClick);
            // 
            // clCancel
            // 
            this.clCancel.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.clCancel.Description = "ยกเลิกรายการ";
            this.clCancel.HeaderText = "";
            this.clCancel.Image = global::SMSMINI.Properties.Resources.fail;
            this.clCancel.Name = "clCancel";
            this.clCancel.ReadOnly = true;
            this.clCancel.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.clCancel.ToolTipText = "ยกเลิกรายการ";
            this.clCancel.Width = 25;
            // 
            // ChangeModelFFA
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(741, 460);
            this.Controls.Add(this.dataGridView_ChgModel);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.toolStrip_Mnu);
            this.Name = "ChangeModelFFA";
            this.Text = "เปลี่ยนแปลงข้อมูล Fixed Asset";
            this.Load += new System.EventHandler(this.ChangeModelFFA_Load);
            this.toolStrip_Mnu.ResumeLayout(false);
            this.toolStrip_Mnu.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_ChgModel)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip_Mnu;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        public System.Windows.Forms.ToolStripTextBox txtSNFind;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripButton_FindStation;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton toolStripButton_Save;
        private System.Windows.Forms.ToolStripButton toolStripButton_Cancel;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButton_Exit;
        private System.Windows.Forms.ToolStripLabel toolStripLabel_Dateime;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.TextBox txtRemark;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtSNc;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbxFBAIDc;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cbxFCATIDc;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cbxFMDIDc;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtSNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbxFBAIDo;
        private System.Windows.Forms.ComboBox cbxFMDIDo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbxFCATIDo;
        private System.Windows.Forms.DataGridView dataGridView_ChgModel;
        private System.Windows.Forms.DataGridViewImageColumn clCancel;
        private System.Windows.Forms.Button btnDelete;
    }
}