﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace SMSMINI.FFA
{
    public partial class Find_changeModel : Form
    {
        public string SNo { get; set; }
        public int FCATIDo { get; set; }
        public int FBAIDo { get; set; }
        public int FMDIDo { get; set; }
        public Find_changeModel()
        {
            InitializeComponent();
        }

        private void Find_changeModel_Load(object sender, EventArgs e)
        {
            if (SNo != null)
            {
                using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
                {

                    var q = (from p in dc.FFixedAsset_SerialNumber_Pools
                             join m in dc.FFixedAsset_Models on p.FMD_ID equals m.FMD_ID
                             join b in dc.FFixedAsset_Brands on m.FBA_ID equals b.FBA_ID
                             //20210419 join c in dc.FFixedAsset_Categories on b.FCAT_ID equals c.FCAT_ID
                             join c in dc.FFixedAsset_Categories on p.FCAT_ID equals c.FCAT_ID //เพื่อเอาข้ออมูลที่ผิดออกมา
                             join s in dc.FFixedAsset_SNStatus on p.StatusUse equals s.FSTUS_ID
                             where p.SerialNumber.Contains(SNo)
                             select new
                             {
                                 SerialNumber = p.SerialNumber,
                                 //20210419 FCAT_ID = c.FCAT_ID,
                                 //20210419 ประเภทอุปกรณ์ = c.FixedAsset_CateName,
                                 FCAT_ID = c.FCAT_ID,
                                 ประเภทอุปกรณ์ = c.FixedAsset_CateName,
                                 FBA_ID = b.FBA_ID,
                                 ยี่ห้อ = b.FBrand,
                                 FMD_ID = m.FMD_ID,
                                 รุ่น = m.FModel,
                                 สถานะ = s.FSTUS_ID + " : " + s.FFAStatus,
                             })
                                   .Distinct()
                                   .OrderBy(t => t.SerialNumber)
                                   .ToList();

                    if (q.Count() > 0)
                    {
                        dataGridView1.DataSource = q;
                    }
                }
            }
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                SNo = dataGridView1["SerialNumber", e.RowIndex].Value.ToString();
                FCATIDo = int.Parse(dataGridView1["FCAT_ID", e.RowIndex].Value.ToString());
                FBAIDo = int.Parse(dataGridView1["FBA_ID", e.RowIndex].Value.ToString());
                FMDIDo = int.Parse(dataGridView1["FMD_ID", e.RowIndex].Value.ToString());

                DialogResult = DialogResult.OK;
            }
            catch (Exception)
            {
                DialogResult = DialogResult.Cancel;
            }
        }
    }
}
