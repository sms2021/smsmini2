﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace SMSMINI.FFA
{
    public partial class Find_SNPool : Form
    {
        public string SN { get; set; }
        public int FCATID { get; set; }
        public int FMDID { get; set; }
        //20140430
        public int FBAID { get; set; }

        //20170202
        public string STA_ID { get; set; }

        //20200716 
        public string InvDate { get; set; }


        public Find_SNPool()
        {
            InitializeComponent();
        }

        private void Find_SNPool_Load(object sender, EventArgs e)
        {
            if (SN != null)
            {
                using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
                {
                    /* //20140430
                    var q = (from t in dc.FFixedAsset_SerialNumber_Pools
                             from c in dc.FFixedAsset_Categories
                             from b in dc.FFixedAsset_Brands
                             from m in dc.FFixedAsset_Models

                             where  (t.FMD_ID == m.FMD_ID && t.FCAT_ID == c.FCAT_ID) &&
                                    (m.FBA_ID== b.FBA_ID ) &&
                                    (t.FCAT_ID == FCATID && t.FMD_ID == FMDID) &&
                                    (
                                        t.SerialNumber.Contains(SN) && 
                                        new string []{"00","66","76"}.Contains (t.StatusUse)
                                    ) 
                     */
                    var q = (from t in dc.FFixedAsset_SerialNumber_Pools
                             from c in dc.FFixedAsset_Categories
                             from b in dc.FFixedAsset_Brands
                             from m in dc.FFixedAsset_Models
                                 //20180903
                             from s in dc.FFixedAsset_SNStatus
                                 //==
                             where (t.FMD_ID == m.FMD_ID && t.FCAT_ID == c.FCAT_ID) &&
                                    (m.FBA_ID == b.FBA_ID) &&
                                    (t.FCAT_ID == FCATID && t.FMD_ID == FMDID && m.FBA_ID == FBAID) &&
                                    //20180903
                                    (t.StatusUse == s.FSTUS_ID) &&
                                    //==
                                    (
                                        t.SerialNumber.Contains(SN) &&
                                        new string[] { "00", "66", "76" }.Contains(t.StatusUse)
                                    )
                             /* 00=S/N พร้อมใช้
                                66=S/N ทำ PDI
                                76=S/N เข้าคลังแล้ว
                              */
                             select new
                             {
                                 SerialNumber = t.SerialNumber,
                                 FCAT_ID = t.FCAT_ID,
                                 ประเภทอุปกรณ์ = c.FixedAsset_CateName,
                                 ยี่ห้อ = b.FBrand,
                                 FMD_ID = t.FMD_ID,
                                 รุ่น = m.FModel,
                                 //20180903 สถานะ = t.StatusUse 
                                 สถานะ = t.StatusUse + " : " + s.FFAStatus,

                                 รายละเอียดอื่นๆ = t.Detail
                                 ,
                                 InvoiceDate = t.INVDate//20200716

                             })
                                    .Distinct()
                                    .OrderBy(t => t.SerialNumber)
                                    .ToList();

                    if (q.Count() > 0)
                    {
                        dataGridView1.DataSource = q;
                    }
                    else
                    {
                        //20160831 MessageBox.Show("ไม่พบข้อมูล Serial Number: [" + SN + "] ตามเงื่อนไขอุปกรณ์ ที่ระบุ", "ผลการค้นหา", MessageBoxButtons.OK, MessageBoxIcon.Information);


                        //20141107
                        //DialogResult = DialogResult.Cancel;

                        /*  //20180831 //ก่อนลบออกจากสถานีเดิมอัตโนมัติ
                        DialogResult = DialogResult.Ignore;
                        this.Close();
                        */


                        //20140430 statusUse ='11'

                        var qDetail = dc.FFixedAsset_Station_Details.Where(t => t.SerialNumber == SN).FirstOrDefault();

                        if (qDetail != null)
                        {

                            /* //20180831 //ก่อนลบออกจากสถานีเดิมอัตโนมัติ
                            var qStation = dc.Stations.Where(t => t.STA_ID == qDetail.STA_ID).FirstOrDefault();

                            MessageBox.Show("Serial Number: [" + SN + "] ถูกใช้งานแล้ว ที่สถานี  " + qStation.STA_ID + " : " + qStation.StationSys + " ", "ผลการค้นหา", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            DialogResult = DialogResult.No;
                            this.Close();
                             */
                            var qStation = dc.Stations.Where(t => t.STA_ID == qDetail.STA_ID).FirstOrDefault();

                            if (STA_ID != qDetail.STA_ID)//20190507
                            {
                                MessageBox.Show("Serial Number: [" + SN + "] ถูกใช้งานแล้ว" +
                                //20210525 " ที่สถานี  " + qStation.STA_ID + " : " + qStation.StationSys + " "+
                                //==20210525
                                 " " + Environment.NewLine +
                                 " ที่สถานี : " + qStation.STA_ID + " : " + qStation.StationSys + " " + Environment.NewLine +
                                 " ที่อยู่ : " + qStation.Address + Environment.NewLine +
                                 //==
                                 " ", "ผลการค้นหา", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }//20190507



                            var q11 = (from t in dc.FFixedAsset_SerialNumber_Pools
                                       from c in dc.FFixedAsset_Categories
                                       from b in dc.FFixedAsset_Brands
                                       from m in dc.FFixedAsset_Models
                                           //20180903
                                       from s in dc.FFixedAsset_SNStatus
                                           //==
                                       where (t.FMD_ID == m.FMD_ID && t.FCAT_ID == c.FCAT_ID) &&
                                              (m.FBA_ID == b.FBA_ID) &&
                                              (t.FCAT_ID == FCATID && t.FMD_ID == FMDID && m.FBA_ID == FBAID) &&
                                              //20180903
                                              (t.StatusUse == s.FSTUS_ID) &&
                                              //==
                                              (
                                                  t.SerialNumber.Contains(SN) &&
                                                  new string[] { "11" }.Contains(t.StatusUse)
                                              )
                                       /* 00=S/N พร้อมใช้
                                          66=S/N ทำ PDI
                                          76=S/N เข้าคลังแล้ว
                                        */
                                       select new
                                       {
                                           SerialNumber = t.SerialNumber,
                                           FCAT_ID = t.FCAT_ID,
                                           ประเภทอุปกรณ์ = c.FixedAsset_CateName,
                                           ยี่ห้อ = b.FBrand,
                                           FMD_ID = t.FMD_ID,
                                           รุ่น = m.FModel,
                                           //20180903 สถานะ = t.StatusUse 
                                           สถานะ = t.StatusUse + " : " + s.FFAStatus,

                                           รายละเอียดอื่นๆ = t.Detail
                                           ,
                                           InvoiceDate = t.INVDate//20200716

                                       })
                                .Distinct()
                                .OrderBy(t => t.SerialNumber)
                                .ToList();

                            if (q11.Count() > 0)
                            {

                                dataGridView1.DataSource = q11;

                            }
                            //20190507
                            else
                            {
                                var qAlertDup11 = (from t in dc.FFixedAsset_SerialNumber_Pools
                                                   from c in dc.FFixedAsset_Categories
                                                   from b in dc.FFixedAsset_Brands
                                                   from m in dc.FFixedAsset_Models

                                                   where (t.FMD_ID == m.FMD_ID && t.FCAT_ID == c.FCAT_ID) &&
                                                          (m.FBA_ID == b.FBA_ID) &&
                                                          //(t.FCAT_ID == FCATID && t.FMD_ID == FMDID && m.FBA_ID == FBAID) &&
                                                          (t.SerialNumber == SN)

                                                   select new
                                                   {
                                                       SerialNumber = t.SerialNumber,
                                                       FCAT_NAME = c.FixedAsset_CateName,
                                                       FBrand = b.FBrand,
                                                       FModel = m.FModel,
                                                       //2016831
                                                       StatusUse = t.StatusUse

                                                   }).Distinct().FirstOrDefault();
                                if (qAlertDup11.StatusUse == "11")
                                {
                                    MessageBox.Show("Serial Number: [" + SN + "] มีข้อมูลแล้ว \n ในอุปกรณ์ :  " + qAlertDup11.FCAT_NAME + "  ยี่ห้อ : " + qAlertDup11.FBrand + " รุ่น : " + qAlertDup11.FModel + " " + Environment.NewLine +
                                        "สถานะ : บันทึกลงสถานีแล้ว ", "ผลการค้นหา", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    DialogResult = DialogResult.No;
                                    this.Close();
                                }


                            }



                        }
                        //20140520 
                        else
                        {//11 ปิดjob  77
                            var qDup = dc.FFixedAsset_SerialNumber_Pools.Where(t => t.SerialNumber == SN).FirstOrDefault();

                            if (qDup != null)
                            {

                                var qAlertDup = (from t in dc.FFixedAsset_SerialNumber_Pools
                                                 from c in dc.FFixedAsset_Categories
                                                 from b in dc.FFixedAsset_Brands
                                                 from m in dc.FFixedAsset_Models

                                                 where (t.FMD_ID == m.FMD_ID && t.FCAT_ID == c.FCAT_ID) &&
                                                        (m.FBA_ID == b.FBA_ID) &&
                                                        //(t.FCAT_ID == FCATID && t.FMD_ID == FMDID && m.FBA_ID == FBAID) &&
                                                        (t.SerialNumber == SN)

                                                 select new
                                                 {
                                                     SerialNumber = t.SerialNumber,
                                                     FCAT_NAME = c.FixedAsset_CateName,
                                                     FBrand = b.FBrand,
                                                     FModel = m.FModel,
                                                     //2016831
                                                     StatusUse = t.StatusUse

                                                 }).Distinct().FirstOrDefault();
                                //20190924--เพิ่มแจ้ง มีอยู่แล้ว พร้อมใช้งาน
                                if (qAlertDup.StatusUse == "00")
                                {

                                    MessageBox.Show("Serial Number: [" + SN + "] มีข้อมูลแล้ว \n ในอุปกรณ์ :  " + qAlertDup.FCAT_NAME + "  ยี่ห้อ : " + qAlertDup.FBrand + " รุ่น : " + qAlertDup.FModel + " " + Environment.NewLine +
                                        "สถานะ : พร้อมใช้งาน ", "ผลการค้นหา", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    DialogResult = DialogResult.No;
                                    this.Close();

                                }
                                //======
                                /*
                                //20160831
                                MessageBox.Show("Serial Number: [" + SN + "] มีข้อมูลแล้ว \n ในอุปกรณ์ :  " + qAlertDup.FCAT_NAME + "  ยี่ห้อ : " + qAlertDup.FBrand + " รุ่น : " + qAlertDup.FModel + "  ", "ผลการค้นหา", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                DialogResult = DialogResult.No;
                                this.Close();
                                */
                                //----=== //20160831=====
                                if (qAlertDup.StatusUse == "77")
                                {

                                    MessageBox.Show("Serial Number: [" + SN + "] มีข้อมูลแล้ว \n ในอุปกรณ์ :  " + qAlertDup.FCAT_NAME + "  ยี่ห้อ : " + qAlertDup.FBrand + " รุ่น : " + qAlertDup.FModel + " " + Environment.NewLine +
                                        "สถานะ : S/N รอรับเข้าคลัง ", "ผลการค้นหา", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    DialogResult = DialogResult.No;
                                    this.Close();

                                }
                                if (qAlertDup.StatusUse == "76")
                                {

                                    MessageBox.Show("Serial Number: [" + SN + "] มีข้อมูลแล้ว \n ในอุปกรณ์ :  " + qAlertDup.FCAT_NAME + "  ยี่ห้อ : " + qAlertDup.FBrand + " รุ่น : " + qAlertDup.FModel + " " + Environment.NewLine +
                                        "สถานะ : S/N รับเข้าคลังแล้ว ", "ผลการค้นหา", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    DialogResult = DialogResult.No;
                                    this.Close();

                                }
                                if (qAlertDup.StatusUse == "66")
                                {

                                    MessageBox.Show("Serial Number: [" + SN + "] มีข้อมูลแล้ว \n ในอุปกรณ์ :  " + qAlertDup.FCAT_NAME + "  ยี่ห้อ : " + qAlertDup.FBrand + " รุ่น : " + qAlertDup.FModel + " " + Environment.NewLine +
                                        "สถานะ : S/N ทำ PDI แล้ว ", "ผลการค้นหา", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    DialogResult = DialogResult.No;
                                    this.Close();

                                }
                                else if (qAlertDup.StatusUse == "11")
                                {
                                    var qAlertCloseJOB = (from jfd in dc.JOB_Failure_Details
                                                          from j in dc.JOBs
                                                          from s in dc.Stations
                                                          from t in dc.FFixedAsset_SerialNumber_Pools
                                                          from c in dc.FFixedAsset_Categories
                                                          from b in dc.FFixedAsset_Brands
                                                          from m in dc.FFixedAsset_Models

                                                          where (t.FMD_ID == m.FMD_ID && t.FCAT_ID == c.FCAT_ID) &&
                                                                   (m.FBA_ID == b.FBA_ID) && (t.SerialNumber == jfd.SerialNumber) &&
                                                                   (jfd.JOB_ID == j.JOB_ID && j.STA_ID == s.STA_ID) &&
                                                                   //20170116 (jfd.SerialNumber == SN)
                                                                   ((jfd.SerialNumber_New == SN) || (jfd.SerialNumber == SN))

                                                          select new
                                                          {
                                                              SerialNumber = jfd.SerialNumber,
                                                              FCAT_NAME = c.FixedAsset_CateName,
                                                              FBrand = b.FBrand,
                                                              FModel = m.FModel,
                                                              StatusUse = t.StatusUse,
                                                              Station = s.STA_ID + " " + s.StationSys,
                                                              Trandate = jfd.trandate

                                                              //20170116}).OrderByDescending(t =>t.Trandate).Distinct().First();//.Distinct().First();
                                                          }).OrderByDescending(t => t.Trandate).Distinct().FirstOrDefault();

                                    if (qAlertCloseJOB != null)
                                    {
                                        MessageBox.Show("Serial Number: [" + SN + "] มีข้อมูลแล้ว \n ในอุปกรณ์ :  " + qAlertCloseJOB.FCAT_NAME + "  ยี่ห้อ : " + qAlertCloseJOB.FBrand + " รุ่น : " + qAlertCloseJOB.FModel + " " + Environment.NewLine +
                                            "สถานะ : ถูกนำไปปิดงาน ก่อนบันทึกลงสถานี " + Environment.NewLine +
                                            "สถานี : " + qAlertCloseJOB.Station + "  ", "ผลการค้นหา", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    }
                                    //20170626 ====  
                                    //else { 


                                    //}

                                    DialogResult = DialogResult.No;
                                    this.Close();
                                }
                                else if (qAlertDup.StatusUse == "99")//20190507
                                {
                                    MessageBox.Show("Serial Number: [" + SN + "] มีข้อมูลแล้ว \n ในอุปกรณ์ :  " + qAlertDup.FCAT_NAME + "  ยี่ห้อ : " + qAlertDup.FBrand + " รุ่น : " + qAlertDup.FModel + " " + Environment.NewLine +
                                            "สถานะ : ถูกยกเลิกการใช้งาน ", "ผลการค้นหา", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    DialogResult = DialogResult.No;
                                    this.Close();
                                }

                                //----========


                            }//มีในpool
                            //20180831 //ก่อนลบออกจากสถานีเดิมอัตโนมัติ
                            else
                            {//ไม่มีในpool


                                DialogResult = DialogResult.Ignore;
                                this.Close();
                            }
                            //=======================
                        }//ยังไม่ระบุสถานี 11 ปิดjob  77
                    }//else ไม่มีใน pool 00 66 76

                } //dal

            }// if (SN != null)

        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                //20170202
                //if (e.RowIndex != -1)
                //{
                //    SN = dataGridView1["SerialNumber", e.RowIndex].Value.ToString();
                //    FCATID = int.Parse(dataGridView1["FCAT_ID", e.RowIndex].Value.ToString());
                //    FMDID = int.Parse(dataGridView1["FMD_ID", e.RowIndex].Value.ToString()); 
                //}
                using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
                {
                    var chkBookSite = dc.vw_Find_FFAControls.Where(b => b.SerialNumber == dataGridView1["SerialNumber", e.RowIndex].Value.ToString()

                        ).FirstOrDefault();

                    if (chkBookSite != null)
                    {

                        if (chkBookSite.STA_ID != null)
                        {
                            //if (chkBookSite.STA_ID == STA_ID ||  string.IsNullOrEmpty(chkBookSite.STA_ID) )
                            if (chkBookSite.STA_ID == STA_ID || chkBookSite.STA_ID == "00000000")
                            {
                                SN = dataGridView1["SerialNumber", e.RowIndex].Value.ToString();
                                FCATID = int.Parse(dataGridView1["FCAT_ID", e.RowIndex].Value.ToString());
                                FMDID = int.Parse(dataGridView1["FMD_ID", e.RowIndex].Value.ToString());
                                //InvDate = DateTime.Parse(dataGridView1["InvoiceDate", e.RowIndex].Value.ToString());//20200716
                                InvDate = dataGridView1["InvoiceDate", e.RowIndex].Value == null ? "" : dataGridView1["InvoiceDate", e.RowIndex].Value.ToString();//20200716

                                //20160214
                                DialogResult = DialogResult.OK;
                            }
                            // if (chkBookSite.STA_ID != STA_ID && !string.IsNullOrEmpty(chkBookSite.STA_ID))
                            if (chkBookSite.STA_ID != STA_ID && chkBookSite.STA_ID != "00000000")
                            {
                                MessageBox.Show("Serial Number: [" + SN + "]    นี้ มีการจองเพื่อบันทึกลงสถานีแล้ว  " + Environment.NewLine +
                                "ใน สถานี : " + chkBookSite.STA_ID + " " + chkBookSite.StationSys + Environment.NewLine +
                                "บันทึกโดย : " + chkBookSite.FName + "  ", "ผลการค้นหา", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                //20160214        
                                DialogResult = DialogResult.Cancel;
                            }
                        }
                        else
                        {
                            if (e.RowIndex != -1)
                            {
                                SN = dataGridView1["SerialNumber", e.RowIndex].Value.ToString();
                                FCATID = int.Parse(dataGridView1["FCAT_ID", e.RowIndex].Value.ToString());
                                FMDID = int.Parse(dataGridView1["FMD_ID", e.RowIndex].Value.ToString());
                                //InvDate = DateTime.Parse(dataGridView1["InvoiceDate", e.RowIndex].Value.ToString());//20200716
                                InvDate = dataGridView1["InvoiceDate", e.RowIndex].Value == null ? "" : dataGridView1["InvoiceDate", e.RowIndex].Value.ToString();//20200716

                                //20160214
                                DialogResult = DialogResult.OK;
                            }
                        }
                    }

                    else
                    {
                        if (e.RowIndex != -1)
                        {
                            SN = dataGridView1["SerialNumber", e.RowIndex].Value.ToString();
                            FCATID = int.Parse(dataGridView1["FCAT_ID", e.RowIndex].Value.ToString());
                            FMDID = int.Parse(dataGridView1["FMD_ID", e.RowIndex].Value.ToString());
                            //InvDate = DateTime.Parse(dataGridView1["InvoiceDate", e.RowIndex].Value.ToString());//20200716
                            InvDate = dataGridView1["InvoiceDate", e.RowIndex].Value == null ? "" : dataGridView1["InvoiceDate", e.RowIndex].Value.ToString();//20200716

                            //20160214
                            DialogResult = DialogResult.OK;
                        }
                    }
                }

                //20160214 DialogResult = DialogResult.OK;
            }
            catch (Exception)
            {

                DialogResult = DialogResult.Cancel;
            }
        }
    }
}
