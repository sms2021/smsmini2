﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace SMSMINI.FFA
{
    public partial class ControlFFA : Form
    {
        List<FFAControl> lsFFAControl = new List<FFAControl>();
        DateTime servDate = DateTime.Now;

        
        public ControlFFA()
        {
            InitializeComponent();
        }
        private void ControlFFA_Load(object sender, EventArgs e)
        {
            CleareSNText();

            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            {
                cbActStatus.DataSource = dc.FFixedAsset_Actions
                            .Where(t => t.IsCancel == false)
                            .Select(t => new
                            {
                                FACT_ID = t.FACT_ID,
                                FACT_Status = t.FACT_Status
                            }).ToList();
                cbActStatus.DisplayMember = "FACT_Status";
                cbActStatus.ValueMember = "FACT_ID";
            }
        }
        private void CleareSNText()
        {
            txtSN.Text = string.Empty;
            cbActStatus.SelectedValue = "00";
            txtRemark.Text = string.Empty;

            lbSN.Text = "SerialNumber";
            lbReserv.Text = "ไม่จอง";
            txtStation.Text = string.Empty;
            txtTranEmp.Text = UserInfo.UserId;

        }



        private void txtSN_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtSN.Text.Length < 3 || string.IsNullOrEmpty(txtSN.Text))
                {
                    MessageBox.Show("กรุณาระบุ รหัส SerialNumber ตั้งแต่ 3 หลักขึ้นไป", "ผลการตรวจสอบ...", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtSN.Focus();
                    return;
                }

                using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
                {
                    var q = (from t in dc.vw_Find_FFAControls
                             where t.SerialNumber.Contains(txtSN.Text.Trim())

                             select new
                             {
                                 SerialNumber = t.SerialNumber,
                                 สถานะ = t.FACT_Status,
                                 หมายเหตุ = t.Remark,
                                 รหัสสถานี = t.STA_ID,
                                 สถานี = t.StationSys,
                                 ผู้บันทึก = t.FName,
                                 วันที่ = (DateTime?)t.TranDate,
                                 ประเภทอุปกรณ์ = t.FixedAsset_CateName,
                                 ยี่ห้อ = t.FBrand,
                                 รุ่น = t.FModel,
                                 สถานะการใช้งาน = t.FFAStatus
                             })
                                 .Distinct()
                                .OrderBy(t => t.SerialNumber)
                                 .ToList();

                    if (q.Count() > 0)
                    {

                        var f = new Find_SNControl();

                        f.SN = txtSN.Text.Trim();
                        var result = f.ShowDialog();

                        if (result == DialogResult.OK)
                        {
                            txtSN.Text = f.SN;
                            lbSN.Text = f.SN;

                            string vfActStatus = f.fActStatus.Substring(0, 2);
                            cbActStatus.SelectedValue = vfActStatus;

                            txtRemark.Text = f.fRemark;
                            lbReserv.Text = f.fReserv;
                            txtStation.Text = f.fStation;
                        }
                        else if (result == DialogResult.Ignore)
                        {
                            txtSN.Focus();
                            lbSN.Text = "SerialNumber";
                        }
                        else if (result == DialogResult.Cancel)
                        {
                            txtSN.Focus();
                            lbSN.Text = "SerialNumber";
                        }
                    }
                    else
                    {
                        var qDetail = dc.FFixedAsset_Station_Details.Where(t => t.SerialNumber == txtSN.Text.Trim()).FirstOrDefault();

                        if (qDetail != null)
                        {
                            var qStation = dc.Stations.Where(t => t.STA_ID == qDetail.STA_ID).FirstOrDefault();

                            MessageBox.Show("Serial Number: [" + txtSN.Text.Trim() + "] ถูกใช้งานแล้ว ที่สถานี  " + qStation.STA_ID + " : " + qStation.StationSys + " ", "ผลการค้นหา", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            //DialogResult = DialogResult.Ignore;
                            //this.Close();
                        }
                        else
                        {//11 แต่ไม่มีใน detail ถูกนำไปปิดjob  
                            var qDup = dc.FFixedAsset_SerialNumber_Pools.Where(t => t.SerialNumber == txtSN.Text.Trim() && t.StatusUse == "11").FirstOrDefault();
                            if (qDup != null)
                            {
                                var qAlertCloseJOB = (from jfd in dc.JOB_Failure_Details
                                                      from j in dc.JOBs
                                                      from s in dc.Stations
                                                      from t in dc.FFixedAsset_SerialNumber_Pools
                                                      from c in dc.FFixedAsset_Categories
                                                      from b in dc.FFixedAsset_Brands
                                                      from m in dc.FFixedAsset_Models

                                                      where (t.FMD_ID == m.FMD_ID && t.FCAT_ID == c.FCAT_ID) &&
                                                               (m.FBA_ID == b.FBA_ID) && (t.SerialNumber == jfd.SerialNumber) &&
                                                               (jfd.JOB_ID == j.JOB_ID && j.STA_ID == s.STA_ID) &&
                                                               ((jfd.SerialNumber_New == txtSN.Text.Trim()) || (jfd.SerialNumber == txtSN.Text.Trim()))

                                                      select new
                                                      {
                                                          SerialNumber = jfd.SerialNumber,
                                                          FCAT_NAME = c.FixedAsset_CateName,
                                                          FBrand = b.FBrand,
                                                          FModel = m.FModel,
                                                          StatusUse = t.StatusUse,
                                                          Station = s.STA_ID + " " + s.StationSys,
                                                          Trandate = jfd.trandate

                                                          //20170116}).OrderByDescending(t =>t.Trandate).Distinct().First();//.Distinct().First();
                                                      }).OrderByDescending(t => t.Trandate).Distinct().FirstOrDefault();

                                if (qAlertCloseJOB != null)
                                {
                                    MessageBox.Show("Serial Number: [" + txtSN.Text.Trim() + "] มีข้อมูลแล้ว \n ในอุปกรณ์ :  " + qAlertCloseJOB.FCAT_NAME + "  ยี่ห้อ : " + qAlertCloseJOB.FBrand + " รุ่น : " + qAlertCloseJOB.FModel + " " + Environment.NewLine +
                                        "สถานะ : ถูกนำไปปิดงาน ก่อนบันทึกลงสถานี " + Environment.NewLine +
                                        "สถานี : " + qAlertCloseJOB.Station + "  ", "ผลการค้นหา", MessageBoxButtons.OK, MessageBoxIcon.Information);

                                    //DialogResult = DialogResult.Ignore;
                                    //this.Close();
                                }
                                else
                                {
                                    MessageBox.Show("Serial Number: [" + txtSN.Text.Trim() + "] ไม่พบข้อมูล  " + Environment.NewLine +
                                           "กรุณาตรวจสอบ ", "ผลการค้นหา", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }

                                //----========
                            }
                        }



                    }
                }//dal
            }
        }

        private void txtStation_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtStation.Text.Length < 3 || string.IsNullOrEmpty(txtStation.Text))
                {
                    MessageBox.Show("กรุณาระบุ รหัสสถานี หรือ ชื่อสถานี ตั้งแต่ 3 หลักขึ้นไป", "ผลการตรวจสอบ...", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtStation.Focus();
                    return;
                }
                var f = new Find_StationControl();

                f.Station = txtStation.Text.Trim();
                var result = f.ShowDialog();
                if (result == DialogResult.OK)
                {
                    txtStation.Text = f.StationSys;
                    lbReserv.Text = f.STA_ID;
                }
                else if (result == DialogResult.Ignore)
                {
                    txtStation.Focus();
                    lbReserv.Text = "ไม่จอง";
                }
                else if (result == DialogResult.Cancel)
                {
                    txtStation.Focus();
                    lbReserv.Text = "ไม่จอง";
                }
            }
        }

        private void btSelect_Click(object sender, EventArgs e)
        {
            if (lbSN.Text == string.Empty || lbSN.Text == "SerialNumber")
            {
                MessageBox.Show(" กรุณาระบุข้อมูล SerialNumber ", "ผลการตรวจสอบ");
                txtSN.Focus();
                txtSN.SelectAll();
                return;
            }
            if (lbReserv.Text == string.Empty || lbReserv.Text == "SerialNumber")
            {
                MessageBox.Show(" กรุณาระบุข้อมูล SerialNumber ", "ผลการตรวจสอบ");
                txtStation.Focus();
                txtStation.SelectAll();
                return;
            }

            //เช็คข้อมูลซ้ำ 
            foreach (var item in lsFFAControl)
            {
                if (txtSN.Text.Trim() == item.SerialNumber)
                {
                    MessageBox.Show("คุณป้อน Serial number [" + txtSN.Text + "] ซ้ำ กรุณาป้อนใหม่", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtSN.Focus();
                    txtSN.SelectAll();
                    return;
                }

            }

            var statusFFA = cbActStatus.SelectedValue.ToString() + ":" + cbActStatus.Text;

            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            {
                var loadFFA = (from t in dc.FFixedAsset_SerialNumber_Pools
                               from c in dc.FFixedAsset_Categories
                               from m in dc.FFixedAsset_Models
                               from b in dc.FFixedAsset_Brands
                               orderby t.SerialNumber
                               where t.SerialNumber == lbSN.Text.Trim() &&
                               t.FCAT_ID == c.FCAT_ID &&
                               t.FMD_ID == m.FMD_ID &&
                               m.FBA_ID == b.FBA_ID

                               select new
                               {
                                   //NO = 0,
                                   t.FCAT_ID,
                                   c.FixedAsset_CateName,
                                   m.FMD_ID,
                                   m.FModel,
                                   m.FBA_ID,
                                   b.FBrand,
                                   t.SerialNumber,
                                   t.RegisDate,
                                   t.PODate,
                                   t.INVDate,
                                   t.BLDate,
                                   t.EMP_ID,
                                   t.WADDate,
                                   t.WAD_EMP,
                                   t.SupExpiryDate,
                                   t.PDIDate,
                                   t.PDI_EMP,

                                   t.SealDate,
                                   t.AssureExpDate,
                                   t.RegistMeter
                               }
                                 )
                             .FirstOrDefault();


                lsFFAControl.Add(new FFAControl
                {
                    //_jobType = lblJOBType.Text,
                    //_statID = lblSTA_ID.Text,
                    //_GOP_ID = lblGROUP.Text,
                    //_jobType1 = _jobtype,
                    //_vAN = lblVANID.Text.Trim(),
                    //_jobQTY = Convert.ToInt32(txtJOBQTY.Text),
                    //_autoJOBDetail = txtJOBDetail.Text == string.Empty ? null : txtJOBDetail.Text,
                    //_imformer = txtInformer.Text == string.Empty ? null : txtInformer.Text.Trim(),
                    //_WorkID = txtWorkID.Text == string.Empty ? null : txtWorkID.Text.Trim(),
                    ////--20160527
                    //_ChargeJOBID = lblJOBID.Text == "JOB ID" ? null : lblJOBID.Text

                    //NO = 0,
                    SerialNumber = lbSN.Text.Trim(),
                    สถานะ = statusFFA, //cbActStatus.Text,
                    หมายเหตุ = txtRemark.Text,
                    รหัสสถานี = lbReserv.Text == "ไม่จอง" ? null : lbReserv.Text,
                    สถานี = txtStation.Text,

                    ประเภทอุปกรณ์ = loadFFA.FixedAsset_CateName,
                    ยี่ห้อ = loadFFA.FBrand,
                    รุ่น = loadFFA.FModel,

                    วันที่_วันที่นำเข้าระบบ = loadFFA.RegisDate,
                    วันที่_รับเข้าคลัง = loadFFA.WADDate,
                    วันที่_หมดประกัน = loadFFA.SupExpiryDate,

                    วันที่ทำ_PDI = loadFFA.PDIDate,
                    วันที่ตีตรา = loadFFA.SealDate,
                    วันหมดอายุคำรับรอง = loadFFA.AssureExpDate,
                    เลขทะเบียนตามมิเตอร์ = loadFFA.RegistMeter

                });




                dataGridView1.DataSource = lsFFAControl.ToList();
                setDataGridViewStyle();
                CleareSNText();

                txtSN.Focus();


            }
        }




        private void setDataGridViewStyle()
        {
            for (int r = 0; r <= dataGridView1.RowCount - 1; r++)
            {
                dataGridView1["SerialNumber", r].Style.BackColor = Color.Wheat;
                dataGridView1["SerialNumber", r].ReadOnly = true;
                dataGridView1["สถานะ", r].Style.BackColor = Color.Wheat;
                dataGridView1["สถานะ", r].ReadOnly = true;
                dataGridView1["หมายเหตุ", r].Style.BackColor = Color.Wheat;
                dataGridView1["หมายเหตุ", r].ReadOnly = true;
                dataGridView1["รหัสสถานี", r].Style.BackColor = Color.Wheat;
                dataGridView1["รหัสสถานี", r].ReadOnly = true;
                dataGridView1["สถานี", r].Style.BackColor = Color.Wheat;
                dataGridView1["สถานี", r].ReadOnly = true;
                dataGridView1["ประเภทอุปกรณ์", r].Style.BackColor = Color.Wheat;
                dataGridView1["ประเภทอุปกรณ์", r].ReadOnly = true;
                dataGridView1["ยี่ห้อ", r].Style.BackColor = Color.Wheat;
                dataGridView1["ยี่ห้อ", r].ReadOnly = true;
                dataGridView1["รุ่น", r].Style.BackColor = Color.Wheat;
                dataGridView1["รุ่น", r].ReadOnly = true;
                dataGridView1["วันที่_วันที่นำเข้าระบบ", r].Style.BackColor = Color.Wheat;
                dataGridView1["วันที่_วันที่นำเข้าระบบ", r].ReadOnly = true;
                dataGridView1["วันที่_รับเข้าคลัง", r].Style.BackColor = Color.Wheat;
                dataGridView1["วันที่_รับเข้าคลัง", r].ReadOnly = true;
                dataGridView1["วันที่_หมดประกัน", r].Style.BackColor = Color.Wheat;
                dataGridView1["วันที่_หมดประกัน", r].ReadOnly = true;
                dataGridView1["วันที่ทำ_PDI", r].Style.BackColor = Color.Wheat;
                dataGridView1["วันที่ทำ_PDI", r].ReadOnly = true;
                dataGridView1["วันที่ตีตรา", r].Style.BackColor = Color.Wheat;
                dataGridView1["วันที่ตีตรา", r].ReadOnly = true;
                dataGridView1["วันหมดอายุคำรับรอง", r].Style.BackColor = Color.Wheat;
                dataGridView1["วันหมดอายุคำรับรอง", r].ReadOnly = true;
                dataGridView1["เลขทะเบียนตามมิเตอร์", r].Style.BackColor = Color.Wheat;
                dataGridView1["เลขทะเบียนตามมิเตอร์", r].ReadOnly = true;

            }
        }

        public class FFAControl
        {
            //public int NO { get; set; }
            public string SerialNumber { get; set; }
            public string สถานะ { get; set; }
            public string หมายเหตุ { get; set; }
            public string รหัสสถานี { get; set; }
            public string สถานี { get; set; }

            public string ประเภทอุปกรณ์ { get; set; }
            public string ยี่ห้อ { get; set; }
            public string รุ่น { get; set; }

            public DateTime? วันที่_วันที่นำเข้าระบบ { get; set; }
            public DateTime? วันที่_รับเข้าคลัง { get; set; }
            //public string ผู้บันทึก_รับเข้าคลัง { get; set; }
            public DateTime? วันที่_หมดประกัน { get; set; }

            public DateTime? วันที่ทำ_PDI { get; set; }
            //public string ผู้บันทึก_PDI { get; set; }
            public DateTime? วันที่ตีตรา { get; set; }
            public DateTime? วันหมดอายุคำรับรอง { get; set; }
            public string เลขทะเบียนตามมิเตอร์ { get; set; }

        }

        private void exitToolStripButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void saveToolStripButton_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("คุณต้องการ บันทึกข้อมูล Fixed Asset ใช่หรือไม่???...", "คำยืนยัน",
               MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                servDate = getServerDate();
                using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
                {
                    //dc.Connection.Open();

                    dc.Connection.Open();
                    dc.Transaction = dc.Connection.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted);

                    try
                    {
                        foreach (var item1 in lsFFAControl)
                        {
                            var vStatus1 = item1.สถานะ.ToString().Substring(0, 2);
                            var newFFaControl = new DAL.SMSManage.FFixedAsset_ControlTran
                            {
                                SerialNumber = item1.SerialNumber,
                                FACT_ID = vStatus1,
                                Remark = item1.หมายเหตุ,
                                STA_ID = item1.รหัสสถานี == null ? "00000000" : item1.รหัสสถานี,
                                EmpTran = UserInfo.UserId,
                                TranDate = servDate
                            };

                            dc.FFixedAsset_ControlTrans.InsertOnSubmit(newFFaControl);
                            dc.SubmitChanges();
                        }

                        dc.Transaction.Commit();

                        MessageBox.Show("บันทึกข้อมูล Fixed Asset เรียบร้อย ", "ผลการทำงาน",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        //closeProgress();
                        dc.Transaction.Rollback();

                        MessageBox.Show("ไม่สามารถ บันทึกข้อมูล Fixed Asset  กรุณาตรวจสอบ" + Environment.NewLine +
                          "1.สัญญาณ อินเตอร์เนต ปกติ หรือไม่?..." + Environment.NewLine +
                          "2.เข้าเว็บไซต์ JOB Monitor ได้ปกติ หรือไม่?..." + Environment.NewLine +
                          "Error: " + ex.Message, "ผลการบันทึกข้อมูล",
                           MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }//using


            }



        }
        private DateTime getServerDate()
        {
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            {
                var srv = dc
                     .ExecuteQuery<DateTime>("SELECT GETDATE()")
                     .First();
                if (srv != null)
                    return srv;
                else
                    return DateTime.Now;

            }
        }
        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1 && (e.ColumnIndex == 0))
            {
                if (MessageBox.Show("คุณต้องการ ยกเลิกรายการ ใช่หรือไม่???...", "คำยืนยัน",
                 MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    var vSN = dataGridView1["SerialNumber", e.RowIndex].Value.ToString();
                    //binding cancel
                    txtSN.Text = dataGridView1["SerialNumber", e.RowIndex].Value.ToString();
                    lbSN.Text = dataGridView1["SerialNumber", e.RowIndex].Value.ToString();
                    var vStatus = dataGridView1["สถานะ", e.RowIndex].Value.ToString().Substring(0, 2);
                    cbActStatus.SelectedValue = vStatus;
                    txtRemark.Text = dataGridView1["หมายเหตุ", e.RowIndex].Value.ToString();
                    //20170203 lbReserv.Text = dataGridView1["รหัสสถานี", e.RowIndex].Value.ToString() == "ไม่จอง" ? "ไม่จอง" : dataGridView1["รหัสสถานี", e.RowIndex].Value.ToString();
                    lbReserv.Text = dataGridView1["รหัสสถานี", e.RowIndex].Value == null ? "ไม่จอง" : dataGridView1["รหัสสถานี", e.RowIndex].Value.ToString();
                    txtStation.Text = dataGridView1["สถานี", e.RowIndex].Value.ToString();
                    //if(string.IsNullOrEmpty(txtStation.Text = dataGridView1["สถานี", e.RowIndex].Value.ToString()))
                    //{

                    //}else{
                    //}
                    txtTranEmp.Text = UserInfo.UserId;

                    var qcancel = lsFFAControl.Where(t => t.SerialNumber == vSN).FirstOrDefault();
                    if (qcancel != null)
                    {
                        //qcancel.สถานะ
                        lsFFAControl.Remove(qcancel);
                        dataGridView1.DataSource = lsFFAControl.ToList();
                    }
                }
            }
        }

        private void btCanReserv_Click(object sender, EventArgs e)
        {
            lbReserv.Text = "ไม่จอง";
            txtStation.Text = string.Empty;
        }

    }
}
