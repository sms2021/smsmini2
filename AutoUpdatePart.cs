﻿using System;

namespace SMSMINI
{
    public class AutoUpdatePart
    {
        public DateTime TrDateTime { get; set; }
        public string TrTableName { get; set; }
        public string TrStatus { get; set; }
        public string SPA_ID { get; set; }
        public string PRI_ID { get; set; }
        public string GOP_ID { get; set; }
        public DateTime? StartDate { get; set; }

    }



    public class AutoSparePartTranType
    {

        public string TRA_ID { get; set; }
        public string TransferName { get; set; }

    }


    public class AutoPart
    {
        public string SPA_ID { get; set; }
        public string SUP_ID { get; set; }
        public string Styp_id { get; set; }
        public string BRA_ID { get; set; }
        public string SparePart { get; set; }
        public decimal? PricePerUnit { get; set; }
        public decimal? Cost { get; set; }
        public DateTime? WartDate { get; set; }
        public int? InStock { get; set; }
        public int? Minimum { get; set; }
        public string UnitID { get; set; }
        public string Description { get; set; }
        public string TranType { get; set; }
        public string WH { get; set; }
        public System.Data.Linq.Binary PartPhoto { get; set; }
        public bool? IsDiscount { get; set; }
        public bool? IsGlobalDiscount { get; set; }
        public bool? IsDelete { get; set; }
    }


    public class Del_PartMasterData
    {

        public DateTime? TrDateTime { get; set; }
        public string TrTableName { get; set; }
        public string TrStatus { get; set; }
        public string SPA_ID { get; set; }
        public int? PRI_ID { get; set; }
        public string GOP_ID { get; set; }
        //public DateTime? StartDate { get; set; }
        //public string VAN_ID { get; set; }
    }



}
