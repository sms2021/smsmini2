﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("SMS MINI21")]
[assembly: AssemblyDescription("Services Management Systems mini : SMS mini\r\nDev. Nuchit Atjanawat\r\nMicrosoft MVP\r\nBlog: http://janawat.spaces.live.com\r\nDate: 2009-01-04\r\n\r\nFLOWCO-ICT\r\nFlowco.Co.,Ltd.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Flowco Co.,Ltd.")]
[assembly: AssemblyProduct("SMS MINI21")]
[assembly: AssemblyCopyright("Copyright ©  2021")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("215d3a84-a615-4af8-b443-4435e8b58cd2")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("3.1.7.2021")]
[assembly: AssemblyFileVersion("3.1.7.2021")]
