﻿namespace SMSMINI
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripBt_Close = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_SMSAbout = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_LogOff = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_SMSWeb = new System.Windows.Forms.ToolStripDropDownButton();
            this.pricesListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.รายงานTAGPrintToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uploadMonitoringToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.networkConnectionTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.sMSWebToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.พมพรายงานFailureCodeWIWPonlineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripSeparator();
            this.พมพJOBPhotoReportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.พมพใบปะหนาSOToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem14 = new System.Windows.Forms.ToolStripSeparator();
            this.รจกผพฒนาToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripDropDownButton_MasterData = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripMenuItem12 = new System.Windows.Forms.ToolStripSeparator();
            this.บนทกขอมลFixedAssetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ยนยนขอมลFixedAssetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem11 = new System.Windows.Forms.ToolStripSeparator();
            this.aaaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ddddToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fffToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ขอมลผใชงานVANToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ขอมลToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ขอมลFixedAssetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripSeparator();
            this.ขอมลจดเสยToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ขอมลTranTypeToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.updatePriceListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateเงอนไขสญญาToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vvvvToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem9 = new System.Windows.Forms.ToolStripSeparator();
            this.เปลยนPasswordToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.fAMToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.เบกอะใหลToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripSeparator();
            this.downloadฐานขอมลToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.อปเดตฐานขอมลToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripSeparator();
            this.updateSQLScriptToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.เปดJOBToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripSeparator();
            this.uploadJOBPhotoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uploadJOBPhotoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem13 = new System.Windows.Forms.ToolStripSeparator();
            this.SurveyDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.EditJOBOnlineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripButton_CloseJOB = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btSMSweb = new System.Windows.Forms.Button();
            this.btSTART_STOP_Mileage = new System.Windows.Forms.Button();
            this.btSMSSurvey = new System.Windows.Forms.Button();
            this.btCloseJOB = new System.Windows.Forms.Button();
            this.btFFA_Save = new System.Windows.Forms.Button();
            this.btMileageStatement = new System.Windows.Forms.Button();
            this.btFFA_Confirm = new System.Windows.Forms.Button();
            this.btReUploadPhoto = new System.Windows.Forms.Button();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator8,
            this.toolStripBt_Close,
            this.toolStripButton_SMSAbout,
            this.toolStripSeparator10,
            this.toolStripButton_LogOff,
            this.toolStripSeparator11,
            this.toolStripButton_SMSWeb,
            this.toolStripDropDownButton_MasterData,
            this.toolStripButton_CloseJOB,
            this.toolStripSeparator7});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.toolStrip1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.toolStrip1.Size = new System.Drawing.Size(885, 54);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 54);
            // 
            // toolStripBt_Close
            // 
            this.toolStripBt_Close.Image = ((System.Drawing.Image)(resources.GetObject("toolStripBt_Close.Image")));
            this.toolStripBt_Close.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripBt_Close.Name = "toolStripBt_Close";
            this.toolStripBt_Close.Size = new System.Drawing.Size(40, 51);
            this.toolStripBt_Close.Text = "Close";
            this.toolStripBt_Close.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripBt_Close.Click += new System.EventHandler(this.toolStripBt_Close_Click);
            // 
            // toolStripButton_SMSAbout
            // 
            this.toolStripButton_SMSAbout.Image = global::SMSMINI.Properties.Resources.about;
            this.toolStripButton_SMSAbout.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_SMSAbout.Name = "toolStripButton_SMSAbout";
            this.toolStripButton_SMSAbout.Size = new System.Drawing.Size(68, 51);
            this.toolStripButton_SMSAbout.Text = "    About    ";
            this.toolStripButton_SMSAbout.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton_SMSAbout.Click += new System.EventHandler(this.toolStripButton_About_Click);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(6, 54);
            // 
            // toolStripButton_LogOff
            // 
            this.toolStripButton_LogOff.Image = global::SMSMINI.Properties.Resources.se;
            this.toolStripButton_LogOff.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_LogOff.Name = "toolStripButton_LogOff";
            this.toolStripButton_LogOff.Size = new System.Drawing.Size(63, 51);
            this.toolStripButton_LogOff.Text = "  Log Off  ";
            this.toolStripButton_LogOff.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton_LogOff.Click += new System.EventHandler(this.toolStripBt_LogOff_Click);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(6, 54);
            // 
            // toolStripButton_SMSWeb
            // 
            this.toolStripButton_SMSWeb.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pricesListToolStripMenuItem,
            this.รายงานTAGPrintToolStripMenuItem,
            this.uploadMonitoringToolStripMenuItem,
            this.toolStripMenuItem3,
            this.networkConnectionTestToolStripMenuItem,
            this.toolStripMenuItem1,
            this.sMSWebToolStripMenuItem,
            this.พมพรายงานFailureCodeWIWPonlineToolStripMenuItem,
            this.toolStripMenuItem5,
            this.พมพJOBPhotoReportsToolStripMenuItem,
            this.พมพใบปะหนาSOToolStripMenuItem,
            this.toolStripMenuItem14,
            this.รจกผพฒนาToolStripMenuItem});
            this.toolStripButton_SMSWeb.Image = global::SMSMINI.Properties.Resources.print_f2;
            this.toolStripButton_SMSWeb.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_SMSWeb.Name = "toolStripButton_SMSWeb";
            this.toolStripButton_SMSWeb.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.toolStripButton_SMSWeb.Size = new System.Drawing.Size(60, 51);
            this.toolStripButton_SMSWeb.Text = "Reports";
            this.toolStripButton_SMSWeb.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // pricesListToolStripMenuItem
            // 
            this.pricesListToolStripMenuItem.Name = "pricesListToolStripMenuItem";
            this.pricesListToolStripMenuItem.Size = new System.Drawing.Size(272, 22);
            this.pricesListToolStripMenuItem.Text = "รายงาน Prices list";
            this.pricesListToolStripMenuItem.Click += new System.EventHandler(this.pricesListToolStripMenuItem_Click);
            // 
            // รายงานTAGPrintToolStripMenuItem
            // 
            this.รายงานTAGPrintToolStripMenuItem.Name = "รายงานTAGPrintToolStripMenuItem";
            this.รายงานTAGPrintToolStripMenuItem.Size = new System.Drawing.Size(272, 22);
            this.รายงานTAGPrintToolStripMenuItem.Text = "รายงาน TAG Print";
            this.รายงานTAGPrintToolStripMenuItem.Click += new System.EventHandler(this.รายงานTAGPrintToolStripMenuItem_Click);
            // 
            // uploadMonitoringToolStripMenuItem
            // 
            this.uploadMonitoringToolStripMenuItem.Name = "uploadMonitoringToolStripMenuItem";
            this.uploadMonitoringToolStripMenuItem.Size = new System.Drawing.Size(272, 22);
            this.uploadMonitoringToolStripMenuItem.Text = "รายงาน Upload Monitoring";
            this.uploadMonitoringToolStripMenuItem.Visible = false;
            this.uploadMonitoringToolStripMenuItem.Click += new System.EventHandler(this.uploadMonitoringToolStripMenuItem_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(269, 6);
            // 
            // networkConnectionTestToolStripMenuItem
            // 
            this.networkConnectionTestToolStripMenuItem.Name = "networkConnectionTestToolStripMenuItem";
            this.networkConnectionTestToolStripMenuItem.Size = new System.Drawing.Size(272, 22);
            this.networkConnectionTestToolStripMenuItem.Text = "Network connection test";
            this.networkConnectionTestToolStripMenuItem.Visible = false;
            this.networkConnectionTestToolStripMenuItem.Click += new System.EventHandler(this.networkConnectionTestToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(269, 6);
            // 
            // sMSWebToolStripMenuItem
            // 
            this.sMSWebToolStripMenuItem.Name = "sMSWebToolStripMenuItem";
            this.sMSWebToolStripMenuItem.Size = new System.Drawing.Size(272, 22);
            this.sMSWebToolStripMenuItem.Text = "SMS Web (Online)";
            this.sMSWebToolStripMenuItem.Click += new System.EventHandler(this.sMSWebToolStripMenuItem_Click);
            // 
            // พมพรายงานFailureCodeWIWPonlineToolStripMenuItem
            // 
            this.พมพรายงานFailureCodeWIWPonlineToolStripMenuItem.Name = "พมพรายงานFailureCodeWIWPonlineToolStripMenuItem";
            this.พมพรายงานFailureCodeWIWPonlineToolStripMenuItem.Size = new System.Drawing.Size(272, 22);
            this.พมพรายงานFailureCodeWIWPonlineToolStripMenuItem.Text = "พิมพ์รายงาน FailureCodeWIWP (online)";
            this.พมพรายงานFailureCodeWIWPonlineToolStripMenuItem.Click += new System.EventHandler(this.พมพรายงานFailureCodeWIWPonlineToolStripMenuItem_Click);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(269, 6);
            // 
            // พมพJOBPhotoReportsToolStripMenuItem
            // 
            this.พมพJOBPhotoReportsToolStripMenuItem.Name = "พมพJOBPhotoReportsToolStripMenuItem";
            this.พมพJOBPhotoReportsToolStripMenuItem.Size = new System.Drawing.Size(272, 22);
            this.พมพJOBPhotoReportsToolStripMenuItem.Text = "พิมพ์ JOB Photo Reports";
            this.พมพJOBPhotoReportsToolStripMenuItem.Click += new System.EventHandler(this.พมพJOBPhotoReportsToolStripMenuItem_Click);
            // 
            // พมพใบปะหนาSOToolStripMenuItem
            // 
            this.พมพใบปะหนาSOToolStripMenuItem.Enabled = false;
            this.พมพใบปะหนาSOToolStripMenuItem.Name = "พมพใบปะหนาSOToolStripMenuItem";
            this.พมพใบปะหนาSOToolStripMenuItem.Size = new System.Drawing.Size(272, 22);
            this.พมพใบปะหนาSOToolStripMenuItem.Text = "พิมพ์ใบปะหน้า SO";
            this.พมพใบปะหนาSOToolStripMenuItem.Visible = false;
            this.พมพใบปะหนาSOToolStripMenuItem.Click += new System.EventHandler(this.พมพใบปะหนาSOToolStripMenuItem_Click);
            // 
            // toolStripMenuItem14
            // 
            this.toolStripMenuItem14.Name = "toolStripMenuItem14";
            this.toolStripMenuItem14.Size = new System.Drawing.Size(269, 6);
            // 
            // รจกผพฒนาToolStripMenuItem
            // 
            this.รจกผพฒนาToolStripMenuItem.Name = "รจกผพฒนาToolStripMenuItem";
            this.รจกผพฒนาToolStripMenuItem.Size = new System.Drawing.Size(272, 22);
            this.รจกผพฒนาToolStripMenuItem.Text = "รู้จักผู้พัฒนา ระบบ SMS";
            this.รจกผพฒนาToolStripMenuItem.Visible = false;
            this.รจกผพฒนาToolStripMenuItem.Click += new System.EventHandler(this.รจกผพฒนาToolStripMenuItem_Click);
            // 
            // toolStripDropDownButton_MasterData
            // 
            this.toolStripDropDownButton_MasterData.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem12,
            this.บนทกขอมลFixedAssetToolStripMenuItem,
            this.ยนยนขอมลFixedAssetToolStripMenuItem,
            this.toolStripMenuItem11,
            this.aaaToolStripMenuItem,
            this.vvvvToolStripMenuItem,
            this.toolStripMenuItem9,
            this.เปลยนPasswordToolStripMenuItem1,
            this.fAMToolStripMenuItem,
            this.เบกอะใหลToolStripMenuItem,
            this.toolStripMenuItem6,
            this.downloadฐานขอมลToolStripMenuItem,
            this.อปเดตฐานขอมลToolStripMenuItem,
            this.toolStripMenuItem7,
            this.updateSQLScriptToolStripMenuItem,
            this.เปดJOBToolStripMenuItem,
            this.toolStripMenuItem8,
            this.uploadJOBPhotoToolStripMenuItem,
            this.uploadJOBPhotoToolStripMenuItem1,
            this.toolStripMenuItem13,
            this.SurveyDataToolStripMenuItem,
            this.EditJOBOnlineToolStripMenuItem});
            this.toolStripDropDownButton_MasterData.Image = global::SMSMINI.Properties.Resources.Add;
            this.toolStripDropDownButton_MasterData.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton_MasterData.Name = "toolStripDropDownButton_MasterData";
            this.toolStripDropDownButton_MasterData.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.toolStripDropDownButton_MasterData.Size = new System.Drawing.Size(82, 51);
            this.toolStripDropDownButton_MasterData.Text = "ปรับปรุงข้อมูล";
            this.toolStripDropDownButton_MasterData.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolStripDropDownButton_MasterData.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // toolStripMenuItem12
            // 
            this.toolStripMenuItem12.Name = "toolStripMenuItem12";
            this.toolStripMenuItem12.Size = new System.Drawing.Size(286, 6);
            // 
            // บนทกขอมลFixedAssetToolStripMenuItem
            // 
            this.บนทกขอมลFixedAssetToolStripMenuItem.Name = "บนทกขอมลFixedAssetToolStripMenuItem";
            this.บนทกขอมลFixedAssetToolStripMenuItem.Size = new System.Drawing.Size(289, 22);
            this.บนทกขอมลFixedAssetToolStripMenuItem.Text = "บันทึกข้อมูล  Fixed Asset";
            this.บนทกขอมลFixedAssetToolStripMenuItem.Click += new System.EventHandler(this.บนทกขอมลFixedAssetToolStripMenuItem_Click);
            // 
            // ยนยนขอมลFixedAssetToolStripMenuItem
            // 
            this.ยนยนขอมลFixedAssetToolStripMenuItem.Name = "ยนยนขอมลFixedAssetToolStripMenuItem";
            this.ยนยนขอมลFixedAssetToolStripMenuItem.Size = new System.Drawing.Size(289, 22);
            this.ยนยนขอมลFixedAssetToolStripMenuItem.Text = "ยืนยัน ข้อมูล Fixed Asset";
            this.ยนยนขอมลFixedAssetToolStripMenuItem.Click += new System.EventHandler(this.ยนยนขอมลFixedAssetToolStripMenuItem_Click);
            // 
            // toolStripMenuItem11
            // 
            this.toolStripMenuItem11.Name = "toolStripMenuItem11";
            this.toolStripMenuItem11.Size = new System.Drawing.Size(286, 6);
            // 
            // aaaToolStripMenuItem
            // 
            this.aaaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ddddToolStripMenuItem,
            this.fffToolStripMenuItem,
            this.ขอมลผใชงานVANToolStripMenuItem,
            this.ขอมลToolStripMenuItem,
            this.ขอมลFixedAssetToolStripMenuItem,
            this.toolStripMenuItem10,
            this.ขอมลจดเสยToolStripMenuItem1,
            this.ขอมลTranTypeToolStripMenuItem2,
            this.toolStripMenuItem4,
            this.updatePriceListToolStripMenuItem,
            this.updateเงอนไขสญญาToolStripMenuItem});
            this.aaaToolStripMenuItem.Enabled = false;
            this.aaaToolStripMenuItem.Name = "aaaToolStripMenuItem";
            this.aaaToolStripMenuItem.Size = new System.Drawing.Size(289, 22);
            this.aaaToolStripMenuItem.Text = "ข้อมูลหลัก (Master Data)";
            this.aaaToolStripMenuItem.Visible = false;
            // 
            // ddddToolStripMenuItem
            // 
            this.ddddToolStripMenuItem.Name = "ddddToolStripMenuItem";
            this.ddddToolStripMenuItem.Size = new System.Drawing.Size(276, 22);
            this.ddddToolStripMenuItem.Text = "ข้อมูลอะไหล่ (Parts)";
            this.ddddToolStripMenuItem.Click += new System.EventHandler(this.ddddToolStripMenuItem_Click);
            // 
            // fffToolStripMenuItem
            // 
            this.fffToolStripMenuItem.Name = "fffToolStripMenuItem";
            this.fffToolStripMenuItem.Size = new System.Drawing.Size(276, 22);
            this.fffToolStripMenuItem.Text = "ข้อมูลสถานี (Station)";
            this.fffToolStripMenuItem.Click += new System.EventHandler(this.fffToolStripMenuItem_Click);
            // 
            // ขอมลผใชงานVANToolStripMenuItem
            // 
            this.ขอมลผใชงานVANToolStripMenuItem.Enabled = false;
            this.ขอมลผใชงานVANToolStripMenuItem.Name = "ขอมลผใชงานVANToolStripMenuItem";
            this.ขอมลผใชงานVANToolStripMenuItem.Size = new System.Drawing.Size(276, 22);
            this.ขอมลผใชงานVANToolStripMenuItem.Text = "ข้อมูลผู้ใช้งาน (VAN)";
            this.ขอมลผใชงานVANToolStripMenuItem.Visible = false;
            this.ขอมลผใชงานVANToolStripMenuItem.Click += new System.EventHandler(this.ขอมลผใชงานVANToolStripMenuItem_Click);
            // 
            // ขอมลToolStripMenuItem
            // 
            this.ขอมลToolStripMenuItem.Enabled = false;
            this.ขอมลToolStripMenuItem.Name = "ขอมลToolStripMenuItem";
            this.ขอมลToolStripMenuItem.Size = new System.Drawing.Size(276, 22);
            this.ขอมลToolStripMenuItem.Text = "ข้อมูล Fixed Asset SerialNumber Master";
            this.ขอมลToolStripMenuItem.Visible = false;
            this.ขอมลToolStripMenuItem.Click += new System.EventHandler(this.ขอมลToolStripMenuItem_Click);
            // 
            // ขอมลFixedAssetToolStripMenuItem
            // 
            this.ขอมลFixedAssetToolStripMenuItem.Enabled = false;
            this.ขอมลFixedAssetToolStripMenuItem.Name = "ขอมลFixedAssetToolStripMenuItem";
            this.ขอมลFixedAssetToolStripMenuItem.Size = new System.Drawing.Size(276, 22);
            this.ขอมลFixedAssetToolStripMenuItem.Text = "ข้อมูล Fixed Asset ในสถานี";
            this.ขอมลFixedAssetToolStripMenuItem.Visible = false;
            this.ขอมลFixedAssetToolStripMenuItem.Click += new System.EventHandler(this.ขอมลFixedAssetToolStripMenuItem_Click);
            // 
            // toolStripMenuItem10
            // 
            this.toolStripMenuItem10.Name = "toolStripMenuItem10";
            this.toolStripMenuItem10.Size = new System.Drawing.Size(273, 6);
            // 
            // ขอมลจดเสยToolStripMenuItem1
            // 
            this.ขอมลจดเสยToolStripMenuItem1.Name = "ขอมลจดเสยToolStripMenuItem1";
            this.ขอมลจดเสยToolStripMenuItem1.Size = new System.Drawing.Size(276, 22);
            this.ขอมลจดเสยToolStripMenuItem1.Text = "ข้อมูลจุดเสีย";
            this.ขอมลจดเสยToolStripMenuItem1.Click += new System.EventHandler(this.ขอมลจดเสยToolStripMenuItem1_Click);
            // 
            // ขอมลTranTypeToolStripMenuItem2
            // 
            this.ขอมลTranTypeToolStripMenuItem2.Name = "ขอมลTranTypeToolStripMenuItem2";
            this.ขอมลTranTypeToolStripMenuItem2.Size = new System.Drawing.Size(276, 22);
            this.ขอมลTranTypeToolStripMenuItem2.Text = "ข้อมูล TranType";
            this.ขอมลTranTypeToolStripMenuItem2.Click += new System.EventHandler(this.ขอมลTranTypeToolStripMenuItem2_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(273, 6);
            // 
            // updatePriceListToolStripMenuItem
            // 
            this.updatePriceListToolStripMenuItem.Enabled = false;
            this.updatePriceListToolStripMenuItem.Name = "updatePriceListToolStripMenuItem";
            this.updatePriceListToolStripMenuItem.Size = new System.Drawing.Size(276, 22);
            this.updatePriceListToolStripMenuItem.Text = "Update PriseLis/ส่วนลดตามไตรมาส";
            this.updatePriceListToolStripMenuItem.Visible = false;
            this.updatePriceListToolStripMenuItem.Click += new System.EventHandler(this.updatePriceListToolStripMenuItem_Click);
            // 
            // updateเงอนไขสญญาToolStripMenuItem
            // 
            this.updateเงอนไขสญญาToolStripMenuItem.Enabled = false;
            this.updateเงอนไขสญญาToolStripMenuItem.Name = "updateเงอนไขสญญาToolStripMenuItem";
            this.updateเงอนไขสญญาToolStripMenuItem.Size = new System.Drawing.Size(276, 22);
            this.updateเงอนไขสญญาToolStripMenuItem.Text = "Update เงื่อนไขสัญญา";
            this.updateเงอนไขสญญาToolStripMenuItem.Click += new System.EventHandler(this.updateเงอนไขสญญาToolStripMenuItem_Click);
            // 
            // vvvvToolStripMenuItem
            // 
            this.vvvvToolStripMenuItem.Enabled = false;
            this.vvvvToolStripMenuItem.Name = "vvvvToolStripMenuItem";
            this.vvvvToolStripMenuItem.Size = new System.Drawing.Size(289, 22);
            this.vvvvToolStripMenuItem.Text = "ล้างข้อมูล JOB";
            this.vvvvToolStripMenuItem.Visible = false;
            this.vvvvToolStripMenuItem.Click += new System.EventHandler(this.vvvvToolStripMenuItem_Click);
            // 
            // toolStripMenuItem9
            // 
            this.toolStripMenuItem9.Name = "toolStripMenuItem9";
            this.toolStripMenuItem9.Size = new System.Drawing.Size(286, 6);
            // 
            // เปลยนPasswordToolStripMenuItem1
            // 
            this.เปลยนPasswordToolStripMenuItem1.Name = "เปลยนPasswordToolStripMenuItem1";
            this.เปลยนPasswordToolStripMenuItem1.Size = new System.Drawing.Size(289, 22);
            this.เปลยนPasswordToolStripMenuItem1.Text = "เปลี่ยน Password";
            this.เปลยนPasswordToolStripMenuItem1.Click += new System.EventHandler(this.เปลยนPasswordToolStripMenuItem1_Click);
            // 
            // fAMToolStripMenuItem
            // 
            this.fAMToolStripMenuItem.Name = "fAMToolStripMenuItem";
            this.fAMToolStripMenuItem.Size = new System.Drawing.Size(289, 22);
            this.fAMToolStripMenuItem.Text = "FAM";
            this.fAMToolStripMenuItem.Visible = false;
            this.fAMToolStripMenuItem.Click += new System.EventHandler(this.fAMToolStripMenuItem_Click);
            // 
            // เบกอะใหลToolStripMenuItem
            // 
            this.เบกอะใหลToolStripMenuItem.Name = "เบกอะใหลToolStripMenuItem";
            this.เบกอะใหลToolStripMenuItem.Size = new System.Drawing.Size(289, 22);
            this.เบกอะใหลToolStripMenuItem.Text = "เบิกอะไหล่ {ต้องเชื่อมต่ออินเทอร์เนต ขณะใช้งาน}";
            this.เบกอะใหลToolStripMenuItem.Click += new System.EventHandler(this.เบกอะใหลToolStripMenuItem_Click);
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(286, 6);
            // 
            // downloadฐานขอมลToolStripMenuItem
            // 
            this.downloadฐานขอมลToolStripMenuItem.Enabled = false;
            this.downloadฐานขอมลToolStripMenuItem.Name = "downloadฐานขอมลToolStripMenuItem";
            this.downloadฐานขอมลToolStripMenuItem.Size = new System.Drawing.Size(289, 22);
            this.downloadฐานขอมลToolStripMenuItem.Text = "(1) Download/Update ฐานข้อมูล";
            this.downloadฐานขอมลToolStripMenuItem.Visible = false;
            this.downloadฐานขอมลToolStripMenuItem.Click += new System.EventHandler(this.downloadฐานขอมลToolStripMenuItem_Click);
            // 
            // อปเดตฐานขอมลToolStripMenuItem
            // 
            this.อปเดตฐานขอมลToolStripMenuItem.Enabled = false;
            this.อปเดตฐานขอมลToolStripMenuItem.Name = "อปเดตฐานขอมลToolStripMenuItem";
            this.อปเดตฐานขอมลToolStripMenuItem.Size = new System.Drawing.Size(289, 22);
            this.อปเดตฐานขอมลToolStripMenuItem.Text = "(2) Update ฐานข้อมูล";
            this.อปเดตฐานขอมลToolStripMenuItem.Visible = false;
            this.อปเดตฐานขอมลToolStripMenuItem.Click += new System.EventHandler(this.อปเดตฐานขอมลToolStripMenuItem_Click);
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(286, 6);
            this.toolStripMenuItem7.Visible = false;
            // 
            // updateSQLScriptToolStripMenuItem
            // 
            this.updateSQLScriptToolStripMenuItem.Enabled = false;
            this.updateSQLScriptToolStripMenuItem.Name = "updateSQLScriptToolStripMenuItem";
            this.updateSQLScriptToolStripMenuItem.Size = new System.Drawing.Size(289, 22);
            this.updateSQLScriptToolStripMenuItem.Text = "Update SQL Script";
            this.updateSQLScriptToolStripMenuItem.Visible = false;
            this.updateSQLScriptToolStripMenuItem.Click += new System.EventHandler(this.updateSQLScriptToolStripMenuItem_Click);
            // 
            // เปดJOBToolStripMenuItem
            // 
            this.เปดJOBToolStripMenuItem.Enabled = false;
            this.เปดJOBToolStripMenuItem.Name = "เปดJOBToolStripMenuItem";
            this.เปดJOBToolStripMenuItem.Size = new System.Drawing.Size(289, 22);
            this.เปดJOBToolStripMenuItem.Text = "เปิด JOB Mileage";
            this.เปดJOBToolStripMenuItem.Visible = false;
            this.เปดJOBToolStripMenuItem.Click += new System.EventHandler(this.เปดJOBToolStripMenuItem_Click);
            // 
            // toolStripMenuItem8
            // 
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            this.toolStripMenuItem8.Size = new System.Drawing.Size(286, 6);
            // 
            // uploadJOBPhotoToolStripMenuItem
            // 
            this.uploadJOBPhotoToolStripMenuItem.Name = "uploadJOBPhotoToolStripMenuItem";
            this.uploadJOBPhotoToolStripMenuItem.Size = new System.Drawing.Size(289, 22);
            this.uploadJOBPhotoToolStripMenuItem.Text = "RE-Upload JOB Photo";
            this.uploadJOBPhotoToolStripMenuItem.Click += new System.EventHandler(this.uploadJOBPhotoToolStripMenuItem_Click);
            // 
            // uploadJOBPhotoToolStripMenuItem1
            // 
            this.uploadJOBPhotoToolStripMenuItem1.Name = "uploadJOBPhotoToolStripMenuItem1";
            this.uploadJOBPhotoToolStripMenuItem1.Size = new System.Drawing.Size(289, 22);
            this.uploadJOBPhotoToolStripMenuItem1.Text = "Upload JOB Photo SlipPayment";
            this.uploadJOBPhotoToolStripMenuItem1.Click += new System.EventHandler(this.uploadJOBPhotoToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem13
            // 
            this.toolStripMenuItem13.Name = "toolStripMenuItem13";
            this.toolStripMenuItem13.Size = new System.Drawing.Size(286, 6);
            // 
            // SurveyDataToolStripMenuItem
            // 
            this.SurveyDataToolStripMenuItem.Enabled = false;
            this.SurveyDataToolStripMenuItem.Name = "SurveyDataToolStripMenuItem";
            this.SurveyDataToolStripMenuItem.Size = new System.Drawing.Size(289, 22);
            this.SurveyDataToolStripMenuItem.Text = "สำรวจข้อมูลสถานี";
            this.SurveyDataToolStripMenuItem.Visible = false;
            this.SurveyDataToolStripMenuItem.Click += new System.EventHandler(this.SurveyDataToolStripMenuItem_Click);
            // 
            // EditJOBOnlineToolStripMenuItem
            // 
            this.EditJOBOnlineToolStripMenuItem.Enabled = false;
            this.EditJOBOnlineToolStripMenuItem.Name = "EditJOBOnlineToolStripMenuItem";
            this.EditJOBOnlineToolStripMenuItem.Size = new System.Drawing.Size(289, 22);
            this.EditJOBOnlineToolStripMenuItem.Text = "แก้ไขข้อมูล JOB ย้อนหลัง";
            this.EditJOBOnlineToolStripMenuItem.Visible = false;
            this.EditJOBOnlineToolStripMenuItem.Click += new System.EventHandler(this.EditJOBOnlineToolStripMenuItem_Click);
            // 
            // toolStripButton_CloseJOB
            // 
            this.toolStripButton_CloseJOB.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_CloseJOB.Image")));
            this.toolStripButton_CloseJOB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_CloseJOB.Name = "toolStripButton_CloseJOB";
            this.toolStripButton_CloseJOB.Size = new System.Drawing.Size(114, 51);
            this.toolStripButton_CloseJOB.Text = "บันทึก ออกงาน/ปิดงาน";
            this.toolStripButton_CloseJOB.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton_CloseJOB.Click += new System.EventHandler(this.toolStripBt_CloseJOB_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 54);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2.Image = global::SMSMINI.Properties.Resources.smsmini2_logo1;
            this.pictureBox2.Location = new System.Drawing.Point(67, 12);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(254, 66);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 8;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Navy;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = global::SMSMINI.Properties.Resources.smsmini22;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(885, 549);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // btSMSweb
            // 
            this.btSMSweb.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btSMSweb.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btSMSweb.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btSMSweb.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btSMSweb.ForeColor = System.Drawing.Color.LightCyan;
            this.btSMSweb.Location = new System.Drawing.Point(589, 382);
            this.btSMSweb.Name = "btSMSweb";
            this.btSMSweb.Size = new System.Drawing.Size(185, 27);
            this.btSMSweb.TabIndex = 5;
            this.btSMSweb.Text = "SMS Web";
            this.btSMSweb.UseVisualStyleBackColor = false;
            this.btSMSweb.Click += new System.EventHandler(this.btSMSweb_Click);
            // 
            // btSTART_STOP_Mileage
            // 
            this.btSTART_STOP_Mileage.BackColor = System.Drawing.Color.RoyalBlue;
            this.btSTART_STOP_Mileage.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btSTART_STOP_Mileage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btSTART_STOP_Mileage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btSTART_STOP_Mileage.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.btSTART_STOP_Mileage.Location = new System.Drawing.Point(589, 448);
            this.btSTART_STOP_Mileage.Name = "btSTART_STOP_Mileage";
            this.btSTART_STOP_Mileage.Size = new System.Drawing.Size(185, 27);
            this.btSTART_STOP_Mileage.TabIndex = 7;
            this.btSTART_STOP_Mileage.Text = "START STOP Mileage";
            this.btSTART_STOP_Mileage.UseVisualStyleBackColor = false;
            this.btSTART_STOP_Mileage.Click += new System.EventHandler(this.btSTART_STOP_Mileage_Click);
            // 
            // btSMSSurvey
            // 
            this.btSMSSurvey.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btSMSSurvey.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btSMSSurvey.Enabled = false;
            this.btSMSSurvey.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btSMSSurvey.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btSMSSurvey.ForeColor = System.Drawing.Color.Azure;
            this.btSMSSurvey.Location = new System.Drawing.Point(589, 153);
            this.btSMSSurvey.Name = "btSMSSurvey";
            this.btSMSSurvey.Size = new System.Drawing.Size(185, 27);
            this.btSMSSurvey.TabIndex = 4;
            this.btSMSSurvey.Text = "ข้อมูลสำรวจสถานี";
            this.btSMSSurvey.UseVisualStyleBackColor = false;
            this.btSMSSurvey.Visible = false;
            this.btSMSSurvey.Click += new System.EventHandler(this.btSMSSurvey_Click);
            // 
            // btCloseJOB
            // 
            this.btCloseJOB.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btCloseJOB.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btCloseJOB.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btCloseJOB.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btCloseJOB.ForeColor = System.Drawing.Color.LightCyan;
            this.btCloseJOB.Location = new System.Drawing.Point(100, 180);
            this.btCloseJOB.Name = "btCloseJOB";
            this.btCloseJOB.Size = new System.Drawing.Size(185, 73);
            this.btCloseJOB.TabIndex = 0;
            this.btCloseJOB.Text = "บันทึก ออกงาน/ปิดงาน";
            this.btCloseJOB.UseVisualStyleBackColor = false;
            this.btCloseJOB.Click += new System.EventHandler(this.btCloseJOB_Click);
            // 
            // btFFA_Save
            // 
            this.btFFA_Save.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btFFA_Save.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btFFA_Save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btFFA_Save.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btFFA_Save.ForeColor = System.Drawing.Color.LightCyan;
            this.btFFA_Save.Location = new System.Drawing.Point(100, 382);
            this.btFFA_Save.Name = "btFFA_Save";
            this.btFFA_Save.Size = new System.Drawing.Size(185, 27);
            this.btFFA_Save.TabIndex = 1;
            this.btFFA_Save.Text = "บันทึกข้อมูล  Fixed Asset";
            this.btFFA_Save.UseVisualStyleBackColor = false;
            this.btFFA_Save.Click += new System.EventHandler(this.btFFA_Save_Click);
            // 
            // btMileageStatement
            // 
            this.btMileageStatement.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btMileageStatement.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btMileageStatement.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btMileageStatement.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btMileageStatement.ForeColor = System.Drawing.Color.LightBlue;
            this.btMileageStatement.Location = new System.Drawing.Point(589, 415);
            this.btMileageStatement.Name = "btMileageStatement";
            this.btMileageStatement.Size = new System.Drawing.Size(185, 27);
            this.btMileageStatement.TabIndex = 6;
            this.btMileageStatement.Text = "Mileage Statement";
            this.btMileageStatement.UseVisualStyleBackColor = false;
            this.btMileageStatement.Click += new System.EventHandler(this.btMileageStatement_Click);
            // 
            // btFFA_Confirm
            // 
            this.btFFA_Confirm.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btFFA_Confirm.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btFFA_Confirm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btFFA_Confirm.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btFFA_Confirm.ForeColor = System.Drawing.Color.LightBlue;
            this.btFFA_Confirm.Location = new System.Drawing.Point(100, 415);
            this.btFFA_Confirm.Name = "btFFA_Confirm";
            this.btFFA_Confirm.Size = new System.Drawing.Size(185, 27);
            this.btFFA_Confirm.TabIndex = 2;
            this.btFFA_Confirm.Text = "ยืนยัน ข้อมูล Fixed Asset";
            this.btFFA_Confirm.UseVisualStyleBackColor = false;
            this.btFFA_Confirm.Click += new System.EventHandler(this.btFFA_Confirm_Click);
            // 
            // btReUploadPhoto
            // 
            this.btReUploadPhoto.BackColor = System.Drawing.Color.RoyalBlue;
            this.btReUploadPhoto.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btReUploadPhoto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btReUploadPhoto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btReUploadPhoto.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.btReUploadPhoto.Location = new System.Drawing.Point(100, 448);
            this.btReUploadPhoto.Name = "btReUploadPhoto";
            this.btReUploadPhoto.Size = new System.Drawing.Size(185, 27);
            this.btReUploadPhoto.TabIndex = 3;
            this.btReUploadPhoto.Text = "RE-Upload JOB Photo";
            this.btReUploadPhoto.UseVisualStyleBackColor = false;
            this.btReUploadPhoto.Click += new System.EventHandler(this.btReUploadPhoto_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(885, 549);
            this.Controls.Add(this.btReUploadPhoto);
            this.Controls.Add(this.btFFA_Confirm);
            this.Controls.Add(this.btFFA_Save);
            this.Controls.Add(this.btCloseJOB);
            this.Controls.Add(this.btSMSSurvey);
            this.Controls.Add(this.btSTART_STOP_Mileage);
            this.Controls.Add(this.btMileageStatement);
            this.Controls.Add(this.btSMSweb);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Main";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SMS MINI2";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Main_FormClosed);
            this.Load += new System.EventHandler(this.Main_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton toolStripBt_Close;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripButton toolStripButton_CloseJOB;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripButton toolStripButton_LogOff;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripButton toolStripButton_SMSAbout;
        private System.Windows.Forms.ToolStripDropDownButton toolStripButton_SMSWeb;
        private System.Windows.Forms.ToolStripMenuItem sMSWebToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uploadMonitoringToolStripMenuItem;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton_MasterData;
        private System.Windows.Forms.ToolStripMenuItem aaaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ddddToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vvvvToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fffToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ขอมลผใชงานVANToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem ขอมลจดเสยToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem ขอมลTranTypeToolStripMenuItem2;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem เปลยนPasswordToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem networkConnectionTestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem รจกผพฒนาToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pricesListToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem รายงานTAGPrintToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem updatePriceListToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolStripMenuItem fAMToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem เบกอะใหลToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateเงอนไขสญญาToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem อปเดตฐานขอมลToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem downloadฐานขอมลToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem updateSQLScriptToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem เปดJOBToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem พมพรายงานFailureCodeWIWPonlineToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem พมพJOBPhotoReportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uploadJOBPhotoToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem uploadJOBPhotoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem บนทกขอมลFixedAssetToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem12;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem11;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem13;
        private System.Windows.Forms.ToolStripMenuItem ขอมลFixedAssetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ขอมลToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ยนยนขอมลFixedAssetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SurveyDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem พมพใบปะหนาSOToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem14;
        private System.Windows.Forms.ToolStripMenuItem EditJOBOnlineToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button btSMSweb;
        private System.Windows.Forms.Button btSTART_STOP_Mileage;
        private System.Windows.Forms.Button btSMSSurvey;
        private System.Windows.Forms.Button btCloseJOB;
        private System.Windows.Forms.Button btFFA_Save;
        private System.Windows.Forms.Button btMileageStatement;
        private System.Windows.Forms.Button btFFA_Confirm;
        private System.Windows.Forms.Button btReUploadPhoto;
    }
}