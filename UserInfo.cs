﻿using Newtonsoft.Json;
using System;

namespace SMSMINI
{
    
    public static class UserInfo
    {
        //private static string userId;
        //private static string userName;
        //private static string password;
        //private static string fullName;
        //private static string userLevID;
        //private static string userLevel;
        //private static string van_ID;
        //private static string sup_VAN;
        //private static string email;
        //private static string van_Type;
        //private static string vehicleRegis;
        //private static string connectMode;
        //private static DateTime smsServerDateTime;

        ////20200313 
        //private static string mySMSMINIVersion;

        ////20200707
        //private static string eDepId;


        ////public UserInfo()
        ////{
        ////}

        //public static string UserId { get { return userId; } set { userId = value; } }

        //public static string EMPId { get; set; }

        //public static string UserName { get { return userName; } set { userName = value; } }

        //public static string Password { get { return password; } set { password = value; } }

        //public static string FullName { get { return fullName; } set { fullName = value; } }

        //public static string UserLevID { get { return userLevID; } set { userLevID = value; } }
        //public static string UserLevel { get { return userLevel; } set { userLevel = value; } }

        //public static string Van_ID { get { return van_ID; } set { van_ID = value; } }
        //public static string SUP_VAN { get { return sup_VAN; } set { sup_VAN = value; } }

        //public static string Email { get { return email; } set { email = value; } }

        ////20200313
        //public static string MySMSMINIVersion { get { return mySMSMINIVersion; } set { mySMSMINIVersion = value; } }


        /////
        //public static DateTime SMSServerDateTime { get { return smsServerDateTime; } set { smsServerDateTime = value; } }


        ///// <summary>
        ///// 30 = VAN ตู้จ่าย
        ///// 31 = VAN ระบบ
        ///// </summary>
        //public static string Van_Type { get { return van_Type; } set { van_Type = value; } }
        //public static string VehicleRegis { get { return vehicleRegis; } set { vehicleRegis = value; } }


        ////connectMode
        ///// <summary>
        ///// 0=Offline
        ///// 1=Online
        ///// </summary>
        //public static string ConnectMode { get { return connectMode; } set { connectMode = value; } }

        //public static string getConnectMode()
        //{
        //    return connectMode;
        //}


        //public static string getVanID() { return van_ID; }

        ////20200707
        //public static string eDepID { get { return eDepId; } set { eDepId = value; } }



        //public int MyProperty { get; set; }
        
        public static string UserId { get; set; }
      
        public static string EMPId { get; set; }
        
        public static string UserName { get; set; }
      
        public static string Password { get; set; }
       
        public static string FullName { get; set; }
        
        public static string UserLevID { get; set; }
      
        public static string UserLevel { get; set; }
        
        public static string Van_ID { get; set; }
         
        public static string SUP_VAN { get; set; }
        
        public static string Email { get; set; }
        
        public static string MySMSMINIVersion { get; set; }
        
        public static DateTime SMSServerDateTime { get; set; }
        
        public static string Van_Type { get; set; }
       
        public static string VehicleRegis { get; set; }
       
        public static string ConnectMode { get; set; }
       
        public static string eDepID { get; set; }
        
        public static int IsSVPrintOnline { get; set; }

    }



    public class xUserInfo
    {

        public string UserId { get; set; }

        public string EMPId { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string FullName { get; set; }

        public string UserLevID { get; set; }

        public string UserLevel { get; set; }

        public string Van_ID { get; set; }

        public string SUP_VAN { get; set; }

        public string Email { get; set; }

        public string MySMSMINIVersion { get; set; }

        public DateTime SMSServerDateTime { get; set; }

        public string Van_Type { get; set; }

        public string VehicleRegis { get; set; }

        public string ConnectMode { get; set; }

        public string eDepID { get; set; }

        public int IsSVPrintOnline { get; set; }

    }

}
