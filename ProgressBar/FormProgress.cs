using System;
using System.Reflection;
using System.Windows.Forms;

namespace SMSMINI.ProgressBar
{
    public partial class FormProgress : Form
    {
        private bool m_Cancel = false;
        public bool Cancel
        {
            get { return m_Cancel; }
        }

        public FormProgress()
        {
            InitializeComponent();
            object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
            if (attributes.Length == 0)
            {
                this.Text = "SMS MINI [v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "]";

            }
            this.Text = ((AssemblyProductAttribute)attributes[0]).Product + " [v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "]";

        }

        private void fmProgress_FormClosing(object sender, FormClosingEventArgs e)
        {
            m_Cancel = true;
            e.Cancel = true;
        }

        private void bnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FormProgress_Load(object sender, EventArgs e)
        {

        }


    }
}