﻿using System;
using System.Data;
using System.Reflection;
using System.Windows.Forms;

namespace SMSMINI.Transaction
{
    public partial class ReturnPartsRoutingTagReport : Form
    {
        private string strConn = "";

        string strUsr = "sa";
        string strPwd = "enabler";
        string strServer = ".\\SQLEXPRESS";
        string strDB = "SMSminiDB";


        string tmpJobID = "";
        public string TmpJobID { get { return tmpJobID; } set { tmpJobID = value; } }

        public ReturnPartsRoutingTagReport()
        {
            InitializeComponent();

            object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
            if (attributes.Length == 0)
            {
                this.Text = "SMS MINI [v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "]";

            }
            this.Text = ((AssemblyProductAttribute)attributes[0]).Product + " [v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "]";

        }

        private void toolStripButton_FindStation_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            if (UserInfo.ConnectMode == "0")

                strConn = Properties.Settings.Default["ServicesMSDBConnectionString"].ToString();
            else
                strConn = Properties.Settings.Default["ServicesMSDBConnectionString"].ToString();


            //strConn = ConnectionManager.GetConnectionString((UserInfo.ConnectMode ?? "0"));
            Report.rptReturnPartsRoutingTag rpt = new SMSMINI.Transaction.Report.rptReturnPartsRoutingTag();

            using (System.Data.SqlClient.SqlConnection Conn = new System.Data.SqlClient.SqlConnection(strConn))
            {
                if (Conn.State == ConnectionState.Closed)
                    Conn.Open();

                DataTable tmpTable = new DataTable();


                string sql = "";

                if (toolStripLabel_cobby.Text == "ตาม JOB No")
                    sql = "select  * from v_rpt_ReturnPartsRoutingTag where JOB_ID like '%" + toolStripText_txtFind.Text + "%' " +
                        " and TRA_ID IN ('CA','CU','IS', 'WA') and StatusSpare = 1 and IsCancel in ('0',null)  Order by NO;";
                else if (toolStripLabel_cobby.Text == "ตาม Part no")
                    sql = "select  * from v_rpt_ReturnPartsRoutingTag  where SPA_ID like '%" + toolStripText_txtFind.Text + "%' " +
                        " and TRA_ID IN ('CA','CU','IS', 'WA') and StatusSpare = 1 and IsCancel in ('0',null)   Order by NO;";
                else if (toolStripLabel_cobby.Text == "ตาม Part name")
                    sql = "select  * from v_rpt_ReturnPartsRoutingTag  where SparePart like '%" + toolStripText_txtFind.Text + "%' " +
                        " and TRA_ID IN ('CA','CU','IS', 'WA') and StatusSpare = 1 and IsCancel in ('0',null)   Order by NO;";
                else   //ทั้งหมด
                    sql = "select  * from v_rpt_ReturnPartsRoutingTag  Order by NO;";



                System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(sql, Conn);
                DataSet ds = new DataSet();
                da.Fill(ds);


                if (ds.Tables[0].Rows.Count > 0)
                {
                    rpt.SetDatabaseLogon(strUsr, strPwd, strServer, strDB);
                    rpt.SetDataSource(ds.Tables[0].DefaultView);

                    rpt.SetParameterValue("pSMSVersion", "SMS MINI v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString());

                    crystalReportViewer1.ReportSource = rpt;


                }
                else
                {
                    crystalReportViewer1.ReportSource = null;
                    MessageBox.Show("ไม่มีข้อมูล กรุณาป้อนเงื่อนไขใหม่ ", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }

            Cursor.Current = Cursors.Default;
        }

        private void ReturnPartsRoutingTagReport_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tmpJobID))
            {
                toolStripText_txtFind.Text = tmpJobID;
                toolStripLabel_cobby.SelectedIndex = 0;

                toolStripButton_FindStation_Click(null, null);
                getDBName();
            }

        }


        private void getDBName()
        {

            string _dbName = Properties.Settings.Default["ServicesMSDBConnectionString"].ToString();
            try
            {
                strDB = _dbName.Split('=')[2].Split('=')[0].Split(';')[0].ToString();
            }
            catch (System.Exception)
            {
                strDB = "SMSminiDB";
            }
        }

        private void toolStripButton_Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void toolStripButton_Save_Click(object sender, EventArgs e)
        {

        }
    }
}
