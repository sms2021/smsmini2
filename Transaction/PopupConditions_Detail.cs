﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace SMSMINI.Transaction
{
    public partial class PopupConditions_Detail : Form
    {

        public List<DAL.SMSManage.vw_mini_pop_Conf_Conditions_Detail> lsPopupC { get; set; }


        public PopupConditions_Detail()
        {
            InitializeComponent();
        }

        private void PopupConditions_Detail_Load(object sender, EventArgs e)
        {
            List<MINI_Pop> lsQPo = new List<MINI_Pop>();
            if (lsPopupC.Count() > 0)
            {
                var q1 = lsPopupC.First();
                textBox1.Text = q1.GOP_ID + "" + q1.GroupStation + " " + q1.TradeName;
                textBox2.Text = q1.JobType;
                textBox3.Text = q1.WorkType;

                ////dataGridView1.DataSource = lsPopupC
                ////    .OrderBy(t => t.WorkType)
                ////    .Select(t => new
                ////    {
                ////        t.NO,
                ////        t.Detail
                ////    })
                ////    .OrderBy(t => t.NO)
                //// .ToList();

                foreach (var item in lsPopupC)
                {
                    var detail = item.Detail.Split('|');

                    string xdetail = "";

                    if (detail.Length > 1)
                    {
                        foreach (var item2 in detail)
                        {
                            xdetail += item2 + Environment.NewLine;
                        }
                    }
                    else
                    {
                        xdetail += detail[0].ToString();
                    }

                    lsQPo.Add(new MINI_Pop { NO = item.NO, Detail = xdetail });
                }
                dataGridView1.DataSource = lsQPo.OrderBy(t => t.NO).ToList();







            }
        }
    }


    public class MINI_Pop
    {
        public int NO { get; set; }
        public string Detail { get; set; }

    }
}
