﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;


namespace SMSMINI.Transaction
{
    public partial class PartsRequisition : Form
    {
        private ProgressBar.FormProgress m_fmProgress = null;

        List<PartREQ_List> reqlist;
        string reqMOD = string.Empty;

        public PartsRequisition()
        {
            InitializeComponent();

            string _mode = (UserInfo.ConnectMode == "0") ? " [Off line]" : " [On line]";
            object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
            if (attributes.Length == 0)
            {
                this.Text = this.Text + " SMS MINI [v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "] " + _mode;

            }
            this.Text = this.Text + " " + ((AssemblyProductAttribute)attributes[0]).Product + " [v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "] " + _mode;


        }

        private void PartsRequisition_Load(object sender, EventArgs e)
        {
            toolStrip_txtCloseJOB.Text = DateTime.Now.ToString("yyyy-MM-dd");

            EnableControl(false, false, false, false, false);

            lblSPA_ID.Text = string.Empty;

            txtREQDetail.Text = string.Empty;

            txtPartRED_ID.Text = string.Empty;
            txtPart_NO.Text = string.Empty;

            txtPartName.Text = string.Empty;
            txtQuantity.Text = string.Empty;

            lblMode.Text = string.Empty;


            reqlist = new List<PartREQ_List>();
            dataGridView1.DataSource = reqlist.ToList();
            dataGridView1.Refresh();


            txtVAN.Text = UserInfo.Van_ID + " " + UserInfo.FullName;

            groupBox2.Enabled = false;

        }



        private void EnableControl(bool _toolStrip_txtCloseJOB, bool _toolStripButton_FindStation,
            bool _toolStripButton_Save, bool _toolStripButton_Save_ERP, bool _toolStripButton_Cancel)
        {
            toolStrip_txtCloseJOB.Enabled = _toolStrip_txtCloseJOB;
            toolStripButton_FindStation.Enabled = _toolStripButton_FindStation;
            toolStripButton_Save.Enabled = _toolStripButton_Save;
            //toolStripButton_Save_ERP.Enabled = _toolStripButton_Save_ERP;
            toolStripButton_Save_ERP.Enabled = false;
            toolStripButton_Cancel.Enabled = _toolStripButton_Cancel;
        }

        private void PartsRequisition_FormClosed(object sender, FormClosedEventArgs e)
        {
            DialogResult = DialogResult.Abort;
        }

        private void toolStripButton_FindStation_Click(object sender, EventArgs e)
        {
            try
            {

                if (toolStrip_txtCloseJOB.Text.Split('-').Length != 3)
                {
                    try
                    {
                        DateTime dt = Convert.ToDateTime(toolStrip_txtCloseJOB.Text);
                    }
                    catch (Exception)
                    {
                        closeProgress();
                        MessageBox.Show("กรูณาป้อนวันที่ให้ถูกต้อง format yyyy-mm-dd (คศ.)", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        toolStrip_txtCloseJOB.Focus();
                        toolStrip_txtCloseJOB.SelectAll();
                        return;
                    }

                    closeProgress();
                    MessageBox.Show("กรูณาป้อนวันที่ให้ถูกต้อง format yyyy-mm-dd (คศ.)", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    toolStrip_txtCloseJOB.Focus();
                    toolStrip_txtCloseJOB.SelectAll();
                    return;
                }
                if (reqMOD == "NEW")//Add New
                {
                    newProgressbar();

                    searchNEW_REQ();

                    checkDuplicateData();
                }
                else if (reqMOD == "EDIT")// Edit REQ
                {

                    searchEDIT_REQ();
                }// if (reqMOD == "NEW")

                closeProgress();
            }
            catch (Exception ex)
            {
                closeProgress();
                MessageBox.Show("เกิดข้อผิดพลาดในการค้นหาข้อมูล Error" + ex.Message, "กรุณาตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }

        private bool isDuplicateData()
        {
            string tmp1 = string.Empty;
            string tmp2 = string.Empty;

            foreach (var item in reqlist.OrderBy(t => t.SPA_ID))
            {
                tmp1 = item.SPA_ID;
                if (tmp1 == tmp2)
                {
                    for (int i = 0; i <= dataGridView1.RowCount - 1; i++)
                    {
                        if (item.REQ_ID == dataGridView1["REQ_ID", i].Value.ToString() &&
                            item.SPA_ID == dataGridView1["SPA_ID", i].Value.ToString())
                        {
                            return true;
                        }
                    }
                }
                tmp2 = item.SPA_ID;
            }

            return false;
        }


        private void checkDuplicateData()
        {
            string tmp1 = string.Empty;
            string tmp2 = string.Empty;

            foreach (var item in reqlist.OrderBy(t => t.SPA_ID))
            {
                tmp1 = item.SPA_ID;
                if (tmp1 == tmp2)
                {
                    for (int i = 0; i <= dataGridView1.RowCount - 1; i++)
                    {
                        if (item.REQ_ID == dataGridView1["REQ_ID", i].Value.ToString() &&
                            item.SPA_ID == dataGridView1["SPA_ID", i].Value.ToString())
                        {
                            for (int cc = 0; cc <= dataGridView1.ColumnCount - 1; cc++)
                            {
                                dataGridView1[cc, i].Style.BackColor = Color.Yellow;
                                dataGridView1[cc, i].Style.ForeColor = Color.Red;
                            }
                        }
                    }
                }
                tmp2 = item.SPA_ID;
            }
        }

        private void toolStrip_txtCloseJOB_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (toolStrip_txtCloseJOB.Text != string.Empty)
                {
                    toolStripButton_FindStation_Click(null, null);
                }
            }

        }


        private void searchEDIT_REQ()
        {

            Finding.Find_REQ_Edit f = new SMSMINI.Transaction.Finding.Find_REQ_Edit();
            f._DateReq = Convert.ToDateTime(toolStrip_txtCloseJOB.Text.Trim());
            f._EMP_ID = UserInfo.UserId;

            f.StartPosition = FormStartPosition.CenterParent;

            DateTime dtCloseJOB = DateTime.Parse(toolStrip_txtCloseJOB.Text);
            if (f.ShowDialog() == DialogResult.OK)
            {
                newProgressbar();
                invoke_Progress("กำลังตรวจสอบ ใบเบิก...");
                txtPartRED_ID.Text = f._REQ_ID;
                dateTimePicker_PartREQ.Value = f._DateReq.Value == null ? dtCloseJOB : f._DateReq.Value;
                txtVAN.Text = f._EMPname;
                chkIsSend_ERP.Checked = f._IsSend_ERP == null ? false : f._IsSend_ERP.Value;
                txtREQDetail.Text = f._REQDetail;

                using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
                {
                    var q = from t in dc.JOB_Detail_REQ_Details
                            from sp in dc.SpareParts
                            orderby t.REQ_ID, t.SPA_ID
                            where (t.SPA_ID == sp.SPA_ID) && (t.REQ_ID == f._REQ_ID)
                            select new
                            {
                                t.REQ_ID,
                                t.SPA_ID,
                                sp.SparePart1,
                                t.Quantity_JOB,
                                t.Quantity_REQ,
                                t.JOBRef,
                                t.ERP_LoadTime,
                                t.REQPart_Detail,
                                t.IsCancel
                            };

                    if (q.Count() > 0)
                    {
                        int xx = q.Count();
                        reqlist = new List<PartREQ_List>();

                        foreach (var item in q)
                        {
                            reqlist.Add(new PartREQ_List()
                            {
                                REQ_ID = item.REQ_ID,
                                SPA_ID = item.SPA_ID,
                                PartName = item.SparePart1,
                                QT_Use = item.Quantity_JOB ?? 0,
                                QT_REQ = item.Quantity_REQ ?? 0,
                                TranDate = dateTimePicker_PartREQ.Value,
                                ERP_LoadTime = item.ERP_LoadTime,
                                JOBRef = item.JOBRef,
                                รายละเอียดการเบิกPart = item.REQPart_Detail//,
                                //IsCancel = item.IsCancel
                            });
                        }
                        dataGridView1.DataSource = reqlist.ToList();
                        dataGridView1.Refresh();
                    }

                }
            }
        }

        private void searchNEW_REQ()
        {
            invoke_Progress("กำลังตรวจสอบ JOB...");
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            {
                var q = (from j in dc.JOBs
                         from jd in dc.JOB_Detail_Spares
                         from sp in dc.SpareParts

                         where (j.JOB_ID == jd.JOB_ID) && (jd.SPA_ID == sp.SPA_ID) &&
                         (j.EMP_ID3 == UserInfo.UserId) &&
                         j.CloseDate.Value.Date >= Convert.ToDateTime(toolStrip_txtCloseJOB.Text.Trim()).Date &&
                         jd.StatusSpare == true
                         select new
                         {
                             j.Opendate,
                             jd.SPA_ID,
                             sp.SparePart1,
                             jd.Quantity,
                             JOBRef1 = getJOB(dc, jd.SPA_ID)
                         }).Distinct();
                if (q.Count() > 0)
                {
                    reqlist = new List<PartREQ_List>();
                    foreach (var item in q)
                    {
                        reqlist.Add(new PartREQ_List()
                        {
                            REQ_ID = txtPartRED_ID.Text.Trim(),
                            SPA_ID = item.SPA_ID,
                            PartName = item.SparePart1,
                            QT_Use = item.Quantity,
                            QT_REQ = item.Quantity,
                            TranDate = dateTimePicker_PartREQ.Value,
                            ERP_LoadTime = null,
                            JOBRef = item.JOBRef1,
                            รายละเอียดการเบิกPart = string.Empty //,
                            //IsCancel = null
                        });
                    }

                    dataGridView1.DataSource = reqlist.OrderBy(t => t.SPA_ID).ToList();
                    //dataGridView1.Refresh();
                }
                else
                {
                    closeProgress();
                    MessageBox.Show("ไม่มีข้อมูล กรุณาตรวจสอบใหม่", "ผลการค้นหา", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    toolStrip_txtCloseJOB.Focus();
                    toolStrip_txtCloseJOB.SelectAll();
                    return;
                }
            }

        }

        private string getJOB(SMSMINI.DAL.SMSManage.SMSManageDataContext dc, string p)
        {
            string _job = string.Empty;

            var q = (from j in dc.JOBs
                     from jd in dc.JOB_Detail_Spares
                     where (j.JOB_ID == jd.JOB_ID) &&
                     (j.EMP_ID3 == UserInfo.UserId) &&
                     j.CloseDate.Value.Date == Convert.ToDateTime(toolStrip_txtCloseJOB.Text.Trim()).Date &&
                     jd.StatusSpare == true
                     select new
                     {
                         j.JOB_ID,
                         jd.SPA_ID
                     }).Distinct();

            foreach (var item in q)
            {
                if (!_job.Contains(item.JOB_ID.Trim()))
                    _job += item.JOB_ID.Trim() + ",";
            }

            if (_job.Split(',').Length > 1)
                return _job.Remove(_job.Length - 1, 1);
            else
                return _job;
        }

        private void toolStripButton_Save_Click(object sender, EventArgs e)
        {
            if (isDuplicateData())
            {
                MessageBox.Show("ไม่สามารถบันทึกข้อมูลได้..." + Environment.NewLine +
                "เนื่องจากตรวจสอบพบ...มีอะไหล่ซ้ำ !!!..." + Environment.NewLine +
                "กรุณายกเลิกรายการที่ซ้ำ...ให้เหลือเพียง 1 รายการ..." + Environment.NewLine +
                "แล้วทำการใส่จำนวนเบิกที่ต้องการ...ในช่อง [QT_REQ]", "ผลการตรวจสอบ...", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (MessageBox.Show("คุณต้องการบันทึกข้อมูลใช่หรือไม่?...", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                if (reqMOD == "NEW")
                {
                    //txtPartRED_ID.Text = setREQ_NewID();
                    AddNew();
                }
                else
                    Edit();

                PartsRequisition_Load(null, null);
            }

        }

        private void Edit()
        {
            newProgressbar();
            invoke_Progress("กำลังบันทึกใบเบิก ออนไลน์....");

            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            {
                try
                {
                    dc.Connection.Open();
                    dc.Transaction = dc.Connection.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted);

                    decimal qtyu = 0;
                    decimal qtyr = 0;

                    var delREQ = from t in dc.JOB_Detail_REQ_Details
                                 where t.REQ_ID == txtPartRED_ID.Text.Trim()
                                 select t;
                    if (delREQ.Count() > 0)
                    {
                        dc.JOB_Detail_REQ_Details.DeleteAllOnSubmit(delREQ);


                        for (int i = 0; i <= dataGridView1.RowCount - 1; i++)
                        {
                            qtyu = decimal.Parse(dataGridView1["QT_Use", i].Value.ToString());
                            qtyr = decimal.Parse(dataGridView1["QT_REQ", i].Value.ToString());

                            var newreqD = new DAL.SMSManage.JOB_Detail_REQ_Detail()
                            {
                                REQ_ID = txtPartRED_ID.Text.Trim(),
                                SPA_ID = dataGridView1["SPA_ID", i].Value.ToString().Trim(),
                                Quantity_JOB = (int)qtyu,
                                Quantity_REQ = (int)qtyr,
                                TranDate = dateTimePicker_PartREQ.Value,
                                ERP_LoadTime = null,
                                JOBRef = dataGridView1["JOBRef", i].Value == null ? null : dataGridView1["JOBRef", i].Value.ToString(),

                                REQPart_Detail = dataGridView1["รายละเอียดการเบิกPart", i].Value.ToString().Trim()//,
                            };

                            dc.JOB_Detail_REQ_Details.InsertOnSubmit(newreqD);
                        }
                    }

                    dc.SubmitChanges();
                    dc.Transaction.Commit();
                    closeProgress();
                    MessageBox.Show("บันทึกข้อมูลเรียบร้อย", "ผลการทำงาน", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                catch (Exception ex)
                {
                    closeProgress();
                    dc.Transaction.Rollback();
                    MessageBox.Show("เกิดข้อผิดพลาดในบันทึกข้อมูล Error " + ex.Message, "ผลการทำงาน", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }

            }
        }

        private void AddNew()
        {
            newProgressbar();
            invoke_Progress("กำลังบันทึกใบเบิก ออนไลน์....");

            string newID = string.Empty;
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            {
                try
                {
                    dc.Connection.Open();
                    dc.Transaction = dc.Connection.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted);

                    newID = setNewREQ_ID(dc);
                    txtPartRED_ID.Text = newID;

                    var newreq = new DAL.SMSManage.JOB_Detail_REQ()
                    {
                        REQ_ID = newID,
                        DateReq = dateTimePicker_PartREQ.Value,
                        EMP_ID = UserInfo.UserId,
                        IsSend_ERP = null,
                        REQDetail = txtREQDetail.Text.Trim()

                    };

                    dc.JOB_Detail_REQs.InsertOnSubmit(newreq);

                    decimal qtyu = 0;
                    decimal qtyr = 0;

                    for (int i = 0; i <= dataGridView1.RowCount - 1; i++)
                    {
                        qtyu = decimal.Parse(dataGridView1["QT_Use", i].Value == null ? "0" : dataGridView1["QT_Use", i].Value.ToString());
                        qtyr = decimal.Parse(dataGridView1["QT_REQ", i].Value == null ? "0" : dataGridView1["QT_REQ", i].Value.ToString());

                        var newreqD = new DAL.SMSManage.JOB_Detail_REQ_Detail()
                        {
                            REQ_ID = newID,//xtPartRED_ID.Text.Trim(),
                            SPA_ID = dataGridView1["SPA_ID", i].Value.ToString().Trim(),

                            Quantity_JOB = (int)qtyu,
                            Quantity_REQ = (int)qtyr,
                            TranDate = dateTimePicker_PartREQ.Value,
                            ERP_LoadTime = null,
                            JOBRef = dataGridView1["JOBRef", i].Value == null ? null : dataGridView1["JOBRef", i].Value.ToString(),
                            REQPart_Detail = dataGridView1["รายละเอียดการเบิกPart", i].Value.ToString().Trim()//,
                            //IsCancel = dataGridView1["IsCancel", i].Value == null ? "0"[0] : dataGridView1["IsCancel", i].Value.ToString()[0]
                        };

                        dc.JOB_Detail_REQ_Details.InsertOnSubmit(newreqD);
                    }


                    dc.SubmitChanges();
                    dc.Transaction.Commit();
                    closeProgress();
                    MessageBox.Show("บันทึกข้อมูลเรียบร้อย", "ผลการทำงาน", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                catch (Exception ex)
                {
                    closeProgress();
                    dc.Transaction.Rollback();
                    MessageBox.Show("ไม่สามารถบันทึกข้อมูลได้ Error: " + ex.ToString(), "ข้อผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
        }

        private static string setNewREQ_ID(DAL.SMSManage.SMSManageDataContext dc)
        {
            string newID = "";
            string pfix = DateTime.Now.ToString("yy", new System.Globalization.CultureInfo("en-US")) + "" +
                DateTime.Now.ToString("MM", new System.Globalization.CultureInfo("en-US"));


            var q = (from t in dc.JOB_Detail_REQs
                     orderby t.REQ_ID descending
                     where t.REQ_ID.Substring(0, 4).Trim() == pfix
                     select t).FirstOrDefault();

            if (q != null)
            {
                //yymmREQ????
                string _id = q.REQ_ID.Substring(7, 4);//0009

                int _id2 = Convert.ToInt32(_id) + 1;
                _id = String.Format("{0:0000}", _id2);

                newID = pfix + "REQ" + _id;
            }
            else
            {
                newID = pfix + "REQ0001";
            }
            return newID;
        }



        private void toolStripButton_Cancel_Click(object sender, EventArgs e)
        {
            PartsRequisition_Load(null, null);
        }

        private void toolStripButton_Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btAdd_Click(object sender, EventArgs e)
        {
            if (lblSPA_ID.Text == string.Empty)
            {
                MessageBox.Show("กรุณาเลือกอะไหล่จาก ข้อมูลหลัก(Part master) เท่านั้น" + Environment.NewLine +
                    "กรุณาป้อนไหล่ แล้วค้นหา?...", "ผลการตรวจสอบ",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtPart_NO.Focus();
                txtPart_NO.SelectAll();
                return;
            }

            for (int i = 0; i <= dataGridView1.RowCount - 1; i++)
            {
                if (lblSPA_ID.Text == dataGridView1["SPA_ID", i].Value.ToString())
                {
                    MessageBox.Show("คุณป้อนข้อมูลซ้ำ กรุณาป้อนใหม่?...", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtPart_NO.Focus();
                    txtPart_NO.SelectAll();
                    return;
                }

            }


            if (txtQuantity.Text == string.Empty || txtQuantity.Text == "0")
            {
                MessageBox.Show("กรุณาป้อนจำนวน ที่ต้องการเบิก", "ผลการตรวจสอบ",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtQuantity.Focus();
                return;
            }

            reqlist.Add(new PartREQ_List()
            {
                REQ_ID = txtPartRED_ID.Text.Trim(),
                SPA_ID = lblSPA_ID.Text,//txtPart_NO.Text.Trim(),
                PartName = txtPartName.Text.Trim(),
                QT_Use = 0,
                QT_REQ = decimal.Parse(txtQuantity.Text),
                TranDate = dateTimePicker_PartREQ.Value,
                ERP_LoadTime = null,
                JOBRef = null,
                รายละเอียดการเบิกPart = this.txtREQPart_Detail.Text.Trim()//,
            });

            dataGridView1.DataSource = reqlist.ToList();
            dataGridView1.Refresh();

            lblSPA_ID.Text = string.Empty;
            txtPart_NO.Text = string.Empty;
            txtPartName.Text = string.Empty;
            txtQuantity.Text = string.Empty;
            txtREQPart_Detail.Text = string.Empty;

            txtPart_NO.Focus();
        }

        private void toolStripButton_New_Click(object sender, EventArgs e)
        {
            reqMOD = "NEW";

            lblMode.ForeColor = Color.LimeGreen;
            lblMode.Text = "[NEW]";

            toolStripLabel1.Text = "วันที่ปิด JOB";


            string pfix = DateTime.Now.ToString("yy", new System.Globalization.CultureInfo("en-US")) + "" +
                DateTime.Now.ToString("MM", new System.Globalization.CultureInfo("en-US"));
            txtPartRED_ID.Text = pfix + "REQ????";

            //txtPartRED_ID.Text = setREQ_NewID();

            EnableControl(true, true, true, true, true);
            groupBox2.Enabled = true;
        }

        private string setREQ_NewID()
        {
            string newID = "";
            string pfix = DateTime.Now.ToString("yy", new System.Globalization.CultureInfo("en-US")) + "" +
                DateTime.Now.ToString("MM", new System.Globalization.CultureInfo("en-US"));

            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            {
                var q = (from t in dc.JOB_Detail_REQs
                         orderby t.REQ_ID descending
                         where t.REQ_ID.Substring(0, 4).Trim() == pfix
                         select t).FirstOrDefault();

                if (q != null)
                {
                    //yymmREQ????
                    string _id = q.REQ_ID.Substring(7, 4);//0009

                    int _id2 = Convert.ToInt32(_id) + 1;
                    _id = String.Format("{0:0000}", _id2);

                    newID = pfix + "REQ" + _id;
                }
                else
                {
                    newID = pfix + "REQ0001";
                }


            }

            return newID;
        }

        private void toolStripButton_Edit_Click(object sender, EventArgs e)
        {
            reqMOD = "EDIT";

            lblMode.ForeColor = Color.Red;
            lblMode.Text = "[EDIT]";
            txtPartRED_ID.Text = string.Empty;

            toolStripLabel1.Text = "วันที่การเบิก";//วันที่ปิด JOB
            EnableControl(true, true, true, true, true);
            groupBox2.Enabled = true;
        }

        private void txtPart_NO_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Finding.Find_REQ_Part f = new SMSMINI.Transaction.Finding.Find_REQ_Part();
                f._SPA_ID = txtPart_NO.Text;
                f.StartPosition = FormStartPosition.CenterParent;

                if (f.ShowDialog() == DialogResult.OK)
                {
                    txtPart_NO.Text = f._SPA_ID;
                    lblSPA_ID.Text = f._SPA_ID;

                    txtPartName.Text = f._PartName;

                    txtQuantity.Focus();
                    txtQuantity.SelectAll();
                }

            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                if (e.ColumnIndex == 0)//ยกเลิกข้อมูล
                {
                    try
                    {

                        if (dataGridView1["ERP_LoadTime", e.RowIndex].Value != null)
                        {
                            MessageBox.Show("รายการเบิกอะไหล่ [" + dataGridView1["SPA_ID", e.RowIndex].Value.ToString() + "] นี้ ถูกส่งเข้าระบบ ERP แล้ว" + Environment.NewLine +
                            "ไม่สามารถยกเลิกได้", "ผลการตรวจสอบ...", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }


                        if (MessageBox.Show("คุณต้องการยกเลิกข้อมูลใช่หรือไม่?...", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            if (reqMOD == "NEW")
                            {
                                gridContenToText(e);
                                checkDuplicateData();
                            }
                            else
                            {
                                gridContenToText(e);
                                checkDuplicateData();
                            }
                        }

                    }
                    catch (Exception)
                    {

                        throw;
                    }
                }
            }


        }

        private void gridContenToText(DataGridViewCellEventArgs e)
        {
            lblSPA_ID.Text = dataGridView1["SPA_ID", e.RowIndex].Value.ToString();
            txtPart_NO.Text = dataGridView1["SPA_ID", e.RowIndex].Value.ToString();
            txtPartName.Text = dataGridView1["PartName", e.RowIndex].Value.ToString();
            txtQuantity.Text = dataGridView1["QT_REQ", e.RowIndex].Value.ToString();

            var q = (from t in reqlist
                     where t.SPA_ID == dataGridView1["SPA_ID", e.RowIndex].Value.ToString().Trim()
                     select t).FirstOrDefault();
            if (q != null)
            {
                reqlist.Remove(q);
                dataGridView1.DataSource = reqlist.ToList();
                dataGridView1.Refresh();
            }
        }

        private void toolStripLabel1_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {

            dataGridView1["REQ_ID", e.RowIndex].ReadOnly = true;
            dataGridView1["SPA_ID", e.RowIndex].ReadOnly = true;
            dataGridView1["PartName", e.RowIndex].ReadOnly = true;
            dataGridView1["QT_Use", e.RowIndex].ReadOnly = true;
            dataGridView1["QT_REQ", e.RowIndex].ReadOnly = false;
            dataGridView1["TranDate", e.RowIndex].ReadOnly = true;
            dataGridView1["ERP_LoadTime", e.RowIndex].ReadOnly = true;
            dataGridView1["JOBRef", e.RowIndex].ReadOnly = true;

            dataGridView1["REQ_ID", e.RowIndex].Style.BackColor = Color.LightGray;
            dataGridView1["SPA_ID", e.RowIndex].Style.BackColor = Color.LightGray;
            dataGridView1["PartName", e.RowIndex].Style.BackColor = Color.LightGray;
            dataGridView1["QT_Use", e.RowIndex].Style.BackColor = Color.LightGray;
            dataGridView1["QT_REQ", e.RowIndex].Style.BackColor = Color.White;
            dataGridView1["TranDate", e.RowIndex].Style.BackColor = Color.LightGray;
            dataGridView1["ERP_LoadTime", e.RowIndex].Style.BackColor = Color.LightGray;
            dataGridView1["JOBRef", e.RowIndex].Style.BackColor = Color.LightGray;
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            m_fmProgress.Activate();
            m_fmProgress.BringToFront();
            m_fmProgress.TopMost = true;
            m_fmProgress.ShowDialog();
        }


        private void newProgressbar()
        {
            m_fmProgress = new ProgressBar.FormProgress();
            backgroundWorker1.RunWorkerAsync();
            System.Threading.Thread.Sleep(3000);
        }

        private void invoke_Progress(string strMsg)
        {
            try
            {
                m_fmProgress.lblDescription.Invoke(
                   (MethodInvoker)delegate ()
                   {
                       m_fmProgress.lblDescription.Text = strMsg;
                   }
                   );
            }
            catch (System.Exception)
            { }
        }

        private void closeProgress()
        {
            if (m_fmProgress != null)
            {
                m_fmProgress.Invoke(
                    (MethodInvoker)delegate ()
                    {
                        m_fmProgress.Hide();
                        m_fmProgress.Dispose();
                        m_fmProgress = null;
                    }
                );
            }
        }

        private void txtQuantity_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < "0"[0] || e.KeyChar > "9"[0]) && e.KeyChar != ControlChars.Back && e.KeyChar != "."[0])
                e.Handled = true;
        }


    }
}
