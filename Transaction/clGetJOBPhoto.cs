﻿using System;

namespace SMSMINI.Transaction
{
    public class clGetJOBPhoto
    {
        public string JOB_ID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string STA_ID { get; set; }

        public string StationSys { get; set; }
        public string Work_ID { get; set; }
        public string VAN { get; set; }
        public string JobFailure_Detail { get; set; }

    }
}
