﻿using System;

namespace SMSMINI.Transaction
{
    public class PartREQ_List
    {
        public string REQ_ID { get; set; }
        public string SPA_ID { get; set; }
        public string PartName { get; set; }
        public decimal? QT_Use { get; set; }//จำนวนที่ใช้ไป
        public decimal? QT_REQ { get; set; }//จำนวนที่แนะนำในการเบิก

        public DateTime TranDate { get; set; }
        public DateTime? ERP_LoadTime { get; set; }
        public string JOBRef { get; set; }//JOB อ้างอิง
        public string รายละเอียดการเบิกPart { get; set; }//รายละเอียดการเบิกอะไหล่

    }
}
