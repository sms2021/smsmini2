﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace SMSMINI.Transaction
{
    public class UploadFFA
    {
        private string strConn = string.Empty;

        public List<string> strJobIDBinding { get; set; }
        List<DAL.SMSManage.JOB_Failure_Detail> lsJOB_Failure = null;

        DateTime servDate = DateTime.Now;//20190516

        public UploadFFA()
        {
            lsJOB_Failure = new List<SMSMINI.DAL.SMSManage.JOB_Failure_Detail>();

        }

        public void getDataFFA()
        {
            strConn = Properties.Settings.Default["ServicesMSDBConnectionString"].ToString();
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn))
            {
                var q = from t in dc.JOBs
                        from jd in dc.JOB_Failure_Details
                        where t.JOB_ID == jd.JOB_ID &&
                        ((jd.SerialNumber != "None") && (jd.SerialNumber_New != "None")) &&
                        ((jd.SerialNumber != "ไม่ระบุ...") && (jd.SerialNumber_New != "ไม่ระบุ...")) && //20150113
                        (t.IsUpload == "0"[0] || t.IsUpload == null) &&
                        strJobIDBinding.ToList().Contains(t.JOB_ID) && new string[] { "07", "99" }.Contains(t.JOBS_ID)
                        select jd;

                foreach (var item in q)
                {
                    lsJOB_Failure.Add(new DAL.SMSManage.JOB_Failure_Detail()
                    {

                        NO = item.NO,
                        JOB_ID = item.JOB_ID,
                        POI_ID = item.POI_ID,
                        FAI_ID = item.FAI_ID,
                        Failure_Detail = item.Failure_Detail,
                        SerialNumber = item.SerialNumber,
                        SerialNumber_New = item.SerialNumber_New,
                        trandate = item.trandate,
                        StartLiter = item.StartLiter,
                        EndLiter = item.EndLiter,
                        LiterTest = item.LiterTest,
                        Location = item.Location,
                        Model = item.Model,
                        IsCancel = item.IsCancel,
                        IsDownload = item.IsDownload,
                        FCAT_ID = item.FCAT_ID,
                        FBA_ID = item.FBA_ID,
                        FMD_ID = item.FMD_ID,
                        LOC_ID = item.LOC_ID//20150113

                    });
                }

            }

        }

        private int getFMD(SMSMINI.DAL.SMSManage.SMSManageDataContext dc, string snOld)
        {
            //20210309 var q = dc.FFixedAsset_Station_Details.Where(t => t.SerialNumber == snOld).FirstOrDefault();
            //--21200309
            var q = (from d in dc.FFixedAsset_Station_Details
                     join p in dc.FFixedAsset_SerialNumber_Pools on d.SerialNumber equals p.SerialNumber
                     join m in dc.FFixedAsset_Models on p.FMD_ID equals m.FMD_ID
                     join b in dc.FFixedAsset_Brands on m.FBA_ID equals b.FBA_ID
                     join c in dc.FFixedAsset_Categories on b.FCAT_ID equals c.FCAT_ID
                     where p.SerialNumber == snOld
                     select m).FirstOrDefault();
            //----------
            if (q == null)
                return 0;
            else
                return q.FMD_ID;
        }

        private int getFBA(SMSMINI.DAL.SMSManage.SMSManageDataContext dc, string snOld)
        {
            //20210309 var q = dc.FFixedAsset_Station_Details.Where(t => t.SerialNumber == snOld).FirstOrDefault();
            //--21200309
            var q = (from d in dc.FFixedAsset_Station_Details
                     join p in dc.FFixedAsset_SerialNumber_Pools on d.SerialNumber equals p.SerialNumber
                     join m in dc.FFixedAsset_Models on p.FMD_ID equals m.FMD_ID
                     join b in dc.FFixedAsset_Brands on m.FBA_ID equals b.FBA_ID
                     join c in dc.FFixedAsset_Categories on b.FCAT_ID equals c.FCAT_ID
                     where p.SerialNumber == snOld
                     select b).FirstOrDefault();
            //----------
            if (q == null)
                return 0;
            else
                return q.FBA_ID;
        }

        private int getCAT(SMSMINI.DAL.SMSManage.SMSManageDataContext dc, string snOld)
        {
            //20210309 var q = dc.FFixedAsset_Station_Details.Where(t => t.SerialNumber == snOld).FirstOrDefault();
            //--21200309
            var q = (from d in dc.FFixedAsset_Station_Details
                     join p in dc.FFixedAsset_SerialNumber_Pools on d.SerialNumber equals p.SerialNumber
                     join m in dc.FFixedAsset_Models on p.FMD_ID equals m.FMD_ID
                     join b in dc.FFixedAsset_Brands on m.FBA_ID equals b.FBA_ID
                     join c in dc.FFixedAsset_Categories on b.FCAT_ID equals c.FCAT_ID
                     where p.SerialNumber == snOld
                     select c).FirstOrDefault();
            //----------
            if (q == null)
                return 0;
            else
                return q.FCAT_ID;
        }



        public string IsUploadFFA()
        {
            string snSta = string.Empty;
            //isUploadFFA = true := สามารถ Upload FFA ได้
            //isUploadFFA = false := ไม่สามารถ Upload FFA ได้, ไม่สามารถ Upload JOB ได้

            if (lsJOB_Failure.Count() > 0)
            {
                strConn = Properties.Settings.Default["ServicesMSDBConnectionString"].ToString();
                using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn))
                {
                    foreach (var item in lsJOB_Failure)
                    {

                        var _st = dc.JOBs.Where(t => t.JOB_ID == item.JOB_ID).FirstOrDefault();
                        if (_st != null)
                        {
                            var _sn = dc.FFixedAsset_SerialNumber_Trackings
                                .Where(t => t.STA_ID_New == _st.STA_ID &&
                                    t.SerialNumber_New == item.SerialNumber_New)
                                .OrderByDescending(t => t.TrackingDate)
                                .FirstOrDefault();

                            if (_sn != null)
                            {
                                if (_sn.STA_ID_Old != _st.STA_ID)
                                {

                                    snSta += " S/N:" + _sn.SerialNumber_New;
                                    var j = dc.FFixedAsset_Station_Details.Where(t => t.SerialNumber == _sn.SerialNumber_New).FirstOrDefault();
                                    if (j != null)
                                    {
                                        snSta += " STA_ID =" + j.STA_ID + " S/N:" + j.SerialNumber;
                                    }
                                    //return snSta;
                                }
                            }
                            //else
                            //    return string.Empty;

                        }
                        //else
                        //    return string.Empty;
                    }
                }
            }

            return string.Empty;
        }

        public void UploadFFAData()
        {
            if (lsJOB_Failure.Count() > 0)
            {
                string _msg = "";
                TransactionOptions options = new TransactionOptions();
                options.Timeout = TimeSpan.FromMinutes(10);
                options.IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted;

                try
                {
                    using (TransactionScope trans = new TransactionScope(TransactionScopeOption.Suppress, options))
                    {
                        strConn = Properties.Settings.Default["ServicesMSDBConnectionString"].ToString();
                        using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn))
                        {
                            string stOld = "00000000";
                            string stNew = "00000000";
                            string vanID = "0000";


                            ////==20190516
                            //var srv = dc
                            //         .ExecuteQuery<DateTime>("SELECT GETDATE()")
                            //         .First();
                            //if (srv != null)
                            //{
                            //    servDate = srv;
                            //}
                            ////==========

                            foreach (var item in lsJOB_Failure)
                            {
                                DateTime getdate = dc
                                    .ExecuteQuery<DateTime>("select getdate() ")
                                    .FirstOrDefault();

                                #region FFixedAsset_SerialNumber_Tracking

                                //ดึงข้อมูลสน. เก่า (STA_ID_Old)
                                _msg = "ดึงข้อมูลสน. เก่า (STA_ID_Old)";
                                string _jobID = item.JOB_ID;

                                var staold = dc.JOB_Failure_Details
                                    .Where(t => t.SerialNumber_New == item.SerialNumber_New)
                                    .OrderByDescending(t => t.trandate).FirstOrDefault();

                                if (staold != null)
                                    _jobID = staold.JOB_ID;
                                var _staold = dc.JOBs.Where(t => t.JOB_ID == _jobID).FirstOrDefault();

                                if (_staold != null)
                                    stOld = _staold.STA_ID;
                                //string stOld = string.Empty;
                                //string stNew = string.Empty;

                                //ดึงข้อมูลสน. ใหม่ (STA_ID_New)
                                _msg = "ดึงข้อมูลสน. ใหม่ (STA_ID_New)";
                                var _staNew = dc.JOBs
                                    .Where(t => t.JOB_ID == item.JOB_ID)
                                    .FirstOrDefault();

                                if (_staNew != null)
                                {
                                    stNew = _staNew.STA_ID;
                                    vanID = _staNew.EMP_ID3;
                                }


                                //if (item.FCAT_ID != 0)//20150114
                                //{
                                string _traStatus = "12";//มีการเปลี่ยน SN
                                if (item.SerialNumber == item.SerialNumber_New)
                                    _traStatus = "11";//ไม่มีการเปลี่ยน SN

                                var snMas = dc.FFixedAsset_SerialNumber_Pools
                                    .Where(t => t.SerialNumber == item.SerialNumber ||
                                        t.SerialNumber == item.SerialNumber_New).FirstOrDefault();
                                if (snMas == null)
                                    _traStatus = "88";//เปลี่ยน SN ที่ไม่อยู่ใน SN Master


                                if ((dc.FFixedAsset_SerialNumber_Trackings
                                    .Where(t =>
                                        t.SerialNumber_Old == item.SerialNumber &&
                                        t.SerialNumber_New == item.SerialNumber_New &&
                                        t.JOB_ID == item.JOB_ID &&
                                        t.STA_ID_Old == stOld &&
                                        t.STA_ID_New == stNew
                                    )).Count() <= 0)
                                {
                                    var newSNTrack = new DAL.SMSManage.FFixedAsset_SerialNumber_Tracking
                                    {
                                        TrackingDate = getdate,
                                        SerialNumber_Old = item.SerialNumber,
                                        SerialNumber_New = item.SerialNumber_New,
                                        JOB_ID = item.JOB_ID,
                                        STA_ID_Old = stOld,
                                        STA_ID_New = stNew,
                                        VAN_ID = vanID,
                                        SerialNumber_Status = _traStatus,//"12",
                                    };

                                    _msg = "Insert New S/N Trackings";
                                    dc.FFixedAsset_SerialNumber_Trackings.InsertOnSubmit(newSNTrack);
                                    dc.SubmitChanges();

                                }

                                //}//if cat_id
                                #endregion

                                //ถ้ามีการเปลี่ยน Serial
                                if (item.SerialNumber != item.SerialNumber_New)
                                {

                                    #region Delete SN Old

                                    int catID = item.FCAT_ID.Value;
                                    int fbaID = item.FBA_ID.Value;
                                    int fmdID = item.FMD_ID.Value;
                                    string staID = stOld;

                                    //20150113
                                    int fLoc_id = item.LOC_ID.Value;

                                    //1. Delete SN Old 
                                    _msg = "Delete SN Old";
                                    var snOld = dc.FFixedAsset_Station_Details
                                        .Where(t => t.SerialNumber == item.SerialNumber)
                                        .FirstOrDefault();

                                    if (snOld != null)
                                    {
                                        if (catID != 0)//20150114
                                        {

                                            //==20190516
                                            var ld = new DAL.SMSManage.Log_DeleteDatabyProgram
                                            {
                                                Trandate = getdate,
                                                DataID = item.SerialNumber,//txtJOB_ID.Text.Trim(),
                                                TableName = "FFixedAsset_Station_Detail",
                                                Menu = "Upload JOB ",
                                                Detail = "Move SN " + item.SerialNumber + " STA_ID " + snOld.STA_ID + " to " + stNew + " JOB " + item.JOB_ID,
                                                UserID = UserInfo.UserId,
                                                Apps = "SMSMINI"

                                            };

                                            dc.Log_DeleteDatabyPrograms.InsertOnSubmit(ld);
                                            dc.SubmitChanges();


                                            //select หาข้อมูลว่ามีอยู่จริงที่ที่สถานีเก่า
                                            //var vchkD = dc.FFixedAsset_Station_Details
                                            //   .Where(t => t.SerialNumber == item.SerialNumber &&
                                            //           t.FCAT_ID == catID &&
                                            //           t.FMD_ID == item2.FMD_ID
                                            //           && t.STA_ID != item2.STA_ID)
                                            //   .FirstOrDefault();

                                            //if (vchkD != null)
                                            //{




                                            /*20210308 
                                                 ////เก็บ log Fixed Asset ของสถานีเก่า
                                                 var newLog = new DAL.SMSManage.FFixedAsset_Station_Detail_logMove
                                                 {

                                                     SerialNumber = snOld.SerialNumber,
                                                     FCAT_ID = snOld.FCAT_ID,
                                                     FBA_ID = snOld.FBA_ID,
                                                     FMD_ID = snOld.FMD_ID,
                                                     STA_ID = snOld.STA_ID,
                                                     OwnerShip = snOld.OwnerShip,
                                                     InstallDate = snOld.InstallDate,
                                                     WarrantDate = snOld.WarrantDate,
                                                     Location = snOld.Location,
                                                     LOC_ID = snOld.LOC_ID,
                                                     Detail = snOld.Detail,
                                                     TranDate = snOld.TranDate,
                                                     PartType1 = snOld.PartType1,
                                                     PartType2 = snOld.PartType2,
                                                     PartType3 = snOld.PartType3,
                                                     CheckGauge = snOld.CheckGauge,
                                                     TranEMP = snOld.TranEMP,
                                                     ControlFixAssetNo = snOld.ControlFixAssetNo,
                                                     IsConfirm = snOld.IsConfirm,
                                                     ConfirmEMP = snOld.ConfirmEMP,
                                                     ConfirmDate = snOld.ConfirmDate,
                                                     STA_ID_MoveTo = stNew,
                                                     TranDate_MoveTo = getdate,
                                                     TranEMP_MoveTo = UserInfo.UserId
                                                 };

                                                 dc.FFixedAsset_Station_Detail_logMoves.InsertOnSubmit(newLog);
                                                 dc.SubmitChanges();
                                             //}

                                            //==========
                                            */
                                            dc.FFixedAsset_Station_Details.DeleteOnSubmit(snOld);
                                            dc.SubmitChanges();

                                        }
                                    }

                                    #region Insert SN New/reduce SN_Old in table FFixedAsset_Station_Categorie

                                    //2.ลบจำนวน Cat ใน FFixedAsset_Station_Category
                                    var _catQtyNew = dc
                                        .FFixedAsset_Station_Categories
                                        //20150113.Where(t => t.FCAT_ID == catID && t.STA_ID == staID)
                                        .Where(t => t.FCAT_ID == catID && t.STA_ID == stNew)
                                        .FirstOrDefault();

                                    if (_catQtyNew == null)
                                    {
                                        if (catID != 0)
                                        {
                                            var newCat = new DAL.SMSManage.FFixedAsset_Station_Category()
                                            {
                                                FCAT_ID = catID,
                                                //20150113 STA_ID = staID,
                                                STA_ID = stNew,
                                                FQuantity = 1
                                            };
                                            dc.FFixedAsset_Station_Categories.InsertOnSubmit(newCat);
                                            dc.SubmitChanges();
                                        }
                                    }
                                    else
                                    {
                                        _catQtyNew.FQuantity = _catQtyNew.FQuantity + 1;
                                        dc.SubmitChanges();
                                    }

                                    _msg = " Insert SN New";

                                    if (catID != 0)
                                    {
                                        if ((dc.FFixedAsset_Station_Details
                                            .Where(t => t.SerialNumber == item.SerialNumber_New &&
                                           /*20210308 t.FCAT_ID == catID &&
                                           t.FBA_ID == fbaID &&
                                           t.FMD_ID == fmdID &&*/
                                           //20150113 t.STA_ID == staID)).Count() <= 0)
                                           t.STA_ID == stNew)).Count() <= 0)
                                        {
                                            var snNew = new DAL.SMSManage.FFixedAsset_Station_Detail()
                                            {
                                                SerialNumber = item.SerialNumber_New,
                                                /*20210308  FCAT_ID = catID,
                                                 FBA_ID = fbaID,
                                                 FMD_ID = fmdID, */
                                                //20150113 STA_ID = staID,
                                                STA_ID = stNew,
                                                OwnerShip = "FLO",
                                                InstallDate = getdate,
                                                LOC_ID = fLoc_id,//20150113 

                                            };

                                            dc.FFixedAsset_Station_Details.InsertOnSubmit(snNew);
                                            dc.SubmitChanges();



                                        }
                                    }

                                    #endregion

                                    //2. ลบจำนวน Cat ใน FFixedAsset_Station_Category
                                    var _catQty = dc.FFixedAsset_Station_Categories
                                        .Where(t => t.FCAT_ID == catID && t.STA_ID == staID)
                                        .FirstOrDefault();

                                    if (_catQty != null)
                                    {
                                        _catQty.FQuantity = _catQty.FQuantity - 1;
                                        dc.SubmitChanges();
                                    }


                                    #endregion


                                    #region Update SN Old รอซ่อม
                                    ////_msg = "Update SN Old รอซ่อม";

                                    ////var snOldPool = dc.FFixedAsset_SerialNumber_Pools
                                    ////    .Where(t => t.SerialNumber == item.SerialNumber)
                                    ////    .FirstOrDefault();

                                    ////if (snOldPool != null)
                                    ////{
                                    ////    snOldPool.StatusUse = "12";//รอซ่อม
                                    ////    dc.SubmitChanges();

                                    ////}

                                    //20150113 ก่อนตรวจสอบเรื่อง ตู้จ่าย
                                    var qpool2 = dc.FFixedAsset_SerialNumber_Pools
                                       .Where(t => t.SerialNumber == item.SerialNumber).FirstOrDefault();

                                    if (qpool2 != null)
                                    {
                                        //20140814 ก่อนตรวจสอบเรื่อง ตู้จ่าย
                                        //qpool2.StatusUse = "12";//รอซ่อม
                                        //db.SubmitChanges();        


                                        if (catID != 0)//20150127
                                        {
                                            var chkpool = ((from p in dc.FFixedAsset_SerialNumber_Pools
                                                            join m in dc.FFixedAsset_Models on p.FMD_ID equals m.FMD_ID
                                                            join b in dc.FFixedAsset_Brands on m.FBA_ID equals b.FBA_ID
                                                            where m.FMD_ID.ToString().Contains(qpool2.FMD_ID.ToString())
                                                            && p.SerialNumber == qpool2.SerialNumber
                                                            && (b.FBA_ID.ToString() == "1"
                                                            || b.FBA_ID.ToString() == "2"
                                                            || b.FBA_ID.ToString() == "3"
                                                            || b.FBA_ID.ToString() == "4")
                                                            select p).FirstOrDefault());

                                            if (chkpool != null)
                                            {
                                                chkpool.StatusUse = "00";//พร้อมใช้
                                                dc.SubmitChanges();
                                            }
                                            else
                                            {
                                                qpool2.StatusUse = "12";//รอซ่อม
                                                dc.SubmitChanges();
                                            }
                                            //20150127} if (qpool2 != null)


                                            #endregion

                                            #region Update SN new to use 1
                                            _msg = "Update SN new to use1";
                                            var snNewPool = dc.FFixedAsset_SerialNumber_Pools
                                               .Where(t => t.SerialNumber == item.SerialNumber_New)
                                               .FirstOrDefault();

                                            if (snNewPool != null)
                                            {
                                                snNewPool.StatusUse = "11";//ใช้แล้ว
                                                dc.SubmitChanges();

                                            }
                                            #endregion
                                        }//catID !=0//20150127
                                    }// if (qpool2 != null)//20150127



                                }
                                else
                                {
                                    #region Update SN new to use 2
                                    _msg = "Update SN new to use2";
                                    var snNewPool = dc.FFixedAsset_SerialNumber_Pools
                                       .Where(t => t.SerialNumber == item.SerialNumber_New)
                                       .FirstOrDefault();

                                    if (snNewPool != null)
                                    {
                                        snNewPool.StatusUse = "11";//ใช้แล้ว
                                        dc.SubmitChanges();

                                    }
                                    #endregion
                                }

                            }


                        }
                        trans.Complete();
                    }

                }//   if (lsJOB_Failure.Count() > 0)
                catch (Exception ex)
                {

                    System.Windows.MessageBox.Show("Error Step " + _msg + Environment.NewLine +
                        "Message Error " + ex.Message);
                }

            }//using (TransactionScope trans = new TransactionScope()) //if (lsJOB_Failure.Count() > 0)
        }// public void UploadFFAData()

    }
}
