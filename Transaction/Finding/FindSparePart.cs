﻿using SMSMINI.DAL.SMSManage;
using System;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace SMSMINI.Transaction.Finding
{
    public partial class FindSparePart : Form
    {
        private string tmpProjectNo = "";
        private string tmpPartNo = "";
        private string tmpPartName = "";
        private string tmpParameter = "";

        private DateTime dOpenJOB;
        private string tmpGopID = "";


        public string TmpProjectNo { get { return tmpProjectNo; } set { tmpProjectNo = value; } }
        public string TmpPartNo { get { return tmpPartNo; } set { tmpPartNo = value; } }
        public string TmpPartName { get { return tmpPartName; } set { tmpPartName = value; } }
        public string TmpParameter { get { return tmpParameter; } set { tmpParameter = value; } }

        public DateTime TmpDopenJOB { get { return dOpenJOB; } set { dOpenJOB = value; } }
        public string TmpGopID { get { return tmpGopID; } set { tmpGopID = value; } }

        public string tmpJobType { get; set; }
        public string tmpTYP_ID1 { get; set; }

        public string TmpGopName { get; set; }

        private string strConn = "";

        private string strPartNOPl = string.Empty;

        public string tmpContractNo { get; set; }

        public string SO_NO { get; set; }
        public string JOBNo { get; set; }
        public int ERP_orderline_id { get; set; }

        public FindSparePart()
        {
            InitializeComponent();
            object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
            if (attributes.Length == 0)
            {
                this.Text = "SMS MINI [v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "]";

            }
            this.Text = ((AssemblyProductAttribute)attributes[0]).Product + " [v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "]";


        }

        private void FindSparePart_Load(object sender, EventArgs e)
        {

            var strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;

            try
            {
                using (SMSManageDataContext db = new SMSManageDataContext(strConn))
                {

                    var q = db.SpareParts
                        .Where(t =>
                               (t.SPA_ID.Contains(tmpParameter.Trim()) || t.SparePart1.Contains(tmpParameter.Trim())) &&
                               (t.IsDelete == false || t.IsDelete == null)
                              )
                            .Select(t => new
                            {
                                PartNo = t.SPA_ID,
                                PartName = t.SparePart1,
                                t.PricePerUnit,
                                ERP_orderline_id = 0
                            })
                            .ToList();

                    if (q.Count() > 0)
                    {
                        dataGridView1.DataSource = q;
                    }
                    else
                    {
                        MessageBox.Show("ไม่มีข้อมูล Part  [" + TmpParameter + "] กรุณาตรวจสอบ " + Environment.NewLine +
                            "เช่น:" + Environment.NewLine +
                            "   - ป้อนชื่อ Part (ให้ครบถ้น)" + Environment.NewLine +
                            "แล้วค้นหาอีกครั้ง" + Environment.NewLine +
                            "ถ้ายังไม่มีข้อมูล กรุณาติดต่อ ผู้จัดการส่วน ของคุณ", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }


            }
            catch (System.Exception ex)
            {
                MessageBox.Show("เกิดข้อผิดพลาด..." + Environment.NewLine + ex.Message, "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
             
        }

        private void getSpareParts_IsPrices(SMSManageDataContext db)
        {
            ////var q = db.SpareParts_IsPrices
            ////    .Where(t => t.SPA_ID.Contains(tmpParameter.Trim()) &&
            ////    t.GOP_ID == tmpGopID);

            ////if (q.Count() > 0)
            ////{
            ////    var q1 = (from t in db.SpareParts
            ////              from p in db.SpareParts_IsPrices
            ////              where (t.SPA_ID == p.SPA_ID) && (p.GOP_ID == tmpGopID) &&
            ////                  (t.SPA_ID.Contains(tmpParameter.Trim()) || t.SparePart1.Contains(tmpParameter.Trim())) &&
            ////                  (t.IsDelete == false || t.IsDelete == null)
            ////              select new
            ////              {
            ////                  PartNo = t.SPA_ID,
            ////                  PartName = t.SparePart1,
            ////                  t.PricePerUnit,
            ////                  ERP_orderline_id = 0
            ////              }).Distinct();

            ////    dataGridView1.DataSource = q1.ToList();
            ////    dataGridView1.Refresh();
            ////}
            ////else
            ////{
            ////    var q1 = (from t in db.SpareParts
            ////              where
            ////                  (t.SPA_ID.Contains(tmpParameter.Trim()) || t.SparePart1.Contains(tmpParameter.Trim())) &&
            ////                  (t.IsDelete == false || t.IsDelete == null)
            ////              select new
            ////              {
            ////                  PartNo = t.SPA_ID,
            ////                  PartName = t.SparePart1,
            ////                  t.PricePerUnit,
            ////                  ERP_orderline_id = 0
            ////              }).Distinct();

            ////    dataGridView1.DataSource = q1.ToList();
            ////    dataGridView1.Refresh();
            ////}



            var q1 = (from t in db.SpareParts
                      where
                          (t.SPA_ID.Contains(tmpParameter.Trim()) || t.SparePart1.Contains(tmpParameter.Trim())) &&
                          (t.IsDelete == false || t.IsDelete == null)
                      select new
                      {
                          PartNo = t.SPA_ID,
                          PartName = t.SparePart1,
                          t.PricePerUnit,
                          ERP_orderline_id = 0
                      }).Distinct();

            dataGridView1.DataSource = q1.ToList();
            dataGridView1.Refresh();

        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex != -1)
                {
                    tmpPartNo = dataGridView1["PartNo", e.RowIndex].Value.ToString();
                    tmpPartName = dataGridView1["PartName", e.RowIndex].Value.ToString();
                    ERP_orderline_id = int.Parse(dataGridView1["ERP_orderline_id", e.RowIndex].Value.ToString());
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("เกิดข้อผิดพลาด " + ex.Message, "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btDownloadPart_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.AppStarting;

                MasterData.Part f = new SMSMINI.MasterData.Part();
                f.ShowDialog();


                this.DialogResult = DialogResult.Abort;
                this.Close();

                Cursor.Current = Cursors.Default;
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("เกิดผิดพลาดในการใช้งานเมนู Upload Job Error " + ex.Message, "ข้อผิดพลาด");
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                string tmpSpareID = dataGridView1["PartNo", e.RowIndex].Value.ToString();
                strPartNOPl = tmpSpareID;
                 

                var strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;



                using (DAL.SMSManage.SMSManageDataContext db = new DAL.SMSManage.SMSManageDataContext(strConn))
                {

                    if (!string.IsNullOrEmpty(tmpContractNo))
                    {

                        #region  tmpContractNo !=""

                        var conP = (from t in db.Conf_Conditions
                                    from sp in db.SpareParts
                                    where (t.SPA_ID == sp.SPA_ID &&
                                        t.SPA_ID == tmpSpareID &&
                                        t.GOP_ID == tmpGopID &&
                                        t.TYP_ID == tmpJobType &&
                                        t.TYP_ID1 == tmpTYP_ID1) &&
                                        t.ContractNo.Contains(tmpContractNo)
                                    select new
                                    {
                                        PartNo = sp.SPA_ID,
                                        PartName = sp.SparePart1,
                                        PricesList = t.PriceList
                                    }).FirstOrDefault();

                        if (conP != null && conP.PricesList > 0)
                        {
                            var conP2 = (from t in db.Conf_Conditions
                                         from sp in db.SpareParts
                                         where (t.SPA_ID == sp.SPA_ID &&
                                             t.SPA_ID == tmpSpareID &&
                                             t.GOP_ID == tmpGopID &&
                                             t.TYP_ID == tmpJobType &&
                                             t.TYP_ID1 == tmpTYP_ID1) &&
                                             t.ContractNo.Contains(tmpContractNo)
                                         select new
                                         {
                                             PartNo = sp.SPA_ID,
                                             PartName = sp.SparePart1,
                                             PricesList = t.PriceList,
                                             ContractNo = t.ContractNo
                                         }).Distinct();
                            if (conP2 != null)
                            {
                                dataGridView2.DataSource = conP2.ToList();
                                dataGridView2.Refresh();
                            }




                        }
                        

                        #endregion

                    }
                    else
                    {

                        #region  tmpContractNo ==""

                        var conP = (from t in db.Conf_Conditions
                                    from sp in db.SpareParts
                                    where (t.SPA_ID == sp.SPA_ID &&
                                        t.SPA_ID == tmpSpareID &&
                                        t.GOP_ID == tmpGopID &&
                                        t.TYP_ID == tmpJobType &&
                                        t.TYP_ID1 == tmpTYP_ID1)
                                    select new
                                    {
                                        PartNo = sp.SPA_ID,
                                        PartName = sp.SparePart1,
                                        PricesList = t.PriceList
                                    }).FirstOrDefault();

                        if (conP != null && conP.PricesList > 0)
                        {
                            var conP2 = (from t in db.Conf_Conditions
                                         from sp in db.SpareParts
                                         where (t.SPA_ID == sp.SPA_ID &&
                                             t.SPA_ID == tmpSpareID &&
                                             t.GOP_ID == tmpGopID &&
                                             t.TYP_ID == tmpJobType &&
                                             t.TYP_ID1 == tmpTYP_ID1)
                                         select new
                                         {
                                             PartNo = sp.SPA_ID,
                                             PartName = sp.SparePart1,
                                             PricesList = t.PriceList,
                                             ContractNo = t.ContractNo
                                         }).Distinct();
                            if (conP2 != null)
                            {
                                dataGridView2.DataSource = conP2.ToList();
                                dataGridView2.Refresh();
                            }




                        }
                        

                        #endregion
                    }

                }

            }
             
        }

        
    }


}
