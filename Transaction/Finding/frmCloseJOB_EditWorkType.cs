﻿using SMSMINI.DAL.SMSManage;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace SMSMINI.Transaction.Finding
{
    public partial class frmCloseJOB_EditWorkType : Form
    {

        public string strConn { get; set; }
        public string jobID { get; set; }


        string _workID = "";

        public frmCloseJOB_EditWorkType()
        {
            InitializeComponent();
        }

        private void toolStripButton_Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmCloseJOB_EditWorkType_Load(object sender, EventArgs e)
        {
            using (SMSManageDataContext dc = new SMSManageDataContext(strConn))
            {
                var job = dc.JOBs.Where(t => t.JOB_ID == jobID).FirstOrDefault();



                if (job != null)
                {
                    var _workType = dc.Types.Where(t => t.TYP_ID == job.TYP_ID1).FirstOrDefault();

                    toolStripTextBox_ContractNo.Text = _workType.TYP_ID + ": " + _workType.Type1;

                    var _type = dc.Types.Where(t => t.StatType == "04").Select(t => new
                    {
                        ID = t.TYP_ID,
                        Type = t.TYP_ID + ": " + t.Type1
                    }).ToList();


                    dataGridView1.DataSource = _type;


                }


            }
        }

        private void toolStripButton_Save_Click(object sender, EventArgs e)
        {
            if (txtNewContracNo.Text != "")
            {
                using (SMSManageDataContext dc = new SMSManageDataContext(strConn))
                {
                    var j = dc.JOBs.Where(t => t.JOB_ID == jobID).FirstOrDefault();
                    if (j != null)
                    {
                        j.TYP_ID1 = _workID;
                        dc.SubmitChanges();

                        var sql_log = "";
                        sql_log = "Insert into [LogSystem] ([TableName] ,[DataID] ,[Detail] ,[UserID] ,[IPAddress]) " +
                            " values ('JOB'  ,'" + jobID.Trim() + "' ,' แก้ไขประเภทงาน [" + toolStripTextBox_ContractNo.Text.Trim() + " => " + txtNewContracNo.Text.Trim() + "]' ,'" + UserInfo.UserId + "' ,CONVERT(char(15), CONNECTIONPROPERTY('client_net_address')))";


                        dc.ExecuteCommand(sql_log);


                        MessageBox.Show("แก้ไข ประเภทงาน เรียบร้อย...", "ผลการทำงาน", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        this.DialogResult = DialogResult.OK;
                        this.Close();

                    }
                }
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                if (dataGridView1["ID", e.RowIndex].Value.ToString() != null)
                {
                    _workID = dataGridView1["ID", e.RowIndex].Value.ToString();
                    txtNewContracNo.Text = dataGridView1["Type", e.RowIndex].Value.ToString();

                }
            }
        }
    }
}
