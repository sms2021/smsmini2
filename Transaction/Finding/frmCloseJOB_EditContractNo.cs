﻿using SMSMINI.DAL.SMSManage;

using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace SMSMINI.Transaction.Finding
{
    public partial class frmCloseJOB_EditContractNo : Form
    {

        public string strConn { get; set; }
        public string jobID { get; set; }

        public frmCloseJOB_EditContractNo()
        {
            InitializeComponent();
        }

        private void toolStripButton_Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void toolStripButton_Save_Click(object sender, EventArgs e)
        {
            if (txtNewContracNo.Text != "")
            {
                using (SMSManageDataContext dc = new SMSManageDataContext(strConn))
                {
                    var j = dc.JOBs.Where(t => t.JOB_ID == jobID).FirstOrDefault();
                    if (j != null)
                    {
                        j.ContractNo = txtNewContracNo.Text.Trim();
                        dc.SubmitChanges();

                        var sql_log = "";
                        sql_log = "Insert into [LogSystem] ([TableName] ,[DataID] ,[Detail] ,[UserID] ,[IPAddress]) " +
                        //20210524 " values ('JOB'  ,'" + jobID.Trim() + "' ,'EditContractNo[" + toolStripTextBox_ContractNo + " => " + txtNewContracNo.Text.Trim() + "]' ,'" + UserInfo.UserId + "' ,CONVERT(char(15), CONNECTIONPROPERTY('client_net_address')))";
                        " values ('JOB'  ,'" + jobID.Trim() + "' ,'EditContractNo[" + toolStripTextBox_ContractNo.Text + " => " + txtNewContracNo.Text.Trim() + "]' ,'" + UserInfo.UserId + "' ,CONVERT(char(15), CONNECTIONPROPERTY('client_net_address')))";


                        dc.ExecuteCommand(sql_log);


                        MessageBox.Show("แก้ไขเลขที่สัญญาเรียบร้อย...", "ผลการทำงาน", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        this.DialogResult = DialogResult.OK;
                        this.Close();

                    }
                }
            }
        }

        private void frmCloseJOB_EditContractNo_Load(object sender, EventArgs e)
        {

            using (SMSManageDataContext dc = new SMSManageDataContext(strConn))
            {
                var job = dc.JOBs.Where(t => t.JOB_ID == jobID).FirstOrDefault();

                if (job != null)
                {
                    toolStripTextBox_ContractNo.Text = job.ContractNo;
                    var station = dc.Stations.Where(t => t.STA_ID == job.STA_ID).FirstOrDefault();
                    if (station != null)
                    {
                        /*var con = dc.Conf_Contracts
                            .Where(t => (//(
                                t.IsCancel == false && 
                                //20200828 t.EndDate.Year >= DateTime.Now.Year) 
                                t.EndDate.Date >= DateTime.Now.Date) 
                                && t.GOP_ID == station.GOP_ID)
                                //20200828|| t.ContractNo == job.ContractNo //ไม่งั้นเงื่อนไขการเช็ควันหมดอายุจะไม่เป็นจริง
                                //)
                            .Select(t => new { t.ContractNo, t.ContractNo_Cus, t.StartDate, t.EndDate, t.Detail }).ToList();

                        dataGridView1.DataSource = con;*/

                        DateTime? srvTimeToComp = null;

                        srvTimeToComp = dc
                               .ExecuteQuery<DateTime>("SELECT GETDATE()")
                               .First();
                        var tmpdatecurr = srvTimeToComp.Value;


                        if (new string[] { "1", "2", "3" }.Contains(station.xContract.ToString()))
                        {
                            var qContPop = from t in dc.Conf_Contracts
                                           where t.GOP_ID == station.GOP_ID &&
                                          (t.STA_ID.StartsWith("0000") || t.STA_ID == station.STA_ID) &&
                                            (
                                                t.EndDate.Date >= tmpdatecurr.Date
                                            ) &&
                                           (t.IsCancel == false || t.IsCancel == null)
                                           && t.TYP_ID == job.TYP_ID

                                           select new
                                           {
                                               รายละเอียด = t.Detail,
                                               ContractNo = t.ContractNo,
                                               เลขที่สัญญา_ลูกค้า = t.ContractNo_Cus,
                                               //20180326 รายละเอียด = t.Detail,
                                               t.GOP_ID,
                                               กลุ่มบริษัท = t.GOP_ID + " " + (t.Station_Group.GroupStation ?? ""),
                                               t.StartDate,
                                               t.EndDate,
                                               //20201005
                                               ประเภทสัญญา = t.ContractType == 0 ? "สัญญาเหมาจ่าย" : "สัญญา MA",
                                               //========
                                               สถานะการยกเลิก = t.IsCancel == true ? "ยกเลิก" : "ใช้งาน"
                                           };
                            if (qContPop.Count() > 0)
                            {
                                dataGridView1.DataSource = qContPop.ToList();
                            }
                            else
                            {
                                DialogResult = DialogResult.Cancel;
                            }
                        }
                        else
                        {
                            var qContr = (from t in dc.Conf_Contract_IsPopups
                                          where t.GOP_ID == station.GOP_ID
                                          select t).FirstOrDefault();

                            if (qContr != null)
                            {
                                var qContPop = from t in dc.Conf_Contracts
                                               where t.GOP_ID == station.GOP_ID &&
                                              (t.STA_ID.StartsWith("0000") || t.STA_ID == station.STA_ID) &&
                                                (
                                                    t.EndDate.Date >= tmpdatecurr.Date
                                                ) &&
                                               (t.IsCancel == false || t.IsCancel == null)
                                               && t.TYP_ID == job.TYP_ID


                                               select new
                                               {
                                                   รายละเอียด = t.Detail,
                                                   ContractNo = t.ContractNo,
                                                   เลขที่สัญญา_ลูกค้า = t.ContractNo_Cus,
                                                   //20180326 รายละเอียด = t.Detail,
                                                   t.GOP_ID,
                                                   กลุ่มบริษัท = t.GOP_ID + " " + (t.Station_Group.GroupStation ?? ""),
                                                   t.StartDate,
                                                   t.EndDate,
                                                   //20201005
                                                   ประเภทสัญญา = t.ContractType == 0 ? "สัญญาเหมาจ่าย" : "สัญญา MA",
                                                   //========
                                                   สถานะการยกเลิก = t.IsCancel == true ? "ยกเลิก" : "ใช้งาน"
                                               };
                                if (qContPop.Count() > 0)
                                {
                                    dataGridView1.DataSource = qContPop.ToList();
                                }
                                else
                                {
                                    DialogResult = DialogResult.Cancel;
                                }
                            }
                            else
                            {

                                var qContPop = from t in dc.Conf_Contracts
                                               where t.GOP_ID == station.GOP_ID &&
                                              (t.STA_ID == station.STA_ID) &&
                                               (
                                                    t.EndDate.Date >= tmpdatecurr.Date
                                                ) &&
                                               (t.IsCancel == false || t.IsCancel == null)
                                               && t.TYP_ID == job.TYP_ID


                                               select new
                                               {
                                                   รายละเอียด = t.Detail,
                                                   ContractNo = t.ContractNo,
                                                   เลขที่สัญญา_ลูกค้า = t.ContractNo_Cus,
                                                   //20180326 รายละเอียด = t.Detail,
                                                   t.GOP_ID,
                                                   กลุ่มบริษัท = t.GOP_ID + " " + (t.Station_Group.GroupStation ?? ""),
                                                   t.StartDate,
                                                   t.EndDate,
                                                   //20201005
                                                   ประเภทสัญญา = t.ContractType == 0 ? "สัญญาเหมาจ่าย" : "สัญญา MA",
                                                   //========
                                                   สถานะการยกเลิก = t.IsCancel == true ? "ยกเลิก" : "ใช้งาน"
                                               };
                                if (qContPop.Count() > 0)
                                {
                                    dataGridView1.DataSource = qContPop.ToList();
                                }
                                else
                                {
                                    DialogResult = DialogResult.Cancel;
                                }
                            }

                        }

                    }

                }

            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                if (dataGridView1["ContractNo", e.RowIndex].Value.ToString() != null)
                {
                    txtNewContracNo.Text = dataGridView1["ContractNo", e.RowIndex].Value.ToString();

                }
            }
        }
    }
}
