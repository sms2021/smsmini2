﻿using System;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace SMSMINI.Transaction.Finding
{
    public partial class FindFailureCodeOnline : Form
    {
        private string tmpFCodeID = "";
        private string tmpFailureCode = "";

        private string tmpjobType = "";

        public string TmpFCodeID { get { return tmpFCodeID; } set { tmpFCodeID = value; } }
        public string TmpFailureCode { get { return tmpFailureCode; } set { tmpFailureCode = value; } }

        public string TmpjobType { get { return tmpjobType; } set { tmpjobType = value; } }

        string strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;

        public FindFailureCodeOnline()
        {
            InitializeComponent();
            object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
            if (attributes.Length == 0)
            {
                this.Text = "SMS MINI [v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "]";

            }
            this.Text = ((AssemblyProductAttribute)attributes[0]).Product + " [v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "]";

        }

        private void FindFailureCodeOnline_Load(object sender, EventArgs e)
        {
            string _jobtype = tmpjobType == "ตู้จ่าย(D)" ? "30" : "31";
            //strConn = ConnectionManager.GetConnectionString(UserInfo.ConnectMode);
            try
            {


                //using (SMSManageDataContext db = new SMSManageDataContext())
                using (DAL.SMSManage.SMSManageDataContext db = new DAL.SMSManage.SMSManageDataContext(strConn))
                {
                    if (_jobtype == "30")
                    {
                        var q = (from t in db.v_FailureCodes
                                 where new string[] { "00", "30" }.Contains(t.TYP_ID) &&
                                 (t.Failure_th.Contains(tmpFailureCode) || t.FAI_ID.Contains(tmpFailureCode))

                                 select new
                                 {
                                     FailureCode = t.FAI_ID,
                                     อาการเสีย = t.Failure_th,
                                     จุดเสีย = t.PointFailure
                                 }).Distinct();


                        dataGridView1.DataSource = q.ToList();
                        dataGridView1.Refresh();
                    }
                    else
                    {
                        var q = (from t in db.v_FailureCodes
                                 where new string[] { "00", "31", "37", "38" }.Contains(t.TYP_ID) &&
                                 (t.Failure_th.Contains(tmpFailureCode) || t.FAI_ID.Contains(tmpFailureCode))

                                 select new
                                 {
                                     FailureCode = t.FAI_ID,
                                     อาการเสีย = t.Failure_th,
                                     จุดเสีย = t.PointFailure
                                 }).Distinct();


                        dataGridView1.DataSource = q.ToList();
                        dataGridView1.Refresh();
                    }

                }

            }
            catch (System.Exception ex)
            {
                MessageBox.Show("เกิดข้อผิดพลาด..." + Environment.NewLine + ex.Message, "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex != -1)
                {
                    tmpFCodeID = dataGridView1["FailureCode", e.RowIndex].Value.ToString();
                    tmpFailureCode = dataGridView1["อาการเสีย", e.RowIndex].Value.ToString();
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("เกิดข้อผิดพลาด " + ex.Message, "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
