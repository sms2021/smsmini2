﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace SMSMINI.Transaction.Finding
{
    public partial class Find_SN : Form
    {
        DataTable dtSNNew = null;
        DataTable dtSNOld = null;
        public string SN_New { get; set; }
        public string SN_Old { get; set; }

        public string FANoNew { get; set; }
        public string FANoOld { get; set; }

        public int FCAT_ID { get; set; }
        public int FBA_ID { get; set; }
        public int FMD_ID { get; set; }
        public int CheckGauge { get; set; }

        public int LOC_ID { get; set; }

#pragma warning disable CS0108 // 'Find_SN.Location' hides inherited member 'Form.Location'. Use the new keyword if hiding was intended.
        public string Location { get; set; }
#pragma warning restore CS0108 // 'Find_SN.Location' hides inherited member 'Form.Location'. Use the new keyword if hiding was intended.
        public string MODEL { get; set; }
        public string STAID { get; set; }

        public bool _IsFound { get; set; }

        public DateTime WarrantDate { get; set; }

        public List<Transaction.SNCancel> lsSNSelec { get; set; }


        public int FCAT_ID_New { get; set; }//20180426

        public int chFCAT_ID { get; set; }//20180801

        //DateTime dt1 = DateTime.Parse("1/1/1990");
        //DateTime.Parse("1/1/1990");

        //DateTime dt1;
        //string dateString = "2/16/2008 12:15:12 PM";
        //dt1 = DateTime.Parse(dateString);

        DateTime dt1 = new DateTime(1990, 1, 1);//DateTime.Parse("01/01/1990 00:00:00");


        // DateTime vDateReg = Convert.ToDateTime(dateString.Value.ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("th-TH")));




        public Find_SN()
        {
            InitializeComponent();
        }

        string strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;

        private void Find_SN_Load(object sender, EventArgs e)
        {

            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn))
            {

                if (SN_New != null)
                {

                    setDtSNNew();
                    toolStripText_txtFind.Text = SN_New;//20141124
                    /* 20141202 เพิ่ม brandvar q = (from t in dc.FFixedAsset_SerialNumber_Pools
                               from c in dc.FFixedAsset_Categories                  
                               from m in dc.FFixedAsset_Models

                               where ( t.FMD_ID == m.FMD_ID) &&
                                       t.FCAT_ID == c.FCAT_ID &&
                                       t.SerialNumber.Contains(SN_New) &&
                                       (t.FCAT_ID == FCAT_ID) &&//SN New ต้องเป็น ประเภทเดียวกับ SN Old
                                       (new string[] { "00", "66", "76" }.Contains(t.StatusUse)) ||
                                       (t.StatusUse == null) //&& // Popup เฉพาะ SN ที่ยังไม่ได้ใช้งาน
                                       //(t.StatusUse == "01") &&
                                       //(!lsSNSelec.Select(sn => sn.SNNew).ToArray().Contains(t.SerialNumber))
                       */
                    var q = (from t in dc.FFixedAsset_SerialNumber_Pools
                             from c in dc.FFixedAsset_Categories
                             from m in dc.FFixedAsset_Models
                             from b in dc.FFixedAsset_Brands
                             where (m.FBA_ID == b.FBA_ID &&
                                     t.FCAT_ID == c.FCAT_ID &&
                                     t.FMD_ID == m.FMD_ID) &&
                                     t.SerialNumber.Contains(SN_New) &&
                                     (t.FCAT_ID == FCAT_ID) &&//SN New ต้องเป็น ประเภทเดียวกับ SN Old
                                     (new string[] { "00", "66", "76" }.Contains(t.StatusUse)) ||
                                     (t.StatusUse == null) //&& // Popup เฉพาะ SN ที่ยังไม่ได้ใช้งาน
                                                           //(t.StatusUse == "01") &&
                                                           //(!lsSNSelec.Select(sn => sn.SNNew).ToArray().Contains(t.SerialNumber)) 

                             select new
                             {
                                 Cate = c.FixedAsset_CateName,
                                 SNNew = t.SerialNumber,
                                 FFaNoNew = t.ControlFixAssetNo,
                                 Location = "",
                                 Model = m.FModel,
                                 Brand = b.FBrand,
                                 รับประกัน = t.ExpiryDate,
                                 t.Detail,
                                 t.StatusUse,
                                 CheckGauge = 0,
                                 FCAT_ID_New = c.FCAT_ID  //20180426
                             })
                            .Distinct();

                    if (q.Count() > 0)
                    {
                        /* dataGridView1.DataSource = q
                             .OrderBy(t => t.Cate)
                             .ToList();
                        */

                        DataRow dr = dtSNNew.NewRow();

                        //20180802
                        var chkForceF = dc.FFixedAsset_Categories
                                           .Where(t => t.FCAT_ID == FCAT_ID && t.IsForce == '1')
                                           .FirstOrDefault();

                        if (chkForceF == null)
                        {

                            dr["Cate"] = "";
                            dr["SNNew"] = "ไม่ระบุ...";
                            dr["FFaNoNew"] = "";
                            dr["Location"] = "";
                            dr["Model"] = "";

                            dr["Brand"] = "";
                            dr["รับประกัน"] = "";
                            dr["Detail"] = "";
                            dr["StatusUse"] = "";
                            dr["CheckGauge"] = "";
                            dr["FCAT_ID_New"] = 0;

                            dtSNNew.Rows.Add(dr);
                        }
                        foreach (var New in q)
                        {
                            DataRow dr1 = dtSNNew.NewRow();
                            dr1["Cate"] = New.Cate;
                            dr1["SNNew"] = New.SNNew;
                            dr1["FFaNoNew"] = New.FFaNoNew;
                            dr1["Location"] = New.Location;
                            dr1["Model"] = New.Model;

                            dr1["Brand"] = New.Brand;
                            dr1["รับประกัน"] = New.รับประกัน;
                            dr1["Detail"] = New.Detail;
                            dr1["StatusUse"] = New.StatusUse;

                            dr1["CheckGauge"] = New.CheckGauge;
                            dr1["FCAT_ID_New"] = New.FCAT_ID_New;   //20180426                          


                            dtSNNew.Rows.Add(dr1);
                        }
                        dataGridView1.DataSource = null;
                        dataGridView1.Refresh();
                        dataGridView1.DataSource = dtSNNew;
                        dataGridView1.Enabled = true;

                        //_IsFound = true;
                    }
                    else
                    {
                        if (FCAT_ID == 0)
                        {
                            DataRow dr = dtSNNew.NewRow();
                            //20180802
                            var chkForceF = dc.FFixedAsset_Categories
                                               .Where(t => t.FCAT_ID == chFCAT_ID && t.IsForce == '1')
                                               .FirstOrDefault();

                            if (chkForceF == null)
                            {
                                dr["Cate"] = "";
                                dr["SNNew"] = "ไม่ระบุ...";
                                dr["FFaNoNew"] = "";
                                dr["Location"] = "";
                                dr["Model"] = "";

                                dr["Brand"] = "";
                                dr["รับประกัน"] = "";
                                dr["Detail"] = "";
                                dr["StatusUse"] = "";
                                dr["CheckGauge"] = "";
                                dr["FCAT_ID_New"] = 0;


                                dtSNNew.Rows.Add(dr);
                            }
                            dataGridView1.DataSource = null;
                            dataGridView1.Refresh();
                            dataGridView1.DataSource = dtSNNew;
                            dataGridView1.Enabled = true;

                            //_IsFound = true;
                        }
                        else
                        {

                            MessageBox.Show("ไม่พบข้อมูล S/N New ตามที่ระบุ" + Environment.NewLine +
                                            "(ต้องป้อน S/N New ประเภทอุปกรณ์เดียวกับ S/N Old เท่านั้น...)" + Environment.NewLine +
                                            "กรุณาตรวจสอบ...", "ผลการตรวจสอบ", MessageBoxButtons.OK);
                            // _IsFound = false;
                            this.Close();

                        }
                    }
                }
                else if (SN_Old != null)
                {
                    setDtSNOld();

                    toolStripText_txtFind.Text = SN_Old;//20141124
                    var q = (from t in dc.FFixedAsset_Station_Details
                             from b in dc.FFixedAsset_Brands
                             from c in dc.FFixedAsset_Categories
                             from m in dc.FFixedAsset_Models

                             from ff in dc.FFixedAsset_SerialNumber_Pools

                             where (
                             /*20210309
                                    t.FBA_ID == b.FBA_ID &&
                                     t.FCAT_ID == c.FCAT_ID &&
                                     t.FMD_ID == m.FMD_ID) &&
                             */
                             //--20210309
                                     m.FBA_ID == b.FBA_ID &&
                                     b.FCAT_ID == c.FCAT_ID &&
                                    ff.FMD_ID == m.FMD_ID) &&
                             //-----
                                     t.SerialNumber.Contains(SN_Old) &&
                                     (t.STA_ID == STAID) &&
                                     (ff.SerialNumber == t.SerialNumber)
                             select new
                             {
                                 Cate = c.FixedAsset_CateName,
                                 SNOld = t.SerialNumber,
                                 FFaNoOld = ff.ControlFixAssetNo,
                                 LOC_ID = t.LOC_ID == null ? 0 : t.LOC_ID,
                                 Location = t.Location,
                                 Model = m.FModel,
                                 Brand = b.FBrand,
                                 รับประกัน = t.WarrantDate,
                                 t.Detail,
                                 StatusUse = "11",

                                 /* 20210309 FCAT_ID = t.FCAT_ID,
                                 FBA_ID = t.FBA_ID,
                                 FMD_ID = t.FMD_ID,*/
                                 //--20210309
                                 FCAT_ID = c.FCAT_ID,
                                 FBA_ID = b.FBA_ID,
                                 FMD_ID = m.FMD_ID,
                                 //---
                                 CheckGauge = t.CheckGauge == null ? 0 : t.CheckGauge
                             })
                            .Distinct();

                    if (q.Count() > 0)
                    {

                        /*dataGridView1.DataSource = q
                            .OrderBy(t => t.Cate)
                            .ToList();
                         */


                        DataRow dr = dtSNOld.NewRow();
                        //==20180801
                        var chkForceF = dc.FFixedAsset_Categories
                                            .Where(t => t.FCAT_ID == chFCAT_ID && t.IsForce == '1')
                                            .FirstOrDefault();

                        if (chkForceF == null)
                        {
                            //==20180801

                            dr["Cate"] = "";
                            dr["SNOld"] = "ไม่ระบุ...";
                            dr["FFaNoOld"] = "";
                            dr["Location"] = "";
                            dr["Model"] = "";

                            dr["Brand"] = "";
                            dr["รับประกัน"] = "";
                            dr["Detail"] = "";
                            dr["StatusUse"] = "";
                            dr["FCAT_ID"] = "";

                            dr["FBA_ID"] = "";
                            dr["FMD_ID"] = "";
                            dr["CheckGauge"] = "";

                            dtSNOld.Rows.Add(dr);
                        }

                        foreach (var old1 in q)
                        {
                            DataRow dr1 = dtSNOld.NewRow();
                            dr1["Cate"] = old1.Cate;
                            dr1["SNOld"] = old1.SNOld;
                            dr1["FFaNoOld"] = old1.FFaNoOld;
                            dr1["Location"] = old1.LOC_ID;
                            dr1["Model"] = old1.Model;

                            dr1["Brand"] = old1.Brand;
                            dr1["รับประกัน"] = old1.รับประกัน;
                            dr1["Detail"] = old1.Detail;
                            dr1["StatusUse"] = old1.StatusUse;
                            dr1["FCAT_ID"] = old1.FCAT_ID;

                            dr1["FBA_ID"] = old1.FBA_ID;
                            dr1["FMD_ID"] = old1.FMD_ID;
                            dr1["CheckGauge"] = old1.CheckGauge;

                            dtSNOld.Rows.Add(dr1);
                        }
                        dataGridView1.DataSource = null;
                        dataGridView1.Refresh();
                        dataGridView1.DataSource = dtSNOld;
                        dataGridView1.Enabled = true;
                        _IsFound = true;
                    }
                    else
                    {

                        //20150529 // เปลี่ยนจากที่หาไม่เจอให้เป็น none ไปเป็น ไม่ระบุ...
                        ////MessageBox.Show("ไม่พบข้อมูล S/N old" + Environment.NewLine +
                        ////                "กรุณาป้อนข้อมูลใหม่...", "ผลการตรวจสอบ", MessageBoxButtons.OK);

                        ////this.Close();
                        ////_IsFound = false;

                        DataRow dr = dtSNOld.NewRow();
                        //==20180801
                        var chkForceF = dc.FFixedAsset_Categories
                                            .Where(t => t.FCAT_ID == chFCAT_ID && t.IsForce == '1')
                                            .FirstOrDefault();

                        if (chkForceF == null)
                        {
                            //==20180801
                            dr["Cate"] = "";
                            dr["SNOld"] = "ไม่ระบุ...";
                            dr["FFaNoOld"] = "";
                            dr["Location"] = "";
                            dr["Model"] = "";

                            dr["Brand"] = "";
                            dr["รับประกัน"] = "";
                            dr["Detail"] = "";
                            dr["StatusUse"] = "";
                            dr["FCAT_ID"] = "";

                            dr["FBA_ID"] = "";
                            dr["FMD_ID"] = "";
                            dr["CheckGauge"] = "";

                            dtSNOld.Rows.Add(dr);
                        }
                        dataGridView1.DataSource = null;
                        dataGridView1.Refresh();
                        dataGridView1.DataSource = dtSNOld;
                        dataGridView1.Enabled = true;
                        _IsFound = true;

                    }
                }
                else if (SN_Old == null) //20150529 // เพิ่มในกรณีไม่ key แต่กดค้นหา จากที่เคยค้นหาไม่ได้ ให้เป็น ไม่ระบุ...
                {
                    DataRow dr = dtSNOld.NewRow();
                    //==20180801
                    var chkForceF = dc.FFixedAsset_Categories
                                        .Where(t => t.FCAT_ID == chFCAT_ID && t.IsForce == '1')
                                        .FirstOrDefault();

                    if (chkForceF == null)
                    {
                        //==20180801
                        dr["Cate"] = "";
                        dr["SNOld"] = "ไม่ระบุ...";
                        dr["FFaNoOld"] = "";
                        dr["Location"] = "";
                        dr["Model"] = "";

                        dr["Brand"] = "";
                        dr["รับประกัน"] = "";
                        dr["Detail"] = "";
                        dr["StatusUse"] = "";
                        dr["FCAT_ID"] = "";

                        dr["FBA_ID"] = "";
                        dr["FMD_ID"] = "";
                        dr["CheckGauge"] = "";

                        dtSNOld.Rows.Add(dr);
                    }

                    dataGridView1.DataSource = null;
                    dataGridView1.Refresh();
                    dataGridView1.DataSource = dtSNOld;
                    dataGridView1.Enabled = true;
                    _IsFound = true;
                }



            }
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

            //DateTime dt1 = DateTime.Parse("1/1/1990");
            try
            {
                if (e.RowIndex != -1)
                {

                    if (SN_New != null)
                    {
                        if (dataGridView1["SNNew", e.RowIndex].Value.ToString() == "ไม่ระบุ...")
                        {
                            SN_New = "ไม่ระบุ...";
                            DialogResult = DialogResult.OK;
                            return;
                        }
                        SN_New = dataGridView1["SNNew", e.RowIndex].Value.ToString();
                        FANoNew = dataGridView1["FFaNoNew", e.RowIndex].Value == null ? "--" : dataGridView1["FFaNoNew", e.RowIndex].Value.ToString();
                        Location = dataGridView1["Location", e.RowIndex].Value == null ? "--" : dataGridView1["Location", e.RowIndex].Value.ToString();

                        MODEL = dataGridView1["Model", e.RowIndex].Value.ToString();
                        // WarrantDate = dataGridView1["รับประกัน", e.RowIndex].Value == null ? dt1 : DateTime.Parse(dataGridView1["รับประกัน", e.RowIndex].Value.ToString());//dt1;//DateTime.Parse(dataGridView1["รับประกัน", e.RowIndex].Value.ToString()) == null ? dt1 : DateTime.Parse(dataGridView1["รับประกัน", e.RowIndex].Value.ToString());  //20141202 DateTime.Parse(dataGridView1["รับประกัน", e.RowIndex].Value.ToString());
                        FCAT_ID_New = Convert.ToInt32(dataGridView1["FCAT_ID_New", e.RowIndex].Value.ToString());
                    }
                    else if (SN_Old != null)
                    {
                        if (dataGridView1["SNOld", e.RowIndex].Value.ToString() == "ไม่ระบุ...")
                        {
                            SN_Old = "ไม่ระบุ...";
                            DialogResult = DialogResult.OK;
                            _IsFound = true;
                            return;
                        }
                        SN_Old = dataGridView1["SNOld", e.RowIndex].Value.ToString();
                        FANoOld = dataGridView1["FFaNoOld", e.RowIndex].Value == null ? "--" : dataGridView1["FFaNoOld", e.RowIndex].Value.ToString();
                        FCAT_ID = Convert.ToInt32(dataGridView1["FCAT_ID", e.RowIndex].Value.ToString());
                        LOC_ID = dataGridView1["Location", e.RowIndex].Value == null ? '0' : Convert.ToInt32(dataGridView1["Location", e.RowIndex].Value.ToString());
                        MODEL = dataGridView1["Model", e.RowIndex].Value.ToString();

                        FBA_ID = Convert.ToInt32(dataGridView1["FBA_ID", e.RowIndex].Value.ToString());
                        FMD_ID = Convert.ToInt32(dataGridView1["FMD_ID", e.RowIndex].Value.ToString());
                        var ddd = dataGridView1["รับประกัน", e.RowIndex].Value;
                        WarrantDate = ddd == DBNull.Value ? new DateTime(1990, 1, 1) : DateTime.Parse(ddd.ToString());
                        CheckGauge = dataGridView1["CheckGauge", e.RowIndex].Value == null ? Convert.ToInt32("0") : Convert.ToInt32(dataGridView1["CheckGauge", e.RowIndex].Value.ToString());
                    }

                }

                DialogResult = DialogResult.OK;
            }
            catch (Exception)
            {

                DialogResult = DialogResult.Cancel;
            }
        }

        private void toolStripButton_FindSN_Click(object sender, EventArgs e)
        {

            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn))
            {
                if (SN_New != null)
                {
                    setDtSNNew();
                    string vSN_New = toolStripText_txtFind.Text.Trim();
                    var q = (from t in dc.FFixedAsset_SerialNumber_Pools
                             from c in dc.FFixedAsset_Categories
                             from m in dc.FFixedAsset_Models
                             from b in dc.FFixedAsset_Brands

                             where (m.FBA_ID == b.FBA_ID &&
                                     t.FCAT_ID == c.FCAT_ID &&
                                     t.FMD_ID == m.FMD_ID) &&
                             //where (t.FMD_ID == m.FMD_ID) &&
                             //        t.FCAT_ID == c.FCAT_ID &&
                                     t.SerialNumber.Contains(vSN_New) &&
                                     (t.FCAT_ID == FCAT_ID) &&//SN New ต้องเป็น ประเภทเดียวกับ SN Old
                                     (new string[] { "00", "66", "76" }.Contains(t.StatusUse)) ||
                                     (t.StatusUse == null) //&& // Popup เฉพาะ SN ที่ยังไม่ได้ใช้งาน
                             //(t.StatusUse == "01") &&
                             //(!lsSNSelec.Select(sn => sn.SNNew).ToArray().Contains(t.SerialNumber))
                             select new
                             {
                                 Cate = c.FixedAsset_CateName,
                                 SNNew = t.SerialNumber,
                                 FFaNoNew = t.ControlFixAssetNo,
                                 Location = "",
                                 Model = m.FModel,
                                 Brand = b.FBrand,
                                 รับประกัน = t.ExpiryDate,
                                 t.Detail,
                                 t.StatusUse,
                                 CheckGauge = 0,
                                 FCAT_ID_New = t.FCAT_ID  //20180426
                             })
                            .Distinct();

                    if (q.Count() > 0)
                    {

                        DataRow dr = dtSNNew.NewRow();

                        //==20180802
                        var chkForceF = dc.FFixedAsset_Categories
                                            .Where(t => t.FCAT_ID == FCAT_ID && t.IsForce == '1')
                                            .FirstOrDefault();

                        if (chkForceF == null)
                        {
                            dr["Cate"] = "";
                            dr["SNNew"] = "ไม่ระบุ...";
                            dr["FFaNoNew"] = "";
                            dr["Location"] = "";
                            dr["Model"] = "";

                            dr["Brand"] = "";
                            dr["รับประกัน"] = "";
                            dr["Detail"] = "";
                            dr["StatusUse"] = "";
                            dr["CheckGauge"] = "";
                            dr["FCAT_ID_New"] = 0;

                            dtSNNew.Rows.Add(dr);
                        }
                        foreach (var N1 in q)
                        {
                            /* //20141202
                             * dataGridView1.DataSource = q
                                .OrderBy(t => t.Cate)
                                .ToList();
                             */

                            DataRow dr1 = dtSNNew.NewRow();
                            dr1["Cate"] = N1.Cate;
                            dr1["SNNew"] = N1.SNNew;
                            dr1["FFaNoNew"] = N1.FFaNoNew;
                            dr1["Location"] = N1.Location;
                            dr1["Model"] = N1.Model;

                            dr1["Brand"] = N1.Brand;
                            dr1["รับประกัน"] = N1.รับประกัน;
                            dr1["Detail"] = N1.Detail;
                            dr1["StatusUse"] = N1.StatusUse;

                            dr1["CheckGauge"] = N1.CheckGauge;
                            dr["FCAT_ID_New"] = N1.FCAT_ID_New; //20180426
                            dtSNNew.Rows.Add(dr1);
                        }
                        dataGridView1.DataSource = null;
                        dataGridView1.Refresh();
                        dataGridView1.DataSource = dtSNNew;
                        dataGridView1.Enabled = true;



                    }
                    else
                    {
                        //MessageBox.Show("ไม่พบข้อมูล S/N New " + Environment.NewLine +
                        //                "ต้องป้อน S/N New ประเภทอุปกรณ์เดียวกับ S/N Old เท่านั้น..." + Environment.NewLine +
                        //                "กรุณาตรวจสอบ...", "ผลการตรวจสอบ", MessageBoxButtons.OK);

                        MessageBox.Show("ไม่พบข้อมูล S/N New ตามที่ระบุ" + Environment.NewLine +
                                        "(ต้องป้อน S/N New ประเภทอุปกรณ์เดียวกับ S/N Old เท่านั้น...)" + Environment.NewLine +
                                        "กรุณาตรวจสอบ...", "ผลการตรวจสอบ", MessageBoxButtons.OK);


                        this.Close();
                    }
                }
                else if (SN_Old != null)
                {
                    String vSN_Old = toolStripText_txtFind.Text.Trim();
                    setDtSNOld();
                    var q = (from t in dc.FFixedAsset_Station_Details
                             from b in dc.FFixedAsset_Brands
                             from c in dc.FFixedAsset_Categories
                             from m in dc.FFixedAsset_Models

                             from ff in dc.FFixedAsset_SerialNumber_Pools

                             where (
                                     /*20210309
                                             t.FBA_ID == b.FBA_ID &&
                                              t.FCAT_ID == c.FCAT_ID &&
                                              t.FMD_ID == m.FMD_ID) &&
                                      */
                                     //--20210309
                                     m.FBA_ID == b.FBA_ID &&
                                     b.FCAT_ID == c.FCAT_ID &&
                                    ff.FMD_ID == m.FMD_ID) &&
                                     //-----

                                     t.SerialNumber.Contains(vSN_Old) &&
                                     (t.STA_ID == STAID) &&
                                     (ff.SerialNumber == t.SerialNumber)
                             select new
                             {
                                 Cate = c.FixedAsset_CateName,
                                 SNOld = t.SerialNumber,
                                 FFaNoOld = ff.ControlFixAssetNo,
                                 LOC_ID = t.LOC_ID == null ? 0 : t.LOC_ID,
                                 Model = m.FModel,
                                 Brand = b.FBrand,
                                 รับประกัน = t.WarrantDate,
                                 t.Detail,
                                 StatusUse = "11",
                                 /* 20210309 FCAT_ID = t.FCAT_ID,
                                  FBA_ID = t.FBA_ID,
                                  FMD_ID = t.FMD_ID,*/
                                 //--20210309
                                 FCAT_ID = c.FCAT_ID,
                                 FBA_ID = b.FBA_ID,
                                 FMD_ID = m.FMD_ID,
                                 //---
                                 CheckGauge = t.CheckGauge == null ? 0 : t.CheckGauge
                             })
                            .Distinct();

                    if (q.Count() > 0)
                    {
                        dataGridView1.DataSource = null;
                        dataGridView1.Refresh();

                        DataRow drSchOld = dtSNOld.NewRow();

                        //20180802
                        var chkForceF = dc.FFixedAsset_Categories
                                          .Where(t => t.FCAT_ID == chFCAT_ID && t.IsForce == '1')
                                          .FirstOrDefault();

                        if (chkForceF == null)
                        {
                            drSchOld["Cate"] = "";
                            drSchOld["SNOld"] = "ไม่ระบุ...";
                            drSchOld["FFaNoOld"] = "";
                            drSchOld["Location"] = "";
                            drSchOld["Model"] = "";

                            drSchOld["Brand"] = "";
                            drSchOld["รับประกัน"] = "";
                            drSchOld["Detail"] = "";
                            drSchOld["StatusUse"] = "";
                            drSchOld["FCAT_ID"] = "";

                            drSchOld["FBA_ID"] = "";
                            drSchOld["FMD_ID"] = "";
                            drSchOld["CheckGauge"] = "";

                            dtSNOld.Rows.Add(drSchOld);
                        }

                        foreach (var old1 in q)
                        {
                            DataRow drSchOld1 = dtSNOld.NewRow();
                            drSchOld1["Cate"] = old1.Cate;
                            drSchOld1["SNOld"] = old1.SNOld;
                            drSchOld1["FFaNoOld"] = old1.FFaNoOld;
                            drSchOld1["Location"] = old1.LOC_ID;
                            drSchOld1["Model"] = old1.Model;

                            drSchOld1["Brand"] = old1.Brand;
                            drSchOld1["รับประกัน"] = old1.รับประกัน;
                            drSchOld1["Detail"] = old1.Detail;
                            drSchOld1["StatusUse"] = old1.StatusUse;
                            drSchOld1["FCAT_ID"] = old1.FCAT_ID;

                            drSchOld1["FBA_ID"] = old1.FBA_ID;
                            drSchOld1["FMD_ID"] = old1.FMD_ID;
                            drSchOld1["CheckGauge"] = old1.CheckGauge;

                            dtSNOld.Rows.Add(drSchOld1);
                        }
                        dataGridView1.DataSource = null;
                        dataGridView1.Refresh();
                        dataGridView1.DataSource = dtSNOld;
                        dataGridView1.Enabled = true;

                        /* //20141202
                         * dataGridView1.DataSource = q
                            .OrderBy(t => t.Cate)
                            .ToList();
                         */

                    }
                    else
                    {
                        MessageBox.Show("ไม่พบข้อมูล S/N old" + Environment.NewLine +
                                        "กรุณาป้อนข้อมูลใหม่...", "ผลการตรวจสอบ", MessageBoxButtons.OK);
                        this.Close();
                    }
                }
            }
        }

        private void toolStripButton_Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void setDtSNNew()
        {

            dtSNNew = new DataTable();
            dtSNNew.Columns.Add(new DataColumn("Cate"));
            dtSNNew.Columns.Add(new DataColumn("SNNew"));
            dtSNNew.Columns.Add(new DataColumn("FFaNoNew"));
            dtSNNew.Columns.Add(new DataColumn("Location"));
            dtSNNew.Columns.Add(new DataColumn("Model"));

            dtSNNew.Columns.Add(new DataColumn("Brand"));
            dtSNNew.Columns.Add(new DataColumn("รับประกัน"));
            dtSNNew.Columns.Add(new DataColumn("Detail"));
            dtSNNew.Columns.Add(new DataColumn("StatusUse"));
            dtSNNew.Columns.Add(new DataColumn("CheckGauge"));

            dtSNNew.Columns.Add(new DataColumn("FCAT_ID_New")); //20180426

        }

        private void setDtSNOld()
        {

            dtSNOld = new DataTable();
            dtSNOld.Columns.Add(new DataColumn("Cate"));
            dtSNOld.Columns.Add(new DataColumn("SNOld"));
            dtSNOld.Columns.Add(new DataColumn("FFaNoOld"));
            //dtSNOld.Columns.Add(new DataColumn("LOC_ID"));
            dtSNOld.Columns.Add(new DataColumn("Location"));
            dtSNOld.Columns.Add(new DataColumn("Model"));

            dtSNOld.Columns.Add(new DataColumn("Brand"));
            dtSNOld.Columns.Add(new DataColumn("รับประกัน"));
            dtSNOld.Columns.Add(new DataColumn("Detail"));
            dtSNOld.Columns.Add(new DataColumn("StatusUse"));
            dtSNOld.Columns.Add(new DataColumn("FCAT_ID"));

            dtSNOld.Columns.Add(new DataColumn("FBA_ID"));
            dtSNOld.Columns.Add(new DataColumn("FMD_ID"));
            dtSNOld.Columns.Add(new DataColumn("CheckGauge"));

        }
    }
}
