﻿using SMSMINI.DAL; //20200623
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
namespace SMSMINI.Transaction.Finding
{
    public partial class Find_JOBPhoto : Form
    {
        public string JOBNo { get; set; }
        public clGetJOBPhoto jPhoto { get; set; }

        clGetJOBPhoto _jPhoto = new clGetJOBPhoto();
        string strConn = ""; //20200623
        public Find_JOBPhoto()
        {
            InitializeComponent();
        }

        private void toolStripButton_FindStation_Click(object sender, EventArgs e)
        {
            //20200623
            //if (tmpeditMode == "job_edit")
            //strConn = GetConnection.Connection;
            //else
            //    strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;

            strConn = GetConnection.CheckConnectionString(toolStripText_txtFind.Text.Trim());
            //========


            if (toolStripText_txtFind.Text == "")
            {
                MessageBox.Show("กรุณาป้อนข้อมูล JOB No.", "ข้อผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Information);
                toolStripText_txtFind.Focus();
                toolStripText_txtFind.SelectAll();
                return;
            }

            //20200623 using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn))
            {
                var q = (from j in dc.JOBs
                         from st in dc.Stations
                             //from wi in dc.JOB_WorkIDs
                         from jd in dc.JOB_Details
                             //join wi in dc.JOB_WorkIDs equals j
                         where j.JOB_ID == jd.JOB_ID &&
                             (j.STA_ID == st.STA_ID) &&
                              j.JOB_ID.Contains(toolStripText_txtFind.Text)
                              && new string[] { "07", "99" }.Contains(jd.JOBS_ID) //20200625
                         select new clGetJOBPhoto
                         {
                             JOB_ID = j.JOB_ID,
                             StartDate = jd.StartDate,
                             EndDate = jd.EndDate,
                             STA_ID = st.STA_ID,
                             StationSys = st.StationSys,
                             Work_ID = getWID(dc, j.JOB_ID),
                             VAN = jd.EMP_ID,
                             JobFailure_Detail = j.JobFailure_Detail
                         }).ToList();
                if (q.Count() > 0)
                    dataGridView1.DataSource = q;
                else
                    MessageBox.Show("ไม่มีข้อมูล JOB...", "ผลการค้นหา", MessageBoxButtons.OK);
            }
        }

        private string getWID(SMSMINI.DAL.SMSManage.SMSManageDataContext dc, string _job_ID)
        {
            var wid = dc.JOB_WorkIDs.Where(t => t.JOB_ID == _job_ID).FirstOrDefault();
            if (wid != null)
                return wid.JOB_ID;
            else
                return "";
        }

        private void toolStripButton_Exit_Click(object sender, EventArgs e)
        {

        }

        private void Find_JOBPhoto_Load(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex != -1)
                {
                    _jPhoto.JOB_ID = dataGridView1["JOB_ID", e.RowIndex].Value.ToString();
                    _jPhoto.StartDate = DateTime.Parse(dataGridView1["StartDate", e.RowIndex].Value.ToString());

                    if (dataGridView1["EndDate", e.RowIndex].Value != null)
                        _jPhoto.EndDate = DateTime.Parse(dataGridView1["EndDate", e.RowIndex].Value.ToString());

                    _jPhoto.STA_ID = dataGridView1["STA_ID", e.RowIndex].Value.ToString();

                    _jPhoto.StationSys = dataGridView1["StationSys", e.RowIndex].Value.ToString();
                    _jPhoto.Work_ID = dataGridView1["Work_ID", e.RowIndex].Value.ToString();
                    _jPhoto.VAN = dataGridView1["VAN", e.RowIndex].Value.ToString();
                    _jPhoto.JobFailure_Detail = dataGridView1["JobFailure_Detail", e.RowIndex].Value.ToString();

                    //20190717
                    _jPhoto.EndDate = DateTime.Parse(dataGridView1["EndDate", e.RowIndex].Value.ToString());

                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("เกิดข้อผิดพลาด " + ex.Message, "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                if (e.RowIndex != -1)
                {
                    jPhoto = _jPhoto;
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("เกิดข้อผิดพลาด " + ex.Message, "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void toolStripText_txtFind_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                toolStripButton_FindStation_Click(null, null);
            }
        }
    }
}
