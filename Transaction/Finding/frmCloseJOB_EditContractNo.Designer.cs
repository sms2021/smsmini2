﻿namespace SMSMINI.Transaction.Finding
{
    partial class frmCloseJOB_EditContractNo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCloseJOB_EditContractNo));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.toolStrip_Mnu = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripTextBox_ContractNo = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_Save = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_Exit = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel_Dateime = new System.Windows.Forms.ToolStripLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNewContracNo = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.toolStrip_Mnu.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(0, 69);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(884, 224);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // toolStrip_Mnu
            // 
            this.toolStrip_Mnu.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip_Mnu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.toolStripSeparator4,
            this.toolStripTextBox_ContractNo,
            this.toolStripSeparator6,
            this.toolStripButton_Save,
            this.toolStripSeparator5,
            this.toolStripButton_Exit,
            this.toolStripLabel_Dateime});
            this.toolStrip_Mnu.Location = new System.Drawing.Point(0, 0);
            this.toolStrip_Mnu.Name = "toolStrip_Mnu";
            this.toolStrip_Mnu.Size = new System.Drawing.Size(884, 39);
            this.toolStrip_Mnu.TabIndex = 4;
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(97, 36);
            this.toolStripLabel1.Text = "เลขสัญญาเปิด JOB:";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripTextBox_ContractNo
            // 
            this.toolStripTextBox_ContractNo.Enabled = false;
            this.toolStripTextBox_ContractNo.Name = "toolStripTextBox_ContractNo";
            this.toolStripTextBox_ContractNo.Size = new System.Drawing.Size(300, 39);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripButton_Save
            // 
            this.toolStripButton_Save.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.toolStripButton_Save.ForeColor = System.Drawing.SystemColors.Desktop;
            this.toolStripButton_Save.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Save.Image")));
            this.toolStripButton_Save.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Save.Name = "toolStripButton_Save";
            this.toolStripButton_Save.Size = new System.Drawing.Size(68, 36);
            this.toolStripButton_Save.Text = "Save";
            this.toolStripButton_Save.ToolTipText = "บันทึกข้อมูล";
            this.toolStripButton_Save.Click += new System.EventHandler(this.toolStripButton_Save_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripButton_Exit
            // 
            this.toolStripButton_Exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.toolStripButton_Exit.ForeColor = System.Drawing.SystemColors.Desktop;
            this.toolStripButton_Exit.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Exit.Image")));
            this.toolStripButton_Exit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Exit.Name = "toolStripButton_Exit";
            this.toolStripButton_Exit.Size = new System.Drawing.Size(60, 36);
            this.toolStripButton_Exit.Text = "Exit";
            this.toolStripButton_Exit.ToolTipText = "ออกจากหน้าจอ";
            this.toolStripButton_Exit.Click += new System.EventHandler(this.toolStripButton_Exit_Click);
            // 
            // toolStripLabel_Dateime
            // 
            this.toolStripLabel_Dateime.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripLabel_Dateime.Font = new System.Drawing.Font("Tahoma", 10F);
            this.toolStripLabel_Dateime.ForeColor = System.Drawing.SystemColors.Desktop;
            this.toolStripLabel_Dateime.Name = "toolStripLabel_Dateime";
            this.toolStripLabel_Dateime.Size = new System.Drawing.Size(0, 36);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "แก้ไขสัญญา:";
            // 
            // txtNewContracNo
            // 
            this.txtNewContracNo.Location = new System.Drawing.Point(85, 47);
            this.txtNewContracNo.Name = "txtNewContracNo";
            this.txtNewContracNo.ReadOnly = true;
            this.txtNewContracNo.Size = new System.Drawing.Size(294, 20);
            this.txtNewContracNo.TabIndex = 6;
            // 
            // frmCloseJOB_EditContractNo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 293);
            this.Controls.Add(this.txtNewContracNo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.toolStrip_Mnu);
            this.Controls.Add(this.dataGridView1);
            this.Name = "frmCloseJOB_EditContractNo";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "แก้ไขเลขที่สัญญา";
            this.Load += new System.EventHandler(this.frmCloseJOB_EditContractNo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.toolStrip_Mnu.ResumeLayout(false);
            this.toolStrip_Mnu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ToolStrip toolStrip_Mnu;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripButton toolStripButton_Save;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton toolStripButton_Exit;
        private System.Windows.Forms.ToolStripLabel toolStripLabel_Dateime;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox_ContractNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNewContracNo;
    }
}