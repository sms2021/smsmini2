﻿using System;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace SMSMINI.Transaction.Finding
{
    public partial class Find_REQ_Edit : Form
    {
        public string _REQ_ID { get; set; }
        public string _EMP_ID { get; set; }
        public DateTime? _DateReq { get; set; }
        public string _EMPname { get; set; }
        public bool? _IsSend_ERP { get; set; }

        public string _REQDetail { get; set; }

        public Find_REQ_Edit()
        {
            InitializeComponent();
            object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
            if (attributes.Length == 0)
            {
                this.Text = this.Text + " SMS MINI [v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "]";

            }
            this.Text = this.Text + " " + ((AssemblyProductAttribute)attributes[0]).Product + " [v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "]";

        }

        private void Find_REQ_Edit_Load(object sender, EventArgs e)
        {
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            {
                var q = from t in dc.JOB_Detail_REQs
                        from em in dc.Employees
                        orderby t.REQ_ID
                        where (t.EMP_ID == em.EMP_ID) && (
                            (t.DateReq.Value.Date >= _DateReq.Value.Date) &&// (t.DateReq.Value.Date <= DateTime.Now.Date))&&
                            t.EMP_ID == _EMP_ID)
                        select new
                        {
                            เลขที่การเบิก = t.REQ_ID,
                            วันที่การเบิก = t.DateReq,
                            พนักงาน = (em.FName ?? "") + (em.LName ?? ""),
                            ส่งERP = t.IsSend_ERP,
                            Detail = t.REQDetail
                        };

                if (q.Count() > 0)
                    dataGridView1.DataSource = q.ToList();
                else
                {
                    MessageBox.Show("ไม่มีข้อมูล การเบิก ตามที่ต้องการ กรุณาระบุ ใหม่", "ผลการค้นหา", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    this.Close();
                }
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //try
            //{
            //    if (e.RowIndex != -1)
            //    {
            //        _REQ_ID = dataGridView1["เลขที่การเบิก", e.RowIndex].Value.ToString();
            //        _DateReq = Convert.ToDateTime(dataGridView1["วันที่การเบิก", e.RowIndex].Value == null ? null : dataGridView1["วันที่การเบิก", e.RowIndex].Value.ToString());
            //        _EMPname = dataGridView1["พนักงาน", e.RowIndex].Value.ToString();
            //        _IsSend_ERP = Convert.ToBoolean(dataGridView1["ส่งERP", e.RowIndex].Value == null ? null : dataGridView1["ส่งERP", e.RowIndex].Value);
            //        _REQDetail = dataGridView1["Detail", e.RowIndex].Value==null?"":dataGridView1["Detail", e.RowIndex].Value.ToString();

            //        this.DialogResult = DialogResult.OK;
            //        this.Close();
            //    }
            //}
            //catch (System.Exception ex)
            //{
            //    MessageBox.Show("เกิดข้อผิดพลาด " + ex.Message, "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex != -1)
                {
                    _REQ_ID = dataGridView1["เลขที่การเบิก", e.RowIndex].Value.ToString();
                    _DateReq = Convert.ToDateTime(dataGridView1["วันที่การเบิก", e.RowIndex].Value == null ? null : dataGridView1["วันที่การเบิก", e.RowIndex].Value.ToString());
                    _EMPname = dataGridView1["พนักงาน", e.RowIndex].Value.ToString();
                    _IsSend_ERP = Convert.ToBoolean(dataGridView1["ส่งERP", e.RowIndex].Value == null ? null : dataGridView1["ส่งERP", e.RowIndex].Value);
                    _REQDetail = dataGridView1["Detail", e.RowIndex].Value == null ? "" : dataGridView1["Detail", e.RowIndex].Value.ToString();

                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("เกิดข้อผิดพลาด " + ex.Message, "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
