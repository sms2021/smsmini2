﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;


namespace SMSMINI.Transaction.Finding
{
    public partial class Find_SNPart : Form
    {
        //private string tmpPartNo = "";
        public string SNpart { get; set; }
        //public string fTmpPartNo { get { return tmpPartNo; } set { tmpPartNo = value; } }
        public string fTmpPartNo { get; set; }

        public Find_SNPart()
        {
            InitializeComponent();
            //object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
            //if (attributes.Length == 0)
            //{
            //    this.Text = "SMS MINI [v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "]";

            //}
            //this.Text = ((AssemblyProductAttribute)attributes[0]).Product + " [v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "]";

        }

        private void Find_SNPart_Load(object sender, EventArgs e)
        {
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            {
                var qSN = dc.SpareParts_SerialNumbers
                    .Where(t => t.SPA_ID == fTmpPartNo)
                    .Select(t => new
                    {
                        PartNo = t.SPA_ID,
                        SerialNumber = t.SerialNumber,
                        ReceivedDate = String.Format("{0:dd/MM/yyyy}", t.ReceivedDate),//t.ReceivedDate ,
                        ExpiryDate = String.Format("{0:dd/MM/yyyy}", t.ExpiryDate),//t.ExpiryDate,
                    }).ToList();

                if (qSN.Count() > 0)
                {
                    dataGridView1.DataSource = qSN;
                }
                else
                {
                    MessageBox.Show("ไม่พบข้อมูล S/N " + Environment.NewLine +
                                           "กรุณาตรวจสอบ...", "ผลการตรวจสอบ", MessageBoxButtons.OK);
                    //this.Close();
                }
            }
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex != -1)
                {
                    SNpart = dataGridView1["SerialNumber", e.RowIndex].Value.ToString();
                }


                DialogResult = DialogResult.OK;

            }
            catch (Exception)
            {

                DialogResult = DialogResult.Cancel;
            }
        }



        private void toolStripButton_Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void toolStripButton_FindSN_Click(object sender, EventArgs e)
        {
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            {
                var qSN = dc.SpareParts_SerialNumbers
                    .Where(t => t.SPA_ID == fTmpPartNo && t.SerialNumber.Contains(toolStripText_txtFind.Text))
                    .Select(t => new
                    {
                        PartNo = t.SPA_ID,
                        SerialNumber = t.SerialNumber,
                        ReceivedDate = String.Format("{0:dd/MM/yyyy}", t.ReceivedDate),//t.ReceivedDate ,
                        ExpiryDate = String.Format("{0:dd/MM/yyyy}", t.ExpiryDate),//t.ExpiryDate,
                    }).ToList();

                if (qSN.Count() > 0)
                {
                    dataGridView1.DataSource = qSN;
                }
                else
                {
                    MessageBox.Show("ไม่พบข้อมูล S/N " + Environment.NewLine +
                                           "กรุณาตรวจสอบ...", "ผลการตรวจสอบ", MessageBoxButtons.OK);
                    //this.Close();
                }
            }
        }

        private void toolStripButton_FindSN_Click_1(object sender, EventArgs e)
        {

        }








    }
}
