﻿using SMSMINI.DAL;
using SMSMINI.DAL.SMSManage; 
using System;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace SMSMINI.Transaction.Finding
{
    public partial class FindJobClose : Form
    {

        private static string tmpSearchMode = "";

        private static string tmpJobID = "";
        private static string tmpJobOpenDate = "";
        private static string tmpFailureCode = "";
        private static string tmpStation = "";
        private static string tmpFailureDetail = "";
        private static string tmpeditMode = "";//"job_edit";

        //private static DateTime tmpEndDate ;//20200608
        private static string tmpEndDate = "";//20200608
        public string TmpSearchMode { get { return tmpSearchMode; } set { tmpSearchMode = value; } }


        public string TmpJobID { get { return tmpJobID; } set { tmpJobID = value; } }
        public string TmpJobOpenDate { get { return tmpJobOpenDate; } set { tmpJobOpenDate = value; } }
        public string TmpFailureCode { get { return tmpFailureCode; } set { tmpFailureCode = value; } }
        public string TmpStation { get { return tmpStation; } set { tmpStation = value; } }

        public string TmpFailureDetail { get { return tmpFailureDetail; } set { tmpFailureDetail = value; } }
        public string TmpEditMode { get { return tmpeditMode; } set { tmpeditMode = value; } }

        public bool isSMSbackup { get; set; }
        string strConn = "";

        public string TmpEndDate { get { return tmpEndDate; } set { tmpEndDate = value; } }
        //public DateTime TmpEndDate { get { return tmpEndDate; } set { tmpEndDate = value; } }

        public FindJobClose()
        {
            //20200609 System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

            InitializeComponent();

            object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
            if (attributes.Length == 0)
            {
                this.Text = "SMS MINI [v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "]";

            }
            this.Text = ((AssemblyProductAttribute)attributes[0]).Product + " [v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "]";


        }



        private void FindJobClose_Load(object sender, EventArgs e)
        {

            if (tmpeditMode == "job_edit")
                strConn = GetConnection.Connection;
            else
                strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;

            try
            {
                using (SMSManageDataContext db = new SMSManageDataContext(strConn))
                {
                    if (tmpeditMode == "job_edit")
                        ShowDataJobEdit(db);
                    else if (tmpeditMode == "job_upload")
                        ShowDataJobUpload(db);
                    else
                        ShowDataJobAdd(db);
                }

            }
            catch (System.Exception ex)
            {
                MessageBox.Show("เกิดข้อผิดพลาด..." + Environment.NewLine + ex.Message, "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.DialogResult = DialogResult.OK;
                this.Close();
            }


        }

        private void ShowDataJobEdit(SMSManageDataContext db)
        {
            var cc = db.Connection;
            string dt = DateTime.Now.ToShortDateString();

            //ถ้าเป็น van แก้ไขได้เฉพาะภายในวันเดียวกัน
            //ถ้าเป็น SUP แก้ไขทุกวัน 
            //=======================//
            //UserInfo.UserLevID
            //=======================//
            //06	Supervisor {VAN}
            //07	Supervisor {UserAdmin}
            //08	Supervisor {Center}
            //09	Manager(Monitor)

            if (UserInfo.SUP_VAN == UserInfo.Van_ID && new string[] { "06", "07", "08", "09" }.Contains(UserInfo.UserLevID))
            {
                //20191017 showSupData(db);
                showSupData(db);
            }
            //=======================//
            //UserInfo.UserLevID
            //=======================//
            //12	Section Manager(ตู้จ่าย)
            //13	Section Manager(ระบบ)
            //99	Administrator
            else if (new string[] { "05", "12", "13", "99" }.Contains(UserInfo.UserLevID))
            {
                //20191017 showSectionData(db);
                showSectionData(db);
            }
            else
            {
                //20191017 showJobByVAN(db, dt);
                showJobByVAN(db, dt);
            }

        }
        //20191017 private void showSectionData(SMSManageDataContext db)
        private void showSectionData(SMSManageDataContext db)
        {
            if (tmpJobID != "")
            {
                if ((from t in db.vWait_DistributeJobs
                     from jd in db.JOB_Details//20200609
                     where
                       new string[] { "07", "99" }.Contains(jd.JOBS_ID) && //20200611
                       (t.JOB_ID.Contains(tmpJobID) &&
                        t.Opendate1.Contains(tmpJobOpenDate) &&
                        t.StationSys.Contains(tmpStation)) &&
                        t.JOB_ID == jd.JOB_ID //20200609

                     select t).Count() > 0)
                {
                    var j1 = from t in db.vWait_DistributeJobs
                             from jd in db.JOB_Details //20200609
                             where
                              new string[] { "07", "99" }.Contains(jd.JOBS_ID) && //20200611
                               (t.JOB_ID.Contains(tmpJobID) &&
                                t.Opendate1.Contains(tmpJobOpenDate) &&
                                t.StationSys.Contains(tmpStation)) &&
                                t.JOB_ID == jd.JOB_ID  //20200609                       
                             /*//20200608
                           select new
                           {
                               JobNo = t.JOB_ID,
                               Date = t.Opendate,
                               STA_ID = t.STA_ID1 ?? t.STA_ID,
                               Station = t.StationSys,
                               VAN = t.EMP_ID3,
                               FailCode = t.FAI_ID,
                               อาการเสีย = t.JobFailure_Detail,
                               ผู้แจ้ง = t.Informer ?? "" + " " + t.InPhone ?? "",
                               JobStatus = t.JobStatus
                           };*/
                             //---20200608
                             select new
                             {
                                 JobNo = jd.JOB_ID,
                                 //ทำไมแปลงค่าแบบนี้แล้ว where enddate ไม่ได้ วันเวลาปิดงาน = jd.EndDate.Value.ToString("dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("en-US")),//jd.EndDate,//Convert.ToDateTime(jd.EndDate.Value.ToString("yyyy/MM/dd HH:mm:ss")),
                                 วันเวลาปิดงาน = jd.EndDate,
                                 STA_ID = t.STA_ID1 ?? t.STA_ID,
                                 Station = t.StationSys,
                                 VAN = t.EMP_ID3,
                                 หมายเหตุ = jd.Resole_Detail,
                                 FailCode = t.FAI_ID,
                                 อาการเสีย = t.JobFailure_Detail,
                                 ผู้แจ้ง = t.Informer ?? "" + " " + t.InPhone ?? "",
                                 JobStatus = t.JobStatus

                             };
                    //---------------------


                    dataGridView1.DataSource = j1.Distinct().ToList();
                    dataGridView1.Refresh();
                }
                else
                {
                    // this.DialogResult = DialogResult.a;
                    this.Close();
                    MessageBox.Show("ไม่มีข้อมูล JOB", "ผลการตรวจสอบ");
                }
            }
            else
            {
                if ((from t in db.vWait_DistributeJobs
                         //from jd in db.JOB_Details
                         // where
                         //(t.JOB_ID.Contains(tmpJobID) &&
                         //t.Opendate1.Contains(tmpJobOpenDate) &&
                         // t.StationSys.Contains(tmpStation))
                         // t.JOB_ID == jd.JOB_ID
                     select t).Count() > 0)
                {
                    var j1 = from t in db.vWait_DistributeJobs
                                 //from jd in db.JOB_Details
                                 //where
                                 //(t.JOB_ID.Contains(tmpJobID) &&
                                 // t.Opendate1.Contains(tmpJobOpenDate) &&
                                 //t.StationSys.Contains(tmpStation)) ///&&
                                 // t.JOB_ID == jd.JOB_ID
                             where t.JOBS_ID == "99" || t.JOBS_ID == "07"
                             select new
                             {
                                 JobNo = t.JOB_ID,
                                 Date = t.Opendate,
                                 STA_ID = t.STA_ID1 ?? t.STA_ID,
                                 Station = t.StationSys,
                                 VAN = t.EMP_ID3,
                                 FailCode = t.FAI_ID,
                                 อาการเสีย = t.JobFailure_Detail,
                                 ผู้แจ้ง = t.Informer ?? "" + " " + t.InPhone ?? "",
                                 JobStatus = t.JobStatus
                             };


                    dataGridView1.DataSource = j1.Distinct().ToList();
                    dataGridView1.Refresh();
                }
                else
                {
                    // this.DialogResult = DialogResult.a;
                    this.Close();
                    MessageBox.Show("ไม่มีข้อมูล JOB", "ผลการตรวจสอบ");
                }
            }
        }
        //20191017 private void showSupData(SMSManageDataContext db)
        private void showSupData(SMSManageDataContext db)
        {
            if (tmpJobID != "")
            {
                if ((from t in db.vWait_DistributeJobs
                     from jd in db.JOB_Details //20200609
                                               //20201207 where new string[] { "07", "99" }.Contains(t.JOBS_ID) &&
                     where new string[] { "07", "99" }.Contains(jd.JOBS_ID) &&
                     t.JOB_ID.Contains(tmpJobID) &&
                     t.Opendate1.Contains(tmpJobOpenDate) &&
                     t.StationSys.Contains(tmpStation) &&
                     t.EMP_ID3 == UserInfo.Van_ID &&
                     t.JOB_ID == jd.JOB_ID //20200609

                     //20191017
                     // &&
                     //t.EndDate.Value >= System.Convert.ToDateTime(dt) &&
                     //t.EndDate.Value < System.Convert.ToDateTime(dt).AddDays(+1)

                     select t).Count() > 0)
                {
                    var j1 = from t in db.vWait_DistributeJobs
                             from jd in db.JOB_Details //20200609
                                                       //20201207 where new string[] { "07", "99" }.Contains(t.JOBS_ID) &&
                             where new string[] { "07", "99" }.Contains(jd.JOBS_ID) &&
                             t.JOB_ID.Contains(tmpJobID) &&
                             t.Opendate1.Contains(tmpJobOpenDate) &&
                             t.StationSys.Contains(tmpStation) &&
                             t.EMP_ID3 == UserInfo.Van_ID //&&
                             //t.JOB_ID == jd.JOB_ID
                             && t.JOB_ID == jd.JOB_ID //20200609
                             /*//20200608
                             select new
                             {
                                 JobNo = t.JOB_ID,
                                 Date = t.Opendate,
                                 STA_ID = t.STA_ID1 ?? t.STA_ID,
                                 Station = t.StationSys,
                                 VAN = t.EMP_ID3,
                                 FailCode = t.FAI_ID,
                                 อาการเสีย = t.JobFailure_Detail,
                                 ผู้แจ้ง = t.Informer ?? "" + " " + t.InPhone ?? "",
                                 JobStatus = t.JobStatus
                             };*/
                             //---20200608
                             select new
                             {
                                 JobNo = jd.JOB_ID,
                                 //ทำไมแปลงค่าแบบนี้แล้ว where enddate ไม่ได้ วันเวลาปิดงาน = jd.EndDate.Value.ToString("dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("en-US")),//jd.EndDate,//Convert.ToDateTime(jd.EndDate.Value.ToString("yyyy/MM/dd HH:mm:ss")),
                                 วันเวลาปิดงาน = jd.EndDate,
                                 STA_ID = t.STA_ID1 ?? t.STA_ID,
                                 Station = t.StationSys,
                                 VAN = t.EMP_ID3,
                                 หมายเหตุ = jd.Resole_Detail,
                                 FailCode = t.FAI_ID,
                                 อาการเสีย = t.JobFailure_Detail,
                                 ผู้แจ้ง = t.Informer ?? "" + " " + t.InPhone ?? "",
                                 JobStatus = t.JobStatus

                             };
                    //---------------------

                    dataGridView1.DataSource = j1.Distinct().ToList();
                    dataGridView1.Refresh();
                }
                else
                {
                    this.Close();
                    MessageBox.Show("ไม่มีข้อมูล JOB", "ผลการตรวจสอบ");
                }
            }
            else
            {
                if ((from t in db.vWait_DistributeJobs
                         //from jd in db.JOB_Details
                     where new string[] { "07", "99" }.Contains(t.JOBS_ID) &&
                     //t.JOB_ID.Contains(tmpJobID) &&
                     //t.Opendate1.Contains(tmpJobOpenDate) &&
                     // t.StationSys.Contains(tmpStation) &&
                     t.EMP_ID3 == UserInfo.Van_ID //&&
                     //t.JOB_ID == jd.JOB_ID

                     select t).Count() > 0)
                {
                    var j1 = from t in db.vWait_DistributeJobs
                                 //from jd in db.JOB_Details
                             where new string[] { "07", "99" }.Contains(t.JOBS_ID) &&
                             //t.JOB_ID.Contains(tmpJobID) &&
                             //t.Opendate1.Contains(tmpJobOpenDate) &&
                             //t.StationSys.Contains(tmpStation) &&
                             t.EMP_ID3 == UserInfo.Van_ID //&&
                             //t.JOB_ID == jd.JOB_ID

                             select new
                             {
                                 JobNo = t.JOB_ID,
                                 Date = t.Opendate,
                                 STA_ID = t.STA_ID1 ?? t.STA_ID,
                                 Station = t.StationSys,
                                 VAN = t.EMP_ID3,
                                 FailCode = t.FAI_ID,
                                 อาการเสีย = t.JobFailure_Detail,
                                 ผู้แจ้ง = t.Informer ?? "" + " " + t.InPhone ?? "",
                                 JobStatus = t.JobStatus
                             };


                    dataGridView1.DataSource = j1.Distinct().ToList();
                    dataGridView1.Refresh();
                }
                else
                {
                    this.Close();
                    MessageBox.Show("ไม่มีข้อมูล JOB", "ผลการตรวจสอบ");
                }
            }
        }

        private void showJobByVAN(SMSManageDataContext db, string dt)
        {
            if (tmpJobID != "")
            {

                if ((from t in db.vWait_DistributeJobs
                     from jd in db.JOB_Details
                         //20201207 where new string[] { "07", "99" }.Contains(t.JOBS_ID) &&
                     where new string[] { "07", "99" }.Contains(jd.JOBS_ID) &&
                     t.JOB_ID.Contains(tmpJobID) &&
                     t.Opendate1.Contains(tmpJobOpenDate) &&
                     t.StationSys.Contains(tmpStation) &&
                     t.EMP_ID3 == UserInfo.Van_ID &&
                    t.JOB_ID == jd.JOB_ID
                     /*20191022 &&
                     jd.EndDate.Value >= System.Convert.ToDateTime(dt) &&
                     jd.EndDate.Value < System.Convert.ToDateTime(dt).AddDays(+1)
                      */


                     select t).Count() > 0)
                {
                    var j1 = from t in db.vWait_DistributeJobs
                             from jd in db.JOB_Details
                                 //20201207 where new string[] { "07", "99" }.Contains(t.JOBS_ID) &&
                             where new string[] { "07", "99" }.Contains(jd.JOBS_ID) &&
                             t.JOB_ID.Contains(tmpJobID) &&
                             t.Opendate1.Contains(tmpJobOpenDate) &&
                             t.StationSys.Contains(tmpStation) &&
                             t.EMP_ID3 == UserInfo.Van_ID &&
                             t.JOB_ID == jd.JOB_ID

                             /*20191022&&
                             jd.EndDate.Value >= System.Convert.ToDateTime(dt) &&
                            jd.EndDate.Value < System.Convert.ToDateTime(dt).AddDays(+1) 
                              */
                             /*//20200608
                             select new
                             {
                                 JobNo = t.JOB_ID,
                                 Date = t.Opendate,
                                 STA_ID = t.STA_ID1 ?? t.STA_ID,
                                 Station = t.StationSys,
                                 VAN = t.EMP_ID3,
                                 FailCode = t.FAI_ID,
                                 อาการเสีย = t.JobFailure_Detail,
                                 ผู้แจ้ง = t.Informer ?? "" + " " + t.InPhone ?? "",
                                 JobStatus = t.JobStatus
                             };*/
                             //---20200608
                             select new
                             {
                                 JobNo = jd.JOB_ID,
                                 //ทำไมแปลงค่าแบบนี้แล้ว where enddate ไม่ได้ วันเวลาปิดงาน = jd.EndDate.Value.ToString("dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("en-US")),//jd.EndDate,//Convert.ToDateTime(jd.EndDate.Value.ToString("yyyy/MM/dd HH:mm:ss")),
                                 วันเวลาปิดงาน = jd.EndDate,
                                 STA_ID = t.STA_ID1 ?? t.STA_ID,
                                 Station = t.StationSys,
                                 VAN = t.EMP_ID3,
                                 หมายเหตุ = jd.Resole_Detail,
                                 FailCode = t.FAI_ID,
                                 อาการเสีย = t.JobFailure_Detail,
                                 ผู้แจ้ง = t.Informer ?? "" + " " + t.InPhone ?? "",
                                 JobStatus = t.JobStatus

                             };
                    //---------------------


                    dataGridView1.DataSource = j1.Distinct().ToList();
                    dataGridView1.Refresh();
                }
                else
                {
                    this.Close();
                    MessageBox.Show("ไม่มีข้อมูล JOB", "ผลการตรวจสอบ");
                }
            }
            else
            {
                if ((from t in db.vWait_DistributeJobs
                     from jd in db.JOB_Details
                         //20201207 where new string[] { "07", "99" }.Contains(t.JOBS_ID) &&
                     where new string[] { "07", "99" }.Contains(jd.JOBS_ID) &&
                     //t.JOB_ID.Contains(tmpJobID) &&
                     // t.Opendate1.Contains(tmpJobOpenDate) &&
                     // t.StationSys.Contains(tmpStation) &&
                     t.EMP_ID3 == UserInfo.Van_ID &&
                    t.JOB_ID == jd.JOB_ID &&
                    jd.EndDate.Value >= System.Convert.ToDateTime(dt) &&
                    jd.EndDate.Value < System.Convert.ToDateTime(dt).AddDays(+1)

                     select t).Count() > 0)
                {
                    var j1 = from t in db.vWait_DistributeJobs
                             from jd in db.JOB_Details
                                 //20201207 where new string[] { "07", "99" }.Contains(t.JOBS_ID) &&
                             where new string[] { "07", "99" }.Contains(jd.JOBS_ID) &&
                             //t.JOB_ID.Contains(tmpJobID) &&
                             //t.Opendate1.Contains(tmpJobOpenDate) &&
                             //t.StationSys.Contains(tmpStation) &&
                             t.EMP_ID3 == UserInfo.Van_ID &&
                             t.JOB_ID == jd.JOB_ID &&
                             jd.EndDate.Value >= System.Convert.ToDateTime(dt) &&
                            jd.EndDate.Value < System.Convert.ToDateTime(dt).AddDays(+1)

                             select new
                             {
                                 JobNo = t.JOB_ID,
                                 Date = t.Opendate,
                                 STA_ID = t.STA_ID1 ?? t.STA_ID,
                                 Station = t.StationSys,
                                 VAN = t.EMP_ID3,
                                 FailCode = t.FAI_ID,
                                 อาการเสีย = t.JobFailure_Detail,
                                 ผู้แจ้ง = t.Informer ?? "" + " " + t.InPhone ?? "",
                                 JobStatus = t.JobStatus
                             };


                    dataGridView1.DataSource = j1.Distinct().ToList();
                    dataGridView1.Refresh();
                }
                else
                {
                    this.Close();
                    MessageBox.Show("ไม่มีข้อมูล JOB", "ผลการตรวจสอบ");
                }
            }
        }
        //ShowDataJobUpload
        private void ShowDataJobUpload(SMSManageDataContext dc)
        {
            var j1 = dc.JOBs
                       .Where(t => t.EMP_ID3 == UserInfo.Van_ID &&
                                       (t.IsUpload == "0"[0] || t.IsUpload == null) &&
                                       new string[] { "07", "99" }.Contains(t.JOBS_ID))
                        .Select(t => new
                        {
                            JobNo = t.JOB_ID,
                            Date = t.Opendate,
                            VAN = t.EMP_ID3,
                            FailCode = t.FAI_ID,
                            อาการเสีย = t.JobFailure_Detail,
                            ผู้แจ้ง = t.Informer ?? "" + " " + t.InPhone ?? ""
                        }).ToList();


            dataGridView1.DataSource = j1.ToList();
            dataGridView1.Refresh();

        }

        private void ShowDataJobAdd(SMSManageDataContext db)
        {
            if (tmpJobID != "")
            {
                if ((from t in db.vWait_DistributeJobs
                     where new string[] { "05", "06" }.Contains(t.JOBS_ID) &&
                     t.JOB_ID.Contains(tmpJobID) &&
                     t.Opendate1.Contains(tmpJobOpenDate) &&
                     t.StationSys.Contains(tmpStation) &&
                     t.EMP_ID3 == UserInfo.Van_ID
                     select t).Count() > 0)
                {
                    var j1 = from t in db.vWait_DistributeJobs
                             where new string[] { "05", "06" }.Contains(t.JOBS_ID) &&
                             t.JOB_ID.Contains(tmpJobID) &&
                             t.Opendate1.Contains(tmpJobOpenDate) &&
                             t.StationSys.Contains(tmpStation) &&
                             t.EMP_ID3 == UserInfo.Van_ID

                             select new
                             {
                                 JobNo = t.JOB_ID,
                                 Date = t.Opendate,
                                 STA_ID = t.STA_ID1 ?? t.STA_ID,
                                 Station = t.StationSys,
                                 VAN = t.EMP_ID3,
                                 FailCode = t.FAI_ID,
                                 อาการเสีย = t.JobFailure_Detail,
                                 ผู้แจ้ง = t.Informer ?? "" + " " + t.InPhone ?? "",
                                 JobStatus = t.JobStatus
                             };


                    dataGridView1.DataSource = j1.ToList();
                    dataGridView1.Refresh();
                }
                else
                {
                    this.Close();
                    MessageBox.Show("ไม่มีข้อมูล JOB", "ผลการตรวจสอบ");
                }
            }
            else
            {
                if ((from t in db.vWait_DistributeJobs
                     where new string[] { "05", "06" }.Contains(t.JOBS_ID) &&
                     // t.JOB_ID.Contains(tmpJobID) &&
                     // t.Opendate1.Contains(tmpJobOpenDate) &&
                     //t.StationSys.Contains(tmpStation) &&
                     t.EMP_ID3 == UserInfo.Van_ID
                     select t).Count() > 0)
                {
                    var j1 = from t in db.vWait_DistributeJobs
                             where new string[] { "05", "06" }.Contains(t.JOBS_ID) &&
                             // t.JOB_ID.Contains(tmpJobID) &&
                             //  t.Opendate1.Contains(tmpJobOpenDate) &&
                             //t.StationSys.Contains(tmpStation) &&
                             t.EMP_ID3 == UserInfo.Van_ID

                             select new
                             {
                                 JobNo = t.JOB_ID,
                                 Date = t.Opendate,
                                 STA_ID = t.STA_ID1 ?? t.STA_ID,
                                 Station = t.StationSys,
                                 VAN = t.EMP_ID3,
                                 FailCode = t.FAI_ID,
                                 อาการเสีย = t.JobFailure_Detail,
                                 ผู้แจ้ง = t.Informer ?? "" + " " + t.InPhone ?? "",
                                 JobStatus = t.JobStatus
                             };


                    dataGridView1.DataSource = j1.ToList();
                    dataGridView1.Refresh();
                }
                else
                {
                    this.Close();
                    MessageBox.Show("ไม่มีข้อมูล JOB", "ผลการตรวจสอบ");
                }
            }
        }

        /*20200608 //private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        //{
        //    try
        //    {
        //        if (e.RowIndex != -1)
        //        {
        //            tmpJobID = dataGridView1["JobNo", e.RowIndex].Value.ToString();

        //            //20200608
        //            //tmpEndDate = dataGridView1["วันเวลาปิดงาน", e.RowIndex].Value.ToString();

        //        }
        //    }
        //    catch (System.Exception ex)
        //    {
        //        MessageBox.Show("เกิดข้อผิดพลาด " + ex.Message, "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    }
        //}*/

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                /* 20200608
                if (e.RowIndex != -1)
                {
                    
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }*/
                if (e.RowIndex != -1)
                {
                    if (dataGridView1["JobNo", e.RowIndex].Value.ToString() != null)
                    {
                        tmpJobID = dataGridView1["JobNo", e.RowIndex].Value.ToString();

                        //20200608
                        if (tmpeditMode == "job_edit")
                        {
                            //tmpEndDate = dataGridView1["วันเวลาปิดงาน", e.RowIndex].Value.ToString();
                            tmpEndDate = dataGridView1["วันเวลาปิดงาน", e.RowIndex].Value.ToString();//.ToString("dd/MM/yyyy HH:mm:sss", new System.Globalization.CultureInfo("th-TH"));
                            //tmpEndDate = Convert.ToDateTime(dataGridView1["วันเวลาปิดงาน", e.RowIndex].Value.ToString());
                        }
                        this.DialogResult = DialogResult.OK;
                        this.Close();
                    }
                }

            }
            catch (System.Exception ex)
            {
                if (tmpeditMode == "job_edit" && dataGridView1["วันเวลาปิดงาน", e.RowIndex].Value == null)
                {//20201008
                    MessageBox.Show("เกิดข้อผิดพลาด วันเวลาปิดงาน กรุณาตรวจสอบ...", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
                else
                {
                    MessageBox.Show("เกิดข้อผิดพลาด " + ex.Message, "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        //private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        //{
        //    if (e.RowIndex != -1)
        //    {
        //        if (dataGridView1["JobNo", e.RowIndex].Value.ToString() != null)
        //        {
        //            tmpJobID = dataGridView1["JobNo", e.RowIndex].Value.ToString();

        //            if (tmpeditMode == "job_edit")
        //            {
        //                tmpEndDate = dataGridView1["วันเวลาปิดงาน", e.RowIndex].Value.ToString();
        //            }
        //        }
        //    }

        //}
    }
}
