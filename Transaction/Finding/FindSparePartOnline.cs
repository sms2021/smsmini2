﻿using System;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace SMSMINI.Transaction.Finding
{
    public partial class FindSparePartOnline : Form
    {
        private string tmpProjectNo = "";
        private string tmpPartNo = "";
        private string tmpPartName = "";
        private string tmpParameter = "";

        private DateTime dOpenJOB;
        private string tmpGopID = "";


        public string TmpProjectNo { get { return tmpProjectNo; } set { tmpProjectNo = value; } }
        public string TmpPartNo { get { return tmpPartNo; } set { tmpPartNo = value; } }
        public string TmpPartName { get { return tmpPartName; } set { tmpPartName = value; } }
        public string TmpParameter { get { return tmpParameter; } set { tmpParameter = value; } }

        public DateTime TmpDopenJOB { get { return dOpenJOB; } set { dOpenJOB = value; } }
        public string TmpGopID { get { return tmpGopID; } set { tmpGopID = value; } }

        public string tmpJobType { get; set; }
        public string tmpTYP_ID1 { get; set; }

        public string TmpGopName { get; set; }

        private string strConn = "";

        private string strPartNOPl = string.Empty;

        public string tmpContractNo { get; set; }

        public string SO_NO { get; set; }
        public string JOBNo { get; set; }
        public int ERP_orderline_id { get; set; }



        public FindSparePartOnline()
        {
            InitializeComponent();
            object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
            if (attributes.Length == 0)
            {
                this.Text = "SMS MINI [v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "]";

            }
            this.Text = ((AssemblyProductAttribute)attributes[0]).Product + " [v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "]";

        }

        private void FindSparePartOnline_Load(object sender, EventArgs e)
        {
            strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
            try
            {
                //using (SMSManageDataContext db = new SMSManageDataContext())
                using (DAL.SMSManage.SMSManageDataContext db = new DAL.SMSManage.SMSManageDataContext(strConn))

                {

                    var q = db.SpareParts
                        .Where(t =>
                                (t.SPA_ID.Contains(tmpParameter.Trim()) || t.SparePart1.Contains(tmpParameter.Trim())) &&
                                (t.IsDelete == false || t.IsDelete == null)
                              )
                            .Select(t => new
                            {
                                PartNo = t.SPA_ID,
                                PartName = t.SparePart1,
                                t.PricePerUnit,
                                ERP_orderline_id = 0
                            })
                            .ToList();

                    if (q.Count() > 0)
                    {
                        dataGridView1.DataSource = q;
                    }
                    else
                    {
                        MessageBox.Show("ไม่มีข้อมูล Part  [" + TmpParameter + "] กรุณาตรวจสอบ " + Environment.NewLine +
                            "เช่น:" + Environment.NewLine +
                            "   - ป้อนชื่อ Part (ให้ครบถ้น)" + Environment.NewLine +
                            "แล้วค้นหาอีกครั้ง" + Environment.NewLine +
                            "ถ้ายังไม่มีข้อมูล กรุณาติดต่อ ผู้จัดการส่วน ของคุณ", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }


            }
            catch (System.Exception ex)
            {
                MessageBox.Show("เกิดข้อผิดพลาด..." + Environment.NewLine + ex.Message, "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                string tmpSpareID = dataGridView1["PartNo", e.RowIndex].Value.ToString();
                strPartNOPl = tmpSpareID;
                btDownloadPriceList.Enabled = true;

                if (UserInfo.ConnectMode == "0")
                    strConn = Properties.Settings.Default["ServicesMSDBConnectionString"].ToString();
                else
                    strConn = Properties.Settings.Default["ServicesMSDBConnectionString"].ToString();


                using (DAL.SMSManage.SMSManageDataContext db = new DAL.SMSManage.SMSManageDataContext(strConn))
                {

                    if (!string.IsNullOrEmpty(tmpContractNo))
                    {

                        #region  tmpContractNo !=""

                        var conP = (from t in db.Conf_Conditions
                                    from sp in db.SpareParts
                                    where (t.SPA_ID == sp.SPA_ID &&
                                        t.SPA_ID == tmpSpareID &&
                                        t.GOP_ID == tmpGopID &&
                                        t.TYP_ID == tmpJobType &&
                                        t.TYP_ID1 == tmpTYP_ID1) &&
                                        t.ContractNo.Contains(tmpContractNo)
                                    select new
                                    {
                                        PartNo = sp.SPA_ID,
                                        PartName = sp.SparePart1,
                                        PricesList = t.PriceList
                                    }).FirstOrDefault();

                        if (conP != null && conP.PricesList > 0)
                        {
                            var conP2 = (from t in db.Conf_Conditions
                                         from sp in db.SpareParts
                                         where (t.SPA_ID == sp.SPA_ID &&
                                             t.SPA_ID == tmpSpareID &&
                                             t.GOP_ID == tmpGopID &&
                                             t.TYP_ID == tmpJobType &&
                                             t.TYP_ID1 == tmpTYP_ID1) &&
                                             t.ContractNo.Contains(tmpContractNo)
                                         select new
                                         {
                                             PartNo = sp.SPA_ID,
                                             PartName = sp.SparePart1,
                                             PricesList = t.PriceList,
                                             ContractNo = t.ContractNo
                                         }).Distinct();
                            if (conP2 != null)
                            {
                                dataGridView2.DataSource = conP2.ToList();
                                dataGridView2.Refresh();
                            }




                        }
                        else
                        {
                            #region Check PriceList

                            ////var pril = (from t in db.SparePart_Quarters
                            ////            where (t.StartDate <= dOpenJOB && t.EndDate >= dOpenJOB) && t.GOP_ID == tmpGopID
                            ////            select t).FirstOrDefault();
                            ////if (pril != null)
                            ////{
                            ////    var qsp = (from sp in db.SpareParts
                            ////               from spl in db.SparePart_Prices_Lists
                            ////               where ((sp.SPA_ID == spl.SPA_ID) &&
                            ////                     (sp.SPA_ID == tmpSpareID) &&
                            ////                     spl.PRI_ID == pril.PRI_ID) &&
                            ////                    (sp.IsDelete == false || sp.IsDelete == null)

                            ////               select new
                            ////               {
                            ////                   PartNo = sp.SPA_ID,
                            ////                   PartName = sp.SparePart1,
                            ////                   PricesList = spl.Prices
                            ////               }).Distinct();


                            ////    dataGridView2.DataSource = qsp.ToList();
                            ////    dataGridView2.Refresh();
                            ////}

                            #endregion
                        }

                        #endregion

                    }
                    else
                    {

                        #region  tmpContractNo ==""

                        var conP = (from t in db.Conf_Conditions
                                    from sp in db.SpareParts
                                    where (t.SPA_ID == sp.SPA_ID &&
                                        t.SPA_ID == tmpSpareID &&
                                        t.GOP_ID == tmpGopID &&
                                        t.TYP_ID == tmpJobType &&
                                        t.TYP_ID1 == tmpTYP_ID1)
                                    select new
                                    {
                                        PartNo = sp.SPA_ID,
                                        PartName = sp.SparePart1,
                                        PricesList = t.PriceList
                                    }).FirstOrDefault();

                        if (conP != null && conP.PricesList > 0)
                        {
                            var conP2 = (from t in db.Conf_Conditions
                                         from sp in db.SpareParts
                                         where (t.SPA_ID == sp.SPA_ID &&
                                             t.SPA_ID == tmpSpareID &&
                                             t.GOP_ID == tmpGopID &&
                                             t.TYP_ID == tmpJobType &&
                                             t.TYP_ID1 == tmpTYP_ID1)
                                         select new
                                         {
                                             PartNo = sp.SPA_ID,
                                             PartName = sp.SparePart1,
                                             PricesList = t.PriceList,
                                             ContractNo = t.ContractNo
                                         }).Distinct();
                            if (conP2 != null)
                            {
                                dataGridView2.DataSource = conP2.ToList();
                                dataGridView2.Refresh();
                            }




                        }
                        else
                        {
                            #region Check PriceList

                            ////var pril = (from t in db.SparePart_Quarters
                            ////            where (t.StartDate <= dOpenJOB && t.EndDate >= dOpenJOB) && t.GOP_ID == tmpGopID
                            ////            select t).FirstOrDefault();
                            ////if (pril != null)
                            ////{
                            ////    var qsp = (from sp in db.SpareParts
                            ////               from spl in db.SparePart_Prices_Lists
                            ////               where ((sp.SPA_ID == spl.SPA_ID) &&
                            ////                     (sp.SPA_ID == tmpSpareID) &&
                            ////                     spl.PRI_ID == pril.PRI_ID) &&
                            ////                    (sp.IsDelete == false || sp.IsDelete == null)

                            ////               select new
                            ////               {
                            ////                   PartNo = sp.SPA_ID,
                            ////                   PartName = sp.SparePart1,
                            ////                   PricesList = spl.Prices
                            ////               }).Distinct();


                            ////    dataGridView2.DataSource = qsp.ToList();
                            ////    dataGridView2.Refresh();
                            ////}

                            #endregion
                        }

                        #endregion
                    }

                }

            }
            else
                btDownloadPriceList.Enabled = false;
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex != -1)
                {
                    tmpPartNo = dataGridView1["PartNo", e.RowIndex].Value.ToString();
                    tmpPartName = dataGridView1["PartName", e.RowIndex].Value.ToString();
                    ERP_orderline_id = int.Parse(dataGridView1["ERP_orderline_id", e.RowIndex].Value.ToString());
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("เกิดข้อผิดพลาด " + ex.Message, "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
