﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace SMSMINI.Transaction.Finding
{
    public partial class FindJOB_PhotoReport : Form
    {
        public string JOBNo { get; set; }

        public FindJOB_PhotoReport()
        {
            InitializeComponent();
        }

        private void toolStripButton_Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void toolStripButton_FindStation_Click(object sender, EventArgs e)
        {
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            {

                var q = (from j in dc.JOB_PhotoReports
                         join _j in dc.JOBs on j.JOB_ID equals _j.JOB_ID
                         join _s in dc.Stations on _j.STA_ID equals _s.STA_ID
                         join _jd in dc.JOB_PhotoReport_Details on j.JOB_ID equals _jd.JOB_ID into jds
                         from jd in jds.DefaultIfEmpty()

                         where j.JOB_ID.Contains(toolStripText_txtFind.Text.Trim())
                         select new
                         {
                             j.JOB_ID,
                             อาการ = j.Failure_Detail,
                             VAN = j.VAN_ID,
                             สถานี = (_s.STA_ID1 ?? _s.STA_ID) + " " + _s.StationSys
                         }).Distinct();

                if (q.Count() > 0)
                    dataGridView1.DataSource = q.ToList();
                else
                    MessageBox.Show("ไม่มีข้อมูล JOB: " + toolStripText_txtFind.Text.Trim() + " กรุณาป้อนใหม่...", "ผลการค้นหา", MessageBoxButtons.OK);
            }
        }

        private void FindJOB_PhotoReport_Load(object sender, EventArgs e)
        {
            toolStripText_txtFind.Focus();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex != -1)
                {
                    JOBNo = dataGridView1["JOB_ID", e.RowIndex].Value.ToString();

                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("เกิดข้อผิดพลาด " + ex.Message, "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                if (e.RowIndex != -1)
                {
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("เกิดข้อผิดพลาด " + ex.Message, "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void toolStripText_txtFind_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                toolStripButton_FindStation_Click(null, null);
        }
    }
}
