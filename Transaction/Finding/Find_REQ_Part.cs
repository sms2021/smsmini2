﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace SMSMINI.Transaction.Finding
{
    public partial class Find_REQ_Part : Form
    {
        public string _SPA_ID { get; set; }
        public string _PartName { get; set; }

        public Find_REQ_Part()
        {
            InitializeComponent();
        }

        private void Find_REQ_Part_Load(object sender, EventArgs e)
        {
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            {
                var q = from t in dc.SpareParts
                        where (t.IsDelete == false || t.IsDelete == null) &&
                        t.SPA_ID.Contains(_SPA_ID)
                        select new
                        {
                            t.SPA_ID,
                            Part = t.SparePart1

                        };


                dataGridView1.DataSource = q.ToList();

            }
        }

        private void dataGridView1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //try
            //{
            //    if (e.RowIndex != -1)
            //    {
            //        _SPA_ID = dataGridView1["SPA_ID", e.RowIndex].Value.ToString();
            //        _PartName = dataGridView1["Part", e.RowIndex].Value.ToString();


            //        this.DialogResult = DialogResult.OK;
            //        this.Close();
            //    }
            //}
            //catch (System.Exception ex)
            //{
            //    MessageBox.Show("เกิดข้อผิดพลาด " + ex.Message, "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex != -1)
                {
                    _SPA_ID = dataGridView1["SPA_ID", e.RowIndex].Value.ToString();
                    _PartName = dataGridView1["Part", e.RowIndex].Value.ToString();


                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("เกิดข้อผิดพลาด " + ex.Message, "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
