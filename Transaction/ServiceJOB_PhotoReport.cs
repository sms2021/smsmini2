﻿using CrystalDecisions.Shared;
using System;
using System.Data;
using System.Windows.Forms;

namespace SMSMINI.Transaction
{
    public partial class ServiceJOB_PhotoReport : Form
    {
        public string _JOBNo { get; set; }

        //20141016 SMSMINI.Transaction.Report.rptJOBPhotoReport2 rpt = new SMSMINI.Transaction.Report.rptJOBPhotoReport2();
        SMSMINI.Transaction.Report.rptJOBPhotoReport2 rptPhoto = new SMSMINI.Transaction.Report.rptJOBPhotoReport2();

        string pJOBNo = "";

        public ServiceJOB_PhotoReport()
        {
            InitializeComponent();
        }
        private string strConn = "";
        string strUsr = "sa";
        string strPwd = "enabler";
        string strServer = "sms.flowco.co.th,1414";
        string strDB = "ServicesMSDB";

        private void ServiceJOB_PhotoReport_Load(object sender, EventArgs e)
        {
            this.Cursor = Cursors.AppStarting;

            pJOBNo = _JOBNo.Trim();
            /*
            //20141015 
            rpt.SetDatabaseLogon("sa", "enabler", "sms.flowco.co.th,1414", "ServicesMSDB");

            rpt.SetParameterValue("pJOB_ID", pJOB);
            //rpt.SetDatabaseLogon(strUsr, strPwd, strServer, strDB);

            crystalReportViewer1.ReportSource = rpt;
            crystalReportViewer1.Refresh();
            this.Cursor = Cursors.Default ;
             */
            strConn = Properties.Settings.Default["ServicesMSDBConnectionString"].ToString();
            //var rptPhoto = new SMSMINI.Transaction.Report.rptJOBPhotoReport2();
            using (System.Data.SqlClient.SqlConnection Conn = new System.Data.SqlClient.SqlConnection(strConn))
            {
                if (Conn.State == ConnectionState.Closed)
                    Conn.Open();

                DataTable tmpTable = new DataTable();
                string sql = " SELECT  *  FROM v_rpt_Services_JOBPhotoReport where  JOB_ID = '" + pJOBNo + "' ";
                System.Data.SqlClient.SqlDataReader dr = null;

                System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(sql, Conn);
                DataSet ds = new DataSet();
                da.Fill(ds);


                rptPhoto.SetDatabaseLogon(strUsr, strPwd, strServer, strDB);
                rptPhoto.SetDataSource(ds.Tables[0].DefaultView);
                rptPhoto.SetParameterValue("pJOB_ID", pJOBNo);


                crystalReportViewer1.ReportSource = rptPhoto;
                crystalReportViewer1.Refresh();
                this.Cursor = Cursors.Default;

            }
        }

        private void btPrint_Click(object sender, EventArgs e)
        {
            Export2PDF();
        }

        private void Export2PDF()
        {

            string f = "";
            SaveFileDialog DialogSave = new SaveFileDialog();
            DialogSave.DefaultExt = "xls";
            DialogSave.Filter = "Adobe Acrobat  (*.pdf)|*.pdf|All files (*.*)|*.*";
            DialogSave.AddExtension = true;
            DialogSave.RestoreDirectory = true;
            DialogSave.Title = "Where do you want to save the file?";
            DialogSave.InitialDirectory = @"C:/";

            string sv = "JOBPhotoReport_" + pJOBNo + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".pdf";

            DialogSave.FileName = sv;

            if (DialogSave.ShowDialog() == DialogResult.OK)
            {
                this.Cursor = Cursors.AppStarting;

                f = DialogSave.FileName.ToString();
                DialogSave.Dispose();
                DialogSave = null;

                try
                {
                    ExportOptions CrExportOptions;
                    DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                    PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();
                    CrDiskFileDestinationOptions.DiskFileName = f;// "c:\\ServicesReport_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".pdf";


                    CrExportOptions = rptPhoto.ExportOptions;
                    {
                        CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                        CrExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                        CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
                        CrExportOptions.FormatOptions = CrFormatTypeOptions;

                    }
                    rptPhoto.Export();
                    rptPhoto.Close();

                    this.Cursor = Cursors.Default;

                    if (System.IO.File.Exists(f))
                    {
                        System.Diagnostics.Process.Start(f);
                    }
                }

                catch (Exception ex)
                {
                    MessageBox.Show("ข้อผิดพลาด " + ex.ToString(), "เกิดข้อผิดพลาดในการ Export ไฟล์ JOB Photo Report PDF");
                }
            }
        }
    }
}
