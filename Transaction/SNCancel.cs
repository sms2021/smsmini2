﻿namespace SMSMINI.Transaction
{
    public class SNCancel
    {
        public string POI_ID { get; set; }
        public string SNold { get; set; }
        public string SNNew { get; set; }
    }
}
