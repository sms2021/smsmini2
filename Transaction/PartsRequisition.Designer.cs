﻿namespace SMSMINI.Transaction
{
    partial class PartsRequisition
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PartsRequisition));
            this.toolStrip_Mnu = new System.Windows.Forms.ToolStrip();
            this.toolStripButton_New = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_Edit = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStrip_txtCloseJOB = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_FindStation = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_Save = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_Save_ERP = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_Cancel = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_Exit = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel_Dateime = new System.Windows.Forms.ToolStripLabel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtREQDetail = new System.Windows.Forms.TextBox();
            this.lblMode = new System.Windows.Forms.Label();
            this.chkIsSend_ERP = new System.Windows.Forms.CheckBox();
            this.txtVAN = new System.Windows.Forms.TextBox();
            this.dateTimePicker_PartREQ = new System.Windows.Forms.DateTimePicker();
            this.txtPartRED_ID = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblSPA_ID = new System.Windows.Forms.Label();
            this.txtREQPart_Detail = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btAdd = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.txtQuantity = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtPartName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPart_NO = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.cbtCancel = new System.Windows.Forms.DataGridViewButtonColumn();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.toolStrip_Mnu.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip_Mnu
            // 
            this.toolStrip_Mnu.Font = new System.Drawing.Font("Tahoma", 10F);
            this.toolStrip_Mnu.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip_Mnu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton_New,
            this.toolStripButton_Edit,
            this.toolStripSeparator5,
            this.toolStripLabel1,
            this.toolStripSeparator4,
            this.toolStrip_txtCloseJOB,
            this.toolStripSeparator2,
            this.toolStripButton_FindStation,
            this.toolStripSeparator3,
            this.toolStripButton_Save,
            this.toolStripButton_Save_ERP,
            this.toolStripButton_Cancel,
            this.toolStripSeparator1,
            this.toolStripButton_Exit,
            this.toolStripLabel_Dateime});
            this.toolStrip_Mnu.Location = new System.Drawing.Point(0, 0);
            this.toolStrip_Mnu.Name = "toolStrip_Mnu";
            this.toolStrip_Mnu.Size = new System.Drawing.Size(1035, 39);
            this.toolStrip_Mnu.TabIndex = 4;
            // 
            // toolStripButton_New
            // 
            this.toolStripButton_New.Image = global::SMSMINI.Properties.Resources.Add;
            this.toolStripButton_New.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_New.Name = "toolStripButton_New";
            this.toolStripButton_New.Size = new System.Drawing.Size(70, 36);
            this.toolStripButton_New.Text = "New";
            this.toolStripButton_New.Click += new System.EventHandler(this.toolStripButton_New_Click);
            // 
            // toolStripButton_Edit
            // 
            this.toolStripButton_Edit.Image = global::SMSMINI.Properties.Resources.Edit;
            this.toolStripButton_Edit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Edit.Name = "toolStripButton_Edit";
            this.toolStripButton_Edit.Size = new System.Drawing.Size(67, 36);
            this.toolStripButton_Edit.Text = "Edit";
            this.toolStripButton_Edit.Click += new System.EventHandler(this.toolStripButton_Edit_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(80, 36);
            this.toolStripLabel1.Text = "วันที่ปิด JOB";
            this.toolStripLabel1.Click += new System.EventHandler(this.toolStripLabel1_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStrip_txtCloseJOB
            // 
            this.toolStrip_txtCloseJOB.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.toolStrip_txtCloseJOB.Name = "toolStrip_txtCloseJOB";
            this.toolStrip_txtCloseJOB.Size = new System.Drawing.Size(100, 39);
            this.toolStrip_txtCloseJOB.Text = "yyyy-mm-dd";
            this.toolStrip_txtCloseJOB.KeyDown += new System.Windows.Forms.KeyEventHandler(this.toolStrip_txtCloseJOB_KeyDown);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripButton_FindStation
            // 
            this.toolStripButton_FindStation.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_FindStation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.toolStripButton_FindStation.Image = global::SMSMINI.Properties.Resources.Find48;
            this.toolStripButton_FindStation.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_FindStation.Name = "toolStripButton_FindStation";
            this.toolStripButton_FindStation.Size = new System.Drawing.Size(36, 36);
            this.toolStripButton_FindStation.ToolTipText = "ค้นหาข้อมูล";
            this.toolStripButton_FindStation.Click += new System.EventHandler(this.toolStripButton_FindStation_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripButton_Save
            // 
            this.toolStripButton_Save.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.toolStripButton_Save.ForeColor = System.Drawing.SystemColors.Desktop;
            this.toolStripButton_Save.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Save.Image")));
            this.toolStripButton_Save.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Save.Name = "toolStripButton_Save";
            this.toolStripButton_Save.Size = new System.Drawing.Size(111, 36);
            this.toolStripButton_Save.Text = "บันทึกใบเบิก";
            this.toolStripButton_Save.ToolTipText = "Upload JOB";
            this.toolStripButton_Save.Click += new System.EventHandler(this.toolStripButton_Save_Click);
            // 
            // toolStripButton_Save_ERP
            // 
            this.toolStripButton_Save_ERP.Enabled = false;
            this.toolStripButton_Save_ERP.ForeColor = System.Drawing.SystemColors.Desktop;
            this.toolStripButton_Save_ERP.Image = global::SMSMINI.Properties.Resources.paste;
            this.toolStripButton_Save_ERP.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Save_ERP.Name = "toolStripButton_Save_ERP";
            this.toolStripButton_Save_ERP.Size = new System.Drawing.Size(147, 36);
            this.toolStripButton_Save_ERP.Text = "ส่งใบเบิกเข้า ERP";
            // 
            // toolStripButton_Cancel
            // 
            this.toolStripButton_Cancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.toolStripButton_Cancel.ForeColor = System.Drawing.SystemColors.Desktop;
            this.toolStripButton_Cancel.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Cancel.Image")));
            this.toolStripButton_Cancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Cancel.Name = "toolStripButton_Cancel";
            this.toolStripButton_Cancel.Size = new System.Drawing.Size(87, 36);
            this.toolStripButton_Cancel.Text = "Cancel";
            this.toolStripButton_Cancel.ToolTipText = "ยกเลิกข้อมูล";
            this.toolStripButton_Cancel.Click += new System.EventHandler(this.toolStripButton_Cancel_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripButton_Exit
            // 
            this.toolStripButton_Exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.toolStripButton_Exit.ForeColor = System.Drawing.SystemColors.Desktop;
            this.toolStripButton_Exit.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Exit.Image")));
            this.toolStripButton_Exit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Exit.Name = "toolStripButton_Exit";
            this.toolStripButton_Exit.Size = new System.Drawing.Size(66, 36);
            this.toolStripButton_Exit.Text = "Exit";
            this.toolStripButton_Exit.ToolTipText = "ออกจากหน้าจอ";
            this.toolStripButton_Exit.Click += new System.EventHandler(this.toolStripButton_Exit_Click);
            // 
            // toolStripLabel_Dateime
            // 
            this.toolStripLabel_Dateime.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripLabel_Dateime.Font = new System.Drawing.Font("Tahoma", 10F);
            this.toolStripLabel_Dateime.ForeColor = System.Drawing.SystemColors.Desktop;
            this.toolStripLabel_Dateime.Name = "toolStripLabel_Dateime";
            this.toolStripLabel_Dateime.Size = new System.Drawing.Size(0, 36);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtREQDetail);
            this.groupBox1.Controls.Add(this.lblMode);
            this.groupBox1.Controls.Add(this.chkIsSend_ERP);
            this.groupBox1.Controls.Add(this.txtVAN);
            this.groupBox1.Controls.Add(this.dateTimePicker_PartREQ);
            this.groupBox1.Controls.Add(this.txtPartRED_ID);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(13, 43);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(796, 103);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ข้อมูลการเบิก";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(59, 76);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(101, 17);
            this.label7.TabIndex = 10;
            this.label7.Text = "รายละเอียดใบเบิก";
            // 
            // txtREQDetail
            // 
            this.txtREQDetail.Location = new System.Drawing.Point(166, 73);
            this.txtREQDetail.Name = "txtREQDetail";
            this.txtREQDetail.Size = new System.Drawing.Size(594, 23);
            this.txtREQDetail.TabIndex = 9;
            // 
            // lblMode
            // 
            this.lblMode.AutoSize = true;
            this.lblMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblMode.ForeColor = System.Drawing.Color.LimeGreen;
            this.lblMode.Location = new System.Drawing.Point(90, 0);
            this.lblMode.Name = "lblMode";
            this.lblMode.Size = new System.Drawing.Size(53, 17);
            this.lblMode.TabIndex = 8;
            this.lblMode.Text = "[NEW]";
            this.lblMode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // chkIsSend_ERP
            // 
            this.chkIsSend_ERP.AutoSize = true;
            this.chkIsSend_ERP.Enabled = false;
            this.chkIsSend_ERP.Location = new System.Drawing.Point(648, 49);
            this.chkIsSend_ERP.Name = "chkIsSend_ERP";
            this.chkIsSend_ERP.Size = new System.Drawing.Size(121, 21);
            this.chkIsSend_ERP.TabIndex = 7;
            this.chkIsSend_ERP.Text = "ส่งใบเบิกให้ ERP";
            this.chkIsSend_ERP.UseVisualStyleBackColor = true;
            // 
            // txtVAN
            // 
            this.txtVAN.BackColor = System.Drawing.SystemColors.Control;
            this.txtVAN.Enabled = false;
            this.txtVAN.Location = new System.Drawing.Point(286, 47);
            this.txtVAN.Name = "txtVAN";
            this.txtVAN.Size = new System.Drawing.Size(356, 23);
            this.txtVAN.TabIndex = 6;
            // 
            // dateTimePicker_PartREQ
            // 
            this.dateTimePicker_PartREQ.CalendarMonthBackground = System.Drawing.SystemColors.Control;
            this.dateTimePicker_PartREQ.CalendarTitleForeColor = System.Drawing.SystemColors.Control;
            this.dateTimePicker_PartREQ.Enabled = false;
            this.dateTimePicker_PartREQ.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker_PartREQ.Location = new System.Drawing.Point(166, 47);
            this.dateTimePicker_PartREQ.Name = "dateTimePicker_PartREQ";
            this.dateTimePicker_PartREQ.Size = new System.Drawing.Size(114, 23);
            this.dateTimePicker_PartREQ.TabIndex = 5;
            // 
            // txtPartRED_ID
            // 
            this.txtPartRED_ID.BackColor = System.Drawing.SystemColors.Control;
            this.txtPartRED_ID.Enabled = false;
            this.txtPartRED_ID.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtPartRED_ID.Location = new System.Drawing.Point(24, 47);
            this.txtPartRED_ID.Name = "txtPartRED_ID";
            this.txtPartRED_ID.Size = new System.Drawing.Size(136, 23);
            this.txtPartRED_ID.TabIndex = 4;
            this.txtPartRED_ID.Text = "9999REQ999";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(163, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "วันที่การเบิก";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(283, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "พนักงานเบิก";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "เลขที่การเบิก";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblSPA_ID);
            this.groupBox2.Controls.Add(this.txtREQPart_Detail);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.btAdd);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.txtQuantity);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.txtPartName);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txtPart_NO);
            this.groupBox2.Location = new System.Drawing.Point(13, 153);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(796, 95);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "รายการเบิก";
            // 
            // lblSPA_ID
            // 
            this.lblSPA_ID.AutoSize = true;
            this.lblSPA_ID.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lblSPA_ID.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lblSPA_ID.Location = new System.Drawing.Point(72, 1);
            this.lblSPA_ID.Name = "lblSPA_ID";
            this.lblSPA_ID.Size = new System.Drawing.Size(77, 17);
            this.lblSPA_ID.TabIndex = 12;
            this.lblSPA_ID.Text = "lblSPA_ID";
            // 
            // txtREQPart_Detail
            // 
            this.txtREQPart_Detail.Location = new System.Drawing.Point(150, 66);
            this.txtREQPart_Detail.Name = "txtREQPart_Detail";
            this.txtREQPart_Detail.Size = new System.Drawing.Size(619, 23);
            this.txtREQPart_Detail.TabIndex = 10;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(1, 72);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(148, 17);
            this.label8.TabIndex = 11;
            this.label8.Text = "รายละเอียด การเบิกอะไหล่";
            // 
            // btAdd
            // 
            this.btAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btAdd.Location = new System.Drawing.Point(671, 30);
            this.btAdd.Name = "btAdd";
            this.btAdd.Size = new System.Drawing.Size(98, 34);
            this.btAdd.TabIndex = 6;
            this.btAdd.Text = "เพิ่มรายการ";
            this.btAdd.UseVisualStyleBackColor = true;
            this.btAdd.Click += new System.EventHandler(this.btAdd_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(532, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 17);
            this.label6.TabIndex = 5;
            this.label6.Text = "จำนวนเบิก";
            // 
            // txtQuantity
            // 
            this.txtQuantity.Location = new System.Drawing.Point(535, 41);
            this.txtQuantity.Name = "txtQuantity";
            this.txtQuantity.Size = new System.Drawing.Size(130, 23);
            this.txtQuantity.TabIndex = 4;
            this.txtQuantity.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtQuantity_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(150, 21);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 17);
            this.label5.TabIndex = 3;
            this.label5.Text = "ชื่ออะไหล่";
            // 
            // txtPartName
            // 
            this.txtPartName.BackColor = System.Drawing.SystemColors.Control;
            this.txtPartName.Enabled = false;
            this.txtPartName.Location = new System.Drawing.Point(150, 41);
            this.txtPartName.Name = "txtPartName";
            this.txtPartName.Size = new System.Drawing.Size(379, 23);
            this.txtPartName.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 17);
            this.label4.TabIndex = 1;
            this.label4.Text = "รหัสอะไหล่";
            // 
            // txtPart_NO
            // 
            this.txtPart_NO.Location = new System.Drawing.Point(24, 41);
            this.txtPart_NO.Name = "txtPart_NO";
            this.txtPart_NO.Size = new System.Drawing.Size(120, 23);
            this.txtPart_NO.TabIndex = 0;
            this.txtPart_NO.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPart_NO_KeyDown);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cbtCancel});
            this.dataGridView1.Location = new System.Drawing.Point(13, 254);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(1010, 381);
            this.dataGridView1.TabIndex = 8;
            this.dataGridView1.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dataGridView1_RowsAdded);
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // cbtCancel
            // 
            this.cbtCancel.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.cbtCancel.FillWeight = 45F;
            this.cbtCancel.HeaderText = "";
            this.cbtCancel.Name = "cbtCancel";
            this.cbtCancel.Text = "ยกเลิก";
            this.cbtCancel.ToolTipText = "ยกเลิก";
            this.cbtCancel.UseColumnTextForButtonValue = true;
            this.cbtCancel.Width = 5;
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.textBox1.ForeColor = System.Drawing.Color.Red;
            this.textBox1.Location = new System.Drawing.Point(817, 51);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(206, 61);
            this.textBox1.TabIndex = 10;
            this.textBox1.Text = "ต้องเชื่อมต่ออินเทอร์เนต ขณะใช้งานเท่านั้น";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // PartsRequisition
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1035, 647);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.toolStrip_Mnu);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "PartsRequisition";
            this.ShowIcon = false;
            this.Text = "ข้อมูลเบิกอะไหล่";
            this.Load += new System.EventHandler(this.PartsRequisition_Load);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.PartsRequisition_FormClosed);
            this.toolStrip_Mnu.ResumeLayout(false);
            this.toolStrip_Mnu.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip_Mnu;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripButton_FindStation;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton toolStripButton_Save;
        private System.Windows.Forms.ToolStripButton toolStripButton_Cancel;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButton_Exit;
        private System.Windows.Forms.ToolStripLabel toolStripLabel_Dateime;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripTextBox toolStrip_txtCloseJOB;
        private System.Windows.Forms.ToolStripButton toolStripButton_Save_ERP;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtPartRED_ID;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtVAN;
        private System.Windows.Forms.DateTimePicker dateTimePicker_PartREQ;
        private System.Windows.Forms.CheckBox chkIsSend_ERP;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtPart_NO;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtQuantity;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtPartName;
        private System.Windows.Forms.Button btAdd;
        private System.Windows.Forms.ToolStripButton toolStripButton_New;
        private System.Windows.Forms.ToolStripButton toolStripButton_Edit;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewButtonColumn cbtCancel;
        private System.Windows.Forms.Label lblMode;
        private System.Windows.Forms.TextBox textBox1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.TextBox txtREQDetail;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtREQPart_Detail;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblSPA_ID;
    }
}