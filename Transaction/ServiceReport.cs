﻿using CrystalDecisions.Shared;
#pragma warning disable CS0246 // The type or namespace name 'Janawat' could not be found (are you missing a using directive or an assembly reference?)
using Janawat.Application.Data;
#pragma warning restore CS0246 // The type or namespace name 'Janawat' could not be found (are you missing a using directive or an assembly reference?)
using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Windows.Forms;



namespace SMSMINI.Transaction
{
    public partial class ServiceReport : Form
    {
        //private ProgressBar.FormProgress m_fmProgress = null;

        //private string strConn = "";
        public string strConn { get; set; }

        private string strConnCus = "";
        private string strConnQuo = "";
        private string strConnLayout = "";
        string tmpStartDate = "";
        string tmpJobID = "";

        string tmpEndDate = "";
        string tmpEndTime = "";


        public string TmpJobID { get { return tmpJobID; } set { tmpJobID = value; } }

        int partCount = 0;
        public int PartCount { get { return partCount; } set { partCount = value; } }


        public string TmpEndDate { get { return tmpEndDate; } set { tmpEndDate = value; } }
        public string TmpEndTime { get { return tmpEndTime; } set { tmpEndTime = value; } }

        //--20180423 
        string chkUpload = "";
        public string vchkUpload { get { return chkUpload; } set { chkUpload = value; } }

        public bool isSMSbackup { get; set; }

        string strUsr = "sa";
        string strPwd = "enabler";
        //20210623 string strServer = ".\\SQLEXPRESS";
        //20210623 string strDB = "SMSminiDB";
        string strServer = "sms.flowco.co.th,1414";//"192.169.1.119\\SQL2016"; //20210623
        string strDB = "ServicesMSDB"; //20210623
        
        bool isReprintSV = false;
        string sv = "";
        string IsCus = "";




        string filePartPriceListPath = "";

        public ServiceReport()
        {
            InitializeComponent();

            object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
            if (attributes.Length == 0)
            {
                this.Text = "SMS MINI [v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "]";

            }
            this.Text = ((AssemblyProductAttribute)attributes[0]).Product + " [v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "]";

        }

        private void ServiceReport_FormClosed(object sender, FormClosedEventArgs e)
        {
            DialogResult = DialogResult.Abort;
        }

        private void ServiceReport_Load(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            try
            {


                if (strConn.Contains("ServicesMSDB_1314"))
                    strDB = "ServicesMSDB_1314";
                else if (strConn.Contains("ServicesMSDB_151617"))
                    strDB = "ServicesMSDB_151617";
                else
                    strDB = "ServicesMSDB";

                //20210623 strServer = "sms.flowco.co.th,1414";
                //20210623 strServer = "192.169.1.119\\SQL2016";

                //newProgressbar();

                //checkPrintSV();
                isReprintSV = true;

                setTmpStartDate();

                //20191007
                setComp();



                //showReport();
                showReport("");

                //closeProgress();

                checkBox_2pdf.Checked = true;

                //----20180423-------
                if (chkUpload == "0")
                {
                    btPrint.Visible = false;
                    crystalReportViewer1.ShowExportButton = false;
                }
                else
                {
                    btPrint.Visible = true;
                    crystalReportViewer1.ShowExportButton = true;
                }
                //------------------------
            }
            catch (System.Exception ex)
            {
                //closeProgress();
                MessageBox.Show("เกิดข้อผิดพลาด Error " + ex.Message, "ข้อผิดพลาด");
            }
            Cursor.Current = Cursors.Default;
        }



        private void checkPrintSV()
        {
            string sdt = DateTime.Now.ToShortDateString();
            string edt = DateTime.Now.AddDays(+1).ToShortDateString();

            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            {
                //=======================//
                //UserInfo.UserLevID
                //=======================//
                //12	Section Manager(ตู้จ่าย)
                //13	Section Manager(ระบบ)
                //99	Administrator
                if (!new string[] { "12", "13", "99" }.Contains(UserInfo.UserLevID))
                {
                    var q = from t in dc.JOBs
                            from jd in dc.JOB_Details
                            where t.JOB_ID.Contains(tmpJobID) &&
                               t.JOB_ID == jd.JOB_ID &&
                             jd.EndDate.Value >= System.Convert.ToDateTime(sdt + " 00:00:00") &&
                             jd.EndDate.Value < System.Convert.ToDateTime(edt + " 00:00:00")
                            select t;

                    if (q.Count() > 0)
                    {
                        chkPrintCopy.Enabled = true;
                        chkPrintCopy.Checked = false;
                        isReprintSV = true;
                    }
                    else
                    {
                        chkPrintCopy.Enabled = false;
                        chkPrintCopy.Checked = false;
                        isReprintSV = false;

                    }

                }
                else//if (!new string[] { "12", "13", "99" }.Contains(UserInfo.UserLevID))
                {
                    chkPrintCopy.Enabled = true;
                    isReprintSV = true;
                }
            }

        }





        private void setTmpStartDate()
        {


            using (DAL.SMSManage.SMSManageDataContext db = new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn))
            {

                if ((from t in db.JOB_Details
                     where t.JOB_ID == tmpJobID.Trim() && t.StatusDetail == true
                     orderby t.RecDate descending
                     select t).Count() > 0)
                {
                    invoke_Progress("ตรวจสอบข้อมูล Services Report");
                    var q = (from t in db.JOB_Details
                             where t.JOB_ID == tmpJobID.Trim() && t.StatusDetail == true
                             orderby t.RecDate descending
                             select t).FirstOrDefault();

                    tmpStartDate = q.StartDate.ToString("yyyy-MM-dd HH:mm:ss:fff", new System.Globalization.CultureInfo("en-US"));
                }
            }
        }

        private string getContact(string jobid)
        {
            string strContact = "N";


            using (DAL.SMSManage.SMSManageDataContext db = new DAL.SMSManage.SMSManageDataContext(strConn))
            {
                strContact = "N";
                //ใช้ในระบบ CSS เท่านั้น, null หรือ 0= ไม่มีสัญญา,1= สัญญาตู้,  2=สัญญาระบบ,3= สัญญา(ระบบ+ตู้)

                var _j = db.JOBs.Where(t => t.JOB_ID == jobid)
                    .FirstOrDefault();
                if (_j != null)
                {
                    var q = (from s in db.Stations
                             where _j.STA_ID == s.STA_ID
                             select s).FirstOrDefault();
                    if (q != null)
                        if (q.xContract != null)
                        {

                            if (_j.TYP_ID == "30")//ตู้จ่าย                      
                            {
                                switch (q.xContract.Value)
                                {
                                    case '0': strContact = "N"; break;
                                    case '1': strContact = "Y"; break;
                                    case '2': strContact = "N"; break;
                                    case '3': strContact = "Y"; break;
                                    default: strContact = "N"; break;
                                }
                            }
                            else if (_j.TYP_ID == "31")//ระบบ
                            {
                                switch (q.xContract.Value)
                                {
                                    case '0': strContact = "N"; break;
                                    case '1': strContact = "N"; break;
                                    case '2': strContact = "Y"; break;
                                    case '3': strContact = "Y"; break;
                                    default: strContact = "N"; break;
                                }
                            }
                            else
                            {
                                switch (q.xContract.Value)
                                {
                                    case '0': strContact = "N"; break;
                                    case '1': strContact = "Y"; break;
                                    case '2': strContact = "Y"; break;
                                    case '3': strContact = "Y"; break;
                                    default: strContact = "N"; break;
                                }
                            }
                        }
                        else
                            strContact = "N";
                }
            }

            return strContact;
        }

        private string StartInCount(string strJobID)
        {
            string resault = "เข้างานครั้ง แรก";


            using (System.Data.SqlClient.SqlConnection ConnCnt = new System.Data.SqlClient.SqlConnection(strConn))
            {
                if (ConnCnt.State == ConnectionState.Closed)
                    ConnCnt.Open();

                DataTable tmpJobInCnt = new DataTable();
                System.Data.SqlClient.SqlDataReader drJobInCnt = null;

                string pStationID = "";
                string pStationName = "";
                string pJobID = "";
                string pJobFailure_Detail = "";
                string pSPA_ID = "";
                string pSparePart = "";
                string pVAN = "";

                string countJOBin = tmpEndDate + " " + tmpEndTime + ":999";
                string strSqlIsCus = " select count(job_id) AS JobINCount from vGetJobIn_Count where job_id ='" + tmpJobID.Trim() + "' " +
                                      " AND EndDate <= '" + countJOBin + "' ";


                using (drJobInCnt = new System.Data.SqlClient.SqlCommand(strSqlIsCus, ConnCnt).ExecuteReader())
                {
                    //drIsCus.Read();
                    if (drJobInCnt.HasRows)
                    {
                        while (drJobInCnt.Read())
                        {
                            resault = "เข้างานครั้งที่ " + drJobInCnt["JobINCount"].ToString();
                        }
                    }
                }
            }


            return resault;

        }


        private void invoke_Progress(string strMsg)
        {
            //try
            //{
            //    m_fmProgress.lblDescription.Invoke(
            //       (MethodInvoker)delegate ()
            //       {
            //           m_fmProgress.lblDescription.Text = strMsg;
            //       }
            //       );
            //}
            //catch (System.Exception)
            //{ }
        }

        //private void newProgressbar()
        //{
        //    m_fmProgress = new ProgressBar.FormProgress();
        //    backgroundWorker1.RunWorkerAsync();
        //    System.Threading.Thread.Sleep(3000);
        //}

        //private void closeProgress()
        //{
        //    if (m_fmProgress != null)
        //    {
        //        m_fmProgress.Invoke(
        //            (MethodInvoker)delegate ()
        //            {
        //                m_fmProgress.Hide();
        //                m_fmProgress.Dispose();
        //                m_fmProgress = null;
        //            }
        //        );
        //    }
        //}

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            //m_fmProgress.Activate();
            //m_fmProgress.BringToFront();
            //m_fmProgress.TopMost = true;
            //m_fmProgress.ShowDialog();
        }

        private void btPrint_Click(object sender, EventArgs e)
        {
            if (checkBox_2pdf.Checked)
                foreach (System.Diagnostics.Process proc in System.Diagnostics.Process.GetProcessesByName("Acrobat")) proc.Kill();

            if (isReprintSV)
            {
                if (checkBox_2pdf.Checked)
                    printSV();
                else
                    if (MessageBox.Show("คุณต้องการพิมพ์รายงาน SV ออกเครื่องพิมพ์ใช่หรือไม่?...", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    printSV();
            }
            else
            {
                showReport("");
            }

        }

        private void printSV()
        {
            ////if (UserInfo.ConnectMode == "0")
            ////{
            ////    if (chkPrintCopy.Checked)
            ////    {
            ////        showReport("ต้นฉบับ");
            ////        showReport("สำเนา 1");
            ////        showReport("สำเนา 2");
            ////    }
            ////    else
            ////    {

            ////        showReport("ต้นฉบับ");
            ////        showReport("สำเนา 1");
            ////    }
            ////    //อะไหล่ลูกค้า
            ////    if(this.cbPrintIsCustomer.Checked==true)
            ////    {
            ////        printPartCustomer();
            ////    }
            ////}
            ////else//Online
            ////{
            ////    if (UserInfo.UserLevID != "05")
            ////    {
            ////        if (chkPrintCopy.Checked )
            ////        {
            ////            showReport("ต้นฉบับ");
            ////            showReport("สำเนา 1");
            ////            showReport("สำเนา 2");
            ////        }
            ////        else
            ////        {

            ////            showReport("ต้นฉบับ");
            ////            showReport("สำเนา 1");
            ////        }
            ////    }
            ////    else
            ////        showReport("สำเนา 3");

            ////    //อะไหล่ลูกค้า
            ////    if (this.cbPrintIsCustomer.Checked == true)
            ////    {
            ////        printPartCustomer();
            ////    }
            ////}



            if (UserInfo.UserLevID != "05")
            {
                if (chkPrintCopy.Checked)
                {
                    showReport("ต้นฉบับ");
                    showReport("สำเนา 1");
                    showReport("สำเนา 2");
                    //20210408 พิมพ์สำเนาครบ

                }
                else
                {

                    showReport("ต้นฉบับ");
                    showReport("สำเนา 1");
                }
            }
            else
                showReport("สำเนา 3");

            //อะไหล่ลูกค้า
            if (this.cbPrintIsCustomer.Checked == true)
            {
                printPartCustomer();
            }

            //20161028
            printQuotation();

            //20190228
            printLayout();

            //////-----------------------------------------------------------------------------------------//
            //////20161105...by:Chit
            //////-----------------------------------------------------------------------------------------//
            ////try
            ////{
            ////    SMSMINI.Report.frmPrintPagePriceList pr = new SMSMINI.Report.frmPrintPagePriceList();
            ////    pr._ReportFilePath = filePartPriceListPath;
            ////    pr.PrintPPriceList(TmpJobID);
            ////}
            ////catch (Exception ex)
            ////{
            ////    MessageBox.Show(ex.Message);
            ////}

            //-----------------------------------------------------------------------------------------//

        }

        private void printQuotation()
        {

            strConnQuo = strConn;
            var rptQuo = new SMSMINI.Transaction.Report.rptQuotation();
            using (System.Data.SqlClient.SqlConnection ConnQuo = new System.Data.SqlClient.SqlConnection(strConnQuo))
            {
                if (ConnQuo.State == ConnectionState.Closed)
                    ConnQuo.Open();

                DataTable tmpTableQuo = new DataTable();
                //System.Data.SqlClient.SqlDataReader drQuo = null;

                string pJobIDQuo = "";
                string pCloseDateQuo = "";

                string strSqlQuo = "SELECT  * FROM v_rptQuotation WHERE JOB_ID ='" + tmpJobID.Trim() + "' ORDER BY PATINDEX('%20ser%',SPA_ID) ASC ";

                System.Data.SqlClient.SqlDataReader drQuo = null;

                System.Data.SqlClient.SqlDataAdapter daQuo = new System.Data.SqlClient.SqlDataAdapter(strSqlQuo, ConnQuo);
                DataSet dsQuo = new DataSet();
                daQuo.Fill(dsQuo);

                using (drQuo = new System.Data.SqlClient.SqlCommand(strSqlQuo, ConnQuo).ExecuteReader())
                {
                    drQuo.Read();
                    if (drQuo.HasRows)
                    {

                        pJobIDQuo = drQuo["JOB_ID"].ToString();
                        pCloseDateQuo = Convert.ToDateTime(drQuo["CloseDate"]).ToString("dd/MM/yyyy", new System.Globalization.CultureInfo("th-TH"));


                        rptQuo.SetDatabaseLogon(strUsr, strPwd, strServer, strDB);
                        //20210623 rptQuo.SetDatabaseLogon("sa", "enabler", "sms.flowco.co.th,1414", "ServicesMSDB"); 
                        //20210623 rptQuo.SetDatabaseLogon("sa", "enabler", "192.169.1.119\\SQL2016", "ServicesMSDB");  //20210623
                        //20200306 rptQuo.SetDatabaseLogon("sa", "enabler", "192.169.1.119", "ServicesMSDB");
                        rptQuo.SetDataSource(dsQuo.Tables[0].DefaultView);
                        rptQuo.Subreports["DetailQuo"].Database.Tables[0].SetDataSource(dsQuo.Tables[0].DefaultView);
                        rptQuo.SetParameterValue("pJobIDQuo", pJobIDQuo);
                        rptQuo.SetParameterValue("pCloseDateQuo", pCloseDateQuo);

                        crystalReportViewer1.ReportSource = rptQuo;
                        crystalReportViewer1.Refresh();

                        ExportQuo2PDF(rptQuo);



                    }

                }

            }

        }

        private void printLayout()
        {

            strConnLayout = strConn;

            using (System.Data.SqlClient.SqlConnection ConnLayout = new System.Data.SqlClient.SqlConnection(strConnLayout))
            {
                if (ConnLayout.State == ConnectionState.Closed)
                    ConnLayout.Open();

                DataTable tmpTable1 = new DataTable();
                System.Data.SqlClient.SqlDataReader drLayout = null;

                //string tmpJobID = "1901S1323";
                string strSqlLayout = "";
                if (UserInfo.ConnectMode == "0")
                {
                    strSqlLayout = " SELECT j.JOB_ID,j.STA_ID,sg.Tradename FROM JOB j " +
                                            " INNER JOIN Job_Detail_Spare jds ON j.job_id = jds.job_id " +
                                            " INNER JOIN Station s ON j.sta_id = s.sta_id  " +
                                            " INNER JOIN Station_Group sg ON s.GOP_ID = sg.GOP_ID  " +
                                            " WHERE j.job_id = '" + tmpJobID.Trim() + "' " +
                                            " AND (j.contractno is not null AND j.contractno <>'') " +
                                            //20190312 " AND jds.statusSpare ='1' AND jds.IsCancel = '0' and jds.TRA_ID = 'IN' AND jds.SPA_ID NOT IN ('20SERA01LC','20SERD02LC')";
                                            " AND jds.statusSpare ='1' AND jds.IsCancel = '0' AND jds.SPA_ID NOT IN ('20SERA01LC','20SERD02LC')";
                }
                else
                {

                    strSqlLayout = " SELECT j.JOB_ID,j.STA_ID,sg.Tradename FROM JOB j " +
                            " INNER JOIN Job_Detail_Spare jds ON j.job_id = jds.job_id " +
                            " INNER JOIN Station s ON j.sta_id = s.sta_id  " +
                            " INNER JOIN Station_Group sg ON s.GOP_ID = sg.GOP_ID  " +
                            " WHERE j.job_id = '" + tmpJobID.Trim() + "' " +
                            " AND (j.contractno is not null AND j.contractno <>'') " +
                            " AND (convert (varchar,EndDate,112) >='" + tmpEndDate + "' and convert (varchar,EndDate,8) >='" + tmpEndTime + "') and (convert (varchar,StartDate,112) <='" + tmpEndDate + "' )" +
                            //20190312 " AND jds.statusSpare ='1' AND jds.IsCancel = '0' and jds.TRA_ID = 'IN' AND jds.SPA_ID NOT IN ('20SERA01LC','20SERD02LC')";
                            " AND jds.statusSpare ='1' AND jds.IsCancel = '0' AND jds.SPA_ID NOT IN ('20SERA01LC','20SERD02LC')";

                }
                //ไม่ต้องเช็ควันที่เพราะโดนกรองมาจากตอนเปิดงานแล้ว
                //แล้วถ้าวันเปิดงานหรือปิดงานยังไม่หมดสัญญา แต่วันพิมพ์ย้อนหลัง(online) มันหมดสัญญาไปแล้วล่ะ ?
                //จะให้ระบบดึงรายงาน layout มาแสดงหรือไม่

                using (drLayout = new System.Data.SqlClient.SqlCommand(strSqlLayout, ConnLayout).ExecuteReader())
                {
                    drLayout.Read();
                    if (drLayout.HasRows)
                    {
                        string a = drLayout["STA_ID"].ToString();
                        //var pdf = "http://sms.flowco.co.th/StationLayout/PTT";
                        var pdf = "http://sms.flowco.co.th/StationLayout/" + drLayout["Tradename"].ToString().Trim();
                        var sta_id = drLayout["STA_ID"].ToString();//"10001501"; 
                        var path = pdf + "/" + sta_id + ".pdf";

                        //System.Diagnostics.Process.Start(path);

                        if (isValidURL(path) == true)
                        {
                            System.Diagnostics.Process.Start(path);
                        }
                        //else {
                        //    MessageBox.Show(" ยังไม่มีข้อมูล Layout ในสถานีนี้ ", "เกิดข้อผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Error);

                        //}

                    }
                }
            }
        }

        public static bool isValidURL(string url)
        {
            WebRequest webRequest = WebRequest.Create(url);
            WebResponse webResponse;
            try
            {
                webResponse = webRequest.GetResponse();
            }
            catch //If exception thrown then couldn't get response from address
            {
                return false;
            }
            return true;
        }


        private void ExportQuo2PDF(Report.rptQuotation rptQuo)
        {
            string f = "";
            SaveFileDialog DialogSave = new SaveFileDialog();
            DialogSave.DefaultExt = "xls";
            DialogSave.Filter = "Adobe Acrobat  (*.pdf)|*.pdf|All files (*.*)|*.*";
            DialogSave.AddExtension = true;
            DialogSave.RestoreDirectory = true;
            DialogSave.Title = "Where do you want to save the file?";
            DialogSave.InitialDirectory = @"C:/";


            IsCus = "QuotationReport_" + tmpJobID + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".pdf";

            DialogSave.FileName = IsCus;

            if (DialogSave.ShowDialog() == DialogResult.OK)
            {
                f = DialogSave.FileName.ToString();
                DialogSave.Dispose();
                DialogSave = null;

                try
                {
                    ExportOptions CrExportOptions;
                    DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                    PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();
                    CrDiskFileDestinationOptions.DiskFileName = f;// "c:\\ServicesReport_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".pdf";


                    CrExportOptions = rptQuo.ExportOptions;
                    {
                        CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                        CrExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                        CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
                        CrExportOptions.FormatOptions = CrFormatTypeOptions;

                    }
                    rptQuo.Export();
                    rptQuo.Close();

                    if (System.IO.File.Exists(f))
                    {
                        System.Diagnostics.Process.Start(f);
                    }

                    //this.Close();
                }

                catch (Exception ex)
                {
                    MessageBox.Show("ข้อผิดพลาด " + ex.ToString(), "เกิดข้อผิดพลาดในการ Export ไฟล์ Quotation PDF");
                }
            }// if (DialogSave.ShowDialog() == DialogResult.OK)
        }

        private void printPartCustomer()
        {
            strConnCus = strConn;// SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;

            var rptCus = new SMSMINI.Transaction.Report.rptBHR010();
            using (System.Data.SqlClient.SqlConnection ConnCus = new System.Data.SqlClient.SqlConnection(strConnCus))
            {
                if (ConnCus.State == ConnectionState.Closed)
                    ConnCus.Open();

                DataTable tmpTable1 = new DataTable();
                System.Data.SqlClient.SqlDataReader drIsCus = null;

                string pStationID = "";
                string pStationName = "";
                string pJobID = "";
                string pJobFailure_Detail = "";
                string pSPA_ID = "";
                string pSparePart = "";
                string pVAN = "";


                string strSqlIsCus = "SELECT     j.JOB_ID, j.EMP_ID3, j.JobFailure_Detail, st.StationSys" +
                                     ",ISNULL( STA_ID1,'') as STA_ID1 , sp.SparePart,sp.SPA_ID,jfb.Failure_Detail" +
                                     " FROM         JOB AS j " +
                                     " INNER JOIN JOB_Detail_Spare AS jds ON j.JOB_ID = jds.JOB_ID " +
                                     " INNER JOIN Station AS st ON j.STA_ID = st.STA_ID " +
                                     " INNER JOIN SpareParts AS sp ON jds.SPA_ID = sp.SPA_ID  " +
                                     " INNER JOIN JOB_Failure_Detail AS jfb ON jds.JOB_ID = jfb.JOB_ID " +
                                     " WHERE j.JOB_ID ='" + tmpJobID.Trim() + "'  AND jds.IsCustomer = '9'";

                using (drIsCus = new System.Data.SqlClient.SqlCommand(strSqlIsCus, ConnCus).ExecuteReader())
                {
                    //drIsCus.Read();
                    if (drIsCus.HasRows)
                    {
                        while (drIsCus.Read())
                        {

                            pStationID = drIsCus["STA_ID1"].ToString();
                            pStationName = drIsCus["StationSys"].ToString();
                            pJobID = drIsCus["JOB_ID"].ToString();
                            pJobFailure_Detail = drIsCus["Failure_Detail"].ToString();
                            pSPA_ID = drIsCus["SPA_ID"].ToString();
                            pSparePart = drIsCus["SparePart"].ToString();
                            pVAN = drIsCus["EMP_ID3"].ToString();


                            //if (dsIsCus.Tables[0].Rows.Count > 0)
                            //{
                            rptCus.SetDatabaseLogon(strUsr, strPwd, strServer, strDB);



                            rptCus.SetParameterValue("pStationID", pStationID);
                            rptCus.SetParameterValue("pStationName", pStationName);
                            rptCus.SetParameterValue("pJobID", pJobID);
                            rptCus.SetParameterValue("pJobFailure_Detail", pJobFailure_Detail);
                            rptCus.SetParameterValue("pSPA_ID", pSPA_ID);
                            rptCus.SetParameterValue("pSparePart", pSparePart);
                            rptCus.SetParameterValue("pVAN", pVAN);


                            //crystalReportViewer1.ReportSource = rptCus;
                            //crystalReportViewer1.Refresh();

                            ExportIsCus2PDF(rptCus);
                            ////}
                        }
                    }
                    //else
                    //{
                    //    crystalReportViewer1.ReportSource = null;
                    //}
                }
            }
        }

        private void ExportIsCus2PDF(Report.rptBHR010 rptCus)
        //private void ExportIsCus2PDF(SMSMINI.Report.rptBHR010 rptCus)

        {
            string f = "";
            SaveFileDialog DialogSave = new SaveFileDialog();
            DialogSave.DefaultExt = "xls";
            DialogSave.Filter = "Adobe Acrobat  (*.pdf)|*.pdf|All files (*.*)|*.*";
            DialogSave.AddExtension = true;
            DialogSave.RestoreDirectory = true;
            DialogSave.Title = "Where do you want to save the file?";
            DialogSave.InitialDirectory = @"C:/";


            IsCus = "BHRReport_" + tmpJobID + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".pdf";

            DialogSave.FileName = IsCus;

            if (DialogSave.ShowDialog() == DialogResult.OK)
            {
                f = DialogSave.FileName.ToString();
                DialogSave.Dispose();
                DialogSave = null;

                try
                {
                    ExportOptions CrExportOptions;
                    DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                    PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();
                    CrDiskFileDestinationOptions.DiskFileName = f;// "c:\\ServicesReport_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".pdf";


                    CrExportOptions = rptCus.ExportOptions;
                    {
                        CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                        CrExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                        CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
                        CrExportOptions.FormatOptions = CrFormatTypeOptions;

                    }
                    rptCus.Export();
                    rptCus.Close();

                    if (System.IO.File.Exists(f))
                    {
                        System.Diagnostics.Process.Start(f);
                    }

                    //this.Close();
                }

                catch (Exception ex)
                {
                    MessageBox.Show("ข้อผิดพลาด " + ex.ToString(), "เกิดข้อผิดพลาดในการ Export ไฟล์ SV PDF");
                }
            }// if (DialogSave.ShowDialog() == DialogResult.OK)

        }






        private void showReport(string mode)
        {
            string pTitle = "";

            //20210511   var rpt = new SMSMINI.Transaction.Report.rptServicesReportNEW();
            var rpt = new SMSMINI.Transaction.Report.rptServicesReportNEWFORM();

            using (System.Data.SqlClient.SqlConnection Conn = new System.Data.SqlClient.SqlConnection(strConn))
            {
                invoke_Progress("ดึงข้อมูล Services Report");
                if (Conn.State == ConnectionState.Closed)
                    Conn.Open();

                DataTable tmpTable = new DataTable();

                //string strSql = " JOB_ID,Work_ID,UserWorkOrder, Opendate, FirstInDate, StartDate, CloseDate, EMP_ID3, FName, Failure_th, Priority_GuestVip, job_PointFailure, Type, JobStatus, Type_1," +
                //          "Informer, InPhone, JobOutside, TypeCloseJob, Problem, fail_NO, fail_POI_ID, PointFailure2, Failure_Detail, SerialNumber, EndDate, Resole_Detail, " +
                //          "STA_ID1, StationSys, FAI_ID, PRI_ID, Job_POI_ID, TYP_ID, " +
                //          //20150429 "JOBS_ID, TYP_ID1, TCO_ID, PMT_ID, Contract, Address, TelNo, FaxNo, JobFailure_Detail, StartLiter,  EndLiter,  LiterTest,  Location,  Model, PFAI_ID";
                //          "JOBS_ID, TYP_ID1, TCO_ID, PMT_ID, Contract, Address, TelNo, FaxNo, JobFailure_Detail, StartLiter,  EndLiter,  LiterTest,  Location,  Model, PFAI_ID,Grade,Locate,FDetail,Cause,Solve,InstallDate,WarrantyDate";

                string strSqlNEW = " JOB_ID,Work_ID,UserWorkOrder, Opendate, FirstInDate, StartDate, CloseDate, EMP_ID3, FName, Failure_th, Priority_GuestVip, job_PointFailure, Type, JobStatus, Type_1," +
                                         "Informer, InPhone, JobOutside, TypeCloseJob, Problem, fail_NO, fail_POI_ID, PointFailure2, Failure_Detail, SerialNumber, EndDate, Resole_Detail, " +
                                         "STA_ID1, StationSys, FAI_ID, PRI_ID, Job_POI_ID, TYP_ID, " +
                                         //20141024 "JOBS_ID, TYP_ID1, TCO_ID, PMT_ID, Contract, Address, TelNo, FaxNo, JobFailure_Detail, StartLiter,  EndLiter,  LiterTest,  Location,  Model, PFAI_ID";
                                         //20141215  "JOBS_ID, TYP_ID1, TCO_ID, PMT_ID, Contract, Address, TelNo, FaxNo, JobFailure_Detail, StartLiter,  EndLiter,  LiterTest,  Location,  Model, PFAI_ID,Grade,Locate,PointFFA,FDetail,Cause,Solve";
                                         //20171207 SETS "JOBS_ID, TYP_ID1, TCO_ID, PMT_ID, Contract, Address, TelNo, FaxNo, JobFailure_Detail, StartLiter,  EndLiter,  LiterTest,  Model, PFAI_ID,Grade,Locate,FDetail,Cause,Solve,InstallDate,WarrantyDate";
                                         "JOBS_ID, TYP_ID1, TCO_ID, PMT_ID, Contract, Address, TelNo, FaxNo, JobFailure_Detail, StartLiter,  EndLiter,  LiterTest,  Model, PFAI_ID,Grade,Locate,FDetail,Cause,Solve,InstallDate,WarrantyDate,DEP_ID,Company";



                string sql = "";
                double pPartSumTotal = 0;

                string sqlGetTitle = "select Count(*) cIsPrices from JOB_Detail_Spare where JOB_ID = '" + tmpJobID.Trim() + "' and  IsPrices = '1'";
                pTitle = getTitleSV(Conn, sqlGetTitle);

                if (tmpStartDate != "")
                {


                    sql = " SELECT distinct " + strSqlNEW + " FROM v_rpt_ServicesReport_web where  JOB_ID = '" + tmpJobID.Trim() + "' and  (convert (varchar,EndDate,112) ='" + tmpEndDate + "' and convert (varchar,EndDate,8) ='" + tmpEndTime + "') and StatusDetail = 1 order by Locate,PointFailure2; " +// add statusDetal order by Locate,PointFailure2; " + //20150407 order by PointFailure2, Location; " +
                                                                                                                                                                                                                                                                                                              //20210507" select * from v_rpt_ServicesReport_web_PartDetail_Reprint where JOB_ID = '" + tmpJobID.Trim() + "' and  SPA_ID not in (SELECT distinct  ERP_SPA_ID FROM  [SpareParts] where ERP_SPA_Split = 1 ) and (convert (varchar,EndDate,112) >='" + tmpEndDate + "' and convert (varchar,EndDate,8)='" + tmpEndTime + "')  and convert (varchar,StartDate,112) <='" + tmpEndDate + "' order by Location,SPA_ID; " + //20150407 order by SPA_ID; " +
                           " select  JOB_ID, SPA_ID, sum(Quantity) as Quantity, StatusSpare, TRA_ID, SparePart, sum(SumTotalPrices) as SumTotalPrices, sum(Prices) as Prices,sum(SumVAT) as SumVAT,sum(SumPrices) as SumPrices, StartDate, Enddate, ReferPage, WarrantDate, CloseDate,'' as location " +//,'' as WA " +
                          " ,dbo.getLocateWarranty(v.JOB_ID,v.SPA_ID,v.StatusSpare,v.TRA_ID,v.StartDate,v.Enddate) as WA from v_rpt_ServicesReport_web_PartDetail_Reprint v where  JOB_ID = '" + tmpJobID.Trim() + "' and  SPA_ID not in (SELECT distinct  ERP_SPA_ID FROM  [SpareParts] where ERP_SPA_Split = 1 ) and (convert (varchar,EndDate,112) >='" + tmpEndDate + "' and convert (varchar,EndDate,8)='" + tmpEndTime + "')  and convert (varchar,StartDate,112) <='" + tmpEndDate + "'  group by JOB_ID, SPA_ID, StatusSpare, TRA_ID, SparePart, StartDate, Enddate, ReferPage, WarrantDate, CloseDate ORDER BY SPA_ID;" +
                          " select sum(SumTotalPrices) as 'SumxTotal',JOB_ID from v_rpt_ServicesReport_web_PartDetail_AdminReprint where JOB_ID = '" + tmpJobID.Trim() + "' and  SPA_ID not in (SELECT distinct  ERP_SPA_ID FROM  [SpareParts] where ERP_SPA_Split = 1 ) and (convert (varchar,EndDate,112) >='" + tmpEndDate + "' and convert (varchar,EndDate,8)='" + tmpEndTime + "')  and convert (varchar,StartDate,112) <='" + tmpEndDate + "'  group by JOB_ID";

                }
                else
                {


                    sql = " SELECT distinct " + strSqlNEW + " FROM v_rpt_ServicesReport_web where  JOB_ID = '" + tmpJobID.Trim() + "' and  (convert (varchar,EndDate,112) ='" + tmpEndDate + "' and convert (varchar,EndDate,8) ='" + tmpEndTime + "') and StatusDetail = 1 order by Locate,PointFailure2; " +// add statusDetal order by Locate,PointFailure2; " + //20150407 order by PointFailure2, Location; " +
                                                                                                                                                                                                                                                                                                              //20210507 " select * from v_rpt_ServicesReport_web_PartDetail_Reprint where JOB_ID = '" + tmpJobID.Trim() + "' and  SPA_ID not in (SELECT distinct  ERP_SPA_ID FROM  [SpareParts] where ERP_SPA_Split = 1 ) and (convert (varchar,EndDate,112) >='" + tmpEndDate + "' and convert (varchar,EndDate,8)='" + tmpEndTime + "')  and convert (varchar,StartDate,112) <='" + tmpEndDate + "' order by Location,SPA_ID; " + //20150407 order by SPA_ID; " +
                          " select  JOB_ID, SPA_ID, sum(Quantity) as Quantity, StatusSpare, TRA_ID, SparePart, sum(SumTotalPrices) as SumTotalPrices, sum(Prices) as Prices,sum(SumVAT) as SumVAT,sum(SumPrices) as SumPrices, StartDate, Enddate, ReferPage, WarrantDate, CloseDate,'' as location,'' as WA " +
                          " ,dbo.getLocateWarranty(v.JOB_ID,v.SPA_ID,v.StatusSpare,v.TRA_ID,v.StartDate,v.Enddate) as WA from v_rpt_ServicesReport_web_PartDetail_Reprint v where  JOB_ID = '" + tmpJobID.Trim() + "' and  SPA_ID not in (SELECT distinct  ERP_SPA_ID FROM  [SpareParts] where ERP_SPA_Split = 1 ) and (convert (varchar,EndDate,112) >='" + tmpEndDate + "' and convert (varchar,EndDate,8)='" + tmpEndTime + "')  and convert (varchar,StartDate,112) <='" + tmpEndDate + "'  group by JOB_ID, SPA_ID, StatusSpare, TRA_ID, SparePart, StartDate, Enddate, ReferPage, WarrantDate, CloseDate  ORDER BY SPA_ID ;" +
                         " select sum(SumTotalPrices) as 'SumxTotal',JOB_ID from v_rpt_ServicesReport_web_PartDetail_AdminReprint where JOB_ID = '" + tmpJobID.Trim() + "' and  SPA_ID not in (SELECT distinct  ERP_SPA_ID FROM  [SpareParts] where ERP_SPA_Split = 1 ) and (convert (varchar,EndDate,112) >='" + tmpEndDate + "' and convert (varchar,EndDate,8)='" + tmpEndTime + "')  and convert (varchar,StartDate,112) <='" + tmpEndDate + "'  group by JOB_ID";

                }

                System.Data.SqlClient.SqlDataReader dr = null;

                System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(sql, Conn);
                DataSet ds = new DataSet();
                da.Fill(ds);


                string pPlobel_Detail = "";
                string pResolve_Detail = "";
                string pStartDate = "";
                string pEndDate = "";
                string vanConten = "";
                string pPrintCopy = mode;

                string pJOBS_ID = "";


                sql = " select top 1 StartDate, EndDate, Problem_Detail, Resole_Detail, JOBS_ID from v_rpt_ServicesReport_web_PloblemResolve where JOB_ID ='" + tmpJobID.Trim() + "' and EndDate is not null and  (convert (varchar,EndDate,112) ='" + tmpEndDate + "' and convert (varchar,EndDate,8) ='" + tmpEndTime + "')ORDER BY RecDate DESC ";


                using (dr = new System.Data.SqlClient.SqlCommand(sql, Conn).ExecuteReader())
                {
                    dr.Read();
                    if (dr.HasRows)
                    {
                        pPlobel_Detail = dr["Problem_Detail"].ToString();
                        pResolve_Detail = dr["Resole_Detail"].ToString();
                        pStartDate = Convert.ToDateTime(dr["StartDate"]).ToString("dd/MM/yy HH:mm", new System.Globalization.CultureInfo("th-TH"));
                        pEndDate = Convert.ToDateTime(dr["EndDate"]).ToString("dd/MM/yy HH:mm", new System.Globalization.CultureInfo("th-TH"));

                        pJOBS_ID = dr["JOBS_ID"].ToString();


                    }
                }

                sql = " SELECT case " +
                      "     when rtrim(ltrim(JobContentMent))='A' then '(A) ดี' " +
                      "     when rtrim(ltrim(JobContentMent))='B' then '(B) พอใช้' " +
                      "     when rtrim(ltrim(JobContentMent))='C' then '(C) ปรับปรุง: {' +  isnull(Remark,'')+'}' " +
                      "     when rtrim(ltrim(JobContentMent))='D' then '(D) ไม่ระบุ'  " +
                      "     else '' end as 'JobContentMent' " +
                      " FROM JOB_ContentMent  where JOB_ID ='" + tmpJobID.Trim() + "' ";

                using (dr = new System.Data.SqlClient.SqlCommand(sql, Conn).ExecuteReader())
                {
                    dr.Read();
                    if (dr.HasRows)
                        vanConten = "ความคิดเห็นของท่านต่อการบริการครั้งนี้ (ช่างเทคนิค) [" + dr["JobContentMent"].ToString() + "]";
                    else
                        vanConten = "ความคิดเห็นของท่านต่อการบริการครั้งนี้ (ช่างเทคนิค)[]";
                }


                string jobInCount = "";



                jobInCount = StartInCount(tmpJobID.Trim());

                if (ds.Tables[2].Rows.Count > 0)
                {
                    pPartSumTotal = Convert.ToDouble(ds.Tables[2].Rows[0]["SumxTotal"].ToString());
                }


                //TranType Desp
                string pTranTypeDesp = "";
                sql = " Declare @DynamicSQL	Varchar(Max), @Columns	Varchar(Max)  " +
                      " Select @Columns = Coalesce(@Columns +',','') + TRA_ID+'='+ TransferName From SparePart_TranType where TRA_ID<>'00'  " +
                      " Select @Columns as [TranType]";


                using (dr = new System.Data.SqlClient.SqlCommand(sql, Conn).ExecuteReader())
                {
                    dr.Read();
                    if (dr.HasRows)
                        pTranTypeDesp = dr["TranType"].ToString();
                    else
                        pTranTypeDesp = "";
                }



                rpt.SetDatabaseLogon(strUsr, strPwd, strServer, strDB);
                //20210623 rpt.SetDatabaseLogon("sa", "enabler", "192.169.1.119\\SQL2016", "ServicesMSDB");


                rpt.SetDataSource(ds.Tables[0].DefaultView);

                rpt.Subreports["failuredetail"].Database.Tables[0].SetDataSource(ds.Tables[0].DefaultView);
                rpt.Subreports["partdetail"].Database.Tables[0].SetDataSource(ds.Tables[1].DefaultView);

                rpt.SetParameterValue("JonStartIn", jobInCount);
                rpt.SetParameterValue("pPlobel_Detail", pPlobel_Detail);
                rpt.SetParameterValue("pResolve_Detail", pResolve_Detail);
                rpt.SetParameterValue("pStartDate", pStartDate);
                rpt.SetParameterValue("pEndDate", pEndDate);

                rpt.SetParameterValue("pPrintCopy", pPrintCopy);

                rpt.SetParameterValue("pVanContment", vanConten);
                rpt.SetParameterValue("pPartSumTotal", pPartSumTotal);


                rpt.SetParameterValue("pSMSMiniVersion", "SMS MINI2 v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString());
                rpt.SetParameterValue("pIscontact", "สถานีในสัญญา: " + getContact(tmpJobID.Trim()));

                rpt.SetParameterValue("pJOBS_ID", pJOBS_ID);
                rpt.SetParameterValue("TranTypeDesp", pTranTypeDesp);

                //pTitle
                rpt.SetParameterValue("pTitle", pTitle);


                //20191007==compCombo
                string v = cbxComp.SelectedValue.ToString();
                rpt.SetParameterValue("pComp", v);
                //==========


                if (mode == "")
                {
                    crystalReportViewer1.ReportSource = rpt;
                    crystalReportViewer1.Refresh();
                }
                else
                {


                    if (checkBox_2pdf.Checked)
                    {
                        Export2PDF(mode, rpt);
                    }
                    else
                    {
                        try
                        {
                            rpt.PrintToPrinter(1, false, 0, 0);
                            //====20200330==
                            //string strConnP = "";
                            //strConnP = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;

                            //20201110try
                            //20201110{
                            string sqlIsPrintSV = "";
                            //20201208 sqlIsPrintSV = " UPDATE JOB_Detail SET IsPrintSV ='1' WHERE JOB_ID IN ('" + tmpJobID.Trim() + "') AND (convert (varchar,EndDate,112) ='" + tmpEndDate + "' and convert (varchar,EndDate,8) ='" + tmpEndTime + "')";
                            sqlIsPrintSV = " UPDATE JOB_Detail SET IsPrintSV ='1' WHERE JOB_ID IN ('" + tmpJobID.Trim() + "') AND (convert (varchar,EndDate,112) ='" + tmpEndDate + "' and convert (varchar,EndDate,8) ='" + tmpEndTime + "') and StatusDetail = 1 ";
                            JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sqlIsPrintSV);
                        }
                        catch (Exception)
                        {
                            //20201110
                            string sqlIsPrintSV = "";
                            //20201208 sqlIsPrintSV = " UPDATE JOB_Detail SET IsPrintSV ='0' WHERE JOB_ID IN ('" + tmpJobID.Trim() + "') AND (convert (varchar,EndDate,112) ='" + tmpEndDate + "' and convert (varchar,EndDate,8) ='" + tmpEndTime + "')";
                            sqlIsPrintSV = " UPDATE JOB_Detail SET IsPrintSV ='0' WHERE JOB_ID IN ('" + tmpJobID.Trim() + "') AND (convert (varchar,EndDate,112) ='" + tmpEndDate + "' and convert (varchar,EndDate,8) ='" + tmpEndTime + "') and StatusDetail = 1 ";
                            JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sqlIsPrintSV);

                        }
                        //==============

                    }


                }


            }
        }

        private string getTitleSV(System.Data.SqlClient.SqlConnection Conn, string sqlGetTitle)
        {
            var title = "";

            ////bool isPrice = false;

            ////using (var dr = new System.Data.SqlClient.SqlCommand(sqlGetTitle, Conn).ExecuteReader())
            ////{
            ////    dr.Read();
            ////    if (dr.HasRows)
            ////    {
            ////        //cIsPrices
            ////        if (int.Parse(dr["cIsPrices"].ToString()) > 0)
            ////            isPrice = true;
            ////    }
            ////}

            ////if (isPrice)
            ////    return  "ใบรายงานการซ่อม" + Environment.NewLine +
            ////            "SERVICE REPORT /" + Environment.NewLine +
            ////            "ใบเสร็จรับเงินชั่วคราว";
            ////else
            ////return "ใบรายงานการซ่อม" + Environment.NewLine +
            ////        "SERVICE REPORT";

            title = "ใบรายงานการซ่อม" + Environment.NewLine +
                    "SERVICE REPORT";

            return title;
        }


        //20210511 private void Export2PDF(string mode, Report.rptServicesReportNEW rpt)
        private void Export2PDF(string mode, Report.rptServicesReportNEWFORM rpt)
        {
            string f = "";
            SaveFileDialog DialogSave = new SaveFileDialog();
            DialogSave.DefaultExt = "pdf";
            DialogSave.Filter = "Adobe Acrobat  (*.pdf)|*.pdf|All files (*.*)|*.*";
            DialogSave.AddExtension = true;
            DialogSave.RestoreDirectory = true;
            DialogSave.Title = "Where do you want to save the file?";
            DialogSave.InitialDirectory = @"C:/";


            sv = "ServicesReport_" + tmpJobID + "_(" + mode + ")_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".pdf";

            DialogSave.FileName = sv;

            if (DialogSave.ShowDialog() == DialogResult.OK)
            {
                f = DialogSave.FileName.ToString();

                filePartPriceListPath = Path.GetDirectoryName(f);

                DialogSave.Dispose();
                DialogSave = null;

                try
                {
                    ExportOptions CrExportOptions;
                    DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                    PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();
                    CrDiskFileDestinationOptions.DiskFileName = f;// "c:\\ServicesReport_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".pdf";


                    CrExportOptions = rpt.ExportOptions;
                    {
                        CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                        CrExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                        CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
                        CrExportOptions.FormatOptions = CrFormatTypeOptions;

                    }
                    rpt.Export();
                    rpt.Close();

                    if (System.IO.File.Exists(f))
                    {
                        System.Diagnostics.Process.Start(f);
                    }

                    //this.Close();

                    //====20200330==
                    //string strConnP = "";
                    //strConnP = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;

                    try
                    {
                        string sqlIsPrintSV = "";
                        //20201208sqlIsPrintSV = " UPDATE JOB_Detail SET IsPrintSV ='1' WHERE JOB_ID IN ('" + tmpJobID.Trim() + "') AND (convert (varchar,EndDate,112) ='" + tmpEndDate + "' and convert (varchar,EndDate,8) ='" + tmpEndTime + "')";
                        sqlIsPrintSV = " UPDATE JOB_Detail SET IsPrintSV ='1' WHERE JOB_ID IN ('" + tmpJobID.Trim() + "') AND (convert (varchar,EndDate,112) ='" + tmpEndDate + "' and convert (varchar,EndDate,8) ='" + tmpEndTime + "') and StatusDetail = 1 ";
                        JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sqlIsPrintSV);
                    }
                    catch (Exception)
                    {
                        string sqlIsPrintSV = "";
                        //20201208sqlIsPrintSV = " UPDATE JOB_Detail SET IsPrintSV ='0' WHERE JOB_ID IN ('" + tmpJobID.Trim() + "') AND (convert (varchar,EndDate,112) ='" + tmpEndDate + "' and convert (varchar,EndDate,8) ='" + tmpEndTime + "')";
                        sqlIsPrintSV = " UPDATE JOB_Detail SET IsPrintSV ='0' WHERE JOB_ID IN ('" + tmpJobID.Trim() + "') AND (convert (varchar,EndDate,112) ='" + tmpEndDate + "' and convert (varchar,EndDate,8) ='" + tmpEndTime + "') and StatusDetail = 1 ";
                        JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sqlIsPrintSV);
                    }
                    //==============
                }

                catch (Exception ex)
                {
                    string sqlIsPrintSV = "";
                    //20201208sqlIsPrintSV = " UPDATE JOB_Detail SET IsPrintSV ='0' WHERE JOB_ID IN ('" + tmpJobID.Trim() + "') AND (convert (varchar,EndDate,112) ='" + tmpEndDate + "' and convert (varchar,EndDate,8) ='" + tmpEndTime + "') ";
                    sqlIsPrintSV = " UPDATE JOB_Detail SET IsPrintSV ='0' WHERE JOB_ID IN ('" + tmpJobID.Trim() + "') AND (convert (varchar,EndDate,112) ='" + tmpEndDate + "' and convert (varchar,EndDate,8) ='" + tmpEndTime + "') and StatusDetail = 1 ";
                    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sqlIsPrintSV);

                    MessageBox.Show("ข้อผิดพลาด " + ex.ToString(), "เกิดข้อผิดพลาดในการ Export ไฟล์ SV PDF");
                }



            }// if (DialogSave.ShowDialog() == DialogResult.OK)

        }

        private void crystalReportViewer1_Load(object sender, EventArgs e)
        {

        }
        private void setComp()
        {
            try
            {
                /**/
                string sql = "";
                //sql = " SELECT DISTINCT ISNULL(COMPANY,'FLO') as Comp FROM Employees;SELECT Company  from job j inner join Employees e on j.EMP_ID3 = e.EMP_ID where j.job_id ='" + tmpJobID.Trim() + "' "; 
                sql = " SELECT DISTINCT ISNULL(COMPANY,'FLO') as Comp FROM Employees;";

                SqlDataReader dr = JaSqlHelper.ExecuteReader(strConn, CommandType.Text, sql);
                DataTable dt;

                if (dr.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(dr);
                    cbxComp.BeginUpdate();

                    cbxComp.ValueMember = "Comp";
                    cbxComp.DisplayMember = "Comp";
                    cbxComp.DataSource = dt;
                    cbxComp.EndUpdate();



                }
                dr.Close();

                string sqls = " SELECT Company  from job j inner join Employees e on j.EMP_ID3 = e.EMP_ID where j.job_id ='" + tmpJobID.Trim() + "' ";

                SqlDataReader drs = JaSqlHelper.ExecuteReader(strConn, CommandType.Text, sqls);
                DataTable dts;
                drs.Read();
                if (drs.HasRows)
                {

                    //string c = drs["Company"].ToString();
                    cbxComp.Text = drs["Company"].ToString();

                }
                drs.Close();

            }
            catch (System.Exception ex)
            {
                MessageBox.Show("เกิดข้อผิดพลาด " + ex.Message, "เกิดข้อผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cbxComp_SelectionChangeCommitted(object sender, EventArgs e) //20200714
        {
            showReport("");
        }
        /*  //20200714
        //private void cbxComp_SelectedIndexChanged(object sender, EventArgs e)
        //{
            
       //string selectComp  = cbxComp.SelectedValue.ToString(); 
             
        //    //ServiceReport_Load(null,null);
        //    showReport("");
        //    //cbxComp.SelectedValue= selectComp.ToString();

        //}
         */


    }

}
