﻿namespace SMSMINI.Transaction
{
    partial class JobClose
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(JobClose));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.toolStrip_Mnu = new System.Windows.Forms.ToolStrip();
            this.toolStripComboBox1 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel_cobby = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripText_txtFind = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_FindStation = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_Save = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_Cancel = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_Print = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_TAGPrimt = new System.Windows.Forms.ToolStripButton();
            this.btFTRPrint = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_Exit = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel_Dateime = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel_QUARTER = new System.Windows.Forms.ToolStripLabel();
            this.radioButton_D = new System.Windows.Forms.RadioButton();
            this.radioButton_C = new System.Windows.Forms.RadioButton();
            this.radioButton_B = new System.Windows.Forms.RadioButton();
            this.radioButton_A = new System.Windows.Forms.RadioButton();
            this.cobProblemType = new System.Windows.Forms.ComboBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label31 = new System.Windows.Forms.Label();
            this.txtResolve_Detail = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cbxChFCAT = new System.Windows.Forms.ComboBox();
            this.label49 = new System.Windows.Forms.Label();
            this.cob_SolvingByWI = new System.Windows.Forms.ComboBox();
            this.cbxLocation = new System.Windows.Forms.ComboBox();
            this.pUpSNNew = new System.Windows.Forms.Button();
            this.pUpSNOld = new System.Windows.Forms.Button();
            this.btSelectPointFail = new System.Windows.Forms.Button();
            this.label51 = new System.Windows.Forms.Label();
            this.cob_Failure_Action_Solving = new System.Windows.Forms.ComboBox();
            this.label50 = new System.Windows.Forms.Label();
            this.cobOilType = new System.Windows.Forms.ComboBox();
            this.label47 = new System.Windows.Forms.Label();
            this.cob_Failure_Action = new System.Windows.Forms.ComboBox();
            this.label46 = new System.Windows.Forms.Label();
            this.btLoadSNNew = new System.Windows.Forms.Button();
            this.txtSNOld = new System.Windows.Forms.TextBox();
            this.lblWarranty = new System.Windows.Forms.Label();
            this.txtFixAsetNoNew = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.txtFixAssetNoOld = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.txtSNNew = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.btClearData = new System.Windows.Forms.Button();
            this.lblFailureCode = new System.Windows.Forms.Label();
            this.dataGridView_FailPoint = new System.Windows.Forms.DataGridView();
            this.clCancel = new System.Windows.Forms.DataGridViewButtonColumn();
            this.cAddPart = new System.Windows.Forms.DataGridViewButtonColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txtLiterTest = new System.Windows.Forms.TextBox();
            this.txtEndtLite = new System.Windows.Forms.TextBox();
            this.txtStartLite = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtFailure = new System.Windows.Forms.TextBox();
            this.txtModel = new System.Windows.Forms.TextBox();
            this.comboBox_PoitFail = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label_1 = new System.Windows.Forms.Label();
            this.btUpdateWorkType = new System.Windows.Forms.Button();
            this.lblLastEndDate = new System.Windows.Forms.Label();
            this.txtWorkID = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txtJobSO = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.txtWorkType = new System.Windows.Forms.TextBox();
            this.txtProjectNO = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtStartDateIn = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtStation = new System.Windows.Forms.TextBox();
            this.txtUserOpenJob = new System.Windows.Forms.TextBox();
            this.txtInformer = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.FailureDetail = new System.Windows.Forms.TextBox();
            this.txtFialureCode = new System.Windows.Forms.TextBox();
            this.txtPointFaile = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lbl_JobType = new System.Windows.Forms.Label();
            this.comboBox_Priority = new System.Windows.Forms.ComboBox();
            this.txtOpenJOB = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtJobID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel_selectEndDate = new System.Windows.Forms.Panel();
            this.label43 = new System.Windows.Forms.Label();
            this.cobEndDate = new System.Windows.Forms.ComboBox();
            this.btSelectDateOK = new System.Windows.Forms.Button();
            this.label36 = new System.Windows.Forms.Label();
            this.lblJobInCount = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.RadioButtonList_JobStatus2 = new System.Windows.Forms.RadioButton();
            this.RadioButtonList_JobStatus1 = new System.Windows.Forms.RadioButton();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtReferPage = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.txtPartSerialNumberOld = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.lblERP_orderline_id = new System.Windows.Forms.Label();
            this.cbIsCustomer = new System.Windows.Forms.CheckBox();
            this.txtFixAssetNo = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.con_ERP_StationCharge = new System.Windows.Forms.ComboBox();
            this.label37 = new System.Windows.Forms.Label();
            this.txtVAT = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.txtPriceUnit = new System.Windows.Forms.TextBox();
            this.chIsPrices = new System.Windows.Forms.CheckBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.txtPriceList = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtDiscQuarter = new System.Windows.Forms.TextBox();
            this.dataGridView_Part = new System.Windows.Forms.DataGridView();
            this.cCancel = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dataGridView_tmpPart = new System.Windows.Forms.DataGridView();
            this.btBtPart_select = new System.Windows.Forms.Button();
            this.txtPartComment = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.cob_TrnType = new System.Windows.Forms.ComboBox();
            this.radTranType1 = new System.Windows.Forms.RadioButton();
            this.radTranType0 = new System.Windows.Forms.RadioButton();
            this.txtPartQT = new System.Windows.Forms.TextBox();
            this.txtPartName = new System.Windows.Forms.TextBox();
            this.txtPartNo = new System.Windows.Forms.TextBox();
            this.cob_PoitFail0 = new System.Windows.Forms.ComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.lblIsContract = new System.Windows.Forms.Label();
            this.txtContract = new System.Windows.Forms.TextBox();
            this.btJobContentMent = new System.Windows.Forms.Button();
            this.btContact_Detail = new System.Windows.Forms.Button();
            this.btEditContractNo = new System.Windows.Forms.Button();
            this.btUpdatePart = new System.Windows.Forms.Button();
            this.label_2 = new System.Windows.Forms.Label();
            this.label_3 = new System.Windows.Forms.Label();
            this.toolStrip_Mnu.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_FailPoint)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.panel_selectEndDate.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Part)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_tmpPart)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip_Mnu
            // 
            this.toolStrip_Mnu.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip_Mnu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripComboBox1,
            this.toolStripSeparator4,
            this.toolStripLabel_cobby,
            this.toolStripSeparator6,
            this.toolStripText_txtFind,
            this.toolStripSeparator2,
            this.toolStripButton_FindStation,
            this.toolStripSeparator3,
            this.toolStripButton_Save,
            this.toolStripButton_Cancel,
            this.toolStripSeparator1,
            this.toolStripButton_Print,
            this.toolStripButton_TAGPrimt,
            this.btFTRPrint,
            this.toolStripSeparator5,
            this.toolStripButton_Exit,
            this.toolStripLabel_Dateime,
            this.toolStripLabel_QUARTER});
            this.toolStrip_Mnu.Location = new System.Drawing.Point(0, 0);
            this.toolStrip_Mnu.Name = "toolStrip_Mnu";
            this.toolStrip_Mnu.Size = new System.Drawing.Size(1164, 75);
            this.toolStrip_Mnu.TabIndex = 3;
            // 
            // toolStripComboBox1
            // 
            this.toolStripComboBox1.Items.AddRange(new object[] {
            "บันทึก ออก/ปิดงาน",
            "แก้ไข ออก/ปิดงาน"});
            this.toolStripComboBox1.Name = "toolStripComboBox1";
            this.toolStripComboBox1.Size = new System.Drawing.Size(130, 75);
            this.toolStripComboBox1.Text = "Mode...";
            this.toolStripComboBox1.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBox1_SelectedIndexChanged);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 75);
            // 
            // toolStripLabel_cobby
            // 
            this.toolStripLabel_cobby.AutoSize = false;
            this.toolStripLabel_cobby.Enabled = false;
            this.toolStripLabel_cobby.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.toolStripLabel_cobby.ForeColor = System.Drawing.SystemColors.Desktop;
            this.toolStripLabel_cobby.Items.AddRange(new object[] {
            "ตาม JOB ID"});
            this.toolStripLabel_cobby.Name = "toolStripLabel_cobby";
            this.toolStripLabel_cobby.Size = new System.Drawing.Size(100, 47);
            this.toolStripLabel_cobby.Text = "ค้นหาข้อมูล...";
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 75);
            // 
            // toolStripText_txtFind
            // 
            this.toolStripText_txtFind.AutoSize = false;
            this.toolStripText_txtFind.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.toolStripText_txtFind.ForeColor = System.Drawing.SystemColors.Desktop;
            this.toolStripText_txtFind.Name = "toolStripText_txtFind";
            this.toolStripText_txtFind.Size = new System.Drawing.Size(120, 20);
            this.toolStripText_txtFind.KeyDown += new System.Windows.Forms.KeyEventHandler(this.toolStripText_txtFind_KeyDown);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 75);
            // 
            // toolStripButton_FindStation
            // 
            this.toolStripButton_FindStation.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_FindStation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.toolStripButton_FindStation.Image = global::SMSMINI.Properties.Resources.Find48;
            this.toolStripButton_FindStation.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_FindStation.Name = "toolStripButton_FindStation";
            this.toolStripButton_FindStation.Size = new System.Drawing.Size(69, 66);
            this.toolStripButton_FindStation.ToolTipText = "ค้นหาข้อมูล";
            this.toolStripButton_FindStation.Click += new System.EventHandler(this.toolStripButton_FindStation_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 75);
            // 
            // toolStripButton_Save
            // 
            this.toolStripButton_Save.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.toolStripButton_Save.ForeColor = System.Drawing.SystemColors.Desktop;
            this.toolStripButton_Save.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Save.Image")));
            this.toolStripButton_Save.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Save.Name = "toolStripButton_Save";
            this.toolStripButton_Save.Size = new System.Drawing.Size(128, 66);
            this.toolStripButton_Save.Text = "Save";
            this.toolStripButton_Save.ToolTipText = "บันทึกข้อมูล";
            this.toolStripButton_Save.Click += new System.EventHandler(this.toolStripButton_Save_Click);
            // 
            // toolStripButton_Cancel
            // 
            this.toolStripButton_Cancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.toolStripButton_Cancel.ForeColor = System.Drawing.SystemColors.Desktop;
            this.toolStripButton_Cancel.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Cancel.Image")));
            this.toolStripButton_Cancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Cancel.Name = "toolStripButton_Cancel";
            this.toolStripButton_Cancel.Size = new System.Drawing.Size(155, 66);
            this.toolStripButton_Cancel.Text = "Cancel";
            this.toolStripButton_Cancel.ToolTipText = "ยกเลิกข้อมูล";
            this.toolStripButton_Cancel.Click += new System.EventHandler(this.toolStripButton_Cancel_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 75);
            // 
            // toolStripButton_Print
            // 
            this.toolStripButton_Print.Image = global::SMSMINI.Properties.Resources.print_f2;
            this.toolStripButton_Print.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Print.Name = "toolStripButton_Print";
            this.toolStripButton_Print.Size = new System.Drawing.Size(191, 66);
            this.toolStripButton_Print.Text = "SV Print ";
            this.toolStripButton_Print.ToolTipText = "พิมพ์รายงาน Services Reports";
            this.toolStripButton_Print.Click += new System.EventHandler(this.toolStripButton_Print_Click);
            // 
            // toolStripButton_TAGPrimt
            // 
            this.toolStripButton_TAGPrimt.Image = global::SMSMINI.Properties.Resources.print_f2;
            this.toolStripButton_TAGPrimt.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_TAGPrimt.Name = "toolStripButton_TAGPrimt";
            this.toolStripButton_TAGPrimt.Size = new System.Drawing.Size(204, 52);
            this.toolStripButton_TAGPrimt.Text = "TAG Print";
            this.toolStripButton_TAGPrimt.ToolTipText = " Return Parts Routing Tag Print";
            this.toolStripButton_TAGPrimt.Click += new System.EventHandler(this.toolStripButton_TAGPrimt_Click);
            // 
            // btFTRPrint
            // 
            this.btFTRPrint.Image = global::SMSMINI.Properties.Resources.print_f2;
            this.btFTRPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btFTRPrint.Name = "btFTRPrint";
            this.btFTRPrint.Size = new System.Drawing.Size(199, 52);
            this.btFTRPrint.Text = "FTR Print";
            this.btFTRPrint.Click += new System.EventHandler(this.btFTRPrint_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 225);
            // 
            // toolStripButton_Exit
            // 
            this.toolStripButton_Exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.toolStripButton_Exit.ForeColor = System.Drawing.SystemColors.Desktop;
            this.toolStripButton_Exit.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Exit.Image")));
            this.toolStripButton_Exit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Exit.Name = "toolStripButton_Exit";
            this.toolStripButton_Exit.Size = new System.Drawing.Size(108, 42);
            this.toolStripButton_Exit.Text = "Exit";
            this.toolStripButton_Exit.ToolTipText = "ออกจากหน้าจอ";
            this.toolStripButton_Exit.Click += new System.EventHandler(this.toolStripButton_Exit_Click);
            // 
            // toolStripLabel_Dateime
            // 
            this.toolStripLabel_Dateime.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripLabel_Dateime.Font = new System.Drawing.Font("Tahoma", 10F);
            this.toolStripLabel_Dateime.ForeColor = System.Drawing.SystemColors.Desktop;
            this.toolStripLabel_Dateime.Name = "toolStripLabel_Dateime";
            this.toolStripLabel_Dateime.Size = new System.Drawing.Size(0, 0);
            // 
            // toolStripLabel_QUARTER
            // 
            this.toolStripLabel_QUARTER.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.toolStripLabel_QUARTER.ForeColor = System.Drawing.Color.Blue;
            this.toolStripLabel_QUARTER.Name = "toolStripLabel_QUARTER";
            this.toolStripLabel_QUARTER.Size = new System.Drawing.Size(266, 48);
            this.toolStripLabel_QUARTER.Text = "QUARTER: ?";
            this.toolStripLabel_QUARTER.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.toolStripLabel_QUARTER.Visible = false;
            // 
            // radioButton_D
            // 
            this.radioButton_D.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radioButton_D.AutoSize = true;
            this.radioButton_D.Enabled = false;
            this.radioButton_D.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radioButton_D.Location = new System.Drawing.Point(714, 39);
            this.radioButton_D.Name = "radioButton_D";
            this.radioButton_D.Size = new System.Drawing.Size(210, 42);
            this.radioButton_D.TabIndex = 6;
            this.radioButton_D.TabStop = true;
            this.radioButton_D.Text = "(D) ไม่ระบุ";
            this.radioButton_D.UseVisualStyleBackColor = true;
            this.radioButton_D.Visible = false;
            // 
            // radioButton_C
            // 
            this.radioButton_C.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radioButton_C.AutoSize = true;
            this.radioButton_C.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radioButton_C.Location = new System.Drawing.Point(622, 39);
            this.radioButton_C.Name = "radioButton_C";
            this.radioButton_C.Size = new System.Drawing.Size(229, 42);
            this.radioButton_C.TabIndex = 5;
            this.radioButton_C.TabStop = true;
            this.radioButton_C.Text = "(C) ปรับปรุง";
            this.radioButton_C.UseVisualStyleBackColor = true;
            this.radioButton_C.Visible = false;
            this.radioButton_C.MouseClick += new System.Windows.Forms.MouseEventHandler(this.radioButton_C_MouseClick);
            // 
            // radioButton_B
            // 
            this.radioButton_B.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radioButton_B.AutoSize = true;
            this.radioButton_B.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radioButton_B.Location = new System.Drawing.Point(540, 39);
            this.radioButton_B.Name = "radioButton_B";
            this.radioButton_B.Size = new System.Drawing.Size(194, 42);
            this.radioButton_B.TabIndex = 4;
            this.radioButton_B.TabStop = true;
            this.radioButton_B.Text = "(B) พอใช้";
            this.radioButton_B.UseVisualStyleBackColor = true;
            this.radioButton_B.Visible = false;
            // 
            // radioButton_A
            // 
            this.radioButton_A.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radioButton_A.AutoSize = true;
            this.radioButton_A.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radioButton_A.Location = new System.Drawing.Point(475, 39);
            this.radioButton_A.Name = "radioButton_A";
            this.radioButton_A.Size = new System.Drawing.Size(138, 42);
            this.radioButton_A.TabIndex = 3;
            this.radioButton_A.TabStop = true;
            this.radioButton_A.Text = "(A) ดี";
            this.radioButton_A.UseVisualStyleBackColor = true;
            this.radioButton_A.Visible = false;
            // 
            // cobProblemType
            // 
            this.cobProblemType.FormattingEnabled = true;
            this.cobProblemType.Location = new System.Drawing.Point(75, 62);
            this.cobProblemType.Name = "cobProblemType";
            this.cobProblemType.Size = new System.Drawing.Size(304, 45);
            this.cobProblemType.TabIndex = 2;
            this.cobProblemType.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cobProblemType_KeyPress);
            // 
            // label33
            // 
            this.label33.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label33.Location = new System.Drawing.Point(384, 68);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(210, 38);
            this.label33.TabIndex = 53;
            this.label33.Text = "ความพึงพอใจ:";
            this.label33.Visible = false;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(2, 65);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(224, 37);
            this.label32.TabIndex = 52;
            this.label32.Text = "Problem Type:";
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.label31);
            this.groupBox4.Controls.Add(this.txtResolve_Detail);
            this.groupBox4.Controls.Add(this.cobProblemType);
            this.groupBox4.Controls.Add(this.radioButton_D);
            this.groupBox4.Controls.Add(this.label32);
            this.groupBox4.Controls.Add(this.radioButton_C);
            this.groupBox4.Controls.Add(this.label33);
            this.groupBox4.Controls.Add(this.radioButton_B);
            this.groupBox4.Controls.Add(this.radioButton_A);
            this.groupBox4.Location = new System.Drawing.Point(15, 483);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(1144, 89);
            this.groupBox4.TabIndex = 51;
            this.groupBox4.TabStop = false;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label31.Location = new System.Drawing.Point(2, 8);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(221, 38);
            this.label31.TabIndex = 25;
            this.label31.Text = "หมายเหตุอื่นๆ: ";
            // 
            // txtResolve_Detail
            // 
            this.txtResolve_Detail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtResolve_Detail.Location = new System.Drawing.Point(5, 24);
            this.txtResolve_Detail.Multiline = true;
            this.txtResolve_Detail.Name = "txtResolve_Detail";
            this.txtResolve_Detail.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtResolve_Detail.Size = new System.Drawing.Size(1135, 33);
            this.txtResolve_Detail.TabIndex = 1;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.cbxChFCAT);
            this.groupBox2.Controls.Add(this.label49);
            this.groupBox2.Controls.Add(this.cob_SolvingByWI);
            this.groupBox2.Controls.Add(this.cbxLocation);
            this.groupBox2.Controls.Add(this.pUpSNNew);
            this.groupBox2.Controls.Add(this.pUpSNOld);
            this.groupBox2.Controls.Add(this.btSelectPointFail);
            this.groupBox2.Controls.Add(this.label51);
            this.groupBox2.Controls.Add(this.cob_Failure_Action_Solving);
            this.groupBox2.Controls.Add(this.label50);
            this.groupBox2.Controls.Add(this.cobOilType);
            this.groupBox2.Controls.Add(this.label47);
            this.groupBox2.Controls.Add(this.cob_Failure_Action);
            this.groupBox2.Controls.Add(this.label46);
            this.groupBox2.Controls.Add(this.btLoadSNNew);
            this.groupBox2.Controls.Add(this.txtSNOld);
            this.groupBox2.Controls.Add(this.lblWarranty);
            this.groupBox2.Controls.Add(this.txtFixAsetNoNew);
            this.groupBox2.Controls.Add(this.label42);
            this.groupBox2.Controls.Add(this.txtFixAssetNoOld);
            this.groupBox2.Controls.Add(this.label41);
            this.groupBox2.Controls.Add(this.txtSNNew);
            this.groupBox2.Controls.Add(this.label40);
            this.groupBox2.Controls.Add(this.btClearData);
            this.groupBox2.Controls.Add(this.lblFailureCode);
            this.groupBox2.Controls.Add(this.dataGridView_FailPoint);
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.txtLiterTest);
            this.groupBox2.Controls.Add(this.txtEndtLite);
            this.groupBox2.Controls.Add(this.txtStartLite);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.txtFailure);
            this.groupBox2.Controls.Add(this.txtModel);
            this.groupBox2.Controls.Add(this.comboBox_PoitFail);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Location = new System.Drawing.Point(2, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1118, 212);
            this.groupBox2.TabIndex = 49;
            this.groupBox2.TabStop = false;
            // 
            // cbxChFCAT
            // 
            this.cbxChFCAT.FormattingEnabled = true;
            this.cbxChFCAT.Location = new System.Drawing.Point(6, 31);
            this.cbxChFCAT.Name = "cbxChFCAT";
            this.cbxChFCAT.Size = new System.Drawing.Size(134, 45);
            this.cbxChFCAT.TabIndex = 59;
            this.cbxChFCAT.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbxChFCAT_KeyPress);
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(2, 16);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(271, 37);
            this.label49.TabIndex = 58;
            this.label49.Text = "เลือกประเภทอุปกรณ์:";
            // 
            // cob_SolvingByWI
            // 
            this.cob_SolvingByWI.DropDownWidth = 270;
            this.cob_SolvingByWI.FormattingEnabled = true;
            this.cob_SolvingByWI.Location = new System.Drawing.Point(635, 110);
            this.cob_SolvingByWI.Name = "cob_SolvingByWI";
            this.cob_SolvingByWI.Size = new System.Drawing.Size(151, 45);
            this.cob_SolvingByWI.TabIndex = 57;
            this.cob_SolvingByWI.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cob_SolvingByWI_KeyPress);
            // 
            // cbxLocation
            // 
            this.cbxLocation.FormattingEnabled = true;
            this.cbxLocation.Location = new System.Drawing.Point(480, 31);
            this.cbxLocation.Name = "cbxLocation";
            this.cbxLocation.Size = new System.Drawing.Size(115, 45);
            this.cbxLocation.TabIndex = 56;
            this.cbxLocation.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbxLocation_KeyPress);
            // 
            // pUpSNNew
            // 
            this.pUpSNNew.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.pUpSNNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.pUpSNNew.Location = new System.Drawing.Point(451, 30);
            this.pUpSNNew.Name = "pUpSNNew";
            this.pUpSNNew.Size = new System.Drawing.Size(25, 23);
            this.pUpSNNew.TabIndex = 55;
            this.pUpSNNew.Text = "...";
            this.pUpSNNew.UseVisualStyleBackColor = false;
            this.pUpSNNew.Click += new System.EventHandler(this.pUpSNNew_Click);
            // 
            // pUpSNOld
            // 
            this.pUpSNOld.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.pUpSNOld.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.pUpSNOld.Location = new System.Drawing.Point(283, 30);
            this.pUpSNOld.Name = "pUpSNOld";
            this.pUpSNOld.Size = new System.Drawing.Size(25, 23);
            this.pUpSNOld.TabIndex = 54;
            this.pUpSNOld.Text = "...";
            this.pUpSNOld.UseVisualStyleBackColor = false;
            this.pUpSNOld.Click += new System.EventHandler(this.pUpSNOld_Click);
            // 
            // btSelectPointFail
            // 
            this.btSelectPointFail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btSelectPointFail.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btSelectPointFail.Location = new System.Drawing.Point(1027, 93);
            this.btSelectPointFail.Name = "btSelectPointFail";
            this.btSelectPointFail.Size = new System.Drawing.Size(85, 41);
            this.btSelectPointFail.TabIndex = 8;
            this.btSelectPointFail.Text = "เลือกจุดเสีย:";
            this.btSelectPointFail.UseVisualStyleBackColor = true;
            this.btSelectPointFail.Click += new System.EventHandler(this.btSelectPointFail_Click);
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(631, 96);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(148, 37);
            this.label51.TabIndex = 53;
            this.label51.Text = "ระบุ WI...:";
            // 
            // cob_Failure_Action_Solving
            // 
            this.cob_Failure_Action_Solving.FormattingEnabled = true;
            this.cob_Failure_Action_Solving.Location = new System.Drawing.Point(635, 70);
            this.cob_Failure_Action_Solving.Name = "cob_Failure_Action_Solving";
            this.cob_Failure_Action_Solving.Size = new System.Drawing.Size(269, 45);
            this.cob_Failure_Action_Solving.TabIndex = 52;
            this.cob_Failure_Action_Solving.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cob_Failure_Action_Solving_KeyPress);
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(632, 57);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(287, 37);
            this.label50.TabIndex = 51;
            this.label50.Text = "การแก้ไข...ตาม WI...:";
            // 
            // cobOilType
            // 
            this.cobOilType.FormattingEnabled = true;
            this.cobOilType.Location = new System.Drawing.Point(76, 110);
            this.cobOilType.Name = "cobOilType";
            this.cobOilType.Size = new System.Drawing.Size(139, 45);
            this.cobOilType.TabIndex = 44;
            this.cobOilType.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cobOilType_KeyPress);
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(73, 95);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(153, 37);
            this.label47.TabIndex = 43;
            this.label47.Text = "ชนิดน้ำมัน:";
            // 
            // cob_Failure_Action
            // 
            this.cob_Failure_Action.FormattingEnabled = true;
            this.cob_Failure_Action.Location = new System.Drawing.Point(414, 70);
            this.cob_Failure_Action.Name = "cob_Failure_Action";
            this.cob_Failure_Action.Size = new System.Drawing.Size(216, 45);
            this.cob_Failure_Action.TabIndex = 42;
            this.cob_Failure_Action.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cob_Failure_Action_KeyPress);
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(411, 55);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(102, 37);
            this.label46.TabIndex = 41;
            this.label46.Text = "สาเหตุ:";
            // 
            // btLoadSNNew
            // 
            this.btLoadSNNew.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btLoadSNNew.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btLoadSNNew.Location = new System.Drawing.Point(360, 8);
            this.btLoadSNNew.Name = "btLoadSNNew";
            this.btLoadSNNew.Size = new System.Drawing.Size(91, 23);
            this.btLoadSNNew.TabIndex = 39;
            this.btLoadSNNew.Text = "Load SN New";
            this.btLoadSNNew.UseVisualStyleBackColor = false;
            this.btLoadSNNew.Visible = false;
            this.btLoadSNNew.Click += new System.EventHandler(this.btLoadSNNew_Click);
            // 
            // txtSNOld
            // 
            this.txtSNOld.Location = new System.Drawing.Point(143, 32);
            this.txtSNOld.MaxLength = 50;
            this.txtSNOld.Name = "txtSNOld";
            this.txtSNOld.Size = new System.Drawing.Size(139, 44);
            this.txtSNOld.TabIndex = 1;
            this.txtSNOld.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSN_KeyDown);
            // 
            // lblWarranty
            // 
            this.lblWarranty.AutoSize = true;
            this.lblWarranty.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblWarranty.ForeColor = System.Drawing.Color.Red;
            this.lblWarranty.Location = new System.Drawing.Point(188, 16);
            this.lblWarranty.Name = "lblWarranty";
            this.lblWarranty.Size = new System.Drawing.Size(206, 38);
            this.lblWarranty.TabIndex = 38;
            this.lblWarranty.Text = "(อยู่ในประกัน)";
            this.lblWarranty.Visible = false;
            // 
            // txtFixAsetNoNew
            // 
            this.txtFixAsetNoNew.Location = new System.Drawing.Point(267, 71);
            this.txtFixAsetNoNew.Name = "txtFixAsetNoNew";
            this.txtFixAsetNoNew.Size = new System.Drawing.Size(141, 44);
            this.txtFixAsetNoNew.TabIndex = 36;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(264, 55);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(281, 37);
            this.label42.TabIndex = 37;
            this.label42.Text = "FixAsset No. New:";
            // 
            // txtFixAssetNoOld
            // 
            this.txtFixAssetNoOld.Location = new System.Drawing.Point(122, 71);
            this.txtFixAssetNoOld.Name = "txtFixAssetNoOld";
            this.txtFixAssetNoOld.Size = new System.Drawing.Size(139, 44);
            this.txtFixAssetNoOld.TabIndex = 34;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(122, 57);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(258, 37);
            this.label41.TabIndex = 35;
            this.label41.Text = "FixAsset No. Old";
            // 
            // txtSNNew
            // 
            this.txtSNNew.Location = new System.Drawing.Point(310, 32);
            this.txtSNNew.MaxLength = 50;
            this.txtSNNew.Name = "txtSNNew";
            this.txtSNNew.Size = new System.Drawing.Size(141, 44);
            this.txtSNNew.TabIndex = 32;
            this.txtSNNew.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSNNew_KeyDown);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(307, 16);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(153, 37);
            this.label40.TabIndex = 33;
            this.label40.Text = "S/N New:";
            // 
            // btClearData
            // 
            this.btClearData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btClearData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btClearData.ForeColor = System.Drawing.Color.Red;
            this.btClearData.Location = new System.Drawing.Point(945, 6);
            this.btClearData.Name = "btClearData";
            this.btClearData.Size = new System.Drawing.Size(80, 23);
            this.btClearData.TabIndex = 31;
            this.btClearData.Text = "ล้างข้อมูล...";
            this.btClearData.UseVisualStyleBackColor = false;
            this.btClearData.Click += new System.EventHandler(this.btClearData_Click);
            // 
            // lblFailureCode
            // 
            this.lblFailureCode.AutoSize = true;
            this.lblFailureCode.Location = new System.Drawing.Point(676, 16);
            this.lblFailureCode.Name = "lblFailureCode";
            this.lblFailureCode.Size = new System.Drawing.Size(191, 37);
            this.lblFailureCode.TabIndex = 30;
            this.lblFailureCode.Text = "FailureCode";
            // 
            // dataGridView_FailPoint
            // 
            this.dataGridView_FailPoint.AllowUserToAddRows = false;
            this.dataGridView_FailPoint.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_FailPoint.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGridView_FailPoint.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_FailPoint.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clCancel,
            this.cAddPart});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView_FailPoint.DefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView_FailPoint.Location = new System.Drawing.Point(6, 135);
            this.dataGridView_FailPoint.Name = "dataGridView_FailPoint";
            this.dataGridView_FailPoint.RowHeadersWidth = 123;
            this.dataGridView_FailPoint.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridView_FailPoint.Size = new System.Drawing.Size(1105, 56);
            this.dataGridView_FailPoint.TabIndex = 28;
            this.dataGridView_FailPoint.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_FailPoint_CellClick);
            this.dataGridView_FailPoint.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_FailPoint_CellEndEdit);
            this.dataGridView_FailPoint.CurrentCellDirtyStateChanged += new System.EventHandler(this.dataGridView_FailPoint_CurrentCellDirtyStateChanged);
            this.dataGridView_FailPoint.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dataGridView_FailPoint_RowsAdded);
            // 
            // clCancel
            // 
            this.clCancel.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.clCancel.FillWeight = 80F;
            this.clCancel.HeaderText = "";
            this.clCancel.MinimumWidth = 15;
            this.clCancel.Name = "clCancel";
            this.clCancel.Text = "ยกเลิก";
            this.clCancel.ToolTipText = "ยกเลิก";
            this.clCancel.UseColumnTextForButtonValue = true;
            this.clCancel.Width = 15;
            // 
            // cAddPart
            // 
            this.cAddPart.HeaderText = "";
            this.cAddPart.MinimumWidth = 15;
            this.cAddPart.Name = "cAddPart";
            this.cAddPart.Text = "Add Part";
            this.cAddPart.ToolTipText = "เพิ่มอะไหล่";
            this.cAddPart.UseColumnTextForButtonValue = true;
            this.cAddPart.Width = 15;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.button1.Location = new System.Drawing.Point(1031, 6);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(80, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "ค้นหา...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(587, 96);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(113, 37);
            this.label20.TabIndex = 23;
            this.label20.Text = "ทดสอบ:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(521, 96);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(74, 37);
            this.label19.TabIndex = 22;
            this.label19.Text = "หลัง:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(427, 96);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(79, 37);
            this.label18.TabIndex = 21;
            this.label18.Text = "ก่อน:";
            // 
            // txtLiterTest
            // 
            this.txtLiterTest.Location = new System.Drawing.Point(546, 111);
            this.txtLiterTest.Name = "txtLiterTest";
            this.txtLiterTest.ReadOnly = true;
            this.txtLiterTest.Size = new System.Drawing.Size(83, 44);
            this.txtLiterTest.TabIndex = 7;
            this.txtLiterTest.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtEndtLite
            // 
            this.txtEndtLite.Location = new System.Drawing.Point(457, 111);
            this.txtEndtLite.Name = "txtEndtLite";
            this.txtEndtLite.Size = new System.Drawing.Size(83, 44);
            this.txtEndtLite.TabIndex = 7;
            this.txtEndtLite.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtEndtLite.TextChanged += new System.EventHandler(this.txtEndtLite_TextChanged);
            this.txtEndtLite.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtEndtLite_KeyDown);
            this.txtEndtLite.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEndtLite_KeyPress);
            // 
            // txtStartLite
            // 
            this.txtStartLite.Location = new System.Drawing.Point(368, 111);
            this.txtStartLite.Name = "txtStartLite";
            this.txtStartLite.Size = new System.Drawing.Size(83, 44);
            this.txtStartLite.TabIndex = 6;
            this.txtStartLite.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtStartLite.TextChanged += new System.EventHandler(this.txtStartLite_TextChanged);
            this.txtStartLite.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtStartLite_KeyDown);
            this.txtStartLite.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtStartLite_KeyPress);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(365, 96);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(165, 37);
            this.label17.TabIndex = 17;
            this.label17.Text = "เลขรวมลิตร:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(596, 16);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(200, 38);
            this.label16.TabIndex = 16;
            this.label16.Text = "อาการเสียพบ:";
            // 
            // txtFailure
            // 
            this.txtFailure.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFailure.Location = new System.Drawing.Point(599, 32);
            this.txtFailure.Name = "txtFailure";
            this.txtFailure.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtFailure.Size = new System.Drawing.Size(512, 44);
            this.txtFailure.TabIndex = 3;
            // 
            // txtModel
            // 
            this.txtModel.Location = new System.Drawing.Point(6, 70);
            this.txtModel.Name = "txtModel";
            this.txtModel.Size = new System.Drawing.Size(110, 44);
            this.txtModel.TabIndex = 0;
            this.txtModel.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtLocation_KeyDown);
            // 
            // comboBox_PoitFail
            // 
            this.comboBox_PoitFail.FormattingEnabled = true;
            this.comboBox_PoitFail.Location = new System.Drawing.Point(6, 109);
            this.comboBox_PoitFail.Name = "comboBox_PoitFail";
            this.comboBox_PoitFail.Size = new System.Drawing.Size(64, 45);
            this.comboBox_PoitFail.TabIndex = 5;
            this.comboBox_PoitFail.SelectedIndexChanged += new System.EventHandler(this.comboBox_PoitFail_SelectedIndexChanged);
            this.comboBox_PoitFail.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboBox_PoitFail_KeyPress);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(2, 56);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(112, 37);
            this.label15.TabIndex = 9;
            this.label15.Text = "Model:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(476, 16);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(148, 37);
            this.label14.TabIndex = 8;
            this.label14.Text = "Location:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(143, 18);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(139, 37);
            this.label13.TabIndex = 7;
            this.label13.Text = "S/N Old:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 94);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(101, 37);
            this.label12.TabIndex = 6;
            this.label12.Text = "จุดเสีย:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label_1);
            this.groupBox1.Controls.Add(this.btUpdateWorkType);
            this.groupBox1.Controls.Add(this.lblLastEndDate);
            this.groupBox1.Controls.Add(this.txtWorkID);
            this.groupBox1.Controls.Add(this.label30);
            this.groupBox1.Controls.Add(this.txtJobSO);
            this.groupBox1.Controls.Add(this.label44);
            this.groupBox1.Controls.Add(this.txtWorkType);
            this.groupBox1.Controls.Add(this.txtProjectNO);
            this.groupBox1.Controls.Add(this.label34);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtStartDateIn);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.txtStation);
            this.groupBox1.Controls.Add(this.txtUserOpenJob);
            this.groupBox1.Controls.Add(this.txtInformer);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.FailureDetail);
            this.groupBox1.Controls.Add(this.txtFialureCode);
            this.groupBox1.Controls.Add(this.txtPointFaile);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.lbl_JobType);
            this.groupBox1.Controls.Add(this.comboBox_Priority);
            this.groupBox1.Controls.Add(this.txtOpenJOB);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtJobID);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.panel_selectEndDate);
            this.groupBox1.Location = new System.Drawing.Point(15, 47);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(800, 147);
            this.groupBox1.TabIndex = 48;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "รายละเอียดใบ JOB";
            // 
            // label_1
            // 
            this.label_1.AutoSize = true;
            this.label_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label_1.ForeColor = System.Drawing.Color.Blue;
            this.label_1.Location = new System.Drawing.Point(365, 14);
            this.label_1.Name = "label_1";
            this.label_1.Size = new System.Drawing.Size(71, 46);
            this.label_1.TabIndex = 57;
            this.label_1.Text = "(1)";
            // 
            // btUpdateWorkType
            // 
            this.btUpdateWorkType.BackColor = System.Drawing.Color.Lime;
            this.btUpdateWorkType.Enabled = false;
            this.btUpdateWorkType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btUpdateWorkType.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btUpdateWorkType.Location = new System.Drawing.Point(368, 33);
            this.btUpdateWorkType.Name = "btUpdateWorkType";
            this.btUpdateWorkType.Size = new System.Drawing.Size(86, 25);
            this.btUpdateWorkType.TabIndex = 56;
            this.btUpdateWorkType.Text = "ประเภทงาน:";
            this.btUpdateWorkType.UseVisualStyleBackColor = false;
            this.btUpdateWorkType.Click += new System.EventHandler(this.btUpdateWorkType_Click);
            // 
            // lblLastEndDate
            // 
            this.lblLastEndDate.AutoSize = true;
            this.lblLastEndDate.Location = new System.Drawing.Point(98, 45);
            this.lblLastEndDate.Name = "lblLastEndDate";
            this.lblLastEndDate.Size = new System.Drawing.Size(120, 37);
            this.lblLastEndDate.TabIndex = 55;
            this.lblLastEndDate.Text = "label52";
            this.lblLastEndDate.Visible = false;
            // 
            // txtWorkID
            // 
            this.txtWorkID.Location = new System.Drawing.Point(666, 124);
            this.txtWorkID.Name = "txtWorkID";
            this.txtWorkID.ReadOnly = true;
            this.txtWorkID.Size = new System.Drawing.Size(128, 44);
            this.txtWorkID.TabIndex = 54;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(631, 127);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(91, 37);
            this.label30.TabIndex = 53;
            this.label30.Text = "W/O:";
            // 
            // txtJobSO
            // 
            this.txtJobSO.Location = new System.Drawing.Point(666, 102);
            this.txtJobSO.Name = "txtJobSO";
            this.txtJobSO.ReadOnly = true;
            this.txtJobSO.Size = new System.Drawing.Size(128, 44);
            this.txtJobSO.TabIndex = 33;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(640, 105);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(72, 37);
            this.label44.TabIndex = 32;
            this.label44.Text = "SO:";
            // 
            // txtWorkType
            // 
            this.txtWorkType.Location = new System.Drawing.Point(460, 38);
            this.txtWorkType.Name = "txtWorkType";
            this.txtWorkType.ReadOnly = true;
            this.txtWorkType.Size = new System.Drawing.Size(158, 44);
            this.txtWorkType.TabIndex = 31;
            // 
            // txtProjectNO
            // 
            this.txtProjectNO.Location = new System.Drawing.Point(666, 79);
            this.txtProjectNO.Name = "txtProjectNO";
            this.txtProjectNO.ReadOnly = true;
            this.txtProjectNO.Size = new System.Drawing.Size(128, 44);
            this.txtProjectNO.TabIndex = 29;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(608, 84);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(170, 37);
            this.label34.TabIndex = 28;
            this.label34.Text = "Project no:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(624, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(125, 37);
            this.label3.TabIndex = 9;
            this.label3.Text = "Priority:";
            // 
            // txtStartDateIn
            // 
            this.txtStartDateIn.Location = new System.Drawing.Point(460, 16);
            this.txtStartDateIn.Name = "txtStartDateIn";
            this.txtStartDateIn.ReadOnly = true;
            this.txtStartDateIn.Size = new System.Drawing.Size(158, 44);
            this.txtStartDateIn.TabIndex = 27;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(428, 18);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 37);
            this.label11.TabIndex = 26;
            this.label11.Text = "เข้า:";
            // 
            // txtStation
            // 
            this.txtStation.Location = new System.Drawing.Point(64, 123);
            this.txtStation.Name = "txtStation";
            this.txtStation.ReadOnly = true;
            this.txtStation.Size = new System.Drawing.Size(528, 44);
            this.txtStation.TabIndex = 25;
            // 
            // txtUserOpenJob
            // 
            this.txtUserOpenJob.Location = new System.Drawing.Point(368, 97);
            this.txtUserOpenJob.Name = "txtUserOpenJob";
            this.txtUserOpenJob.ReadOnly = true;
            this.txtUserOpenJob.Size = new System.Drawing.Size(224, 44);
            this.txtUserOpenJob.TabIndex = 24;
            // 
            // txtInformer
            // 
            this.txtInformer.Location = new System.Drawing.Point(64, 98);
            this.txtInformer.Name = "txtInformer";
            this.txtInformer.ReadOnly = true;
            this.txtInformer.Size = new System.Drawing.Size(224, 44);
            this.txtInformer.TabIndex = 23;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(21, 130);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(126, 37);
            this.label10.TabIndex = 22;
            this.label10.Text = "Station:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(305, 104);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(145, 37);
            this.label9.TabIndex = 21;
            this.label9.Text = " ผู้เปิดงาน:";
            // 
            // FailureDetail
            // 
            this.FailureDetail.Location = new System.Drawing.Point(64, 61);
            this.FailureDetail.Multiline = true;
            this.FailureDetail.Name = "FailureDetail";
            this.FailureDetail.ReadOnly = true;
            this.FailureDetail.Size = new System.Drawing.Size(528, 32);
            this.FailureDetail.TabIndex = 20;
            // 
            // txtFialureCode
            // 
            this.txtFialureCode.Location = new System.Drawing.Point(666, 57);
            this.txtFialureCode.Name = "txtFialureCode";
            this.txtFialureCode.ReadOnly = true;
            this.txtFialureCode.Size = new System.Drawing.Size(128, 44);
            this.txtFialureCode.TabIndex = 19;
            // 
            // txtPointFaile
            // 
            this.txtPointFaile.Location = new System.Drawing.Point(233, 38);
            this.txtPointFaile.Name = "txtPointFaile";
            this.txtPointFaile.ReadOnly = true;
            this.txtPointFaile.Size = new System.Drawing.Size(129, 44);
            this.txtPointFaile.TabIndex = 18;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(10, 105);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(138, 37);
            this.label8.TabIndex = 17;
            this.label8.Text = "ผู้แจ้งงาน:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(4, 45);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(233, 37);
            this.label7.TabIndex = 16;
            this.label7.Text = " อาการเสียรับแจ้ง:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(596, 65);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(209, 37);
            this.label6.TabIndex = 15;
            this.label6.Text = "Failure Code:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(187, 41);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(101, 37);
            this.label5.TabIndex = 14;
            this.label5.Text = "จุดเสีย:";
            // 
            // lbl_JobType
            // 
            this.lbl_JobType.AutoSize = true;
            this.lbl_JobType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lbl_JobType.ForeColor = System.Drawing.Color.Red;
            this.lbl_JobType.Location = new System.Drawing.Point(658, 11);
            this.lbl_JobType.Name = "lbl_JobType";
            this.lbl_JobType.Size = new System.Drawing.Size(150, 38);
            this.lbl_JobType.TabIndex = 11;
            this.lbl_JobType.Text = "JobType";
            // 
            // comboBox_Priority
            // 
            this.comboBox_Priority.Enabled = false;
            this.comboBox_Priority.FormattingEnabled = true;
            this.comboBox_Priority.Location = new System.Drawing.Point(666, 33);
            this.comboBox_Priority.Name = "comboBox_Priority";
            this.comboBox_Priority.Size = new System.Drawing.Size(128, 45);
            this.comboBox_Priority.TabIndex = 10;
            // 
            // txtOpenJOB
            // 
            this.txtOpenJOB.Location = new System.Drawing.Point(233, 16);
            this.txtOpenJOB.Name = "txtOpenJOB";
            this.txtOpenJOB.ReadOnly = true;
            this.txtOpenJOB.Size = new System.Drawing.Size(129, 44);
            this.txtOpenJOB.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(182, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(143, 37);
            this.label2.TabIndex = 7;
            this.label2.Text = "เปิด JOB:";
            // 
            // txtJobID
            // 
            this.txtJobID.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtJobID.ForeColor = System.Drawing.Color.Blue;
            this.txtJobID.Location = new System.Drawing.Point(64, 19);
            this.txtJobID.Name = "txtJobID";
            this.txtJobID.ReadOnly = true;
            this.txtJobID.Size = new System.Drawing.Size(117, 53);
            this.txtJobID.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label1.Location = new System.Drawing.Point(1, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(172, 38);
            this.label1.TabIndex = 5;
            this.label1.Text = "เลขที่ JOB:";
            // 
            // panel_selectEndDate
            // 
            this.panel_selectEndDate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.panel_selectEndDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_selectEndDate.Controls.Add(this.label43);
            this.panel_selectEndDate.Controls.Add(this.cobEndDate);
            this.panel_selectEndDate.Controls.Add(this.btSelectDateOK);
            this.panel_selectEndDate.Controls.Add(this.label36);
            this.panel_selectEndDate.Location = new System.Drawing.Point(556, 5);
            this.panel_selectEndDate.Name = "panel_selectEndDate";
            this.panel_selectEndDate.Size = new System.Drawing.Size(224, 117);
            this.panel_selectEndDate.TabIndex = 52;
            this.panel_selectEndDate.Visible = false;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label43.Location = new System.Drawing.Point(3, 60);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(336, 46);
            this.label43.TabIndex = 6;
            this.label43.Text = "วันเวลาออกงาน (F8)";
            // 
            // cobEndDate
            // 
            this.cobEndDate.FormattingEnabled = true;
            this.cobEndDate.Location = new System.Drawing.Point(6, 80);
            this.cobEndDate.Name = "cobEndDate";
            this.cobEndDate.Size = new System.Drawing.Size(209, 45);
            this.cobEndDate.TabIndex = 5;
            // 
            // btSelectDateOK
            // 
            this.btSelectDateOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btSelectDateOK.ForeColor = System.Drawing.Color.Blue;
            this.btSelectDateOK.Location = new System.Drawing.Point(144, 32);
            this.btSelectDateOK.Name = "btSelectDateOK";
            this.btSelectDateOK.Size = new System.Drawing.Size(75, 39);
            this.btSelectDateOK.TabIndex = 4;
            this.btSelectDateOK.Text = "OK";
            this.btSelectDateOK.UseVisualStyleBackColor = true;
            this.btSelectDateOK.Click += new System.EventHandler(this.btSelectDateOK_Click);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label36.Location = new System.Drawing.Point(13, 5);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(521, 55);
            this.label36.TabIndex = 2;
            this.label36.Text = "ระบุวันเวลา(F8) ที่ต้องการ";
            // 
            // lblJobInCount
            // 
            this.lblJobInCount.AutoSize = true;
            this.lblJobInCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblJobInCount.ForeColor = System.Drawing.Color.Red;
            this.lblJobInCount.Location = new System.Drawing.Point(655, 198);
            this.lblJobInCount.Name = "lblJobInCount";
            this.lblJobInCount.Size = new System.Drawing.Size(43, 46);
            this.lblJobInCount.TabIndex = 13;
            this.lblJobInCount.Text = "3";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label4.ForeColor = System.Drawing.Color.Blue;
            this.label4.Location = new System.Drawing.Point(575, 200);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(195, 38);
            this.label4.TabIndex = 12;
            this.label4.Text = "เข้างานครั้งที่:";
            // 
            // RadioButtonList_JobStatus2
            // 
            this.RadioButtonList_JobStatus2.AutoSize = true;
            this.RadioButtonList_JobStatus2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadioButtonList_JobStatus2.ForeColor = System.Drawing.Color.Green;
            this.RadioButtonList_JobStatus2.Location = new System.Drawing.Point(422, 198);
            this.RadioButtonList_JobStatus2.Name = "RadioButtonList_JobStatus2";
            this.RadioButtonList_JobStatus2.Size = new System.Drawing.Size(384, 42);
            this.RadioButtonList_JobStatus2.TabIndex = 4;
            this.RadioButtonList_JobStatus2.TabStop = true;
            this.RadioButtonList_JobStatus2.Text = "บันทึกปิดงาน (ปิด JOB)";
            this.RadioButtonList_JobStatus2.UseVisualStyleBackColor = true;
            this.RadioButtonList_JobStatus2.Click += new System.EventHandler(this.RadioButtonList_JobStatus2_Click);
            // 
            // RadioButtonList_JobStatus1
            // 
            this.RadioButtonList_JobStatus1.AutoSize = true;
            this.RadioButtonList_JobStatus1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.RadioButtonList_JobStatus1.ForeColor = System.Drawing.Color.Red;
            this.RadioButtonList_JobStatus1.Location = new System.Drawing.Point(261, 198);
            this.RadioButtonList_JobStatus1.Name = "RadioButtonList_JobStatus1";
            this.RadioButtonList_JobStatus1.Size = new System.Drawing.Size(404, 42);
            this.RadioButtonList_JobStatus1.TabIndex = 3;
            this.RadioButtonList_JobStatus1.TabStop = true;
            this.RadioButtonList_JobStatus1.Text = "บันทึกงานค้าง (JOB ค้าง)";
            this.RadioButtonList_JobStatus1.UseVisualStyleBackColor = true;
            this.RadioButtonList_JobStatus1.Click += new System.EventHandler(this.RadioButtonList_JobStatus1_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(15, 200);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1144, 287);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Location = new System.Drawing.Point(12, 58);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1120, 217);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "อาการเสียที่พบ(เพิ่มเติม): ";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox3);
            this.tabPage2.Location = new System.Drawing.Point(12, 58);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1120, 217);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "รายละเอียดอะไหล่:";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.txtReferPage);
            this.groupBox3.Controls.Add(this.label48);
            this.groupBox3.Controls.Add(this.txtPartSerialNumberOld);
            this.groupBox3.Controls.Add(this.label45);
            this.groupBox3.Controls.Add(this.lblERP_orderline_id);
            this.groupBox3.Controls.Add(this.cbIsCustomer);
            this.groupBox3.Controls.Add(this.txtFixAssetNo);
            this.groupBox3.Controls.Add(this.label39);
            this.groupBox3.Controls.Add(this.con_ERP_StationCharge);
            this.groupBox3.Controls.Add(this.label37);
            this.groupBox3.Controls.Add(this.txtVAT);
            this.groupBox3.Controls.Add(this.label35);
            this.groupBox3.Controls.Add(this.txtPriceUnit);
            this.groupBox3.Controls.Add(this.chIsPrices);
            this.groupBox3.Controls.Add(this.label28);
            this.groupBox3.Controls.Add(this.label21);
            this.groupBox3.Controls.Add(this.txtPriceList);
            this.groupBox3.Controls.Add(this.label22);
            this.groupBox3.Controls.Add(this.txtDiscQuarter);
            this.groupBox3.Controls.Add(this.dataGridView_Part);
            this.groupBox3.Controls.Add(this.dataGridView_tmpPart);
            this.groupBox3.Controls.Add(this.btBtPart_select);
            this.groupBox3.Controls.Add(this.txtPartComment);
            this.groupBox3.Controls.Add(this.label29);
            this.groupBox3.Controls.Add(this.cob_TrnType);
            this.groupBox3.Controls.Add(this.radTranType1);
            this.groupBox3.Controls.Add(this.radTranType0);
            this.groupBox3.Controls.Add(this.txtPartQT);
            this.groupBox3.Controls.Add(this.txtPartName);
            this.groupBox3.Controls.Add(this.txtPartNo);
            this.groupBox3.Controls.Add(this.cob_PoitFail0);
            this.groupBox3.Controls.Add(this.label27);
            this.groupBox3.Controls.Add(this.label26);
            this.groupBox3.Controls.Add(this.label25);
            this.groupBox3.Controls.Add(this.label24);
            this.groupBox3.Controls.Add(this.label23);
            this.groupBox3.Location = new System.Drawing.Point(12, 5);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1096, 206);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            // 
            // txtReferPage
            // 
            this.txtReferPage.Enabled = false;
            this.txtReferPage.Location = new System.Drawing.Point(180, 106);
            this.txtReferPage.Name = "txtReferPage";
            this.txtReferPage.Size = new System.Drawing.Size(128, 44);
            this.txtReferPage.TabIndex = 78;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(177, 90);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(170, 37);
            this.label48.TabIndex = 77;
            this.label48.Text = "Page/Item:";
            // 
            // txtPartSerialNumberOld
            // 
            this.txtPartSerialNumberOld.Enabled = false;
            this.txtPartSerialNumberOld.Location = new System.Drawing.Point(10, 106);
            this.txtPartSerialNumberOld.Name = "txtPartSerialNumberOld";
            this.txtPartSerialNumberOld.Size = new System.Drawing.Size(164, 44);
            this.txtPartSerialNumberOld.TabIndex = 76;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(7, 90);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(228, 37);
            this.label45.TabIndex = 75;
            this.label45.Text = "อะไหล่.S/N Old:";
            // 
            // lblERP_orderline_id
            // 
            this.lblERP_orderline_id.AutoSize = true;
            this.lblERP_orderline_id.Location = new System.Drawing.Point(115, -1);
            this.lblERP_orderline_id.Name = "lblERP_orderline_id";
            this.lblERP_orderline_id.Size = new System.Drawing.Size(266, 37);
            this.lblERP_orderline_id.TabIndex = 74;
            this.lblERP_orderline_id.Text = "ERP_orderline_id";
            // 
            // cbIsCustomer
            // 
            this.cbIsCustomer.AutoSize = true;
            this.cbIsCustomer.Location = new System.Drawing.Point(241, 8);
            this.cbIsCustomer.Name = "cbIsCustomer";
            this.cbIsCustomer.Size = new System.Drawing.Size(205, 41);
            this.cbIsCustomer.TabIndex = 73;
            this.cbIsCustomer.Text = "อะไหล่ลูกค้า";
            this.cbIsCustomer.UseVisualStyleBackColor = true;
            this.cbIsCustomer.CheckedChanged += new System.EventHandler(this.cbIsCustomer_CheckedChanged);
            // 
            // txtFixAssetNo
            // 
            this.txtFixAssetNo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFixAssetNo.Location = new System.Drawing.Point(514, 65);
            this.txtFixAssetNo.Name = "txtFixAssetNo";
            this.txtFixAssetNo.Size = new System.Drawing.Size(158, 44);
            this.txtFixAssetNo.TabIndex = 72;
            // 
            // label39
            // 
            this.label39.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(511, 50);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(199, 37);
            this.label39.TabIndex = 71;
            this.label39.Text = "FixAsset No.";
            // 
            // con_ERP_StationCharge
            // 
            this.con_ERP_StationCharge.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.con_ERP_StationCharge.FormattingEnabled = true;
            this.con_ERP_StationCharge.Items.AddRange(new object[] {
            "สถานี",
            "บริษัทน้ำมัน"});
            this.con_ERP_StationCharge.Location = new System.Drawing.Point(678, 64);
            this.con_ERP_StationCharge.Name = "con_ERP_StationCharge";
            this.con_ERP_StationCharge.Size = new System.Drawing.Size(111, 45);
            this.con_ERP_StationCharge.TabIndex = 69;
            this.con_ERP_StationCharge.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.con_ERP_StationCharge_KeyPress);
            // 
            // label37
            // 
            this.label37.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(675, 49);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(195, 37);
            this.label37.TabIndex = 70;
            this.label37.Text = "ออกบิลในนาม:";
            // 
            // txtVAT
            // 
            this.txtVAT.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtVAT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtVAT.Location = new System.Drawing.Point(771, 25);
            this.txtVAT.Name = "txtVAT";
            this.txtVAT.ReadOnly = true;
            this.txtVAT.Size = new System.Drawing.Size(40, 48);
            this.txtVAT.TabIndex = 4;
            this.txtVAT.Text = "7.00";
            this.txtVAT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtVAT.TextChanged += new System.EventHandler(this.txtVAT_TextChanged);
            this.txtVAT.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVAT_KeyPress);
            // 
            // label35
            // 
            this.label35.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(768, 9);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(140, 37);
            this.label35.TabIndex = 68;
            this.label35.Text = "VAT(%):";
            // 
            // txtPriceUnit
            // 
            this.txtPriceUnit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPriceUnit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtPriceUnit.ForeColor = System.Drawing.SystemColors.Desktop;
            this.txtPriceUnit.Location = new System.Drawing.Point(817, 25);
            this.txtPriceUnit.Name = "txtPriceUnit";
            this.txtPriceUnit.ReadOnly = true;
            this.txtPriceUnit.Size = new System.Drawing.Size(144, 48);
            this.txtPriceUnit.TabIndex = 64;
            this.txtPriceUnit.Text = "0.00";
            this.txtPriceUnit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // chIsPrices
            // 
            this.chIsPrices.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chIsPrices.AutoSize = true;
            this.chIsPrices.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.chIsPrices.ForeColor = System.Drawing.Color.Red;
            this.chIsPrices.Location = new System.Drawing.Point(788, 9);
            this.chIsPrices.Name = "chIsPrices";
            this.chIsPrices.Size = new System.Drawing.Size(310, 44);
            this.chIsPrices.TabIndex = 66;
            this.chIsPrices.Text = "พิมพ์ราคาบน SV";
            this.chIsPrices.UseVisualStyleBackColor = true;
            this.chIsPrices.Click += new System.EventHandler(this.chIsPrices_Click);
            // 
            // label28
            // 
            this.label28.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label28.Location = new System.Drawing.Point(914, 9);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(149, 40);
            this.label28.TabIndex = 65;
            this.label28.Text = "ราคารวม:";
            // 
            // label21
            // 
            this.label21.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label21.Location = new System.Drawing.Point(587, 9);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(343, 40);
            this.label21.TabIndex = 63;
            this.label21.Text = "ราคา/หน่วย(Price list):";
            // 
            // txtPriceList
            // 
            this.txtPriceList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPriceList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtPriceList.ForeColor = System.Drawing.SystemColors.Desktop;
            this.txtPriceList.Location = new System.Drawing.Point(589, 25);
            this.txtPriceList.Name = "txtPriceList";
            this.txtPriceList.ReadOnly = true;
            this.txtPriceList.Size = new System.Drawing.Size(111, 48);
            this.txtPriceList.TabIndex = 3;
            this.txtPriceList.Text = "0.00";
            this.txtPriceList.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPriceList.TextChanged += new System.EventHandler(this.txtPriceList_TextChanged);
            this.txtPriceList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPriceList_KeyDown);
            this.txtPriceList.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPriceList_KeyPress);
            // 
            // label22
            // 
            this.label22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label22.Location = new System.Drawing.Point(725, 9);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(122, 40);
            this.label22.TabIndex = 61;
            this.label22.Text = "ส่วนลด:";
            // 
            // txtDiscQuarter
            // 
            this.txtDiscQuarter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDiscQuarter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtDiscQuarter.ForeColor = System.Drawing.SystemColors.Desktop;
            this.txtDiscQuarter.Location = new System.Drawing.Point(706, 25);
            this.txtDiscQuarter.Name = "txtDiscQuarter";
            this.txtDiscQuarter.ReadOnly = true;
            this.txtDiscQuarter.Size = new System.Drawing.Size(59, 48);
            this.txtDiscQuarter.TabIndex = 60;
            this.txtDiscQuarter.Text = "0.00";
            this.txtDiscQuarter.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtDiscQuarter.TextChanged += new System.EventHandler(this.txtDiscQuarter_TextChanged);
            // 
            // dataGridView_Part
            // 
            this.dataGridView_Part.AllowUserToAddRows = false;
            this.dataGridView_Part.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_Part.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGridView_Part.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_Part.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cCancel});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView_Part.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView_Part.Location = new System.Drawing.Point(9, 132);
            this.dataGridView_Part.Name = "dataGridView_Part";
            this.dataGridView_Part.RowHeadersWidth = 123;
            this.dataGridView_Part.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridView_Part.Size = new System.Drawing.Size(1080, 68);
            this.dataGridView_Part.TabIndex = 27;
            this.dataGridView_Part.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_Part_CellClick);
            this.dataGridView_Part.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_Part_CellEndEdit);
            this.dataGridView_Part.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dataGridView_Part_CellValidating);
            this.dataGridView_Part.CurrentCellDirtyStateChanged += new System.EventHandler(this.dataGridView_Part_CurrentCellDirtyStateChanged);
            this.dataGridView_Part.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dataGridView_Part_RowsAdded);
            // 
            // cCancel
            // 
            this.cCancel.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.cCancel.HeaderText = "";
            this.cCancel.MinimumWidth = 15;
            this.cCancel.Name = "cCancel";
            this.cCancel.Text = "ยกเลิก";
            this.cCancel.ToolTipText = "ยกเลิกข้อมูลอะไหล่";
            this.cCancel.UseColumnTextForButtonValue = true;
            this.cCancel.Width = 15;
            // 
            // dataGridView_tmpPart
            // 
            this.dataGridView_tmpPart.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_tmpPart.Location = new System.Drawing.Point(524, 142);
            this.dataGridView_tmpPart.Name = "dataGridView_tmpPart";
            this.dataGridView_tmpPart.RowHeadersWidth = 123;
            this.dataGridView_tmpPart.Size = new System.Drawing.Size(206, 55);
            this.dataGridView_tmpPart.TabIndex = 28;
            // 
            // btBtPart_select
            // 
            this.btBtPart_select.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btBtPart_select.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btBtPart_select.Location = new System.Drawing.Point(967, 52);
            this.btBtPart_select.Name = "btBtPart_select";
            this.btBtPart_select.Size = new System.Drawing.Size(122, 33);
            this.btBtPart_select.TabIndex = 9;
            this.btBtPart_select.Text = "เลือกอะไหล่";
            this.btBtPart_select.UseVisualStyleBackColor = true;
            this.btBtPart_select.Click += new System.EventHandler(this.btBtPart_select_Click);
            // 
            // txtPartComment
            // 
            this.txtPartComment.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPartComment.Location = new System.Drawing.Point(10, 65);
            this.txtPartComment.Name = "txtPartComment";
            this.txtPartComment.Size = new System.Drawing.Size(499, 44);
            this.txtPartComment.TabIndex = 7;
            this.txtPartComment.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPartComment_KeyDown);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(7, 49);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(145, 37);
            this.label29.TabIndex = 21;
            this.label29.Text = "หมายเหตุ: ";
            // 
            // cob_TrnType
            // 
            this.cob_TrnType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cob_TrnType.Enabled = false;
            this.cob_TrnType.FormattingEnabled = true;
            this.cob_TrnType.Location = new System.Drawing.Point(794, 64);
            this.cob_TrnType.Name = "cob_TrnType";
            this.cob_TrnType.Size = new System.Drawing.Size(167, 45);
            this.cob_TrnType.TabIndex = 8;
            this.cob_TrnType.SelectionChangeCommitted += new System.EventHandler(this.cob_TrnType_SelectionChangeCommitted);
            this.cob_TrnType.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cob_TrnType_KeyPress);
            // 
            // radTranType1
            // 
            this.radTranType1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radTranType1.AutoSize = true;
            this.radTranType1.Location = new System.Drawing.Point(948, 30);
            this.radTranType1.Name = "radTranType1";
            this.radTranType1.Size = new System.Drawing.Size(141, 41);
            this.radTranType1.TabIndex = 6;
            this.radTranType1.TabStop = true;
            this.radTranType1.Text = "เปลี่ยน";
            this.radTranType1.UseVisualStyleBackColor = true;
            // 
            // radTranType0
            // 
            this.radTranType0.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radTranType0.AutoSize = true;
            this.radTranType0.Location = new System.Drawing.Point(928, 30);
            this.radTranType0.Name = "radTranType0";
            this.radTranType0.Size = new System.Drawing.Size(105, 41);
            this.radTranType0.TabIndex = 5;
            this.radTranType0.TabStop = true;
            this.radTranType0.Text = "ค้าง";
            this.radTranType0.UseVisualStyleBackColor = true;
            // 
            // txtPartQT
            // 
            this.txtPartQT.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPartQT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtPartQT.Location = new System.Drawing.Point(541, 25);
            this.txtPartQT.Name = "txtPartQT";
            this.txtPartQT.Size = new System.Drawing.Size(43, 48);
            this.txtPartQT.TabIndex = 2;
            this.txtPartQT.Text = "1";
            this.txtPartQT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPartQT.TextChanged += new System.EventHandler(this.txtPartQT_TextChanged);
            this.txtPartQT.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPartQT_KeyDown);
            this.txtPartQT.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPartQT_KeyPress);
            // 
            // txtPartName
            // 
            this.txtPartName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPartName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtPartName.Location = new System.Drawing.Point(180, 25);
            this.txtPartName.Name = "txtPartName";
            this.txtPartName.Size = new System.Drawing.Size(358, 48);
            this.txtPartName.TabIndex = 1;
            this.txtPartName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPartName_KeyDown);
            // 
            // txtPartNo
            // 
            this.txtPartNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.txtPartNo.Location = new System.Drawing.Point(67, 25);
            this.txtPartNo.Name = "txtPartNo";
            this.txtPartNo.Size = new System.Drawing.Size(107, 48);
            this.txtPartNo.TabIndex = 0;
            this.txtPartNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPartNo_KeyDown);
            // 
            // cob_PoitFail0
            // 
            this.cob_PoitFail0.Enabled = false;
            this.cob_PoitFail0.FormattingEnabled = true;
            this.cob_PoitFail0.Location = new System.Drawing.Point(10, 25);
            this.cob_PoitFail0.Name = "cob_PoitFail0";
            this.cob_PoitFail0.Size = new System.Drawing.Size(52, 45);
            this.cob_PoitFail0.TabIndex = 0;
            this.cob_PoitFail0.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cob_PoitFail0_KeyPress);
            // 
            // label27
            // 
            this.label27.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(547, 10);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(108, 37);
            this.label27.TabIndex = 11;
            this.label27.Text = "จำนวน:";
            // 
            // label26
            // 
            this.label26.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(791, 50);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(163, 37);
            this.label26.TabIndex = 11;
            this.label26.Text = "TranType:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(177, 10);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(151, 37);
            this.label25.TabIndex = 10;
            this.label25.Text = "ชื่ออะไหล่: ";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(64, 10);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(165, 37);
            this.label24.TabIndex = 9;
            this.label24.Text = "รหัสอะไหล่.:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.ForeColor = System.Drawing.Color.Red;
            this.label23.Location = new System.Drawing.Point(7, 10);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(101, 37);
            this.label23.TabIndex = 8;
            this.label23.Text = "จุดเสีย:";
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // lblIsContract
            // 
            this.lblIsContract.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblIsContract.AutoSize = true;
            this.lblIsContract.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblIsContract.ForeColor = System.Drawing.Color.Red;
            this.lblIsContract.Location = new System.Drawing.Point(819, 43);
            this.lblIsContract.Name = "lblIsContract";
            this.lblIsContract.Size = new System.Drawing.Size(265, 46);
            this.lblIsContract.TabIndex = 53;
            this.lblIsContract.Text = "สถานีในสัญญา:";
            this.lblIsContract.Visible = false;
            // 
            // txtContract
            // 
            this.txtContract.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtContract.Location = new System.Drawing.Point(821, 63);
            this.txtContract.Multiline = true;
            this.txtContract.Name = "txtContract";
            this.txtContract.ReadOnly = true;
            this.txtContract.Size = new System.Drawing.Size(338, 131);
            this.txtContract.TabIndex = 54;
            this.txtContract.Visible = false;
            this.txtContract.TextChanged += new System.EventHandler(this.txtContract_TextChanged);
            // 
            // btJobContentMent
            // 
            this.btJobContentMent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btJobContentMent.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btJobContentMent.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btJobContentMent.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btJobContentMent.ForeColor = System.Drawing.Color.Green;
            this.btJobContentMent.Location = new System.Drawing.Point(677, 195);
            this.btJobContentMent.Name = "btJobContentMent";
            this.btJobContentMent.Size = new System.Drawing.Size(291, 23);
            this.btJobContentMent.TabIndex = 55;
            this.btJobContentMent.Text = "บันทึกข้อมูล ความคิดเห็นการให้บริการของลูกค้า";
            this.btJobContentMent.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btJobContentMent.UseVisualStyleBackColor = false;
            this.btJobContentMent.Visible = false;
            this.btJobContentMent.Click += new System.EventHandler(this.btJobContentMent_Click);
            // 
            // btContact_Detail
            // 
            this.btContact_Detail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btContact_Detail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.btContact_Detail.ForeColor = System.Drawing.Color.Red;
            this.btContact_Detail.Location = new System.Drawing.Point(118, 39);
            this.btContact_Detail.Name = "btContact_Detail";
            this.btContact_Detail.Size = new System.Drawing.Size(102, 23);
            this.btContact_Detail.TabIndex = 56;
            this.btContact_Detail.Text = "เงื่อนไขสัญญา";
            this.btContact_Detail.UseVisualStyleBackColor = false;
            this.btContact_Detail.Click += new System.EventHandler(this.btContact_Detail_Click);
            // 
            // btEditContractNo
            // 
            this.btEditContractNo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btEditContractNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btEditContractNo.Location = new System.Drawing.Point(977, 38);
            this.btEditContractNo.Name = "btEditContractNo";
            this.btEditContractNo.Size = new System.Drawing.Size(97, 23);
            this.btEditContractNo.TabIndex = 57;
            this.btEditContractNo.Text = "แก้ไขเลขสัญญา";
            this.btEditContractNo.UseVisualStyleBackColor = false;
            this.btEditContractNo.Visible = false;
            this.btEditContractNo.Click += new System.EventHandler(this.btEditContractNo_Click);
            // 
            // btUpdatePart
            // 
            this.btUpdatePart.BackColor = System.Drawing.Color.Cyan;
            this.btUpdatePart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btUpdatePart.ForeColor = System.Drawing.Color.Blue;
            this.btUpdatePart.Location = new System.Drawing.Point(1080, 38);
            this.btUpdatePart.Name = "btUpdatePart";
            this.btUpdatePart.Size = new System.Drawing.Size(79, 23);
            this.btUpdatePart.TabIndex = 58;
            this.btUpdatePart.Text = "Update Part";
            this.btUpdatePart.UseVisualStyleBackColor = false;
            this.btUpdatePart.Click += new System.EventHandler(this.btUpdatePart_Click);
            // 
            // label_2
            // 
            this.label_2.AutoSize = true;
            this.label_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label_2.ForeColor = System.Drawing.Color.Blue;
            this.label_2.Location = new System.Drawing.Point(971, 20);
            this.label_2.Name = "label_2";
            this.label_2.Size = new System.Drawing.Size(71, 46);
            this.label_2.TabIndex = 59;
            this.label_2.Text = "(2)";
            // 
            // label_3
            // 
            this.label_3.AutoSize = true;
            this.label_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label_3.ForeColor = System.Drawing.Color.Blue;
            this.label_3.Location = new System.Drawing.Point(1074, 20);
            this.label_3.Name = "label_3";
            this.label_3.Size = new System.Drawing.Size(71, 46);
            this.label_3.TabIndex = 60;
            this.label_3.Text = "(3)";
            // 
            // JobClose
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1164, 573);
            this.Controls.Add(this.label_3);
            this.Controls.Add(this.label_2);
            this.Controls.Add(this.btUpdatePart);
            this.Controls.Add(this.btEditContractNo);
            this.Controls.Add(this.btContact_Detail);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btJobContentMent);
            this.Controls.Add(this.lblIsContract);
            this.Controls.Add(this.txtContract);
            this.Controls.Add(this.lblJobInCount);
            this.Controls.Add(this.RadioButtonList_JobStatus1);
            this.Controls.Add(this.RadioButtonList_JobStatus2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.toolStrip_Mnu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "JobClose";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "บันทึกข้อมูลออกงาน/ปิดงาน";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.JobClose_FormClosed);
            this.Load += new System.EventHandler(this.JobClose_Load);
            this.toolStrip_Mnu.ResumeLayout(false);
            this.toolStrip_Mnu.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_FailPoint)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel_selectEndDate.ResumeLayout(false);
            this.panel_selectEndDate.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Part)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_tmpPart)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip_Mnu;
        private System.Windows.Forms.ToolStripComboBox toolStripLabel_cobby;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripTextBox toolStripText_txtFind;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripButton_FindStation;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton toolStripButton_Save;
        private System.Windows.Forms.ToolStripButton toolStripButton_Cancel;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButton_Exit;
        private System.Windows.Forms.ToolStripLabel toolStripLabel_Dateime;
        private System.Windows.Forms.ToolStripButton toolStripButton_Print;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.RadioButton radioButton_D;
        private System.Windows.Forms.RadioButton radioButton_C;
        private System.Windows.Forms.RadioButton radioButton_B;
        private System.Windows.Forms.RadioButton radioButton_A;
        private System.Windows.Forms.ComboBox cobProblemType;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txtResolve_Detail;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btSelectPointFail;
        private System.Windows.Forms.DataGridView dataGridView_FailPoint;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtLiterTest;
        private System.Windows.Forms.TextBox txtEndtLite;
        private System.Windows.Forms.TextBox txtStartLite;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtFailure;
        private System.Windows.Forms.TextBox txtModel;
        private System.Windows.Forms.TextBox txtSNOld;
        private System.Windows.Forms.ComboBox comboBox_PoitFail;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtStation;
        private System.Windows.Forms.TextBox txtUserOpenJob;
        private System.Windows.Forms.TextBox txtInformer;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox FailureDetail;
        private System.Windows.Forms.TextBox txtFialureCode;
        private System.Windows.Forms.TextBox txtPointFaile;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblJobInCount;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbl_JobType;
        private System.Windows.Forms.ComboBox comboBox_Priority;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtOpenJOB;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtJobID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton RadioButtonList_JobStatus2;
        private System.Windows.Forms.RadioButton RadioButtonList_JobStatus1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView dataGridView_Part;
        private System.Windows.Forms.Button btBtPart_select;
        private System.Windows.Forms.TextBox txtPartComment;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox cob_TrnType;
        private System.Windows.Forms.RadioButton radTranType1;
        private System.Windows.Forms.RadioButton radTranType0;
        private System.Windows.Forms.TextBox txtPartQT;
        private System.Windows.Forms.TextBox txtPartName;
        private System.Windows.Forms.TextBox txtPartNo;
        private System.Windows.Forms.ComboBox cob_PoitFail0;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.DataGridViewButtonColumn cCancel;
        private System.Windows.Forms.DataGridViewButtonColumn clCancel;
        private System.Windows.Forms.DataGridViewButtonColumn cAddPart;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.TextBox txtStartDateIn;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtProjectNO;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label lblFailureCode;
        private System.Windows.Forms.DataGridView dataGridView_tmpPart;
        private System.Windows.Forms.Button btClearData;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txtPriceUnit;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtPriceList;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtDiscQuarter;
        private System.Windows.Forms.CheckBox chIsPrices;
        private System.Windows.Forms.TextBox txtVAT;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.ToolStripButton toolStripButton_TAGPrimt;
        private System.Windows.Forms.Panel panel_selectEndDate;
        private System.Windows.Forms.Button btSelectDateOK;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.ComboBox cobEndDate;
        private System.Windows.Forms.ToolStripLabel toolStripLabel_QUARTER;
        private System.Windows.Forms.Label lblIsContract;
        private System.Windows.Forms.TextBox txtContract;
        private System.Windows.Forms.ComboBox con_ERP_StationCharge;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox txtWorkType;
        private System.Windows.Forms.TextBox txtFixAssetNo;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox txtSNNew;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Button btJobContentMent;
        private System.Windows.Forms.TextBox txtFixAsetNoNew;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox txtFixAssetNoOld;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.ToolStripButton btFTRPrint;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.CheckBox cbIsCustomer;
        private System.Windows.Forms.TextBox txtJobSO;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label lblERP_orderline_id;
        private System.Windows.Forms.Label lblWarranty;
        private System.Windows.Forms.Button btLoadSNNew;
        private System.Windows.Forms.Button btContact_Detail;
        private System.Windows.Forms.TextBox txtPartSerialNumberOld;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.ComboBox cob_Failure_Action;
        private System.Windows.Forms.ComboBox cobOilType;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.ComboBox cob_Failure_Action_Solving;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Button pUpSNNew;
        private System.Windows.Forms.Button pUpSNOld;
        private System.Windows.Forms.ComboBox cbxLocation;
        private System.Windows.Forms.ComboBox cob_SolvingByWI;
        private System.Windows.Forms.TextBox txtWorkID;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txtReferPage;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.ComboBox cbxChFCAT;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label lblLastEndDate;
        private System.Windows.Forms.Button btEditContractNo;
        private System.Windows.Forms.Button btUpdatePart;
        private System.Windows.Forms.Button btUpdateWorkType;
        private System.Windows.Forms.Label label_1;
        private System.Windows.Forms.Label label_2;
        private System.Windows.Forms.Label label_3;
    }
}