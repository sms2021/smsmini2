﻿namespace SMSMINI.Transaction
{
    partial class JobUploadPhoto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(JobUploadPhoto));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.txtFailure_Detail = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtJOB_ID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtWorkID = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtStation = new System.Windows.Forms.TextBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.btImg1 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btAddnew = new System.Windows.Forms.Button();
            this.btCancel = new System.Windows.Forms.Button();
            this.btSaveImg = new System.Windows.Forms.Button();
            this.dataGridView_Photo = new System.Windows.Forms.DataGridView();
            this.cCancel = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Item = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.JOB_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StartDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Enddate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ImgBefore = new System.Windows.Forms.DataGridViewImageColumn();
            this.ImgAfter = new System.Windows.Forms.DataGridViewImageColumn();
            this.Equipment = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Decription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Remark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TranDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ImgBeforeRepair = new System.Windows.Forms.DataGridViewImageColumn();
            this.ImgAfterRepair = new System.Windows.Forms.DataGridViewImageColumn();
            this.toolStrip_Mnu = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel_cobby = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripText_txtFind = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_FindStation = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_Save = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_Cancel = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton_Exit = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel_Dateime = new System.Windows.Forms.ToolStripLabel();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Photo)).BeginInit();
            this.toolStrip_Mnu.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.dateTimePicker2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtFailure_Detail);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtJOB_ID);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtWorkID);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtStation);
            this.groupBox1.Controls.Add(this.dateTimePicker1);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.groupBox1.Location = new System.Drawing.Point(210, 66);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(928, 152);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "รายละเอียด JOB";
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            this.dateTimePicker2.Enabled = false;
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker2.Location = new System.Drawing.Point(382, 19);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(162, 23);
            this.dateTimePicker2.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(354, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 17);
            this.label3.TabIndex = 15;
            this.label3.Text = "ออก";
            // 
            // txtFailure_Detail
            // 
            this.txtFailure_Detail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFailure_Detail.Location = new System.Drawing.Point(68, 72);
            this.txtFailure_Detail.Multiline = true;
            this.txtFailure_Detail.Name = "txtFailure_Detail";
            this.txtFailure_Detail.Size = new System.Drawing.Size(853, 45);
            this.txtFailure_Detail.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(20, 100);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 17);
            this.label7.TabIndex = 14;
            this.label7.Text = "อาการ";
            // 
            // txtJOB_ID
            // 
            this.txtJOB_ID.Location = new System.Drawing.Point(68, 20);
            this.txtJOB_ID.Name = "txtJOB_ID";
            this.txtJOB_ID.ReadOnly = true;
            this.txtJOB_ID.Size = new System.Drawing.Size(96, 23);
            this.txtJOB_ID.TabIndex = 0;
            this.txtJOB_ID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtJOB_ID_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "JOB";
            // 
            // txtWorkID
            // 
            this.txtWorkID.Location = new System.Drawing.Point(68, 46);
            this.txtWorkID.Name = "txtWorkID";
            this.txtWorkID.Size = new System.Drawing.Size(266, 23);
            this.txtWorkID.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Work ID";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 129);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 17);
            this.label5.TabIndex = 10;
            this.label5.Text = "ชื่อสถานี";
            // 
            // txtStation
            // 
            this.txtStation.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStation.Location = new System.Drawing.Point(68, 123);
            this.txtStation.Name = "txtStation";
            this.txtStation.Size = new System.Drawing.Size(854, 23);
            this.txtStation.TabIndex = 9;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            this.dateTimePicker1.Enabled = false;
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(194, 19);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(154, 23);
            this.dateTimePicker1.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(170, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(26, 17);
            this.label4.TabIndex = 7;
            this.label4.Text = "เข้า";
            // 
            // btImg1
            // 
            this.btImg1.Location = new System.Drawing.Point(164, 181);
            this.btImg1.Name = "btImg1";
            this.btImg1.Size = new System.Drawing.Size(34, 26);
            this.btImg1.TabIndex = 25;
            this.btImg1.Text = "...";
            this.toolTip1.SetToolTip(this.btImg1, "เลือก รูปภาพ สถานี");
            this.btImg1.UseVisualStyleBackColor = true;
            this.btImg1.Click += new System.EventHandler(this.btImg1_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(9, 18);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(190, 190);
            this.pictureBox1.TabIndex = 24;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btImg1);
            this.groupBox2.Controls.Add(this.pictureBox1);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.groupBox2.Location = new System.Drawing.Point(2, 1);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(207, 217);
            this.groupBox2.TabIndex = 26;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "ภาพสถานี";
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.btAddnew);
            this.groupBox3.Controls.Add(this.btCancel);
            this.groupBox3.Controls.Add(this.btSaveImg);
            this.groupBox3.Controls.Add(this.dataGridView_Photo);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.groupBox3.Location = new System.Drawing.Point(2, 219);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1136, 403);
            this.groupBox3.TabIndex = 27;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "ภาพอะไหล่";
            // 
            // btAddnew
            // 
            this.btAddnew.Location = new System.Drawing.Point(231, 32);
            this.btAddnew.Name = "btAddnew";
            this.btAddnew.Size = new System.Drawing.Size(106, 26);
            this.btAddnew.TabIndex = 41;
            this.btAddnew.Text = "เพิ่มรายการใหม่...";
            this.btAddnew.UseVisualStyleBackColor = true;
            this.btAddnew.Click += new System.EventHandler(this.btAddnew_Click);
            // 
            // btCancel
            // 
            this.btCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btCancel.ForeColor = System.Drawing.Color.Red;
            this.btCancel.Location = new System.Drawing.Point(120, 32);
            this.btCancel.Name = "btCancel";
            this.btCancel.Size = new System.Drawing.Size(92, 26);
            this.btCancel.TabIndex = 40;
            this.btCancel.Text = "ยกเลิกรายการ";
            this.btCancel.UseVisualStyleBackColor = false;
            this.btCancel.Click += new System.EventHandler(this.btCancel_Click);
            // 
            // btSaveImg
            // 
            this.btSaveImg.Enabled = false;
            this.btSaveImg.Location = new System.Drawing.Point(8, 32);
            this.btSaveImg.Name = "btSaveImg";
            this.btSaveImg.Size = new System.Drawing.Size(106, 26);
            this.btSaveImg.TabIndex = 39;
            this.btSaveImg.Text = "บันทึก รูปภาพ...";
            this.btSaveImg.UseVisualStyleBackColor = true;
            this.btSaveImg.Click += new System.EventHandler(this.btSaveImg_Click);
            // 
            // dataGridView_Photo
            // 
            this.dataGridView_Photo.AllowUserToAddRows = false;
            this.dataGridView_Photo.AllowUserToDeleteRows = false;
            this.dataGridView_Photo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_Photo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGridView_Photo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_Photo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cCancel,
            this.Item,
            this.JOB_ID,
            this.StartDate,
            this.Enddate,
            this.ImgBefore,
            this.ImgAfter,
            this.Equipment,
            this.Decription,
            this.Remark,
            this.TranDate,
            this.ImgBeforeRepair,
            this.ImgAfterRepair});
            this.dataGridView_Photo.Location = new System.Drawing.Point(11, 64);
            this.dataGridView_Photo.Name = "dataGridView_Photo";
            this.dataGridView_Photo.RowHeadersWidth = 123;
            this.dataGridView_Photo.Size = new System.Drawing.Size(1118, 331);
            this.dataGridView_Photo.TabIndex = 38;
            this.dataGridView_Photo.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_Photo_CellClick);
            this.dataGridView_Photo.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_Photo_CellDoubleClick);
            this.dataGridView_Photo.CurrentCellDirtyStateChanged += new System.EventHandler(this.dataGridView_Photo_CurrentCellDirtyStateChanged);
            this.dataGridView_Photo.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dataGridView_Photo_DataBindingComplete);
            this.dataGridView_Photo.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dataGridView_Photo_RowsAdded);
            // 
            // cCancel
            // 
            this.cCancel.HeaderText = "";
            this.cCancel.MinimumWidth = 15;
            this.cCancel.Name = "cCancel";
            this.cCancel.Text = "ยกเลิก";
            this.cCancel.ToolTipText = "ยกเลิก";
            this.cCancel.UseColumnTextForButtonValue = true;
            this.cCancel.Visible = false;
            this.cCancel.Width = 15;
            // 
            // Item
            // 
            this.Item.DataPropertyName = "Item";
            this.Item.HeaderText = "ลำดับ";
            this.Item.MinimumWidth = 15;
            this.Item.Name = "Item";
            this.Item.ReadOnly = true;
            this.Item.Width = 62;
            // 
            // JOB_ID
            // 
            this.JOB_ID.DataPropertyName = "JOB_ID";
            this.JOB_ID.HeaderText = "JOB_ID";
            this.JOB_ID.MinimumWidth = 15;
            this.JOB_ID.Name = "JOB_ID";
            this.JOB_ID.ReadOnly = true;
            this.JOB_ID.Visible = false;
            this.JOB_ID.Width = 81;
            // 
            // StartDate
            // 
            this.StartDate.DataPropertyName = "StartDate";
            this.StartDate.HeaderText = "เข้า";
            this.StartDate.MinimumWidth = 15;
            this.StartDate.Name = "StartDate";
            this.StartDate.ReadOnly = true;
            this.StartDate.Visible = false;
            this.StartDate.Width = 51;
            // 
            // Enddate
            // 
            this.Enddate.DataPropertyName = "Enddate";
            this.Enddate.HeaderText = "ออก";
            this.Enddate.MinimumWidth = 15;
            this.Enddate.Name = "Enddate";
            this.Enddate.ReadOnly = true;
            this.Enddate.Visible = false;
            this.Enddate.Width = 55;
            // 
            // ImgBefore
            // 
            this.ImgBefore.DataPropertyName = "ImgBefore";
            this.ImgBefore.HeaderText = "ภาพก่อนซ่อม";
            this.ImgBefore.MinimumWidth = 15;
            this.ImgBefore.Name = "ImgBefore";
            this.ImgBefore.Width = 84;
            // 
            // ImgAfter
            // 
            this.ImgAfter.DataPropertyName = "ImgAfter";
            this.ImgAfter.HeaderText = "ภาพหลังซ่อม";
            this.ImgAfter.MinimumWidth = 15;
            this.ImgAfter.Name = "ImgAfter";
            this.ImgAfter.Width = 82;
            // 
            // Equipment
            // 
            this.Equipment.DataPropertyName = "Equipment";
            this.Equipment.HeaderText = "อุปกรณ์";
            this.Equipment.MinimumWidth = 15;
            this.Equipment.Name = "Equipment";
            this.Equipment.Width = 74;
            // 
            // Decription
            // 
            this.Decription.DataPropertyName = "Decription";
            this.Decription.HeaderText = "รายละเอียด";
            this.Decription.MinimumWidth = 15;
            this.Decription.Name = "Decription";
            this.Decription.Width = 92;
            // 
            // Remark
            // 
            this.Remark.DataPropertyName = "Remark";
            this.Remark.HeaderText = "หมายเหตุ";
            this.Remark.MinimumWidth = 15;
            this.Remark.Name = "Remark";
            this.Remark.Width = 84;
            // 
            // TranDate
            // 
            this.TranDate.DataPropertyName = "TranDate";
            this.TranDate.HeaderText = "TranDate";
            this.TranDate.MinimumWidth = 15;
            this.TranDate.Name = "TranDate";
            this.TranDate.Visible = false;
            this.TranDate.Width = 93;
            // 
            // ImgBeforeRepair
            // 
            this.ImgBeforeRepair.DataPropertyName = "ImgBeforeRepair";
            this.ImgBeforeRepair.HeaderText = "ImgBeforeRepair";
            this.ImgBeforeRepair.MinimumWidth = 15;
            this.ImgBeforeRepair.Name = "ImgBeforeRepair";
            this.ImgBeforeRepair.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ImgBeforeRepair.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ImgBeforeRepair.Visible = false;
            this.ImgBeforeRepair.Width = 139;
            // 
            // ImgAfterRepair
            // 
            this.ImgAfterRepair.DataPropertyName = "ImgAfterRepair";
            this.ImgAfterRepair.HeaderText = "ImgAfterRepair";
            this.ImgAfterRepair.MinimumWidth = 15;
            this.ImgAfterRepair.Name = "ImgAfterRepair";
            this.ImgAfterRepair.Visible = false;
            this.ImgAfterRepair.Width = 108;
            // 
            // toolStrip_Mnu
            // 
            this.toolStrip_Mnu.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip_Mnu.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip_Mnu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel_cobby,
            this.toolStripSeparator4,
            this.toolStripText_txtFind,
            this.toolStripSeparator2,
            this.toolStripButton_FindStation,
            this.toolStripSeparator3,
            this.toolStripButton_Save,
            this.toolStripButton_Cancel,
            this.toolStripSeparator1,
            this.toolStripButton_Exit,
            this.toolStripLabel_Dateime});
            this.toolStrip_Mnu.Location = new System.Drawing.Point(212, 10);
            this.toolStrip_Mnu.Name = "toolStrip_Mnu";
            this.toolStrip_Mnu.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.toolStrip_Mnu.Size = new System.Drawing.Size(491, 39);
            this.toolStrip_Mnu.TabIndex = 28;
            // 
            // toolStripLabel_cobby
            // 
            this.toolStripLabel_cobby.AutoSize = false;
            this.toolStripLabel_cobby.Enabled = false;
            this.toolStripLabel_cobby.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.toolStripLabel_cobby.ForeColor = System.Drawing.SystemColors.Desktop;
            this.toolStripLabel_cobby.Items.AddRange(new object[] {
            "ตาม JOB ID",
            "ตาม วันที่(yyyymmdd)",
            "ตาม สถานี",
            "ตาม VAN",
            "ทั้งหมด"});
            this.toolStripLabel_cobby.Name = "toolStripLabel_cobby";
            this.toolStripLabel_cobby.Size = new System.Drawing.Size(100, 21);
            this.toolStripLabel_cobby.Text = "ค้นหาข้อมูล...";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripText_txtFind
            // 
            this.toolStripText_txtFind.AutoSize = false;
            this.toolStripText_txtFind.Enabled = false;
            this.toolStripText_txtFind.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.toolStripText_txtFind.ForeColor = System.Drawing.SystemColors.Desktop;
            this.toolStripText_txtFind.Name = "toolStripText_txtFind";
            this.toolStripText_txtFind.Size = new System.Drawing.Size(100, 20);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripButton_FindStation
            // 
            this.toolStripButton_FindStation.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_FindStation.Enabled = false;
            this.toolStripButton_FindStation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.toolStripButton_FindStation.Image = global::SMSMINI.Properties.Resources.Find48;
            this.toolStripButton_FindStation.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_FindStation.Name = "toolStripButton_FindStation";
            this.toolStripButton_FindStation.Size = new System.Drawing.Size(36, 36);
            this.toolStripButton_FindStation.ToolTipText = "ค้นหาข้อมูล";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripButton_Save
            // 
            this.toolStripButton_Save.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.toolStripButton_Save.ForeColor = System.Drawing.SystemColors.Desktop;
            this.toolStripButton_Save.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Save.Image")));
            this.toolStripButton_Save.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Save.Name = "toolStripButton_Save";
            this.toolStripButton_Save.Size = new System.Drawing.Size(77, 36);
            this.toolStripButton_Save.Text = "Upload";
            this.toolStripButton_Save.ToolTipText = "Upload JOB";
            this.toolStripButton_Save.Click += new System.EventHandler(this.toolStripButton_Save_Click);
            // 
            // toolStripButton_Cancel
            // 
            this.toolStripButton_Cancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.toolStripButton_Cancel.ForeColor = System.Drawing.SystemColors.Desktop;
            this.toolStripButton_Cancel.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Cancel.Image")));
            this.toolStripButton_Cancel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Cancel.Name = "toolStripButton_Cancel";
            this.toolStripButton_Cancel.Size = new System.Drawing.Size(76, 36);
            this.toolStripButton_Cancel.Text = "Cancel";
            this.toolStripButton_Cancel.ToolTipText = "ยกเลิกข้อมูล";
            this.toolStripButton_Cancel.Click += new System.EventHandler(this.toolStripButton_Cancel_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripButton_Exit
            // 
            this.toolStripButton_Exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.toolStripButton_Exit.ForeColor = System.Drawing.SystemColors.Desktop;
            this.toolStripButton_Exit.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Exit.Image")));
            this.toolStripButton_Exit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Exit.Name = "toolStripButton_Exit";
            this.toolStripButton_Exit.Size = new System.Drawing.Size(60, 36);
            this.toolStripButton_Exit.Text = "Exit";
            this.toolStripButton_Exit.ToolTipText = "ออกจากหน้าจอ";
            this.toolStripButton_Exit.Click += new System.EventHandler(this.toolStripButton_Exit_Click);
            // 
            // toolStripLabel_Dateime
            // 
            this.toolStripLabel_Dateime.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripLabel_Dateime.Font = new System.Drawing.Font("Tahoma", 10F);
            this.toolStripLabel_Dateime.ForeColor = System.Drawing.SystemColors.Desktop;
            this.toolStripLabel_Dateime.Name = "toolStripLabel_Dateime";
            this.toolStripLabel_Dateime.Size = new System.Drawing.Size(0, 36);
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // JobUploadPhoto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(774, 436);
            this.Controls.Add(this.toolStrip_Mnu);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "JobUploadPhoto";
            this.Text = "UPLOAD JOB Photo Report";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.JobUploadPhoto_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Photo)).EndInit();
            this.toolStrip_Mnu.ResumeLayout(false);
            this.toolStrip_Mnu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtFailure_Detail;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtJOB_ID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtWorkID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtStation;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btImg1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView dataGridView_Photo;
        private System.Windows.Forms.ToolStrip toolStrip_Mnu;
        private System.Windows.Forms.ToolStripComboBox toolStripLabel_cobby;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripTextBox toolStripText_txtFind;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripButton_FindStation;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton toolStripButton_Save;
        private System.Windows.Forms.ToolStripButton toolStripButton_Cancel;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButton_Exit;
        private System.Windows.Forms.ToolStripLabel toolStripLabel_Dateime;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Label label3;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button btSaveImg;
        private System.Windows.Forms.Button btCancel;
        private System.Windows.Forms.Button btAddnew;
        private System.Windows.Forms.DataGridViewButtonColumn cCancel;
        private System.Windows.Forms.DataGridViewTextBoxColumn Item;
        private System.Windows.Forms.DataGridViewTextBoxColumn JOB_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn StartDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Enddate;
        private System.Windows.Forms.DataGridViewImageColumn ImgBefore;
        private System.Windows.Forms.DataGridViewImageColumn ImgAfter;
        private System.Windows.Forms.DataGridViewTextBoxColumn Equipment;
        private System.Windows.Forms.DataGridViewTextBoxColumn Decription;
        private System.Windows.Forms.DataGridViewTextBoxColumn Remark;
        private System.Windows.Forms.DataGridViewTextBoxColumn TranDate;
        private System.Windows.Forms.DataGridViewImageColumn ImgBeforeRepair;
        private System.Windows.Forms.DataGridViewImageColumn ImgAfterRepair;
    }
}