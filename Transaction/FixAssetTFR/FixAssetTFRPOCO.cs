﻿using System;

namespace SMSMINI.Transaction.FixAssetTFR
{
    public class FixAssetTFRPOCO
    {
        public string JOB_ID { get; set; }
        public string FLO { get; set; }
        public string Station { get; set; }
        public string Asset_ID { get; set; }
        public string Asset_Description { get; set; }
        public string SerialNo { get; set; }
        public decimal Quantity { get; set; }
        public double Original_Cost { get; set; }
        public string Condition { get; set; }
        public string Remarks { get; set; }

        public DateTime CloseJOB { get; set; }
        public string VAN { get; set; }

    }
}
