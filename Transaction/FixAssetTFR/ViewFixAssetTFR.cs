﻿using CrystalDecisions.Shared;
using System;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace SMSMINI.Transaction.FixAssetTFR
{
    public partial class ViewFixAssetTFR : Form
    {
        public string TMPJobID { get; set; }
        PrintFixAssetTFR rpt = null;
        string smsversin = "";
        public ViewFixAssetTFR()
        {
            InitializeComponent();
        }

        private void ViewFixAssetTFR_Load(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.AppStarting;
            object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
            if (attributes.Length == 0)
                smsversin = "SMS MINI v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            else
                smsversin = ((AssemblyProductAttribute)attributes[0]).Product + " v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();

            rpt = new PrintFixAssetTFR();
            rpt.SetDatabaseLogon("sa", "enabler", ".\\SQLEXPRESS", "SMSminiPPLDB");
            ShowReport("VIEW");

            Cursor.Current = Cursors.Default;
        }

        private void ShowReport(string mode)
        {

            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            {
                switch (mode)
                {
                    case "OLD":
                        var qOld = from t in dc.v_ReportPrint_FixAssetTransferRequests
                                   where t.JOB_ID == TMPJobID
                                   select new FixAssetTFRPOCO
                                   {
                                       FLO = t.STA_ID1 + " " + t.StationSys,
                                       Station = "Flowco LTD.",
                                       JOB_ID = t.JOB_ID,
                                       Asset_ID = t.FixAssetNo,
                                       Asset_Description = t.SparePart,
                                       SerialNo = t.SerialNumber,
                                       Quantity = t.Quantity.Value,
                                       Original_Cost = 0,
                                       Condition = "ชำรุด",
                                       Remarks = "",
                                       VAN = t.VAN,
                                       CloseJOB = t.CloseDate.Value
                                   };
                        if (qOld.Count() > 0)
                            reportBinding(mode, qOld);
                        else
                            noData();
                        break;

                    case "NEW":
                        var qNew = from t in dc.v_ReportPrint_FixAssetTransferRequests
                                   where t.JOB_ID == TMPJobID
                                   select new FixAssetTFRPOCO
                                   {
                                       FLO = "Flowco LTD.",
                                       Station = t.STA_ID1 + " " + t.StationSys,
                                       JOB_ID = t.JOB_ID,
                                       Asset_ID = t.FixAssetNo_New,
                                       Asset_Description = t.SparePart,
                                       SerialNo = t.SerialNumber_New,
                                       Quantity = t.Quantity.Value,
                                       Original_Cost = 0,
                                       Condition = "OK",
                                       Remarks = "",
                                       VAN = t.VAN,
                                       CloseJOB = t.CloseDate.Value
                                   };

                        if (qNew.Count() > 0)
                            reportBinding(mode, qNew);
                        else
                            noData();

                        break;

                    default:
                        var q = from t in dc.v_ReportPrint_FixAssetTransferRequests
                                where t.JOB_ID == TMPJobID
                                select new FixAssetTFRPOCO
                                {
                                    FLO = "Flowco LTD.",
                                    Station = t.STA_ID1 + " " + t.StationSys,
                                    JOB_ID = t.JOB_ID,
                                    Asset_ID = t.FixAssetNo,
                                    Asset_Description = t.SparePart,
                                    SerialNo = t.SerialNumber,
                                    Quantity = t.Quantity.Value,
                                    Original_Cost = 0,
                                    Condition = "ชำรุด",
                                    Remarks = "",
                                    VAN = t.VAN,
                                    CloseJOB = t.CloseDate.Value
                                };

                        if (q.Count() > 0)
                        {
                            rpt.SetDataSource(q.ToList());
                            rpt.SetParameterValue("smsVersion", smsversin);
                            crystalReportViewer1.ReportSource = rpt;
                        }
                        else
                            noData();
                        break;
                }

            }
        }

        private void noData()
        {
            MessageBox.Show("ไม่มีข้อมูล Fixed Asset Transfer Requests", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            this.Close();
        }

        private void reportBinding(string mode, IQueryable<FixAssetTFRPOCO> qOld)
        {
            rpt.SetDataSource(qOld.ToList());
            rpt.SetParameterValue("smsVersion", smsversin);
            Export2PDF(rpt, mode);
        }

        private void btPrint_Click(object sender, EventArgs e)
        {
            ShowReport("OLD");
            ShowReport("NEW");
        }

        private void Export2PDF(PrintFixAssetTFR rpt, string mode)
        {
            {
                string f = "";
                SaveFileDialog DialogSave = new SaveFileDialog();
                DialogSave.DefaultExt = "xls";
                DialogSave.Filter = "Adobe Acrobat  (*.pdf)|*.pdf|All files (*.*)|*.*";
                DialogSave.AddExtension = true;
                DialogSave.RestoreDirectory = true;
                DialogSave.Title = "Where do you want to save the file?";
                DialogSave.InitialDirectory = @"C:/";


                string sv = "FixedAssetTFR_" + TMPJobID + "_(" + mode + ")_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".pdf";

                DialogSave.FileName = sv;

                if (DialogSave.ShowDialog() == DialogResult.OK)
                {
                    f = DialogSave.FileName.ToString();
                    DialogSave.Dispose();
                    DialogSave = null;

                    try
                    {
                        ExportOptions CrExportOptions;
                        DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                        PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();
                        CrDiskFileDestinationOptions.DiskFileName = f;


                        CrExportOptions = rpt.ExportOptions;
                        {
                            CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                            CrExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                            CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
                            CrExportOptions.FormatOptions = CrFormatTypeOptions;

                        }
                        rpt.Export();
                        rpt.Close();

                        if (System.IO.File.Exists(f))
                        {
                            System.Diagnostics.Process.Start(f);
                        }

                        //this.Close();
                    }

                    catch (Exception ex)
                    {
                        MessageBox.Show("ข้อผิดพลาด " + ex.ToString(), "เกิดข้อผิดพลาดในการ Export ไฟล์ SV PDF");
                    }
                }// if (DialogSave.ShowDialog() == DialogResult.OK)

            }// if (mode == "")
        }

        private void ViewFixAssetTFR_FormClosed(object sender, FormClosedEventArgs e)
        {
            DialogResult = DialogResult.Abort;
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
