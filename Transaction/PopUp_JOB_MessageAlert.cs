﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace SMSMINI.Transaction
{
    public partial class PopUp_JOB_MessageAlert : Form
    {
        List<jobMAlert> lsjobMAlert = new List<jobMAlert>();
        List<jobMAlertDetail> lsjobMAlertD = new List<jobMAlertDetail>();


        public PopUp_JOB_MessageAlert()
        {
            InitializeComponent();
        }


        private void PopUp_JOB_MessageAlert_Load(object sender, EventArgs e)
        {
            using (var dc = new DAL.SMSManage.SMSManageDataContext())
            {
                #region MessageAlert By VAN
                var mAlert = dc.vw_JOB_MessageAlert_by_VANs
                    .Where(t => t.VAN == UserInfo.Van_ID)
                    .Select(t => new
                    {
                        t.VAN,
                        t.TranDate,
                        t.MessageAlert,
                        Admin = t.FName + " " + (t.LName ?? "")

                    }).Distinct()
                    .ToList();
                if (mAlert.Count() > 0)
                {
                    //MessageAlert By VAN
                    foreach (var item in mAlert)
                    {
                        lsjobMAlert.Add(new jobMAlert
                        {
                            วันที่แจ้ง = item.TranDate,
                            แจ้งเตือน = item.MessageAlert,
                            ผู้แจ้ง = item.Admin,
                            id = item.VAN

                        });
                    }
                }

                #endregion

                #region MessageAlert Detail By VAN

                //var mAlertD = dc.JOB_MessageAlert_Details // source
                //    .Where(t => t.VAN_STA == UserInfo.Van_ID)
                //   .Join(dc.vw_JOB_MessageAlert_by_VANs, // target
                //    s => s.VAN_STA,// FK
                //    t => t.VAN,// PK
                //    (s, t) => new // select result
                //    {
                //        s.NO,
                //        JOB_No = s.JOB_ID,
                //        s.Remark,
                //        id=t.VAN ,
                //        date = t.TranDate
                //    }).Distinct ();

                var mAlertD = from t in dc.JOB_MessageAlert_Details
                              from v in dc.vw_JOB_MessageAlert_by_VANs
                              from s in dc.Stations
                              from j in dc.JOBs
                              where t.VAN_STA == UserInfo.Van_ID &&
                                 t.VAN_STA == v.VAN &&
                                 t.JOB_ID == j.JOB_ID &&
                                 j.STA_ID == s.STA_ID
                              select new
                              {
                                  t.NO,
                                  JOB_No = t.JOB_ID,
                                  t.Remark,
                                  Station = "ID:=" + s.STA_ID + " ชื่อ:=" + s.StationSys + " ที่อยู่:=" + s.Address,
                                  id = v.VAN,
                                  date = t.TranDate
                              };


                if (mAlertD.Count() > 0)
                {
                    //MessageAlert Detail By VAN
                    foreach (var item in mAlertD)
                    {
                        lsjobMAlertD.Add(new jobMAlertDetail
                        {
                            NO = item.NO,
                            JOB_No = item.JOB_No,
                            หมายเหตุ = item.Remark,
                            Station = item.Station,
                            id = item.id,
                            date = item.date
                        });
                    }

                    //dataGridView2.DataSource = lsjobMAlertD;
                }
                #endregion


                #region MessageAlert By SAT
                var mAlert1 = dc.vw_JOB_MessageAlert_by_SATs
                    .Where(t => t.VAN == UserInfo.Van_ID)
                    .Select(t => new
                    {
                        t.TranDate,
                        t.MessageAlert,
                        Admin = t.FName + " " + (t.LName ?? ""),
                        id = t.SAT

                    }).Distinct()
                    .ToList();
                if (mAlert1.Count() > 0)
                {
                    //MessageAlert By VAN
                    foreach (var item in mAlert1)
                    {
                        lsjobMAlert.Add(new jobMAlert
                        {
                            วันที่แจ้ง = item.TranDate,
                            แจ้งเตือน = item.MessageAlert,
                            ผู้แจ้ง = item.Admin,
                            id = item.id

                        });
                    }

                    #region MessageAlert Detail By SAT

                    var mAlertD1 = (from t in dc.JOB_MessageAlert_Details
                                    from s in dc.Stations
                                    from d in dc.vw_JOB_MessageAlert_by_SATs
                                    where t.VAN_STA == d.SAT &&
                                    d.VAN == UserInfo.Van_ID &&
                                    t.VAN_STA == s.STA_ID
                                    select new
                                    {
                                        t.NO,
                                        JOB_No = t.JOB_ID,
                                        t.Remark,
                                        Station = "ID:=" + s.STA_ID + " ชื่อ:=" + s.StationSys + " ที่อยู่:=" + s.Address,
                                        t.VAN_STA,
                                        t.TranDate

                                    }).Distinct();


                    if (mAlertD1.Count() > 0)
                    {
                        //MessageAlert Detail By SAT
                        foreach (var item in mAlertD1)
                        {
                            lsjobMAlertD.Add(new jobMAlertDetail
                            {
                                NO = item.NO,
                                JOB_No = item.JOB_No,
                                หมายเหตุ = item.Remark,
                                Station = item.Station,
                                id = item.VAN_STA,
                                date = item.TranDate
                            });
                        }


                    }
                    #endregion

                }

                #endregion

                dataGridView1.DataSource = lsjobMAlert;
                dataGridView2.DataSource = lsjobMAlertD;

            }

        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                // value = DataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
                var id = dataGridView1.Rows[e.RowIndex].Cells["id"].Value.ToString();
                var date = DateTime.Parse(dataGridView1.Rows[e.RowIndex].Cells["วันที่แจ้ง"].Value.ToString());

                var q = lsjobMAlertD
                    .Where(t => t.id == id && t.date == date)
                    .ToList();

                dataGridView2.DataSource = q;
            }
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }


    }

    public class jobMAlert
    {
        public DateTime วันที่แจ้ง { get; set; }
        public string แจ้งเตือน { get; set; }
        public string ผู้แจ้ง { get; set; }
        public string id { get; set; }

    }

    public class jobMAlertDetail
    {
        public int NO { get; set; }
        public string JOB_No { get; set; }
        public string หมายเหตุ { get; set; }
        public string Station { get; set; }
        public string id { get; set; }
        public DateTime date { get; set; }
    }
}
