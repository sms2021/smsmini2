﻿#pragma warning disable CS0246 // The type or namespace name 'Janawat' could not be found (are you missing a using directive or an assembly reference?)
using Janawat.Application.Data;
#pragma warning restore CS0246 // The type or namespace name 'Janawat' could not be found (are you missing a using directive or an assembly reference?)
using Microsoft.VisualBasic;
using SMSMINI.DAL.SMSManage;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
//using SMSMINI.DAL.SMSManage;
using System.Reflection;
using System.Windows.Forms;


namespace SMSMINI.Transaction
{
    public partial class JOBOnline : Form
    {
        private ProgressBar.FormProgress m_fmProgress = null;

        DataTable dtFailure = null;
        DataTable dtPart = null;
        string tmpjobID = "";
        string editMode = "";


        string tmpRemarkJobContentMent = "";//ความพึงพอใจลูกค้า = (C) ปรับปรุง

        int PartCount = 0;

        string tmpLocation = "";
        string tmpSN = "";
        string tmpFAIID = "";


        DateTime _dateServer = DateTime.Now;//Get datetime from Server
        DateTime _Jobopendate = DateTime.Now;//Get date open job

        string tmpCalPoitFailID = "";
        string tmpCalSN = "";
        string tmpCalFAI_ID = "";

        string tmpCalSPA_ID = "";
        int tmpPA_ID_NO = 0;

        List<string> chkCancelSPA_ID;
        List<string> logCancelSPA_ID;

        List<SNCancel> lsSNCancel = null;
        List<SNCancel> lsSNSelect = null;

        string tmpJobType = "";
        string tmpTYP_ID1 = "";

        string tmpGOPId = "";
        string tmpSTAID = "";

        string tmpGopName = "";
        int tmpRounup = 0;

        string tmpEndDate = "";
        string tmpEndTime = "";

        string tmpStatusPart = "";
        string tmpStatusPartID = "";

        string strConn = "";
        int FCAT_ID = 0;
        int FBA_ID = 0;
        int FMD_ID = 0;

        int LOC_ID = 0;

        bool IsQT = true;

        string tmpSPA_ID = string.Empty;

        bool isLockTranType = false;
        string _contractNo = string.Empty;

        double _vat = 0;

        decimal _PricesList = 0;
        decimal _Prices = 0;
        decimal _Discount = 0;
        decimal _SumPrices = 0;
        decimal _SumTotalPrices = 0;
        decimal _SumVAT = 0;
        decimal _Quantity = 0;

        DAL.SMSManage.JOB _job = null;

        List<DAL.SMSManage.JOB> lsJOB = null;

        bool _IsPrintPrice = false;
        bool _IsStationCharge = false;

        //20141224
        bool IsFound;//= true;

        //server date for RecDate_Endate
        DateTime Server_date = DateTime.Now;

        public JOBOnline()
        {

            InitializeComponent();

            string _mode = (UserInfo.ConnectMode == "0") ? " [Off line]" : " [On line]";
            object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
            if (attributes.Length == 0)
            {
                this.Text = "SMS MINI [v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + _mode + "แก้ไขข้อมูล JOB ย้อนหลัง" + "] ";

            }
            this.Text = ((AssemblyProductAttribute)attributes[0]).Product + " [v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + _mode + "แก้ไขข้อมูล JOB ย้อนหลัง" + "] ";

        }

        private void JOBOnline_Load(object sender, EventArgs e)
        {

            try
            {
                lblJobInCount.Text = "";

                chkCancelSPA_ID = new List<string>();
                lsSNCancel = new List<SNCancel>();
                lsSNSelect = new List<SNCancel>();

                editMode = "";

                disableControl(true);
                ClearControl();

                setComboBox();

                setDtFailure();
                setDtPart();

                FailureBinding();
                PartBindind();

                ClearControl();

                ClearTextFail();
                ClearPartText();

                dataGridView_tmpPart.DataSource = null;
                dataGridView_tmpPart.Refresh();

                dataGridView_FailPoint.Enabled = false;

                butttonEnable(false, false, false, false, true);

                //--====
                //20170601 แก้ไขย้อนหลัง ไม่อนุญาตให้ print chkEnableSVReport(txtJobID.Text.Trim());
                toolStripButton_Print.Enabled = false;
                //--====

                toolStripComboBox1.SelectedIndex = 0;

                RadioButtonList_JobStatus1.Checked = false;
                RadioButtonList_JobStatus2.Checked = false;

                FailureBinding();
                PartBindind();

                toolStripLabel_cobby.Text = "ค้นหาข้อมูล...";
                toolStripLabel_cobby.Items.Clear();

                //if (UserInfo.ConnectMode == "0")//Offline
                //{
                //    toolStripLabel_cobby.Items.Add("ตาม JOB ID");
                //    toolStripLabel_cobby.Items.Add("ตาม วันที่(yyyymmdd)");
                //    toolStripLabel_cobby.Items.Add("ตาม สถานี");
                //    toolStripLabel_cobby.Items.Add("ทั้งหมด");

                //}
                //else//Online
                //{

                toolStripLabel_cobby.Items.Add("ตาม JOB ID");
                //201511006 บังคับกรอกแค่ job toolStripLabel_cobby.Items.Add("ตาม วันที่(yyyymmdd)");
                //201511006 บังคับกรอกแค่ job toolStripLabel_cobby.Items.Add("ตาม สถานี");
                toolStripText_txtFind.Focus();

                toolStripLabel_cobby.SelectedIndex = 0;

                //201511006 บังคับกรอกแค่ แก้ไข 
                toolStripComboBox1.Enabled = false;
                toolStripComboBox1.SelectedIndex = 1;
                editMode = "job_edit";
                //====
                //}


                tabControl1.SelectedIndex = 0;
                groupBox3.Enabled = false;

                panel_selectEndDate.Visible = false;

                _IsStationCharge = false;




            }
            catch (System.Exception ex)
            {
                MessageBox.Show("ข้อผิดพลาด " + ex.Message, "เกิดข้อผิดพลาด");
            }


            txtSNOld.Text = (txtSNOld.Text == "" ? "None" : txtSNOld.Text);
            txtSNNew.Text = (txtSNNew.Text == "" ? "None" : txtSNNew.Text);

            //if (UserInfo.ConnectMode == "1")
            //    toolStripBtSVAdmin.Visible = true;
            //else
            //    toolStripBtSVAdmin.Visible = false;

            if (UserInfo.ConnectMode == "1" && new string[] { "06", "07", "08", "09", "10", "11", "12", "13", "99" }.Contains(UserInfo.UserLevID.ToString()))
            {
                toolStripButton_Save.Enabled = true;
                toolStripButton_Cancel.Enabled = true;
                //201511006 บังคับกรอกแค่ แก้ไข  toolStripComboBox1.Enabled = true;

            }
            else if (UserInfo.ConnectMode == "1")
            {
                toolStripButton_Save.Enabled = false;
                toolStripButton_Cancel.Enabled = false;
                toolStripComboBox1.Enabled = false;


            }

            lblWarranty.Visible = false;


        }
        private void disableControl(bool isDisable)
        {

            groupBox1.Enabled = !isDisable;
            btContact_Detail.Enabled = !isDisable;

            groupBox2.Enabled = !isDisable;
            groupBox3.Enabled = !isDisable;
            groupBox4.Enabled = !isDisable;


            cobProblemType.Enabled = !isDisable;
            radioButton_A.Enabled = !isDisable;
            radioButton_B.Enabled = !isDisable;
            radioButton_C.Enabled = !isDisable;
            radioButton_D.Enabled = !isDisable;

            label33.Enabled = !isDisable;
            label32.Enabled = !isDisable;
        }

        private void ClearControl()
        {
            ClearControl(groupBox1.Controls);
            ClearControl(groupBox2.Controls);
            ClearControl(groupBox3.Controls);
            ClearControl(groupBox4.Controls);
        }

        private void ClearControl(Control.ControlCollection objContrl)
        {
            IEnumerator eContrl = null;
            try
            {
                eContrl = objContrl.GetEnumerator();
                while (eContrl.MoveNext())
                {
                    Control cont = (Control)eContrl.Current;

                    if (cont.HasChildren)
                        this.ClearControl(cont.Controls);
                    else
                    {
                        if (cont is TextBox) ((TextBox)cont).Text = "";
                        if (cont is CheckBox) ((CheckBox)cont).Checked = false;
                        if (cont is RadioButton) ((RadioButton)cont).Checked = false;
                        if (cont is ComboBox) ((ComboBox)cont).SelectedIndex = -1;
                        if (cont is ListBox) ((ListBox)cont).Items.Clear();
                        if (cont is NumericUpDown) ((NumericUpDown)cont).Value = ((NumericUpDown)cont).Minimum;
                    }
                }
            }
            finally
            {
                if (eContrl is IDisposable)
                {
                    ((IDisposable)eContrl).Dispose();
                }
            }
        }

        private void setComboBox()
        {
            //if (UserInfo.ConnectMode == "0")
            //    strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
            //else
            strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;

            try
            {
                string _typeID = UserInfo.Van_Type;
                string sql = "";

                //client 20170602  sql = " SELECT POI_ID, PointFailure FROM [PointFailure] WHERE IsCancel = 'Y' Order by OrderNo;  " +

                //client 20170602     " SELECT POI_ID as 'POI_ID1', PointFailure as 'PointFailure1'  FROM [PointFailure]  WHERE IsCancel = 'Y'  Order by OrderNo;  " +
                sql = " SELECT POI_ID, PointFailure FROM [PointFailure]  Order by OrderNo;  " +
                        " SELECT POI_ID as 'POI_ID1', PointFailure as 'PointFailure1'  FROM [PointFailure]    Order by OrderNo;  " +
                        " SELECT PMT_ID, '['+PMT_ID+'] '+Problem as Detail  FROM JOB_ProblemType where (IsCancel is null or IsCancel = 0)   ORDER BY Detail ;" +
                        " SELECT TRA_ID, '[' + TRA_ID + '] ' +  TransferName as 'Detail' FROM SparePart_TranType where TRA_ID<>'00' AND IsCancel = '0' ORDER BY TRA_ID; " +
                        " SELECT PRI_ID, PriorityDetail from Priority; " +

                        " SELECT [PRT_ID],Convert(varchar,[PRT_ID] ) +': '+[Product_Type] as  [Product_Type] FROM  [Product_Type]; " +
                        //client 20170602 " SELECT [LOC_ID], Convert(varchar,LOC_ID)+ ': ' + [Location]  as [Location] FROM  [Location] Where (( [LocType] ='D') or ([LOC_ID] = 0) ) Order By [LOC_ID]; " +
                        " SELECT [LOC_ID], Convert(varchar,LOC_ID)+ ': ' + [Location]  as [Location] FROM  [Location] Order By [LOC_ID]; " +
                        " SELECT [FAS_ID] ,Convert(varchar,[FAS_ID])+': '+[Failure_Action_Solving] as [Failure_Action_Solving] FROM  [Failure_Action_Solving]; " +
                        //client 20170602 " SELECT [FSWI_ID] , [WICode]+': '+[WIDetail] as [WICode] FROM [Failure_Solving_WI] WHERE (JOBType in ('30','31')) ORDER BY [FSWI_ID]; "; //20141224
                        " SELECT [FSWI_ID] , [WICode]+': '+[WIDetail] as [WICode] FROM [Failure_Solving_WI] ORDER BY [FSWI_ID]; "; //20141224

                SqlDataReader dr = JaSqlHelper.ExecuteReader(strConn, CommandType.Text, sql);
                DataTable dt;

                if (dr.HasRows)
                {
                    //comboBox_PoitFail
                    dt = new DataTable();
                    dt.Load(dr);
                    comboBox_PoitFail.BeginUpdate();
                    comboBox_PoitFail.ValueMember = "POI_ID";
                    comboBox_PoitFail.DisplayMember = "PointFailure";
                    comboBox_PoitFail.DataSource = dt;
                    comboBox_PoitFail.EndUpdate();
                    comboBox_PoitFail.SelectedIndex = -1;

                    //cob_PoitFail0
                    dt = new DataTable();
                    dt.Load(dr);
                    cob_PoitFail0.BeginUpdate();
                    cob_PoitFail0.ValueMember = "POI_ID1";
                    cob_PoitFail0.DisplayMember = "PointFailure1";
                    cob_PoitFail0.DataSource = dt;
                    cob_PoitFail0.EndUpdate();
                    cob_PoitFail0.SelectedIndex = -1;


                    //comProbType
                    dt = new DataTable();
                    dt.Load(dr);
                    cobProblemType.BeginUpdate();
                    cobProblemType.ValueMember = "PMT_ID";
                    cobProblemType.DisplayMember = "Detail";
                    cobProblemType.DataSource = dt;
                    cobProblemType.EndUpdate();
                    cobProblemType.SelectedIndex = -1;

                    //comboBox_SpareTranType
                    dt = new DataTable();
                    dt.Load(dr);
                    cob_TrnType.BeginUpdate();
                    cob_TrnType.ValueMember = "TRA_ID";
                    cob_TrnType.DisplayMember = "Detail";
                    cob_TrnType.DataSource = dt;
                    cob_TrnType.EndUpdate();
                    cob_TrnType.SelectedValue = "IN";

                    //comboBox_Priority
                    dt = new DataTable();
                    dt.Load(dr);
                    comboBox_Priority.BeginUpdate();
                    comboBox_Priority.ValueMember = "PRI_ID";
                    comboBox_Priority.DisplayMember = "PriorityDetail";
                    comboBox_Priority.DataSource = dt;
                    comboBox_Priority.EndUpdate();
                    comboBox_Priority.SelectedIndex = -1;


                    /* //20141127ยกเลิก
                    //cob_Equipment_Position
                    dt = new DataTable();
                    dt.Load(dr);
                    cob_Equipment_Position.BeginUpdate();
                    cob_Equipment_Position.ValueMember = "EQP_ID";
                    cob_Equipment_Position.DisplayMember = "Equipment_Position";
                    cob_Equipment_Position.DataSource = dt;
                    cob_Equipment_Position.EndUpdate();
                    cob_Equipment_Position.SelectedIndex = -1;
                    */

                    //cobOilType
                    dt = new DataTable();
                    dt.Load(dr);
                    cobOilType.BeginUpdate();
                    cobOilType.ValueMember = "PRT_ID";
                    cobOilType.DisplayMember = "Product_Type";
                    cobOilType.DataSource = dt;
                    cobOilType.EndUpdate();
                    cobOilType.SelectedIndex = -1;


                    //cbxLocation
                    dt = new DataTable();
                    dt.Load(dr);
                    cbxLocation.BeginUpdate();
                    cbxLocation.ValueMember = "LOC_ID";
                    cbxLocation.DisplayMember = "Location";
                    cbxLocation.DataSource = dt;
                    cbxLocation.EndUpdate();
                    cbxLocation.SelectedIndex = -1;

                    //cob_Failure_Action_Solving
                    dt = new DataTable();
                    dt.Load(dr);
                    cob_Failure_Action_Solving.BeginUpdate();
                    cob_Failure_Action_Solving.ValueMember = "FAS_ID";
                    cob_Failure_Action_Solving.DisplayMember = "Failure_Action_Solving";
                    cob_Failure_Action_Solving.DataSource = dt;
                    cob_Failure_Action_Solving.EndUpdate();
                    cob_Failure_Action_Solving.SelectedIndex = -1;

                    //cob_SolvingByWI
                    dt = new DataTable();
                    dt.Load(dr);
                    cob_SolvingByWI.BeginUpdate();
                    cob_SolvingByWI.ValueMember = "FSWI_ID";
                    cob_SolvingByWI.DisplayMember = "WICode";
                    cob_SolvingByWI.DataSource = dt;
                    cob_SolvingByWI.EndUpdate();
                    cob_SolvingByWI.SelectedIndex = -1;

                }
                dr.Close();
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("เกิดข้อผิดพลาด " + ex.Message, "เกิดข้อผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        private void setDtFailure()
        {
            dtFailure = new DataTable();
            dtFailure.Columns.Add(new DataColumn("NO."));
            dtFailure.Columns.Add(new DataColumn("รหัสจุดเสีย"));
            dtFailure.Columns.Add(new DataColumn("จุดเสีย"));
            dtFailure.Columns.Add(new DataColumn("อาการเสีย"));
            dtFailure.Columns.Add(new DataColumn("S/N(Old)"));
            dtFailure.Columns.Add(new DataColumn("FixAssetNo"));

            dtFailure.Columns.Add(new DataColumn("S/N(New)"));
            dtFailure.Columns.Add(new DataColumn("FixAssetNo_New"));

            dtFailure.Columns.Add(new DataColumn("FCAT_ID"));
            dtFailure.Columns.Add(new DataColumn("FBA_ID"));
            dtFailure.Columns.Add(new DataColumn("FMD_ID"));

            dtFailure.Columns.Add(new DataColumn("Location"));
            dtFailure.Columns.Add(new DataColumn("Model"));

            dtFailure.Columns.Add(new DataColumn("StartLiter"));
            dtFailure.Columns.Add(new DataColumn("EndLiter"));
            dtFailure.Columns.Add(new DataColumn("LiterTest"));
            dtFailure.Columns.Add(new DataColumn("FAI_ID"));

            //20141020
            dtFailure.Columns.Add(new DataColumn("PRT_ID"));
            //20141127ยกเลิก dtFailure.Columns.Add(new DataColumn("EQP_ID"));
            dtFailure.Columns.Add(new DataColumn("ATF_ID"));
            dtFailure.Columns.Add(new DataColumn("FAS_ID"));
            dtFailure.Columns.Add(new DataColumn("SolvingByWI"));


            dtFailure.Columns.Add(new DataColumn("IsCancel"));
            dtFailure.Columns.Add(new DataColumn("IsDownload"));

        }

        //อะไหล่
        private void setDtPart()
        {

            dtPart = new DataTable();
            dtPart.Columns.Add(new DataColumn("รหัสจุดเสีย"));//0
            dtPart.Columns.Add(new DataColumn("จุดเสีย"));//1
            dtPart.Columns.Add(new DataColumn("NO."));//2
            //20140905
            dtPart.Columns.Add(new DataColumn("S/N Old"));
            dtPart.Columns.Add(new DataColumn("Part no."));//3
            dtPart.Columns.Add(new DataColumn("Part"));
            dtPart.Columns.Add(new DataColumn("Type"));//TranType

            dtPart.Columns.Add(new DataColumn("จำนวน"));//Qutity 
            dtPart.Columns.Add(new DataColumn("PricesList"));//Qutity    
            dtPart.Columns.Add(new DataColumn("ราคา/หน่วย"));//Prices
            dtPart.Columns.Add(new DataColumn("รวม"));//SumPrices
            dtPart.Columns.Add(new DataColumn("ภาษี"));//SumVAT
            dtPart.Columns.Add(new DataColumn("รวมสุทธิ"));//SumTotalPrices

            dtPart.Columns.Add(new DataColumn("Discount"));//ส่วนลด %
            dtPart.Columns.Add(new DataColumn("VAT"));// VAT%

            dtPart.Columns.Add(new DataColumn("สถานะ"));
            dtPart.Columns.Add(new DataColumn("Comment"));
            dtPart.Columns.Add(new DataColumn("FAI_ID"));
            dtPart.Columns.Add(new DataColumn("Location"));
            dtPart.Columns.Add(new DataColumn("S/N"));
            dtPart.Columns.Add(new DataColumn("FixAssetNo"));//PTTRM Control Fix Asset No. 


            dtPart.Columns.Add(new DataColumn("IsCancel"));
            dtPart.Columns.Add(new DataColumn("IsDownload"));

            dtPart.Columns.Add(new DataColumn("isPrices"));//part เก็บเงินหน้างาน
            dtPart.Columns.Add(new DataColumn("ออกบิลในนาม"));//ออกบิลในนามสถานี [0=ออกบิลสถานี (เงินสด), 1= ออกบิลบริษัทน้ำมัน]            

            dtPart.Columns.Add(new DataColumn("ERP_SPA_ID"));
            dtPart.Columns.Add(new DataColumn("ERP_PartName"));
            //อะไหล่ลูกค้า
            dtPart.Columns.Add(new DataColumn("อะไหล่ลูกค้า"));

            //ERP_orderline_id
            dtPart.Columns.Add(new DataColumn("ERP_orderline_id"));

            //20141021
            //20141215 dtPart.Columns.Add(new DataColumn("ตำแหน่งที่เปลี่ยน"));
            dtPart.Columns.Add(new DataColumn("Page/Item"));

        }


        private void FailureBinding()
        {
            dataGridView_FailPoint.DataSource = null;
            dataGridView_FailPoint.Refresh();

            dataGridView_FailPoint.DataSource = dtFailure;
            dataGridView_FailPoint.Enabled = true;

            setDataGridViewFailPointStyle();

        }


        private void setDataGridViewFailPointStyle()
        {
            for (int r = 0; r <= dataGridView_FailPoint.RowCount - 1; r++)
            {
                if (dataGridView_FailPoint["IsCancel", r].Value.ToString() == "1")
                {
                    dataGridView_FailPoint["IsCancel", r].Style.BackColor = System.Drawing.Color.Yellow;

                    for (int c = 0; c <= dataGridView_FailPoint.ColumnCount - 1; c++)
                    {
                        dataGridView_FailPoint[c, r].Style.Font = new Font(dataGridView_Part.Font, FontStyle.Bold);
                        dataGridView_FailPoint[c, r].Style.ForeColor = System.Drawing.Color.Red;
                    }


                    dataGridView_FailPoint["อาการเสีย", r].Style.BackColor = Color.Yellow;
                    dataGridView_FailPoint["NO.", r].Style.BackColor = Color.Yellow;
                    dataGridView_FailPoint["รหัสจุดเสีย", r].Style.BackColor = Color.Yellow;
                    dataGridView_FailPoint["จุดเสีย", r].Style.BackColor = Color.Yellow;

                    dataGridView_FailPoint["S/N(Old)", r].Style.BackColor = Color.White;
                    dataGridView_FailPoint["S/N(New)", r].Style.BackColor = Color.White;
                    dataGridView_FailPoint["Location", r].Style.BackColor = Color.White;
                    dataGridView_FailPoint["Model", r].Style.BackColor = Color.White;


                    dataGridView_FailPoint["StartLiter", r].Style.BackColor = Color.White;
                    dataGridView_FailPoint["EndLiter", r].Style.BackColor = Color.White;
                    dataGridView_FailPoint["LiterTest", r].Style.BackColor = Color.Yellow;

                    dataGridView_FailPoint["FAI_ID", r].Style.BackColor = Color.Yellow;
                    dataGridView_FailPoint["IsCancel", r].Style.BackColor = Color.Yellow;
                    dataGridView_FailPoint["IsDownload", r].Style.BackColor = Color.Yellow;


                }
                else
                {

                    dataGridView_FailPoint["IsCancel", r].Style.BackColor = System.Drawing.Color.LightGray;

                    for (int c = 0; c <= dataGridView_FailPoint.ColumnCount - 1; c++)
                    {
                        dataGridView_FailPoint[c, r].Style.Font = new Font(dataGridView_Part.Font, FontStyle.Regular);
                        dataGridView_FailPoint[c, r].Style.ForeColor = System.Drawing.Color.Black;
                    }


                    dataGridView_FailPoint["อาการเสีย", r].Style.BackColor = Color.White;
                    dataGridView_FailPoint["NO.", r].Style.BackColor = Color.LightGray;
                    dataGridView_FailPoint["รหัสจุดเสีย", r].Style.BackColor = Color.LightGray;
                    dataGridView_FailPoint["จุดเสีย", r].Style.BackColor = Color.LightGray;

                    dataGridView_FailPoint["S/N(Old)", r].Style.BackColor = Color.White;
                    dataGridView_FailPoint["S/N(New)", r].Style.BackColor = Color.White;
                    dataGridView_FailPoint["Location", r].Style.BackColor = Color.White;
                    dataGridView_FailPoint["Model", r].Style.BackColor = Color.White;


                    dataGridView_FailPoint["StartLiter", r].Style.BackColor = Color.White;
                    dataGridView_FailPoint["EndLiter", r].Style.BackColor = Color.White;
                    dataGridView_FailPoint["LiterTest", r].Style.BackColor = Color.LightGray;

                    dataGridView_FailPoint["FAI_ID", r].Style.BackColor = Color.LightGray;
                    dataGridView_FailPoint["IsCancel", r].Style.BackColor = Color.LightGray;
                    dataGridView_FailPoint["IsDownload", r].Style.BackColor = Color.LightGray;



                }
            }
        }

        private void PartBindind()
        {
            //cobEndDate.Items.Clear();
            dataGridView_Part.DataSource = null;
            dataGridView_Part.Refresh();

            dataGridView_Part.DataSource = dtPart;
            setDataGridViewStyle();
        }


        private void setDataGridViewStyle()
        {
            for (int r = 0; r <= dataGridView_Part.RowCount - 1; r++)
            {
                if (dataGridView_Part["IsCancel", r].Value.ToString() == "1")
                {
                    dataGridView_Part["IsCancel", r].Style.BackColor = System.Drawing.Color.Yellow;

                    for (int c = 0; c <= dataGridView_Part.ColumnCount - 1; c++)
                    {
                        dataGridView_Part[c, r].Style.Font = new Font(dataGridView_Part.Font, FontStyle.Bold);
                        dataGridView_Part[c, r].Style.ForeColor = System.Drawing.Color.Red;
                    }
                    dataGridView_Part["จำนวน", r].Style.BackColor = Color.Yellow;
                    dataGridView_Part["Comment", r].Style.BackColor = Color.White;
                    dataGridView_Part["สถานะ", r].Style.BackColor = Color.Yellow;
                    dataGridView_Part["รหัสจุดเสีย", r].Style.BackColor = Color.Yellow;
                    dataGridView_Part["จุดเสีย", r].Style.BackColor = Color.Yellow;

                    dataGridView_Part["NO.", r].Style.BackColor = Color.Yellow;
                    //20140908
                    dataGridView_Part["S/N Old", r].Style.BackColor = Color.Yellow;

                    dataGridView_Part["Part no.", r].Style.BackColor = Color.Yellow;
                    dataGridView_Part["Part", r].Style.BackColor = Color.Yellow;
                    dataGridView_Part["Type", r].Style.BackColor = Color.Yellow;
                    dataGridView_Part["FAI_ID", r].Style.BackColor = Color.Yellow;
                    dataGridView_Part["Location", r].Style.BackColor = Color.Yellow;

                    dataGridView_Part["IsDownload", r].Style.BackColor = Color.Yellow;
                    dataGridView_Part["IsPrices", r].Style.BackColor = Color.Yellow;
                    //อะไหล่ลูกค้า
                    //2013-02013 dataGridView_Part["อะไหล่ลูกค้า", r].Style.BackColor = Color.White;
                    dataGridView_Part["อะไหล่ลูกค้า", r].Style.BackColor = Color.Yellow;

                    //20141021
                    //20141215
                    //dataGridView_Part["ตำแหน่งที่เปลี่ยน", r].Style.BackColor = Color.Yellow;

                    //20161115
                    dataGridView_Part["Page/Item", r].Style.BackColor = Color.Yellow;


                }
                else
                {

                    dataGridView_Part["IsCancel", r].Style.BackColor = System.Drawing.Color.LightGray;
                    for (int c = 0; c <= dataGridView_Part.ColumnCount - 1; c++)
                    {
                        dataGridView_Part[c, r].Style.Font = new Font(dataGridView_Part.Font, FontStyle.Regular);
                        dataGridView_Part[c, r].Style.ForeColor = Color.Black;
                    }

                    dataGridView_Part["จำนวน", r].Style.BackColor = Color.White;
                    dataGridView_Part["Comment", r].Style.BackColor = Color.White;
                    dataGridView_Part["สถานะ", r].Style.BackColor = Color.White;

                    dataGridView_Part["รหัสจุดเสีย", r].Style.BackColor = Color.LightGray;
                    dataGridView_Part["จุดเสีย", r].Style.BackColor = Color.LightGray;
                    dataGridView_Part["NO.", r].Style.BackColor = Color.LightGray;
                    //20140908
                    dataGridView_Part["S/N Old", r].Style.BackColor = Color.LightGray;

                    dataGridView_Part["Part no.", r].Style.BackColor = Color.LightGray;
                    dataGridView_Part["Part", r].Style.BackColor = Color.LightGray;
                    dataGridView_Part["Type", r].Style.BackColor = Color.LightGray;

                    dataGridView_Part["PricesList", r].Style.BackColor = Color.LightGray;
                    dataGridView_Part["ราคา/หน่วย", r].Style.BackColor = Color.LightGray;
                    dataGridView_Part["รวม", r].Style.BackColor = Color.LightGray;
                    dataGridView_Part["ภาษี", r].Style.BackColor = Color.LightGray;
                    dataGridView_Part["รวมสุทธิ", r].Style.BackColor = Color.LightGray;
                    dataGridView_Part["Discount", r].Style.BackColor = Color.LightGray;
                    dataGridView_Part["VAT", r].Style.BackColor = Color.LightGray;
                    //dataGridView_Part["FixAssetNo", r].Style.BackColor = Color.LightGray;

                    dataGridView_Part["FAI_ID", r].Style.BackColor = Color.LightGray;
                    dataGridView_Part["Location", r].Style.BackColor = Color.LightGray;
                    dataGridView_Part["IsDownload", r].Style.BackColor = Color.LightGray;

                    if (_IsPrintPrice)
                        dataGridView_Part["IsPrices", r].Style.BackColor = Color.LightGray;
                    else
                        dataGridView_Part["IsPrices", r].Style.BackColor = Color.White;

                    dataGridView_Part["ออกบิลในนาม", r].Style.BackColor = Color.LightGray;
                    dataGridView_Part["ERP_SPA_ID", r].Style.BackColor = Color.LightGray;
                    dataGridView_Part["ERP_PartName", r].Style.BackColor = Color.LightGray;
                    //อะไหล่ลูกค้า
                    dataGridView_Part["อะไหล่ลูกค้า", r].Style.BackColor = Color.White;


                    //20141021
                    //20141215
                    //dataGridView_Part["ตำแหน่งที่เปลี่ยน", r].Style.BackColor = Color.LightGray;

                    //20161115
                    dataGridView_Part["Page/Item", r].Style.BackColor = Color.LightGray;

                }
            }
        }


        private bool CheckFailureData()
        {

            for (int i = 0; i <= dtFailure.Rows.Count - 1; i++)
            {
                if (comboBox_PoitFail.SelectedValue.ToString() == dtFailure.Rows[i]["รหัสจุดเสีย"].ToString().Trim() &&
                    txtFailure.Text == dtFailure.Rows[i]["อาการเสีย"].ToString().Trim() &&
                    txtSNOld.Text == dtFailure.Rows[i]["S/N(Old)"].ToString().Trim())
                    return true;
            }

            return false;
        }
        private void ClearTextFail()
        {
            comboBox_PoitFail.SelectedIndex = -1;


            txtStartLite.Text = "0";
            txtEndtLite.Text = "0";
            txtLiterTest.Text = "0";


        }
        private void ClearPartText()
        {
            tmpSPA_ID = string.Empty;
            txtPartNo.Text = "";
            txtPartName.Text = "";
            txtVAT.Text = _vat.ToString("#0.00");
            txtPriceList.Text = "0.00";
            txtDiscQuarter.Text = "0.00";

            txtPriceUnit.Text = "0.00";

            txtPartQT.Text = "1";
            chIsPrices.Checked = false;
            chIsPrices.Enabled = true;

            radTranType0.Checked = false;
            radTranType1.Checked = false;

            txtPartComment.Text = "";
            txtFixAssetNo.Text = "";

            //cob_TrnType.SelectedIndex = -1;
            cob_TrnType.SelectedValue = "IN";

            con_ERP_StationCharge.SelectedIndex = -1;
            con_ERP_StationCharge.Enabled = true;

            //อะไหล่ลูกค้า
            cbIsCustomer.Checked = false;
            cbIsCustomer.Enabled = true;

            //20140908
            txtPartSerialNumberOld.Text = "";

            //2016115
            txtReferPage.Text = "";
        }

        private void butttonEnable(bool isEnable, bool _Save, bool _Cancel, bool _Print, bool _Exit)
        {
            toolStripLabel_cobby.Enabled = isEnable;
            toolStripText_txtFind.Enabled = isEnable;
            toolStripButton_FindStation.Enabled = isEnable;

            toolStripButton_Save.Enabled = _Save;
            toolStripButton_Cancel.Enabled = _Cancel;
            toolStripButton_Print.Enabled = _Print;
            //toolStripBtSVAdmin.Enabled = _Print;

            toolStripButton_TAGPrimt.Enabled = _Print;
            btFTRPrint.Enabled = _Print;
            toolStripButton_Exit.Enabled = _Exit;
        }





        private void toolStripButton_FindStation_Click(object sender, EventArgs e)
        {
            RadioButtonList_JobStatus1.Enabled = true;
            RadioButtonList_JobStatus2.Enabled = true;

            RadioButtonList_JobStatus1.Checked = false;
            RadioButtonList_JobStatus2.Checked = false;

            try
            {
                if (UserInfo.ConnectMode == "0")
                {
                    checkBox_SMS_BAK.Checked = false;
                    checkBox_SMS_BAK.Visible = false;
                }
                else
                {
                    if (toolStripText_txtFind.Text.Length > 8)
                    {
                        int yy = int.Parse(toolStripText_txtFind.Text.Substring(0, 2));
                        if (yy < 15)
                        {
                            checkBox_SMS_BAK.Visible = true;
                            checkBox_SMS_BAK.Checked = true;
                        }
                    }
                }

                newProgressbar();

                searchData(toolStripText_txtFind.Text.Trim(), UserInfo.ConnectMode);

                closeProgress();

                tabControl1.SelectedIndex = 0;
                groupBox3.Enabled = false;

                txtModel.Focus();

                if (lbl_JobType.Text.Trim() == "ตู้จ่าย(D)")
                {
                    txtModel.Text = string.Empty;
                    txtSNOld.Text = string.Empty;
                    txtSNNew.Text = string.Empty;
                    txtModel.Text = string.Empty;
                    txtStartLite.Text = string.Empty;
                    txtEndtLite.Text = string.Empty;
                }

            }
            catch (System.Exception ex)
            {
                closeProgress();
                MessageBox.Show("ข้อผิดพลาด " + ex.Message, "เกิดข้อผิดพลาด");
            }

            if (editMode == "job_edit")
            {

                //20150629toolStripButton_Print.Enabled = true;

                //--====
                //20170601 แก้ไขย้อนหลัง ไม่อนุญาตให้ print chkEnableSVReport(txtJobID.Text.Trim());
                toolStripButton_Print.Enabled = false;
                //--====
                toolStripButton_TAGPrimt.Enabled = true;
                btFTRPrint.Enabled = true;
                btJobContentMent.Visible = false;


                // using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext())
                strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
                using (DAL.SMSManage.SMSManageDataContext dc = new DAL.SMSManage.SMSManageDataContext(strConn))
                {
                    var j = dc.JOBs
                        .Where(t => t.JOB_ID == txtJobID.Text.Trim() && t.JOBS_ID == "99")
                        .FirstOrDefault();

                    if (j != null)
                    {
                        RadioButtonList_JobStatus2.Checked = true;

                        btJobContentMent.Visible = true;
                        var q = dc.JOB_ContentMents.Where(t => t.JOB_ID == txtJobID.Text.Trim() &&
                            t.ContentStatus == "1"[0]);
                        if (q.Count() > 0)
                        {
                            this.btJobContentMent.ForeColor = System.Drawing.Color.Green;
                            this.btJobContentMent.Image = global::SMSMINI.Properties.Resources.pass;
                            this.btJobContentMent.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
                        }
                        else
                        {
                            this.btJobContentMent.ForeColor = System.Drawing.Color.Red;
                            this.btJobContentMent.Image = global::SMSMINI.Properties.Resources.fail;
                            this.btJobContentMent.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;

                        }
                    }
                    else//Edit Old JOB
                        RadioButtonList_JobStatus1.Checked = true;
                }
            }
            else
            {
                //20150629 toolStripButton_Print.Enabled = false;
                //chkEnableSVReport(txtJobID.Text.Trim());
                //--====
                //20170601 แก้ไขย้อนหลัง ไม่อนุญาตให้ print chkEnableSVReport(txtJobID.Text.Trim());
                toolStripButton_Print.Enabled = false;
                //--====

                toolStripButton_TAGPrimt.Enabled = false;
                btFTRPrint.Enabled = false;

            }

        }

        private void searchData(string txtID, string connectionMode)
        {
            setDtFailure();
            setDtPart();

            //if (UserInfo.ConnectMode == "0")
            //    strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
            //else
            //{
            if (checkBox_SMS_BAK.Checked == true)
                strConn = SMSMINI.Properties.Settings.Default.ServicesMSDB_BAKConnectionString;
            else
                strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
            //}



            string _typeID = UserInfo.Van_Type;

            lblJobInCount.Text = "";

            Finding.FindJobClose f = new SMSMINI.Transaction.Finding.FindJobClose();

            if (checkBox_SMS_BAK.Checked == true)
                f.isSMSbackup = true;
            else
                f.isSMSbackup = false;

            if (toolStripLabel_cobby.Text == "ตาม JOB ID")
            {
                invoke_Progress("ตรวจสอบข้อมูล JOB ตาม JOB ID");
                f.TmpJobID = toolStripText_txtFind.Text.Trim();
            }

            else if (toolStripLabel_cobby.Text == "ตาม วันที่(yyyymmdd)")
            {
                invoke_Progress("ตรวจสอบข้อมูล JOB ตาม วันที่(yyyymmdd)");
                f.TmpJobOpenDate = toolStripText_txtFind.Text.Trim();
            }

            else if (toolStripLabel_cobby.Text == "ตาม สถานี")
            {
                invoke_Progress("ตรวจสอบข้อมูล JOB ตาม สถานี");
                f.TmpStation = toolStripText_txtFind.Text.Trim();
            }
            else
            {
                f.TmpJobID = "";
                f.TmpJobOpenDate = "";
                f.TmpStation = "";
            }

            f.TmpEditMode = editMode;
            f.TmpSearchMode = toolStripLabel_cobby.Text.Trim();
            string _jobID = "";
            closeProgress();

            if (f.ShowDialog() == DialogResult.OK)
            {
                _jobID = f.TmpJobID;
                if (_jobID != "")
                {
                    setJob_Detail_hist(_jobID);

                    checkPartRemain();
                    disableControl(false);

                    //20151105 ป้องกัน ถ้า upload job แล้วห้ามเลือกจุดเสียและเลือกอะไหล่

                    //strConn = Properties.Settings.Default["ServicesMSDBConnectionString"].ToString();
                    strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
                    using (DAL.SMSManage.SMSManageDataContext dc = new DAL.SMSManage.SMSManageDataContext(strConn))
                    {
                        var j = dc.JOBs
                            .Where(t => t.JOB_ID == txtJobID.Text.Trim() && new string[] { "99", "07" }.Contains(t.JOBS_ID) && t.IsUpload == '9')
                            .FirstOrDefault();

                        if (j != null)
                        {
                            btSelectPointFail.Enabled = false;
                            btBtPart_select.Enabled = false;
                            toolStripButton_Save.Enabled = false;

                        }
                        //else
                        //{
                        //    btSelectPointFail.Enabled = true;
                        //    btBtPart_select.Enabled = true;
                        //}
                    }

                    //=======


                    #region checkClientDateTime

                    //if (checkClientDateTime())
                    //{
                    //    MessageBox.Show("วันที่เปิด JOB " + txtOpenJOB.Text + " มากกว่าเวลาบนเครื่อง VAN " +
                    //        DateTime.Now.ToString() + " หรือ" + Environment.NewLine +
                    //        "เวลา Client ไม่ตรงกับ Server " + Environment.NewLine +
                    //        "กรุณาตรวจสอบ เวลาเครื่อง Client ที่เมนู (1) Time Sync", "ผลการตรวจสอบ");

                    //    JOBOnline_Load(null, null);
                    //    return;
                    //}


                    //if (checkClient_StartEnd_DateTime(_jobID))
                    //{
                    //    MessageBox.Show("วันที่ออกงาน... มากกว่าเวลาที่เข้างาน..." + Environment.NewLine +
                    //          "กรุณาตรวจสอบ เวลาเครื่อง Client ที่เมนู (1) Time Sync", "ผลการตรวจสอบ");
                    //    JOBOnline_Load(null, null);
                    //    return;
                    //}
                    #endregion

                }

            }



        }
        private void checkPartRemain()
        {
            for (int i = 0; i <= dataGridView_Part.RowCount - 1; i++)
            {

                if (dataGridView_Part["สถานะ", i].Value.ToString() == "ค้าง")// && dataGridView_Part["IsCancel", i].Value.ToString()=="0")
                {
                    RadioButtonList_JobStatus1.Checked = true;

                    RadioButtonList_JobStatus1.Enabled = false;
                    RadioButtonList_JobStatus2.Enabled = false;

                    tabControl1.Enabled = true;
                    return;
                }
            }

            if (RadioButtonList_JobStatus2.Checked != true)
            {
                //RadioButtonList_JobStatus1.Checked = false;
                // RadioButtonList_JobStatus2.Checked = false;

                RadioButtonList_JobStatus1.Enabled = true;
                RadioButtonList_JobStatus2.Enabled = true;
            }
            else
            {
                RadioButtonList_JobStatus1.Enabled = false;
                RadioButtonList_JobStatus2.Enabled = false;
            }
        }

        decimal _ActualDistance = 0;
        decimal _DistanceFromCenter = 0;
        string _TYP_ID1 = "";

        private void setJob_Detail_hist(string jobid)
        {

            //if (UserInfo.ConnectMode == "0")
            //    strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
            //else
            ////strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
            //{
            if (checkBox_SMS_BAK.Checked == true)
                strConn = SMSMINI.Properties.Settings.Default.ServicesMSDB_BAKConnectionString;
            else
                strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
            //}


            using (DAL.SMSManage.SMSManageDataContext db = new DAL.SMSManage.SMSManageDataContext(strConn))
            {

                #region GetVAT
                var _v = db.VATs.Where(t => t.VAT_ID == "1"[0]).FirstOrDefault();
                if (_v != null)
                    _vat = _v.VAT1.Value;

                #endregion

                #region JOB
                string tmpQuarter = "";

                var job = db.JOBs
                    .Where(t => t.JOB_ID.Trim() == jobid.Trim())
                    .FirstOrDefault();

                _job = job;

                //lsJOB

                JOB_IS_PrintPrice(db);
                tmpTYP_ID1 = job.TYP_ID1;


                if (!string.IsNullOrEmpty(_job.ERP_documentno))
                    txtJobSO.Text = _job.ERP_documentno;

                //==20151106 เพิ่ม workOrder
                txtWorkID.Text = "";
                var jobWO = db.JOB_WorkIDs
                   .Where(t => t.JOB_ID.Trim() == job.JOB_ID)
                   .FirstOrDefault();

                if (jobWO != null)
                {
                    if (!string.IsNullOrEmpty(jobWO.Work_ID))
                        txtWorkID.Text = jobWO.Work_ID;
                }


                //==

                _contractNo = string.IsNullOrEmpty(job.ContractNo) ? string.Empty : job.ContractNo;

                comboBox_Priority.SelectedValue = job.PRI_ID.Trim();


                //====20141201
                setComboBox_PoitFail(job.TYP_ID.Trim());



                //RadioButtonList_JobStatus
                if (editMode == "job_edit")//แก้ไข
                {
                    if (job.JOBS_ID.Trim() != "99")
                        RadioButtonList_JobStatus1.Checked = true;
                    else
                        RadioButtonList_JobStatus2.Checked = true;//Close JOB
                }

                string uopenJob = "...";

                if ((from t in db.Employees where t.EMP_ID.Trim() == job.EMP_ID0.Trim() select t).Count() > 0)
                {
                    var u = (from t in db.Employees where t.EMP_ID.Trim() == job.EMP_ID0.Trim() select t).FirstOrDefault();
                    uopenJob = u.FName ?? "";
                }

                var sGopID = (from t in db.Stations where t.STA_ID.Trim() == job.STA_ID.Trim() select t).FirstOrDefault();

                var xStation_Group = db.Station_Groups.Where(t => t.GOP_ID == sGopID.GOP_ID).FirstOrDefault();

                /*20170503  SVSAM_Offline ต้อง comment  if gop  */
                if (sGopID != null)
                {
                    tmpGOPId = sGopID.GOP_ID;//ใช้ map คำนวญ Price list
                    tmpSTAID = sGopID.STA_ID;
                    tmpGopName = xStation_Group.GroupStation ?? sGopID.GOP_ID;
                }

                var s = (from t in db.Stations where t.STA_ID.Trim() == job.STA_ID.Trim() select t).FirstOrDefault();
                var spro = db.Provinces.Where(t => t.PRO_ID == s.PRO_ID).FirstOrDefault();

                var jt = (from t in db.Types where t.TYP_ID.Trim() == job.TYP_ID.Trim() select t).FirstOrDefault();

                var inc = (from t in db.JOB_Details where t.JOB_ID.Trim() == job.JOB_ID.Trim() && t.StatusDetail == true select t).Count();
                //ดึงเวลาเข้าครั้งล่าสุด
                var dateIn = ((from t in db.JOB_Details where t.JOB_ID.Trim() == job.JOB_ID.Trim() && t.StatusDetail == true orderby t.RecDate descending select t).FirstOrDefault());


                //ระยะทาง
                var sd = (from t in db.Station_Distances where t.STA_ID.Trim() == job.STA_ID.Trim() && t.TYP_ID == job.TYP_ID select t).FirstOrDefault();
                if (sd != null)
                {
                    _ActualDistance = sd.ActualDistance;
                    _DistanceFromCenter = sd.DistanceFromCenter ?? 0;
                }
                //*******************************************************************
                //รายละเอียดใบ JOB
                //*******************************************************************                
                txtJobID.Text = job.JOB_ID ?? "";
                tmpjobID = txtJobID.Text.Trim();//เก็ยไว้นำไปพิมพ์ SV
                tmpJobType = job.TYP_ID; //เก็บประเภท JOB 

                txtProjectNO.Text = job.PROJ_ID ?? "";
                _Jobopendate = job.Opendate.Value;

                tmpQuarter = ConnectionManager.dates_Quarter(Convert.ToDateTime(job.Opendate.Value));
                toolStripLabel_QUARTER.Text = "QUARTER: " + tmpQuarter;



                txtOpenJOB.Text = job.Opendate.ToString();
                txtStartDateIn.Text = dateIn == null ? "" : dateIn.StartDate.ToString("dd/MM/yyyy HH:mm:sss", new System.Globalization.CultureInfo("th-TH"));
                //txtPointFaile.Text =  txtPointFaile.Text = job.PointFailure.PointFailure1 ?? "";
                txtPointFaile.Text = job.PointFailure.PointFailure1 ?? "";
                txtFialureCode.Text = job.FAI_ID ?? "";
                FailureDetail.Text = job.JobFailure_Detail ?? "";
                txtInformer.Text = (job.Informer ?? "") + " " + (job.InPhone ?? "");
                txtUserOpenJob.Text = uopenJob ?? "";

                txtStation.Text = (s.STA_ID1 ?? s.STA_ID) + ": " +
                    (s.StationSys ?? "") + " ที่อยู่:" +
                    (s.Address ?? "") + " จ." +
                    (spro == null ? "<ยังไม่ระบุ ข้อมูล>" : (spro.Province1)) + " ระยะทาง:" +
                    _ActualDistance.ToString() + " กม. ห่างจากศูนย์: " +
                    _DistanceFromCenter.ToString(); // StationSys

                lbl_JobType.Text = jt.Type1 ?? "ไม่ระบุ";//ประเภท JOB
                lblJobInCount.Text = inc.ToString();// เข้าครั้งที่

                var workType = (from t in db.Types where t.TYP_ID == job.TYP_ID1 select t).FirstOrDefault();
                txtWorkType.Text = workType == null ? "" : workType.TYP_ID + ":" + workType.Type1;
                _TYP_ID1 = workType == null ? "" : workType.TYP_ID;


                //20130926
                if (job.IsStationCharge == true)
                {
                    MessageBox.Show("เลขที่ Job." + job.JOB_ID.ToString() + " เก็บเงินสดที่สถานีบริการ " + Environment.NewLine +
                                "กรุณาตรวจสอบ...", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }



                //เก็บเงินสดหน้าสถานี
                if (job.IsStationCharge == true)
                {
                    _IsStationCharge = true;
                }







                #region ล็อก TranType ตามเงื่อนไขสัญญา
                var _jobid = txtJobID.Text.Substring(4, 1);

                if (new string[] { "D", "S" }.Contains(_jobid))
                {

                    var qlt = db.Conf_JOB_IsLockTranTypes
                        .Where(t => t.GOP_ID == tmpGOPId &&
                                t.TYP_ID == _job.TYP_ID &&
                                t.TYP_ID1 == _job.TYP_ID1).FirstOrDefault();


                    if (qlt != null)//งานในสัญญา                         
                        isLockTranType = true;
                    else
                        isLockTranType = false;


                }
                else
                {
                    isLockTranType = false;
                }

                lockTranType();

                if (workType.TYP_ID == "OU")// OU = งาน นอกสัญญาทั่วไป
                {
                    //cob_TrnType_SetNewItem(db);
                }

                #endregion


                //get Contract
                try
                {
                    lblIsContract.Visible = true;
                    txtContract.Visible = true;

                    if (s.IsContract != null)//if (s.xContract != null)
                    {
                        lblIsContract.Text = (s.xContract.Value == "0"[0] || s.xContract.Value.Equals(null)) ? "สถานีในสัญญา: NO" : "สถานีในสัญญา: YES";


                        txtContract.Text =
                       "สถานี: " + (s.StationSys ?? "") + Environment.NewLine +
                       "กลุ่มบริษัท: " + xStation_Group.GroupStation + Environment.NewLine +
                       "หมดประกัน: " + (s.WarrantyDate ?? null) + Environment.NewLine +
                       "ติดตั้ง: " + (s.InstallDate ?? null) + Environment.NewLine +
                       "เลขที่สัญญา: " + _contractNo;
                    }
                    else
                    {
                        lblIsContract.Text = "สถานีในสัญญา: NO";
                        txtContract.Text =
                       "สถานี: " + (s.StationSys ?? "") + Environment.NewLine +
                       "กลุ่มบริษัท: " + xStation_Group.GroupStation + Environment.NewLine +
                       "หมดประกัน:-- " + Environment.NewLine +
                       "ติดตั้ง:-- " + Environment.NewLine +
                       "เลขที่สัญญา: " + _contractNo;
                    }
                }
                catch (Exception)
                {

                    //throw;
                }

                #endregion


                #region dataGridView_tmpPart

                var part = from t in db.JOB_Detail_Spares where t.JOB_ID.Trim() == jobid.Trim() select t;
                if (part.Count() > 0)
                {
                    dataGridView_tmpPart.DataSource = part.ToList();
                    dataGridView_tmpPart.Refresh();

                    ////---เปลี่ยนแปลงเงื่อนไขให้ printได้ทุกวันที่ปิดงานค้างและปิดงาน 20151217
                    //var _endDate = (from t in db.JOB_Detail_Spares where t.JOB_ID.Trim() == jobid.Trim() select t.Enddate).Distinct();
                    ////--20150813 เพิ่ม clear  วันที่ f8 เดิม
                    //cobEndDate.Items.Clear();

                    ////cobEndDate.SelectedItem= "";

                    ////cobEndDate.Items.Add("");
                    ////------------------------------------
                    //foreach (var endDate in _endDate)
                    //{
                    //    if (endDate.Value != null)
                    //    {
                    //        cobEndDate.Items.Add(endDate.Value);

                    //    }
                    //}
                    ////---เปลี่ยนแปลงเงื่อนไขให้ printได้ทุกวันที่ปิดงานค้างและปิดงาน

                    //===== 20151217
                    cobEndDate.Items.Clear();




                }// var part 
                else
                {
                    dataGridView_tmpPart.DataSource = null;
                    dataGridView_tmpPart.Refresh();
                    //cobEndDate.Items.Clear();


                    ////=== 20150831 ====
                    cobEndDate.Items.Clear();



                }


                if (cobEndDate.Items.Count < 1)
                {
                    var _endDate = db.JOB_Details
                        .Where(t => t.JOB_ID.Trim() == jobid.Trim() && t.EndDate != null && new string[] { "99", "07" }.Contains(t.JOBS_ID))
                        .Distinct();

                    if (_endDate.Count() > 0)
                    {
                        //--20150813 เพิ่ม clear  วันที่ f8 เดิม
                        cobEndDate.Items.Clear();
                        //cobEndDate.SelectedItem = "";
                        //cobEndDate.Items.Add("");
                        //cobEndDate.SelectedValue = "";
                        //------------------------------------
                        foreach (var endDate in _endDate)
                        {
                            if (endDate.EndDate.Value != null)
                            {
                                cobEndDate.Items.Add(endDate.EndDate.Value);
                            }
                        }
                    }
                }

                #endregion


                logCancelSPA_ID = new List<string>();

                if (editMode == "job_edit")//ถ้าเป็นการแก้ไขต้องดึงอะใหล่มาแสดงทั้งหมดใน Job นั้น
                {
                    #region job_edit

                    var q1 = from t in db.JOB_Detail_Spares
                             from p in db.PointFailures
                             from sp in db.SpareParts
                             where t.JOB_ID.Trim() == jobid.Trim() &&
                                t.POI_ID.Trim() == p.POI_ID.Trim() &&
                                t.SPA_ID == sp.SPA_ID
                             select new
                             {
                                 POI_ID = p.POI_ID,
                                 PoiFailure = p.PointFailure1,
                                 t.NO,
                                 SPA_ID = t.SPA_ID,
                                 PartName = sp.SparePart1,

                                 VAT = t.VAT ?? 0,
                                 t.ControlFixAssetNo,
                                 PricesList = t.PricesList ?? 0,
                                 Prices = t.Prices ?? 0,
                                 SumPrices = t.SumPrices ?? 0,
                                 SumVAT = t.SumVAT ?? 0,
                                 SumTotalPrices = t.SumTotalPrices ?? 0,
                                 Discount = t.Discount ?? 0,

                                 Quantity = t.Quantity ?? 0,
                                 StatusSpare = t.StatusSpare,
                                 TRA_ID = t.TRA_ID,
                                 Comment = t.Comment,
                                 FAI_ID = t.FAI_ID ?? "0000000",
                                 Location = t.Location ?? "",
                                 t.SerialNumber,
                                 t.IsCancel,
                                 t.IsDownload,
                                 IsPrices = (t.IsPrices ?? false),
                                 t.ERP_StationCharge,

                                 t.ERP_SPA_ID,
                                 t.ERP_PartName,
                                 //อะไหล่ลูกค้า
                                 t.IsCustomer

                                 //20140908
                                 ,
                                 t.SerialNumber_Old

                                 //20141022
                                 //20141127ยกเลิก ,t.EQP_ID


                                 //20161114
                                 ,
                                 t.ReferPage

                             };


                    foreach (var x1 in q1)
                    {

                        DataRow dr = dtPart.NewRow();
                        dr["รหัสจุดเสีย"] = x1.POI_ID;
                        dr["จุดเสีย"] = x1.PoiFailure == "&nbsp;" ? "" : x1.PoiFailure;

                        dr["NO."] = x1.NO;

                        //20140908
                        dr["S/N Old"] = x1.SerialNumber_Old;

                        dr["Part no."] = x1.SPA_ID;
                        dr["Part"] = x1.PartName;
                        dr["Type"] = x1.TRA_ID;

                        dr["จำนวน"] = x1.Quantity;
                        dr["PricesList"] = x1.PricesList.ToString("#,##0.00");
                        dr["ราคา/หน่วย"] = x1.Prices.ToString("#,##0.00");
                        dr["รวม"] = x1.SumPrices.ToString("#,##0.00");
                        dr["ภาษี"] = x1.SumVAT.ToString("#,##0.00");

                        dr["รวมสุทธิ"] = x1.SumTotalPrices.ToString("#,##0.00");

                        dr["VAT"] = x1.VAT.ToString("#,##0.00");
                        dr["FixAssetNo"] = x1.ControlFixAssetNo;
                        dr["Discount"] = x1.Discount.ToString("#,##0.00");

                        dr["สถานะ"] = x1.StatusSpare == true ? "เปลี่ยน" : "ค้าง";

                        dr["Comment"] = x1.Comment == "&nbsp;" ? "" : x1.Comment;
                        dr["FAI_ID"] = x1.FAI_ID;
                        dr["Location"] = x1.Location;
                        dr["S/N"] = x1.SerialNumber ?? "";
                        dr["IsCancel"] = x1.IsCancel ?? "0"[0];
                        dr["IsDownload"] = x1.IsDownload ?? "Y"[0];

                        dr["isPrices"] = (x1.IsPrices == false ? "0" : "1");
                        dr["ออกบิลในนาม"] = (x1.ERP_StationCharge == null ? "สถานี" : (x1.ERP_StationCharge == '0' ? "สถานี" : "บริษัทน้ำมัน"));


                        dr["ERP_SPA_ID"] = x1.ERP_SPA_ID;
                        dr["ERP_PartName"] = x1.ERP_PartName;
                        //อะไหล่ลูกค้า
                        dr["อะไหล่ลูกค้า"] = x1.IsCustomer == '9' ? "Y" : "N";

                        //20141022
                        //20141127ยกเลิก dr["ตำแหน่งที่เปลี่ยน"] = x1.EQP_ID;

                        //20161114
                        dr["Page/Item"] = x1.ReferPage;

                        logCancelSPA_ID.Add(x1.SPA_ID);

                        dtPart.Rows.Add(dr);



                    }

                    #endregion
                }
                else//ถ้าไม่ใช่แก้ไข ดึงเฉพาะอะไหล่ที่ค้างมาแสดง
                {

                    #region job_add

                    var q1 = from t in db.JOB_Detail_Spares
                             from p in db.PointFailures
                             from sp in db.SpareParts
                             where t.JOB_ID.Trim() == jobid.Trim() &&
                                t.POI_ID.Trim() == p.POI_ID.Trim() &&
                                t.SPA_ID == sp.SPA_ID &&
                                t.StatusSpare == false //ดึงเฉพาะอะไหล่ที่ค้างมาแสดง
                             select new
                             {
                                 //Poi_NO = t.Poi_NO,
                                 POI_ID = p.POI_ID,
                                 PoiFailure = p.PointFailure1,
                                 t.NO,
                                 t.SPA_ID,
                                 PartName = sp.SparePart1,

                                 VAT = t.VAT ?? 0,
                                 t.ControlFixAssetNo,
                                 PricesList = t.PricesList ?? 0,
                                 Prices = t.Prices ?? 0,
                                 Discount = t.Discount ?? 0,
                                 SumPrices = t.SumPrices ?? 0,
                                 SumTotalPrices = t.SumTotalPrices ?? 0,
                                 SumVAT = t.SumVAT ?? 0,
                                 Quantity = t.Quantity ?? 0,
                                 StatusSpare = t.StatusSpare,
                                 TRA_ID = t.TRA_ID,
                                 Comment = t.Comment,
                                 FAI_ID = t.FAI_ID ?? "0000000",
                                 Location = t.Location ?? "",
                                 t.SerialNumber,
                                 IsPrices = (t.IsPrices ?? false),
                                 t.ERP_StationCharge,

                                 t.ERP_SPA_ID,
                                 t.ERP_PartName,
                                 t.IsCustomer

                                 //20140908
                                 ,
                                 t.SerialNumber_Old

                                 //20141022
                                 //20141127ยกเลิก ,t.EQP_ID


                                 //20161114
                                 ,
                                 t.ReferPage
                             };


                    foreach (var x1 in q1)
                    {
                        DataRow dr = dtPart.NewRow();
                        dr["รหัสจุดเสีย"] = x1.POI_ID;
                        dr["จุดเสีย"] = x1.PoiFailure == "&nbsp;" ? "" : x1.PoiFailure;

                        dr["NO."] = x1.NO;

                        //20140908
                        dr["S/N Old"] = x1.SerialNumber_Old;

                        dr["Part no."] = x1.SPA_ID;
                        dr["Part"] = x1.PartName;
                        dr["Type"] = x1.TRA_ID;

                        dr["จำนวน"] = x1.Quantity;

                        dr["PricesList"] = x1.PricesList.ToString("#,##0.00");

                        dr["ราคา/หน่วย"] = x1.Prices.ToString("#,##0.00");
                        dr["รวม"] = x1.SumPrices.ToString("#,##0.00");
                        dr["ภาษี"] = x1.SumVAT.ToString("#,##0.00");

                        dr["รวมสุทธิ"] = x1.SumTotalPrices.ToString("#,##0.00");

                        dr["VAT"] = x1.VAT.ToString("#,##0.00");
                        dr["FixAssetNo"] = x1.ControlFixAssetNo;
                        dr["Discount"] = x1.Discount.ToString("#,##0.00");

                        dr["สถานะ"] = x1.StatusSpare == true ? "เปลี่ยน" : "ค้าง";

                        dr["Comment"] = x1.Comment == "&nbsp;" ? "" : x1.Comment;
                        dr["FAI_ID"] = x1.FAI_ID;
                        dr["Location"] = x1.Location;
                        dr["S/N"] = x1.SerialNumber ?? "";
                        dr["IsCancel"] = "0";
                        dr["IsDownload"] = "Y";

                        dr["isPrices"] = (x1.IsPrices == false ? "0" : "1");

                        dr["ออกบิลในนาม"] = (x1.ERP_StationCharge == null ? "สถานี" : (x1.ERP_StationCharge == '0' ? "สถานี" : "บริษัทน้ำมัน"));

                        dr["ERP_SPA_ID"] = x1.ERP_SPA_ID;
                        dr["ERP_PartName"] = x1.ERP_PartName;
                        //อะไหล่ลูกค้า
                        dr["อะไหล่ลูกค้า"] = x1.IsCustomer == '9' ? "Y" : "N";

                        //20141022
                        //20141127ยกเลิก dr["ตำแหน่งที่เปลี่ยน"] = x1.EQP_ID;

                        //20161114
                        dr["Page/Item"] = x1.ReferPage;


                        logCancelSPA_ID.Add(x1.SPA_ID);

                        dtPart.Rows.Add(dr);

                    }

                    #endregion
                }

                PartBindind();


                #region JOB_Failure_Details

                var q = from t in db.JOB_Failure_Details where t.JOB_ID.Trim() == jobid.Trim() select t;
                if (q.Count() > 0)
                {
                    foreach (var x in q)
                    {
                        var xPointFailure = db.PointFailures.Where(t => t.POI_ID == x.POI_ID).FirstOrDefault();

                        DataRow dr = dtFailure.NewRow();
                        dr["NO."] = x.NO;
                        dr["รหัสจุดเสีย"] = x.POI_ID;
                        dr["จุดเสีย"] = (xPointFailure ==null ? "" : xPointFailure.PointFailure1);
                        dr["อาการเสีย"] = x.Failure_Detail == "&nbsp;" ? "" : x.Failure_Detail;
                        dr["S/N(Old)"] = x.SerialNumber == "&nbsp;" ? "" : x.SerialNumber;
                        dr["FixAssetNo"] = x.SerialNumber == "&nbsp;" ? "" : x.FixAssetNo;

                        dr["S/N(New)"] = x.SerialNumber_New == "&nbsp;" ? "" : x.SerialNumber_New;
                        dr["FixAssetNo_New"] = x.SerialNumber_New == "&nbsp;" ? "" : x.FixAssetNo_New;

                        dr["FCAT_ID"] = x.FCAT_ID ?? 0;
                        dr["FBA_ID"] = x.FBA_ID ?? 0;
                        dr["FMD_ID"] = x.FMD_ID ?? 0;

                        dr["Location"] = x.LOC_ID == null ? "0" : Convert.ToString(x.LOC_ID);//20141215 x.Location == "&nbsp;" ? "" : x.Location;
                        dr["Model"] = x.Model == "&nbsp;" ? "" : x.Model;
                        dr["StartLiter"] = x.StartLiter ?? 0;
                        dr["EndLiter"] = x.EndLiter ?? 0;
                        dr["LiterTest"] = x.LiterTest ?? 0;
                        dr["FAI_ID"] = x.FAI_ID ?? "0000000";

                        dr["IsCancel"] = x.IsCancel ?? "0"[0];
                        dr["IsDownload"] = x.IsDownload ?? "Y"[0];

                        //20141022
                        dr["PRT_ID"] = x.PRT_ID;
                        //20141127ยกเลิก dr["EQP_ID"] = x.EQP_ID;
                        dr["ATF_ID"] = x.ATF_ID;
                        dr["FAS_ID"] = x.FAS_ID;
                        dr["SolvingByWI"] = x.SolvingByWI;

                        dtFailure.Rows.Add(dr);

                    }
                    FailureBinding();
                }
                else
                {
                    if (dtPart.Rows.Count > 0)
                    {

                        DataRow dr = dtFailure.NewRow();
                        dr["NO."] = "1";
                        dr["รหัสจุดเสีย"] = "000";
                        dr["จุดเสีย"] = "ไม่ระบุ";
                        dr["อาการเสีย"] = "ไม่ระบุ";
                        dr["S/N(Old)"] = "None";
                        dr["S/N(New)"] = "None";

                        dr["FCAT_ID"] = 0;
                        dr["FBA_ID"] = 0;
                        dr["FMD_ID"] = 0;

                        dr["Location"] = "None";
                        dr["Model"] = "None";
                        dr["StartLiter"] = 0;
                        dr["EndLiter"] = 0;
                        dr["LiterTest"] = 0;
                        dr["FAI_ID"] = "0000000";

                        dr["IsCancel"] = "0"[0];
                        dr["IsDownload"] = "N"[0];

                        //20141022
                        dr["PRT_ID"] = 0;
                        //20141127ยกเลิก dr["EQP_ID"] = 0;
                        dr["ATF_ID"] = null;
                        dr["FAS_ID"] = 0;
                        dr["SolvingByWI"] = null;
                        dtFailure.Rows.Add(dr);

                        FailureBinding();
                    }
                    //20150831
                    FailureBinding();
                }



                #endregion


                #region Problem_Detail, Resolve_Detail
                if ((from t in db.JOB_Details where t.JOB_ID == jobid orderby t.RecDate descending select t).Count() > 0)
                {
                    var q2 = (from t in db.JOB_Details where t.JOB_ID == jobid orderby t.RecDate descending select t).FirstOrDefault();

                    //20150126 txtProblem_Detail.Text = q2.Problem_Detail ?? "";
                    txtResolve_Detail.Text = q2.Resole_Detail ?? "";
                }

                #endregion


                #region cobProblemType
                //cobProblemType

                if (job.PMT_ID == null)
                {
                    if (lbl_JobType.Text.Trim() == "ตู้จ่าย(D)")
                        cobProblemType.SelectedValue = "AO";//"BW";
                    else
                        cobProblemType.SelectedIndex = -1;
                }
                else
                    cobProblemType.SelectedValue = job.PMT_ID;

                cobProblemType.Enabled = true;

                #endregion


                #region ความพึงพอใจ:


                //ความพึงพอใจ:
                if ((from t in db.JOB_ContentMents where t.JOB_ID == jobid select t).Count() > 0)
                {
                    var q2 = (from t in db.JOB_ContentMents where t.JOB_ID == jobid select t).FirstOrDefault();

                    switch (q2.JobContentMent.Trim())
                    {
                        case "A": radioButton_A.Checked = true; break;
                        case "B": radioButton_B.Checked = true; break;
                        case "C": radioButton_C.Checked = true; break;
                        case "D": radioButton_D.Checked = true; break;

                    }
                }

                #endregion



                if (_TYP_ID1 == "SS" && txtJobSO.Text != string.Empty)
                {
                    MessageBox.Show("งานติดตั้ง (S5)..." + Environment.NewLine +
                    "บริษัท จ่ายค่าคอมมิชั่น" + Environment.NewLine +
                    "ขอบคุณเจ้าหน้าที่ โฟลว์โก้ ที่ช่วยกันดูแล ลูกค้าเป็นอย่างดี", "งานติดตั้ง (S5)",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
            }
        } //end setJob_Detail_hist
        private void JOB_IS_PrintPrice(DAL.SMSManage.SMSManageDataContext dc)
        {
            var isP = dc.Conf_JOB_IS_PrintPrices
                .Where(t => t.GOP_ID == tmpGOPId && t.TYP_ID == _job.TYP_ID)
                .FirstOrDefault();

            if (isP != null)
            {
                if (isP.SVIsPrintPrice == true)
                {
                    chIsPrices.Checked = true;
                    chIsPrices.Enabled = false;
                    _IsPrintPrice = true;
                }
                else
                {
                    chIsPrices.Checked = false;
                    //chIsPrices.Enabled = false;
                }
            }
            else
            {
                chIsPrices.Checked = false;
                //chIsPrices.Enabled = false;
            }
        }

        private void setComboBox_PoitFail(string vJOBType)
        {
            //if (UserInfo.ConnectMode == "0")
            //    strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
            //else
            strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;

            try
            {
                string _typeID = UserInfo.Van_Type;
                string sql = "";
                /*//20150212  sql = "SELECT POI_ID, PointFailure FROM [PointFailure] WHERE (JOBType = '" + vJOBType + "')  Order by OrderNo; " +
                          " SELECT [FSWI_ID] , [WICode] FROM [Failure_Solving_WI] WHERE (JOBType = '" + vJOBType + "' or FSWI_ID = '0' )   ORDER BY [FSWI_ID]; ";
                 */
                sql = "SELECT POI_ID, PointFailure FROM [PointFailure] WHERE IsCancel = 'Y' Order by OrderNo; " +
                         //" SELECT [FSWI_ID] , [WICode] FROM [Failure_Solving_WI] WHERE (JOBType = '" + vJOBType + "' or FSWI_ID = '0' )   ORDER BY [FSWI_ID]; ";
                         " SELECT [FSWI_ID] , [WICode]+': '+[WIDetail] as [WICode] FROM [Failure_Solving_WI] WHERE (JOBType = '" + vJOBType + "' or FSWI_ID = '0' )   ORDER BY [FSWI_ID]; ";


                SqlDataReader dr = JaSqlHelper.ExecuteReader(strConn, CommandType.Text, sql);
                DataTable dt;

                if (dr.HasRows)
                {
                    //comboBox_PoitFail
                    dt = new DataTable();
                    dt.Load(dr);
                    comboBox_PoitFail.BeginUpdate();
                    comboBox_PoitFail.ValueMember = "POI_ID";
                    comboBox_PoitFail.DisplayMember = "PointFailure";
                    comboBox_PoitFail.DataSource = dt;
                    comboBox_PoitFail.EndUpdate();
                    comboBox_PoitFail.SelectedIndex = -1;

                    //cob_SolvingByWI
                    dt = new DataTable();
                    dt.Load(dr);
                    cob_SolvingByWI.BeginUpdate();
                    cob_SolvingByWI.ValueMember = "FSWI_ID";
                    cob_SolvingByWI.DisplayMember = "WICode";
                    cob_SolvingByWI.DataSource = dt;
                    cob_SolvingByWI.EndUpdate();
                    cob_SolvingByWI.SelectedIndex = -1;//0;//-1;

                }
                dr.Close();
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("เกิดข้อผิดพลาด " + ex.Message, "เกิดข้อผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lockTranType()
        {
            if (isLockTranType)
            {
                cob_TrnType.Enabled = true;//20160829 false;
                txtPriceList.ReadOnly = true;
                txtDiscQuarter.ReadOnly = true;
                txtVAT.ReadOnly = true;
            }
            else
            {
                cob_TrnType.Enabled = true;

                txtPriceList.ReadOnly = false;
                txtDiscQuarter.ReadOnly = false;
                txtVAT.ReadOnly = false;
            }
        }


        //private bool checkClientDateTime()
        //{
        //    DateTime dtOpenJOB = Convert.ToDateTime(_Jobopendate.ToString("yyyy/MM/dd HH:mm"));
        //    DateTime dtCloseJOB = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm"));

        //    if (DateTime.Compare(dtOpenJOB, dtCloseJOB) > 0)  //วันที่เปิด JOB มากกว่า วันที่ปิด JOB
        //        return true;
        //    return false;

        //}


        //private bool checkClient_StartEnd_DateTime(string jobID)
        //{
        //    using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext())
        //    {
        //        var q = dc.JOB_Details.Where(t => t.JOB_ID == jobID)
        //            .OrderByDescending(r => r.RecDate)
        //            .FirstOrDefault();
        //        if (q != null)
        //        {
        //            DateTime dtStart = q.StartDate;
        //            DateTime dtEnd = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm"));

        //            if (DateTime.Compare(dtStart, dtEnd) > 0)  //วันที่เปิด JOB มากกว่า วันที่ปิด JOB
        //                return true;
        //        }
        //    }
        //    return false;

        //}










        //20150626
        private void chkEnableSVReport(string jobID)
        {
            //if (UserInfo.ConnectMode == "0")
            //    strConn = Properties.Settings.Default["ServicesMSDBConnectionString"].ToString();
            //else
            //    strConn = Properties.Settings.Default["ServicesMSDBConnectionString"].ToString();


            //if (UserInfo.ConnectMode == "0")
            //{
            //    strConn = Properties.Settings.Default["ServicesMSDBConnectionString"].ToString();
            //    using (DAL.SMSManage.SMSManageDataContext dc = new DAL.SMSManage.SMSManageDataContext(strConn))
            //    {
            //        var j = dc.JOBs
            //            .Where(t => t.JOB_ID == jobID && t.IsUpload == '9')//t.JOBS_ID == "99")
            //            .FirstOrDefault();

            //        if (j != null)
            //        {
            //            toolStripButton_Print.Enabled = true;

            //        }
            //        else
            //        {
            //            toolStripButton_Print.Enabled = false;
            //        }
            //    }
            //}
            //else
            ////strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
            //{
            if (checkBox_SMS_BAK.Checked == true)
            {
                strConn = SMSMINI.Properties.Settings.Default.ServicesMSDB_BAKConnectionString;
                using (DAL.SMSManage.SMSManageDataContext dc = new DAL.SMSManage.SMSManageDataContext(strConn))
                {
                    var j = dc.JOBs
                        .Where(t => t.JOB_ID == jobID && new string[] { "99", "07" }.Contains(t.JOBS_ID))//t.UploadDate  != null )//t.JOBS_ID == "99")
                        .FirstOrDefault();

                    if (j != null)
                    {
                        toolStripButton_Print.Enabled = true;

                    }
                    else
                    {
                        toolStripButton_Print.Enabled = false;
                    }
                }
            }
            else
            {
                strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
                using (DAL.SMSManage.SMSManageDataContext dc = new DAL.SMSManage.SMSManageDataContext(strConn))
                {
                    var j = dc.JOBs
                        .Where(t => t.JOB_ID == jobID && new string[] { "99", "07" }.Contains(t.JOBS_ID))//t.UploadDate != null)//t.JOBS_ID == "99")
                        .FirstOrDefault();

                    if (j != null)
                    {
                        toolStripButton_Print.Enabled = true;

                    }
                    else
                    {
                        toolStripButton_Print.Enabled = false;
                    }
                }
            }


            //}


            //using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext())
            //using (DAL.SMSManage.SMSManageDataContext dc = new DAL.SMSManage.SMSManageDataContext(strConn))
            // {
            //     var j = dc.JOBs
            //         .Where(t => t.JOB_ID == jobID && t.IsUpload == '9')//t.JOBS_ID == "99")
            //         .FirstOrDefault();

            //     if (j != null)
            //     {
            //         toolStripButton_Print.Enabled = true;

            //     }
            //     else
            //     {
            //         toolStripButton_Print.Enabled = false;
            //     }
            // }
        }

        private void toolStripButton_Save_Click(object sender, EventArgs e)
        {
            //PopupConditions_Detail();


            //if (checkClient_StartEnd_DateTime(txtJobID.Text.Trim()))
            //{
            //    MessageBox.Show("วันที่ออกงาน... มากกว่าเวลาที่เข้างาน..." + Environment.NewLine +
            //          "กรุณาตรวจสอบ เวลาเครื่อง Client ที่เมนู (1) Time Sync", "ผลการตรวจสอบ");
            //    return;
            //}

            if (RadioButtonList_JobStatus1.Checked == false && RadioButtonList_JobStatus2.Checked == false)
            {
                MessageBox.Show("กรุณาเลือกบันทึกงานค้าง หรือ ปิดงาน", "ผลการตรวจสอบ");
                return;
            }

            //20170216======================================================== 
            DateTime? srvTime = null;
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            {

                srvTime = dc
                   .ExecuteQuery<DateTime>("SELECT GETDATE()")
                   .First();

                ////if (srvTime != null)
                ////{
                ////    Server_date = srvTime.Value;
                ////}
                ////else
                ////{
                ////    MessageBox.Show("กรุณาเชื่อมต่อ Internet ก่อนการบันทึกปิดงาน", "ผลการตรวจสอบ");
                ////    return;
                ////}
            }
            //===========================================================

            if (txtResolve_Detail.Text == string.Empty)
            {

                MessageBox.Show("กรุณาป้อนข้อมูล หมายเหตุอื่นๆ", "ผลการตรวจสอบ");
                txtResolve_Detail.Focus();
                return;
            }

            if (cobProblemType.SelectedIndex == -1)
            {
                MessageBox.Show("กรุณาเลือกข้อมูล Problem Type", "ผลการตรวจสอบ");
                cobProblemType.Focus();
                return;
            }

            if (editMode == "job_edit")
            {
                #region job_edit


                if (MessageBox.Show("คุณต้องการแก้ไขข้อมูลใบ JOB ใช่หรือไม่???...", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (checkPartRemaiCloseJob())
                    {
                        //========================================//
                        //บันทึกการแก้ไข ข้อมูลJOB
                        //========================================//
                        EditJOB();

                        //20150629toolStripButton_Print.Enabled = true;
                        chkEnableSVReport(txtJobID.Text.Trim());
                        btFTRPrint.Enabled = true;

                    }
                }

                #endregion
            }














        }

        private void toolStripText_txtFind_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {


                toolStripButton_FindStation_Click(null, null);
            }
        }
        private void invoke_Progress(string strMsg)
        {
            try
            {
                m_fmProgress.lblDescription.Invoke(
                   (MethodInvoker)delegate ()
                   {
                       m_fmProgress.lblDescription.Text = strMsg;
                   }
                   );
            }
            catch (System.Exception)
            { }
        }

        private void newProgressbar()
        {
            m_fmProgress = new ProgressBar.FormProgress();
            backgroundWorker1.RunWorkerAsync();
            System.Threading.Thread.Sleep(3000);
        }

        private void closeProgress()
        {
            if (m_fmProgress != null)
            {
                m_fmProgress.Invoke(
                    (MethodInvoker)delegate ()
                    {
                        m_fmProgress.Hide();
                        m_fmProgress.Dispose();
                        m_fmProgress = null;
                    }
                );
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            m_fmProgress.Activate();
            m_fmProgress.BringToFront();
            m_fmProgress.TopMost = true;
            m_fmProgress.ShowDialog();
        }

        //public class ComboBoxItem
        //{
        //    public string Value;
        //    public string Text;

        //    public ComboBoxItem(string val, string text) { Value = val; Text = text; }

        //    public override string ToString() { return Text; }
        //}

        public class SPLIT_Part_ERP
        {
            public string ERP_SPA_ID { get; set; }
            public string ERP_TRA_ID { get; set; }
            public string ERP_PartName { get; set; }
        }

        private void toolStripComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (UserInfo.ConnectMode == "0")
            {
                if (toolStripComboBox1.Text == "บันทึก ออก/ปิดงาน")
                    editMode = "job_add";
                else
                    editMode = "job_edit";

            }
            else
            {
                toolStripComboBox1.Enabled = false;
                editMode = "job_edit";
            }



            butttonEnable(true, true, true, false, true);

            //20150629
            //chkEnableSVReport(txtJobID.Text.Trim());
            //--====
            //20170601 แก้ไขย้อนหลัง ไม่อนุญาตให้ print chkEnableSVReport(txtJobID.Text.Trim());
            toolStripButton_Print.Enabled = false;
            //--====

        }

        private void toolStripButton_Exit_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Abort;
            this.Close();
        }

        private void toolStripButton_Cancel_Click(object sender, EventArgs e)
        {
            JOBOnline_Load(null, null);


            tmpEndDate = "";
            tmpEndTime = "";

            lblIsContract.Visible = false;
            txtContract.Visible = false;
        }
        DateTime WarrantDate;
        private void pUpSNOld_Click(object sender, EventArgs e)
        {
            //txtModel.Focus();
            Finding.Find_SNOnline f = new SMSMINI.Transaction.Finding.Find_SNOnline();
            f.Text = "ค้นหา S/N Old...";
            f.SN_Old = txtSNOld.Text;//20141223"";//20141202 txtSNOld.Text;
            f.STAID = tmpSTAID;
            f.SN_New = null;
            f.lsSNSelec = lsSNSelect;
            if (f.ShowDialog() == DialogResult.OK)
            {

                if (f.SN_Old != "ไม่ระบุ...")
                {
                    txtSNOld.Text = f.SN_Old;
                    txtFixAssetNoOld.Text = f.FANoOld;

                    FCAT_ID = f.FCAT_ID;
                    FBA_ID = f.FBA_ID;
                    FMD_ID = f.FMD_ID;

                    LOC_ID = f.LOC_ID;

                    //if (f.Location != "--" && !string.IsNullOrEmpty(f.Location))
                    //    txtModel.Text = f.Location;

                    //20141203
                    cbxLocation.SelectedValue = LOC_ID;// f.LOC_ID;

                    if (f.MODEL != "--" && !string.IsNullOrEmpty(f.MODEL))
                        txtModel.Text = f.MODEL;

                    WarrantDate = f.WarrantDate;
                    lblWarranty.Visible = true;

                    txtSNNew.Text = f.SN_Old; //20141223 default S/N old ใน new


                    //20141224
                    IsFound = f._IsFound;
                    if (IsFound == true)
                    {
                        cbxLocation.Enabled = false;
                    }/*else{
                        cbxLocation.Enabled = true;
                    }*/

                    if (WarrantDate > DateTime.Now)
                    {
                        lblWarranty.Text = "(อยู่ในประกัน)";
                        if (f.CheckGauge > 0)
                        {
                            MessageBox.Show("S/N [" + txtSNOld.Text + "] นี้" + Environment.NewLine +
                                "รับประกันค่าตรวจสอบมาตรวัด",
                                "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    else
                    {
                        lblWarranty.Text = "(หมดประกัน)";
                    }
                }
                else
                {
                    txtSNOld.Text = "ไม่ระบุ...";
                    cbxLocation.Enabled = true;

                    ////20150114 ส่งค่ากลับ
                    FCAT_ID = 0;
                    FBA_ID = 0;
                    FMD_ID = 0;

                    //====20161117===
                    txtSNNew.Text = "ไม่ระบุ...";
                    txtFixAsetNoNew.Text = null;
                    txtModel.Text = null;
                    //====20161117===
                }
                txtSNNew.Focus();
            }
            else
            {
                FCAT_ID = 0;
                FBA_ID = 0;
                FMD_ID = 0;

                txtSNOld.Text = "None";
                cbxLocation.Enabled = true;

            }
        }

        private void pUpSNNew_Click(object sender, EventArgs e)
        {
            if (txtSNOld.Text == string.Empty)
            {
                MessageBox.Show("กรุณาป้อนข้อมูล S/N Old ก่อน..." + Environment.NewLine +
                "เนื่องจาก ต้องเปลี่ยน S/N ที่อยู่ในประเภทอุปกรณ์เดียวกันเท่านั้น..." + Environment.NewLine +
                "กรุณาตรวจสอบ...", "ผลการตรวจสอบ...", MessageBoxButtons.OK);
                txtSNOld.Focus();
                return;
            }

            if (txtSNNew.Text == string.Empty)
            {
                MessageBox.Show("กรุณาป้อนข้อมูล S/N New ก่อน..." + Environment.NewLine +
                   "เนื่องจาก ต้องเปลี่ยน S/N ที่อยู่ในประเภทอุปกรณ์เดียวกันเท่านั้น..." + Environment.NewLine +
                   "กรุณาตรวจสอบ...", "ผลการตรวจสอบ...", MessageBoxButtons.OK);
                txtSNNew.Focus();
                return;
            }

            //if (txtSNNew.Text == string.Empty && txtSNOld.Text != "ไม่ระบุ...")
            //{
            //    MessageBox.Show("กรุณาป้อนข้อมูล S/N New ก่อน..." + Environment.NewLine +
            //           "เนื่องจาก ต้องเปลี่ยน S/N ที่อยู่ในประเภทอุปกรณ์เดียวกันเท่านั้น..." + Environment.NewLine +
            //           "กรุณาตรวจสอบ...", "ผลการตรวจสอบ...", MessageBoxButtons.OK);
            //    txtSNNew.Focus();
            //    return;
            //}



            //if (txtSNOld.Text != "ไม่ระบุ..." )//20141216
            //{
            //    if (txtSNNew.Text == string.Empty)
            //    { }
            //}

            FindSNNew();
        }


        private void FindSNNew()
        {
            Finding.Find_SN f = new SMSMINI.Transaction.Finding.Find_SN();
            f.Text = "ค้นหา S/N New...";
            f.SN_Old = null;
            f.FCAT_ID = FCAT_ID;
            f.SN_New = txtSNNew.Text;//20141223 "";//20141202 txtSNNew.Text;
            f.lsSNSelec = lsSNSelect;
            if (f.ShowDialog() == DialogResult.OK)
            {
                //txtSNNew.Text = f.SN_New;
                //txtFixAsetNoNew.Text = f.FANoNew;
                //txtModel.Text = f.MODEL;
                //txtModel.Focus();

                //--20161117
                if (f.SN_New != "ไม่ระบุ...")
                {
                    txtSNNew.Text = f.SN_New;
                    txtFixAsetNoNew.Text = f.FANoNew;
                    txtModel.Text = f.MODEL;
                    txtModel.Focus();
                }
                else
                {
                    txtSNNew.Text = "ไม่ระบุ...";
                    txtFixAsetNoNew.Text = f.FANoNew;
                    txtModel.Text = f.MODEL;
                    txtModel.Focus();
                }
                //---20161117
            }
            else
                txtSNNew.Text = "None";
        }

        private void btClearData_Click(object sender, EventArgs e)
        {
            txtModel.Text = string.Empty;
            txtSNOld.Text = string.Empty;
            txtModel.Text = string.Empty;
            txtFailure.Text = string.Empty;
            txtSNNew.Text = string.Empty;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Finding.FindFailureCode f = new SMSMINI.Transaction.Finding.FindFailureCode();
                string fail = "";
                f.TmpFailureCode = txtFailure.Text.Trim();
                f.TmpjobType = lbl_JobType.Text.Trim();
                if (f.ShowDialog() == DialogResult.OK)
                {
                    fail = f.TmpFailureCode;
                    if (fail != "")
                    {
                        lblFailureCode.Text = f.TmpFCodeID;
                        txtFailure.Text = fail;
                        setcob_Failure_Action(lblFailureCode.Text);

                        comboBox_PoitFail.Focus();
                    }
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("ข้อผิดพลาด " + ex.Message, "เกิดข้อผิดพลาด");
            }
        }
        private void setcob_Failure_Action(string FCodeID)
        {
            if (UserInfo.ConnectMode == "0")
                strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
            else
                strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;

            try
            {
                string _typeID = UserInfo.Van_Type;
                string sql = "";
                sql = " SELECT [ATF_ID],[ATF_ID] +': '+[Action_Failure] as [Action_Failure] FROM [Failure_Action] Where [FAI_ID] = '" + FCodeID + "' or [FAI_ID]='0000000' Order by [Priority]";

                SqlDataReader dr = JaSqlHelper.ExecuteReader(strConn, CommandType.Text, sql);
                DataTable dt;

                if (dr.HasRows)
                {
                    //comboBox_PoitFail
                    dt = new DataTable();
                    dt.Load(dr);
                    cob_Failure_Action.BeginUpdate();
                    cob_Failure_Action.ValueMember = "ATF_ID";
                    cob_Failure_Action.DisplayMember = "Action_Failure";
                    cob_Failure_Action.DataSource = dt;
                    cob_Failure_Action.EndUpdate();
                    cob_Failure_Action.SelectedIndex = -1;



                }
                dr.Close();
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("เกิดข้อผิดพลาด " + ex.Message, "เกิดข้อผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btSelectPointFail_Click(object sender, EventArgs e)
        {
            try
            {
                if (comboBox_PoitFail.SelectedIndex == -1)
                {
                    comboBox_PoitFail.Focus();
                    MessageBox.Show("กรุณาเลือก จุดเสีย", "ผลการตรวจสอบ");
                    return;
                }

                if (lbl_JobType.Text.Trim() == "ตู้จ่าย(D)")
                {
                    //if (txtModel.Text == string.Empty || txtModel.Text == "None")
                    //{
                    //    MessageBox.Show("กรุณาป้อนข้อมูล Location...[ห้ามใส่ None]", "ผลการตรวจสอบ");
                    //    txtModel.Focus();
                    //    txtModel.SelectAll();
                    //    return;
                    //}


                    //txtSNOld.Text = (txtSNOld.Text == "" ? "None" : txtSNOld.Text);
                    //txtSNNew.Text = (txtSNNew.Text == "" ? "None" : txtSNNew.Text);


                    ////20141224 เอาไว้ป้องการการไม่ระบุ SN
                    //if ((txtSNOld.Text != string.Empty || txtSNOld.Text != "None") && (txtSNNew.Text == string.Empty || txtSNNew.Text == "None" || txtSNNew.Text == "ไม่ระบุ..."))
                    //{
                    //    MessageBox.Show("กรุณาป้อนข้อมูล S/N New ", "ผลการตรวจสอบ");
                    //    txtSNNew.Focus();
                    //    txtSNNew.SelectAll();
                    //    return;
                    //}
                    //if ((txtSNNew.Text != string.Empty || txtSNNew.Text != "None") && (txtSNOld.Text == string.Empty || txtSNOld.Text == "None" || txtSNOld.Text == "ไม่ระบุ..."))
                    //{
                    //    MessageBox.Show("กรุณาป้อนข้อมูล S/N New ", "ผลการตรวจสอบ");
                    //    txtSNOld.Focus();
                    //    txtSNOld.SelectAll();
                    //    return;
                    //}



                    if (txtSNOld.Text == string.Empty || txtSNOld.Text == "None")
                    {
                        MessageBox.Show("กรุณาป้อนข้อมูล S/N Old...[ห้ามใส่ None]", "ผลการตรวจสอบ");
                        txtSNOld.Focus();
                        txtSNOld.SelectAll();
                        return;
                    }

                    if (txtSNNew.Text == string.Empty || txtSNNew.Text == "None")
                    {
                        MessageBox.Show("กรุณาป้อนข้อมูล S/N New...[ห้ามใส่ None]", "ผลการตรวจสอบ");
                        txtSNNew.Focus();
                        txtSNNew.SelectAll();
                        return;
                    }


                    if (txtModel.Text == string.Empty || txtSNNew.Text == "None")
                    {
                        if (txtSNOld.Text != "ไม่ระบุ..." && txtSNNew.Text != "ไม่ระบุ...")//20141216
                        {
                            MessageBox.Show("กรุณาป้อนข้อมูล Model...[ห้ามใส่ None]", "ผลการตรวจสอบ");
                            txtModel.Focus();
                            return;
                        }

                    }

                    //20141225
                    //if (cob_Failure_Action_Solving.SelectedValue.ToString() != "99" )//&& string.IsNullOrEmpty(cob_SolvingByWI.SelectedValue.ToString()))
                    //20150318  if (string.IsNullOrEmpty(cob_SolvingByWI.SelectedValue.ToString()) )
                    if (cob_SolvingByWI.SelectedIndex == -1)
                    {
                        MessageBox.Show("กรุณาระบุ WI", "ผลการตรวจสอบ");
                        cob_SolvingByWI.Focus();
                        return;
                    }


                    ////เลขรวมลิตร:

                    ///*ไม่ลอค
                    //    39	งาน วิเคราะห์ ATG
                    //    BD	งาน โปรเจ็ค (ENS) 
                    // */
                    //if (txtWorkType.Text.Split(':')[0] != "39" && cob_TrnType.SelectedValue.ToString() != "BD")
                    //{
                    //    //20141223
                    //    //ไม่ล็อค งานตู้จ่าย ที่เป็น 015 เครื่องเติมลม,F22 other
                    //    if (comboBox_PoitFail.SelectedValue.ToString() != "015" && comboBox_PoitFail.SelectedValue.ToString() != "F22")
                    //    {

                    //        var ff = new PopStartLiteMessage();
                    //        ff.ShowDialog();

                    //        if (txtStartLite.Text == string.Empty || txtStartLite.Text == "0")
                    //        {

                    //            MessageBox.Show("กรุณาป้อนข้อมูล เลขรวมลิตร [ก่อน]...", "ผลการตรวจสอบ");
                    //            txtStartLite.Focus();
                    //            return;
                    //        }

                    //        if (txtEndtLite.Text == string.Empty || txtEndtLite.Text == "0")
                    //        {

                    //            MessageBox.Show("กรุณาป้อนข้อมูล เลขรวมลิตร [หลัง]...", "ผลการตรวจสอบ");
                    //            txtEndtLite.Focus();
                    //            return;
                    //        }
                    //    }
                    //    else
                    //    {
                    //        txtStartLite.Text = "0";
                    //        txtEndtLite.Text = "0";
                    //    }
                    //}

                    //=======================================================
                    //เลขรวมลิตร:
                    //ล็อค งาน A1-B4

                    if (new string[] { "016", "017", "018", "019", "020", "021", "022", "023" }.Contains(comboBox_PoitFail.SelectedValue.ToString()))
                    {

                        var ff = new PopStartLiteMessage();
                        ff.ShowDialog();

                        if (txtStartLite.Text == string.Empty || txtStartLite.Text == "0")
                        {

                            MessageBox.Show("กรุณาป้อนข้อมูล เลขรวมลิตร [ก่อน]...", "ผลการตรวจสอบ");
                            txtStartLite.Focus();
                            return;
                        }

                        if (txtEndtLite.Text == string.Empty || txtEndtLite.Text == "0")
                        {

                            MessageBox.Show("กรุณาป้อนข้อมูล เลขรวมลิตร [หลัง]...", "ผลการตรวจสอบ");
                            txtEndtLite.Focus();
                            return;
                        }
                    }
                    else
                    {
                        txtStartLite.Text = "0";
                        txtEndtLite.Text = "0";
                    }
                    //}
                    //else
                    //{ 
                    //    txtStartLite.Text = "0";
                    //    txtEndtLite.Text = "0";                    
                    //}

                }// if (lbl_JobType.Text.Trim() == "ตู้จ่าย(D)")
                else
                {
                    if (txtSNOld.Text == string.Empty)
                    {

                        MessageBox.Show("กรุณาป้อนข้อมูล S/N...", "ผลการตรวจสอบ");
                        txtSNOld.Focus();
                        return;
                    }

                    if (txtModel.Text == string.Empty)
                    {

                        MessageBox.Show("กรุณาป้อนข้อมูล txtModel...", "ผลการตรวจสอบ");
                        txtModel.Focus();
                        return;
                    }
                }


                if (lblFailureCode.Text == string.Empty || lblFailureCode.Text == "FailureCode")
                {

                    MessageBox.Show("กรุณาป้อนข้อมูล อาการเสีย..." + Environment.NewLine, "ผลการตรวจสอบ");
                    txtFailure.Focus();
                    txtFailure.SelectAll();
                    return;
                }

                //ห้ามป้อนข้อมูลจุดเสีย+failureCode+Location ซ้ำ 
                if (checkFailureCode(txtModel.Text, lblFailureCode.Text, comboBox_PoitFail.SelectedValue.ToString()))
                {
                    MessageBox.Show("คุณป้อนข้อมูล {Location:,FailureCode และ จุดเสีย:} ซ้ำ กรุณาป้อนใหม่...",
                        "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtModel.Focus();
                    txtModel.SelectAll();
                    return;
                }
                if (Convert.ToDouble(txtStartLite.Text) > Convert.ToDouble(txtEndtLite.Text))
                {
                    txtEndtLite.Focus();
                    MessageBox.Show("เลขรวมลิตร: หลัง ต้องมากกว่า ก่อน", "ผลการตรวจสอบ",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                txtStartLite.Text = txtStartLite.Text == "" ? "0" : txtStartLite.Text;
                txtEndtLite.Text = txtEndtLite.Text == "" ? "0" : txtEndtLite.Text;
                txtEndtLite.Text = txtEndtLite.Text == "" ? "0" : txtEndtLite.Text;

                if (txtFailure.Text == "")
                {
                    txtFailure.Focus();
                    MessageBox.Show("กรุณาป้อนข้อมูล อาการเสีย...", "ผลการตรวจสอบ");
                    return;
                }

                if (lsSNCancel.Count() > 0)
                {
                    var snCal = lsSNCancel
                        .Where(t => t.POI_ID == comboBox_PoitFail.SelectedValue.ToString() &&
                        t.SNNew == txtSNNew.Text.Trim() &&
                        t.SNold == txtSNOld.Text.Trim()).FirstOrDefault();
                    if (snCal != null)
                        lsSNCancel.Remove(snCal);
                }



                int xrow = 0;

                DataRow dr = dtFailure.NewRow();
                dr["NO."] = xrow;/*0*/
                dr["รหัสจุดเสีย"] = comboBox_PoitFail.SelectedValue.ToString();/*0*/
                dr["จุดเสีย"] = comboBox_PoitFail.Text.Trim();/*0*/
                dr["อาการเสีย"] = txtFailure.Text.Trim();/*0*/
                dr["S/N(Old)"] = txtSNOld.Text.Trim();/*0*/
                dr["FixAssetNo"] = txtFixAssetNoOld.Text.Trim();/*0*/

                dr["S/N(New)"] = txtSNNew.Text.Trim();/*0*/
                dr["FixAssetNo_New"] = txtFixAsetNoNew.Text.Trim();/*0*/

                dr["FCAT_ID"] = FCAT_ID;/*0*/
                dr["FBA_ID"] = FBA_ID;/*9*/
                dr["FMD_ID"] = FMD_ID;/*10*/

                dr["Location"] = cbxLocation.SelectedValue;//20141204  txtModel.Text.Trim();/*11*/
                dr["Model"] = txtModel.Text.Trim();/*12*/
                dr["StartLiter"] = txtStartLite.Text.Trim();/*13*/
                dr["EndLiter"] = txtEndtLite.Text.Trim();/*14*/
                dr["LiterTest"] = txtLiterTest.Text.Trim();/*15*/
                dr["FAI_ID"] = lblFailureCode.Text;

                //20141020
                dr["PRT_ID"] = cobOilType.SelectedValue; //"";
                //20141127ยกเลิก dr["EQP_ID"] = cob_Equipment_Position.SelectedValue; //"";
                dr["ATF_ID"] = cob_Failure_Action.SelectedValue;  //"";
                dr["FAS_ID"] = cob_Failure_Action_Solving.SelectedValue; //"";
                dr["SolvingByWI"] = cob_SolvingByWI.SelectedValue;//20141224 txtSolvingWI.Text;//"";


                dr["IsCancel"] = "0";
                dr["IsDownload"] = "N";

                dtFailure.Rows.Add(dr);

                lsSNSelect.Add(new SNCancel
                {
                    POI_ID = comboBox_PoitFail.SelectedValue.ToString(),
                    SNold = txtSNOld.Text.Trim(),
                    SNNew = txtSNNew.Text.Trim()
                });

                FailureBinding();


                ClearTextFail();
                dataGridView_FailPoint.Enabled = true;

                countFailureNO();

                comboBox_PoitFail.Focus();
            }

            catch (System.Exception ex)
            {
                MessageBox.Show("ข้อผิดพลาด " + ex.Message, "เกิดข้อผิดพลาด");
            }
        }

        private bool checkFailureCode(string txtLocation, string lblFailureCode, string comboBox_PoitFail)
        {
            for (int i = 0; i <= dataGridView_FailPoint.RowCount - 1; i++)
            {
                string _Location = dataGridView_FailPoint["Location", i].Value.ToString();
                string _FailureCode = dataGridView_FailPoint["FAI_ID", i].Value.ToString();
                string _cobPoitFail = dataGridView_FailPoint["รหัสจุดเสีย", i].Value.ToString();

                if ((txtLocation == _Location) && (lblFailureCode == _FailureCode) && (comboBox_PoitFail == _cobPoitFail))
                    return true;

            }

            return false;
        }


        private void countFailureNO()
        {
            for (int i = 0; i <= dataGridView_FailPoint.RowCount - 1; i++)
            {
                dataGridView_FailPoint["NO.", i].Value = i + 1;
            }
        }

        private void dataGridView_FailPoint_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                if (e.ColumnIndex == 0)//ยกเลิกข้อมูล
                {
                    #region ยกเลิกข้อมูล
                    try
                    {
                        if (disableCalFNO(e.RowIndex))
                        {
                            MessageBox.Show("คุณไม่สามารถยกเลิกรายการจุดเสียนี้ได้" + Environment.NewLine +
                            "เนื่องจากมีอะไหล่อยู่ กรุณายกเลิกรายการอะไหล่ก่อน", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }

                        if (dataGridView_FailPoint["IsDownload", e.RowIndex].Value.ToString() == "Y")
                        {
                            if (dataGridView_FailPoint["IsCancel", e.RowIndex].Value.ToString() == "1")
                                dataGridView_FailPoint["IsCancel", e.RowIndex].Value = "0";
                            else
                            {
                                if (MessageBox.Show("ไม่สามารถยกเลิกเพื่อแก้ไขข้อมูลได้ เนื่องจาก Download จาก Server" + Environment.NewLine +
                                    "คุณต้องการระบุสถานะยกเลิก ใช่หรือไม่", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                    dataGridView_FailPoint["IsCancel", e.RowIndex].Value = dataGridView_FailPoint["IsCancel", e.RowIndex].Value.ToString() == "0" ? "1" : "0";
                                setDataGridViewFailPointStyle();
                            }
                        }
                        else
                        {


                            if (MessageBox.Show("คุณต้องการยกเลิกข้อมูล ใช่หรือไม่???...", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {

                                tmpCalPoitFailID = dataGridView_FailPoint["รหัสจุดเสีย", e.RowIndex].Value.ToString();
                                tmpCalSN = dataGridView_FailPoint["S/N(New)", e.RowIndex].Value.ToString();
                                //tmpCalSN = dataGridView_FailPoint["S/N(Old)", e.RowIndex].Value.ToString();

                                tmpCalFAI_ID = dataGridView_FailPoint["FAI_ID", e.RowIndex].Value.ToString();


                                tmpFAIID = dataGridView_FailPoint["FAI_ID", e.RowIndex].Value.ToString();
                                lblFailureCode.Text = tmpFAIID;

                                if (dataGridView_FailPoint["FCAT_ID", e.RowIndex].Value != DBNull.Value)
                                    FCAT_ID = Convert.ToInt32(dataGridView_FailPoint["FCAT_ID", e.RowIndex].Value);

                                if (dataGridView_FailPoint["FBA_ID", e.RowIndex].Value != DBNull.Value)
                                    FBA_ID = Convert.ToInt32(dataGridView_FailPoint["FBA_ID", e.RowIndex].Value);

                                if (dataGridView_FailPoint["FMD_ID", e.RowIndex].Value != DBNull.Value)
                                    FMD_ID = Convert.ToInt32(dataGridView_FailPoint["FMD_ID", e.RowIndex].Value);

                                string _snNew = string.Empty;
                                string _snOld = string.Empty;

                                if (dataGridView_FailPoint["S/N(New)", e.RowIndex].Value != null)
                                    _snNew = dataGridView_FailPoint["S/N(New)", e.RowIndex].Value.ToString();

                                if (dataGridView_FailPoint["S/N(Old)", e.RowIndex].Value != null)
                                    _snOld = dataGridView_FailPoint["S/N(Old)", e.RowIndex].Value.ToString();

                                lsSNCancel.Add(new SNCancel
                                {
                                    POI_ID = tmpCalPoitFailID,
                                    SNNew = _snNew,
                                    SNold = _snOld
                                }
                                );



                                if (lsSNSelect.Count() > 0)
                                {
                                    var canSNSele = lsSNSelect
                                        .Where(t => t.POI_ID == tmpCalPoitFailID &&
                                                t.SNNew == _snNew &&
                                                t.SNold == _snOld).FirstOrDefault();
                                    if (canSNSele != null)
                                        lsSNSelect.Remove(canSNSele);
                                }

                                GridViewFailtoTextBox(e);
                                dtFailure.Rows[e.RowIndex].Delete();


                                FailureBinding();

                                countFailureNO();
                            }

                        }
                    }
                    catch (System.Exception ex)
                    {
                        MessageBox.Show("ข้อผิดพลาด " + ex.Message, "เกิดข้อผิดพลาด");
                    }

                    if (dataGridView_FailPoint.RowCount == 0)
                        groupBox3.Enabled = false;

                    #endregion

                }
                else if (e.ColumnIndex == 1)//เพิ่มอะไหล่
                {
                    string _pf = dataGridView_FailPoint["รหัสจุดเสีย", e.RowIndex].Value.ToString();

                    //tmpSN = dataGridView_FailPoint["S/N(New)", e.RowIndex].Value.ToString();
                    //S/N(Old)
                    tmpSN = dataGridView_FailPoint["S/N(Old)", e.RowIndex].Value.ToString();
                    tmpLocation = dataGridView_FailPoint["Location", e.RowIndex].Value.ToString();
                    tmpFAIID = dataGridView_FailPoint["FAI_ID", e.RowIndex].Value.ToString();

                    //20141021
                    //20141127ยกเลิก lbEQP_ID.Text = dataGridView_FailPoint["EQP_ID", e.RowIndex].Value.ToString();
                    //20141127ยกเลิก int vEQP_ID = int.Parse(dataGridView_FailPoint["EQP_ID", e.RowIndex].Value.ToString());

                    //20141127ยกเลิก
                    /*string sqlEQ = "";
                    sqlEQ = " SELECT EQP_ID,Equipment_Position FROM [Equipment_Position] WHERE  EQP_ID =" + vEQP_ID + " ";
                    SqlDataReader drEQ = JaSqlHelper.ExecuteReader(strConn, CommandType.Text, sqlEQ);
                   
                        if (drEQ.HasRows)
                        {
                            while (drEQ.Read())
                            {
                                
                                //20141127ยกเลิก  txtEquipment_Position.Text = drEQ["Equipment_Position"].ToString();
                            }
                        }
                     */



                    //using (DAL.SMSManage.SMSManageDataContext db = new SMSManageDataContext())
                    //{
                    //    var q2 = (from t in db.equ where t.JOB_ID == jobid orderby t.RecDate descending select t).FirstOrDefault();

                    //    txtProblem_Detail.Text = q2.Problem_Detail ?? "";
                    //}

                    this.tabControl1.SelectedTab = tabPage2;
                    cob_PoitFail0.SelectedValue = _pf.Trim();

                    groupBox3.Enabled = true;
                    setDataGridViewStyle();
                }

            }
        }
        private bool disableCalFNO(int rowIndex)
        {
            string _locat;
            string _pf;

            for (int i = 0; i <= dataGridView_Part.RowCount - 1; i++)
            {
                if (dataGridView_Part["IsCancel", i].Value.ToString() == "0")
                {
                    _locat = dataGridView_Part["Location", i].Value.ToString();
                    _pf = dataGridView_Part["รหัสจุดเสีย", i].Value.ToString();

                    for (int x = 0; x <= dataGridView_FailPoint.RowCount - 1; x++)
                    {
                        if (x == rowIndex &&
                            _locat == dataGridView_FailPoint["Location", x].Value.ToString() &&
                            _pf == dataGridView_FailPoint["รหัสจุดเสีย", x].Value.ToString())
                            return true;
                    }
                }
            }

            return false;

        }

        private void GridViewFailtoTextBox(DataGridViewCellEventArgs e)
        {
            comboBox_PoitFail.SelectedValue = dataGridView_FailPoint.Rows[e.RowIndex].Cells["รหัสจุดเสีย"].Value.ToString();
            txtSNOld.Text = dataGridView_FailPoint.Rows[e.RowIndex].Cells["S/N(Old)"].Value.ToString();
            txtFixAssetNoOld.Text = dataGridView_FailPoint.Rows[e.RowIndex].Cells["FixAssetNo"].Value.ToString();


            txtSNNew.Text = dataGridView_FailPoint.Rows[e.RowIndex].Cells["S/N(New)"].Value.ToString();
            txtFixAsetNoNew.Text = dataGridView_FailPoint.Rows[e.RowIndex].Cells["FixAssetNo_New"].Value.ToString();

            //20141214  txtModel.Text = dataGridView_FailPoint.Rows[e.RowIndex].Cells["Location"].Value.ToString();
            string tmpLOC = "";
            // if (string.IsNullOrEmpty(dataGridView_FailPoint.Rows[e.RowIndex].Cells["Location"].Value.ToString()))
            if (string.IsNullOrEmpty(dataGridView_FailPoint.Rows[e.RowIndex].Cells["Location"].Value.ToString()) || dataGridView_FailPoint.Rows[e.RowIndex].Cells["Location"].Value.ToString() == "None")
            {
                tmpLOC = "0";
            }
            else
            {
                tmpLOC = dataGridView_FailPoint.Rows[e.RowIndex].Cells["Location"].Value.ToString();
            }
            cbxLocation.SelectedValue = Convert.ToUInt32(tmpLOC);//Convert.ToInt32(dataGridView_FailPoint.Rows[e.RowIndex].Cells["Location"].Value.ToString() == null ? "0" : dataGridView_FailPoint.Rows[e.RowIndex].Cells["Location"].Value.ToString());

            //20141215 Convert.ToInt32(dataGridView_FailPoint.Rows[e.RowIndex].Cells["Location"].Value.ToString());
            txtModel.Text = dataGridView_FailPoint.Rows[e.RowIndex].Cells["Model"].Value.ToString();
            txtFailure.Text = dataGridView_FailPoint.Rows[e.RowIndex].Cells["อาการเสีย"].Value.ToString();

            txtStartLite.Text = dataGridView_FailPoint.Rows[e.RowIndex].Cells["StartLiter"].Value.ToString();
            txtEndtLite.Text = dataGridView_FailPoint.Rows[e.RowIndex].Cells["EndLiter"].Value.ToString();

            txtLiterTest.Text = dataGridView_FailPoint.Rows[e.RowIndex].Cells["LiterTest"].Value.ToString();

            //20141021
            cobOilType.SelectedValue = dataGridView_FailPoint.Rows[e.RowIndex].Cells["PRT_ID"].Value;
            //20141127ยกเลิก cob_Equipment_Position.SelectedValue = dataGridView_FailPoint.Rows[e.RowIndex].Cells["EQP_ID"].Value;
            //lblFailureCode
            setcob_Failure_Action(lblFailureCode.Text);

            cob_Failure_Action.SelectedValue = dataGridView_FailPoint.Rows[e.RowIndex].Cells["ATF_ID"].Value;
            cob_Failure_Action_Solving.SelectedValue = dataGridView_FailPoint.Rows[e.RowIndex].Cells["FAS_ID"].Value;
            //20141224 txtSolvingWI.Text = dataGridView_FailPoint.Rows[e.RowIndex].Cells["SolvingByWI"].Value.ToString();
            cob_SolvingByWI.SelectedValue = dataGridView_FailPoint.Rows[e.RowIndex].Cells["SolvingByWI"].Value;

        }

        private void dataGridView_FailPoint_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {

            //if (e.ColumnIndex == 9)
            //    dataGridView_FailPoint[9, e.RowIndex].Value =
            //        Convert.ToDouble(dataGridView_FailPoint[9, e.RowIndex].Value).ToString("#,##0.00");
            //if (e.ColumnIndex == 10)
            //    dataGridView_FailPoint[10, e.RowIndex].Value =
            //        Convert.ToDouble(dataGridView_FailPoint[10, e.RowIndex].Value).ToString("#,##0.00");


            if (e.ColumnIndex == 15)//StartLiter
                dataGridView_FailPoint[15, e.RowIndex].Value =
                    Convert.ToDouble(dataGridView_FailPoint[15, e.RowIndex].Value).ToString("#,##0.00");

            if (e.ColumnIndex == 16)//EndLiter
                dataGridView_FailPoint[16, e.RowIndex].Value =
                    Convert.ToDouble(dataGridView_FailPoint[16, e.RowIndex].Value).ToString("#,##0.00");




            //if (e.ColumnIndex == 10)// Cell "EndLiter"
            if (e.ColumnIndex == 16)// Cell "EndLiter"
            {

                if (Convert.ToDouble(dataGridView_FailPoint["StartLiter",
                    e.RowIndex].Value.ToString()) > Convert.ToDouble(dataGridView_FailPoint["EndLiter",
                    e.RowIndex].Value.ToString()))
                {
                    MessageBox.Show("เลขรวมลิตร: หลังทดสอบ ต้องมากกว่า ก่อนทดสอบ", "ผลการตรวจสอบ", MessageBoxButtons.OK);
                    return;
                }

                if (Convert.ToDouble(dataGridView_FailPoint["StartLiter", e.RowIndex].Value.ToString()) > 0 && Convert.ToDouble(dataGridView_FailPoint["EndLiter", e.RowIndex].Value.ToString()) > 0)
                    dataGridView_FailPoint["LiterTest", e.RowIndex].Value =
                        (Convert.ToDouble(dataGridView_FailPoint["EndLiter", e.RowIndex].Value.ToString()) -
                        Convert.ToDouble(dataGridView_FailPoint["StartLiter", e.RowIndex].Value.ToString())).ToString("#,##0.00");
            }
        }

        private void dataGridView_FailPoint_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            dataGridView_FailPoint["NO.", e.RowIndex].ReadOnly = true;
            dataGridView_FailPoint["รหัสจุดเสีย", e.RowIndex].ReadOnly = true;
            dataGridView_FailPoint["จุดเสีย", e.RowIndex].ReadOnly = true;

            dataGridView_FailPoint["อาการเสีย", e.RowIndex].ReadOnly = false;
            dataGridView_FailPoint["S/N(Old)", e.RowIndex].ReadOnly = false;
            dataGridView_FailPoint["S/N(New)", e.RowIndex].ReadOnly = false;
            dataGridView_FailPoint["Location", e.RowIndex].ReadOnly = false;
            dataGridView_FailPoint["Model", e.RowIndex].ReadOnly = false;

            dataGridView_FailPoint["อาการเสีย", e.RowIndex].Style.BackColor = Color.White;
            //dataGridView_FailPoint["S/N(Old)", e.RowIndex].Style.BackColor = Color.White;
            //dataGridView_FailPoint["S/N(New)", e.RowIndex].Style.BackColor = Color.White;
            //dataGridView_FailPoint["Location", e.RowIndex].Style.BackColor = Color.White;
            //dataGridView_FailPoint["Model", e.RowIndex].Style.BackColor = Color.White;

            dataGridView_FailPoint["StartLiter", e.RowIndex].ReadOnly = false;
            dataGridView_FailPoint["EndLiter", e.RowIndex].ReadOnly = false;
            dataGridView_FailPoint["LiterTest", e.RowIndex].ReadOnly = true;

            dataGridView_FailPoint["FAI_ID", e.RowIndex].ReadOnly = true;
            dataGridView_FailPoint["IsCancel", e.RowIndex].ReadOnly = true;
            dataGridView_FailPoint["IsDownload", e.RowIndex].ReadOnly = true;

            dataGridView_FailPoint["NO.", e.RowIndex].Style.BackColor = Color.LightGray;
            dataGridView_FailPoint["รหัสจุดเสีย", e.RowIndex].Style.BackColor = Color.LightGray;
            dataGridView_FailPoint["จุดเสีย", e.RowIndex].Style.BackColor = Color.LightGray;

            dataGridView_FailPoint["S/N(Old)", e.RowIndex].Style.BackColor = Color.White;
            dataGridView_FailPoint["S/N(New)", e.RowIndex].Style.BackColor = Color.White;
            dataGridView_FailPoint["Location", e.RowIndex].Style.BackColor = Color.White;
            dataGridView_FailPoint["Model", e.RowIndex].Style.BackColor = Color.White;


            dataGridView_FailPoint["StartLiter", e.RowIndex].Style.BackColor = Color.White;
            dataGridView_FailPoint["EndLiter", e.RowIndex].Style.BackColor = Color.White;
            dataGridView_FailPoint["LiterTest", e.RowIndex].Style.BackColor = Color.LightGray;

            dataGridView_FailPoint["FAI_ID", e.RowIndex].Style.BackColor = Color.LightGray;

            dataGridView_FailPoint["IsCancel", e.RowIndex].Style.BackColor = Color.LightGray;
            dataGridView_FailPoint["IsDownload", e.RowIndex].Style.BackColor = Color.LightGray;

        }

        private void dataGridView_FailPoint_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dataGridView_FailPoint.CurrentCell is DataGridViewCheckBoxCell)
                dataGridView_FailPoint.CommitEdit(DataGridViewDataErrorContexts.Commit);

        }

        private void txtPartNo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;

                try
                {
                    DateTime openjob = Convert.ToDateTime(Convert.ToDateTime(txtOpenJOB.Text).ToShortDateString());

                    Finding.FindSparePartOnline f = new SMSMINI.Transaction.Finding.FindSparePartOnline();

                    f.TmpParameter = txtPartNo.Text.Trim();

                    f.TmpDopenJOB = openjob;
                    f.TmpGopID = tmpGOPId;
                    f.TmpGopName = tmpGopName;

                    f.tmpJobType = tmpJobType;
                    f.tmpTYP_ID1 = tmpTYP_ID1;

                    f.tmpContractNo = _contractNo;

                    //f.SO_NO = txtJobSO.Text;
                    f.JOBNo = txtJobID.Text;

                    if (txtProjectNO.Text != "")
                        f.TmpProjectNo = txtProjectNO.Text.Trim();

                    string partNo = "";

                    string TmpPartName = "";
                    if (f.ShowDialog() == DialogResult.OK)
                    {
                        partNo = f.TmpPartNo;
                        tmpSPA_ID = f.TmpPartNo;
                        TmpPartName = f.TmpPartName;

                        lblERP_orderline_id.Text = f.ERP_orderline_id.ToString();

                        if (partNo != "")
                        {
                            setPartDetail(partNo, TmpPartName);



                            //=====20161114======
                            strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;

                            //using (var dc = new SMSManageDataContext())
                            using (var dc = new SMSManageDataContext(strConn))
                            {
                                var qReferPage = (from j in dc.JOBs
                                                  from s in dc.Stations
                                                      ///from jds in dc.JOB_Detail_Spares
                                                  from con in dc.Conf_Conditions
                                                  where j.STA_ID == s.STA_ID &&
                                                  //j.JOB_ID == jds.JOB_ID &&
                                                  //jds.SPA_ID == con.SPA_ID  &&
                                                  j.ContractNo == con.ContractNo &&
                                                  s.GOP_ID == con.GOP_ID &&
                                                  j.TYP_ID == con.TYP_ID &&
                                                  // j.TYP_ID1 == con.TYP_ID1 &&
                                                  j.JOB_ID == txtJobID.Text.Trim() &&
                                                  con.SPA_ID == partNo
                                                  select con).FirstOrDefault();

                                if (qReferPage != null)
                                {
                                    txtReferPage.Text = qReferPage.ReferPage;
                                }
                                else
                                {
                                    txtReferPage.Text = "";
                                }


                            }
                            //=================
                        }

                        //20141121 
                        con_ERP_StationCharge.SelectedIndex = con_ERP_StationCharge.FindString("บริษัทน้ำมัน");

                        //เก็บเงินสดหน้าสถานี
                        if (_IsStationCharge == true)
                        {

                            con_ERP_StationCharge.SelectedIndex = con_ERP_StationCharge.FindString("สถานี");
                            //cob_TrnType.SelectedValue = "IN";
                            cob_TrnType.SelectedIndex = cob_TrnType.FindString("[IN] Invoice");

                            chIsPrices.Checked = true;//พิมพ์ราคาออก SV
                        }


                        if (txtPriceList.Text == "0" || txtPriceList.Text == "0.00")
                        {
                            //using (var dc = new SMSManageDataContext())
                            using (var dc = new SMSManageDataContext(strConn))
                            {
                                var q = dc.SpareParts.Where(t => t.SPA_ID == txtPartNo.Text.Trim())
                                    .FirstOrDefault();

                                if (q != null)
                                {
                                    if (q.PricePerUnit > 0)
                                    {
                                        txtPriceList.Text = q.PricePerUnit.Value.ToString();
                                    }
                                }
                            }
                        }

                        //IsIsCloseJobCheckSN //20140904

                        using (var dc = new SMSManageDataContext(strConn))
                        {
                            var q = dc.SpareParts.Where(t => t.SPA_ID == txtPartNo.Text.Trim() && t.IsCloseJobCheckSN == true)
                                .FirstOrDefault();

                            if (q != null)
                            {
                                //popUp_SN
                                string tmpSNPart;
                                //Finding.Find_SNPart fprt = new SMSMINI.Transaction.Finding.Find_SNPart();
                                var fprt = new SMSMINI.Transaction.Finding.Find_SNPart();

                                fprt.fTmpPartNo = txtPartNo.Text.Trim();

                                var result = fprt.ShowDialog();
                                if (result == DialogResult.OK)
                                {
                                    //=====
                                    tmpSNPart = fprt.SNpart;
                                    var qSN = dc.SpareParts_SerialNumbers.Where(t => t.SPA_ID == txtPartNo.Text.Trim() && t.SerialNumber == tmpSNPart)
                                    .FirstOrDefault();
                                    if (qSN != null)
                                    {
                                        txtPartSerialNumberOld.Text = qSN.SerialNumber.ToString();

                                        ////20140925
                                        //txtPartSerialNumberOld.Enabled = false;

                                        //20141112
                                        txtPartSerialNumberOld.Enabled = true;

                                        //20140925 if (DateTime.Now <= qSN.ExpiryDate)//อยู่ในประกัน
                                        DateTime chkGetDate = dc.ExecuteQuery<DateTime>("SELECT GETDATE()").First();
                                        if (chkGetDate <= qSN.ExpiryDate)
                                        {
                                            cob_TrnType.SelectedValue = "WA";
                                        }

                                    }
                                }//if (f.ShowDialog() == DialogResult.OK)
                                else
                                {//20140925

                                    txtPartSerialNumberOld.Text = "";
                                    //20141112 txtPartSerialNumberOld.Enabled = false;
                                    txtPartSerialNumberOld.Enabled = true;
                                }

                            }//if (q != null)

                            //20141112
                            else
                            {
                                txtPartSerialNumberOld.Text = "";
                                txtPartSerialNumberOld.Enabled = true;
                            }

                        }


                    }//if (f.ShowDialog() == DialogResult.OK)


                }
                catch (System.Exception ex)
                {
                    MessageBox.Show("ข้อผิดพลาด " + ex.Message, "เกิดข้อผิดพลาด");
                }


                txtPartQT.Focus();
                //txtVAT.Text = _vat.ToString();
                sumVAT();
            }



            // con_ERP_StationCharge.SelectedIndex = con_ERP_StationCharge.FindString("บริษัทน้ำมัน");

        }
        private void sumVAT()
        {
            if (txtVAT.Text == "")
                txtVAT.Text = "0";
            supTotalPrices();
        }
        private void supTotalPrices()
        {
            double _discount = double.Parse(txtDiscQuarter.Text == "" ? "0" : txtDiscQuarter.Text);
            double _prices = double.Parse(txtPriceList.Text == "0.00" ? "0" : txtPriceList.Text);
            double quantity = double.Parse(txtPartQT.Text == "" ? "0" : txtPartQT.Text);

            if (_discount <= 0)//ไม่คิดส่วนลด
            {
                if (string.IsNullOrEmpty(txtVAT.Text) || txtVAT.Text == "0.00")//ไม่คิด VAT
                {
                    //txtPriceUnit.Text = Math.Round(_prices * quantity, tmpRounup).ToString("#,##0.00");
                    if (tmpRounup > 0)
                        txtPriceUnit.Text = Math.Round(_prices * quantity, tmpRounup).ToString("#,##0.00");
                    else
                        txtPriceUnit.Text = (_prices * quantity).ToString("#,##0.00");
                }
                else//คิด VAT
                {
                    double _total = (_prices * quantity);
                    double _tvat = Math.Round((Convert.ToDouble(txtVAT.Text) / 100) * _total, 2);

                    if (tmpRounup > 0)
                        txtPriceUnit.Text = Math.Round((_total + _tvat), tmpRounup).ToString("#,##0.00");
                    else
                        txtPriceUnit.Text = (_total + _tvat).ToString("#,##0.00");
                }

            }
            else//คิดส่วนลด
            {
                if (string.IsNullOrEmpty(txtVAT.Text) || txtVAT.Text == "0.00")//ไม่คิด VAT
                {
                    if (tmpRounup > 0)
                        txtPriceUnit.Text = Math.Round((_prices - ((_discount / 100) * _prices)) * quantity, tmpRounup).ToString("#,##0.00");
                    else
                        txtPriceUnit.Text = ((_prices - ((_discount / 100) * _prices)) * quantity).ToString("#,##0.00");

                }
                else//คิด VAT
                {
                    double _total = ((_prices - ((_discount / 100) * _prices)) * quantity);
                    double _tvat = Math.Round((Convert.ToDouble(txtVAT.Text) / 100) * _total, 2);

                    if (tmpRounup > 0)
                        txtPriceUnit.Text = Math.Round((_total + _tvat), tmpRounup).ToString("#,##0.00");
                    else
                        txtPriceUnit.Text = (_total + _tvat).ToString("#,##0.00");
                }
            }
        }
        private void btLoadSNNew_Click(object sender, EventArgs e)
        {
            //txtSNNew
            try
            {
                Cursor.Current = Cursors.AppStarting;
                var f = new SMSMINI.MasterData.FixedAsset_SerialNumber_Master();
                f.FFSN = txtSNNew.Text.Trim();

                if (f.ShowDialog() == DialogResult.OK)
                {
                    Cursor.Current = Cursors.Default;
                    FindSNNew();
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("เกิดผิดพลาดในการใช้งานเมนู ข้อมูล Fixed Asset SerialNumber Master " + Environment.NewLine
                    + ex.Message, "ข้อผิดพลาด");
                this.Show();
            }

        }

        private DataRow setPartDetail(DAL.SMSManage.SMSManageDataContext dc, DataRow dr, string _poid, string _pode, string radPartStatus, decimal vat, decimal dis, decimal qt, decimal pricePerUnit, int xrow, decimal _xtotal, decimal _xvat, string _cbIsCustomer, string _SNOld, string vReferPage)
        {
            dr = dtPart.NewRow();
            dr["รหัสจุดเสีย"] = _poid;
            dr["จุดเสีย"] = _pode;
            dr["NO."] = xrow + 1;
            //20140905
            dr["S/N Old"] = _SNOld;
            dr["Part no."] = tmpSPA_ID;//txtPartNo.Text;
            dr["Part"] = txtPartName.Text.Trim();
            dr["จำนวน"] = qt.ToString();

            dr["VAT"] = vat.ToString("#,##0.00");
            dr["FixAssetNo"] = txtFixAssetNo.Text.Trim();

            dr["PricesList"] = decimal.Parse(txtPriceList.Text).ToString("#,##0.00");

            dr["ราคา/หน่วย"] = pricePerUnit.ToString("#,##0.00");
            dr["รวม"] = _xtotal.ToString("#,##0.00");
            dr["ภาษี"] = Math.Round(_xvat, 2).ToString("#,##0.00");

            dr["รวมสุทธิ"] = Convert.ToDecimal(_xtotal + _xvat).ToString("#,##0.00");

            dr["Discount"] = dis.ToString("#,##0.00");

            dr["สถานะ"] = radPartStatus;
            dr["Type"] = cob_TrnType.SelectedValue.ToString();
            dr["Comment"] = txtPartComment.Text.Trim();

            dr["FAI_ID"] = tmpFAIID;
            dr["Location"] = tmpLocation;
            dr["S/N"] = tmpSN;
            dr["IsCancel"] = "0";
            dr["IsDownload"] = "N";

            dr["isPrices"] = (chIsPrices.Checked == true && cob_TrnType.SelectedValue.ToString() == "IN") ? "1" :
                checkConf_JOB_IS_PrintPrice(dc, txtJobID.Text.Trim(), cob_TrnType.SelectedValue.ToString());

            dr["ออกบิลในนาม"] = con_ERP_StationCharge.Text.Trim().ToString();


            bool ERP_SPA_IDSend = false;
            string ERP_SPA_ID = "";


            //var q1 = dc.SpareParts
            //            .Where(t => t.SPA_ID == tmpSPA_ID && t.ERP_SPA_ID != null && t.ERP_SPA_Split == false)
            //            .FirstOrDefault();

            //if (q1 != null)
            //{
            //    ERP_SPA_IDSend = true;
            //    ERP_SPA_ID = q1.ERP_SPA_ID;
            //}

            //if (ERP_SPA_IDSend)
            //{
            //    dr["ERP_SPA_ID"] = ERP_SPA_ID;
            //    dr["ERP_PartName"] = txtPartName.Text.Trim();
            //}
            //else
            //{
            //    dr["ERP_SPA_ID"] = null;
            //    dr["ERP_PartName"] = null;
            //}

            #region Part แทน

            if (!string.IsNullOrEmpty(_contractNo))
            {
                var q1 = (from ct in dc.Conf_Conditions
                          from sp in dc.SpareParts

                          where (ct.SPA_ID == sp.SPA_ID) &&
                              (ct.SPA_ID == tmpSPA_ID && ct.GOP_ID == tmpGOPId && ct.ContractNo == _contractNo && ct.TYP_ID == _job.TYP_ID) &&
                              (ct.ERP_SPA_ID != null && ct.ERP_TRA_ID == null)
                          select new SPLIT_Part_ERP
                          {
                              ERP_SPA_ID = ct.ERP_SPA_ID,
                              ERP_PartName = sp.SparePart1,
                              ERP_TRA_ID = ct.ERP_TRA_ID
                          }).FirstOrDefault();

                if (q1 != null)
                {
                    ERP_SPA_IDSend = true;
                    ERP_SPA_ID = q1.ERP_SPA_ID;
                }

                if (ERP_SPA_IDSend)
                {
                    dr["ERP_SPA_ID"] = q1.ERP_SPA_ID;
                    dr["ERP_PartName"] = txtPartName.Text.Trim();
                }
                else
                {
                    dr["ERP_SPA_ID"] = null;
                    dr["ERP_PartName"] = null;
                }
            }

            #endregion

            //อะไหล่ลูกค้า

            dr["อะไหล่ลูกค้า"] = _cbIsCustomer;

            //ERP_orderline_id
            dr["ERP_orderline_id"] = lblERP_orderline_id.Text;

            //20141021
            //20141127ยกเลิก dr["ตำแหน่งที่เปลี่ยน"] = _EQP_ID;

            //20161114
            dr["Page/Item"] = vReferPage;

            dtPart.Rows.Add(dr);
            return dr;
        }
        private void setPartDetail(string partNo, string TmpPartName)
        {
            ClearPartText();

            //***********************************//
            //เก็บเงินทุก Part ดีฟอลต์ TranType = IN
            cob_TrnType.SelectedValue = "IN";

            getPricesList(partNo, TmpPartName);


            var jobID = txtJobID.Text.Substring(4, 1).ToString();
            // if (jobID == "D") //งานตู้จ่าย //20160405 ไม่ lock ประเภท job เกี่ยวเนื่องงาน I
            // {//20160405 ไม่ lock ประเภท job เกี่ยวเนื่องงาน I
            check_FFA_Warrant(tmpSN, partNo);//เช็คอะไหล่ในประกัน

            var Trantype = cob_TrnType.SelectedValue.ToString();

            //20151104check_FFA_Price_Charges(tmpSN, partNo);//เช็คอะไหล่ในประกัน เก็บเงิน Part พิเศษ       
            check_FFA_Price_Charges(tmpSN, partNo, Trantype);

            //}//20160405 ไม่ lock ประเภท job เกี่ยวเนื่องงาน I

            //เช็คอะไหล่ ตามเงื่อนไขสัญญา
            string workTypID = "";
            //if (new string[] { "D", "S" }.Contains(jobID)) //20160405 ไม่ lock ประเภท job เกี่ยวเนื่องงาน I
            //{//เฉพาะ JOB ระบบ และ JOB ตู้จ่าย //20160405 ไม่ lock ประเภท job เกี่ยวเนื่องงาน I

            if (!string.IsNullOrEmpty(txtWorkType.Text))
                workTypID = txtWorkType.Text.Split(':')[0];

            if (!check_Conf_Conditions(partNo, workTypID))
            {
                // SMS MINI v.17.0.9.2014
                // ปรับปรุง
                //  งานนอกสัญญา Default TranType ตาม Part master
                DefaultTranTypeByPartMaster(partNo);

            }

            check_SpareParts_IsTravel(partNo, workTypID);
            //} //20160405 ไม่ lock ประเภท job เกี่ยวเนื่องงาน I

            //==20160622==part 19==IS==

            check_partIS(partNo, workTypID);
            //==20160622==part 19==IS==


            lockTranType();
        }


        private string checkConf_JOB_IS_PrintPrice(DAL.SMSManage.SMSManageDataContext dc, string jobID, string tranType)
        {
            //chIsPrices

            if (tranType == "IN")
            {
                var j = dc.JOBs.Where(t => t.JOB_ID == jobID).FirstOrDefault();
                if (j != null)
                {
                    var s = dc.Stations.Where(t => t.STA_ID == j.STA_ID).FirstOrDefault();
                    if (s != null)
                    {
                        var q = dc.Conf_JOB_IS_PrintPrices
                            .Where(t => t.GOP_ID == s.GOP_ID && t.TYP_ID == j.TYP_ID)
                            .FirstOrDefault();
                        if (q != null)
                        {
                            return (q.SVIsPrintPrice == true ? "1" : "0");
                        }
                    }
                }
            }

            return "0";
        }

        private void getPricesList(string partNo, string partName)
        {
            strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
            int roundup = 0;

            double _prices = 0;
            double _discount = 0;
            int quantity = 0;

            if (txtPartQT.Text == "0" || txtPartQT.Text == "")
                txtPartQT.Text = "1";

            quantity = int.Parse(txtPartQT.Text);

            txtPartNo.Text = partNo;
            tmpSPA_ID = partNo;
            txtPartName.Text = partName;
            txtPartQT.Focus();

            using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext(strConn))
            {
                string stGroupName = string.Empty;
                //ค้นหา Part ตามโปรเจ็กต์ ID

                if (!string.IsNullOrEmpty(_contractNo) && _contractNo != "null")
                {
                    #region Part ไม่อ้างอิงโปรเจ็กต์ มีเลขที่สัญญา

                    //ดึงราคาในสัญญ ตาม ....SPA_ID,TYP_ID,TYP_ID1
                    var conP = (from t in dc.Conf_Conditions
                                from sp in dc.SpareParts
                                where (t.SPA_ID == sp.SPA_ID &&
                                    t.SPA_ID == partNo &&
                                    t.GOP_ID == tmpGOPId &&
                                    t.TYP_ID == tmpJobType &&
                                    t.TYP_ID1 == tmpTYP_ID1) &&
                                    t.ContractNo == _contractNo
                                group t by t.SPA_ID
                                    into g
                                select new { PriceList = g.Max(t => t.PriceList) }).FirstOrDefault();

                    if (conP != null && conP.PriceList > 0)
                    {
                        #region Check Price Conf_Conditions

                        txtPriceList.Text = conP.PriceList.ToString("#,##0.00");
                        _prices = Convert.ToDouble(conP.PriceList);

                        txtPriceUnit.Text = (Convert.ToDouble(_prices) * quantity).ToString("#,##0.00");

                        #endregion
                    }
                    else
                    {

                        #region OLD

                        ////////#region Check Price List

                        ////////var q = (from t in dc.SpareParts where t.SPA_ID == partNo select t).FirstOrDefault();

                        ////////var qIsPrice = (from t in dc.SpareParts_IsPrices where t.SPA_ID == partNo && t.GOP_ID == tmpGOPId select t).FirstOrDefault();
                        ////////var stg = (from t in dc.Station_Groups where t.GOP_ID == tmpGOPId select t).FirstOrDefault();
                        ////////stGroupName = stg == null ? string.Empty : stg.GroupStation;

                        ////////DateTime openjob = Convert.ToDateTime(Convert.ToDateTime(txtOpenJOB.Text).ToShortDateString());

                        ////////var pri = (from sp in dc.SpareParts
                        ////////             where sp.SPA_ID == partNo
                        ////////           select new
                        ////////           {
                        ////////             sp.SPA_ID,
                        ////////             sp.PricePerUnit
                        ////////           }).FirstOrDefault();
                        ////////  if (pri != null)
                        ////////  {
                        ////////       if (qIsPrice != null)
                        ////////             {
                        ////////              #region มี Price List

                        ////////              txtPriceList.Text = Convert.ToDouble(pri.PricePerUnit).ToString("#,##0.00");//pri.Prices.ToString("#,##0.00");

                        ////////              txtDiscQuarter.Text = "0";
                        ////////              txtPriceUnit.Text = (Convert.ToDouble(_prices) * quantity).ToString("#,##0.00");

                        ////////              if (qIsPrice.IsPrices != null)
                        ////////                    chIsPrices.Checked = qIsPrice.IsPrices.Value;

                        ////////                #endregion

                        ////////              }
                        ////////              else
                        ////////              {
                        ////////                  #region ไม่มี PriceList
                        ////////                  MessageBox.Show("Part: [" + partNo + " " + partName + "] ไม่มี Price list" + Environment.NewLine +
                        ////////                                             "กล่มบริษัท: " + stGroupName + Environment.NewLine +
                        ////////                                             "ใช้ราคาจากข้อมูลหลัก (Master data) ของ part" + Environment.NewLine +
                        ////////                                             "โดยหมายเหตุ ระบุเป็น \"#Part นอก Prices\" ", "Part ไม่มี Price list", MessageBoxButtons.OK);

                        ////////                  _prices = Convert.ToDouble(q.PricePerUnit.Value == 0 ? 0 : q.PricePerUnit.Value);
                        ////////                  txtDiscQuarter.Text = "0";


                        ////////                  //txtPriceList.Text = _prices.ToString("#,##0.00");
                        ////////                  txtPartComment.Text = "#Part นอก Prices";

                        ////////                  txtPriceList.Text = "0";
                        ////////                  txtDiscQuarter.Text = "0";
                        ////////                  txtVAT.Text = "0";
                        ////////                  txtPriceUnit.Text = "0";
                        ////////                  #endregion

                        ////////              }



                        ////////      /* 
                        ////////       // ยกเลิก 20150429  var pri = (from pl in dc.SparePart_Prices_Lists
                        ////////                    from isp in dc.SpareParts_IsPrices
                        ////////                    from pq in dc.SparePart_Quarters
                        ////////                    from tg in dc.Station_Groups
                        ////////                    where (pl.PRI_ID == pq.PRI_ID) &&
                        ////////                         (pl.SPA_ID == isp.SPA_ID && tg.GOP_ID == isp.GOP_ID) &&
                        ////////                         (pq.GOP_ID == tg.GOP_ID) &&
                        ////////                         (pl.SPA_ID == partNo) &&
                        ////////                          tg.GOP_ID == tmpGOPId &&
                        ////////                          pq.StartDate <= openjob && pq.EndDate >= openjob
                        ////////                    select new
                        ////////                    {
                        ////////                        pl.SPA_ID,
                        ////////                        pq.localDiscount,
                        ////////                        pq.globalDiscount,
                        ////////                        pq.decRoundUp,
                        ////////                        pl.Prices,
                        ////////                        pq.Quarter
                        ////////                    }).FirstOrDefault();




                        ////////         if (pri != null)
                        ////////         {
                        ////////             #region มี Price List

                        ////////             roundup = Convert.ToInt32(pri.decRoundUp ?? 0);//ปัดเศษ
                        ////////             tmpRounup = roundup;

                        ////////             txtPriceList.Text = pri.Prices.ToString("#,##0.00");
                        ////////             _prices = Convert.ToDouble(pri.Prices);//Price list

                        ////////             if (qIsPrice != null)
                        ////////             {
                        ////////                 #region มี Price List

                        ////////                 if (qIsPrice.IsGlobalDiscount.Value != '2')
                        ////////                 {
                        ////////                     if (qIsPrice.IsGlobalDiscount.Value == '1')
                        ////////                         _discount = Convert.ToDouble(pri.globalDiscount);//ใช้ส่วนลดต่างประเทศ
                        ////////                     else
                        ////////                         _discount = Convert.ToDouble(pri.localDiscount);//ใช้ส่วนลดในประเทศ

                        ////////                     txtDiscQuarter.Text = _discount.ToString(); //ส่วนลด

                        ////////                     if (_discount <= 0)
                        ////////                         txtPriceUnit.Text = Math.Round(pri.Prices * quantity, tmpRounup).ToString("#,##0.00");
                        ////////                     else
                        ////////                         txtPriceUnit.Text = Math.Round((_prices - ((_discount / 100) * _prices)) * quantity, tmpRounup).ToString("#,##0.00");

                        ////////                 }
                        ////////                 else
                        ////////                 {
                        ////////                     txtDiscQuarter.Text = "0";
                        ////////                     if (tmpRounup > 0)
                        ////////                         txtPriceUnit.Text = Math.Round(Convert.ToDouble(_prices) * quantity, tmpRounup).ToString("#,##0.00");
                        ////////                     else
                        ////////                         txtPriceUnit.Text = (Convert.ToDouble(_prices) * quantity).ToString("#,##0.00");

                        ////////                 }// if (q.IsDiscount == true)

                        ////////                 if (qIsPrice.IsPrices != null)
                        ////////                     chIsPrices.Checked = qIsPrice.IsPrices.Value;

                        ////////                 #endregion

                        ////////             }
                        ////////             else
                        ////////             {
                        ////////                 #region ไม่มี PriceList
                        ////////                 MessageBox.Show("Part: [" + partNo + " " + partName + "] ไม่มี Price list" + Environment.NewLine +
                        ////////                                            "กล่มบริษัท: " + stGroupName + Environment.NewLine +
                        ////////                                            "ใช้ราคาจากข้อมูลหลัก (Master data) ของ part" + Environment.NewLine +
                        ////////                                            "โดยหมายเหตุ ระบุเป็น \"#Part นอก Prices\" ", "Part ไม่มี Price list", MessageBoxButtons.OK);

                        ////////                 _prices = Convert.ToDouble(q.PricePerUnit.Value == 0 ? 0 : q.PricePerUnit.Value);
                        ////////                 txtDiscQuarter.Text = "0";


                        ////////                 //txtPriceList.Text = _prices.ToString("#,##0.00");
                        ////////                 txtPartComment.Text = "#Part นอก Prices";

                        ////////                 txtPriceList.Text = "0";
                        ////////                 txtDiscQuarter.Text = "0";
                        ////////                 txtVAT.Text = "0";
                        ////////                 txtPriceUnit.Text = "0";
                        ////////                 #endregion

                        ////////             }

                        ////////             #endregion

                        ////////         } //end pri 
                        ////////                */
                        ////////  }//end pri 
                        ////////  else
                        ////////  {
                        ////////      #region ไม่มี PriceList
                        ////////      txtPriceList.Text = "0";
                        ////////      txtDiscQuarter.Text = "0";

                        ////////      _prices = Convert.ToDouble(q.PricePerUnit.Value == 0 ? 0 : q.PricePerUnit.Value);

                        ////////      //txtPriceList.Text = _prices.ToString("#,##0.00");

                        ////////      MessageBox.Show("Part: [" + partNo + " " + partName + "] ไม่มี Price list" + Environment.NewLine +
                        ////////                                "กล่มบริษัท: " + stGroupName + Environment.NewLine +
                        ////////                                "ใช้ราคาจากข้อมูลหลัก (Master data) ของ part" + Environment.NewLine +
                        ////////                                "โดยหมายเหตุ ระบุเป็น \"#Part นอก Prices\" ", "Part ไม่มี Price list", MessageBoxButtons.OK);

                        ////////      txtPartComment.Text = "#Part นอก Prices";

                        ////////      txtPriceList.Text = "0";
                        ////////      txtDiscQuarter.Text = "0";
                        ////////      txtVAT.Text = "0";
                        ////////      txtPriceUnit.Text = "0";
                        ////////      txtPartComment.Text = "#Part นอก Prices";

                        ////////      #endregion

                        ////////  }

                        ////////#endregion

                        #endregion

                        //ดึงราคาในสัญญา ตาม ....SPA_ID
                        var conP1 = (from t in dc.Conf_Conditions
                                     from sp in dc.SpareParts
                                     where (t.SPA_ID == sp.SPA_ID &&
                                         t.SPA_ID == partNo &&
                                         t.GOP_ID == tmpGOPId
                                         //t.TYP_ID == tmpJobType &&
                                         //t.TYP_ID1 == tmpTYP_ID1
                                         ) &&
                                         t.ContractNo == _contractNo
                                     group t by t.SPA_ID
                                         into g
                                     select new { PriceList = g.Max(t => t.PriceList) }).FirstOrDefault();

                        if (conP1 != null && conP1.PriceList > 0)
                        {
                            #region Check Price Conf_Conditions

                            txtPriceList.Text = conP1.PriceList.ToString("#,##0.00");
                            _prices = Convert.ToDouble(conP1.PriceList);

                            txtPriceUnit.Text = (Convert.ToDouble(_prices) * quantity).ToString("#,##0.00");

                            #endregion
                        }
                        else
                        { //ใช้ราคาใน sparePart
                            var priSP = (from sp in dc.SpareParts
                                         where sp.SPA_ID == partNo
                                         select new
                                         {
                                             sp.SPA_ID,
                                             sp.PricePerUnit
                                         }).FirstOrDefault();
                            if (priSP != null)
                            {

                                txtPriceList.Text = Convert.ToDouble(priSP.PricePerUnit).ToString("#,##0.00");
                                txtDiscQuarter.Text = "0";
                                txtPriceUnit.Text = (Convert.ToDouble(priSP.PricePerUnit) * quantity).ToString("#,##0.00");

                            }

                        }

                        #endregion
                    }//ไม่มีในสัญญา ,TYP_ID,TYP_ID1
                }//เลขที่สัญญา(ไม่)เป็น nulll
                else
                {  // if (!string.IsNullOrEmpty(_contractNo) && _contractNo != "null") เลขที่สัญญาเป็น nulll

                    #region Old

                    //////////  #region Part ไม่อ้างอิงโปรเจ็กต์ ไม่มี _contractNo

                    //////////  //20150504var conP = (from t in dc.Conf_Conditions
                    //////////  //            from sp in dc.SpareParts
                    //////////  //            where (t.SPA_ID == sp.SPA_ID &&
                    //////////  //                t.SPA_ID == partNo &&
                    //////////  //                t.GOP_ID == tmpGOPId &&
                    //////////  //                t.TYP_ID == tmpJobType &&
                    //////////  //                t.TYP_ID1 == tmpTYP_ID1)
                    //////////  //            select t).FirstOrDefault();
                    ////////// //=========================

                    //////////  //ใช้ราคามากสุดตามเงื่อนไข 20150504
                    ///////////*  var conP = (from t in dc.Conf_Conditions
                    //////////              from sp in dc.SpareParts
                    //////////              where (t.SPA_ID == sp.SPA_ID &&
                    //////////                  t.SPA_ID == partNo &&
                    //////////                  t.GOP_ID == tmpGOPId &&
                    //////////                  t.TYP_ID == tmpJobType &&
                    //////////                  t.TYP_ID1 == tmpTYP_ID1)
                    //////////              select
                    //////////                  t.PriceList
                    //////////              //t
                    //////////              ).FirstOrDefault();
                    //////////      //.Max(t => t.PriceList);
                    //////////             //.Max();*/

                    //////////  //var conP = (from t in dc.Conf_Conditions select t).Max();
                    //////////  //var conP = (from t in dc.Conf_Conditions
                    //////////  //            from sp in dc.SpareParts
                    //////////  //            where (t.SPA_ID == sp.SPA_ID &&
                    //////////  //                t.SPA_ID == partNo &&  
                    //////////  //                t.GOP_ID == tmpGOPId &&
                    //////////  //                t.TYP_ID == tmpJobType &&
                    //////////  //                t.TYP_ID1 == tmpTYP_ID1) 
                    //////////  //            //select t.PriceList
                    //////////  //            select new
                    //////////  //            {
                    //////////  //                   t.PriceList,
                    //////////  //                   t.SPA_ID,
                    //////////  //                   t.GOP_ID,
                    //////////  //                   t.TYP_ID,
                    //////////  //                   t.TYP_ID1
                    //////////  //               }

                    //////////  //            ).Max();


                    //////////  //var conP1 = from t in dc.Conf_Conditions
                    //////////  //            //join sp in dc.SpareParts on t.SPA_ID equals sp.SPA_ID
                    //////////  //            where //t.SPA_ID == sp.SPA_ID &&
                    //////////  //                t.SPA_ID == partNo &&
                    //////////  //                t.GOP_ID == tmpGOPId &&
                    //////////  //                t.TYP_ID == tmpJobType &&
                    //////////  //                t.TYP_ID1 == tmpTYP_ID1
                    //////////  //            select
                    //////////  //                t.PriceList;

                    //////////  //var conP = (from t in dc.Conf_Conditions
                    //////////  //            from sp in dc.SpareParts
                    //////////  //            where (t.SPA_ID == sp.SPA_ID &&
                    //////////  //                t.SPA_ID == partNo &&
                    //////////  //                t.GOP_ID == tmpGOPId &&
                    //////////  //                t.TYP_ID == tmpJobType &&
                    //////////  //                t.TYP_ID1 == tmpTYP_ID1)
                    //////////  //            select
                    //////////  //                t.PriceList
                    //////////  //    //t
                    //////////  //            )//.FirstOrDefault();
                    //////////  ////.Max(t => t.PriceList);
                    //////////  //.Max();



                    //////////  var conP = (from t in dc.Conf_Conditions
                    //////////              from sp in dc.SpareParts
                    //////////              where t.SPA_ID == sp.SPA_ID &&
                    //////////             t.SPA_ID == partNo &&
                    //////////             t.GOP_ID == tmpGOPId &&
                    //////////             t.TYP_ID == tmpJobType &&
                    //////////             t.TYP_ID1 == tmpTYP_ID1
                    //////////              group t by t.SPA_ID
                    //////////              into g
                    //////////               select new { PriceList = g.Max(t => t.PriceList) }).FirstOrDefault();


                    //////////  if (conP != null && conP.PriceList > 0)
                    //////////  {
                    //////////      #region Check Price Conf_Conditions

                    //////////      txtPriceList.Text = conP.PriceList.ToString("#,##0.00");
                    //////////      _prices = Convert.ToDouble(conP.PriceList);

                    //////////      txtPriceUnit.Text = (Convert.ToDouble(_prices) * quantity).ToString("#,##0.00");

                    //////////      #endregion
                    //////////  }
                    //////////  else
                    //////////  {
                    //////////      #region Check Price List

                    //////////      var q = (from t in dc.SpareParts where t.SPA_ID == partNo select t).FirstOrDefault();

                    //////////      //อะไหล่เก็บเงินหน้างาน
                    //////////      var qIsPrice = (from t in dc.SpareParts_IsPrices where t.SPA_ID == partNo && t.GOP_ID == tmpGOPId select t).FirstOrDefault();
                    //////////      var stg = (from t in dc.Station_Groups where t.GOP_ID == tmpGOPId select t).FirstOrDefault();
                    //////////      stGroupName = stg == null ? string.Empty : stg.GroupStation;

                    //////////      DateTime openjob = Convert.ToDateTime(Convert.ToDateTime(txtOpenJOB.Text).ToShortDateString());


                    //////////      var pri = (from sp in dc.SpareParts
                    //////////                 where sp.SPA_ID == partNo
                    //////////                 select new
                    //////////                 {
                    //////////                     sp.SPA_ID,
                    //////////                     sp.PricePerUnit
                    //////////                 }).FirstOrDefault();
                    //////////      if (pri != null)
                    //////////      {
                    //////////          if (qIsPrice != null)
                    //////////          {
                    //////////              #region มี Price List

                    //////////              txtPriceList.Text = Convert.ToDouble(pri.PricePerUnit).ToString("#,##0.00");//pri.Prices.ToString("#,##0.00");

                    //////////              txtDiscQuarter.Text = "0";
                    //////////              txtPriceUnit.Text = (Convert.ToDouble(_prices) * quantity).ToString("#,##0.00");

                    //////////              if (qIsPrice.IsPrices != null)
                    //////////                  chIsPrices.Checked = qIsPrice.IsPrices.Value;

                    //////////              #endregion

                    //////////          }
                    //////////          else
                    //////////          {
                    //////////              #region ไม่มี PriceList
                    //////////              MessageBox.Show("Part: [" + partNo + " " + partName + "] ไม่มี Price list" + Environment.NewLine +
                    //////////                                         "กล่มบริษัท: " + stGroupName + Environment.NewLine +
                    //////////                                         "ใช้ราคาจากข้อมูลหลัก (Master data) ของ part" + Environment.NewLine +
                    //////////                                         "โดยหมายเหตุ ระบุเป็น \"#Part นอก Prices\" ", "Part ไม่มี Price list", MessageBoxButtons.OK);

                    //////////              _prices = Convert.ToDouble(q.PricePerUnit.Value == 0 ? 0 : q.PricePerUnit.Value);
                    //////////              txtDiscQuarter.Text = "0";


                    //////////              //txtPriceList.Text = _prices.ToString("#,##0.00");
                    //////////              txtPartComment.Text = "#Part นอก Prices";

                    //////////              txtPriceList.Text = "0";
                    //////////              txtDiscQuarter.Text = "0";
                    //////////              txtVAT.Text = "0";
                    //////////              txtPriceUnit.Text = "0";
                    //////////              #endregion

                    //////////          }


                    //////////     /*

                    //////////     //201500429  ยกเลิก
                    //////////      var pri = (from pl in dc.SparePart_Prices_Lists
                    //////////                 from isp in dc.SpareParts_IsPrices
                    //////////                 from pq in dc.SparePart_Quarters
                    //////////                 from tg in dc.Station_Groups
                    //////////                 where (pl.PRI_ID == pq.PRI_ID) &&
                    //////////                      (pl.SPA_ID == isp.SPA_ID && tg.GOP_ID == isp.GOP_ID) &&
                    //////////                      (pq.GOP_ID == tg.GOP_ID) &&
                    //////////                      (pl.SPA_ID == partNo) &&
                    //////////                       tg.GOP_ID == tmpGOPId &&
                    //////////                       pq.StartDate <= openjob && pq.EndDate >= openjob
                    //////////                 select new
                    //////////                 {
                    //////////                     pl.SPA_ID,
                    //////////                     pq.localDiscount,
                    //////////                     pq.globalDiscount,
                    //////////                     pq.decRoundUp,
                    //////////                     pl.Prices,
                    //////////                     pq.Quarter
                    //////////                 }).FirstOrDefault();




                    //////////      if (pri != null)
                    //////////      {
                    //////////          roundup = Convert.ToInt32(pri.decRoundUp ?? 0);//ปัดเศษ
                    //////////          tmpRounup = roundup;

                    //////////          txtPriceList.Text = pri.Prices.ToString("#,##0.00");
                    //////////          _prices = Convert.ToDouble(pri.Prices);//Price list

                    //////////          if (qIsPrice != null)
                    //////////          {
                    //////////              if (qIsPrice.IsGlobalDiscount.Value != '2')
                    //////////              {
                    //////////                  if (qIsPrice.IsGlobalDiscount.Value == '1')
                    //////////                      _discount = Convert.ToDouble(pri.globalDiscount);//ใช้ส่วนลดต่างประเทศ
                    //////////                  else
                    //////////                      _discount = Convert.ToDouble(pri.localDiscount);//ใช้ส่วนลดในประเทศ

                    //////////                  txtDiscQuarter.Text = _discount.ToString(); //ส่วนลด

                    //////////                  if (_discount <= 0)
                    //////////                      txtPriceUnit.Text = Math.Round(pri.Prices * quantity, tmpRounup).ToString("#,##0.00");
                    //////////                  else
                    //////////                      txtPriceUnit.Text = Math.Round((_prices - ((_discount / 100) * _prices)) * quantity, tmpRounup).ToString("#,##0.00");

                    //////////              }
                    //////////              else
                    //////////              {
                    //////////                  txtDiscQuarter.Text = "0";
                    //////////                  if (tmpRounup > 0)
                    //////////                      txtPriceUnit.Text = Math.Round(Convert.ToDouble(_prices) * quantity, tmpRounup).ToString("#,##0.00");
                    //////////                  else
                    //////////                      txtPriceUnit.Text = (Convert.ToDouble(_prices) * quantity).ToString("#,##0.00");

                    //////////              }// if (q.IsDiscount == true)

                    //////////          }
                    //////////          else
                    //////////          {
                    //////////              MessageBox.Show("Part: [" + partNo + " " + partName + "] ไม่มี Price list" + Environment.NewLine +
                    //////////                                         "กล่มบริษัท: " + stGroupName + Environment.NewLine +
                    //////////                                         "ใช้ราคาจากข้อมูลหลัก (Master data) ของ part" + Environment.NewLine +
                    //////////                                         "โดยหมายเหตุ ระบุเป็น \"#Part นอก Prices\" ", "Part ไม่มี Price list", MessageBoxButtons.OK);

                    //////////              _prices = Convert.ToDouble(q.PricePerUnit.Value == 0 ? 0 : q.PricePerUnit.Value);
                    //////////              txtDiscQuarter.Text = "0";


                    //////////              //txtPriceList.Text = _prices.ToString("#,##0.00");
                    //////////              txtPartComment.Text = "#Part นอก Prices";

                    //////////              txtPriceList.Text = "0";
                    //////////              txtDiscQuarter.Text = "0";
                    //////////              txtVAT.Text = "0";
                    //////////              txtPriceUnit.Text = "0";

                    //////////          }
                    //////////      //} //end pri
                    //////////          */
                    //////////  }//end pri
                    //////////      else
                    //////////      {
                    //////////          txtPriceList.Text = "0";
                    //////////          txtDiscQuarter.Text = "0";

                    //////////          _prices = Convert.ToDouble(q.PricePerUnit.Value == 0 ? 0 : q.PricePerUnit.Value);

                    //////////          MessageBox.Show("Part: [" + partNo + " " + partName + "] ไม่มี Price list" + Environment.NewLine +
                    //////////             "กล่มบริษัท: " + stGroupName + Environment.NewLine +
                    //////////             "ใช้ราคาจากข้อมูลหลัก (Master data) ของ part" + Environment.NewLine +
                    //////////             "โดยหมายเหตุ ระบุเป็น \"#Part นอก Prices\" ", "Part ไม่มี Price list", MessageBoxButtons.OK);

                    //////////          txtPartComment.Text = "#Part นอก Prices";

                    //////////          txtPriceList.Text = "0";
                    //////////          txtDiscQuarter.Text = "0";
                    //////////          txtVAT.Text = "0";
                    //////////          txtPriceUnit.Text = "0";
                    //////////          txtPartComment.Text = "#Part นอก Prices";

                    //////////      }

                    //////////      #endregion
                    //////////  }

                    //////////  #endregion

                    #endregion

                    //ดึงราคากลาง ในตาราง SpareParts
                    var priSP = (from sp in dc.SpareParts
                                 where sp.SPA_ID == partNo
                                 select new
                                 {
                                     sp.SPA_ID,
                                     sp.PricePerUnit
                                 }).FirstOrDefault();
                    if (priSP != null)
                    {

                        txtPriceList.Text = Convert.ToDouble(priSP.PricePerUnit).ToString("#,##0.00");
                        txtDiscQuarter.Text = "0";
                        txtPriceUnit.Text = (Convert.ToDouble(priSP.PricePerUnit) * quantity).ToString("#,##0.00");

                    }

                }


            }//using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext(ConnectionManager.GetConnectionString("0")))

        }//fn


        private void check_FFA_Warrant(string tmpSN, string partNo)
        {
            strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
            //20151104 เพิ่มตัวแปรเข้า fn
            string vTrantype = null;
            //0 ตรวจสอบสัญญา
            var dnow = DateTime.Today.Date;
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext(strConn))
            {
                var ffa = dc.FFixedAsset_Station_Details
                   .Where(t => t.STA_ID == tmpSTAID && t.SerialNumber == tmpSN && t.WarrantDate > dnow)
                    //.Where(t => t.SerialNumber == tmpSN && t.WarrantDate > dnow)
                    .FirstOrDefault();
                if (ffa != null)
                {
                    cob_TrnType.SelectedValue = "WA";// WA=อะไหล่อยู่ในประกัน
                    vTrantype = "WA";

                    var spw = dc.FFixedAsset_Part_Warranties
                        .Where(t => t.SPA_ID == partNo)
                        .FirstOrDefault();

                    string partMess = "";
                    if (spw != null)
                    {
                        int? partTypeWar = 0;
                        switch (spw.PartType)
                        {
                            case '1': //PartType 1=จำนวน ปี รับประกันของ มือจ่าย       
                                partTypeWar = ffa.PartType1;
                                partMess = "มือจ่าย";
                                break;

                            case '2': //PartType 2=จำนวน ปี รับประกันของ สายจ่าย
                                partTypeWar = ffa.PartType1;
                                partMess = "สายจ่าย";
                                break;

                            case '3': //PartType 3=จำนวน ปี รับประกันของ เบรคอะเว 
                                partTypeWar = ffa.PartType1;
                                partMess = "เบรคอะเว";
                                break;

                            default: break;
                        }

                        if (dnow > ffa.InstallDate.Value.AddYears(partTypeWar.Value))//Part หมดประกัน
                        {
                            cob_TrnType.SelectedValue = "IN";//เก็บเงิน
                            vTrantype = "IN";
                            MessageBox.Show(
                                "Serial Number [" + tmpSN + "] นี้ อยู่ในประกัน..." + Environment.NewLine +
                                "แต่ Part:" + partNo + "[" + partMess + "] หมดประกัน" + Environment.NewLine +
                                "TranType:=IN", "ผลการตรวจสอบ...", MessageBoxButtons.OK);
                        }
                        else
                        {

                            MessageBox.Show("Serial Number [" + tmpSN + "] นี้" + Environment.NewLine +
                               "อยู่ในประกัน TranType:=WA", "ผลการตรวจสอบ...", MessageBoxButtons.OK);
                        }

                    }
                    else
                    {
                        MessageBox.Show("Serial Number [" + tmpSN + "] นี้" + Environment.NewLine +
                            "อยู่ในประกัน TranType:=WA", "ผลการตรวจสอบ...", MessageBoxButtons.OK);
                    }
                }
            }

        }

        private void check_FFA_Price_Charges(string tmpSN, string partNo, string vTrantype)
        {
            strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
            //1...
            //Check ตู้จ่ายอยู่ในประกัน
            //2...
            //Check Part เก็บเงิน
            con_ERP_StationCharge.SelectedIndex = con_ERP_StationCharge.FindString("บริษัทน้ำมัน");

            using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext(strConn))
            {

                //0 ตรวจสอบสัญญา
                var dnow = DateTime.Today.Date;

                var con0 = dc.Conf_Contracts
                    .Where(t => t.ContractNo.Contains(_contractNo))
                    .FirstOrDefault();
                if (con0 != null)
                {
                    var con = dc.Conf_Contracts
                        .Where(t => t.StartDate.Date <= dnow && t.EndDate >= dnow &&
                            t.ContractNo.Contains(_contractNo))
                            .FirstOrDefault();
                    if (con == null)//สัญญาหมด
                        return;
                }

                //20151104ก่อนเปลี่ยนเงื่อนไขให้อิงตามตู้ในประกัน 
                //ยกเลิก var _Trantyp = "IN"; //จะยกไปไว้ใน flow ตู้ไม่อยู่ในประกัน
                var _Trantyp = vTrantype;

                //1... 
                //Check ตู้จ่ายอยู่ในประกัน
                var ffa = from t in dc.FFixedAsset_Station_Details
                          where t.STA_ID == tmpSTAID &&
                                t.SerialNumber == tmpSN &&
                                t.WarrantDate.Value > DateTime.Now.Date
                          select t;

                if (ffa.Count() > 0)//<==| ถ้าตู้จ่ายอยู่ในประกัน
                {

                    # region ถ้าตู้จ่ายอยู่ในประกัน
                    /*
                    //Check Part เก็บเงิน,<==| ตามสถานี
                    var priceCh = (from t in dc.Conf_PartPrice_Charges
                                   where t.STA_ID == tmpSTAID &&//<==| ตามสถานี
                                         t.GOP_ID == tmpGOPId &&
                                         t.SPA_ID == partNo
                                   select t).FirstOrDefault();

                    if (priceCh != null)//ถ้ามี Part เก็บค่าใช้จ่าย...
                    {
                        //cob_TrnType.SelectedValue = "IN";
                        //txtPriceList.Text = priceCh.PriceList.ToString("#,##0.00");

                        //====20150408============
                        //20150420 var _Trantyp = "IN";
                        if (priceCh.TRA_ID != null)
                            _Trantyp = priceCh.TRA_ID;

                        try
                        {
                            cob_TrnType.SelectedValue =_Trantyp;
                            txtPriceList.Text = priceCh.PriceList.ToString("#,##0.00");
                        }
                        catch (System.Exception )
                        {
                        }
                        //========================
                    }
                    else
                    {
                        //Check Part เก็บเงิน,<==| ตามบริษัท
                        var priceCh_stg = (from t in dc.Conf_PartPrice_Charges
                                           where t.GOP_ID == tmpGOPId &&//<==| ตามบริษัท
                                                 t.SPA_ID == partNo
                                           select t).FirstOrDefault();

                        if (priceCh_stg != null)//ถ้ามี Part เก็บค่าใช้จ่าย...
                        {
                            //====20150408============

                            //cob_TrnType.SelectedValue = "IN";
                            //txtPriceList.Text = priceCh_stg.PriceList.ToString("#,##0.00");

                            //var _Trantyp2 = "IN";
                            if (priceCh_stg.TRA_ID != null)
                                _Trantyp = priceCh_stg.TRA_ID;

                            try
                            {
                                cob_TrnType.SelectedValue = _Trantyp;//_Trantyp2;
                                txtPriceList.Text = priceCh_stg.PriceList.ToString("#,##0.00");
                            }
                            catch (System.Exception)
                            {
                            }
                            //=======================

                            */

                    //20150422
                    var priceCh = (from t in dc.Conf_PartPrice_Charges
                                   where t.SPA_ID == partNo
                                   select t).FirstOrDefault();

                    if (priceCh != null)//ถ้ามี Part เก็บค่าใช้จ่าย...
                    {
                        //cob_TrnType.SelectedValue = "IN";
                        //txtPriceList.Text = priceCh.PriceList.ToString("#,##0.00");

                        //====20150408============
                        //20150420 var _Trantyp = "IN";
                        if (priceCh.TRA_ID != null)
                            _Trantyp = priceCh.TRA_ID;

                        try
                        {
                            cob_TrnType.SelectedValue = _Trantyp;
                            //20150504 ให้ใช้ราคา getPriceList txtPriceList.Text = priceCh.PriceList.ToString("#,##0.00");
                        }
                        catch (System.Exception)
                        {
                        }
                        //========================

                    }
                    //else
                    //{
                    //    cob_TrnType.SelectedValue = "CS";
                    //    //txtPriceList.Text = "0.00";

                    //    //2...
                    //    //Check Part เก็บเงิน
                    //    //ตรวจสอบเงื่อนไข Part เก็บเงินสด
                    //    //check_Conf_Conditions(partNo);
                    //}


                    //20150420 
                    else
                    { //ถ้าไม่มีใน Conf_PartPrice_Charges ให้ตรวจสอบใน condition



                        //var vChkCond = (from t in dc.Conf_Conditions
                        //       where t.STA_ID == tmpSTAID &&//<==| ตามสถานี
                        //             t.GOP_ID == tmpGOPId &&
                        //             t.SPA_ID == partNo
                        //       select t).FirstOrDefault();

                        //===========================================




                        //===========================================

                        ////20150427 เช็คงานในสัญญาและนอกสัญญา
                        ////if (tmpTYP_ID1 == "GR") //ในสัญญา
                        ////{
                        ////    var vChkCond = dc.Conf_Conditions
                        ////                                .Where(t =>
                        ////                                    t.GOP_ID == tmpGOPId &&
                        ////                                    t.TYP_ID == tmpJobType &&
                        ////                                    t.TYP_ID1 == tmpTYP_ID1 &&
                        ////                                            t.SPA_ID == partNo &&
                        ////        t.ContractNo.Contains(_contractNo)
                        ////                               ).FirstOrDefault();

                        ////    if (vChkCond != null)//ถ้ามี Part เก็บค่าใช้จ่าย...
                        ////    {
                        ////        if (vChkCond.TRA_ID != null)
                        ////            _Trantyp = vChkCond.TRA_ID;

                        ////        try
                        ////        {
                        ////            cob_TrnType.SelectedValue = _Trantyp;//_Trantyp2;
                        ////            txtPriceList.Text = vChkCond.PriceList.ToString("#,##0.00");
                        ////        }
                        ////        catch (System.Exception)
                        ////        {
                        ////        }
                        ////    }
                        ////}
                        ////else if (tmpTYP_ID1 == "OU") //งานนอกสัญญา
                        ////{ 


                        ////}


                        //===========================================


                        /* 
                        var vChkCond = dc.Conf_Conditions
                                                 .Where(t =>
                                                            //t.GOP_ID == tmpGOPId &&
                                                            //t.TYP_ID == tmpJobType &&
                                                            //t.TYP_ID1 == tmpTYP_ID1 &&
                                                             t.SPA_ID == partNo //&&
                                                            //t.ContractNo.Contains(_contractNo)
                                                ).FirstOrDefault();

                            if (vChkCond != null)//ถ้ามี Part เก็บค่าใช้จ่าย...
                            {
                                  if (vChkCond.TRA_ID != null)
                                _Trantyp = vChkCond.TRA_ID;

                                try
                                {
                                    cob_TrnType.SelectedValue = _Trantyp;//_Trantyp2;
                                    txtPriceList.Text = vChkCond.PriceList.ToString("#,##0.00");
                                }
                                catch (System.Exception)
                                {
                                }


                        }
                    else
                        {

                                var vConfConD = (from t in dc.Conf_Conditions
                                                 where (t.GOP_ID == tmpGOPId) &&    //<==| ตามบริษัท
                                                       t.SPA_ID == partNo
                                                 select t).FirstOrDefault();

                                if (vConfConD != null)//ถ้ามี Part เก็บค่าใช้จ่าย...
                                {
                                      if (vConfConD.TRA_ID != null)
                                _Trantyp = vConfConD.TRA_ID;

                                try
                                {
                                    cob_TrnType.SelectedValue = _Trantyp;//_Trantyp2;
                                    txtPriceList.Text = vConfConD.PriceList.ToString("#,##0.00");
                                }
                                catch (System.Exception)
                                {
                                }
                            }

                    }//else {   //Conf_Conditions 
                   */
                        //======================20150504==================================
                        //20151104ก่อนเปลี่ยนเงื่อนไขให้อิงตามตู้ในประกัน 
                        //ยกเลิก _Trantyp ="CS";
                        _Trantyp = vTrantype;


                        try
                        {
                            cob_TrnType.SelectedValue = _Trantyp;//_Trantyp2;

                        }
                        catch (System.Exception)
                        {
                        }
                        //========================================================
                        /* ยกเลิก เพราะหากไม่มีใน   Conf_PartPrice_Charges ให้กำหนดเป็น  CS
                          if (_contractNo == null) //ไม่มีเลขที่สัญญา
                          {
                              var vChkCond = dc.Conf_Conditions
                                                   .Where(t =>
                                                              t.GOP_ID == tmpGOPId &&
                                                             t.TYP_ID == tmpJobType &&
                                                              t.TYP_ID1 == tmpTYP_ID1 &&
                                                               t.SPA_ID == partNo &&
                                  t.TRA_ID.Contains("IN")
                                                  )//.FirstOrDefault();
                              .OrderByDescending (r=>r.PriceList )
                              .FirstOrDefault();

                              if (vChkCond != null)//ถ้ามี Part เก็บค่าใช้จ่าย...
                              {
                                  if (vChkCond.TRA_ID != null)
                                      _Trantyp = vChkCond.TRA_ID;

                                  try
                                  {
                                      cob_TrnType.SelectedValue = _Trantyp;//_Trantyp2;
                                     // txtPriceList.Text = vChkCond.PriceList.ToString("#,##0.00");
                                  }
                                  catch (System.Exception)
                                  {
                                  }


                              }


                          }//ไม่มีเลขที่สัญญา
                          else
                          {//มีเลขที่สัญญา
                              var vChkCond = dc.Conf_Conditions
                                                   .Where(t =>
                                                              t.GOP_ID == tmpGOPId &&
                                                             t.TYP_ID == tmpJobType &&
                                                              t.TYP_ID1 == tmpTYP_ID1 &&
                                                               t.SPA_ID == partNo &&
                                  t.ContractNo.Contains(_contractNo)
                                                  ).FirstOrDefault();

                              if (vChkCond != null)//ถ้ามี Part เก็บค่าใช้จ่าย...
                              {
                                  if (vChkCond.TRA_ID != null)
                                      _Trantyp = vChkCond.TRA_ID;

                                  try
                                  {
                                      cob_TrnType.SelectedValue = _Trantyp;//_Trantyp2;
                                      // txtPriceList.Text = vChkCond.PriceList.ToString("#,##0.00");
                                  }
                                  catch (System.Exception)
                                  {
                                  }


                              }

                          }//ไม่มีเลขที่สัญญา
                          */
                    }//ถ้าไม่มีใน Conf_PartPrice_Charges ให้ตรวจสอบใน condition
                    #endregion
                } //if (ffa.Count() > 0)//<==| ถ้าตู้จ่ายอยู่ในประกัน
                else
                { //ถ้าตู้จ่ายไม่อยู่ในประกัน

                    //20150421
                    // cob_TrnType.SelectedValue = "IN";


                    #region ถ้าตู้จ่ายไม่อยู่ในประกัน
                    /*
                    ////2...
                    ////Check Part เก็บเงิน
                    ////ตรวจสอบเงื่อนไข Part เก็บเงินสด
                    ////check_Conf_Conditions(partNo);

                     
                     
                    //var ffa2 = from t in dc.FFixedAsset_Station_Details
                    //           from s in dc.Stations
                    //           from st in dc.Station_Groups
                    //           where (t.STA_ID == s.STA_ID &&
                    //                 s.GOP_ID == tmpGOPId) &&
                    //                 (t.SerialNumber == tmpSN &&
                    //                 t.WarrantDate.Value.Date >= DateTime.Now.Date)
                    //           select t;

                    //if (ffa2.Count() > 0)
                    //{
                    //    var _Trantyp = "IN";
                    //    //Check Part เก็บเงิน,<==| ตามสถานี
                    //    var priceCh = (from t in dc.Conf_PartPrice_Charges
                    //                   where t.STA_ID == tmpSTAID &&//<==| ตามสถานี
                    //                         t.GOP_ID == tmpGOPId &&
                    //                         t.SPA_ID == partNo
                    //                   select t).FirstOrDefault();

                    //    if (priceCh != null)//ถ้ามี Part เก็บค่าใช้จ่าย...
                    //    {

                    //       //20150408
                    //        //cob_TrnType.SelectedValue = "IN";
                    //       //txtPriceList.Text = priceCh.PriceList.ToString("#,##0.00");
                    //        //====20150408============
                    //        //20150420 var _Trantyp = "IN";
                    //        if (priceCh.TRA_ID != null)
                    //            _Trantyp = priceCh.TRA_ID;

                    //        try
                    //        {
                    //            cob_TrnType.SelectedValue = _Trantyp;
                    //            txtPriceList.Text = priceCh.PriceList.ToString("#,##0.00");
                    //        }
                    //        catch (System.Exception )
                    //        {
                    //        }
                    //        //========================
                    //    }
                    //    else
                    //    {
                    //        //Check Part เก็บเงิน,<==| ตามบริษัท
                    //        var priceCh_stg = (from t in dc.Conf_PartPrice_Charges
                    //                           where t.GOP_ID == tmpGOPId &&//<==| ตามบริษัท
                    //                                 t.SPA_ID == partNo
                    //                           select t).FirstOrDefault();

                    //        if (priceCh_stg != null)//ถ้ามี Part เก็บค่าใช้จ่าย...
                    //        {
                    //            //===20150408=============
                    //           //cob_TrnType.SelectedValue = "IN";
                    //            //txtPriceList.Text = priceCh_stg.PriceList.ToString("#,##0.00");

                    //            //20150420 var _Trantyp2 = "IN";
                    //            if (priceCh_stg.TRA_ID != null)
                    //                _Trantyp = priceCh_stg.TRA_ID;

                    //            try
                    //            {
                    //                cob_TrnType.SelectedValue = _Trantyp;//_Trantyp2;
                    //                txtPriceList.Text = priceCh_stg.PriceList.ToString("#,##0.00");
                    //            }
                    //            catch (System.Exception )
                    //            {
                    //            }
                    //            //==========================

                    //        }







                    //        //else
                    //        //{
                    //        //    cob_TrnType.SelectedValue = "CS";
                    //        //    //txtPriceList.Text = "0.00";

                    //        //    //2...
                    //        //    //Check Part เก็บเงิน
                    //        //    //ตรวจสอบเงื่อนไข Part เก็บเงินสด
                    //        //    //check_Conf_Conditions(partNo);
                    //        //}
                    //    }
                    //} //if (ffa2.Count() > 0)
                    ////else//หมดประกัน
                    ////{
                    ////    cob_TrnType.SelectedValue = "IN";
                    ////    //txtPriceList.Text = "0.00";
                    ////    //check_Conf_Conditions(partNo);
                    ////}
                    */

                    _Trantyp = "IN";//ตู้จ่ายไม่อยู่ในประกัน ต้องเก็บเงิน ยกลงมาจากด้านบน //20151104

                    //=============20150504=========================
                    if (!string.IsNullOrEmpty(_contractNo) && _contractNo != "null")
                    {
                        #region มีเลขที่สัญญา
                        var vConf = (from t in dc.Conf_Conditions
                                     from sp in dc.SpareParts
                                     where (t.SPA_ID == sp.SPA_ID &&
                                         t.SPA_ID == partNo &&
                                         t.GOP_ID == tmpGOPId &&
                                         t.TYP_ID == tmpJobType &&
                                         t.TYP_ID1 == tmpTYP_ID1) &&
                                         t.ContractNo == _contractNo
                                     select t).FirstOrDefault();

                        if (vConf != null)
                        {
                            if (vConf.TRA_ID != null)
                                _Trantyp = vConf.TRA_ID;

                            try
                            {
                                cob_TrnType.SelectedValue = _Trantyp;

                            }
                            catch (System.Exception)
                            {
                            }

                        }
                        else
                        {//อะไหล่ใน warranty มีเลขที่สัญญา แต่หาไม่เจอ
                            // _Trantyp = "IN";
                            cob_TrnType.SelectedValue = _Trantyp;

                        }
                        #endregion

                    }
                    else
                    {
                        /*
                       //อะไหล่นอกwarranty ถ้า ไม่มี เลขที่สัญญา ให้ Trantyp = IN ทั้งหมด
                       */
                        //_Trantyp = "IN";
                        cob_TrnType.SelectedValue = _Trantyp;
                    }








                    //============================================


                    #endregion
                }//หมดประกัน
            }  //using (DAL


        }

        private bool check_Conf_Conditions(string partNo, string workTypeID)
        {
            bool isContract = false;

            strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext(strConn))
            {
                #region ตรวจสอบวันที่สัญญา

                //0 ตรวจสอบวันที่สัญญา
                var dnow = DateTime.Today.Date;

                if (!string.IsNullOrEmpty(_contractNo) && _contractNo != "null")
                {
                    var con0 = dc.Conf_Contracts
                                .Where(t => t.ContractNo.Contains(_contractNo))
                                .FirstOrDefault();
                    if (con0 != null)//มีสัญญาจึงไปเช็คเงื่อนไข
                    {
                        var con = dc.Conf_Contracts
                            .Where(t => (t.StartDate.Date <= dnow && t.EndDate >= dnow &&
                                t.TYP_ID == tmpJobType &&
                                t.GOP_ID == tmpGOPId &&
                                t.ContractNo.Contains(_contractNo)) //&&  t.IsCheckTranType == true  
                                )
                            .FirstOrDefault();

                        if (con == null)//สัญญาหมด
                        {
                            //isContract = false;
                            return false;
                        }
                        else
                            isContract = true;
                    }// if (con0 != null)
                }// if (!string.IsNullOrEmpty(_contractNo) && _contractNo != "null")
                else
                {//ไม่มีเลขที่สัญญา
                    var con0 = dc.Conf_Contracts
                               //20160506.Where(t => t.ContractNo.Contains(_contractNo))
                               .Where(t => t.ContractNo == _contractNo)
                               .FirstOrDefault();
                    if (con0 != null)//มีสัญญาจึงไปเช็คเงื่อนไข (ไม่เข้าอยู่แล้ว)
                    {
                        var con = dc.Conf_Contracts
                               .Where(t => (t.StartDate.Date <= dnow && t.EndDate >= dnow &&
                                   t.TYP_ID == tmpJobType &&
                                   t.GOP_ID == tmpGOPId)
                                   )
                               .FirstOrDefault();
                        if (con == null)//สัญญาหมด
                            return false;
                        else
                            isContract = true;

                    }
                }

                #endregion

                //Check เงื่อนไขสัญญาตามสถานี...
                var qst = (from t in dc.Conf_Contracts
                           from tc in dc.Conf_Conditions
                           from sp in dc.SpareParts
                           where  // t.IsCheckTranType == true && (
                                    t.ContractNo.Contains(_contractNo) &&
                                   (t.ContractNo == tc.ContractNo) &&
                                   (tc.SPA_ID == sp.SPA_ID) &&
                                   (t.GOP_ID == tmpGOPId) &&
                                   (t.STA_ID == tmpSTAID) &&//<==| ตามสถานี
                                                            //(tc.TYP_ID == tmpJobType) &&//ประเภท JOB
                                   (tc.TYP_ID == workTypeID) && //ประเภทงานในเงื่อนไขสัญญา ตรงกับ JOB
                                   ((tc.SPA_ID == partNo)) &&
                                   (tc.IsAutoPart == false)//<==| ดึงเฉพาะ Part ที่ไม่โหลด Auto
                           //)
                           select tc).FirstOrDefault();

                if (qst != null)
                {
                    con_ERP_StationCharge.SelectedIndex = con_ERP_StationCharge.FindString("สถานี");
                    chIsPrices.Checked = true;


                    cob_TrnType.SelectedValue = qst.TRA_ID;
                    isContract = true;
                }
                else
                {
                    if (!string.IsNullOrEmpty(_contractNo) && _contractNo != "null")
                    {
                        #region Check เงื่อนไขสัญญาตามกลุ่มบริษัท...
                        //Check เงื่อนไขสัญญาตามกลุ่มบริษัท...
                        var q = (from t in dc.Conf_Contracts
                                 from tc in dc.Conf_Conditions
                                 from sp in dc.SpareParts
                                 where  //t.IsCheckTranType == true && (
                                        t.ContractNo.Contains(_contractNo) &&
                                        ((t.ContractNo == tc.ContractNo) &&
                                         (tc.SPA_ID == sp.SPA_ID)) &&
                                         (t.GOP_ID == tmpGOPId) &&
                                         //(t.STA_ID == tmpSTAID) &&//<==| ตามสถานี
                                         //(tc.TYP_ID == tmpJobType) && //ประเภท JOB
                                         (tc.TYP_ID1 == workTypeID) && //ประเภทงานในเงื่อนไขสัญญา ตรงกับ JOB
                                         ((tc.SPA_ID == partNo)) &&
                                         (tc.IsAutoPart == false)//<==| ดึงเฉพาะ Part ที่ไม่โหลด Auto
                                 //)
                                 select tc).FirstOrDefault();

                        if (q != null)
                        {
                            if (q.IsPartPriceCharges)
                            {
                                con_ERP_StationCharge.SelectedIndex = con_ERP_StationCharge.FindString("สถานี");
                                chIsPrices.Checked = true;
                            }
                            else
                            {
                                con_ERP_StationCharge.SelectedIndex = con_ERP_StationCharge.FindString("บริษัทน้ำมัน");
                                chIsPrices.Checked = false;
                            }

                            cob_TrnType.SelectedValue = q.TRA_ID;
                            isContract = true;
                        }
                        else
                        {
                            chIsPrices.Checked = false;
                            //20151105 ทดสอบเปลี่ยนค่า isContract = false;
                            isContract = true;
                        }

                        #endregion

                    }
                    else
                    {
                        #region Check เงื่อนไขสัญญาตามกลุ่มบริษัท...
                        //Check เงื่อนไขสัญญาตามกลุ่มบริษัท...
                        var q = (from t in dc.Conf_Contracts
                                 from tc in dc.Conf_Conditions
                                 from sp in dc.SpareParts
                                 where  //t.IsCheckTranType == true && (
                                        ((t.ContractNo == tc.ContractNo) &&
                                         (tc.SPA_ID == sp.SPA_ID)) &&
                                         (t.GOP_ID == tmpGOPId) &&
                                         (tc.TYP_ID1 == workTypeID) && //ประเภทงานในเงื่อนไขสัญญา ตรงกับ JOB
                                         ((tc.SPA_ID == partNo)) &&
                                         (tc.IsAutoPart == false)//<==| ดึงเฉพาะ Part ที่ไม่โหลด Auto
                                 //)
                                 select tc).FirstOrDefault();

                        if (q != null)
                        {
                            if (q.IsPartPriceCharges)
                            {
                                con_ERP_StationCharge.SelectedIndex = con_ERP_StationCharge.FindString("สถานี");
                                chIsPrices.Checked = true;
                            }
                            else
                            {
                                con_ERP_StationCharge.SelectedIndex = con_ERP_StationCharge.FindString("บริษัทน้ำมัน");
                                chIsPrices.Checked = false;
                            }

                            cob_TrnType.SelectedValue = q.TRA_ID;
                            isContract = true;
                        }
                        else
                        {
                            chIsPrices.Checked = false;
                            //20151105 ทดสอบเปลี่ยนค่า isContract = false;
                            isContract = true;
                        }

                        #endregion
                    }
                }

                #region พิมพ์ราคาบน Service Report
                JOB_IS_PrintPrice(dc);

                #endregion
            }

            return isContract;
        }
        private void DefaultTranTypeByPartMaster(string partNo)
        {
            strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
            using (SMSManageDataContext dc = new SMSManageDataContext(strConn))
            {
                var q = dc.SpareParts
                                .Where(t => t.SPA_ID == partNo)
                                .FirstOrDefault();

                if (q != null && !string.IsNullOrEmpty(q.TranType) && q.TranType != "00")
                {
                    cob_TrnType.SelectedValue = q.TranType;
                }
            }
        }
        private void check_SpareParts_IsTravel(string partNo, string workTypID)
        {
            strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext(strConn))
            {
                //0 ตรวจสอบสัญญา
                var dnow = DateTime.Today.Date;

                var con0 = dc.Conf_Contracts
                   .Where(t => t.ContractNo.Contains(_contractNo))
                   .FirstOrDefault();
                if (con0 != null)//มีสัญญา จึงไปเช็ควันที่
                {
                    var con = dc.Conf_Contracts
                        .Where(t => t.StartDate.Date <= dnow && t.EndDate >= dnow &&
                            t.TYP_ID == tmpJobType &&
                            t.ContractNo.Contains(_contractNo))
                        .FirstOrDefault();
                    if (con == null)//สัญญาหมด
                    {
                        //var conShow = dc.Conf_Contracts
                        //.Where(t =>
                        //    t.TYP_ID == tmpJobType &&
                        //    t.ContractNo.Contains(_contractNo))
                        //.FirstOrDefault();
                        //DateTime dtStart = Convert.ToDateTime(conShow.StartDate.ToString("dd/MM/yyyy HH:mm"));
                        //DateTime dtEnd = Convert.ToDateTime(conShow.EndDate.ToString("dd/MM/yyyy HH:mm"));

                        //string vStart = Convert.ToString(dtStart);
                        // string vEnd = Convert.ToString(dtEnd); 
                        if (!string.IsNullOrEmpty(_contractNo))
                            MessageBox.Show("เลขที่สัญญา " + _contractNo + " หมดอายุ" + Environment.NewLine +
                            // " วันที่เริ่มต้นสัญญา " + dtStart + " " + Environment.NewLine +
                            // " วันที่สิ้นสุดสัญญา " + dtEnd + " " + Environment.NewLine +
                            "กรุณาตรวจสอบ...", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        return;
                    }

                }

                //Check ข้อมูลในตาราง SpareParts_IsTravel
                var iTv = (from t in dc.SpareParts_IsTravels
                           where t.GOP_ID == tmpGOPId &&
                           t.SPA_ID == partNo
                           select t).FirstOrDefault();

                if (iTv != null)
                {
                    var st = dc.Stations
                        .Where(t => t.STA_ID == tmpSTAID)
                        .FirstOrDefault();
                    if (st != null)
                    {
                        decimal acDistance = _ActualDistance;// st.ActualDistance.Value;
                        decimal dFrom = _DistanceFromCenter;// st.DistanceFromCenter.Value;
                        decimal perKM = 0;// st.PerKM.Value == null ? 0 : st.PerKM.Value;

                        if (!string.IsNullOrEmpty(_contractNo))
                        {
                            #region Part only  _contractNo != ""

                            var qconf = dc.Conf_Conditions
                            .Where(t =>
                                       t.GOP_ID == tmpGOPId &&
                                       t.TYP_ID == tmpJobType &&
                                       t.TYP_ID1 == tmpTYP_ID1 &&
                                        t.SPA_ID == partNo &&
                                       t.ContractNo.Contains(_contractNo)
                           ).FirstOrDefault();

                            if (qconf != null)
                            {
                                perKM = qconf.PriceList;
                            }
                            else
                            {
                                //var pp = (from t in dc.SparePart_Prices_Lists
                                //          from t1 in dc.SpareParts_IsPrices
                                //          where ((t.SPA_ID == t1.SPA_ID) &&
                                //          t.SPA_ID == partNo && t1.GOP_ID == tmpGOPId)
                                //          select t).FirstOrDefault();

                                //if (pp != null)
                                //    perKM = pp.Prices;
                                //else
                                //{
                                //    var pp1 = dc.SpareParts.Where(t => t.SPA_ID == partNo)
                                //        .FirstOrDefault();
                                //    if (pp1 != null)
                                //        perKM = pp1.PricePerUnit.Value;
                                //}

                                var pp1 = dc.SpareParts.Where(t => t.SPA_ID == partNo)
                                        .FirstOrDefault();
                                if (pp1 != null)
                                    perKM = pp1.PricePerUnit.Value;


                            }

                            #endregion
                        }
                        else
                        {
                            #region Part only  _contractNo == ""

                            var qconf = dc.Conf_Conditions
                            .Where(t =>
                                       t.GOP_ID == tmpGOPId &&
                                       t.TYP_ID == tmpJobType &&
                                       t.TYP_ID1 == tmpTYP_ID1 &&
                                       t.SPA_ID == partNo
                           ).FirstOrDefault();

                            if (qconf != null)
                            {
                                perKM = qconf.PriceList;
                            }
                            else
                            {
                                //var pp = (from t in dc.SparePart_Prices_Lists
                                //          from t1 in dc.SpareParts_IsPrices
                                //          where ((t.SPA_ID == t1.SPA_ID) &&
                                //          t.SPA_ID == partNo && t1.GOP_ID == tmpGOPId)
                                //          select t).FirstOrDefault();

                                //if (pp != null)
                                //    perKM = pp.Prices;
                                //else
                                //{
                                //    var pp1 = dc.SpareParts.Where(t => t.SPA_ID == partNo)
                                //        .FirstOrDefault();
                                //    if (pp1 != null)
                                //        perKM = pp1.PricePerUnit.Value;
                                //}

                                var pp1 = dc.SpareParts.Where(t => t.SPA_ID == partNo)
                                      .FirstOrDefault();
                                if (pp1 != null)
                                    perKM = pp1.PricePerUnit.Value;

                            }

                            #endregion

                        }

                        txtPriceList.Text = perKM.ToString("#,##0.00");//(dFrom * perKM).ToString("#,##0.00");
                        txtStation.Text = txtStation.Text +
                            " [ระยะทางจริง " + acDistance.ToString("#,##0.00") +
                            " จากศูนย์ " + dFrom.ToString("#,##0.00") +
                            " กม.ละ " + perKM.ToString("#,##0.00") + "]";
                        txtPartQT.Text = (acDistance - dFrom).ToString("#,##0.00");
                        txtPriceUnit.Text = ((acDistance - dFrom) * perKM).ToString("#,##0.00");

                        //chIsPrices.Checked = true;
                        chIsPrices.Checked = false;
                        chIsPrices.Enabled = true;

                    }
                    else
                    {
                        txtStation.Text = txtStation.Text;
                        chIsPrices.Checked = false;
                        chIsPrices.Enabled = true;
                    }

                }

            }

        }

        private void check_partIS(string partNo, string TypID1)
        {

            var vPartNo = partNo.Substring(0, 2).ToString();

            if (vPartNo == "19" && TypID1 != "OU")
            {
                cob_TrnType.SelectedValue = "IS";//ไม่เก็บเงิน
                con_ERP_StationCharge.SelectedIndex = con_ERP_StationCharge.FindString("บริษัทน้ำมัน");
                chIsPrices.Checked = false;
            }
            else if (vPartNo == "19" && TypID1 == "OU")
            {
                //cob_TrnType.SelectedValue = "IN"; //รอคำสั่งอนุมัติเรื่องการจ่ายคอม แล้วให้ใช้ตัวนี้//20160722
                cob_TrnType.SelectedValue = "IS";
                con_ERP_StationCharge.SelectedIndex = con_ERP_StationCharge.FindString("บริษัทน้ำมัน");
                chIsPrices.Checked = false;
            }

        }

        private void txtPartName_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {

                try
                {
                    DateTime openjob = Convert.ToDateTime(Convert.ToDateTime(txtOpenJOB.Text).ToShortDateString());
                    Finding.FindSparePartOnline f = new SMSMINI.Transaction.Finding.FindSparePartOnline();

                    //f.TmpParameter = txtPartName.Text.Trim();

                    //f.TmpDopenJOB = openjob;
                    //f.TmpGopID = tmpGOPId;
                    //f.TmpGopName = tmpGopName;

                    f.TmpParameter = txtPartNo.Text.Trim();

                    f.TmpDopenJOB = openjob;
                    f.TmpGopID = tmpGOPId;
                    f.TmpGopName = tmpGopName;

                    f.tmpJobType = tmpJobType;
                    f.tmpTYP_ID1 = tmpTYP_ID1;

                    f.tmpContractNo = _contractNo;


                    if (txtProjectNO.Text != "")
                        f.TmpProjectNo = txtProjectNO.Text.Trim();

                    string partNo = "";
                    string TmpPartName = "";

                    if (f.ShowDialog() == DialogResult.OK)
                    {


                        partNo = f.TmpPartNo;
                        tmpSPA_ID = f.TmpPartNo;
                        TmpPartName = f.TmpPartName;

                        if (partNo != "")
                        {
                            setPartDetail(partNo, TmpPartName);

                        }
                    }



                }
                catch (System.Exception ex)
                {
                    MessageBox.Show("ข้อผิดพลาด " + ex.Message, "เกิดข้อผิดพลาด");
                }

                txtPartQT.Focus();


            }
        }

        private void cbIsCustomer_CheckedChanged(object sender, EventArgs e)
        {
            if (cbIsCustomer.Checked == true)
            {
                cob_TrnType.SelectedValue = "CO";
            }
        }

        private void txtPartQT_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtPartQT_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                if (txtPartQT.Text == "0") return;
                txtPriceList.Focus();

                supTotalPrices();
            }
        }

        private void txtPartQT_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < "0"[0] || e.KeyChar > "9"[0]) && e.KeyChar != ControlChars.Back && e.KeyChar != "."[0])
                e.Handled = true;
        }

        private void txtPriceList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtPriceList.Text == "0") return;
                txtVAT.Focus();
            }
        }

        private void txtPriceList_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < "0"[0] || e.KeyChar > "9"[0]) && e.KeyChar != ControlChars.Back && e.KeyChar != "."[0])
                e.Handled = true;

        }

        private void txtPriceList_TextChanged(object sender, EventArgs e)
        {
            if (txtPriceList.Text == "")
                txtPriceList.Text = "0.00";




            try
            {
                txtPriceList.Text = double.Parse(txtPriceList.Text).ToString("#,##0.00");
                txtPriceList.Select(txtPriceList.Text.Length - 3, 0);
            }
            catch (Exception)
            {
                MessageBox.Show("ป้อนเฉพาะตัวเลข กรุณาตรวจสอบ", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtPriceList.SelectAll();
                txtPriceList.Focus();

                return;
            }


            supTotalPrices();
        }

        private void txtDiscQuarter_TextChanged(object sender, EventArgs e)
        {
            if (txtDiscQuarter.Text == "")
                txtDiscQuarter.Text = "0.00";
            if (decimal.Parse(txtDiscQuarter.Text) > 100)
            {
                MessageBox.Show("ห้ามป้อนส่วนลดเกิน 100", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtDiscQuarter.SelectAll();
                txtDiscQuarter.Focus();
                return;
            }

            supTotalPrices();
        }

        private void txtVAT_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < "0"[0] || e.KeyChar > "9"[0]) && e.KeyChar != ControlChars.Back && e.KeyChar != "."[0])
                e.Handled = true;
        }

        private void txtVAT_TextChanged(object sender, EventArgs e)
        {
            sumVAT();
        }

        private void chIsPrices_Click(object sender, EventArgs e)
        {
            if (chIsPrices.Checked)
            {
                txtVAT.Text = "7.00";
                con_ERP_StationCharge.SelectedIndex = con_ERP_StationCharge.FindString("สถานี");
                con_ERP_StationCharge.Enabled = false;
            }
            else
            {
                txtVAT.Text = "0.00";
                con_ERP_StationCharge.Enabled = true;
            }
        }

        private void txtPartComment_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) radTranType0.Focus();
        }

        private void cob_TrnType_SelectionChangeCommitted(object sender, EventArgs e)
        {
            txtPartComment.Focus();
            try
            {
                if (txtWorkType.Text.Split(':')[0] == "OU" && cob_TrnType.SelectedValue.ToString() != "IN")
                {
                    MessageBox.Show("ประเภทงาน: " + txtWorkType.Text.Split(':')[1] + Environment.NewLine +
                        "Part เก็บเงินทุกกรณี" + Environment.NewLine +
                        "TranType ต้องเป็น IN" + Environment.NewLine +
                        "กรุณาตรวจสอบ"
                        , "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    cob_TrnType.Focus();
                }
            }
            catch (Exception)
            {

            }

        }

        private void btBtPart_select_Click(object sender, EventArgs e)
        {

            #region Validate data

            if (cob_PoitFail0.SelectedIndex == -1 || cob_PoitFail0.Items.Count <= 0)
            {
                MessageBox.Show("กรุณาเลือกจุดเสีย ", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            //tmpSPA_ID txtPartNo.Text
            if (!checkPartInDB(tmpSPA_ID))
            {
                MessageBox.Show("กรุณาระบุอะไหล่ โดยการค้นหาจากฐานข้อมูล ", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (txtPartQT.Text == "" || txtPartQT.Text == "0")
            {
                txtPartQT.Focus();
                MessageBox.Show("กรุณาระบุจำนวนที่เปลี่ยน...", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (cob_PoitFail0.SelectedIndex == -1)
            {
                MessageBox.Show("กรุณาเลือกจุดเสียก่อนป้อน อะไหล่", "ผลการตรวจสอบ");
                return;
            }

            if (txtPartNo.Text == "" || tmpSPA_ID == string.Empty)
            {
                txtPartNo.Focus();
                MessageBox.Show("กรุณาป้อนข้อมูลรหัสอะไหล่...", "ผลการตรวจสอบ");
                return;
            }

            if (!radTranType0.Checked && !radTranType1.Checked)
            {
                MessageBox.Show("กรุณาเลือกข้อมูล สถานะ (ค้าง หรือเปลี่ยน)...", "ผลการตรวจสอบ");
                return;
            }

            //con_ERP_StationCharge
            if (con_ERP_StationCharge.SelectedIndex == -1)
            {
                MessageBox.Show("กรุณาเลือกข้อมูล ออกบิลในนาม...", "ผลการตรวจสอบ");
                return;
            }

            if (cob_TrnType.SelectedIndex == -1)
            {
                MessageBox.Show("กรุณาเลือกข้อมูล TranType...", "ผลการตรวจสอบ");
                return;
            }

            //if (lbEQP_ID.Text == "")
            //{
            //    MessageBox.Show("กรุณาเลือกข้อมูล TranType...", "ผลการตรวจสอบ");
            //    return;
            //}

            #endregion


            newProgressbar();
            invoke_Progress("กำลังตรวจสอบข้อมูล...");

            try
            {
                DataRow dr = null;

                string _poid = cob_PoitFail0.SelectedValue == null ? "000" : cob_PoitFail0.SelectedValue.ToString().Trim();
                string _pode = cob_PoitFail0.Text.Trim() == "" ? "Other" : cob_PoitFail0.Text.Trim();

                //20140905
                string _SNOld = txtPartSerialNumberOld.Text.Trim() == "" ? "" : txtPartSerialNumberOld.Text.Trim();

                string radPartStatus = "";
                if (radTranType0.Checked)
                    radPartStatus = "ค้าง";
                else if (radTranType1.Checked)
                    radPartStatus = "เปลี่ยน";

                //อะไหล่ลูกค้า
                string _cbIsCustomer = "";
                if (this.cbIsCustomer.Checked == true)
                {
                    _cbIsCustomer = "Y";
                }
                else
                {
                    _cbIsCustomer = "N";
                }


                if (checkSPA_ID(tmpSPA_ID, radPartStatus, _poid, tmpFAIID, tmpLocation, cob_TrnType.SelectedValue.ToString()))
                {
                    closeProgress();
                    MessageBox.Show("คุณป้อนข้อมูล อะไหล่ ซ้ำ กรุณาเลือกใหม่", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtPartNo.Focus();
                    txtPartNo.SelectAll();
                    return;
                }
                decimal number;

                invoke_Progress("กำลังตรวจสอบ Price List...");
                decimal priceList = 0;
                decimal vat = 0;
                decimal dis = 0;
                priceList = Decimal.TryParse(txtPriceList.Text, out number) ? number : 0;
                vat = Decimal.TryParse(txtVAT.Text, out number) ? number : 0;
                dis = Decimal.TryParse(txtDiscQuarter.Text, out number) ? number : 0;

                //จำนวนอะใหล่
                decimal qt = 0;
                qt = Decimal.TryParse(txtPartQT.Text, out number) ? number : 0;

                if (dis > 0)
                    priceList = priceList - (priceList * (dis / 100));

                //ราคา/หน่วย
                decimal pricePerUnit = 0;// Math.Round(priceList, tmpRounup);

                if (tmpRounup > 0)
                    pricePerUnit = Math.Round(priceList, tmpRounup);
                else
                    pricePerUnit = priceList;

                //dtPart
                int xrow = 0;

                if (tmpPA_ID_NO != 0)
                {
                    xrow = tmpPA_ID_NO - 1;
                    tmpPA_ID_NO = 0;
                }
                else
                {
                    for (int r = 0; r <= dtPart.Rows.Count - 1; r++)
                    {
                        if (Convert.ToInt32(dtPart.Rows[r]["NO."].ToString()) > xrow)
                            xrow = Convert.ToInt32(dtPart.Rows[r]["NO."].ToString());
                    }
                }


                //รวมสุทธิ
                decimal _xtotal = pricePerUnit * qt;

                //ภาษี
                decimal _xvat = vat > 0 ? _xtotal * (vat / 100) : 0;


                //20161114
                string _vReferPage = txtReferPage.Text.Trim();

                //20141127ยกเลิก int _EQP_ID = int.Parse(lbEQP_ID.Text.ToString());
                using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext())
                {
                    //dr = setPartDetail(dc, dr, _poid, _pode, radPartStatus, vat, dis, qt, pricePerUnit, xrow, _xtotal, _xvat, _cbIsCustomer);
                    //20141127ยกเลิก dr = setPartDetail(dc, dr, _poid, _pode, radPartStatus, vat, dis, qt, pricePerUnit, xrow, _xtotal, _xvat, _cbIsCustomer, _SNOld, _EQP_ID);

                    //20161114 dr = setPartDetail(dc, dr, _poid, _pode, radPartStatus, vat, dis, qt, pricePerUnit, xrow, _xtotal, _xvat, _cbIsCustomer, _SNOld);
                    dr = setPartDetail(dc, dr, _poid, _pode, radPartStatus, vat, dis, qt, pricePerUnit, xrow, _xtotal, _xvat, _cbIsCustomer, _SNOld, _vReferPage);

                    //dr = setPartDetail(dc, dr, _poid, _pode, radPartStatus, vat, dis, qt, priceP
                    //Split Part {Load Part to ERP}
                    if (radPartStatus == "เปลี่ยน")
                    {
                        if (!string.IsNullOrEmpty(_contractNo))
                        {
                            var q = (from ct in dc.Conf_Conditions
                                     from sp in dc.SpareParts

                                     where (ct.SPA_ID == sp.SPA_ID) &&
                                         (ct.SPA_ID == tmpSPA_ID && ct.GOP_ID == tmpGOPId && ct.ContractNo == _contractNo && ct.TYP_ID == _job.TYP_ID) &&
                                         (ct.ERP_SPA_ID != null && ct.ERP_TRA_ID != null)
                                     select new SPLIT_Part_ERP
                                     {
                                         ERP_SPA_ID = ct.ERP_SPA_ID,
                                         ERP_PartName = sp.SparePart1,
                                         ERP_TRA_ID = ct.ERP_TRA_ID
                                     }).FirstOrDefault();

                            if (q != null)
                            {
                                //dr = setPartDetil_to_ERP(dc, dr, _poid, _pode, radPartStatus, vat, dis, qt, pricePerUnit, xrow, _xtotal, _xvat, q);
                                //20140908
                                //20141127ยกเลิก dr = setPartDetil_to_ERP(dc, dr, _poid, _pode, radPartStatus, vat, dis, qt, pricePerUnit, xrow, _xtotal, _xvat, q,_SNOld,_EQP_ID);

                                dr = setPartDetil_to_ERP(dc, dr, _poid, _pode, radPartStatus, vat, dis, qt, pricePerUnit, xrow, _xtotal, _xvat, q, _SNOld);


                            }

                        }

                    }

                }

                PartBindind();
                ClearPartText();

                invoke_Progress("กำลังตรวจสอบ Lock TranType...");
                lockTranType();

                invoke_Progress("กำลังตรวจสอบ ค้างอะไหล่...");
                checkPartRemain();

                closeProgress();
            }
            catch (System.Exception ex)
            {
                closeProgress();
                MessageBox.Show("ข้อผิดพลาด " + ex.Message, "เกิดข้อผิดพลาด");
            }
        }


        private bool checkPartInDB(string p)
        {


            if (checkBox_SMS_BAK.Checked == true)
                strConn = SMSMINI.Properties.Settings.Default.ServicesMSDB_BAKConnectionString;
            else
                strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;



            using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext(strConn))
            {
                if ((from t in dc.SpareParts where t.SPA_ID == p select t).Count() > 0)
                    return true;
                else
                    return false;
            }
        }
        private bool checkSPA_ID(string pID, string radPartStatus, string cobPoitFail0, string faiid, string local, string tranType)
        {
            for (int i = 0; i <= dataGridView_Part.Rows.Count - 1; i++)
            {
                if (pID == dataGridView_Part["Part no.", i].Value.ToString() &&
                    radPartStatus == dataGridView_Part["สถานะ", i].Value.ToString() &&
                    cobPoitFail0.Trim() == dataGridView_Part["รหัสจุดเสีย", i].Value.ToString() &&
                    faiid == dataGridView_Part["FAI_ID", i].Value.ToString() &&
                    local == dataGridView_Part["Location", i].Value.ToString() &&
                    (tranType == dataGridView_Part["Type", i].Value.ToString()) &&
                    //Convert.ToInt32(dataGridView_Part["จำนวน", i].Value.ToString() == "0.00" ? "0" : dataGridView_Part["จำนวน", i].Value.ToString()) == Convert.ToInt32(txtPartQT.Text == "0.00" ? "0" : txtPartQT.Text))
                    decimal.Parse(dataGridView_Part["จำนวน", i].Value.ToString()) == decimal.Parse(txtPartQT.Text))
                    return true;
            }
            return false;
        }

        private DataRow setPartDetil_to_ERP(DAL.SMSManage.SMSManageDataContext dc, DataRow dr, string _poid, string _pode, string radPartStatus, decimal vat, decimal dis, decimal qt, decimal pricePerUnit, int xrow, decimal _xtotal, decimal _xvat, SPLIT_Part_ERP q, string _SNOld)
        {
            dr = dtPart.NewRow();
            dr["รหัสจุดเสีย"] = _poid;
            dr["จุดเสีย"] = _pode;
            dr["NO."] = xrow + 1;

            //20140908
            dr["S/N Old"] = _SNOld;
            dr["Part no."] = q.ERP_SPA_ID;
            dr["Part"] = q.ERP_PartName;
            dr["จำนวน"] = qt.ToString();

            dr["VAT"] = vat.ToString("#,##0.00");
            dr["FixAssetNo"] = txtFixAssetNo.Text.Trim();

            dr["PricesList"] = decimal.Parse(txtPriceList.Text).ToString("#,##0.00");

            dr["ราคา/หน่วย"] = pricePerUnit.ToString("#,##0.00");
            dr["รวม"] = _xtotal.ToString("#,##0.00");
            dr["ภาษี"] = Math.Round(_xvat, 2).ToString("#,##0.00");

            dr["รวมสุทธิ"] = Convert.ToDecimal(_xtotal + _xvat).ToString("#,##0.00");

            dr["Discount"] = dis.ToString("#,##0.00");

            dr["สถานะ"] = radPartStatus;
            dr["Type"] = q.ERP_TRA_ID;
            dr["Comment"] = "Auto Load to ERP";

            dr["FAI_ID"] = tmpFAIID;
            dr["Location"] = tmpLocation;
            dr["S/N"] = tmpSN;
            dr["IsCancel"] = "0";
            dr["IsDownload"] = "Y";

            dr["isPrices"] = checkConf_JOB_IS_PrintPrice(dc, txtJobID.Text.Trim(), cob_TrnType.SelectedValue.ToString());
            dr["ออกบิลในนาม"] = con_ERP_StationCharge.Text.Trim().ToString();


            dr["ERP_SPA_ID"] = null;
            dr["ERP_PartName"] = null;

            dr["ERP_orderline_id"] = lblERP_orderline_id.Text;

            //20141127ยกเลิก dr["ตำแหน่งที่เปลี่ยน"] = _EQP_ID;

            dtPart.Rows.Add(dr);
            return dr;
        }

        private void dataGridView_Part_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
            if (e.RowIndex != -1)
            {
                int xCol = dataGridView_Part.ColumnCount - 1;
                switch (e.ColumnIndex)
                {
                    case 0://ยกเลิกข้อมูล
                        #region ยกเลิกข้อมูล

                        try
                        {
                            if (dataGridView_Part["IsDownload", e.RowIndex].Value.ToString() == "Y")
                            {
                                using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext(strConn))
                                {
                                    string _spaID = dataGridView_Part["Part no.", e.RowIndex].Value.ToString();
                                    int _spaNo = Convert.ToInt32(dataGridView_Part["NO.", e.RowIndex].Value.ToString());

                                    //var q = dc.SpareParts
                                    //     .Where(t => t.ERP_SPA_ID == _spaID && t.ERP_SPA_Split == true)
                                    //     .FirstOrDefault();

                                    var q = dc.Conf_Conditions
                                        .Where(t => t.ERP_SPA_ID == _spaID && t.GOP_ID == tmpGOPId && t.ContractNo == _contractNo)
                                        .FirstOrDefault();

                                    if (q != null)
                                    {
                                        MessageBox.Show("Part Auto Load to ERP" + Environment.NewLine
                                            + "ไม่สามารถ เปลี่ยน สถานะ", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        return;
                                    }
                                    else
                                    {


                                        if (dataGridView_Part["IsCancel", e.RowIndex].Value.ToString() == "1")
                                        {
                                            dataGridView_Part["IsCancel", e.RowIndex].Value = "0";

                                            string _xNO = "";
                                            string erpNO = "";
                                            string erpPartNO = "";

                                            try
                                            {
                                                _xNO = dataGridView_Part["NO.", e.RowIndex].Value.ToString();
                                                erpNO = dataGridView_Part["NO.", e.RowIndex + 1].Value.ToString();
                                                erpPartNO = dataGridView_Part["Part no.", e.RowIndex + 1].Value.ToString();

                                            }
                                            catch (Exception)
                                            {
                                            }
                                            if (_xNO == erpNO && erpPartNO != "")
                                            {
                                                dataGridView_Part["IsCancel", e.RowIndex + 1].Value = "0";
                                                dataGridView_Part["สถานะ", e.RowIndex + 1].Value = "เปลี่ยน";
                                            }


                                            if (tmpStatusPart == "ค้าง" && tmpStatusPartID == dataGridView_Part["Part no.", e.RowIndex].Value.ToString())
                                                dataGridView_Part["สถานะ", e.RowIndex].Value = "เปลี่ยน";

                                        }
                                        else
                                        {
                                            #region ไม่สามารถยกเลิกเพื่อแก้ไขข้อมูลได้ เนื่องจาก Download จาก Server

                                            if (MessageBox.Show("ไม่สามารถยกเลิกเพื่อแก้ไขข้อมูลได้ เนื่องจาก Download จาก Server" + Environment.NewLine +
                                                "คุณต้องการระบุสถานะยกเลิก ใช่หรือไม่", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                            {
                                                dataGridView_Part["IsCancel", e.RowIndex].Value = dataGridView_Part["IsCancel", e.RowIndex].Value.ToString() == "0" ? "1" : "0";

                                                string _xNO = "";

                                                string erpNO = "";
                                                string erpPartNO = "";

                                                try
                                                {
                                                    _xNO = dataGridView_Part["NO.", e.RowIndex].Value.ToString();
                                                    erpNO = dataGridView_Part["NO.", e.RowIndex + 1].Value.ToString();
                                                    erpPartNO = dataGridView_Part["Part no.", e.RowIndex + 1].Value.ToString();

                                                }
                                                catch (Exception)
                                                {
                                                }
                                                if (_xNO == erpNO && erpPartNO != "")
                                                {
                                                    dataGridView_Part["IsCancel", e.RowIndex + 1].Value = dataGridView_Part["IsCancel", e.RowIndex + 1].Value.ToString() == "0" ? "1" : "0";
                                                    dataGridView_Part["สถานะ", e.RowIndex + 1].Value = "เปลี่ยน";
                                                }

                                                if (dataGridView_Part["สถานะ", e.RowIndex].Value.ToString() == "ค้าง")
                                                {
                                                    tmpStatusPart = "ค้าง";
                                                    tmpStatusPartID = dataGridView_Part["Part no.", e.RowIndex].Value.ToString();
                                                    dataGridView_Part["สถานะ", e.RowIndex].Value = "เปลี่ยน";
                                                }
                                            }

                                            #endregion
                                        }


                                        setDataGridViewStyle();
                                    }// if (q != null)
                                }// using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext())


                            }
                            else
                            {
                                if (MessageBox.Show("คุณต้องการยกเลิกข้อมูล ใช่หรือไม่???...", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                {
                                    tmpCalSPA_ID = dataGridView_Part["Part no.", e.RowIndex].Value.ToString();
                                    tmpPA_ID_NO = Convert.ToInt32(dataGridView_Part["NO.", e.RowIndex].Value.ToString());

                                    setGridPart_ToText(e);
                                    try
                                    {

                                        using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext(strConn))
                                        {
                                            //var q = dc.SpareParts
                                            //    .Where(t => t.SPA_ID == tmpCalSPA_ID && t.ERP_SPA_ID  != null)
                                            //    .FirstOrDefault();
                                            var q = dc.Conf_Conditions
                                                .Where(t => t.SPA_ID == tmpCalSPA_ID &&
                                                    t.ERP_SPA_ID != null &&
                                                    t.GOP_ID == tmpGOPId &&
                                                    t.ContractNo == _contractNo)
                                                .FirstOrDefault();

                                            if (q != null)
                                            {
                                                dtPart.Rows[e.RowIndex + 1].Delete();//Delet Part autoload to ERP
                                            }
                                        }
                                    }
                                    catch (Exception)
                                    {
                                    }

                                    dtPart.Rows[e.RowIndex].Delete();
                                    PartBindind();

                                }
                                //countPartNO();
                            }

                            checkPartRemain();
                        }
                        catch (System.Exception ex)
                        {
                            MessageBox.Show("ข้อผิดพลาด " + ex.Message, "เกิดข้อผิดพลาด");
                        }
                        break;

                    #endregion

                    //20160817 case 15://สถานะ
                    case 16://สถานะ
                        #region การเปลี่ยน อะไหล่

                        if (dataGridView_Part["สถานะ", e.RowIndex].Value.ToString() == "ค้าง")
                        {
                            if (MessageBox.Show("คุณต้องการเปลี่ยน อะไหล่ ใช่หรือไม่???...", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {
                                using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext(strConn))
                                {

                                    DataRow dr = null;
                                    string _spaID = dataGridView_Part["Part no.", e.RowIndex].Value.ToString();
                                    int _spaNo = Convert.ToInt32(dataGridView_Part["NO.", e.RowIndex].Value.ToString());

                                    //20140908
                                    string _SNOld = dataGridView_Part["S/N Old", e.RowIndex].Value.ToString();

                                    var q = dc.SpareParts
                                         .Where(t => t.ERP_SPA_ID == _spaID && t.ERP_SPA_Split == true)
                                         .FirstOrDefault();
                                    if (q != null)
                                    {
                                        MessageBox.Show("Part Auto Load to ERP" + Environment.NewLine
                                            + "ไม่สามารถ เปลี่ยน สถานะ", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        return;
                                    }

                                    dataGridView_Part["สถานะ", e.RowIndex].Value = "เปลี่ยน";


                                    string _poid = dataGridView_Part["รหัสจุดเสีย", e.RowIndex].Value.ToString(); //cob_PoitFail0.SelectedValue == null ? "000" : cob_PoitFail0.SelectedValue.ToString().Trim();
                                    string _pode = dataGridView_Part["จุดเสีย", e.RowIndex].Value.ToString();
                                    string radPartStatus = "เปลี่ยน";

                                    decimal priceList = string.IsNullOrEmpty(dataGridView_Part["PricesList", e.RowIndex].Value.ToString()) ? 0 : decimal.Parse(dataGridView_Part["PricesList", e.RowIndex].Value.ToString());
                                    decimal vat = string.IsNullOrEmpty(dataGridView_Part["VAT", e.RowIndex].Value.ToString()) ? 0 : decimal.Parse(dataGridView_Part["VAT", e.RowIndex].Value.ToString());
                                    decimal dis = string.IsNullOrEmpty(dataGridView_Part["Discount", e.RowIndex].Value.ToString()) ? 0 : decimal.Parse(dataGridView_Part["Discount", e.RowIndex].Value.ToString());

                                    //จำนวนอะใหล่
                                    decimal qt = decimal.Parse(dataGridView_Part["จำนวน", e.RowIndex].Value.ToString());

                                    if (dis > 0)
                                        priceList = priceList - (priceList * (dis / 100));

                                    //ราคา/หน่วย
                                    decimal pricePerUnit = 0;// Math.Round(priceList, tmpRounup);
                                    if (tmpRounup > 0)
                                        pricePerUnit = Math.Round(priceList, tmpRounup);
                                    else
                                        pricePerUnit = priceList;


                                    //dtPart
                                    int xrow = 0;

                                    if (tmpPA_ID_NO != 0)
                                    {
                                        xrow = tmpPA_ID_NO - 1;
                                        tmpPA_ID_NO = 0;
                                    }

                                    //รวมสุทธิ
                                    decimal _xtotal = pricePerUnit * qt;

                                    //ภาษี
                                    decimal _xvat = vat > 0 ? _xtotal * (vat / 100) : 0;

                                    //20141021
                                    //20141127ยกเลิก  int _EQP_ID = int.Parse(dataGridView_Part["ตำแหน่งที่เปลี่ยน", e.RowIndex].Value.ToString());

                                    //var q2 = dc.SpareParts
                                    //      .Where(t => t.SPA_ID == _spaID && t.ERP_SPA_ID != null && t.ERP_SPA_Split == true)
                                    //      .FirstOrDefault();

                                    var q2 = (from ct in dc.Conf_Conditions
                                              from sp in dc.SpareParts

                                              where (ct.SPA_ID == sp.SPA_ID) &&
                                                  (ct.SPA_ID == tmpSPA_ID && ct.GOP_ID == tmpGOPId && ct.ContractNo == _contractNo) &&
                                                  (ct.ERP_SPA_ID != null && ct.ERP_TRA_ID != null)
                                              select new SPLIT_Part_ERP
                                              {
                                                  ERP_SPA_ID = ct.ERP_SPA_ID,
                                                  ERP_PartName = sp.SparePart1,
                                                  ERP_TRA_ID = ct.ERP_TRA_ID
                                              }).FirstOrDefault();

                                    if (q2 != null)
                                    {
                                        //dr = setPartDetil_to_ERP(dc, dr, _poid, _pode, radPartStatus, vat, dis, qt, pricePerUnit, _spaNo - 1, _xtotal, _xvat, q2);
                                        //20140908
                                        //20141127ยกเลิก dr = setPartDetil_to_ERP(dc, dr, _poid, _pode, radPartStatus, vat, dis, qt, pricePerUnit, _spaNo - 1, _xtotal, _xvat, q2, _SNOld,_EQP_ID);
                                        dr = setPartDetil_to_ERP(dc, dr, _poid, _pode, radPartStatus, vat, dis, qt, pricePerUnit, _spaNo - 1, _xtotal, _xvat, q2, _SNOld);
                                        PartBindind();
                                    }
                                }// using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext())

                            }
                        }
                        else
                        {
                            if (MessageBox.Show("คุณต้องการค้าง อะไหล่ ใช่หรือไม่???...", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {
                                string _spa_id = dataGridView_Part["Part no.", e.RowIndex].Value.ToString();
                                int _spaNo = Convert.ToInt32(dataGridView_Part["NO.", e.RowIndex].Value.ToString());

                                using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext(strConn))
                                {

                                    var q = dc.SpareParts
                                            .Where(t => t.ERP_SPA_ID == _spa_id && t.ERP_SPA_Split == true)
                                            .FirstOrDefault();
                                    if (q != null)
                                    {
                                        MessageBox.Show("Part Auto Load to ERP" + Environment.NewLine
                                           + "ไม่สามารถ เปลี่ยน สถานะ", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        return;
                                    }

                                    dataGridView_Part["สถานะ", e.RowIndex].Value = "ค้าง";


                                    try
                                    {


                                        var q2 = dc.SpareParts
                                            .Where(t => t.SPA_ID == _spa_id && t.ERP_SPA_ID != null)
                                            .FirstOrDefault();
                                        if (q2 != null)
                                        {
                                            foreach (DataGridViewRow row in dataGridView_Part.Rows)
                                            {
                                                if (int.Parse(row.Cells["NO."].Value.ToString()) == _spaNo &&
                                                    row.Cells["Part no."].Value.ToString() == q2.ERP_SPA_ID)
                                                    dataGridView_Part.Rows.Remove(row);
                                            }


                                        }
                                    }
                                    catch (Exception)
                                    {
                                    }
                                }

                            }
                        }

                        checkPartRemain();

                        break;
                    #endregion

                    //20160817 case 23://เก็บเงินหน้างาน
                    case 24://เก็บเงินหน้างาน
                        #region IsPrices
                        ////ห้ามแก้ไข Edit 2012-08-19 v.10.1.2012
                        if (!_IsPrintPrice)
                        {
                            if (dataGridView_Part["isPrices", e.RowIndex].Value.ToString() == "0")
                            {
                                if (MessageBox.Show("คุณต้องการ พิมพ์ราคา ใช่หรือไม่???...", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                {
                                    dataGridView_Part["isPrices", e.RowIndex].Value = "1";
                                    dataGridView_Part["ออกบิลในนาม", e.RowIndex].Value = "สถานี";
                                }
                            }
                            //20160817 else
                            else if (dataGridView_Part["isPrices", e.RowIndex].Value.ToString() == "1")
                            {
                                if (MessageBox.Show("คุณไม่ต้องการ พิมพ์ราคา ใช่หรือไม่???...", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                {
                                    dataGridView_Part["isPrices", e.RowIndex].Value = "0";
                                    dataGridView_Part["ออกบิลในนาม", e.RowIndex].Value = "บริษัทน้ำมัน";
                                }
                            }
                        }
                        //checkPartRemain();

                        break;

                    #endregion

                    //20160817 case 27://อะไหล่ลูกค้า
                    case 28://อะไหล่ลูกค้า
                        #region IsCustomer

                        if (dataGridView_Part["อะไหล่ลูกค้า", e.RowIndex].Value.ToString() == "N")
                        {
                            if (MessageBox.Show("คุณต้องการ เปลี่ยนสถานะเป็น อะไหล่ลูกค้า ใช่หรือไม่???...", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {
                                dataGridView_Part["อะไหล่ลูกค้า", e.RowIndex].Value = "Y";

                            }
                        }
                        else if (dataGridView_Part["อะไหล่ลูกค้า", e.RowIndex].Value.ToString() == "Y")
                        {
                            if (MessageBox.Show("คุณต้องการ เปลี่ยนสถานะเป็น ไม่ใช่อะไหล่ลูกค้า ใช่หรือไม่???...", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {
                                dataGridView_Part["อะไหล่ลูกค้า", e.RowIndex].Value = "N";

                            }
                        }

                        //checkPartRemain();

                        break;

                        #endregion

                }

            }
        }


        private void setGridPart_ToText(DataGridViewCellEventArgs e)
        {
            strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;

            tmpLocation = dataGridView_Part.Rows[e.RowIndex].Cells["Location"].Value.ToString();
            tmpFAIID = dataGridView_Part.Rows[e.RowIndex].Cells["FAI_ID"].Value.ToString();

            cob_PoitFail0.SelectedValue = dataGridView_Part.Rows[e.RowIndex].Cells["รหัสจุดเสีย"].Value.ToString();

            txtPartNo.Text = dataGridView_Part.Rows[e.RowIndex].Cells["Part no."].Value.ToString();
            tmpSPA_ID = dataGridView_Part.Rows[e.RowIndex].Cells["Part no."].Value.ToString();
            txtPartName.Text = dataGridView_Part.Rows[e.RowIndex].Cells["Part"].Value.ToString();

            //decimal price = dataGridView_Part.Rows[e.RowIndex].Cells["ราคา/หน่วย"].Value.ToString()
            txtVAT.Text = dataGridView_Part.Rows[e.RowIndex].Cells["VAT"].Value.ToString();
            //dr["PricesList"] = txtPriceList.Text; 
            txtPriceList.Text = dataGridView_Part.Rows[e.RowIndex].Cells["PricesList"].Value.ToString();
            txtDiscQuarter.Text = dataGridView_Part.Rows[e.RowIndex].Cells["Discount"].Value.ToString();

            txtPartQT.Text = dataGridView_Part.Rows[e.RowIndex].Cells["จำนวน"].Value.ToString();

            string radTranType = dataGridView_Part.Rows[e.RowIndex].Cells["สถานะ"].Value.ToString();

            if (radTranType == "ค้าง")
                radTranType0.Checked = true;
            else if (radTranType == "เปลี่ยน")
                radTranType1.Checked = true;

            txtPartComment.Text = dataGridView_Part.Rows[e.RowIndex].Cells["Comment"].Value.ToString();


            cob_TrnType.SelectedValue = dataGridView_Part.Rows[e.RowIndex].Cells["Type"].Value.ToString();

            chIsPrices.Checked = (dataGridView_Part.Rows[e.RowIndex].Cells["IsPrices"].Value.ToString() == "0" ? false : true);
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext(strConn))
            {
                JOB_IS_PrintPrice(dc);
            }

            con_ERP_StationCharge.Text = dataGridView_Part.Rows[e.RowIndex].Cells["ออกบิลในนาม"].Value.ToString();

            //20140908
            txtPartSerialNumberOld.Text = dataGridView_Part.Rows[e.RowIndex].Cells["S/N Old"].Value.ToString();

            //20141021
            //20141127ยกเลิก lbEQP_ID.Text = dataGridView_Part.Rows[e.RowIndex].Cells["ตำแหน่งที่เปลี่ยน"].Value.ToString();
            //20141127ยกเลิก int vEQP_ID = int.Parse(dataGridView_Part.Rows[e.RowIndex].Cells["ตำแหน่งที่เปลี่ยน"].Value.ToString());
            //20141127ยกเลิก 
            /*string sqlEQ = "";
            sqlEQ = " SELECT EQP_ID,Equipment_Position FROM [Equipment_Position] WHERE  EQP_ID =" + vEQP_ID + " ";
            SqlDataReader drEQ = JaSqlHelper.ExecuteReader(strConn, CommandType.Text, sqlEQ);

            if (drEQ.HasRows)
            {
                while (drEQ.Read())
                {

                    //20141127ยกเลิก txtEquipment_Position.Text = drEQ["Equipment_Position"].ToString();
                }
            }*/
            txtFixAssetNo.Text = dataGridView_Part.Rows[e.RowIndex].Cells["FixAssetNo"].Value.ToString();
            //20141024
            lblERP_orderline_id.Text = dataGridView_Part.Rows[e.RowIndex].Cells["ERP_orderline_id"].Value.ToString();

            //201611116
            txtReferPage.Text = dataGridView_Part.Rows[e.RowIndex].Cells["Page/Item"].Value.ToString();


        }
        private void dataGridView_Part_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            decimal _qty = Convert.ToDecimal(dataGridView_Part.Rows[e.RowIndex].Cells["จำนวน"].Value);
            decimal _PricesList = Convert.ToDecimal(dataGridView_Part.Rows[e.RowIndex].Cells["PricesList"].Value);
            decimal _priUnit = Convert.ToDecimal(dataGridView_Part.Rows[e.RowIndex].Cells["ราคา/หน่วย"].Value);
            decimal _sumPri = Convert.ToDecimal(dataGridView_Part.Rows[e.RowIndex].Cells["รวม"].Value);
            decimal _sumVat = Convert.ToDecimal(dataGridView_Part.Rows[e.RowIndex].Cells["ภาษี"].Value);
            decimal _sumTotal = Convert.ToDecimal(dataGridView_Part.Rows[e.RowIndex].Cells["รวมสุทธิ"].Value);
            decimal _discount = Convert.ToDecimal(dataGridView_Part.Rows[e.RowIndex].Cells["Discount"].Value);
            decimal _vat = Convert.ToDecimal(dataGridView_Part.Rows[e.RowIndex].Cells["VAT"].Value);


            decimal _priUnit2 = (_PricesList - (_PricesList * (_discount / 100)));
            decimal _sumPri2 = ((_PricesList - (_PricesList * (_discount / 100))) * _qty);
            decimal _sumVat2 = _sumPri2 * (_vat / 100);

            dataGridView_Part.Rows[e.RowIndex].Cells["จำนวน"].Value = _qty;
            dataGridView_Part.Rows[e.RowIndex].Cells["PricesList"].Value = _PricesList.ToString("#,##0.00");
            if (tmpRounup > 0)
                dataGridView_Part.Rows[e.RowIndex].Cells["ราคา/หน่วย"].Value = Math.Round(_priUnit2, this.tmpRounup).ToString("#,##0.00");
            else
                dataGridView_Part.Rows[e.RowIndex].Cells["ราคา/หน่วย"].Value = (_priUnit2).ToString("#,##0.00");

            dataGridView_Part.Rows[e.RowIndex].Cells["รวม"].Value = _sumPri2.ToString("#,##0.00");

            dataGridView_Part.Rows[e.RowIndex].Cells["ภาษี"].Value = Math.Round(_sumVat2, 2).ToString("#,##0.00");


            dataGridView_Part.Rows[e.RowIndex].Cells["รวมสุทธิ"].Value = (_sumPri2 + _sumVat2).ToString("#,##0.00");

            dataGridView_Part.Rows[e.RowIndex].Cells["Discount"].Value = _discount.ToString("#,##0.00");
            dataGridView_Part.Rows[e.RowIndex].Cells["VAT"].Value = _vat.ToString("#,##0.00");

            dataGridView_Part.Rows[e.RowIndex].ErrorText = String.Empty;
        }

        private void dataGridView_Part_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            dataGridView_Part.Rows[e.RowIndex].ErrorText = "";

            int newInteger;
            if (dataGridView_Part.Rows[e.RowIndex].IsNewRow)
                return;

            try
            {
                if (new string[] { "จำนวน" }.Contains(dataGridView_Part.Columns[e.ColumnIndex].Name))
                {
                    if (!int.TryParse(Convert.ToInt32(e.FormattedValue.ToString().Replace(".", "").Replace(",", "")).ToString(), out newInteger) || newInteger < 0)//newInteger < 1)
                    {
                        MessageBox.Show("กรุณาป้อนเฉพาะตัวเลข", "ผลหารตรวจสอบ");
                        dataGridView_Part.Rows[e.RowIndex].ErrorText = "กรุณาป้อนเฉพาะตัวเลข";
                        e.Cancel = true;
                    }

                }
                else if (new string[] { "Discount", "VAT", "PricesList" }.Contains(dataGridView_Part.Columns[e.ColumnIndex].Name))
                {
                    if (!int.TryParse(Convert.ToInt32(e.FormattedValue.ToString().Replace(".", "").Replace(",", "")).ToString(), out newInteger) || newInteger < 0)
                    {
                        MessageBox.Show("กรุณาป้อนเฉพาะตัวเลข", "ผลหารตรวจสอบ");
                        dataGridView_Part.Rows[e.RowIndex].ErrorText = "กรุณาป้อนเฉพาะตัวเลข";
                        e.Cancel = true;
                    }
                }
            }
            catch (System.Exception)
            {

                dataGridView_Part.Rows[e.RowIndex].ErrorText = "กรุณาป้อนเฉพาะตัวเลข";
                e.Cancel = true;
            }
        }

        private void dataGridView_Part_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dataGridView_Part.CurrentCell is DataGridViewCheckBoxCell)
                dataGridView_Part.CommitEdit(DataGridViewDataErrorContexts.Commit);
        }

        private void dataGridView_Part_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            dataGridView_Part["รหัสจุดเสีย", e.RowIndex].ReadOnly = true;
            dataGridView_Part["จุดเสีย", e.RowIndex].ReadOnly = true;
            dataGridView_Part["NO.", e.RowIndex].ReadOnly = true;

            //20140908 
            dataGridView_Part["S/N Old", e.RowIndex].ReadOnly = false;
            dataGridView_Part["S/N Old", e.RowIndex].Style.BackColor = Color.White;

            dataGridView_Part["Part no.", e.RowIndex].ReadOnly = true;

            //แก้ไขได้-----------------------------------------------------------//
            dataGridView_Part["จำนวน", e.RowIndex].ReadOnly = false;
            dataGridView_Part["จำนวน", e.RowIndex].Style.BackColor = Color.White;
            //<----------------------------------------------------------->//

            dataGridView_Part["Part", e.RowIndex].ReadOnly = true;
            dataGridView_Part["Type", e.RowIndex].ReadOnly = true;

            //แก้ไขได้-----------------------------------------------------------//
            dataGridView_Part["Comment", e.RowIndex].ReadOnly = false;
            dataGridView_Part["Comment", e.RowIndex].Style.BackColor = Color.White;
            //<----------------------------------------------------------->//

            dataGridView_Part["สถานะ", e.RowIndex].ReadOnly = false;
            dataGridView_Part["สถานะ", e.RowIndex].Style.BackColor = Color.White;

            dataGridView_Part["FAI_ID", e.RowIndex].ReadOnly = true;
            dataGridView_Part["Location", e.RowIndex].ReadOnly = true;

            dataGridView_Part["IsCancel", e.RowIndex].ReadOnly = true;
            dataGridView_Part["IsDownload", e.RowIndex].ReadOnly = true;

            if (_IsPrintPrice)
            {
                //PrintPrice
                dataGridView_Part["isPrices", e.RowIndex].ReadOnly = true;
                dataGridView_Part["isPrices", e.RowIndex].Style.BackColor = Color.LightGray;
            }
            else
            {
                dataGridView_Part["isPrices", e.RowIndex].ReadOnly = false;
                dataGridView_Part["isPrices", e.RowIndex].Style.BackColor = Color.White;
            }

            dataGridView_Part["รหัสจุดเสีย", e.RowIndex].Style.BackColor = Color.LightGray;
            dataGridView_Part["จุดเสีย", e.RowIndex].Style.BackColor = Color.LightGray;
            dataGridView_Part["NO.", e.RowIndex].Style.BackColor = Color.LightGray;
            //20140908
            dataGridView_Part["S/N Old", e.RowIndex].Style.BackColor = Color.LightGray;

            dataGridView_Part["Part no.", e.RowIndex].Style.BackColor = Color.LightGray;
            dataGridView_Part["Part", e.RowIndex].Style.BackColor = Color.LightGray;
            dataGridView_Part["Type", e.RowIndex].Style.BackColor = Color.LightGray;
            dataGridView_Part["FAI_ID", e.RowIndex].Style.BackColor = Color.LightGray;
            dataGridView_Part["Location", e.RowIndex].Style.BackColor = Color.LightGray;
            dataGridView_Part["IsCancel", e.RowIndex].Style.BackColor = Color.LightGray;
            dataGridView_Part["IsDownload", e.RowIndex].Style.BackColor = Color.LightGray;


            dataGridView_Part["PricesList", e.RowIndex].Style.BackColor = Color.LightGray;
            dataGridView_Part["ราคา/หน่วย", e.RowIndex].Style.BackColor = Color.LightGray;
            dataGridView_Part["รวม", e.RowIndex].Style.BackColor = Color.LightGray;
            dataGridView_Part["ภาษี", e.RowIndex].Style.BackColor = Color.LightGray;
            dataGridView_Part["รวมสุทธิ", e.RowIndex].Style.BackColor = Color.LightGray;

            dataGridView_Part["Discount", e.RowIndex].Style.BackColor = Color.LightGray;
            dataGridView_Part["VAT", e.RowIndex].Style.BackColor = Color.LightGray;

            dataGridView_Part["PricesList", e.RowIndex].ReadOnly = true;
            dataGridView_Part["ราคา/หน่วย", e.RowIndex].ReadOnly = true;
            dataGridView_Part["รวม", e.RowIndex].ReadOnly = true;
            dataGridView_Part["ภาษี", e.RowIndex].ReadOnly = true;
            dataGridView_Part["รวมสุทธิ", e.RowIndex].ReadOnly = true;

            dataGridView_Part["Discount", e.RowIndex].ReadOnly = true;
            dataGridView_Part["VAT", e.RowIndex].ReadOnly = true;

            //20141021
            //20141215 dataGridView_Part["ตำแหน่งที่เปลี่ยน", e.RowIndex].ReadOnly = true;

            //20161115
            dataGridView_Part["Page/Item", e.RowIndex].ReadOnly = true;
        }

        private void btContact_Detail_Click(object sender, EventArgs e)
        {
            PopupConditions_Detail(txtJobID.Text);
        }
        private void PopupConditions_Detail(string JOBID)
        {
            strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
            try
            {


                using (DAL.SMSManage.SMSManageDataContext dc =
                    new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn))
                {

                    var q = (from t in dc.JOBs
                             from s in dc.Stations
                             from v in dc.vw_mini_pop_Conf_Conditions_Details
                             where t.JOB_ID == JOBID &&
                             //(v.PopPage.Value == "1"[0] || v.PopPage.Value == "3"[0]) &&
                             v.GOP_ID == s.GOP_ID &&
                             v.TYP_ID == t.TYP_ID &&
                             v.TYP_ID1 == t.TYP_ID1 &&
                             (t.STA_ID == s.STA_ID)

                             select v).ToList();


                    if (q.Count() > 0)
                    {
                        var f = new Transaction.PopupConditions_Detail();
                        f.lsPopupC = q;
                        f.ShowDialog();
                    }
                    else
                    {
                        MessageBox.Show("ไม่มีรายละเอียด เงื่อนไขสัญญา...", "ผลการตรวจสอบ",
                         MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }

            }
            catch (Exception)
            {

                MessageBox.Show("กรุณาเชื่อมต่อ อินเตอร์เน็ต...", "ผลการตรวจสอบ",
                     MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void RadioButtonList_JobStatus1_Click(object sender, EventArgs e)
        {
            tabControl1.Enabled = true;
        }

        private void radioButton3_Click(object sender, EventArgs e)
        {
            for (int i = 0; i <= dataGridView_Part.Rows.Count - 1; i++)
            {
                if ((dataGridView_Part["IsCancel", i].Value.ToString().Trim() == "0" && dataGridView_Part["สถานะ", i].Value.ToString().Trim() == "ค้าง") && RadioButtonList_JobStatus2.Checked == true)
                {
                    MessageBox.Show("มีอะไหล่ค้าง ไม่สามารถเลือกบันทึกปิดงานได้", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    RadioButtonList_JobStatus2.Checked = false;
                    return;
                }



            }

            tabControl1.Enabled = true;

            #region Autoload Part

            AutoAddPart();

            #endregion
        }
        private void AutoAddPart()
        {

            #region เพิ่ม Part Auto ตามเงื่อนไขสัญญา

            if (RadioButtonList_JobStatus2.Checked == true)//บันทึกปิด งาน
            {

                AddPartAutoByConfig();
                PartBindind();
                CancelPartTravel();
            }

            #region TranType = WA, ให้ระบบ Pop Up เพื่อยืนยันการลบ Part TRAVEL, SERVICE3

            if (dataGridView_Part.RowCount > 0)
            {
                var workTypr = txtWorkType.Text.Split(':');
                string _workTypr = "";
                if (workTypr.Length > 1)
                    _workTypr = workTypr[0].ToString();

                int waCount = 0;
                int irow = 0;
                for (int i = 0; i <= dataGridView_Part.Rows.Count - 1; i++)
                {
                    irow++;
                    if (dataGridView_Part["Type", i].Value.ToString() == "WA" && _workTypr == "OU")
                    {
                        waCount++;
                    }

                    string item = dataGridView_Part["Part no.", i].Value.ToString();
                    if (item == "SERVICE3" || item == "TRAVEL")
                        irow--;
                }

                if ((waCount > 0 && irow > 0) && (waCount == irow))//ลบ SERVICE3 และ TRAVEL
                {
                    string item = dataGridView_Part["Part no.", 0].Value.ToString();

                RemoveItem:
                    if (item == "SERVICE3" || item == "TRAVEL")
                        deletePartTRANVELandSERVICE();

                    for (int i = 0; i <= dataGridView_Part.RowCount - 1; i++)
                    {
                        item = dataGridView_Part["Part no.", i].Value.ToString();
                        if (item == "SERVICE3" || item == "TRAVEL")
                            goto RemoveItem;
                    }
                }

            }

            #endregion


            #endregion

        }
        private void AddPartAutoByConfig()
        {
            strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
            int tmp_NO = 0;
            string tmp_POI_ID = "";
            string tmp_POI_Detail = "";
            string tmp_Failure_Detail = "";

            string SerialNumber = "";
            string fixAssetNo = "";

            string SerialNumber_new = "";
            string fixAssetNo_New = "";


            string tmp_Location = "";

            string tmp_FAI_ID = "";
            string tmp_Model = "";


            if (dataGridView_FailPoint.RowCount < 1)//Default จุดเสีย
            {
                DataRow dr = dtFailure.NewRow();
                dr["NO."] = "1";
                dr["รหัสจุดเสีย"] = "000";
                dr["จุดเสีย"] = "Other";
                dr["อาการเสีย"] = "ไม่ระบุ";
                dr["S/N(Old)"] = "None";
                dr["S/N(New)"] = "None";

                dr["FCAT_ID"] = 0;
                dr["FBA_ID"] = 0;
                dr["FMD_ID"] = 0;

                dr["Location"] = "None";
                dr["Model"] = "None";
                dr["StartLiter"] = 0;
                dr["EndLiter"] = 0;
                dr["LiterTest"] = 0;
                dr["FAI_ID"] = "0000000";

                dr["IsCancel"] = "0"[0];
                dr["IsDownload"] = "N"[0];



                dtFailure.Rows.Add(dr);

                FailureBinding();
            }

            for (int i = 0; i <= dataGridView_FailPoint.Rows.Count - 1; i++)
            {
                if (dataGridView_FailPoint.Rows.Count > 0)
                {
                    tmp_NO = int.Parse(dataGridView_FailPoint["NO.", 0].Value.ToString());
                    tmp_POI_ID = dataGridView_FailPoint["รหัสจุดเสีย", 0].Value.ToString();
                    tmp_POI_Detail = dataGridView_FailPoint["จุดเสีย", 0].Value.ToString();
                    tmp_Failure_Detail = dataGridView_FailPoint["อาการเสีย", 0].Value.ToString();

                    SerialNumber = dataGridView_FailPoint["S/N(Old)", 0].Value.ToString();
                    fixAssetNo = dataGridView_FailPoint["FixAssetNo", 0].Value.ToString();

                    SerialNumber_new = dataGridView_FailPoint["S/N(New)", 0].Value.ToString();
                    fixAssetNo_New = dataGridView_FailPoint["FixAssetNo_New", 0].Value.ToString();


                    tmp_Location = dataGridView_FailPoint["Location", 0].Value.ToString();
                    tmp_FAI_ID = dataGridView_FailPoint["FAI_ID", 0].Value.ToString();
                    tmp_Model = dataGridView_FailPoint["Model", 0].Value.ToString();

                    RadioButtonList_JobStatus1.Enabled = false;
                    RadioButtonList_JobStatus2.Enabled = false;
                }
            }



            if (!string.IsNullOrEmpty(_contractNo) && _contractNo != "null")
            {

                using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext(strConn))
                {
                    #region เพิ่ม Part Auto ตามเงื่อนไขสัญญา

                    //Check เงื่อนไขสัญญาตามสถานี...
                    var contrac_Sat = from t in dc.Conf_Contracts
                                      from tc in dc.Conf_Conditions
                                      from sp in dc.SpareParts
                                      where (t.ContractNo == _contractNo) &&
                                            ((t.ContractNo == tc.ContractNo) &&
                                            (tc.SPA_ID == sp.SPA_ID)) &&
                                            (t.GOP_ID == tmpGOPId) &&
                                            (t.STA_ID == tmpSTAID) && //ตามสถานี...
                                            (tc.TYP_ID1 == tmpTYP_ID1) &&
                                            (tc.IsAutoPart == true)
                                      select new
                                      {
                                          POI_ID = tmp_POI_ID == "" ? "000" : tmp_POI_ID,
                                          PoiFailure = tmp_POI_Detail == "" ? "ไม่ระบุ" : tmp_POI_Detail,
                                          NO = tmp_NO,
                                          SPA_ID = tc.SPA_ID,
                                          PartName = sp.SparePart1,

                                          VAT = _vat,
                                          ControlFixAssetNo = fixAssetNo,
                                          PricesList = 0,
                                          Prices = _Prices,
                                          Discount = 0,
                                          SumPrices = _SumPrices,
                                          SumTotalPrices = _SumTotalPrices,
                                          SumVAT = 0,
                                          Quantity = _Quantity,
                                          StatusSpare = true,
                                          TRA_ID = tc.TRA_ID,//"IN"
                                          Comment = string.Empty,
                                          FAI_ID = tmp_FAI_ID == "" ? "0000000" : tmp_FAI_ID,
                                          Location = tmp_Location == "" ? "None" : tmp_Location,
                                          SerialNumber = SerialNumber_new == "" ? "None" : SerialNumber_new,

                                          IsPrices = false,//checkConf_JOB_IS_PrintPrice(db, txtJobID.Text.Trim(), "IN"),//false,

                                          ERP_StationCharge = (tc.IsPartPriceCharges == null ? '0' : (tc.IsPartPriceCharges == true ? '0' : '1')),
                                          sp.ERP_SPA_ID,
                                          ERP_PartName = sp.SparePart1

                                          //20161117
                                          ,
                                          ReferPage = tc.ReferPage == null ? "" : tc.ReferPage

                                      };


                    if (contrac_Sat.Count() > 0)
                    {
                        #region contrac_Sat

                        foreach (var x1 in contrac_Sat)
                        {
                            if (!checkSPA_ID(x1.SPA_ID, (x1.StatusSpare == true ? "เปลี่ยน" : "ค้าง"),
                                x1.POI_ID, x1.FAI_ID, x1.Location, x1.TRA_ID))
                            {

                                var _pri = getPricesList_AutoLoadPart(dc, x1.SPA_ID);
                                DataRow dr = dtPart.NewRow();
                                dr["รหัสจุดเสีย"] = x1.POI_ID;
                                dr["จุดเสีย"] = x1.PoiFailure == "&nbsp;" ? "" : x1.PoiFailure;

                                dr["NO."] = x1.NO;
                                dr["Part no."] = x1.SPA_ID;
                                dr["Part"] = x1.PartName;
                                dr["Type"] = x1.TRA_ID;

                                dr["จำนวน"] = _Quantity.ToString("#,##0.00"); //x1.Quantity;

                                dr["PricesList"] = _PricesList.ToString("#,##0.00"); //x1.PricesList.ToString("#,##0.00");
                                dr["ราคา/หน่วย"] = _Prices.ToString("#,##0.00"); //x1.Prices.ToString("#,##0.00");
                                dr["รวม"] = _SumPrices.ToString("#,##0.00"); //x1.SumPrices.ToString("#,##0.00");
                                dr["ภาษี"] = _SumVAT.ToString("#0.00");//x1.SumVAT.ToString("#,##0.00");

                                dr["รวมสุทธิ"] = _SumTotalPrices.ToString("#,##0.00"); //= x1.SumTotalPrices.ToString("#,##0.00");

                                dr["VAT"] = x1.VAT.ToString("#,##0.00");
                                dr["FixAssetNo"] = x1.ControlFixAssetNo;

                                dr["Discount"] = x1.Discount.ToString("#,##0.00");

                                dr["สถานะ"] = x1.StatusSpare == true ? "เปลี่ยน" : "ค้าง";
                                dr["Comment"] = x1.Comment == "&nbsp;" ? "" : x1.Comment;
                                dr["FAI_ID"] = x1.FAI_ID;
                                dr["Location"] = x1.Location;
                                dr["S/N"] = x1.SerialNumber ?? "";
                                dr["IsCancel"] = "0";
                                dr["IsDownload"] = "N";

                                dr["isPrices"] = checkConf_JOB_IS_PrintPrice(dc, txtJobID.Text.Trim(), "IN");//false,
                                //x1.ERP_StationCharge == '0' ? "1" : "0";
                                dr["ออกบิลในนาม"] = x1.ERP_StationCharge == '0' ? "สถานี" : "บริษัทน้ำมัน";

                                dr["ERP_SPA_ID"] = null;//x1.ERP_SPA_ID;
                                dr["ERP_PartName"] = null;//x1.ERP_PartName;  

                                //==20141120==
                                dr["อะไหล่ลูกค้า"] = "N";
                                dr["ERP_orderline_id"] = "0";
                                //20150612 dr["ตำแหน่งที่เปลี่ยน"] = "99";
                                //====

                                //20161117
                                dr["Page/Item"] = x1.ReferPage;



                                logCancelSPA_ID.Add(x1.SPA_ID);
                                dtPart.Rows.Add(dr);
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        //Check เงื่อนไขสัญญาตามกลุ่มบริษัท...
                        #region contrac_Grop

                        var contrac_Grop = from t in dc.Conf_Contracts
                                           from tc in dc.Conf_Conditions
                                           from sp in dc.SpareParts
                                           where (t.ContractNo == _contractNo) &&
                                                 ((t.ContractNo == tc.ContractNo) &&
                                                 (tc.SPA_ID == sp.SPA_ID)) &&
                                                 (t.GOP_ID == tmpGOPId) && //ตามสถานี...
                                                 (tc.TYP_ID1 == tmpTYP_ID1) &&
                                                 (tc.IsAutoPart == true)
                                           select new
                                           {
                                               POI_ID = tmp_POI_ID == "" ? "000" : tmp_POI_ID,
                                               PoiFailure = tmp_POI_Detail == "" ? "ไม่ระบุ" : tmp_POI_Detail,
                                               NO = tmp_NO,
                                               SPA_ID = tc.SPA_ID,
                                               PartName = sp.SparePart1,

                                               VAT = _vat,
                                               ControlFixAssetNo = fixAssetNo,
                                               PricesList = 0,
                                               Prices = _Prices,
                                               Discount = 0,
                                               SumPrices = _SumPrices,
                                               SumTotalPrices = _SumTotalPrices,
                                               SumVAT = 0,
                                               Quantity = _Quantity,
                                               StatusSpare = true,
                                               TRA_ID = tc.TRA_ID,//"IN"
                                               Comment = string.Empty,
                                               FAI_ID = tmp_FAI_ID == "" ? "0000000" : tmp_FAI_ID,
                                               Location = tmp_Location == "" ? "None" : tmp_Location,
                                               SerialNumber = SerialNumber_new == "" ? "None" : SerialNumber_new,

                                               IsPrices = false,//checkConf_JOB_IS_PrintPrice(db, txtJobID.Text.Trim(), "IN"),//false,

                                               ERP_StationCharge = (tc.IsPartPriceCharges == null ? '0' : (tc.IsPartPriceCharges == true ? '0' : '1')),
                                               sp.ERP_SPA_ID,
                                               ERP_PartName = sp.SparePart1

                                               //20161117
                                                 ,
                                               ReferPage = tc.ReferPage == null ? "" : tc.ReferPage

                                           };

                        if (contrac_Grop.Count() > 0)
                        {
                            foreach (var x1 in contrac_Grop)
                            {
                                if (!checkSPA_ID(x1.SPA_ID, (x1.StatusSpare == true ? "เปลี่ยน" : "ค้าง"),
                                x1.POI_ID, x1.FAI_ID, x1.Location, x1.TRA_ID))
                                {

                                    var _pri = getPricesList_AutoLoadPart(dc, x1.SPA_ID);

                                    DataRow dr = dtPart.NewRow();
                                    dr["รหัสจุดเสีย"] = x1.POI_ID;
                                    dr["จุดเสีย"] = x1.PoiFailure == "&nbsp;" ? "" : x1.PoiFailure;

                                    dr["NO."] = x1.NO;
                                    dr["Part no."] = x1.SPA_ID;
                                    dr["Part"] = x1.PartName;
                                    dr["Type"] = x1.TRA_ID;

                                    dr["จำนวน"] = _Quantity.ToString("#,##0.00"); //x1.Quantity;

                                    dr["PricesList"] = _PricesList.ToString("#,##0.00"); //x1.PricesList.ToString("#,##0.00");
                                    dr["ราคา/หน่วย"] = _Prices.ToString("#,##0.00"); //x1.Prices.ToString("#,##0.00");
                                    dr["รวม"] = _SumPrices.ToString("#,##0.00"); //x1.SumPrices.ToString("#,##0.00");
                                    dr["ภาษี"] = _SumVAT.ToString("#0.00");//x1.SumVAT.ToString("#,##0.00");


                                    dr["รวมสุทธิ"] = _SumTotalPrices.ToString("#,##0.00"); //= x1.SumTotalPrices.ToString("#,##0.00");
                                    dr["VAT"] = x1.VAT.ToString("#,##0.00");
                                    dr["FixAssetNo"] = x1.ControlFixAssetNo;

                                    dr["Discount"] = x1.Discount.ToString("#,##0.00");

                                    dr["สถานะ"] = x1.StatusSpare == true ? "เปลี่ยน" : "ค้าง";
                                    dr["Comment"] = x1.Comment == "&nbsp;" ? "" : x1.Comment;
                                    dr["FAI_ID"] = x1.FAI_ID;
                                    dr["Location"] = x1.Location;
                                    dr["S/N"] = x1.SerialNumber ?? "";
                                    dr["IsCancel"] = "0";
                                    dr["IsDownload"] = "N";


                                    dr["isPrices"] = checkConf_JOB_IS_PrintPrice(dc, txtJobID.Text.Trim(), "IN");//false,

                                    dr["ออกบิลในนาม"] = x1.ERP_StationCharge == '0' ? "สถานี" : "บริษัทน้ำมัน";

                                    dr["ERP_SPA_ID"] = null;//x1.ERP_SPA_ID;
                                    dr["ERP_PartName"] = null;//x1.ERP_PartName;  

                                    //==20141120==
                                    dr["อะไหล่ลูกค้า"] = "N";
                                    dr["ERP_orderline_id"] = "0";
                                    //20150612dr["ตำแหน่งที่เปลี่ยน"] = "99";
                                    //====

                                    //20161117
                                    dr["Page/Item"] = x1.ReferPage;


                                    logCancelSPA_ID.Add(x1.SPA_ID);
                                    dtPart.Rows.Add(dr);
                                }
                            }

                        }

                        #endregion

                    }

                    #endregion
                }

            }
            else
            {
                using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext(strConn))
                {

                    #region เพิ่ม Part Auto ตามเงื่อนไขสัญญา


                    //Check เงื่อนไขสัญญาตามสถานี...
                    var contrac_Sat = from t in dc.Conf_Contracts
                                      from tc in dc.Conf_Conditions
                                      from sp in dc.SpareParts
                                      where //(t.ContractNo == _contractNo) &&
                                            ((t.ContractNo == tc.ContractNo) &&
                                            (tc.SPA_ID == sp.SPA_ID)) &&
                                            (t.GOP_ID == tmpGOPId) &&
                                            (t.STA_ID == tmpSTAID) && //ตามสถานี...
                                            (tc.TYP_ID1 == tmpTYP_ID1) &&
                                            (tc.IsAutoPart == true)
                                      select new
                                      {
                                          POI_ID = tmp_POI_ID == "" ? "000" : tmp_POI_ID,
                                          PoiFailure = tmp_POI_Detail == "" ? "ไม่ระบุ" : tmp_POI_Detail,
                                          NO = tmp_NO,
                                          SPA_ID = tc.SPA_ID,
                                          PartName = sp.SparePart1,

                                          VAT = _vat,
                                          ControlFixAssetNo = fixAssetNo,
                                          PricesList = 0,
                                          Prices = _Prices,
                                          Discount = 0,
                                          SumPrices = _SumPrices,
                                          SumTotalPrices = _SumTotalPrices,
                                          SumVAT = 0,
                                          Quantity = _Quantity,
                                          StatusSpare = true,
                                          TRA_ID = tc.TRA_ID, //sp.TranType,//"IN"
                                          Comment = string.Empty,
                                          FAI_ID = tmp_FAI_ID == "" ? "0000000" : tmp_FAI_ID,
                                          Location = tmp_Location == "" ? "None" : tmp_Location,
                                          SerialNumber = SerialNumber_new == "" ? "None" : SerialNumber_new,

                                          IsPrices = false,//checkConf_JOB_IS_PrintPrice(db, txtJobID.Text.Trim(), "IN"),//false,

                                          ERP_StationCharge = (tc.IsPartPriceCharges == null ? '0' : (tc.IsPartPriceCharges == true ? '0' : '1')),
                                          sp.ERP_SPA_ID,
                                          ERP_PartName = sp.SparePart1

                                          //20161117
                                           ,
                                          ReferPage = tc.ReferPage == null ? "" : tc.ReferPage


                                      };


                    if (contrac_Sat.Count() > 0)
                    {
                        #region contrac_Sat

                        foreach (var x1 in contrac_Sat)
                        {
                            if (!checkSPA_ID(x1.SPA_ID, (x1.StatusSpare == true ? "เปลี่ยน" : "ค้าง"),
                                x1.POI_ID, x1.FAI_ID, x1.Location, x1.TRA_ID))
                            {

                                var _pri = getPricesList_AutoLoadPart(dc, x1.SPA_ID);
                                DataRow dr = dtPart.NewRow();
                                dr["รหัสจุดเสีย"] = x1.POI_ID;
                                dr["จุดเสีย"] = x1.PoiFailure == "&nbsp;" ? "" : x1.PoiFailure;

                                dr["NO."] = x1.NO;
                                dr["Part no."] = x1.SPA_ID;
                                dr["Part"] = x1.PartName;
                                dr["Type"] = x1.TRA_ID;

                                dr["จำนวน"] = _Quantity.ToString("#,##0.00"); //x1.Quantity;

                                dr["PricesList"] = _PricesList.ToString("#,##0.00"); //x1.PricesList.ToString("#,##0.00");
                                dr["ราคา/หน่วย"] = _Prices.ToString("#,##0.00"); //x1.Prices.ToString("#,##0.00");
                                dr["รวม"] = _SumPrices.ToString("#,##0.00"); //x1.SumPrices.ToString("#,##0.00");
                                dr["ภาษี"] = _SumVAT.ToString("#0.00");//x1.SumVAT.ToString("#,##0.00");

                                dr["รวมสุทธิ"] = _SumTotalPrices.ToString("#,##0.00"); //= x1.SumTotalPrices.ToString("#,##0.00");

                                dr["VAT"] = x1.VAT.ToString("#,##0.00");
                                dr["FixAssetNo"] = x1.ControlFixAssetNo;

                                dr["Discount"] = x1.Discount.ToString("#,##0.00");

                                dr["สถานะ"] = x1.StatusSpare == true ? "เปลี่ยน" : "ค้าง";
                                dr["Comment"] = x1.Comment == "&nbsp;" ? "" : x1.Comment;
                                dr["FAI_ID"] = x1.FAI_ID;
                                dr["Location"] = x1.Location;
                                dr["S/N"] = x1.SerialNumber ?? "";
                                dr["IsCancel"] = "0";
                                dr["IsDownload"] = "N";

                                dr["isPrices"] = checkConf_JOB_IS_PrintPrice(dc, txtJobID.Text.Trim(), "IN");//false,
                                //x1.ERP_StationCharge == '0' ? "1" : "0";
                                dr["ออกบิลในนาม"] = x1.ERP_StationCharge == '0' ? "สถานี" : "บริษัทน้ำมัน";

                                dr["ERP_SPA_ID"] = null;//x1.ERP_SPA_ID;
                                dr["ERP_PartName"] = null;//x1.ERP_PartName;  

                                //==20141120==
                                dr["อะไหล่ลูกค้า"] = "N";
                                dr["ERP_orderline_id"] = "0";
                                //20150612dr["ตำแหน่งที่เปลี่ยน"] = "99";
                                //====

                                //20161117
                                dr["Page/Item"] = x1.ReferPage;

                                logCancelSPA_ID.Add(x1.SPA_ID);
                                dtPart.Rows.Add(dr);
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        //Check เงื่อนไขสัญญาตามกลุ่มบริษัท...
                        #region contrac_Grop

                        var contrac_Grop = from t in dc.Conf_Contracts
                                           from tc in dc.Conf_Conditions
                                           from sp in dc.SpareParts
                                           where //(t.ContractNo == _contractNo) &&
                                                 ((t.ContractNo == tc.ContractNo) &&
                                                 (tc.SPA_ID == sp.SPA_ID)) &&
                                                 (t.GOP_ID == tmpGOPId) && //ตามสถานี...
                                                 (tc.TYP_ID1 == tmpTYP_ID1) &&
                                                 (tc.IsAutoPart == true)
                                           select new
                                           {
                                               POI_ID = tmp_POI_ID == "" ? "000" : tmp_POI_ID,
                                               PoiFailure = tmp_POI_Detail == "" ? "ไม่ระบุ" : tmp_POI_Detail,
                                               NO = tmp_NO,
                                               SPA_ID = tc.SPA_ID,
                                               PartName = sp.SparePart1,

                                               VAT = _vat,
                                               ControlFixAssetNo = fixAssetNo,
                                               PricesList = 0,
                                               Prices = _Prices,
                                               Discount = 0,
                                               SumPrices = _SumPrices,
                                               SumTotalPrices = _SumTotalPrices,
                                               SumVAT = 0,
                                               Quantity = _Quantity,
                                               StatusSpare = true,
                                               TRA_ID = tc.TRA_ID,//"IN"
                                               Comment = string.Empty,
                                               FAI_ID = tmp_FAI_ID == "" ? "0000000" : tmp_FAI_ID,
                                               Location = tmp_Location == "" ? "None" : tmp_Location,
                                               SerialNumber = SerialNumber_new == "" ? "None" : SerialNumber_new,

                                               IsPrices = false,//checkConf_JOB_IS_PrintPrice(db, txtJobID.Text.Trim(), "IN"),//false,

                                               ERP_StationCharge = (tc.IsPartPriceCharges == null ? '0' : (tc.IsPartPriceCharges == true ? '0' : '1')),
                                               sp.ERP_SPA_ID,
                                               ERP_PartName = sp.SparePart1

                                               //20161117
                                                 ,
                                               ReferPage = tc.ReferPage == null ? "" : tc.ReferPage

                                           };

                        if (contrac_Grop.Count() > 0)
                        {
                            foreach (var x1 in contrac_Grop)
                            {
                                if (!checkSPA_ID(x1.SPA_ID, (x1.StatusSpare == true ? "เปลี่ยน" : "ค้าง"),
                                x1.POI_ID, x1.FAI_ID, x1.Location, x1.TRA_ID))
                                {

                                    var _pri = getPricesList_AutoLoadPart(dc, x1.SPA_ID);

                                    DataRow dr = dtPart.NewRow();
                                    dr["รหัสจุดเสีย"] = x1.POI_ID;
                                    dr["จุดเสีย"] = x1.PoiFailure == "&nbsp;" ? "" : x1.PoiFailure;

                                    dr["NO."] = x1.NO;
                                    dr["Part no."] = x1.SPA_ID;
                                    dr["Part"] = x1.PartName;
                                    dr["Type"] = x1.TRA_ID;

                                    dr["จำนวน"] = _Quantity.ToString("#,##0.00"); //x1.Quantity;

                                    dr["PricesList"] = _PricesList.ToString("#,##0.00"); //x1.PricesList.ToString("#,##0.00");
                                    dr["ราคา/หน่วย"] = _Prices.ToString("#,##0.00"); //x1.Prices.ToString("#,##0.00");
                                    dr["รวม"] = _SumPrices.ToString("#,##0.00"); //x1.SumPrices.ToString("#,##0.00");
                                    dr["ภาษี"] = _SumVAT.ToString("#0.00");//x1.SumVAT.ToString("#,##0.00");


                                    dr["รวมสุทธิ"] = _SumTotalPrices.ToString("#,##0.00"); //= x1.SumTotalPrices.ToString("#,##0.00");
                                    dr["VAT"] = x1.VAT.ToString("#,##0.00");
                                    dr["FixAssetNo"] = x1.ControlFixAssetNo;

                                    dr["Discount"] = x1.Discount.ToString("#,##0.00");

                                    dr["สถานะ"] = x1.StatusSpare == true ? "เปลี่ยน" : "ค้าง";
                                    dr["Comment"] = x1.Comment == "&nbsp;" ? "" : x1.Comment;
                                    dr["FAI_ID"] = x1.FAI_ID;
                                    dr["Location"] = x1.Location;
                                    dr["S/N"] = x1.SerialNumber ?? "";
                                    dr["IsCancel"] = "0";
                                    dr["IsDownload"] = "N";


                                    dr["isPrices"] = checkConf_JOB_IS_PrintPrice(dc, txtJobID.Text.Trim(), "IN");//false,

                                    dr["ออกบิลในนาม"] = x1.ERP_StationCharge == '0' ? "สถานี" : "บริษัทน้ำมัน";

                                    dr["ERP_SPA_ID"] = null;//x1.ERP_SPA_ID;
                                    dr["ERP_PartName"] = null;//x1.ERP_PartName;  

                                    //==20141120==
                                    dr["อะไหล่ลูกค้า"] = "N";
                                    dr["ERP_orderline_id"] = "0";
                                    //20150612dr["ตำแหน่งที่เปลี่ยน"] = "99";
                                    //====

                                    //20161117
                                    dr["Page/Item"] = x1.ReferPage;

                                    logCancelSPA_ID.Add(x1.SPA_ID);
                                    dtPart.Rows.Add(dr);
                                }
                            }

                        }

                        #endregion

                    }

                    #endregion
                }
            }

            if (dtPart.Rows.Count <= 0 && dataGridView_FailPoint.RowCount == 1)
            {//   dr["รหัสจุดเสีย"] = "000";
                if (dataGridView_FailPoint["รหัสจุดเสีย", 0].Value.ToString() == "000")
                {
                    setDtFailure();
                    FailureBinding();
                }
            }
        }


        private decimal getPricesList_AutoLoadPart(SMSManageDataContext dc, string partNo)
        {

            _PricesList = 0;
            _Prices = 0;
            _Discount = 0;
            _SumPrices = 0;
            _SumTotalPrices = 0;
            _SumVAT = 0;
            _Quantity = 1;


            //Check ข้อมูลในตาราง SpareParts_IsTravel
            var iTv = (from t in dc.SpareParts_IsTravels
                       where t.GOP_ID == tmpGOPId &&
                       t.SPA_ID == partNo
                       select t).FirstOrDefault();

            if (iTv != null)//Part Travel
            {
                #region Check price Conf_Conditions
                decimal acDistance = 0;
                decimal dFrom = 0;
                decimal perKM = 0;


                var conP = dc.Conf_Conditions
                    .Where(t => t.SPA_ID == partNo &&
                    t.GOP_ID == tmpGOPId &&
                   //t.STA_ID == "" &&
                   t.TYP_ID == tmpJobType &&
                   t.TYP_ID1 == tmpTYP_ID1).FirstOrDefault();
                if (conP != null && conP.PriceList > 0)
                {
                    perKM = conP.PriceList;
                }
                else
                {

                    #region Check Price list
                    //===20160929====เอาออก ไม่ใช้ pricelist
                    //Check Price list
                    //var pp = (from t in dc.SparePart_Prices_Lists
                    //          from t1 in dc.SpareParts_IsPrices
                    //          where ((t.SPA_ID == t1.SPA_ID) &&
                    //          t.SPA_ID == partNo && t1.GOP_ID == tmpGOPId)
                    //          select t).FirstOrDefault();

                    //if (pp != null)
                    //    perKM = pp.Prices;
                    //else
                    //{

                    var pp1 = dc.SpareParts.Where(t => t.SPA_ID == partNo)
                        .FirstOrDefault();
                    if (pp1 != null)
                        perKM = pp1.PricePerUnit.Value;
                    //} //===20160929====เอาออก ไม่ใช้ pricelist



                    #endregion
                }

                _PricesList = perKM;
                _Prices = perKM;
                _Discount = 0;

                #endregion



                var st = dc.Station_Distances
                  .Where(t => t.STA_ID == tmpSTAID && t.TYP_ID == _job.TYP_ID)
                  .FirstOrDefault();

                if (st != null)
                {
                    acDistance = st.ActualDistance;
                    dFrom = st.DistanceFromCenter ?? 0;

                    _SumPrices = ((acDistance - dFrom) * perKM);
                    double _xvat = Convert.ToDouble(_vat) > 0 ? Convert.ToDouble(_SumPrices) * (Convert.ToDouble(_vat) / 100.00) : 0;
                    _SumVAT = Convert.ToDecimal(_xvat);

                    _SumTotalPrices = ((acDistance - dFrom) * perKM) + _SumVAT;
                    _Quantity = (acDistance - dFrom);

                }
                else
                    _Quantity = 0;

            }
            else//ไม่ใช่ Part Travel
            {
                #region Part ไม่อ้างอิงโปรเจ็กต์

                var quantity = 1;
                double _xvat = 0;
                // --where contract เพิ่ม
                var conP = dc.Conf_Conditions
                      .Where(t => t.SPA_ID == partNo &&
                      t.GOP_ID == tmpGOPId &&
                     t.TYP_ID == tmpJobType &&
                     t.TYP_ID1 == tmpTYP_ID1).FirstOrDefault();
                if (conP != null && conP.PriceList > 0)
                {
                    #region Check on Conf_Conditions

                    _PricesList = conP.PriceList;
                    _Prices = conP.PriceList * quantity;



                    _SumPrices = _Prices;
                    _xvat = Convert.ToDouble(_vat) > 0 ? Convert.ToDouble(_SumPrices) * (Convert.ToDouble(_vat) / 100.00) : 0;
                    _SumVAT = Convert.ToDecimal(_xvat);


                    _SumTotalPrices = _Prices + _SumVAT;

                    _Quantity = quantity;




                    #endregion
                }
                else
                {

                    #region Check Price list

                    ////var q = (from t in dc.SpareParts where t.SPA_ID == partNo select t).FirstOrDefault();

                    ////var qIsPrice = (from t in dc.SpareParts_IsPrices where t.SPA_ID == partNo && t.GOP_ID == tmpGOPId select t).FirstOrDefault();
                    ////var stg = (from t in dc.Station_Groups where t.GOP_ID == tmpGOPId select t).FirstOrDefault();
                    ////var stGroupName = stg == null ? string.Empty : stg.GroupStation;

                    ////DateTime openjob = Convert.ToDateTime(Convert.ToDateTime(txtOpenJOB.Text).ToShortDateString());

                    ////var pri = (from pl in dc.SparePart_Prices_Lists
                    ////           from isp in dc.SpareParts_IsPrices
                    ////           from pq in dc.SparePart_Quarters
                    ////           from tg in dc.Station_Groups
                    ////           where (pl.PRI_ID == pq.PRI_ID) &&
                    ////                (pl.SPA_ID == isp.SPA_ID && tg.GOP_ID == isp.GOP_ID) &&
                    ////                (pq.GOP_ID == tg.GOP_ID) &&
                    ////                (pl.SPA_ID == partNo) &&
                    ////                 tg.GOP_ID == tmpGOPId &&
                    ////                 pq.StartDate <= openjob && pq.EndDate >= openjob
                    ////           select new
                    ////           {
                    ////               pl.SPA_ID,
                    ////               pq.localDiscount,
                    ////               pq.globalDiscount,
                    ////               pq.decRoundUp,//ปัดเศษ
                    ////               pl.Prices,
                    ////               pq.Quarter

                    ////           }).FirstOrDefault();




                    ////if (pri != null)
                    ////{
                    ////    var _discount = 0.0;
                    ////    //var quantity = 1;
                    ////    var roundup = Convert.ToInt32(pri.decRoundUp ?? 0);//ปัดเศษ
                    ////    tmpRounup = roundup;

                    ////    //txtPriceList.Text = pri.Prices.ToString("#,##0.00");
                    ////    _PricesList = pri.Prices;
                    ////    var _prices = Convert.ToDouble(pri.Prices);//Price list

                    ////    if (qIsPrice != null)
                    ////    {
                    ////        if (qIsPrice.IsGlobalDiscount.Value != '2')
                    ////        {
                    ////            if (qIsPrice.IsGlobalDiscount.Value == '1')
                    ////                _discount = Convert.ToDouble(pri.globalDiscount);//ใช้ส่วนลดต่างประเทศ
                    ////            else
                    ////                _discount = Convert.ToDouble(pri.localDiscount);//ใช้ส่วนลดในประเทศ

                    ////            //txtDiscQuarter.Text = _discount.ToString(); //ส่วนลด
                    ////            _Discount = decimal.Parse(_discount.ToString());
                    ////            if (_discount <= 0)
                    ////                _Prices = Math.Round(pri.Prices * quantity, tmpRounup);
                    ////            else
                    ////                _Prices = decimal.Parse(Math.Round((_prices - ((_discount / 100) * _prices)) * quantity, tmpRounup).ToString());


                    ////        }
                    ////        else
                    ////        {
                    ////            //txtDiscQuarter.Text = "0";
                    ////            if (tmpRounup > 0)
                    ////                _Prices = decimal.Parse(Math.Round(Convert.ToDouble(_prices) * quantity, tmpRounup).ToString());
                    ////            else
                    ////                _Prices = decimal.Parse((Convert.ToDouble(_prices) * quantity).ToString());

                    ////        }// if (q.IsDiscount == true)

                    ////        if (qIsPrice.IsPrices != null)
                    ////        {
                    ////            chIsPrices.Checked = qIsPrice.IsPrices.Value;
                    ////            //chIsPrices.Enabled = false;
                    ////        }
                    ////        else
                    ////        {
                    ////            chIsPrices.Checked = false;
                    ////            //chIsPrices.Enabled = false;
                    ////        }

                    ////        _SumPrices = _Prices;
                    ////        _SumTotalPrices = _Prices;
                    ////        _Quantity = quantity;
                    ////    }
                    ////    else
                    ////    {

                    ////        _prices = Convert.ToDouble(q.PricePerUnit.Value == 0 ? 0 : q.PricePerUnit.Value);
                    ////        //txtDiscQuarter.Text = "0";

                    ////        _SumPrices = _Prices;
                    ////        _SumTotalPrices = _Prices;
                    ////        _Quantity = quantity;


                    ////    }
                    ////}
                    ////else
                    ////{
                    ////    //txtPriceList.Text = "0";
                    ////    // txtDiscQuarter.Text = "0";

                    ////    var _prices = Convert.ToDouble(q.PricePerUnit.Value == 0 ? 0 : q.PricePerUnit.Value);

                    ////    _PricesList = 0;
                    ////    _Prices = 0;
                    ////    _Discount = 0;
                    ////    _SumPrices = 0;
                    ////    _SumTotalPrices = 0;
                    ////    _SumVAT = 0;
                    ////    _Quantity = 1;
                    ////}
                    
                    
                    #endregion



                    //var _prices = 0;

                    _PricesList = 0;
                    _Prices = 0;
                    _Discount = 0;
                    _SumPrices = 0;
                    _SumTotalPrices = 0;
                    _SumVAT = 0;
                    _Quantity = 1;
                }


                #endregion
            }

            return _PricesList;
        }

        private void CancelPartTravel()
        {
            strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext(strConn))
            {
                #region Check Station_Travel_Center Cancel Part TRAVEL

                var tvCenter = (from tv in dc.Station_Travel_Centers
                                from st in dc.Stations
                                from j in dc.JOBs
                                where ((j.JOB_ID == tmpjobID) &&
                                (j.TYP_ID == tv.TYP_ID) &&//Job Type                              
                                (j.STA_ID == st.STA_ID)) &&
                                ((tv.GOP_ID == st.GOP_ID) &&
                                (tv.PRO_ID == st.PRO_ID))
                                select tv).Distinct();



                if (tvCenter.Count() > 0)// สถานี อยู่ในศูนย์
                {

                    if (dataGridView_Part.RowCount > 0)// มีอะไหล่ Auto load
                    {

                        string item = dataGridView_Part["Part no.", 0].Value.ToString();
                        var istv = dc.SpareParts_IsTravels.Where(t => t.SPA_ID == item).FirstOrDefault();


                    RemoveItem:
                        if (istv != null)//(item == "TRAVEL")
                            deletePartTRANVEL(istv.SPA_ID);


                        for (int i = 0; i <= dataGridView_Part.RowCount - 1; i++)
                        {
                            item = dataGridView_Part["Part no.", i].Value.ToString();
                            istv = dc.SpareParts_IsTravels.Where(t => t.SPA_ID == item).FirstOrDefault();

                            //item = dataGridView_Part["Part no.", i].Value.ToString();
                            if (istv != null)//(item == "TRAVEL")
                                goto RemoveItem;
                        }
                    }
                }

                #endregion
            }
        }
        private void deletePartTRANVELandSERVICE()
        {
            for (int i = 0; i <= dataGridView_Part.RowCount - 1; i++)
            {
                string item = dataGridView_Part["Part no.", i].Value.ToString();

                if (item == "SERVICE3" || item == "TRAVEL")
                {
                    dtPart.Rows[i].Delete();
                    PartBindind();
                    return;

                }
            }
        }
        private void deletePartTRANVEL(string spa_id)
        {
            for (int i = 0; i <= dataGridView_Part.RowCount - 1; i++)
            {
                string item = dataGridView_Part["Part no.", i].Value.ToString();

                if (item == spa_id)//"TRAVEL")
                {
                    dtPart.Rows[i].Delete();
                    PartBindind();
                    return;

                }
            }
        }


        //private void PopupConditions_Detail(string JOBID)
        //{
        //    try
        //    {


        //        using (DAL.SMSManage.SMSManageDataContext dc =
        //            new SMSMINI.DAL.SMSManage.SMSManageDataContext())
        //        {

        //            var q = (from t in dc.JOBs
        //                     from s in dc.Stations
        //                     from v in dc.vw_mini_pop_Conf_Conditions_Details
        //                     where t.JOB_ID == JOBID &&
        //                         //(v.PopPage.Value == "1"[0] || v.PopPage.Value == "3"[0]) &&
        //                     v.GOP_ID == s.GOP_ID &&
        //                     v.TYP_ID == t.TYP_ID &&
        //                     v.TYP_ID1 == t.TYP_ID1 &&
        //                     (t.STA_ID == s.STA_ID)

        //                     select v).ToList();


        //            if (q.Count() > 0)
        //            {
        //                var f = new Transaction.PopupConditions_Detail();
        //                f.lsPopupC = q;
        //                f.ShowDialog();
        //            }
        //            else
        //            {
        //                MessageBox.Show("ไม่มีรายละเอียด เงื่อนไขสัญญา...", "ผลการตรวจสอบ",
        //                 MessageBoxButtons.OK, MessageBoxIcon.Information);
        //            }
        //        }

        //    }
        //    catch (Exception)
        //    {

        //        MessageBox.Show("กรุณาเชื่อมต่อ อินเตอร์เน็ต...", "ผลการตรวจสอบ",
        //             MessageBoxButtons.OK, MessageBoxIcon.Information);
        //    }
        //}
        private void PopupConditions_Detail()
        {

            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            {
                var q = (from t in dc.JOBs
                         from s in dc.Stations
                         from v in dc.vw_mini_pop_Conf_Conditions_Details
                         where (v.PopPage.Value == "2"[0]) &&
                         v.GOP_ID == s.GOP_ID &&
                         v.TYP_ID == t.TYP_ID &&
                         v.TYP_ID1 == t.TYP_ID1 &&
                         (t.STA_ID == s.STA_ID) &&
                          (
                             t.EMP_ID3 == UserInfo.Van_ID && (t.IsUpload == "0"[0] || t.IsUpload == null) &&
                             new string[] { "05", "06" }.Contains(t.JOBS_ID) && // Download JOB เข้างาน เท่านั้น
                             t.TYP_ID != "46" && //JOB งาน เบิกค่า ใช้จ่ายพิเศษ/Mileage
                             t.TYP_ID1 != "ML"
                          )

                         select v).ToList();


                if (q.Count() > 0)
                {
                    var f = new Transaction.PopupConditions_Detail();
                    f.lsPopupC = q;
                    f.ShowDialog();
                }
            }
        }
        private bool checkPartRemaiCloseJob()
        {
            for (int i = 0; i <= dataGridView_Part.Rows.Count - 1; i++)
            {
                if ((dataGridView_Part["สถานะ", i].Value.ToString().Trim() == "ค้าง" && dataGridView_Part["IsCancel", i].Value.ToString().Trim() == "0") && RadioButtonList_JobStatus2.Checked == true)
                    return false;
            }

            return true;
        }

        //--=======EditJOB=====================================================
        //--=======EditJOB=====================================================

        private void EditJOB()
        {
            string strPoint = "";
            DateTime tmpStartDate = DateTime.Now;
            DateTime tmpEndDate = DateTime.Now;
            DateTime tmpLastEditDate = DateTime.Now;

            string tmpVAN_ID = "";

            //if (UserInfo.ConnectMode == "0")
            //    strConn = Properties.Settings.Default["ServicesMSDBConnectionString"].ToString();
            //else
            ////strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
            //{
            if (checkBox_SMS_BAK.Checked == true)
                strConn = SMSMINI.Properties.Settings.Default.ServicesMSDB_BAKConnectionString;
            else
                strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
            //}

            //20170616 using (DAL.SMSManage.SMSManageDataContext db = new DAL.SMSManage.SMSManageDataContext(strConn))
            using (DAL.SMSManage.SMSManageDataContext db = new SMSMINI.DAL.SMSManage.SMSManageDataContext())

            {
                try
                {
                    db.Connection.Open();
                    db.Transaction = db.Connection.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted);


                    //======================================
                    // Close JOB
                    //======================================
                    #region Edit JOB
                    strPoint = "Update Close JOB"; //แก้ไขเฉพาะ Job ที่ค้าง และ ปิด

                    var qj = (from t in db.JOBs where t.JOB_ID == txtJobID.Text.Trim() select t).First();

                    if (qj == null)
                    {
                        MessageBox.Show("ตรวจสอบไม่พบ JOB กรุณาติดต่อผู้ดูแลระบบ", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    if (RadioButtonList_JobStatus2.Checked == true)//CloseJOB
                    {
                        if (qj != null)
                        {
                            char IsSpecialCharge = '0';
                            if (UserInfo.UserId.ToLower().StartsWith("v"))
                                if (MessageBox.Show("JOB [" + txtJobID.Text.Trim() + "] นี้ ต้องเก็บเงินพิเศษ หรือไม่ ?...", "ยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                    IsSpecialCharge = '1';

                            //20170608 qj.CloseDate = tmpEndDate;
                            //20170608 qj.JOBS_ID = "99";

                            //20170608 if (UserInfo.Van_ID != UserInfo.SUP_VAN)//Sup แก้ไข JOB ไม่ต้อง Update VAN                                   
                            //20170608     qj.EMP_ID3 = UserInfo.UserId;

                            qj.PMT_ID = cobProblemType.SelectedValue.ToString();
                            //20170608 qj.TCO_ID = "04";//ประเภทการปิด JOB {04 = เข้า Site (ผ่าน web)}
                            //20170608 qj.PRI_ID = comboBox_Priority.SelectedValue.ToString();
                            //20170608 qj.IsUpload = "0"[0];


                            //qj.IsSpecialCharge = IsSpecialCharge;

                            //20170619
                            qj.LastEditBy = UserInfo.UserId;
                            qj.LastEditDate = tmpLastEditDate;


                        }

                        //20170608 db.SubmitChanges();
                    }
                    else//JOB ค้าง
                    {
                        if (qj != null)
                        {
                            //20170616 qj.JOBS_ID = "07";
                            //20170616 qj.IsUpload = "0"[0];

                            //20170616 if (UserInfo.Van_ID != UserInfo.SUP_VAN)//Sup แก้ไข JOB ไม่ต้อง Update VAN                                   
                            //20170616     qj.EMP_ID3 = UserInfo.UserId;

                            qj.PMT_ID = cobProblemType.SelectedValue.ToString();
                            //20170616 qj.PRI_ID = comboBox_Priority.SelectedValue.ToString();


                            if (isPartRement())//ตรวจสอบอะไหล่ค้าง
                            {
                                //ตรวจสอบ ค้าง JOB เพื่อทำเสนอราคา
                                //IsQuotation = null , 0= ไม่เสนอราคา, 1=ต้องทำใบเสนอราคา, 2=ทำใบเสนอราคาแล้ว
                                if (isPartRement())
                                {
                                    if (MessageBox.Show("JOB [" + txtJobID.Text.Trim() + "] นี้ ต้องการทำใบเสนอราคาหรือไม่?...", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                        qj.IsQuotation = "1"[0];
                                    else
                                        qj.IsQuotation = "0"[0];
                                }
                            }
                            //20160713
                            //20170616 qj.TCO_ID = "04";//ประเภทการปิด JOB {04 = เข้า Site (ผ่าน web)}
                        }

                        //20170619
                        qj.LastEditBy = UserInfo.UserId;
                        qj.LastEditDate = tmpLastEditDate;

                        //20170608 db.SubmitChanges();
                    }

                    #endregion


                    //======================================
                    // JOB_ContentMent (บันทึกข้อมูลความพึงพอใจ)
                    //======================================
                    #region Insert JOB_ContentMent
                    /*    //20170616 น่าจะไม่ต้องบันทึกเพราะแค่แก้ JOB Online   
                      strPoint = "Insert JOB_ContentMent";



                           string _jobremark = (tmpRemarkJobContentMent == "") ? null : tmpRemarkJobContentMent;
                           string JobCont = "";
                           if (radioButton_A.Checked) JobCont = "A";//(A) ดี
                           else if (radioButton_B.Checked) JobCont = "B";//(B) พอใช้
                           else if (radioButton_C.Checked) JobCont = "C";//(C) ปรับปรุง
                           else if (radioButton_D.Checked) JobCont = "D";//(D) ไม่ระบุ               

                           string _cmdStatus = "0";
                           if ((from t in db.JOB_ContentMents
                                where t.JOB_ID == txtJobID.Text.Trim() && t.ContentStatus == _cmdStatus[0] && t.CMD_ID == "00"

                                select t).Count() > 0)
                           {
                               var delq11 = from t in db.JOB_ContentMents
                                            where t.JOB_ID == txtJobID.Text.Trim() && t.ContentStatus == _cmdStatus[0] && t.CMD_ID == "00"
                                            select t;
                               db.JOB_ContentMents.DeleteAllOnSubmit(delq11);
                               //20170608 db.SubmitChanges();
                           }

                           if (JobCont != "")
                           {
                               JOB_ContentMent jcm = new JOB_ContentMent()
                               {
                                   JOB_ID = txtJobID.Text.Trim(),
                                   CMD_ID = "00",//ความคิดเห็นของเจ้าหน้าที่ Flowco
                                   ContentStatus = _cmdStatus[0],
                                   JobContentMent = JobCont,
                                   Remark = _jobremark
                               };
                               db.JOB_ContentMents.InsertOnSubmit(jcm);
                               //20170608 db.SubmitChanges();
                           }
                     */
                    #endregion
                    //======================================
                    // Update JOB_Detail
                    //======================================
                    #region Get StartDate
                    strPoint = "get JOB_Detail StartDate";

                    var qjd = (from t in db.JOB_Details
                               where t.JOB_ID == txtJobID.Text.Trim() && t.StatusDetail == true
                               orderby t.RecDate descending
                               select t).FirstOrDefault();

                    if (qjd != null)
                    {
                        tmpStartDate = qjd.StartDate;
                        //tmpEndDate = qjd.EndDate ?? DateTime.Now;
                        tmpVAN_ID = qjd.EMP_ID ?? UserInfo.UserId;

                    }

                    bool TypeJobRemain = false;

                    #endregion



                    //======================================
                    // Insert JOB_Failure_Detail
                    //======================================
                    #region Insert JOB_Failure_Detail
                    strPoint = "Insert JOB_Failure_Detail";

                    //20150126
                    string vProblem_Detail = null;

                    if ((from t in db.JOB_Failure_Details where t.JOB_ID == txtJobID.Text.Trim() select t).Count() > 0)
                    {
                        var delq11 = from t in db.JOB_Failure_Details
                                     where t.JOB_ID == txtJobID.Text.Trim()
                                     select t;
                        //20170608 db.JOB_Failure_Details.DeleteAllOnSubmit(delq11);
                        //20170608 db.SubmitChanges();

                    }


                    for (int i = 0; i <= dataGridView_FailPoint.Rows.Count - 1; i++)
                    {
                        //---20150126
                        var qATF = (from t in db.Failure_Actions
                                    where t.ATF_ID == dataGridView_FailPoint["ATF_ID", i].Value.ToString()
                                    select t).FirstOrDefault();

                        if (qATF != null)
                        {

                            vProblem_Detail += "รหัสจุดเสีย " + dataGridView_FailPoint["รหัสจุดเสีย", i].Value.ToString()
                            + ": " + dataGridView_FailPoint["ATF_ID", i].Value.ToString()
                            + " " + qATF.Action_Failure + "  ";

                        }
                        //---20150126

                        string tmp_POI_ID = dataGridView_FailPoint["รหัสจุดเสีย", i].Value.ToString();
                        string tmp_Failure_Detail = dataGridView_FailPoint["อาการเสีย", i].Value.ToString();
                        string SerialNumber = dataGridView_FailPoint["S/N(Old)", i].Value.ToString();
                        string fixAssetNo = dataGridView_FailPoint["FixAssetNo", i].Value.ToString();

                        string SerialNumber_new = dataGridView_FailPoint["S/N(New)", i].Value.ToString();
                        string fixAssetNo_New = dataGridView_FailPoint["FixAssetNo_New", i].Value.ToString();

                        string tmp_FAI_ID = dataGridView_FailPoint["FAI_ID", i].Value.ToString();
                        //string tmp_Location = dataGridView_FailPoint["Location", i].Value.ToString();
                        //string tmp_Location = dataGridView_FailPoint["Location", i].Value.ToString() == null ? "0" : dataGridView_FailPoint["Location", i].Value.ToString();//dataGridView_FailPoint["Location", i].Value.ToString();


                        /* //20150709
                        string tmp_Location = "";
                        if (string.IsNullOrEmpty(dataGridView_FailPoint["Location", i].Value.ToString()))
                        {
                            tmp_Location = "0";
                        }
                        else
                        {
                            tmp_Location = dataGridView_FailPoint["Location", i].Value.ToString();
                        }
                        */


                        //=============20150709==================
                        int _LOC_ID = 0;
                        string _vfLocation = dataGridView_FailPoint["Location", i].Value.ToString().Trim();

                        int _cfLocation = 0;

                        if (dataGridView_FailPoint["Location", i].Value != null)
                            _cfLocation = dataGridView_FailPoint["Location", i].Value.ToString().Trim().Length;

                        if ((_cfLocation > 2 || _vfLocation == null))
                        {
                            //_vLocation
                            _LOC_ID = 0;
                        }
                        else
                        {
                            var qfLocate = (from t in db.Locations
                                            where Convert.ToString(t.LOC_ID) == _vfLocation //dataGridView_Part["SPA_ID", i].Value.ToString()
                                            select t).FirstOrDefault();
                            if (qfLocate == null)
                            {
                                _LOC_ID = 0;
                            }
                            else
                            {
                                _LOC_ID = qfLocate.LOC_ID;
                            }
                        }
                        //===============20150709================


                        string tmp_Model = dataGridView_FailPoint["Model", i].Value.ToString();

                        decimal tmp_StartLiter = Convert.ToDecimal(dataGridView_FailPoint["StartLiter", i].Value.ToString() == "&nbsp;" ? "0" : dataGridView_FailPoint["StartLiter", i].Value.ToString());
                        decimal tmp_EndLiter = Convert.ToDecimal(dataGridView_FailPoint["EndLiter", i].Value.ToString() == "&nbsp;" ? "0" : dataGridView_FailPoint["EndLiter", i].Value.ToString());
                        decimal tmp_LiterTest = Convert.ToDecimal(dataGridView_FailPoint["LiterTest", i].Value.ToString() == "&nbsp;" ? "0" : dataGridView_FailPoint["LiterTest", i].Value.ToString());

                        int _fcatID = 0;
                        int _fbaID = 0;
                        int _fmdID = 0;

                        //20141204
                        //20150709 ใช้ process แปลง int _LOC_ID = 0;

                        if (dataGridView_FailPoint["FCAT_ID", i].Value != DBNull.Value)
                            _fcatID = Convert.ToInt32(dataGridView_FailPoint["FCAT_ID", i].Value);

                        if (dataGridView_FailPoint["FBA_ID", i].Value != DBNull.Value)
                            _fbaID = Convert.ToInt32(dataGridView_FailPoint["FBA_ID", i].Value);

                        if (dataGridView_FailPoint["FMD_ID", i].Value != DBNull.Value)
                            _fmdID = Convert.ToInt32(dataGridView_FailPoint["FMD_ID", i].Value);

                        //20141204
                        //20150709 ใช้ process แปลง _LOC_ID = Convert.ToInt32(tmp_Location);


                        //===20141022
                        int _fPRT_ID = 0;
                        //20141127ยกเลิก int _fEQP_ID = 0;
                        int _fFAS_ID = 0;

                        if (dataGridView_FailPoint["PRT_ID", i].Value != DBNull.Value)
                            _fPRT_ID = Convert.ToInt32(dataGridView_FailPoint["PRT_ID", i].Value);

                        //20141127ยกเลิกif (dataGridView_FailPoint["EQP_ID", i].Value != DBNull.Value)
                        //20141127ยกเลิก _fEQP_ID = Convert.ToInt32(dataGridView_FailPoint["EQP_ID", i].Value);

                        string _fATF_ID = dataGridView_FailPoint["ATF_ID", i].Value.ToString();

                        if (dataGridView_FailPoint["FAS_ID", i].Value != DBNull.Value)
                            _fFAS_ID = Convert.ToInt32(dataGridView_FailPoint["FAS_ID", i].Value);

                        string SolvingByWI = dataGridView_FailPoint["SolvingByWI", i].Value.ToString();

                        //20150126



                        /* dal 20170626
                        JOB_Failure_Detail jf = new JOB_Failure_Detail()
                        {
                            NO = i + 1,
                            JOB_ID = txtJobID.Text.Trim(),
                            POI_ID = tmp_POI_ID,
                            FAI_ID = tmp_FAI_ID == "" ? "0000000" : tmp_FAI_ID,
                            Failure_Detail = tmp_Failure_Detail == "" ? null : tmp_Failure_Detail.Trim(),
                            trandate = DateTime.Now,
                            SerialNumber = SerialNumber,
                            FixAssetNo = fixAssetNo,
                            SerialNumber_New = SerialNumber_new,
                            FixAssetNo_New = fixAssetNo_New,
                            FCAT_ID = _fcatID,
                            FBA_ID = _fbaID,
                            FMD_ID = _fmdID,
                            StartLiter = tmp_StartLiter,
                            EndLiter = tmp_EndLiter,
                            LiterTest = tmp_LiterTest,
                            LOC_ID = _LOC_ID,//20141204
                            Location = null,//20141204 tmp_Location.Trim() == "" ? null : tmp_Location.Trim(),
                            Model = tmp_Model.Trim() == "" ? null : tmp_Model.Trim(),
                            IsCancel = dataGridView_FailPoint["IsCancel", i].Value.ToString()[0],
                            IsDownload = dataGridView_FailPoint["IsDownload", i].Value.ToString()[0],
                            //20141022
                            PRT_ID = _fPRT_ID,
                            //20141127ยกเลิก EQP_ID = _fEQP_ID,
                            ATF_ID = _fATF_ID,
                            FAS_ID = _fFAS_ID,
                            SolvingByWI = SolvingByWI

                            //20170621
                            ,LastEditBy =  UserInfo.UserId
                            ,LastEditDate = tmpLastEditDate

                        };

                        //20170608 db.JOB_Failure_Details.InsertOnSubmit(jf);
                        //20170608 db.SubmitChanges();
                          */
                    }// for (


                    #endregion


                    //======================================
                    // Insert JOB_Detail_Spare
                    //======================================
                    #region Insert JOB_Detail_Spare
                    strPoint = "Insert JOB_Detail_Spare";

                    //string 
                    ////20170621 ] กรณีลบทิ้ง เสี่ยงไป  //Clear Old part
                    //  if ((from t in db.JOB_Detail_Spares where t.JOB_ID == txtJobID.Text.Trim() select t).Count() > 0)
                    //  {
                    //      var delq2 = from t in db.JOB_Detail_Spares where t.JOB_ID == txtJobID.Text.Trim() select t;
                    //      db.JOB_Detail_Spares.DeleteAllOnSubmit(delq2);
                    //      //20170608 db.SubmitChanges();
                    //  }

                    DateTime _StartDate = tmpStartDate;
                    for (int i = 0; i <= dataGridView_Part.Rows.Count - 1; i++)
                    {


                        string _SPA_ID = dataGridView_Part["Part no.", i].Value.ToString();

                        //Write Part cancel
                        #region Write Part cancel

                        //if (dataGridView_Part["IsDownload", i].Value.ToString() == "Y" && dataGridView_Part["IsCancel", i].Value.ToString() == "1")
                        //{
                        //    foreach (var partID in logCancelSPA_ID)
                        //    {
                        //        if (partID == dataGridView_Part["Part no.", i].Value.ToString())
                        //            partLogCancel(
                        //                dataGridView_Part["Part no.", i].Value.ToString(),
                        //                dataGridView_Part["Location", i].Value.ToString(),
                        //                dataGridView_Part["รหัสจุดเสีย", i].Value.ToString(),
                        //                dataGridView_Part["FAI_ID", i].Value.ToString(),
                        //                dataGridView_Part["Type", i].Value.ToString()
                        //                );
                        //    }
                        //}
                        #endregion

                        //20170626 hold _StartDate = checkStartDate(_StartDate, _SPA_ID);

                        bool _StatusSpare = (dataGridView_Part["สถานะ", i].Value.ToString() == "ค้าง") ? false : true;//7dr["สถานะ"]
                        //อะไหล่ลูกค้า
                        string _StatusSpareCus = (dataGridView_Part["อะไหล่ลูกค้า", i].Value.ToString() == "Y") ? "9" : "0";


                        int xNO = int.Parse(dataGridView_Part["NO.", i].Value.ToString());

                        char stCharge = dataGridView_Part["ออกบิลในนาม", i].Value.ToString() == "สถานี" ? '0' : '1';
                        //string vtxtJobID = txtJobID.Text.Trim();

                        if ((from j in db.JOB_Detail_Spares
                             where j.JOB_ID == txtJobID.Text.Trim() &&
                             j.StartDate == _StartDate &&
                             j.SPA_ID == _SPA_ID &&
                              j.NO == xNO
                             select j).Count() > 0)
                        {

                            var sp = (from j in db.JOB_Detail_Spares
                                      where j.JOB_ID == txtJobID.Text.Trim() &&
                                          j.StartDate == _StartDate &&
                                          j.SPA_ID == _SPA_ID &&
                                          j.NO == xNO
                                      select j).FirstOrDefault();

                            sp.POI_ID = dataGridView_Part["รหัสจุดเสีย", i].Value.ToString();

                            sp.Quantity = decimal.Parse(dataGridView_Part["จำนวน", i].Value.ToString());
                            sp.PricesList = decimal.Parse(dataGridView_Part["PricesList", i].Value.ToString());
                            sp.Prices = decimal.Parse(dataGridView_Part["ราคา/หน่วย", i].Value.ToString());
                            sp.SumPrices = decimal.Parse(dataGridView_Part["รวม", i].Value.ToString());
                            sp.SumVAT = decimal.Parse(dataGridView_Part["ภาษี", i].Value.ToString());
                            sp.SumTotalPrices = decimal.Parse(dataGridView_Part["รวมสุทธิ", i].Value.ToString());


                            sp.Discount = decimal.Parse(dataGridView_Part["Discount", i].Value.ToString());
                            sp.VAT = decimal.Parse(dataGridView_Part["VAT", i].Value.ToString());
                            sp.ControlFixAssetNo = dataGridView_Part["FixAssetNo", i].Value.ToString();

                            sp.StatusSpare = _StatusSpare;
                            sp.TRA_ID = dataGridView_Part["Type", i].Value.ToString();
                            sp.Comment = dataGridView_Part["Comment", i].Value == null ? null + (IsQT == false ? "#QT=None" : "") : dataGridView_Part["Comment", i].Value.ToString() + (IsQT == false ? "#QT=None" : "");//dr["Comment"]                         
                            sp.Enddate = tmpEndDate;
                            sp.EMP_ID = tmpVAN_ID;//UserInfo.UserId;
                            sp.FAI_ID = dataGridView_Part["FAI_ID", i].Value == null ? null : dataGridView_Part["FAI_ID", i].Value.ToString();

                            //20150709 ใช้ process แปลง sp.Location = dataGridView_Part["Location", i].Value == null ? null : dataGridView_Part["Location", i].Value.ToString();
                            //===========20150709=============================
                            string _vLocation = dataGridView_Part["Location", i].Value.ToString().Trim();

                            int _cLocation = 0;

                            if (dataGridView_Part["Location", i].Value != null)
                                _cLocation = dataGridView_Part["Location", i].Value.ToString().Trim().Length;

                            if ((_cLocation > 2 || _vLocation == null))
                            {
                                //_vLocation
                                sp.Location = "0";
                            }
                            else
                            {
                                var qLocate = (from t in db.Locations
                                               where Convert.ToString(t.LOC_ID) == _vLocation
                                               select t).FirstOrDefault();
                                if (qLocate == null)
                                {
                                    sp.Location = "0";
                                }
                                else
                                {
                                    sp.Location = Convert.ToString(qLocate.LOC_ID);
                                }
                            }
                            //===========20150709=============================


                            sp.SerialNumber = dataGridView_Part["S/N", i].Value == null ? null : dataGridView_Part["S/N", i].Value.ToString();

                            sp.IsCancel = dataGridView_Part["IsCancel", i].Value.ToString()[0];
                            sp.IsDownload = dataGridView_Part["IsDownload", i].Value.ToString()[0];
                            sp.IsPrices = dataGridView_Part["IsPrices", i].Value.ToString() == "0" ? false : true;
                            sp.ERP_StationCharge = stCharge;

                            if (dataGridView_Part["ERP_SPA_ID", i].Value != DBNull.Value && !string.IsNullOrEmpty(dataGridView_Part["ERP_SPA_ID", i].Value.ToString()))
                                sp.ERP_SPA_ID = dataGridView_Part["ERP_SPA_ID", i].Value.ToString();
                            else
                                sp.ERP_SPA_ID = null;

                            if (dataGridView_Part["ERP_PartName", i].Value != DBNull.Value && !string.IsNullOrEmpty(dataGridView_Part["ERP_PartName", i].Value.ToString()))
                                sp.ERP_PartName = dataGridView_Part["ERP_PartName", i].Value.ToString();
                            else
                                sp.ERP_PartName = null;


                            //อะไหล่ลูกค้า
                            sp.IsCustomer = Convert.ToChar(_StatusSpareCus);

                            if (dataGridView_Part["ERP_orderline_id", i].Value != DBNull.Value && !string.IsNullOrEmpty(dataGridView_Part["ERP_orderline_id", i].Value.ToString()))
                                sp.ERP_orderline_id = int.Parse(dataGridView_Part["ERP_orderline_id", i].Value.ToString());
                            else
                                sp.ERP_orderline_id = null;

                            //20140905
                            sp.SerialNumber_Old = dataGridView_Part["S/N Old", i].Value.ToString();

                            //20141022
                            //20141127ยกเลิก  sp.EQP_ID = int.Parse(dataGridView_Part["ตำแหน่งที่เปลี่ยน", i].Value.ToString());

                            //20161116
                            sp.ReferPage = dataGridView_Part["Page/Item", i].Value.ToString();




                            //20170608 db.SubmitChanges();

                        }
                        else
                        {
                            JOB_Detail_Spare jPart = new JOB_Detail_Spare();


                            jPart.NO = xNO;
                            jPart.JOB_ID = txtJobID.Text.Trim();
                            jPart.POI_ID = dataGridView_Part["รหัสจุดเสีย", i].Value.ToString();//dr["รหัสจุดเสีย"]
                            jPart.SPA_ID = dataGridView_Part["Part no.", i].Value.ToString();//dr["Part no."]
                            jPart.StartDate = _StartDate;
                            jPart.Quantity = decimal.Parse(dataGridView_Part["จำนวน", i].Value.ToString());//dr["จำนวน"]
                            jPart.PricesList = decimal.Parse(dataGridView_Part["PricesList", i].Value.ToString());
                            jPart.Prices = decimal.Parse(dataGridView_Part["ราคา/หน่วย", i].Value.ToString());
                            jPart.SumPrices = decimal.Parse(dataGridView_Part["รวม", i].Value.ToString());
                            jPart.SumVAT = decimal.Parse(dataGridView_Part["ภาษี", i].Value.ToString());
                            jPart.SumTotalPrices = decimal.Parse(dataGridView_Part["รวมสุทธิ", i].Value.ToString());

                            jPart.Discount = decimal.Parse(dataGridView_Part["Discount", i].Value.ToString());
                            jPart.VAT = decimal.Parse(dataGridView_Part["VAT", i].Value.ToString());
                            jPart.ControlFixAssetNo = dataGridView_Part["FixAssetNo", i].Value.ToString();

                            jPart.StatusSpare = _StatusSpare;//dr["สถานะ"]
                            jPart.TRA_ID = dataGridView_Part["Type", i].Value.ToString();//dr["Type"]
                            jPart.Comment = dataGridView_Part["Comment", i].Value == null ? null + (IsQT == false ? "#QT=None" : "") : dataGridView_Part["Comment", i].Value.ToString() + (IsQT == false ? "#QT=None" : "");//dr["Comment"]                         
                            jPart.Enddate = tmpEndDate;
                            jPart.EMP_ID = tmpVAN_ID;// UserInfo.UserId;

                            jPart.FAI_ID = dataGridView_Part["FAI_ID", i].Value == null ? null : dataGridView_Part["FAI_ID", i].Value.ToString();

                            //20150709 ใช้ process แปลง jPart.Location = dataGridView_Part["Location", i].Value == null ? null : dataGridView_Part["Location", i].Value.ToString();
                            //===========20150709=============================
                            string _vLocation = dataGridView_Part["Location", i].Value.ToString().Trim();

                            int _cLocation = 0;

                            if (dataGridView_Part["Location", i].Value != null)
                                _cLocation = dataGridView_Part["Location", i].Value.ToString().Trim().Length;

                            if ((_cLocation > 2 || _vLocation == null))
                            {
                                //_vLocation
                                jPart.Location = "0";
                            }
                            else
                            {
                                var qLocate = (from t in db.Locations
                                               where Convert.ToString(t.LOC_ID) == _vLocation
                                               select t).FirstOrDefault();
                                if (qLocate == null)
                                {
                                    jPart.Location = "0";
                                }
                                else
                                {
                                    jPart.Location = Convert.ToString(qLocate.LOC_ID);
                                }
                            }
                            //===========20150709=============================


                            jPart.SerialNumber = dataGridView_Part["S/N", i].Value == null ? null : dataGridView_Part["S/N", i].Value.ToString();


                            jPart.IsCancel = dataGridView_Part["IsCancel", i].Value.ToString()[0];
                            jPart.IsDownload = dataGridView_Part["IsDownload", i].Value.ToString()[0];
                            jPart.IsPrices = dataGridView_Part["IsPrices", i].Value.ToString() == "0" ? false : true;
                            jPart.ERP_StationCharge = stCharge;

                            if (dataGridView_Part["ERP_SPA_ID", i].Value != DBNull.Value && !string.IsNullOrEmpty(dataGridView_Part["ERP_SPA_ID", i].Value.ToString()))
                                jPart.ERP_SPA_ID = dataGridView_Part["ERP_SPA_ID", i].Value.ToString();
                            else
                                jPart.ERP_SPA_ID = null;

                            if (dataGridView_Part["ERP_PartName", i].Value != DBNull.Value && !string.IsNullOrEmpty(dataGridView_Part["ERP_PartName", i].Value.ToString()))
                                jPart.ERP_PartName = dataGridView_Part["ERP_PartName", i].Value.ToString();
                            else
                                jPart.ERP_PartName = null;

                            //jPart.ERP_SPA_ID = dataGridView_Part["ERP_SPA_ID", i].Value == DBNull.Value ? null : dataGridView_Part["ERP_SPA_ID", i].Value.ToString();
                            //jPart.ERP_PartName = dataGridView_Part["ERP_PartName", i].Value == DBNull.Value? null: dataGridView_Part["ERP_PartName", i].Value.ToString();

                            //อะไหล่ลูกค้า

                            jPart.IsCustomer = Convert.ToChar(_StatusSpareCus);

                            if (dataGridView_Part["ERP_orderline_id", i].Value != DBNull.Value && !string.IsNullOrEmpty(dataGridView_Part["ERP_orderline_id", i].Value.ToString()))
                                jPart.ERP_orderline_id = int.Parse(dataGridView_Part["ERP_orderline_id", i].Value.ToString());
                            else
                                jPart.ERP_orderline_id = null;


                            //string a = dataGridView_Part["S/N Old", i].Value.ToString();
                            ////20140905
                            //jPart.SerialNumber_Old = dataGridView_Part["S/N Old", i].Value.ToString();


                            if (dataGridView_Part["S/N Old", i].Value != DBNull.Value && !string.IsNullOrEmpty(dataGridView_Part["S/N Old", i].Value.ToString()))
                                jPart.SerialNumber_Old = dataGridView_Part["S/N Old", i].Value.ToString();
                            else
                                jPart.SerialNumber_Old = null;


                            //20141022
                            //20141127ยกเลิก jPart.EQP_ID = int.Parse(dataGridView_Part["ตำแหน่งที่เปลี่ยน", i].Value.ToString());

                            //20161116
                            jPart.ReferPage = dataGridView_Part["Page/Item", i].Value.ToString();


                            //20170608db.JOB_Detail_Spares.InsertOnSubmit(jPart);
                            //20170608 db.SubmitChanges();


                            PartCount++;//ตรวจสอบการเพิ่ม Part เพื่อนำไปตรวจสอบ การออกรายงาน Services Report
                            if (_StatusSpare == true)
                                TypeJobRemain = true;

                        }//  if ((from j in db.JOB_Detail_Spares

                    }//for (int i = 0; i <= dataGridView_Part.Rows.Count - 1; i++)

                    #endregion


                    //======================================
                    // Insert JOB_Detail
                    //======================================
                    #region Insert JOB_Detail
                    strPoint = "Insert JOB_Detail";
                    if (qjd != null)
                    {
                        //tmpEndDate
                        //20170616 qjd.EndDate = tmpEndDate;
                        qjd.Problem_Detail = vProblem_Detail;// txtProblem_Detail.Text.Trim() == "" ? null : txtProblem_Detail.Text.Trim(); //สาเหตุ
                        qjd.Resole_Detail = txtResolve_Detail.Text.Trim() == "" ? null : txtResolve_Detail.Text.Trim();//วิธีการแก้ไข

                        if (TypeJobRemain == true)
                            qjd.TRE_ID = "02";//02	ค้างอะไหล่
                        else
                            qjd.TRE_ID = "03";// Job ค้าง


                        if (RadioButtonList_JobStatus2.Checked)//CloseJOB
                            qjd.JOBS_ID = "99";//ปิด JOB 
                        else
                            qjd.JOBS_ID = "07";//ค้าง JOB

                        //20170620 //20160713
                        //20170620 qjd.TCO_ID = "04";//ประเภทการปิด JOB {04 = เข้า Site (ผ่าน web)}

                        //20170215
                        //20170616 qjd.RecDate_Endate = Server_date;//เวลา server


                        //20170608 db.SubmitChanges();
                    }

                    #endregion


                    tmpjobID = txtJobID.Text.Trim();


                    updateSN_byStation(db);

                    db.Transaction.Commit();



                    setDtFailure();
                    setDtPart();

                    FailureBinding();
                    PartBindind();

                    ClearControl();

                    ClearTextFail();
                    ClearPartText();

                    butttonEnable(false, false, true, true, true);

                    //20150629
                    chkEnableSVReport(txtJobID.Text.Trim());

                }

                catch (Exception ex)
                {
                    db.Transaction.Rollback();
                    PartCount = 0;
                    MessageBox.Show("เกิดข้อผิดพลาด Error: ในการบันทึกข้อมูล  หมายเลข JOB: " + txtJobID.Text.Trim() +
                        Environment.NewLine + " จุด " + strPoint +
                        Environment.NewLine + " Error: " + ex.Message, " กรุณาติดต่อผู้ดูแลระบบ...",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }

        }

        private bool isPartRement()
        {
            for (int i = 0; i <= dtPart.Rows.Count - 1; i++)
            {
                if (dtPart.Rows[i]["สถานะ"].ToString() == "ค้าง")
                    return true;
            }

            return false;
        }

        //private DateTime checkStartDate(DateTime _StartDate, string _SPA_ID)
        //{

        //    for (int i = 0; i <= dataGridView_tmpPart.Rows.Count - 1; i++)
        //    {
        //        if (_SPA_ID == dataGridView_tmpPart["SPA_ID", i].Value.ToString())
        //        {
        //            _StartDate = DateTime.Parse(dataGridView_tmpPart["StartDate", i].Value.ToString());
        //            return _StartDate;
        //        }
        //    }
        //    return _StartDate;
        //}

        // 20170622 private void updateSN_byStation(DAL.SMSManage.SMSManageDataContext db)
        private void updateSN_byStation(DAL.SMSManage.SMSManageDataContext db)
        {
            for (int i = 0; i <= dataGridView_FailPoint.Rows.Count - 1; i++)
            {

                string SerialNumber = dataGridView_FailPoint["S/N(Old)", i].Value.ToString();
                string SerialNumber_new = dataGridView_FailPoint["S/N(New)", i].Value.ToString();



                //20141223 if ((SerialNumber != "None" && SerialNumber != "None") && (SerialNumber != "ไม่ระบุ..." && SerialNumber != "ไม่ระบุ..."))
                if ((SerialNumber != "None" && SerialNumber_new != "None") && (SerialNumber != "ไม่ระบุ..." && SerialNumber_new != "ไม่ระบุ..."))
                {

                    //20141224 เช็คข้อมุล SN ที่ user key เองมีใน pool หรือไม่
                    var qchkPool = from a in db.FFixedAsset_SerialNumber_Pools
                                   where a.SerialNumber == SerialNumber_new
                                   select a;

                    if (qchkPool.Count() > 0)
                    {

                        //ถ้ามีการเปลี่ยน Serial
                        if (SerialNumber != SerialNumber_new)
                        {

                            #region FFixedAsset_Station_Details


                            var q = from t in db.FFixedAsset_Station_Details
                                    where t.STA_ID == tmpSTAID &&
                                            t.SerialNumber == SerialNumber
                                    select t;

                            if (q.Count() > 0)
                            {
                                var delSN = q.Where(t => t.SerialNumber == SerialNumber

                                              &&
                                           /*20210308  t.FCAT_ID == t.FCAT_ID &&
                                           t.FMD_ID == t.FMD_ID &&*/
                                           t.STA_ID == t.STA_ID

                                          //  &&
                                          //  t.FCAT_ID == tmpSN.FCAT_ID) &&
                                          // t.FMD_ID == tmpSN.FMD_ID &&
                                          // t.STA_ID == tmpSN.STA_ID

                                          ).FirstOrDefault();
                                if (delSN != null)
                                {
                                    var tmpSN = delSN;
                                    db.FFixedAsset_Station_Details.DeleteOnSubmit(delSN);

                                    /* 20170626 hold  var newSN = new FFixedAsset_Station_Detail
                                      {
                                          SerialNumber = SerialNumber_new,
                                          FCAT_ID = tmpSN.FCAT_ID,
                                          FBA_ID = tmpSN.FBA_ID,
                                          FMD_ID = tmpSN.FMD_ID,
                                          STA_ID = tmpSN.STA_ID,
                                          OwnerShip = tmpSN.OwnerShip,
                                          InstallDate = tmpSN.InstallDate,
                                          Detail = tmpSN.Detail,
                                          //20141208
                                          LOC_ID = tmpSN.LOC_ID
                                      };

                                      db.FFixedAsset_Station_Details.InsertOnSubmit(newSN);
                                      db.SubmitChanges();
                                     */
                                }

                                //เปลี่ยน 20150105   }

                                #endregion

                                #region Update SN Pool


                                var qpool = db.FFixedAsset_SerialNumber_Pools
                                    .Where(t => t.SerialNumber == SerialNumber_new).FirstOrDefault();

                                if (qpool != null)
                                {
                                    qpool.StatusUse = "11";//ใช้งานแล้ว
                                    db.SubmitChanges();
                                }
                                //20140814 ก่อนตรวจสอบเรื่อง ตู้จ่าย
                                var qpool2 = db.FFixedAsset_SerialNumber_Pools
                                   .Where(t => t.SerialNumber == SerialNumber).FirstOrDefault();

                                if (qpool2 != null)
                                {
                                    //20140814 ก่อนตรวจสอบเรื่อง ตู้จ่าย
                                    //qpool2.StatusUse = "12";//รอซ่อม
                                    //db.SubmitChanges();                          

                                    var chkpool = ((from p in db.FFixedAsset_SerialNumber_Pools
                                                    join m in db.FFixedAsset_Models on p.FMD_ID equals m.FMD_ID
                                                    join b in db.FFixedAsset_Brands on m.FBA_ID equals b.FBA_ID
                                                    where m.FMD_ID.ToString().Contains(qpool2.FMD_ID.ToString())
                                                    && p.SerialNumber == qpool2.SerialNumber
                                                    && (b.FBA_ID.ToString() == "1"
                                                    || b.FBA_ID.ToString() == "2"
                                                    || b.FBA_ID.ToString() == "3"
                                                    || b.FBA_ID.ToString() == "4")
                                                    select p).FirstOrDefault());

                                    if (chkpool != null)
                                    {
                                        chkpool.StatusUse = "00";//พร้อมใช้
                                        db.SubmitChanges();
                                    }
                                    else
                                    {
                                        qpool2.StatusUse = "12";//รอซ่อม
                                        db.SubmitChanges();
                                    }
                                }
                            }//เปลี่ยน 20150105


                            //คืนค่า ถ้ามีการยกเลิก
                            foreach (var item in lsSNCancel)
                            {
                                var resetSNCanOld = db.FFixedAsset_SerialNumber_Pools
                               .Where(t => t.SerialNumber == item.SNold).FirstOrDefault();
                                if (resetSNCanOld != null)
                                {
                                    resetSNCanOld.StatusUse = "11";
                                    db.SubmitChanges();
                                }

                                var resetSNCanNew = db.FFixedAsset_SerialNumber_Pools
                              .Where(t => t.SerialNumber == item.SNNew).FirstOrDefault();
                                if (resetSNCanNew != null)
                                {
                                    resetSNCanNew.StatusUse = "00";
                                    db.SubmitChanges();
                                }
                            }
                            #endregion



                        }
                        else
                        {
                            var qpool = db.FFixedAsset_SerialNumber_Pools
                                   .Where(t => t.SerialNumber == SerialNumber_new).FirstOrDefault();

                            if (qpool != null)
                            {
                                qpool.StatusUse = "11";//ใช้งานแล้ว
                                db.SubmitChanges();
                            }
                        }
                    }//end chkpool

                }//  if (SerialNumber != "None" && SerialNumber != "None")

            }// for (int i ...
        }
















    }
}
