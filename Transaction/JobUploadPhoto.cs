﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using SMSMINI.DAL;
using System.Windows.Media.Imaging;



namespace SMSMINI.Transaction
{
    public partial class JobUploadPhoto : Form
    {
        public string _MODE { get; set; }//_MODE = 0 Uploading, 1 = RE-Uploading
        public string _jOBNo { get; set; }
        public string _failureDetail { get; set; }
        public DateTime _startDate { get; set; }
        public DateTime? _endDate { get; set; }
        public string _workID { get; set; }
        public string _stationNo { get; set; }
        public string _stationName { get; set; }

        public bool _IsMaxPrice { get; set; }

        List<DAL.SMSManage.JOB_PhotoReport> lsJOB = null;
        List<DAL.SMSManage.JOB_PhotoReport_Detail> lsJPart_cur = null;

        public List<DAL.SMSManage.JOB_PhotoReport_Detail> lsJPart { get; set; }
        public List<DAL.SMSManage.JOB_PhotoReport_Detail> lsNewCPart { get; set; }

        //==20201022
        public List<string> _LConfTRA { get; set; }
        public List<DAL.SMSManage.JOB_PhotoReport_Detail> lsJPartServ { get; set; }
        public List<DAL.SMSManage.JOB_PhotoReport_Detail> lsJPartDet { get; set; }
        //===

        DateTime svDT;//Server SMS DateTime

        DateTime servDate = DateTime.Now;
        string strConn = ""; //20200623



        private ProgressBar.FormProgress m_fmProgress = null;


        public JobUploadPhoto()
        {
            InitializeComponent();
        }

        private void JobUploadPhoto_Load(object sender, EventArgs e)
        {
            //MessageBox.Show("_jOBNo " + _jOBNo);
            _IsMaxPrice = true;
            lsJPart_cur = new List<SMSMINI.DAL.SMSManage.JOB_PhotoReport_Detail>();

            //20200623
            strConn = GetConnection.CheckConnectionString(_jOBNo);
            //======


            if (!string.IsNullOrEmpty(_jOBNo))
            {
                lsJOB = new List<DAL.SMSManage.JOB_PhotoReport>();

                if (lsJPart == null || lsJPart.Count() == 0)
                {
                    lsJPart = new List<SMSMINI.DAL.SMSManage.JOB_PhotoReport_Detail>();
                }
                // else
                //  lsJPart_cur = lsJPart;

                svDT = SMSMINI.ConnectionManager.getServerTime();

                if (_MODE == "0")
                    //Current Upload
                    getJOBPhoto(_jOBNo);
                else
                    //Upload ย้อนหลัง...
                    getRE_JOBPhoto(_jOBNo);
                //...
            }
            else
            {
                MessageBox.Show("ตรวจสอบ ไม่มี JOB ไม่สามารถทำการ Upload รูปภาพอะไหล่", "ผลการตรวจสอบ...",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }

            if (pictureBox1.Image == null)
                groupBox3.Enabled = false;
            else
                groupBox3.Enabled = true;
        }

        //Online Only
        private void getRE_JOBPhoto(string jobNO)
        {
            //DAL.SMSManage.JOB_Detail _jd = null;
            //20200623using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn))
            {

                var jphoto = dc.JOB_PhotoReports
                    .Where(t => t.JOB_ID == jobNO)
                    .FirstOrDefault();

                if (jphoto != null)
                {
                    byte[] img = (byte[])(jphoto.ImgStation).ToArray();
                    pictureBox1.Image = ByteArrayToImage(img);
                    /* //20151007
                     // เปลี่ยน flow ไป ให้แสดง part ทั้งหมด
                        var jd = dc.JOB_PhotoReport_Details
                            .Where(t => t.JOB_ID == jobNO).ToList();

                        lsJPart_cur = jd;
                        lsJPart = new List<SMSMINI.DAL.SMSManage.JOB_PhotoReport_Detail>();

                        if (jd.Count() > 0)
                        {
                            foreach (var item in jd)
                            {
                                lsJPart.Add(new DAL.SMSManage.JOB_PhotoReport_Detail()
                                {
                                    Item = item.Item,
                                    JOB_ID = item.JOB_ID,
                                    StartDate = item.StartDate,
                                    Enddate = item.Enddate,
                                    ImgBefore = null,
                                    ImgAfter = null,
                                    Equipment = item.Equipment,
                                    Decription = item.Decription,
                                    TranDate = item.TranDate,
                                    Remark = item.Remark,
                                    ImgBeforeRepair = null,
                                    ImgAfterRepair = null
                                });
                            }
                        }
                        else
                        {
                            var jsp = dc.JOB_Detail_Spares
                                    .Where(t =>
                                           //20150915  (t.JOB_ID == jobNO && t.TRA_ID == "IN") && 
                                            (t.JOB_ID == jobNO &&   new string[] { "IN", "WA" }.Contains(t.TRA_ID)) && 
                                            (!new string[] { "TRAVEL", "SERVICE" }.Contains(t.SPA_ID))
                                            && !t.SPA_ID.StartsWith("20")//20151001
                       
                                           ); 




                            foreach (var itemx in jsp)
                            {
                                lsJPart.Add(new DAL.SMSManage.JOB_PhotoReport_Detail
                                {
                                    Item = itemx.NO,
                                    JOB_ID = itemx.JOB_ID,
                                    StartDate = itemx.StartDate,
                                    Enddate = (itemx.Enddate == null ? DateTime.Now : itemx.Enddate.Value),
                                    ImgBefore = null,
                                    ImgAfter = null,
                                    Equipment = itemx.SPA_ID,
                                    Decription = jphoto.Failure_Detail,
                                    Remark = "",
                                    ImgBeforeRepair = null,
                                    ImgAfterRepair = null,
                                    TranDate = itemx.StartDate
                                });
                            }
                        }
                    
                    }
                    else
                        pictureBox1.Image = null;

                    //20151007===เปลี่ยน flow โชว์ทุก part ที่อยู่ในเงื่อนไข
                    //var jsp = dc.JOB_Detail_Spares
                    //                .Where(t =>
                    //                    //20150915  (t.JOB_ID == jobNO && t.TRA_ID == "IN") && 
                    //                        (t.JOB_ID == jobNO && new string[] { "IN", "WA" }.Contains(t.TRA_ID)) &&
                    //                        (!new string[] { "TRAVEL", "SERVICE" }.Contains(t.SPA_ID))
                    //                        && !t.SPA_ID.StartsWith("20")//20151001

                    //                       );

                //    var jsp = from jds in dc.JOB_Detail_Spares
                //join pet in jpd on dc.JOB_PhotoReport_Details equals pet.Owner into gj
                //from subpet in gj.DefaultIfEmpty()
                //select new { person.FirstName, PetName = (subpet == null ? String.Empty : subpet.Name) };

                    //var lines =
                    //                  from jds in dc.JOB_Detail_Spares
                    //                  join jpd in dc.JOB_PhotoReport_Details on jds.JOB_ID equals jpd.JOB_ID 
                    //                  //where tl.ResourceNo == resourceNo

                    //                  select new
                    //                  {
                    //                      EntryDate = tl.EntryDate,
                    //                      Hours = tl.Hours,
                    //                      Job = j.JobName
                    //                  };
                     * //20161007
                    */
                }
                //else
                //{
                //    string _vDecription = null;


                //}
                //20151012
                List<string> LConfNoPart = new List<string>();
                LConfNoPart = dc.Conf_Upload_JOB_Photo_Report_NoParts.Select(t => t.SPA_ID).ToList();
                var jd = dc.JOB_PhotoReport_Details
                         .Where(t => t.JOB_ID == jobNO
                         //20190712 โชวืตามวันที่เลือก
                         //20190715 && t.StartDate.Date == Convert.ToDateTime(_startDate.ToString()).Date
                         //20200310 test comment && t.StartDate == Convert.ToDateTime(_startDate.ToString())
                         //20190717
                         && t.Enddate == Convert.ToDateTime(_endDate.ToString())
                         //===
                         ).ToList();

                lsJPart_cur = jd;
                lsJPart = new List<SMSMINI.DAL.SMSManage.JOB_PhotoReport_Detail>();

                //==20201026====
                lsJPartServ = jd;
                DateTime xc = Convert.ToDateTime(_endDate.ToString());
                string stmpEndDateReU = xc.ToString("yyyyMMdd");
                string stmpEndTimeReU = xc.ToString("HH:mm:ss");


                string yyyy = stmpEndDateReU.ToString().Substring(0, 4);
                string mm = stmpEndDateReU.ToString().Substring(4, 2);
                string dd = stmpEndDateReU.ToString().Substring(6, 2);
                string dtp = yyyy + "/" + mm + "/" + dd;
                string b = DateTime.Parse(stmpEndTimeReU.ToString()).ToString("HH:mm:ss");
                string a = DateTime.Parse(dtp).ToString("yyyy/MM/dd");

                string c = a + " " + b;

                string d = DateTime.Parse(c).ToString("yyyy/MM/dd HH:mm:ss");

                DateTime e = DateTime.Parse(d);


                List<string> LConfTRA = new List<string>();
                var qitem = dc.JOBs
                       .Where(t => t.JOB_ID == jobNO.Trim())
                       .FirstOrDefault();
                if (qitem != null)//มี JOB พร้อม Upload
                {
                    var st = dc.Stations.Where(t => t.STA_ID == qitem.STA_ID).FirstOrDefault();
                    LConfTRA = dc.Conf_Upload_JOB_Photo_Reports.Where(
                                        t => t.GOP_ID == st.GOP_ID &&
                                        t.TYP_ID == qitem.TYP_ID &&
                                        t.TYP_ID1 == qitem.TYP_ID1).Select(t => t.TRA_ID).ToList();
                }

                //==============

                if (jd.Count() > 0)
                {
                    foreach (var item in jd)
                    {
                        lsJPart.Add(new DAL.SMSManage.JOB_PhotoReport_Detail()
                        {
                            Item = item.Item,
                            JOB_ID = item.JOB_ID,
                            StartDate = item.StartDate,
                            Enddate = item.Enddate,
                            ImgBefore = null,
                            ImgAfter = null,
                            Equipment = item.Equipment,
                            Decription = item.Decription,
                            TranDate = item.TranDate,
                            Remark = item.Remark,
                            ImgBeforeRepair = null,
                            ImgAfterRepair = null
                        });
                    }
                }
                else
                {
                    var jsp = dc.JOB_Detail_Spares
                            .Where(t =>
                            //20150915  (t.JOB_ID == jobNO && t.TRA_ID == "IN") && 
                            //20201022 (t.JOB_ID == jobNO && new string[] { "IN", "WA" }.Contains(t.TRA_ID)) &&
                            (t.JOB_ID == jobNO && LConfTRA.Contains(t.TRA_ID)) && //20201026
                            (!new string[] { "TRAVEL", "SERVICE" }.Contains(t.SPA_ID))
                            && !LConfNoPart.Contains(t.SPA_ID)//20151012
                            && t.IsCancel != '1' //20190710
                            //==20201026 
                            && t.Enddate.Value.Date == e.Date
                            && t.Enddate.Value.Hour == e.Hour
                            && t.Enddate.Value.Minute == e.Minute
                            && t.Enddate.Value.Second == e.Second
                            //==========
                          ).OrderBy(m => m.StartDate); //20210408;


                    foreach (var itemx in jsp)
                    {
                        lsJPart.Add(new DAL.SMSManage.JOB_PhotoReport_Detail
                        {
                            Item = itemx.NO,
                            JOB_ID = itemx.JOB_ID,
                            StartDate = itemx.StartDate,
                            Enddate = (itemx.Enddate == null ? DateTime.Now : itemx.Enddate.Value),
                            ImgBefore = null,
                            ImgAfter = null,
                            Equipment = itemx.SPA_ID,
                            //Decription = jphoto.Failure_Detail, //20160420
                            Decription = jphoto == null ? null : jphoto.Failure_Detail,

                            Remark = "",
                            ImgBeforeRepair = null,
                            ImgAfterRepair = null,
                            TranDate = itemx.StartDate
                        });
                    }
                }

                //===============================================================

                /* //20201029 จะลองใช้ flow ใหม่ ยังทำไม่เสร็จเก็บไว้ก่อน
                 var jsp = dc.JOB_Detail_Spares
                         .Where(t =>
                             //20150915  (t.JOB_ID == jobNO && t.TRA_ID == "IN") && 
                             //20201022 (t.JOB_ID == jobNO && new string[] { "IN", "WA" }.Contains(t.TRA_ID)) &&
                         (t.JOB_ID == jobNO && LConfTRA.Contains(t.TRA_ID)) && //20201026
                         (!new string[] { "TRAVEL", "SERVICE" }.Contains(t.SPA_ID))
                         && !LConfNoPart.Contains(t.SPA_ID)//20151012
                         && t.IsCancel != '1' //20190710
                             //==20201026 
                         && t.IsCancel != '1'
                         && t.Enddate.Value.Date == e.Date
                         && t.Enddate.Value.Hour == e.Hour
                         && t.Enddate.Value.Minute == e.Minute
                         && t.Enddate.Value.Second == e.Second
                     //==========
                       );

                 
                 if (jd.Count() > 0)
                 {
                     foreach (var item in jd)
                     {
                         lsJPart.Add(new DAL.SMSManage.JOB_PhotoReport_Detail()
                         {
                             Item = item.Item,
                             JOB_ID = item.JOB_ID,
                             StartDate = item.StartDate,
                             Enddate = item.Enddate,
                             ImgBefore = null,
                             ImgAfter = null,
                             Equipment = item.Equipment,
                             Decription = item.Decription,
                             TranDate = item.TranDate,
                             Remark = item.Remark,
                             ImgBeforeRepair = null,
                             ImgAfterRepair = null
                         });
                     }






                     if (jsp.Count() > 0)
                     {
                         foreach (var itemx in jsp)
                         {
                             lsJPart.Add(new DAL.SMSManage.JOB_PhotoReport_Detail
                             {
                                 Item = itemx.NO,
                                 JOB_ID = itemx.JOB_ID,
                                 StartDate = itemx.StartDate,
                                 Enddate = (itemx.Enddate == null ? DateTime.Now : itemx.Enddate.Value),
                                 ImgBefore = null,
                                 ImgAfter = null,
                                 Equipment = itemx.SPA_ID,
                                 //Decription = jphoto.Failure_Detail, //20160420
                                 Decription = jphoto == null ? null : jphoto.Failure_Detail,

                                 Remark = "",
                                 ImgBeforeRepair = null,
                                 ImgAfterRepair = null,
                                 TranDate = itemx.StartDate
                             });
                         }

                         lsJPartDet = lsJPart;

                         foreach (var itemDetail in lsJPartDet)
                         {
                             foreach (var itemServ in lsJPartServ)
                             {
                                 if (itemDetail.Item == itemServ.Item && itemDetail.Equipment == itemServ.Equipment)
                                 {
                                     lsJPart.Add(new DAL.SMSManage.JOB_PhotoReport_Detail()
                                     {
                                         Item = item.Item,
                                         JOB_ID = item.JOB_ID,
                                         StartDate = item.StartDate,
                                         Enddate = item.Enddate,
                                         ImgBefore = null,
                                         ImgAfter = null,
                                         Equipment = item.Equipment,
                                         Decription = item.Decription,
                                         TranDate = item.TranDate,
                                         Remark = item.Remark,
                                         ImgBeforeRepair = null,
                                         ImgAfterRepair = null
                                     });
                                 }
                                 else {
                                     lsJPart.Add(new DAL.SMSManage.JOB_PhotoReport_Detail
                                     {
                                         Item = itemx.NO,
                                         JOB_ID = itemx.JOB_ID,
                                         StartDate = itemx.StartDate,
                                         Enddate = (itemx.Enddate == null ? DateTime.Now : itemx.Enddate.Value),
                                         ImgBefore = null,
                                         ImgAfter = null,
                                         Equipment = itemx.SPA_ID,
                                         //Decription = jphoto.Failure_Detail, //20160420
                                         Decription = jphoto == null ? null : jphoto.Failure_Detail,

                                         Remark = "",
                                         ImgBeforeRepair = null,
                                         ImgAfterRepair = null,
                                         TranDate = itemx.StartDate
                                     });
                                 }
                             }
                         }

                     }


                 }
                 else {
                     foreach (var itemx in jsp)
                     {
                         lsJPart.Add(new DAL.SMSManage.JOB_PhotoReport_Detail
                         {
                             Item = itemx.NO,
                             JOB_ID = itemx.JOB_ID,
                             StartDate = itemx.StartDate,
                             Enddate = (itemx.Enddate == null ? DateTime.Now : itemx.Enddate.Value),
                             ImgBefore = null,
                             ImgAfter = null,
                             Equipment = itemx.SPA_ID,
                             //Decription = jphoto.Failure_Detail, //20160420
                             Decription = jphoto == null ? null : jphoto.Failure_Detail,

                             Remark = "",
                             ImgBeforeRepair = null,
                             ImgAfterRepair = null,
                             TranDate = itemx.StartDate
                         });
                     }

                 
                 }
                */


                //================================================

                /*
             //hold ไว้ก่อน
               //===20151007 เปลี่ยนโฟลว โชว์ทั้งหมด

               List<string> LConfNoPart = new List<string>();
               LConfNoPart = dc.Conf_Upload_JOB_Photo_Report_NoParts.Select(t => t.SPA_ID).ToList();

               var lsJPart_cur = (from jds in dc.JOB_Detail_Spares
                                  join jpd in dc.JOB_PhotoReport_Details
                                      //on jds.JOB_ID equals jpd.JOB_ID //on  jds.SPA_ID equals jpd.Equipment
                                  on new { a = jds.JOB_ID, b = jds.SPA_ID } equals new { a=jpd.JOB_ID,b=jpd.Equipment}
                                  into JoinedJDS
                                  from jpd1 in JoinedJDS.DefaultIfEmpty() //JoinedJDS.Where(f => f.Equipment == jds.SPA_ID).DefaultIfEmpty()
                                  where //(jds.SPA_ID == jpd.Equipment) &&
                             (jds.JOB_ID == jobNO) &&
                             (new string[] { "IN", "WA" }.Contains(jds.TRA_ID)) &&
                             (!new string[] { "TRAVEL", "SERVICE" }.Contains(jds.SPA_ID)) &&
                             (!LConfNoPart.Contains(jds.SPA_ID))

                                  select new
                                 {

                                     jds.JOB_ID, //= item.JOB_ID,
                                     jds.StartDate, //= item.StartDate,
                                     jds.Enddate, //= item.Enddate,
                                     Item = jpd1.Item == null ? 0 : jpd1.Item,
                                     //ImgBefore= null,
                                     //ImgAfter= null,
                                     //jpd.Equipment, //= item.Equipment,
                                     Equipment = jds.SPA_ID,
                                     jpd1.Decription, //= item.Decription,
                                     jpd1.TranDate, //= item.TranDate,
                                     jpd1.Remark, //= item.Remark,
                                     jpd1.ImgBeforeRepair, //= null,
                                     jpd1.ImgAfterRepair //= null

                                 }).Distinct().ToList();//.ToList();
               //lsJPart_cur = qJDS;
           //}
              */


                if (lsJPart.Count() > 0)
                {
                    if (lsJPart_cur.Count() <= 0)
                        lsJPart_cur = lsJPart;

                    dataGridView_Photo.DataSource = lsJPart.ToList();
                }
                else
                    dataGridView_Photo.DataSource = null;

                /*
                  //hold ไว้ก่อน
                if (lsJPart_cur.Count() > 0)
                {
                   // lsJPart = lsJPart_cur.ToList();
                    dataGridView_Photo.DataSource = lsJPart_cur.ToList();
                }
                else {
                    dataGridView_Photo.DataSource = null;

                }
                 */

                textBinding();
            }
        }

        private void textBinding()
        {
            // MessageBox.Show("5 jobNO " + _jOBNo);

            txtJOB_ID.Text = _jOBNo;
            dateTimePicker1.Value = _startDate;
            dateTimePicker2.Value = (_endDate == null ? DateTime.Now : _endDate.Value);
            txtWorkID.Text = _workID;

            txtFailure_Detail.Text = _failureDetail;
            txtStation.Text = _stationNo + ": " + _stationName;


        }

        private void toolStripButton_Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void toolStripButton_Save_Click(object sender, EventArgs e)
        {
            if (pictureBox1.Image == null)
            {
                MessageBox.Show("คุณยังไม่เลือก ภาพสถานี" + Environment.NewLine +
                                 "กรุณาเลือกภาพสถานี และรูปภาพอะไหล่ จากนั้นป้อนรายละเอียดรูปภาพ แล้วทำการ กดปุ่ม เลือกเข้ารายการ", "ผลการตรวจสอบ",
                                 MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;

            }
            /*//20151002
            if (dataGridView_Photo.RowCount <= 0)
            {
                MessageBox.Show("คุณยังไม่เลือก ภาพอะไหล่"+Environment.NewLine +
                                "กรุณาเลือกรูปภาพ จากนั้นป้อนรายละเอียดรูปภาพ แล้วทำการ กดปุ่ม เลือกเข้ารายการ", "ผลการตรวจสอบ", 
                                MessageBoxButtons.OK,MessageBoxIcon.Error );
                return;
            }*/

            //20151002

            foreach (var itemPhoto in lsJPart)
            {
                /* 20151013
                using (DAL.SMSManage.SMSManageDataContext dc = new DAL.SMSManage.SMSManageDataContext())
                {
                    var jpdDetail = dc.JOB_PhotoReport_Details
                   .Where(t => t.JOB_ID == _jOBNo &&
                       t.Equipment ==  itemPhoto.Equipment 
                        


                            )
                              .FirstOrDefault();
                    if (jpdDetail == null)
                    {
                        MessageBox.Show("คุณยังไม่เลือก ภาพอะไหล่" + Environment.NewLine +
                                    "กรุณาเลือกรูปภาพ จากนั้นป้อนรายละเอียดรูปภาพ แล้วทำการ กดปุ่ม เลือกเข้ารายการ", "ผลการตรวจสอบ",
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
               */
                //20151012เช็ครูปทุกรายการก่อน save

                if ((itemPhoto.ImgBefore == null || itemPhoto.ImgAfter == null))
                {
                    MessageBox.Show("คุณยังไม่เลือก ภาพอะไหล่" + Environment.NewLine +
                                //20210427 "กรุณาเลือกรูปภาพ จากนั้นป้อนรายละเอียดรูปภาพ แล้วทำการ กดปุ่ม เลือกเข้ารายการ", "ผลการตรวจสอบ",
                                "กรุณาเลือกรูปภาพ จากนั้นป้อน รหัสอะไหล่ ( อุปกรณ์ ) และ รายละเอียด รูปภาพ ", "ผลการตรวจสอบ",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                //==20210426==
                if ((string.IsNullOrEmpty(itemPhoto.Equipment)))
                {
                    MessageBox.Show("คุณยังไม่ระบุ รหัสอะไหล่ ( อุปกรณ์ )" + Environment.NewLine +
                                "", "ผลการตรวจสอบ",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if ((!string.IsNullOrEmpty(itemPhoto.Equipment)))
                {
                    using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn))
                    {
                        if ((from t in dc.SpareParts where t.SPA_ID == itemPhoto.Equipment select t).Count() <= 0)
                        {
                            MessageBox.Show("รหัสอะไหล่ ( Equipment ) ไม่ถูกต้อง... " + Environment.NewLine +
                               "รบกวนตรวจสอบอีกครั้ง", "ผลการตรวจสอบ",
                               MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                    }
                }
                //============

            }

            if (MessageBox.Show("คุณต้องการ Upload ข้อมูลรูปภาพ อะไหล่ ใช่หรือไม่???...", "คำยืนยัน",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                #region tScope MINI
                //using (System.Transactions.TransactionScope tScope = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required))
                //{
                //    #region SAVE MINI
                //    try
                //    {
                //        newProgressbar();
                //        invoke_Progress("บันทึก รูปภาพ อะไหล่...");

                //        #region SMSManageDataContext
                //        using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
                //        {
                //            var bImg1 = ImageToByteArray(pictureBox1.Image);
                //            var _img1 = new System.Data.Linq.Binary(bImg1);

                //            #region Delete Old JOB
                //            var oldj = dc.JOB_PhotoReports
                //                .Where(t => t.JOB_ID == txtJOB_ID.Text.Trim() //&& t.StartDate == _startDate && t.Enddate == _endDate
                //                        ).FirstOrDefault();
                //            if (oldj != null)
                //            {
                //                dc.JOB_PhotoReports.DeleteOnSubmit(oldj);
                //                dc.SubmitChanges();

                //                var oldjd = dc.JOB_PhotoReport_Details.Where(t => t.JOB_ID == oldj.JOB_ID).ToList();
                //                if (oldjd.Count() > 0)
                //                {
                //                    dc.JOB_PhotoReport_Details.DeleteAllOnSubmit(oldjd);
                //                    dc.SubmitChanges();
                //                }
                //            }

                //            #endregion

                //            var newj = new DAL.SMSManage.JOB_PhotoReport
                //            {
                //                JOB_ID = txtJOB_ID.Text.Trim(),
                //                StartDate = _startDate,
                //                Enddate = (_endDate == null ? DateTime.Now : _endDate.Value),
                //                ReportDate = svDT,
                //                VAN_ID = UserInfo.Van_ID,
                //                Failure_Detail = txtFailure_Detail.Text.Trim(),
                //                TranDate = svDT,
                //                ImgStation = _img1
                //            };

                //            dc.JOB_PhotoReports.InsertOnSubmit(newj);
                //            dc.SubmitChanges();

                //            lsJOB.Add(new DAL.SMSManage.JOB_PhotoReport()
                //            {
                //                JOB_ID = newj.JOB_ID,
                //                StartDate = newj.StartDate,
                //                Enddate = newj.Enddate,
                //                ReportDate = newj.ReportDate,
                //                VAN_ID = newj.VAN_ID,
                //                Equipment = newj.Equipment,
                //                Failure_Detail = newj.Failure_Detail,
                //                TranDate = newj.TranDate,
                //                ImgStation = newj.ImgStation
                //            });


                //            //int _no = 1;
                //            //foreach (var itemx in lsJPart)
                //            //{
                //            //    itemx.Item = _no;
                //            //    _no++;
                //            //}


                //            foreach (DataGridViewRow row in this.dataGridView_Photo.Rows)
                //            {
                //                var newjd = new DAL.SMSManage.JOB_PhotoReport_Detail();

                //                newjd.Item = int.Parse(row.Cells["Item"].Value.ToString());
                //                newjd.TranDate = DateTime.Parse(row.Cells["TranDate"].Value.ToString());
                //                newjd.JOB_ID = row.Cells["JOB_ID"].Value.ToString();
                //                newjd.StartDate = DateTime.Parse(row.Cells["StartDate"].Value.ToString());
                //                newjd.Enddate = DateTime.Parse(row.Cells["Enddate"].Value.ToString());
                //                newjd.Decription = row.Cells["Decription"].Value.ToString();
                //                newjd.Equipment = row.Cells["Equipment"].Value.ToString();

                //                if (row.Cells["ImgBefore"].Value != null)
                //                {
                //                    byte[] imageData = (byte[])row.Cells["ImgBefore"].Value;
                //                    Image newImage;
                //                    using (MemoryStream ms = new MemoryStream(imageData, 0, imageData.Length))
                //                    {
                //                        ms.Write(imageData, 0, imageData.Length);
                //                        newImage = Image.FromStream(ms, true);
                //                    }

                //                    var bfImg1 = ImageToByteArray(newImage);
                //                    var _bfImg1 = new System.Data.Linq.Binary(bfImg1);
                //                    newjd.ImgBeforeRepair = _bfImg1;

                //                }

                //                if (row.Cells["ImgAfter"].Value != null)
                //                {
                //                    byte[] imageData = (byte[])row.Cells["ImgAfter"].Value;
                //                    Image newImage;
                //                    using (MemoryStream ms = new MemoryStream(imageData, 0, imageData.Length))
                //                    {
                //                        ms.Write(imageData, 0, imageData.Length);
                //                        newImage = Image.FromStream(ms, true);
                //                    }

                //                    var baImg1 = ImageToByteArray(newImage);
                //                    var _baImg1 = new System.Data.Linq.Binary(baImg1);

                //                    newjd.ImgAfterRepair = _baImg1;

                //                }


                //                if (row.Cells["Remark"].Value != null)
                //                    newjd.Remark = row.Cells["Remark"].Value.ToString();


                //                dc.JOB_PhotoReport_Details.InsertOnSubmit(newjd);
                //                dc.SubmitChanges();

                //            }
                //        }
                //        _IsMaxPrice = false;
                //        // this.DialogResult = DialogResult.OK;

                //        #endregion
                //    }
                //    catch (Exception ex)
                //    {
                //        closeProgress();
                //        MessageBox.Show("เกิดข้อผิดพลาด...Error " + ex.Message, "ผลการทำงาน", MessageBoxButtons.OK);
                //        return;
                //    }


                //    #endregion
                //    tScope.Complete();
                //}
                #endregion



                using (System.Transactions.TransactionScope tScope =
                    new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required))
                {
                    #region UPLOAD

                    try
                    {
                        newProgressbar();//20210622
                        invoke_Progress("กำลังอัปโหลด รูปภาพ อะไหล่...");
                        //20200623 using (DAL.SMSManage.SMSManageDataContext dc = new DAL.SMSManage.SMSManageDataContext())
                        using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn))
                        {
                            dc.CommandTimeout = 0;
                            dc.ExecuteCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");

                            var bImg1 = ImageToByteArray(pictureBox1.Image);
                            var _img1 = new System.Data.Linq.Binary(bImg1);

                            #region Delete Old JOB
                            var oldj = dc.JOB_PhotoReports
                                .Where(t => t.JOB_ID == txtJOB_ID.Text.Trim() //&& t.StartDate == _startDate && t.Enddate == _endDate
                                        ).FirstOrDefault();
                            if (oldj != null)
                            {
                                dc.JOB_PhotoReports.DeleteOnSubmit(oldj);
                                //20151014 
                                dc.SubmitChanges();


                                /*
                                 
                               //20151013 เปลี่ยนให้เช็คลบทีละรายการ
                                var oldjd = dc.JOB_PhotoReport_Details.Where(t => t.JOB_ID == oldj.JOB_ID).ToList();
                                if (oldjd.Count() > 0)
                                {
                                    dc.JOB_PhotoReport_Details.DeleteAllOnSubmit(oldjd);
                                    dc.SubmitChanges();
                                }
                                 */


                            }



                            #endregion

                            var newj = new DAL.SMSManage.JOB_PhotoReport
                            {
                                JOB_ID = txtJOB_ID.Text.Trim(),
                                StartDate = _startDate,
                                Enddate = (_endDate == null ? DateTime.Now : _endDate.Value),
                                ReportDate = svDT,
                                VAN_ID = UserInfo.Van_ID,
                                Failure_Detail = txtFailure_Detail.Text.Trim(),
                                TranDate = svDT,
                                ImgStation = _img1
                            };

                            dc.JOB_PhotoReports.InsertOnSubmit(newj);
                            //20151014 
                            dc.SubmitChanges();


                            /*
                             * //20151013 เปลี่ยน process check ลบทีละรายการ
                                 foreach (DataGridViewRow row in this.dataGridView_Photo.Rows)
                                 {
                                     var newjd = new DAL.SMSManage.JOB_PhotoReport_Detail();

                                     newjd.Item = int.Parse(row.Cells["Item"].Value.ToString());
                                     newjd.TranDate = DateTime.Parse(row.Cells["TranDate"].Value.ToString());
                                     newjd.JOB_ID = row.Cells["JOB_ID"].Value.ToString();
                                     newjd.StartDate = DateTime.Parse(row.Cells["StartDate"].Value.ToString());
                                     newjd.Enddate = DateTime.Parse(row.Cells["Enddate"].Value.ToString());

                                     if (row.Cells["Decription"].Value != null)
                                         newjd.Decription = row.Cells["Decription"].Value.ToString();


                                     if (row.Cells["Equipment"].Value != null)
                                         newjd.Equipment = row.Cells["Equipment"].Value.ToString();

                                     if (row.Cells["ImgBefore"].Value != null)
                                     {
                                         byte[] imageData = (byte[])row.Cells["ImgBefore"].Value;
                                         Image newImage;
                                         using (MemoryStream ms = new MemoryStream(imageData, 0, imageData.Length))
                                         {
                                             ms.Write(imageData, 0, imageData.Length);
                                             newImage = Image.FromStream(ms, true);
                                         }

                                         var bfImg1 = ImageToByteArray(newImage);
                                         var _bfImg1 = new System.Data.Linq.Binary(bfImg1);
                                         newjd.ImgBeforeRepair = _bfImg1;

                                     }

                                     if (row.Cells["ImgAfter"].Value != null)
                                     {
                                         byte[] imageData = (byte[])row.Cells["ImgAfter"].Value;
                                         Image newImage;
                                         using (MemoryStream ms = new MemoryStream(imageData, 0, imageData.Length))
                                         {
                                             ms.Write(imageData, 0, imageData.Length);
                                             newImage = Image.FromStream(ms, true);
                                         }

                                         var baImg1 = ImageToByteArray(newImage);
                                         var _baImg1 = new System.Data.Linq.Binary(baImg1);

                                         newjd.ImgAfterRepair = _baImg1;

                                     }


                                     if (row.Cells["Remark"].Value != null)
                                         newjd.Remark = row.Cells["Remark"].Value.ToString();


                                     dc.JOB_PhotoReport_Details.InsertOnSubmit(newjd);
                                    dc.SubmitChanges();

                                 } */



                            //20151013 เปลี่ยน process check 
                            foreach (DataGridViewRow row in this.dataGridView_Photo.Rows)
                            {

                                //var oldjd = dc.JOB_PhotoReport_Details
                                //                .Where(t => t.JOB_ID == row.Cells["JOB_ID"].Value.ToString()
                                //                    && t.Equipment == row.Cells["Equipment"].Value.ToString()
                                //                ).ToList();
                                //if (oldjd.Count() <= 0) //ยังไม่เคยมีรูป

                                DateTime vStartDate = DateTime.Parse(row.Cells["StartDate"].Value.ToString());
                                DateTime vEnddate = DateTime.Parse(row.Cells["Enddate"].Value.ToString());


                                var oldjd = dc.JOB_PhotoReport_Details
                                               .Where(t => t.JOB_ID == row.Cells["JOB_ID"].Value.ToString()
                                                   //20190710 ยกเลิก && t.Equipment == row.Cells["Equipment"].Value.ToString()
                                                   //20190710
                                                   && t.Item == int.Parse(row.Cells["Item"].Value.ToString())
                                                   //20190715 && t.StartDate.Date == Convert.ToDateTime(row.Cells["StartDate"].Value.ToString()).Date
                                                   //20190715 && t.Enddate.Date == Convert.ToDateTime(row.Cells["EndDate"].Value.ToString()).Date
                                                   //20200310 test comment && t.StartDate == vStartDate//Convert.ToDateTime(row.Cells["StartDate"].Value.ToString())
                                                   && t.Enddate == vEnddate//Convert.ToDateTime(row.Cells["EndDate"].Value.ToString())
                                               ).FirstOrDefault();
                                if (oldjd == null) //ยังไม่เคยมีรูป
                                {
                                    //var vItem = dc.JOB_PhotoReport_Details
                                    //           .Where(t => t.JOB_ID == row.Cells["JOB_ID"].Value.ToString() 
                                    //             ).Max(t=>t.Item);

                                    /* //20190712 ยกเลิกการ Run Item ใหม่ เพราะต้องใช้เป็น key
                                    var vItem = (from y in dc.JOB_PhotoReport_Details
                                          where y.JOB_ID == row.Cells["JOB_ID"].Value.ToString()
                                                 select (int?)y.Item).Max();

                                        int _vItemPlus = 0;
                                        if (vItem != null)                                  
                                        {
                                             _vItemPlus = int.Parse(vItem.ToString());
                                        }
                                     */
                                    // int aad = _vItemPlus++;
                                    var newjd = new DAL.SMSManage.JOB_PhotoReport_Detail();

                                    newjd.Item = int.Parse(row.Cells["Item"].Value.ToString());//20190712 ยกเลิกการ Run Item ใหม่  _vItemPlus+1;//int.Parse(row.Cells["Item"].Value.ToString());
                                    newjd.TranDate = DateTime.Parse(row.Cells["TranDate"].Value.ToString());
                                    newjd.JOB_ID = row.Cells["JOB_ID"].Value.ToString();
                                    newjd.StartDate = DateTime.Parse(row.Cells["StartDate"].Value.ToString());
                                    newjd.Enddate = DateTime.Parse(row.Cells["Enddate"].Value.ToString());

                                    if (row.Cells["Decription"].Value != null)
                                        newjd.Decription = row.Cells["Decription"].Value.ToString();


                                    if (row.Cells["Equipment"].Value != null)
                                        newjd.Equipment = row.Cells["Equipment"].Value.ToString();

                                    if (row.Cells["ImgBefore"].Value != null)
                                    {
                                        byte[] imageData = (byte[])row.Cells["ImgBefore"].Value;
                                        Image newImage;
                                        using (MemoryStream ms = new MemoryStream(imageData, 0, imageData.Length))
                                        {
                                            ms.Write(imageData, 0, imageData.Length);
                                            newImage = Image.FromStream(ms, true);
                                        }

                                        var bfImg1 = ImageToByteArray(newImage);
                                        var _bfImg1 = new System.Data.Linq.Binary(bfImg1);
                                        newjd.ImgBeforeRepair = _bfImg1;

                                    }

                                    if (row.Cells["ImgAfter"].Value != null)
                                    {
                                        byte[] imageData = (byte[])row.Cells["ImgAfter"].Value;
                                        Image newImage;
                                        using (MemoryStream ms = new MemoryStream(imageData, 0, imageData.Length))
                                        {
                                            ms.Write(imageData, 0, imageData.Length);
                                            newImage = Image.FromStream(ms, true);
                                        }

                                        var baImg1 = ImageToByteArray(newImage);
                                        var _baImg1 = new System.Data.Linq.Binary(baImg1);

                                        newjd.ImgAfterRepair = _baImg1;

                                    }


                                    if (row.Cells["Remark"].Value != null)
                                        newjd.Remark = row.Cells["Remark"].Value.ToString();


                                    dc.JOB_PhotoReport_Details.InsertOnSubmit(newjd);
                                    //20151014 
                                    dc.SubmitChanges();

                                }
                                else //เคยมีรูปแล้ว
                                {

                                    oldjd.TranDate = DateTime.Parse(row.Cells["TranDate"].Value.ToString());
                                    //oldjd.JOB_ID = row.Cells["JOB_ID"].Value.ToString();
                                    //oldjd.StartDate = DateTime.Parse(row.Cells["StartDate"].Value.ToString());
                                    //oldjd.Enddate = DateTime.Parse(row.Cells["Enddate"].Value.ToString());

                                    if (row.Cells["Decription"].Value != null)
                                        oldjd.Decription = row.Cells["Decription"].Value.ToString();


                                    if (row.Cells["Equipment"].Value != null)
                                        oldjd.Equipment = row.Cells["Equipment"].Value.ToString();

                                    if (row.Cells["ImgBefore"].Value != null)
                                    {
                                        byte[] imageData = (byte[])row.Cells["ImgBefore"].Value;
                                        Image newImage;
                                        using (MemoryStream ms = new MemoryStream(imageData, 0, imageData.Length))
                                        {
                                            ms.Write(imageData, 0, imageData.Length);
                                            newImage = Image.FromStream(ms, true);
                                        }

                                        var bfImg1 = ImageToByteArray(newImage);
                                        var _bfImg1 = new System.Data.Linq.Binary(bfImg1);
                                        oldjd.ImgBeforeRepair = _bfImg1;

                                    }//   if (row.Cells["ImgBefore"].Value != null)

                                    if (row.Cells["ImgAfter"].Value != null)
                                    {
                                        byte[] imageData = (byte[])row.Cells["ImgAfter"].Value;
                                        Image newImage;
                                        using (MemoryStream ms = new MemoryStream(imageData, 0, imageData.Length))
                                        {
                                            ms.Write(imageData, 0, imageData.Length);
                                            newImage = Image.FromStream(ms, true);
                                        }

                                        var baImg1 = ImageToByteArray(newImage);
                                        var _baImg1 = new System.Data.Linq.Binary(baImg1);

                                        oldjd.ImgAfterRepair = _baImg1;

                                    }// if (row.Cells["ImgAfter"].Value != null)


                                    if (row.Cells["Remark"].Value != null)
                                        oldjd.Remark = row.Cells["Remark"].Value.ToString();

                                    dc.SubmitChanges();

                                } //End else //เคยมีรูปแล้ว

                            }

                        }
                        closeProgress();
                        tScope.Complete();

                        MessageBox.Show("Upload ข้อมูลเรียบร้อย ...", "ผลการทำงาน", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //this.DialogResult = DialogResult.Yes;
                        this.Close();
                    }
                    catch (Exception ex)
                    {
                        closeProgress();
                        MessageBox.Show("เกิดข้อผิดพลาด...Error " + ex.Message, "ผลการทำงาน", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    #endregion

                }


            }
        }

        private void toolStripButton_Cancel_Click(object sender, EventArgs e)
        {
            JobUploadPhoto_Load(null, null);
        }

        public Bitmap ResizeBitmap(Bitmap bmp, int nWidth, int nHeight)
        {
            Bitmap result = new Bitmap(nWidth, nHeight);
            using (Graphics g = Graphics.FromImage((Image)result))
            {
                g.DrawImage(bmp, 0, 0, nWidth, nHeight);
            }

            return result;
        }

        private byte[] ImageToByteArray(System.Drawing.Image imageIn)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                return ms.ToArray();
            }
        }


        public Image ByteArrayToImage(byte[] byteArrayIn)
        {
            var ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }


        private void btImg1_Click(object sender, EventArgs e)
        {
            OpenFileDialog imageDialog = new OpenFileDialog();
            imageDialog.Filter = "Pictures (*.jpg)|*.jpg";
            imageDialog.FilterIndex = 1;
            imageDialog.Multiselect = true;

            if (imageDialog.ShowDialog() == DialogResult.OK)
            {
                var img = ResizeBitmap(new Bitmap(imageDialog.FileName), 190, 190);
                pictureBox1.Image = img;

                groupBox3.Enabled = true;
            }
        }


        private void txtJOB_ID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    Finding.FindJobClose f = new SMSMINI.Transaction.Finding.FindJobClose();
                    f.TmpEditMode = "job_upload";
                    if (f.ShowDialog() == DialogResult.OK)
                    {
                        _jOBNo = f.TmpJobID;
                        if (_jOBNo != "")
                        {
                            getJOBPhoto(_jOBNo);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("เกิดข้อผิดพลาด " + ex.Message, "เกิดข้อผิพลาด...", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

            }
        }

        private void getJOBPhoto(string jobNO)
        {
            //MessageBox.Show("1 jobNO " + jobNO);

            DAL.SMSManage.JOB_Detail _jd = null;


            //20200623 using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn))
            {
                dc.ExecuteCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");

                var j1 = dc.JOBs.Where(t => t.JOB_ID == jobNO)
                              .FirstOrDefault();

                if (j1 != null)//มี JOB พร้อม Upload
                {
                    // MessageBox.Show("2 j1.JOB_ID " + j1.JOB_ID);

                    try
                    {
                        _jOBNo = j1.JOB_ID;
                        _failureDetail = j1.JobFailure_Detail;

                        var st = dc.Stations.Where(t => t.STA_ID == j1.STA_ID).FirstOrDefault();
                        _stationNo = st.STA_ID;
                        _stationName = st.StationSys;

                        var wid = dc.JOB_WorkIDs.Where(t => t.JOB_ID == j1.JOB_ID).FirstOrDefault();
                        _workID = wid == null ? "" : wid.Work_ID;


                        var jdp = dc.JOB_Details
                            .Where(t => t.JOB_ID == j1.JOB_ID
                            //20200226
                            && t.EndDate.Value == _endDate
                            //=======
                            )
                            .FirstOrDefault();

                        if (jdp != null)
                        {
                            _jd = jdp;
                        }
                    }
                    catch (Exception exx)
                    {
                        // MessageBox.Show("2.1 ERROR " + exx.Message);

                    }


                }
                //

                //MessageBox.Show("3....");
                try
                {
                    var jphoto = dc.JOB_PhotoReports
                    .Where(t => t.JOB_ID == j1.JOB_ID)
                    .FirstOrDefault();

                    //MessageBox.Show("3.1... _jd.JOB_ID: " + _jd.JOB_ID);

                    if (jphoto != null)
                    {
                        //MessageBox.Show("3.2...    if (jphoto != null)");
                        byte[] img = (byte[])(jphoto.ImgStation).ToArray();
                        pictureBox1.Image = ByteArrayToImage(img);
                    }
                    else
                        pictureBox1.Image = null;

                    if (_jd != null)
                    {
                        DateTime dEndDate = DateTime.Now;
                        if (_jd.EndDate != null)
                            dEndDate = _jd.EndDate.Value;

                        var jd = dc.JOB_PhotoReport_Details
                            .Where(t => t.JOB_ID == _jd.JOB_ID &&
                                       t.StartDate == _jd.StartDate &&
                                       t.Enddate == dEndDate)//20151119
                                       .ToList();

                        // MessageBox.Show("3.3... var jd = dc.JOB_PhotoReport_Details");
                        // if (jd.Count ()>0)
                        //  

                        //ยกเลิก 20151002 lsJPart = new List<SMSMINI.DAL.SMSManage.JOB_PhotoReport_Detail>();
                        if (jd.Count() > 0)
                        {
                            //MessageBox.Show("3.4... if (jd.Count() > 0)");
                            lsJPart_cur = jd;
                            foreach (var item in jd)
                            {

                                lsJPart.Add(new DAL.SMSManage.JOB_PhotoReport_Detail()
                                {
                                    Item = item.Item,
                                    JOB_ID = item.JOB_ID,
                                    StartDate = item.StartDate,
                                    Enddate = item.Enddate,
                                    ImgBefore = null,
                                    ImgAfter = null,
                                    Equipment = item.Equipment,
                                    Decription = item.Decription,
                                    TranDate = item.TranDate,
                                    Remark = item.Remark,
                                    ImgBeforeRepair = null,
                                    ImgAfterRepair = null
                                });
                            }

                        }
                    }//   if (_jd != null)
                     // else
                     //MessageBox.Show("xxx if (_jd != null)");

                }


                catch (Exception ex)
                {

                    MessageBox.Show("3.5..ERROR " + ex.Message);
                }


            }



            if (lsJPart.Count() > 0)
            {
                try
                {
                    // MessageBox.Show("3.6    if (lsJPart.Count() > 0)");
                    if (lsJPart_cur.Count() <= 0)
                        lsJPart_cur = lsJPart;

                    dataGridView_Photo.DataSource = lsJPart.ToList();

                    //MessageBox.Show("3.7 dataGridView_Photo.DataSource = lsJPart.ToList();");
                }
                catch (Exception exx)
                {
                    MessageBox.Show("3.8 ERROR " + exx.Message);

                }

            }
            else
                dataGridView_Photo.DataSource = null;


            // MessageBox.Show("4 textBinding();");
            textBinding();
        }

        private void newProgressbar()
        {
            m_fmProgress = new ProgressBar.FormProgress();
            backgroundWorker1.RunWorkerAsync();
            System.Threading.Thread.Sleep(3000);
        }

        private void closeProgress()
        {
            if (m_fmProgress != null)
            {
                m_fmProgress.Invoke(
                    (MethodInvoker)delegate ()
                    {
                        m_fmProgress.Hide();
                        m_fmProgress.Dispose();
                        m_fmProgress = null;
                    }
                );
            }
        }

        private void invoke_Progress(string strMsg)
        {
            try
            {
                m_fmProgress.lblDescription.Invoke(
                   (MethodInvoker)delegate ()
                   {
                       m_fmProgress.lblDescription.Text = strMsg;
                   }
                   );

            }
            catch (System.Exception)
            {

            }
           
        }

            private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            m_fmProgress.Activate();
            m_fmProgress.BringToFront();
            m_fmProgress.TopMost = true;
            m_fmProgress.ShowDialog();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }


        private void UploadAttachment(DataGridViewCell dgvCell, int CellIndex)
        {
            using (OpenFileDialog fileDialog = new OpenFileDialog())
            {
                //Set File dialog properties
                fileDialog.CheckFileExists = true;
                fileDialog.CheckPathExists = true;
                fileDialog.Filter = "Pictures (*.jpg)|*.jpg";// "Pictures (*.jpg)|*.jpg";
                fileDialog.Title = "Select a file";
                fileDialog.Multiselect = false;

                if (fileDialog.ShowDialog() == DialogResult.OK)
                {
                    FileInfo fileInfo = new FileInfo(fileDialog.FileName);
                    byte[] binaryData = File.ReadAllBytes(fileDialog.FileName);
                    Image returnImage = null;
                    try
                    {
                        MemoryStream ms = new MemoryStream(binaryData);
                        var img = Image.FromStream(ms, false, true);
                        returnImage = ResizeBitmap(new Bitmap(img), 190, 190);
                    }
                    catch
                    {
                        MessageBox.Show("Not an Image File", "Show Picture", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }


                    if (dataGridView_Photo.Columns[dgvCell.ColumnIndex].Width < returnImage.Width)
                        dataGridView_Photo.Columns[dgvCell.ColumnIndex].Width = returnImage.Width;

                    if (dataGridView_Photo.Rows[dgvCell.RowIndex].Height < returnImage.Height)
                        dataGridView_Photo.Rows[dgvCell.RowIndex].Height = returnImage.Height;

                    dataGridView_Photo.Rows[dgvCell.RowIndex].Cells[CellIndex].Value = returnImage;

                    var q = lsJPart_cur.Where(t => t.Item == int.Parse(dataGridView_Photo.Rows[dgvCell.RowIndex].Cells["Item"].Value.ToString()))
                        .FirstOrDefault();
                    if (q != null)
                    {
                        if (CellIndex == 11)
                            q.ImgBeforeRepair = binaryData;
                        else
                            q.ImgAfterRepair = binaryData;

                    }
                }
            }
        }


        //private void dataGridView_Photo_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        //{
        //    //if (e.RowIndex != -1)
        //    //{
        //    //    if (e.ColumnIndex != 0)
        //    //        return;

        //    //    if ((MessageBox.Show("คุณต้องการยกเลิก หรือ แก้ไข ข้อมูลใช่หรือไม่", "คำยืนยัน",
        //    //        MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No))
        //    //        return;

        //    //    try
        //    //    {


        //    //        var item = int.Parse(dataGridView_Photo["Item", e.RowIndex].Value.ToString());
        //    //        var q = lsJPart.Where(t => t.Item == item).FirstOrDefault();

        //    //        if (q != null)
        //    //        {
        //    //            //txtItem.Text = q.Item.ToString();

        //    //            if (q.ImgBeforeRepair != null)
        //    //            {
        //    //                byte[] img2 = (byte[])(q.ImgBeforeRepair).ToArray();
        //    //                //pictureBox2.Image = ByteArrayToImage(img2);
        //    //            }

        //    //            if (q.ImgAfterRepair != null)
        //    //            {
        //    //                byte[] img3 = (byte[])(q.ImgAfterRepair).ToArray();
        //    //                //pictureBox3.Image = ByteArrayToImage(img3);
        //    //            }

        //    //            //txtEquipment.Text = q.Equipment;
        //    //            //txtDescription.Text = q.Decription;
        //    //            //txtRemark.Text = q.Remark;

        //    //            lsJPart.Remove(q);
        //    //        }

        //    //        dataGridView_Photo.DataSource = lsJPart.ToList();

        //    //    }
        //    //    catch (Exception ex)
        //    //    {
        //    //        MessageBox.Show(ex.Message);
        //    //    }

        //    //}

        //}

        private void dataGridView_Photo_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                //20210427 if (e.ColumnIndex == 1 || e.ColumnIndex == 2 || e.ColumnIndex == 11 || e.ColumnIndex == 12 || e.ColumnIndex == 13)//
                if (e.ColumnIndex == 1 || e.ColumnIndex == 2 || e.ColumnIndex == 12 || e.ColumnIndex == 13)//20210427
                {
                    //Index == 11,ImgBefore
                    //Index == 12,ImgAfter                   
                    UploadAttachment(dataGridView_Photo.SelectedCells[0], e.ColumnIndex);
                }
                else
                    return;
            }
            else
                MessageBox.Show("Select a single cell from Attachment column", "Error uploading file", MessageBoxButtons.OK, MessageBoxIcon.Error);

        }

        //private void dataGridView_Photo_CellContentClick(object sender, DataGridViewCellEventArgs e)
        //{
        //    //if (e.RowIndex != -1 && e.ColumnIndex == 0)
        //    //{ 

        //    //    if ((MessageBox.Show("คุณต้องการยกเลิก หรือ แก้ไข ข้อมูลใช่หรือไม่", "คำยืนยัน",
        //    //        MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No))
        //    //        return;

        //    //    try
        //    //    {


        //    //        var item = int.Parse(dataGridView_Photo["Item", e.RowIndex].Value.ToString());
        //    //        var q = lsJPart.Where(t => t.Item == item).FirstOrDefault();

        //    //        if (q != null)
        //    //        {
        //    //            //txtItem.Text = q.Item.ToString();

        //    //            if (q.ImgBeforeRepair != null)
        //    //            {
        //    //                byte[] img2 = (byte[])(q.ImgBeforeRepair).ToArray();
        //    //                //pictureBox2.Image = ByteArrayToImage(img2);
        //    //            }

        //    //            if (q.ImgAfterRepair != null)
        //    //            {
        //    //                byte[] img3 = (byte[])(q.ImgAfterRepair).ToArray();
        //    //                //pictureBox3.Image = ByteArrayToImage(img3);
        //    //            }

        //    //            //txtEquipment.Text = q.Equipment;
        //    //            //txtDescription.Text = q.Decription;
        //    //            //txtRemark.Text = q.Remark;

        //    //            lsJPart.Remove(q);
        //    //        }

        //    //        //int _no = 1;
        //    //        //foreach (var itemx in lsJPart)
        //    //        //{
        //    //        //    itemx.Item = _no;
        //    //        //    _no++;
        //    //        //}

        //    //        dataGridView_Photo.DataSource = lsJPart.ToList();

        //    //    }
        //    //    catch (Exception ex)
        //    //    {
        //    //        MessageBox.Show(ex.Message);
        //    //    }

        //    //}

        //}

        int cellSelect = 0;
        private void btSaveImg_Click(object sender, EventArgs e)
        {
            if (dataGridView_Photo.SelectedCells.Count == 1 &&
                dataGridView_Photo.SelectedCells[cellSelect].Value != null)
            {
                if ((MessageBox.Show("คุณต้องการบันทึกรูปภาพ ใช่หรือไม่", "คำยืนยัน",
                 MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No))
                    return;
                DownloadAttachment(dataGridView_Photo.SelectedCells[cellSelect]);
            }
            else
                MessageBox.Show("Select a single cell from Attachment column", "Error uploading file",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);

        }
        private void DownloadAttachment(DataGridViewCell dgvCell)
        {

            string fileName = _SPAID == "" ? DateTime.Now.ToString("yyyyMMdd_hhmmss") : _SPAID;


            if (fileName == string.Empty)
                return;

            FileInfo fileInfo = new FileInfo(fileName);
            string fileExtension = fileInfo.Extension;

            byte[] byteData = null;

            using (SaveFileDialog saveFileDialog1 = new SaveFileDialog())
            {
                saveFileDialog1.Filter = "Files (*.jpg)|*.jpg";
                saveFileDialog1.Title = "Save File as";
                saveFileDialog1.CheckPathExists = true;
                saveFileDialog1.FileName = fileName;

                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    byteData = (byte[])dgvCell.Value;
                    File.WriteAllBytes(saveFileDialog1.FileName, byteData);


                    //MemoryStream ms = new MemoryStream(byteData);
                    //var img = Image.FromStream(ms, false, true);
                    //Image returnImage = ResizeBitmap(new Bitmap(img), 1600, 1000);
                    //var byteData2 = ImageToByteArray(returnImage);
                    //File.WriteAllBytes(saveFileDialog1.FileName, byteData2);
                }
            }
            /*1212D0554
             * 1600
                1000*/
        }

        private void dataGridView_Photo_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            for (int i = 0; i < dataGridView_Photo.Rows.Count; i++)
            {


                var im = lsJPart_cur
                        .Where(t => t.Item == int.Parse(dataGridView_Photo.Rows[i].Cells["Item"].Value.ToString()))
                        .FirstOrDefault();
                if (im != null)
                {
                    if (im.ImgBeforeRepair != null)
                    {
                        byte[] binaryData = im.ImgBeforeRepair.ToArray();
                        Image returnImage = null;
                        try
                        {
                            MemoryStream ms = new MemoryStream(binaryData);
                            var img = Image.FromStream(ms, false, true);
                            returnImage = ResizeBitmap(new Bitmap(img), 190, 190);
                        }
                        catch
                        {

                        }
                        dataGridView_Photo.Columns["ImgBefore"].Width = 190;
                        dataGridView_Photo.Columns["ImgAfter"].Width = 190;
                        dataGridView_Photo.Rows[i].Height = 190;
                        dataGridView_Photo.Rows[i].Cells["ImgBefore"].Value = returnImage;
                    }

                    if (im.ImgAfterRepair != null)
                    {
                        byte[] binaryData = im.ImgAfterRepair.ToArray();
                        Image returnImage = null;
                        try
                        {
                            MemoryStream ms = new MemoryStream(binaryData);
                            var img = Image.FromStream(ms, false, true);
                            returnImage = ResizeBitmap(new Bitmap(img), 190, 190);
                        }
                        catch
                        {

                        }
                        dataGridView_Photo.Columns["ImgBefore"].Width = 190;
                        dataGridView_Photo.Columns["ImgAfter"].Width = 190;
                        dataGridView_Photo.Rows[i].Height = 190;
                        dataGridView_Photo.Rows[i].Cells["ImgAfter"].Value = returnImage;
                    }


                }


            }
        }



        private void btCancel_Click(object sender, EventArgs e)
        {
            if ((MessageBox.Show("คุณต้องการ ยกเลิกข้อมูล ใช่หรือไม่", "คำยืนยัน",
                  MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No))
                return;

            /* //20190717 เช็คข้อมูล ถ้ามีให้ลบบน server จริง
            try
            {
                var item = int.Parse(dataGridView_Photo["Item", xRowIndex].Value.ToString());
                var ImgBefore = dataGridView_Photo["ImgBefore", xRowIndex].Value;

                var q = lsJPart.Where(t => t.Item == item && t.ImgBefore == ImgBefore).FirstOrDefault();

                if (q != null)
                {
                    lsJPart.Remove(q);
                }

                dataGridView_Photo.DataSource = lsJPart.ToList();
            }
            catch (Exception)
            {
            }
             */

            //20190717
            try
            {

                servDate = getServerDate();
                DateTime vStartDate = DateTime.Parse(dataGridView_Photo["StartDate", xRowIndex].Value.ToString());
                DateTime vEnddate = DateTime.Parse(dataGridView_Photo["Enddate", xRowIndex].Value.ToString());

                using (DAL.SMSManage.SMSManageDataContext dc = new DAL.SMSManage.SMSManageDataContext())
                {
                    dc.CommandTimeout = 0;
                    dc.ExecuteCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");

                    var oldjd = dc.JOB_PhotoReport_Details
                                   .Where(t => t.JOB_ID == dataGridView_Photo["JOB_ID", xRowIndex].Value.ToString()
                                       && t.Item == int.Parse(dataGridView_Photo["Item", xRowIndex].Value.ToString())
                                       && t.StartDate == vStartDate
                                       && t.Enddate == vEnddate
                                   ).FirstOrDefault();
                    if (oldjd == null) //ยังไม่เคยมีรูป
                    {

                        var item = int.Parse(dataGridView_Photo["Item", xRowIndex].Value.ToString());
                        var ImgBefore = dataGridView_Photo["ImgBefore", xRowIndex].Value;

                        var q = lsJPart.Where(t => t.Item == item
                            && t.ImgBefore == ImgBefore
                            ).FirstOrDefault();

                        if (q != null)
                        {
                            lsJPart.Remove(q);
                        }

                        dataGridView_Photo.DataSource = lsJPart.ToList();
                    }
                    else
                    {
                        if ((MessageBox.Show("คุณต้องการ ลบข้อมูล ที่มีอยู่บน Server ใช่หรือไม่", "คำยืนยัน",
                           MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes))
                        {
                            var ld = new DAL.SMSManage.Log_DeleteDatabyProgram
                            {
                                Trandate = servDate,
                                DataID = dataGridView_Photo["JOB_ID", xRowIndex].Value.ToString(),
                                TableName = "JOB_PhotoReport_Detail",
                                Menu = "Upload Photo",
                                Detail = "Datete Item " + dataGridView_Photo["Item", xRowIndex].Value.ToString() + " StartDate " + dataGridView_Photo["StartDate", xRowIndex].Value.ToString() + " EndDate " + dataGridView_Photo["Enddate", xRowIndex].Value.ToString() + " SPA_ID " + dataGridView_Photo["Equipment", xRowIndex].Value.ToString() + " by User ",
                                UserID = UserInfo.UserId,
                                Apps = "SMSMINI"

                            };
                            dc.Log_DeleteDatabyPrograms.InsertOnSubmit(ld);
                            dc.SubmitChanges();

                            //--
                            dc.JOB_PhotoReport_Details.DeleteOnSubmit(oldjd);
                            dc.SubmitChanges();

                            //--
                            var item = int.Parse(dataGridView_Photo["Item", xRowIndex].Value.ToString());
                            var ImgBefore = dataGridView_Photo["ImgBefore", xRowIndex].Value;

                            var q = lsJPart.Where(t => t.Item == item
                               && t.ImgBefore == ImgBefore
                               ).FirstOrDefault();

                            if (q != null)
                            {
                                lsJPart.Remove(q);
                            }
                            dataGridView_Photo.DataSource = lsJPart.ToList();
                        }
                    }//มีรูปบน server
                }//dal
            }
            catch
            {

            }




        }

        string _SPAID = "";
        int xRowIndex = 0;
        private void dataGridView_Photo_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            if (e.RowIndex != -1)
            {
                xRowIndex = e.RowIndex;
                if (dataGridView_Photo["Equipment", e.RowIndex].Value != null)
                    _SPAID = dataGridView_Photo["Equipment", e.RowIndex].Value.ToString();
            }
        }

        private void dataGridView_Photo_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dataGridView_Photo.CurrentCell is DataGridViewCheckBoxCell)
                dataGridView_Photo.CommitEdit(DataGridViewDataErrorContexts.Commit);

        }


        private void btAddnew_Click(object sender, EventArgs e)
        {
            /*
            int newC = 0 ;
            int curC = 0;

            curC = dataGridView_Photo.RowCount;
            //เก็บค่า datagrid ปัจจุบัน
            lsNewCPart = null;//dataGridView_Photo.valu//dataGridView_Photo.bi;
            //lsNewCPart.Add(dataGridView_Photo);
            //for (int i = 0; i < dataGridView_Photo.Rows.Count; i++)
            foreach (DataGridViewRow row in this.dataGridView_Photo.Rows)
            {
                newC++;
                if (row.Cells["ImgBefore"].Value != null)
                {
                    byte[] imageData = (byte[])row.Cells["ImgBefore"].Value;
                    Image newImage;
                    using (MemoryStream ms = new MemoryStream(imageData, 0, imageData.Length))
                    {
                        ms.Write(imageData, 0, imageData.Length);
                        newImage = Image.FromStream(ms, true);
                    }

                    var bfImg1 = ImageToByteArray(newImage);
                    var _bfImg1 = new System.Data.Linq.Binary(bfImg1);
                    //newjd.ImgBeforeRepair = _bfImg1;

                }

                if (row.Cells["ImgAfter"].Value != null)
                {
                    byte[] imageData = (byte[])row.Cells["ImgAfter"].Value;
                    Image newImage;
                    using (MemoryStream ms = new MemoryStream(imageData, 0, imageData.Length))
                    {
                        ms.Write(imageData, 0, imageData.Length);
                        newImage = Image.FromStream(ms, true);
                    }

                    var baImg1 = ImageToByteArray(newImage);
                    var _baImg1 = new System.Data.Linq.Binary(baImg1);

                    //newjd.ImgAfterRepair = _baImg1;

                }
                lsJPart = null;//reset ค่าใหม่

                //lsNewCPart.Add(new DAL.SMSManage.JOB_PhotoReport_Detail()
                lsJPart.Add(new DAL.SMSManage.JOB_PhotoReport_Detail()
                       {
                           Item = newC,
                           JOB_ID = row.Cells["JOB_ID"].Value.ToString(),
                           StartDate = DateTime.Parse(row.Cells["StartDate"].Value.ToString()),
                           Enddate = DateTime.Parse(row.Cells["Enddate"].Value.ToString()),
                           ImgBefore = null,
                           ImgAfter = null,
                           Equipment = row.Cells["Equipment"].Value != null ? row.Cells["Equipment"].Value.ToString() : null,
                           Decription = null,//row.Cells["Decription"].Value != null ? row.Cells["Decription"].Value.ToString() : null,
                           Remark =  null,//row.Cells["Remark"].Value != null ? row.Cells["Remark"].Value.ToString() : null,
                           ImgBeforeRepair = null,
                           ImgAfterRepair = null,
                           TranDate =  null,//DateTime.Parse(row.Cells["TranDate"].Value.ToString())
                       });                           
                    
                    
           
            }
        */



            //เพิ่มต่อจาก ค่าที่เรียงใหม่
            int btAddnew_item = 1;
            for (int i = 0; i <= dataGridView_Photo.RowCount - 1; i++)
            {
                btAddnew_item++;
            }


            lsJPart.Add(new DAL.SMSManage.JOB_PhotoReport_Detail
            {
                Item = btAddnew_item,
                JOB_ID = txtJOB_ID.Text.Trim(),
                StartDate = dateTimePicker1.Value,
                Enddate = dateTimePicker2.Value,
                ImgBefore = null,
                ImgAfter = null,
                Equipment = "",
                Decription = txtFailure_Detail.Text,// 20210427 ""
                Remark = "",
                ImgBeforeRepair = null,
                ImgAfterRepair = null,
                TranDate = dateTimePicker1.Value
            });

            dataGridView_Photo.DataSource = lsJPart.ToList();

        }

        private DateTime getServerDate()
        {
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            {
                var srv = dc
                     .ExecuteQuery<DateTime>("SELECT GETDATE()")
                     .First();
                if (srv != null)
                    return srv;
                else
                    return DateTime.Now;

            }
        }

        private void dataGridView_Photo_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            //20210622 dataGridView_Photo["JOB", e.RowIndex].ReadOnly = true;
            //20210622 dataGridView_Photo["JOB", e.RowIndex].Style.BackColor = Color.LightGray;
            dataGridView_Photo["JOB_ID", e.RowIndex].ReadOnly = true;
            dataGridView_Photo["JOB_ID", e.RowIndex].Style.BackColor = Color.LightGray;
        }
    }
}