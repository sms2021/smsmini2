﻿using System;
using System.Windows.Forms;

namespace SMSMINI.Transaction
{
    public partial class PopStartLiteMessage : Form
    {
        public PopStartLiteMessage()
        {
            InitializeComponent();
        }

        private void btOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
