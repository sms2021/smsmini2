﻿


using Janawat.Application.Data;

using JR.Utils.GUI.Forms;
using Microsoft.VisualBasic;
using Newtonsoft.Json;
using SMSMINI.DAL;
using SMSMINI.DAL.SMSManage;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Windows.Forms;


namespace SMSMINI.Transaction
{
    public partial class JobClose : Form
    {
        private ProgressBar.FormProgress m_fmProgress = null;

        DataTable dtFailure = null;
        DataTable dtPart = null;
        string tmpjobID = "";
        string tmpVANID = "";//20200205 chk stop ML
        string editMode = "";


        string tmpRemarkJobContentMent = "";//ความพึงพอใจลูกค้า = (C) ปรับปรุง

        int PartCount = 0;

        string tmpLocation = "";
        string tmpSN = "";
        string tmpSNChkWarranty = "";//20210129
        string tmpSNOLD = "";
        string tmpFAIID = "";




        DateTime _dateServer = DateTime.Now;//Get datetime from Server
        DateTime _Jobopendate = DateTime.Now;//Get date open job

        string tmpCalPoitFailID = "";
        string tmpCalSN = "";
        string tmpCalFAI_ID = "";

        string tmpCalSPA_ID = "";
        int tmpPA_ID_NO = 0;

        List<string> chkCancelSPA_ID;
        List<string> logCancelSPA_ID;

        List<SNCancel> lsSNCancel = null;
        List<SNCancel> lsSNSelect = null;

        string tmpJobType = "";
        string tmpTYP_ID1 = "";

        string tmpGOPId = "";
        string tmpSTAID = "";

        string tmpGopName = "";
        int tmpRounup = 0;

        string tmpEndDate = "";
        string tmpEndTime = "";

        //20191224
        string stmpEndDate = "";
        string stmpEndTime = "";

        string tmpStatusPart = "";
        string tmpStatusPartID = "";

        string strConn = "";

        int FCAT_ID = 0;
        int FBA_ID = 0;
        int FMD_ID = 0;

        int LOC_ID = 0;

        bool IsQT = true;

        string tmpSPA_ID = string.Empty;

        bool isLockTranType = false;
        string _contractNo = string.Empty;

        double _vat = 0;

        decimal _PricesList = 0;
        decimal _Prices = 0;
        decimal _Discount = 0;
        decimal _SumPrices = 0;
        decimal _SumTotalPrices = 0;
        decimal _SumVAT = 0;
        decimal _Quantity = 0;

        DAL.SMSManage.JOB _job = null;

        List<DAL.SMSManage.JOB> lsJOB = null;

        bool _IsPrintPrice = false;
        bool _IsStationCharge = false;

        //20141224
        bool IsFound;//= true;

        //server date for RecDate_Endate
        DateTime Server_date = DateTime.Now;



        //20180423
        string chkUpload = "0";
        string chkCloseJOB = "0";


        //20200127 get ค่าวันที่เวลาล่าสุด ที่บันทึกไปแล้วแล้วดึงมาแก้ไข
        DateTime? tmpEndDateTimeEditJOB;
        DateTime tmpStartDateTimeEditJOB;
        //=============

        //20200610
        string _pEnddate = null;
        //=======


        //20200626
        List<LChecklogEditDel> LstChecklogEditDel = null;
        //LChecklogEditDel = new List<LChecklogEditDel>();
        //===


        int vFCAT_ID_NEW = 0;  //20180426
        public JobClose()
        {
            InitializeComponent();

            //string _mode = (UserInfo.ConnectMode == "0") ? " [Off line]" : " [On line]";
            string _mode = " [On line]";

            object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
            if (attributes.Length == 0)
            {
                this.Text = "SMS MINI [v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "] " + _mode;

            }
            this.Text = ((AssemblyProductAttribute)attributes[0]).Product + " [v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "] " + _mode;


        }



        private void JobClose_Load(object sender, EventArgs e)
        {

            try
            {

                btEditContractNo.Visible = false;
                btUpdatePart.Visible = false;
                btUpdateWorkType.Enabled = false;
                btUpdateWorkType.BackColor = Color.Transparent;

                label_1.Visible = false;
                label_2.Visible = false;
                label_3.Visible = false;


                lblJobInCount.Text = "";

                chkCancelSPA_ID = new List<string>();
                lsSNCancel = new List<SNCancel>();
                lsSNSelect = new List<SNCancel>();

                editMode = "";

                disableControl(true);
                ClearControl();

                setComboBox();

                setDtFailure();
                setDtPart();

                FailureBinding();
                PartBindind();

                //20200218 ทำไมต้องเคลียร์หลายรอบ ClearControl();

                ClearTextFail();
                ClearPartText();

                dataGridView_tmpPart.DataSource = null;
                dataGridView_tmpPart.Refresh();

                dataGridView_FailPoint.Enabled = false;

                butttonEnable(false, false, false, false, true);

                //20150629
                chkEnableSVReport(txtJobID.Text.Trim());



                //20191202 toolStripComboBox1.SelectedIndex = 1;

                toolStripComboBox1.SelectedIndex = 0;

                RadioButtonList_JobStatus1.Checked = false;
                RadioButtonList_JobStatus2.Checked = false;

                FailureBinding();
                PartBindind();

                toolStripLabel_cobby.SelectedIndex = 0;


                tabControl1.SelectedIndex = 0;
                groupBox3.Enabled = false;

                panel_selectEndDate.Visible = false;

                _IsStationCharge = false;

                //20200626
                //List<LChecklogEditDel> LstChecklogEditDel = null;
                //LChecklogEditDel = new List<LChecklogEditDel>();
                //===

            }
            catch (System.Exception ex)
            {
                closeProgress();
                MessageBox.Show("ข้อผิดพลาด " + ex.Message, "เกิดข้อผิดพลาด");
            }


            //20210128 txtSNOld.Text = (txtSNOld.Text == "" ? "None" : txtSNOld.Text);
            //20210128 txtSNNew.Text = (txtSNNew.Text == "" ? "None" : txtSNNew.Text);



            lblWarranty.Visible = false;


        }


        private void butttonEnable(bool isEnable, bool _Save, bool _Cancel, bool _Print, bool _Exit)
        {
            //toolStripLabel_cobby.Enabled = isEnable;
            toolStripText_txtFind.Enabled = isEnable;
            toolStripButton_FindStation.Enabled = isEnable;

            toolStripButton_Save.Enabled = _Save;
            toolStripButton_Cancel.Enabled = _Cancel;
            toolStripButton_Print.Enabled = _Print;
            //toolStripBtSVAdmin.Enabled = _Print;

            toolStripButton_TAGPrimt.Enabled = _Print;
            btFTRPrint.Enabled = _Print;
            toolStripButton_Exit.Enabled = _Exit;
        }

        private void ClearControl()
        {
            ClearControl(groupBox1.Controls);
            ClearControl(groupBox2.Controls);
            ClearControl(groupBox3.Controls);
            ClearControl(groupBox4.Controls);
        }

        //จุดเสีย
        private void setDtFailure()
        {
            dtFailure = new DataTable();
            dtFailure.Columns.Add(new DataColumn("NO."));
            dtFailure.Columns.Add(new DataColumn("รหัสจุดเสีย"));
            dtFailure.Columns.Add(new DataColumn("จุดเสีย"));
            dtFailure.Columns.Add(new DataColumn("อาการเสีย"));
            dtFailure.Columns.Add(new DataColumn("S/N(Old)"));
            dtFailure.Columns.Add(new DataColumn("FixAssetNo"));

            dtFailure.Columns.Add(new DataColumn("S/N(New)"));
            dtFailure.Columns.Add(new DataColumn("FixAssetNo_New"));

            dtFailure.Columns.Add(new DataColumn("FCAT_ID"));
            dtFailure.Columns.Add(new DataColumn("FBA_ID"));
            dtFailure.Columns.Add(new DataColumn("FMD_ID"));

            dtFailure.Columns.Add(new DataColumn("Location"));
            dtFailure.Columns.Add(new DataColumn("Model"));

            dtFailure.Columns.Add(new DataColumn("StartLiter"));
            dtFailure.Columns.Add(new DataColumn("EndLiter"));
            dtFailure.Columns.Add(new DataColumn("LiterTest"));
            dtFailure.Columns.Add(new DataColumn("FAI_ID"));

            //20141020
            dtFailure.Columns.Add(new DataColumn("PRT_ID"));
            //20141127ยกเลิก dtFailure.Columns.Add(new DataColumn("EQP_ID"));
            dtFailure.Columns.Add(new DataColumn("ATF_ID"));
            dtFailure.Columns.Add(new DataColumn("FAS_ID"));
            dtFailure.Columns.Add(new DataColumn("SolvingByWI"));


            dtFailure.Columns.Add(new DataColumn("IsCancel"));
            dtFailure.Columns.Add(new DataColumn("IsDownload"));
            dtFailure.Columns.Add(new DataColumn("FCAT_CH"));//20180803


        }

        //อะไหล่
        private void setDtPart()
        {

            dtPart = new DataTable();
            dtPart.Columns.Add(new DataColumn("รหัสจุดเสีย"));//0
            dtPart.Columns.Add(new DataColumn("จุดเสีย"));//1
            dtPart.Columns.Add(new DataColumn("NO."));//2
            //20140905
            dtPart.Columns.Add(new DataColumn("S/N Old"));
            dtPart.Columns.Add(new DataColumn("Part no."));//3
            dtPart.Columns.Add(new DataColumn("Part"));
            dtPart.Columns.Add(new DataColumn("Type"));//TranType

            dtPart.Columns.Add(new DataColumn("จำนวน"));//Qutity 
            dtPart.Columns.Add(new DataColumn("PricesList"));//Qutity    
            dtPart.Columns.Add(new DataColumn("ราคา/หน่วย"));//Prices
            dtPart.Columns.Add(new DataColumn("รวม"));//SumPrices
            dtPart.Columns.Add(new DataColumn("ภาษี"));//SumVAT
            dtPart.Columns.Add(new DataColumn("รวมสุทธิ"));//SumTotalPrices

            dtPart.Columns.Add(new DataColumn("Discount"));//ส่วนลด %
            dtPart.Columns.Add(new DataColumn("VAT"));// VAT%

            dtPart.Columns.Add(new DataColumn("สถานะ"));
            dtPart.Columns.Add(new DataColumn("Comment"));
            dtPart.Columns.Add(new DataColumn("FAI_ID"));
            dtPart.Columns.Add(new DataColumn("Location"));
            dtPart.Columns.Add(new DataColumn("S/N"));
            dtPart.Columns.Add(new DataColumn("FixAssetNo"));//PTTRM Control Fix Asset No. 


            dtPart.Columns.Add(new DataColumn("IsCancel"));
            dtPart.Columns.Add(new DataColumn("IsDownload"));

            dtPart.Columns.Add(new DataColumn("isPrices"));//part เก็บเงินหน้างาน
            dtPart.Columns.Add(new DataColumn("ออกบิลในนาม"));//ออกบิลในนามสถานี [0=ออกบิลสถานี (เงินสด), 1= ออกบิลบริษัทน้ำมัน]            

            dtPart.Columns.Add(new DataColumn("ERP_SPA_ID"));
            dtPart.Columns.Add(new DataColumn("ERP_PartName"));
            //อะไหล่ลูกค้า
            dtPart.Columns.Add(new DataColumn("อะไหล่ลูกค้า"));

            //ERP_orderline_id
            dtPart.Columns.Add(new DataColumn("ERP_orderline_id"));

            //20141021
            //20141215 dtPart.Columns.Add(new DataColumn("ตำแหน่งที่เปลี่ยน"));
            dtPart.Columns.Add(new DataColumn("Page/Item"));



            dtPart.Columns.Add(new DataColumn("ERP_LoadTime"));
        }


        private void FailureBinding()
        {
            dataGridView_FailPoint.DataSource = null;
            dataGridView_FailPoint.Refresh();

            dataGridView_FailPoint.DataSource = dtFailure;
            dataGridView_FailPoint.Enabled = true;

            setDataGridViewFailPointStyle();

        }

        private void PartBindind()
        {
            //cobEndDate.Items.Clear();
            dataGridView_Part.DataSource = null;
            dataGridView_Part.Refresh();

            dataGridView_Part.DataSource = dtPart;
            setDataGridViewStyle();
        }


        private void toolStripButton_FindStation_Click(object sender, EventArgs e)
        {
            RadioButtonList_JobStatus1.Enabled = true;
            RadioButtonList_JobStatus2.Enabled = true;

            RadioButtonList_JobStatus1.Checked = false;
            RadioButtonList_JobStatus2.Checked = false;

            //string errorDetail = "";//20201007
            try
            {

                newProgressbar();



                searchData(toolStripText_txtFind.Text.Trim(), "1");

                if (strConn == "")
                {
                    toolStripText_txtFind.Focus();
                    toolStripText_txtFind.SelectAll();
                    return;
                }

                closeProgress();

                tabControl1.SelectedIndex = 0;
                //20201002 groupBox3.Enabled = false;
                //===20201002============================================
                if (editMode == "job_edit")
                {
                    try
                    {                      
                        using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext(strConn))
                        {
                            /* 20210323 ก่อนแก้ไขเงื่อนไขเรื่อง เก็บเงินสด
                              DateTime _vpEnddate = DateTime.Parse(_pEnddate);
                              var qJDEnd = (from t in dc.JOB_Detail_Spares
                                            where t.JOB_ID.Trim() == txtJobID.Text.Trim()
                                            && t.Enddate.Value.Date == _vpEnddate.Date
                                            && t.Enddate.Value.Hour == _vpEnddate.Hour
                                            && t.Enddate.Value.Minute == _vpEnddate.Minute
                                            && t.Enddate.Value.Second == _vpEnddate.Second
                                            select t).OrderByDescending(c => c.Enddate).FirstOrDefault();

                              var qJIDate = (from t in dc.JOB_Invoices
                                             where t.JOB_ID == txtJobID.Text.Trim()
                                             select t).OrderByDescending(c => c.Invoice_Date).FirstOrDefault();

                              if (qJDEnd != null && qJIDate != null)
                              {
                                  if (qJDEnd.Enddate.Value.Date <= qJIDate.Invoice_Date.Date)
                                  {
                                      //20202001 disableControl(true);
                                      disableControlInv(true);
                                      //return;

                                  }
                                  else
                                  {

                                      groupBox3.Enabled = false;

                                  }
                              }
                              else
                              {

                                  groupBox3.Enabled = false;

                              } */

                            //20210322 แก้ไขเงื่อนไขเรื่อง เก็บเงินสด
                            DateTime _vpEnddate = DateTime.Parse(_pEnddate);

                            var qIsPrice = (from t in dc.JOB_Detail_Spares
                                            where t.JOB_ID.Trim() == txtJobID.Text.Trim()
                                            && t.Enddate.Value.Date == _vpEnddate.Date
                                            && t.Enddate.Value.Hour == _vpEnddate.Hour
                                            && t.Enddate.Value.Minute == _vpEnddate.Minute
                                            && t.Enddate.Value.Second == _vpEnddate.Second
                                            && t.IsPrices == true //20210322 เก็บเงินสด
                                            select t).OrderByDescending(c => c.Enddate).FirstOrDefault();
                            if (qIsPrice != null)
                            {
                                //disableControlInv(false);
                            }
                            else
                            {
                                var qJDEnd = (from t in dc.JOB_Detail_Spares
                                              where t.JOB_ID.Trim() == txtJobID.Text.Trim()
                                              && t.Enddate.Value.Date == _vpEnddate.Date
                                              && t.Enddate.Value.Hour == _vpEnddate.Hour
                                              && t.Enddate.Value.Minute == _vpEnddate.Minute
                                              && t.Enddate.Value.Second == _vpEnddate.Second
                                              select t).OrderByDescending(c => c.Enddate).FirstOrDefault();

                                var qJIDate = (from t in dc.JOB_Invoices
                                               where t.JOB_ID == txtJobID.Text.Trim()
                                               select t).OrderByDescending(c => c.Invoice_Date).FirstOrDefault();

                                if (qJDEnd != null && qJIDate != null)
                                {
                                    //เงื่อนไขนี้เช็คตอน isprice = 0 ทั้งหมด
                                    if (qJDEnd.Enddate.Value.Date <= qJIDate.Invoice_Date.Date)
                                    {
                                        disableControlInv(true);
                                    }
                                    else
                                    {

                                        groupBox3.Enabled = false;

                                    }
                                }
                                else
                                {

                                    groupBox3.Enabled = false;

                                }
                            }

                        }//dal

                        //=20210921==จ่ายคอมแล้วห้ามยกเลิก====
                        using (System.Data.SqlClient.SqlConnection Conn = new System.Data.SqlClient.SqlConnection(strConn))
                        {
                            if (Conn.State == ConnectionState.Closed)
                                Conn.Open();
                            //============================
                            System.Data.SqlClient.SqlDataReader dr = null;

                            string sqlC = "";
                            sqlC = " Declare @IsCommission INT " +
                                  " Exec @IsCommission = JobIsCommission '" + txtJobID.Text.Trim() + "'" +
                                  " Select @IsCommission ";


                            using (dr = new System.Data.SqlClient.SqlCommand(sqlC, Conn).ExecuteReader())
                            {
                                dr.Read();
                                if (dr.HasRows)
                                {

                                    disableControlInv(true);
                                    //20210930 return;

                                }
                                //else
                                //{

                                //    groupBox3.Enabled = false;

                                //}

                            }
                        }
                        //================================

                    }
                    catch (System.Exception ex)//20201007
                    {

                        MessageBox.Show("เกิดข้อผิดพลาด ข้อมูลการเข้างานและข้อมูลการปิดงาน กรุณาตรวจสอบ...", "เกิดข้อผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        disableControl(true);
                        return;

                    }

                }
                else
                {//20201005
                    groupBox3.Enabled = false;
                }

                //=======================================================
                txtModel.Focus();

                if (lbl_JobType.Text.Trim() == "ตู้จ่าย(D)")
                {
                    txtModel.Text = string.Empty;
                    txtSNOld.Text = string.Empty;
                    txtSNNew.Text = string.Empty;
                    txtModel.Text = string.Empty;
                    txtStartLite.Text = string.Empty;
                    txtEndtLite.Text = string.Empty;
                }

            }
            catch (System.Exception ex)
            {

                closeProgress();
                //20201007 MessageBox.Show("ข้อผิดพลาด " + ex.Message, "เกิดข้อผิดพลาด");

            }

            if (editMode == "job_edit")
            {

                //20150629toolStripButton_Print.Enabled = true;
                //chkEnableSVReport(txtJobID.Text.Trim());

                chkEnableSVReport(txtJobID.Text.Trim());//20191203 

                toolStripButton_Print.Enabled = true;
                toolStripButton_TAGPrimt.Enabled = true;
                btFTRPrint.Enabled = true;
                //คำสั่งยกลิกการประเมิน 20200508 btJobContentMent.Visible = false;


                using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext(strConn))
                {
                    ////==20210713
                    //var j = dc.JOBs
                    //    .Where(t => t.JOB_ID == txtJobID.Text.Trim() && t.JOBS_ID == "99")
                    //    .FirstOrDefault();
                    ////===============

                    //=======20210713===================
                    DateTime _vpEnddateEdit = DateTime.Parse(_pEnddate);
                    var j = ((from t in dc.JOB_Details
                              where t.JOB_ID.Trim() == txtJobID.Text.Trim() && t.JOBS_ID == "99"
                                   && t.StatusDetail == true
                                   && t.EndDate.Value.Date == _vpEnddateEdit.Date
                                   && t.EndDate.Value.Hour == _vpEnddateEdit.Hour
                                   && t.EndDate.Value.Minute == _vpEnddateEdit.Minute
                                   && t.EndDate.Value.Second == _vpEnddateEdit.Second
                              select t).FirstOrDefault());

                    //=====================

                    if (j != null)
                    {
                        RadioButtonList_JobStatus2.Checked = true;

                        /*คำสั่งยกลิกการประเมิน 20200508
                        btJobContentMent.Visible = true;
                        var q = dc.JOB_ContentMents.Where(t => t.JOB_ID == txtJobID.Text.Trim() &&
                            t.ContentStatus == "1"[0]);
                        if (q.Count() > 0)
                        {
                            this.btJobContentMent.ForeColor = System.Drawing.Color.Green;
                            this.btJobContentMent.Image = global::SMSMINI.Properties.Resources.pass;
                            this.btJobContentMent.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
                        }
                        else
                        {
                            this.btJobContentMent.ForeColor = System.Drawing.Color.Red;
                            this.btJobContentMent.Image = global::SMSMINI.Properties.Resources.fail;
                            this.btJobContentMent.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;

                        }
                         */
                    }
                    else//Edit Old JOB
                        RadioButtonList_JobStatus1.Checked = true;
                }
            }
            else
            {

                //20150629 toolStripButton_Print.Enabled = false;
                //chkEnableSVReport(txtJobID.Text.Trim());

                toolStripButton_Print.Enabled = true;

                toolStripButton_TAGPrimt.Enabled = false;
                btFTRPrint.Enabled = false;

            }


        }

        private void PopupConditions_Detail()
        {

            using (DAL.SMSManage.SMSManageDataContext dc =
                new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn))
            {
                var q = (from t in dc.JOBs
                         from s in dc.Stations
                         from v in dc.vw_mini_pop_Conf_Conditions_Details
                         where (v.PopPage.Value == "2"[0]) &&
                         v.GOP_ID == s.GOP_ID &&
                         v.TYP_ID == t.TYP_ID &&
                         v.TYP_ID1 == t.TYP_ID1 &&
                         (t.STA_ID == s.STA_ID) &&
                          (
                             t.EMP_ID3 == UserInfo.Van_ID && (t.IsUpload == "0"[0] || t.IsUpload == null) &&
                             new string[] { "05", "06" }.Contains(t.JOBS_ID) && // Download JOB เข้างาน เท่านั้น
                             t.TYP_ID != "46" && //JOB งาน เบิกค่า ใช้จ่ายพิเศษ/Mileage
                             t.TYP_ID1 != "ML"
                          )

                         select v).ToList();


                if (q.Count() > 0)
                {
                    var f = new Transaction.PopupConditions_Detail();
                    f.lsPopupC = q;
                    f.ShowDialog();
                }
            }
        }

        private void toolStripButton_Save_Click(object sender, EventArgs e)
        {


            if (RadioButtonList_JobStatus1.Checked == false && RadioButtonList_JobStatus2.Checked == false)
            {
                closeProgress();
                MessageBox.Show("กรุณาเลือกบันทึกงานค้าง หรือ ปิดงาน", "ผลการตรวจสอบ");
                return;
            }


            //20170216======================================================== 
            DateTime? srvTime = null;
            //strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn))
            {

                srvTime = dc
                   .ExecuteQuery<DateTime>("SELECT GETDATE()")
                   .First();

            }
            //===========================================================

            if (txtResolve_Detail.Text == string.Empty)
            {
                closeProgress();
                MessageBox.Show("กรุณาป้อนข้อมูล หมายเหตุอื่นๆ", "ผลการตรวจสอบ");
                txtResolve_Detail.Focus();
                return;
            }

            if (cobProblemType.SelectedIndex == -1)
            {
                MessageBox.Show("กรุณาเลือกข้อมูล Problem Type", "ผลการตรวจสอบ");
                cobProblemType.Focus();
                return;
            }

            //--==== 20201204
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn))
            {


                if (RadioButtonList_JobStatus2.Checked == true)
                {
                    var j99 = dc.JOBs.Where(t => t.JOBS_ID == "07"
                        && t.JOB_ID == txtJobID.Text.Trim()
                        && !new string[] { "06", "05", "03", "02", "01" }.Contains(t.JOBS_ID)
                        ).FirstOrDefault();
                    if (j99 != null) //if (j99 == null)
                    {
                        closeProgress();
                        MessageBox.Show("JOB : " + txtJobID.Text.Trim() + " ปิด JOBค้าง ไปแล้ว  " + Environment.NewLine + " ไม่อนุญาตให้เปลี่ยนสถานะการปิดงาน", "ผลการตรวจสอบ");
                        return;
                    }
                }
                if (RadioButtonList_JobStatus1.Checked == true)
                {
                    var j07 = dc.JOBs.Where(t => t.JOBS_ID == "99"
                       && t.JOB_ID == txtJobID.Text.Trim()
                       && !new string[] { "06", "05", "03", "02", "01" }.Contains(t.JOBS_ID)
                       ).FirstOrDefault();
                    if (j07 != null)
                    {
                        closeProgress();
                        //20210714MessageBox.Show("JOB : " + txtJobID.Text.Trim() + " ปิด JOB ไปแล้ว  " + Environment.NewLine + " ไม่สามารถเปลี่ยนสถานะการปิดงานได้", "ผลการตรวจสอบ");
                        //20210714return;
                    }


                }
            }
            //===============

            try
            {

                if (editMode == "job_edit")
                {
                    #region job_edit


                    if (MessageBox.Show("คุณต้องการแก้ไขข้อมูลใบ JOB ใช่หรือไม่???...", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        if (checkPartRemaiCloseJob())
                        {
                            //========================================//
                            //บันทึกการแก้ไข ข้อมูลJOB
                            //========================================//
                            EditJOB();


                            //20150629toolStripButton_Print.Enabled = true;
                            //chkEnableSVReport(txtJobID.Text.Trim());

                            toolStripButton_Print.Enabled = true;

                            btFTRPrint.Enabled = true;



                        }
                    }

                    #endregion
                }
                else
                {
                    #region job_Add

                    if (MessageBox.Show("คุณต้องการบันทึกปิดงาน ใช่หรือไม่???...", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {

                        //========================================//
                        //บันทึกข้อมูล การปิด JOB หรือ การออกงาน
                        //========================================//

                        InsertExitCloseJOB();

                        //20150629toolStripButton_Print.Enabled = true;
                        //chkEnableSVReport(txtJobID.Text.Trim());

                        toolStripButton_Print.Enabled = true;
                        btFTRPrint.Enabled = true;


                    }

                    #endregion

                }


                lsSNCancel = new List<SNCancel>();
                lsSNSelect = new List<SNCancel>();

                lblIsContract.Visible = false;
                txtContract.Visible = false;

                btEditContractNo.Visible = false;
                btUpdatePart.Visible = false;
                btUpdateWorkType.Enabled = false;
                btUpdateWorkType.BackColor = Color.Transparent;

                label_1.Visible = false;
                label_2.Visible = false;
                label_3.Visible = false;

                _IsStationCharge = false;

            }
            catch (System.Exception ex)
            {
                closeProgress();
                MessageBox.Show("ข้อผิดพลาด " + ex.Message, "เกิดข้อผิดพลาด");
            }


        }

        private void CancelPartTravel()
        {
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext(strConn))
            {
                #region Check Station_Travel_Center Cancel Part TRAVEL

                var tvCenter = (from tv in dc.Station_Travel_Centers
                                from st in dc.Stations
                                from j in dc.JOBs
                                where ((j.JOB_ID == tmpjobID) &&
                                (j.TYP_ID == tv.TYP_ID) &&//Job Type                              
                                (j.STA_ID == st.STA_ID)) &&
                                ((tv.GOP_ID == st.GOP_ID) &&
                                (tv.PRO_ID == st.PRO_ID))
                                select tv).Distinct();



                if (tvCenter.Count() > 0)// สถานี อยู่ในศูนย์
                {

                    if (dataGridView_Part.RowCount > 0)// มีอะไหล่ Auto load
                    {

                        string item = dataGridView_Part["Part no.", 0].Value.ToString();
                        var istv = dc.SpareParts_IsTravels.Where(t => t.SPA_ID == item).FirstOrDefault();


                    RemoveItem:
                        if (istv != null)//(item == "TRAVEL")
                            deletePartTRANVEL(istv.SPA_ID);


                        for (int i = 0; i <= dataGridView_Part.RowCount - 1; i++)
                        {
                            item = dataGridView_Part["Part no.", i].Value.ToString();
                            istv = dc.SpareParts_IsTravels.Where(t => t.SPA_ID == item).FirstOrDefault();

                            //item = dataGridView_Part["Part no.", i].Value.ToString();
                            if (istv != null)//(item == "TRAVEL")
                                goto RemoveItem;
                        }
                    }
                }

                #endregion
            }
        }

        private void AddPartAutoByConfig()
        {
            int tmp_NO = 0;
            string tmp_POI_ID = "";
            string tmp_POI_Detail = "";
            string tmp_Failure_Detail = "";

            string SerialNumber = "";
            string fixAssetNo = "";

            string SerialNumber_new = "";
            string fixAssetNo_New = "";


            string tmp_Location = "";

            string tmp_FAI_ID = "";
            string tmp_Model = "";

            //20181017
            if (!string.IsNullOrEmpty(_contractNo) && _contractNo != "null")
            {
                using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext(strConn))
                {
                    //=====20181016 เลขที่สัญญาหมดอายุ หรือ ไม่มีเลขที่สัญญาไม่ auto load


                    var dnow = DateTime.Today.Date;
                    var con0 = dc.Conf_Contracts
                               .Where(t => t.ContractNo.Contains(_contractNo)
                                   && t.StartDate.Date <= dnow && t.EndDate >= dnow
                              )

                       .FirstOrDefault();
                    if (con0 == null)
                    {
                        return;
                    }
                    //=====================================
                    //เพิ่ม chk เพื่อแสดงจุดเสีย
                    var contrac_chkGrop = from t in dc.Conf_Contracts
                                          from tc in dc.Conf_Conditions
                                          from sp in dc.SpareParts
                                          where (t.ContractNo == _contractNo) &&
                                                ((t.ContractNo == tc.ContractNo) &&
                                                (tc.SPA_ID == sp.SPA_ID)) &&
                                                (t.GOP_ID == tmpGOPId) && //ตามสถานี...
                                                (tc.TYP_ID1 == tmpTYP_ID1) &&
                                                (tc.IsAutoPart == true)
                                          select t;
                    if (contrac_chkGrop.Count() > 0)
                    {
                        //===

                        if (dataGridView_FailPoint.RowCount < 1)//Default จุดเสีย
                        {
                            DataRow dr = dtFailure.NewRow();
                            dr["NO."] = "1";
                            dr["รหัสจุดเสีย"] = "000";
                            dr["จุดเสีย"] = "Other";
                            dr["อาการเสีย"] = "ไม่ระบุ";
                            dr["S/N(Old)"] = "None";
                            dr["S/N(New)"] = "None";

                            dr["FCAT_ID"] = 0;
                            dr["FBA_ID"] = 0;
                            dr["FMD_ID"] = 0;

                            dr["Location"] = "None";
                            dr["Model"] = "None";
                            dr["StartLiter"] = 0;
                            dr["EndLiter"] = 0;
                            dr["LiterTest"] = 0;
                            dr["FAI_ID"] = "0000000";

                            dr["IsCancel"] = "0"[0];
                            dr["IsDownload"] = "N"[0];



                            dtFailure.Rows.Add(dr);

                            FailureBinding();
                        }

                        for (int i = 0; i <= dataGridView_FailPoint.Rows.Count - 1; i++)
                        {
                            if (dataGridView_FailPoint.Rows.Count > 0)
                            {
                                tmp_NO = int.Parse(dataGridView_FailPoint["NO.", 0].Value.ToString());
                                tmp_POI_ID = dataGridView_FailPoint["รหัสจุดเสีย", 0].Value.ToString();
                                tmp_POI_Detail = dataGridView_FailPoint["จุดเสีย", 0].Value.ToString();
                                tmp_Failure_Detail = dataGridView_FailPoint["อาการเสีย", 0].Value.ToString();

                                SerialNumber = dataGridView_FailPoint["S/N(Old)", 0].Value.ToString();
                                fixAssetNo = dataGridView_FailPoint["FixAssetNo", 0].Value.ToString();

                                SerialNumber_new = dataGridView_FailPoint["S/N(New)", 0].Value.ToString();
                                fixAssetNo_New = dataGridView_FailPoint["FixAssetNo_New", 0].Value.ToString();


                                tmp_Location = dataGridView_FailPoint["Location", 0].Value.ToString();
                                tmp_FAI_ID = dataGridView_FailPoint["FAI_ID", 0].Value.ToString();
                                tmp_Model = dataGridView_FailPoint["Model", 0].Value.ToString();

                                RadioButtonList_JobStatus1.Enabled = false;
                                RadioButtonList_JobStatus2.Enabled = false;
                            }
                        }
                    }//20181017

                    //20181017 ย้ายขึ้นด้านบน
                    //if (!string.IsNullOrEmpty(_contractNo) && _contractNo != "null")
                    //{
                    //20181017 ย้ายขึ้นด้านบน
                    //using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext())
                    //{


                    #region เพิ่ม Part Auto ตามเงื่อนไขสัญญา

                    //Check เงื่อนไขสัญญาตามสถานี...
                    var contrac_Sat = from t in dc.Conf_Contracts
                                      from tc in dc.Conf_Conditions
                                      from sp in dc.SpareParts
                                      where (t.ContractNo == _contractNo) &&
                                            ((t.ContractNo == tc.ContractNo) &&
                                            (tc.SPA_ID == sp.SPA_ID)) &&
                                            (t.GOP_ID == tmpGOPId) &&
                                            (t.STA_ID == tmpSTAID) && //ตามสถานี...
                                            (tc.TYP_ID1 == tmpTYP_ID1) &&
                                            (tc.IsAutoPart == true)
                                      select new
                                      {
                                          POI_ID = tmp_POI_ID == "" ? "000" : tmp_POI_ID,
                                          PoiFailure = tmp_POI_Detail == "" ? "ไม่ระบุ" : tmp_POI_Detail,
                                          NO = tmp_NO,
                                          SPA_ID = tc.SPA_ID,
                                          PartName = sp.SparePart1,

                                          VAT = _vat,
                                          ControlFixAssetNo = fixAssetNo,
                                          PricesList = 0,
                                          Prices = _Prices,
                                          Discount = 0,
                                          SumPrices = _SumPrices,
                                          SumTotalPrices = _SumTotalPrices,
                                          SumVAT = 0,
                                          Quantity = _Quantity,
                                          StatusSpare = true,
                                          TRA_ID = tc.TRA_ID,//"IN"
                                          Comment = string.Empty,
                                          FAI_ID = tmp_FAI_ID == "" ? "0000000" : tmp_FAI_ID,
                                          Location = tmp_Location == "" ? "None" : tmp_Location,
                                          SerialNumber = SerialNumber_new == "" ? "None" : SerialNumber_new,

                                          IsPrices = false,//checkConf_JOB_IS_PrintPrice(db, txtJobID.Text.Trim(), "IN"),//false,

                                          ERP_StationCharge = (tc.IsPartPriceCharges == null ? '0' : (tc.IsPartPriceCharges == true ? '0' : '1')),
                                          sp.ERP_SPA_ID,
                                          ERP_PartName = sp.SparePart1

                                          //20161117
                                          ,
                                          ReferPage = tc.ReferPage == null ? "" : tc.ReferPage

                                      };


                    if (contrac_Sat.Count() > 0)
                    {
                        #region contrac_Sat

                        foreach (var x1 in contrac_Sat)
                        {
                            if (!checkSPA_ID(x1.SPA_ID, (x1.StatusSpare == true ? "เปลี่ยน" : "ค้าง"),
                                x1.POI_ID, x1.FAI_ID, x1.Location, x1.TRA_ID))
                            {

                                var _pri = getPricesList_AutoLoadPart(dc, x1.SPA_ID);
                                DataRow dr = dtPart.NewRow();
                                dr["รหัสจุดเสีย"] = x1.POI_ID;
                                dr["จุดเสีย"] = x1.PoiFailure == "&nbsp;" ? "" : x1.PoiFailure;

                                dr["NO."] = x1.NO;
                                dr["Part no."] = x1.SPA_ID;
                                dr["Part"] = x1.PartName;
                                dr["Type"] = x1.TRA_ID;

                                dr["จำนวน"] = _Quantity.ToString("#,##0.00"); //x1.Quantity;

                                dr["PricesList"] = _PricesList.ToString("#,##0.00"); //x1.PricesList.ToString("#,##0.00");
                                dr["ราคา/หน่วย"] = _Prices.ToString("#,##0.00"); //x1.Prices.ToString("#,##0.00");
                                dr["รวม"] = _SumPrices.ToString("#,##0.00"); //x1.SumPrices.ToString("#,##0.00");
                                dr["ภาษี"] = _SumVAT.ToString("#0.00");//x1.SumVAT.ToString("#,##0.00");

                                dr["รวมสุทธิ"] = _SumTotalPrices.ToString("#,##0.00"); //= x1.SumTotalPrices.ToString("#,##0.00");

                                dr["VAT"] = x1.VAT.ToString("#,##0.00");
                                dr["FixAssetNo"] = x1.ControlFixAssetNo;

                                dr["Discount"] = x1.Discount.ToString("#,##0.00");

                                dr["สถานะ"] = x1.StatusSpare == true ? "เปลี่ยน" : "ค้าง";
                                dr["Comment"] = x1.Comment == "&nbsp;" ? "" : x1.Comment;
                                dr["FAI_ID"] = x1.FAI_ID;
                                dr["Location"] = x1.Location;
                                dr["S/N"] = x1.SerialNumber ?? "";
                                dr["IsCancel"] = "0";
                                dr["IsDownload"] = "N";

                                dr["isPrices"] = checkConf_JOB_IS_PrintPrice(dc, txtJobID.Text.Trim(), "IN");//false,
                                //x1.ERP_StationCharge == '0' ? "1" : "0";
                                dr["ออกบิลในนาม"] = x1.ERP_StationCharge == '0' ? "สถานี" : "บริษัทน้ำมัน";

                                dr["ERP_SPA_ID"] = null;//x1.ERP_SPA_ID;
                                dr["ERP_PartName"] = null;//x1.ERP_PartName;  

                                //==20141120==
                                dr["อะไหล่ลูกค้า"] = "N";
                                dr["ERP_orderline_id"] = "0";
                                //20150612 dr["ตำแหน่งที่เปลี่ยน"] = "99";
                                //====

                                //20161117
                                dr["Page/Item"] = x1.ReferPage;



                                logCancelSPA_ID.Add(x1.SPA_ID);
                                dtPart.Rows.Add(dr);
                            }
                        }
                        #endregion
                        //20181017 
                        if (dtPart.Rows.Count <= 0 && dataGridView_FailPoint.RowCount == 1)
                        {//   dr["รหัสจุดเสีย"] = "000";
                            if (dataGridView_FailPoint["รหัสจุดเสีย", 0].Value.ToString() == "000")
                            {
                                setDtFailure();
                                FailureBinding();
                            }
                        }
                        //=====
                    }
                    else
                    {
                        //Check เงื่อนไขสัญญาตามกลุ่มบริษัท...
                        #region contrac_Grop

                        var contrac_Grop = from t in dc.Conf_Contracts
                                           from tc in dc.Conf_Conditions
                                           from sp in dc.SpareParts
                                           where (t.ContractNo == _contractNo) &&
                                                 ((t.ContractNo == tc.ContractNo) &&
                                                 (tc.SPA_ID == sp.SPA_ID)) &&
                                                 (t.GOP_ID == tmpGOPId) && //ตามสถานี...
                                                 (tc.TYP_ID1 == tmpTYP_ID1) &&
                                                 (tc.IsAutoPart == true)
                                           select new
                                           {
                                               POI_ID = tmp_POI_ID == "" ? "000" : tmp_POI_ID,
                                               PoiFailure = tmp_POI_Detail == "" ? "ไม่ระบุ" : tmp_POI_Detail,
                                               NO = tmp_NO,
                                               SPA_ID = tc.SPA_ID,
                                               PartName = sp.SparePart1,

                                               VAT = _vat,
                                               ControlFixAssetNo = fixAssetNo,
                                               PricesList = 0,
                                               Prices = _Prices,
                                               Discount = 0,
                                               SumPrices = _SumPrices,
                                               SumTotalPrices = _SumTotalPrices,
                                               SumVAT = 0,
                                               Quantity = _Quantity,
                                               StatusSpare = true,
                                               TRA_ID = tc.TRA_ID,//"IN"
                                               Comment = string.Empty,
                                               FAI_ID = tmp_FAI_ID == "" ? "0000000" : tmp_FAI_ID,
                                               Location = tmp_Location == "" ? "None" : tmp_Location,
                                               SerialNumber = SerialNumber_new == "" ? "None" : SerialNumber_new,

                                               IsPrices = false,//checkConf_JOB_IS_PrintPrice(db, txtJobID.Text.Trim(), "IN"),//false,

                                               ERP_StationCharge = (tc.IsPartPriceCharges == null ? '0' : (tc.IsPartPriceCharges == true ? '0' : '1')),
                                               sp.ERP_SPA_ID,
                                               ERP_PartName = sp.SparePart1

                                               //20161117
                                                 ,
                                               ReferPage = tc.ReferPage == null ? "" : tc.ReferPage

                                           };

                        if (contrac_Grop.Count() > 0)
                        {
                            foreach (var x1 in contrac_Grop)
                            {
                                if (!checkSPA_ID(x1.SPA_ID, (x1.StatusSpare == true ? "เปลี่ยน" : "ค้าง"),
                                x1.POI_ID, x1.FAI_ID, x1.Location, x1.TRA_ID))
                                {

                                    var _pri = getPricesList_AutoLoadPart(dc, x1.SPA_ID);

                                    DataRow dr = dtPart.NewRow();
                                    dr["รหัสจุดเสีย"] = x1.POI_ID;
                                    dr["จุดเสีย"] = x1.PoiFailure == "&nbsp;" ? "" : x1.PoiFailure;

                                    dr["NO."] = x1.NO;
                                    dr["Part no."] = x1.SPA_ID;
                                    dr["Part"] = x1.PartName;
                                    dr["Type"] = x1.TRA_ID;

                                    dr["จำนวน"] = _Quantity.ToString("#,##0.00"); //x1.Quantity;

                                    dr["PricesList"] = _PricesList.ToString("#,##0.00"); //x1.PricesList.ToString("#,##0.00");
                                    dr["ราคา/หน่วย"] = _Prices.ToString("#,##0.00"); //x1.Prices.ToString("#,##0.00");
                                    dr["รวม"] = _SumPrices.ToString("#,##0.00"); //x1.SumPrices.ToString("#,##0.00");
                                    dr["ภาษี"] = _SumVAT.ToString("#0.00");//x1.SumVAT.ToString("#,##0.00");


                                    dr["รวมสุทธิ"] = _SumTotalPrices.ToString("#,##0.00"); //= x1.SumTotalPrices.ToString("#,##0.00");
                                    dr["VAT"] = x1.VAT.ToString("#,##0.00");
                                    dr["FixAssetNo"] = x1.ControlFixAssetNo;

                                    dr["Discount"] = x1.Discount.ToString("#,##0.00");

                                    dr["สถานะ"] = x1.StatusSpare == true ? "เปลี่ยน" : "ค้าง";
                                    dr["Comment"] = x1.Comment == "&nbsp;" ? "" : x1.Comment;
                                    dr["FAI_ID"] = x1.FAI_ID;
                                    dr["Location"] = x1.Location;
                                    dr["S/N"] = x1.SerialNumber ?? "";
                                    dr["IsCancel"] = "0";
                                    dr["IsDownload"] = "N";


                                    dr["isPrices"] = checkConf_JOB_IS_PrintPrice(dc, txtJobID.Text.Trim(), "IN");//false,

                                    dr["ออกบิลในนาม"] = x1.ERP_StationCharge == '0' ? "สถานี" : "บริษัทน้ำมัน";

                                    dr["ERP_SPA_ID"] = null;//x1.ERP_SPA_ID;
                                    dr["ERP_PartName"] = null;//x1.ERP_PartName;  

                                    //==20141120==
                                    dr["อะไหล่ลูกค้า"] = "N";
                                    dr["ERP_orderline_id"] = "0";
                                    //20150612dr["ตำแหน่งที่เปลี่ยน"] = "99";
                                    //====

                                    //20161117
                                    dr["Page/Item"] = x1.ReferPage;


                                    logCancelSPA_ID.Add(x1.SPA_ID);
                                    dtPart.Rows.Add(dr);
                                }
                            }
                            //20181017 
                            if (dtPart.Rows.Count <= 0 && dataGridView_FailPoint.RowCount == 1)
                            {//   dr["รหัสจุดเสีย"] = "000";
                                if (dataGridView_FailPoint["รหัสจุดเสีย", 0].Value.ToString() == "000")
                                {
                                    setDtFailure();
                                    FailureBinding();
                                }
                            }
                            //=====

                        }

                        #endregion

                    }

                    #endregion
                }//dal

            }
            else
            {//ไม่มีเลขที่สัญญา
                return; //=====20181016
                /*//=====20181016 เลขที่สัญญาหมดอายุ หรือ ไม่มีเลขที่สัญญาไม่ auto load
                using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext())
                {
                    
                    ////=====20181016 เลขที่สัญญาหมดอายุ หรือ ไม่มีเลขที่สัญญาไม่ auto load

                    //var dnow = DateTime.Today.Date;
                    //var con0 = dc.Conf_Contracts
                    //           .Where(t => t.ContractNo.Contains(_contractNo)
                    //               && t.StartDate.Date <= dnow && t.EndDate >= dnow
                    //          )

                    //   .FirstOrDefault();
                    //if (con0 == null)
                    //{
                    //    return;
                    //}
                    ////=====================================

                    #region เพิ่ม Part Auto ตามเงื่อนไขสัญญา


                    //Check เงื่อนไขสัญญาตามสถานี...
                    var contrac_Sat = from t in dc.Conf_Contracts
                                      from tc in dc.Conf_Conditions
                                      from sp in dc.SpareParts
                                      where //(t.ContractNo == _contractNo) &&
                                            ((t.ContractNo == tc.ContractNo) &&
                                            (tc.SPA_ID == sp.SPA_ID)) &&
                                            (t.GOP_ID == tmpGOPId) &&
                                            (t.STA_ID == tmpSTAID) && //ตามสถานี...
                                            (tc.TYP_ID1 == tmpTYP_ID1) &&
                                            (tc.IsAutoPart == true)
                                      select new
                                      {
                                          POI_ID = tmp_POI_ID == "" ? "000" : tmp_POI_ID,
                                          PoiFailure = tmp_POI_Detail == "" ? "ไม่ระบุ" : tmp_POI_Detail,
                                          NO = tmp_NO,
                                          SPA_ID = tc.SPA_ID,
                                          PartName = sp.SparePart1,

                                          VAT = _vat,
                                          ControlFixAssetNo = fixAssetNo,
                                          PricesList = 0,
                                          Prices = _Prices,
                                          Discount = 0,
                                          SumPrices = _SumPrices,
                                          SumTotalPrices = _SumTotalPrices,
                                          SumVAT = 0,
                                          Quantity = _Quantity,
                                          StatusSpare = true,
                                          TRA_ID = tc.TRA_ID, //sp.TranType,//"IN"
                                          Comment = string.Empty,
                                          FAI_ID = tmp_FAI_ID == "" ? "0000000" : tmp_FAI_ID,
                                          Location = tmp_Location == "" ? "None" : tmp_Location,
                                          SerialNumber = SerialNumber_new == "" ? "None" : SerialNumber_new,

                                          IsPrices = false,//checkConf_JOB_IS_PrintPrice(db, txtJobID.Text.Trim(), "IN"),//false,

                                          ERP_StationCharge = (tc.IsPartPriceCharges == null ? '0' : (tc.IsPartPriceCharges == true ? '0' : '1')),
                                          sp.ERP_SPA_ID,
                                          ERP_PartName = sp.SparePart1

                                          //20161117
                                           ,ReferPage =tc.ReferPage == null ? "" : tc.ReferPage


                                      };


                    if (contrac_Sat.Count() > 0)
                    {
                        #region contrac_Sat

                        foreach (var x1 in contrac_Sat)
                        {
                            if (!checkSPA_ID(x1.SPA_ID, (x1.StatusSpare == true ? "เปลี่ยน" : "ค้าง"),
                                x1.POI_ID, x1.FAI_ID, x1.Location, x1.TRA_ID))
                            {

                                var _pri = getPricesList_AutoLoadPart(dc, x1.SPA_ID);
                                DataRow dr = dtPart.NewRow();
                                dr["รหัสจุดเสีย"] = x1.POI_ID;
                                dr["จุดเสีย"] = x1.PoiFailure == "&nbsp;" ? "" : x1.PoiFailure;

                                dr["NO."] = x1.NO;
                                dr["Part no."] = x1.SPA_ID;
                                dr["Part"] = x1.PartName;
                                dr["Type"] = x1.TRA_ID;

                                dr["จำนวน"] = _Quantity.ToString("#,##0.00"); //x1.Quantity;

                                dr["PricesList"] = _PricesList.ToString("#,##0.00"); //x1.PricesList.ToString("#,##0.00");
                                dr["ราคา/หน่วย"] = _Prices.ToString("#,##0.00"); //x1.Prices.ToString("#,##0.00");
                                dr["รวม"] = _SumPrices.ToString("#,##0.00"); //x1.SumPrices.ToString("#,##0.00");
                                dr["ภาษี"] = _SumVAT.ToString("#0.00");//x1.SumVAT.ToString("#,##0.00");

                                dr["รวมสุทธิ"] = _SumTotalPrices.ToString("#,##0.00"); //= x1.SumTotalPrices.ToString("#,##0.00");

                                dr["VAT"] = x1.VAT.ToString("#,##0.00");
                                dr["FixAssetNo"] = x1.ControlFixAssetNo;

                                dr["Discount"] = x1.Discount.ToString("#,##0.00");

                                dr["สถานะ"] = x1.StatusSpare == true ? "เปลี่ยน" : "ค้าง";
                                dr["Comment"] = x1.Comment == "&nbsp;" ? "" : x1.Comment;
                                dr["FAI_ID"] = x1.FAI_ID;
                                dr["Location"] = x1.Location;
                                dr["S/N"] = x1.SerialNumber ?? "";
                                dr["IsCancel"] = "0";
                                dr["IsDownload"] = "N";

                                dr["isPrices"] = checkConf_JOB_IS_PrintPrice(dc, txtJobID.Text.Trim(), "IN");//false,
                                //x1.ERP_StationCharge == '0' ? "1" : "0";
                                dr["ออกบิลในนาม"] = x1.ERP_StationCharge == '0' ? "สถานี" : "บริษัทน้ำมัน";

                                dr["ERP_SPA_ID"] = null;//x1.ERP_SPA_ID;
                                dr["ERP_PartName"] = null;//x1.ERP_PartName;  

                                //==20141120==
                                dr["อะไหล่ลูกค้า"] = "N";
                                dr["ERP_orderline_id"] = "0";
                                //20150612dr["ตำแหน่งที่เปลี่ยน"] = "99";
                                //====

                                //20161117
                                dr["Page/Item"] = x1.ReferPage;

                                logCancelSPA_ID.Add(x1.SPA_ID);
                                dtPart.Rows.Add(dr);
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        //Check เงื่อนไขสัญญาตามกลุ่มบริษัท...
                        #region contrac_Grop

                        var contrac_Grop = from t in dc.Conf_Contracts
                                           from tc in dc.Conf_Conditions
                                           from sp in dc.SpareParts
                                           where //(t.ContractNo == _contractNo) &&
                                                 ((t.ContractNo == tc.ContractNo) &&
                                                 (tc.SPA_ID == sp.SPA_ID)) &&
                                                 (t.GOP_ID == tmpGOPId) && //ตามสถานี...
                                                 (tc.TYP_ID1 == tmpTYP_ID1) &&
                                                 (tc.IsAutoPart == true)
                                           select new
                                           {
                                               POI_ID = tmp_POI_ID == "" ? "000" : tmp_POI_ID,
                                               PoiFailure = tmp_POI_Detail == "" ? "ไม่ระบุ" : tmp_POI_Detail,
                                               NO = tmp_NO,
                                               SPA_ID = tc.SPA_ID,
                                               PartName = sp.SparePart1,

                                               VAT = _vat,
                                               ControlFixAssetNo = fixAssetNo,
                                               PricesList = 0,
                                               Prices = _Prices,
                                               Discount = 0,
                                               SumPrices = _SumPrices,
                                               SumTotalPrices = _SumTotalPrices,
                                               SumVAT = 0,
                                               Quantity = _Quantity,
                                               StatusSpare = true,
                                               TRA_ID = tc.TRA_ID,//"IN"
                                               Comment = string.Empty,
                                               FAI_ID = tmp_FAI_ID == "" ? "0000000" : tmp_FAI_ID,
                                               Location = tmp_Location == "" ? "None" : tmp_Location,
                                               SerialNumber = SerialNumber_new == "" ? "None" : SerialNumber_new,

                                               IsPrices = false,//checkConf_JOB_IS_PrintPrice(db, txtJobID.Text.Trim(), "IN"),//false,

                                               ERP_StationCharge = (tc.IsPartPriceCharges == null ? '0' : (tc.IsPartPriceCharges == true ? '0' : '1')),
                                               sp.ERP_SPA_ID,
                                               ERP_PartName = sp.SparePart1

                                               //20161117
                                                 ,ReferPage =tc.ReferPage == null ? "" : tc.ReferPage

                                           };

                        if (contrac_Grop.Count() > 0)
                        {
                            foreach (var x1 in contrac_Grop)
                            {
                                if (!checkSPA_ID(x1.SPA_ID, (x1.StatusSpare == true ? "เปลี่ยน" : "ค้าง"),
                                x1.POI_ID, x1.FAI_ID, x1.Location, x1.TRA_ID))
                                {

                                    var _pri = getPricesList_AutoLoadPart(dc, x1.SPA_ID);

                                    DataRow dr = dtPart.NewRow();
                                    dr["รหัสจุดเสีย"] = x1.POI_ID;
                                    dr["จุดเสีย"] = x1.PoiFailure == "&nbsp;" ? "" : x1.PoiFailure;

                                    dr["NO."] = x1.NO;
                                    dr["Part no."] = x1.SPA_ID;
                                    dr["Part"] = x1.PartName;
                                    dr["Type"] = x1.TRA_ID;

                                    dr["จำนวน"] = _Quantity.ToString("#,##0.00"); //x1.Quantity;

                                    dr["PricesList"] = _PricesList.ToString("#,##0.00"); //x1.PricesList.ToString("#,##0.00");
                                    dr["ราคา/หน่วย"] = _Prices.ToString("#,##0.00"); //x1.Prices.ToString("#,##0.00");
                                    dr["รวม"] = _SumPrices.ToString("#,##0.00"); //x1.SumPrices.ToString("#,##0.00");
                                    dr["ภาษี"] = _SumVAT.ToString("#0.00");//x1.SumVAT.ToString("#,##0.00");


                                    dr["รวมสุทธิ"] = _SumTotalPrices.ToString("#,##0.00"); //= x1.SumTotalPrices.ToString("#,##0.00");
                                    dr["VAT"] = x1.VAT.ToString("#,##0.00");
                                    dr["FixAssetNo"] = x1.ControlFixAssetNo;

                                    dr["Discount"] = x1.Discount.ToString("#,##0.00");

                                    dr["สถานะ"] = x1.StatusSpare == true ? "เปลี่ยน" : "ค้าง";
                                    dr["Comment"] = x1.Comment == "&nbsp;" ? "" : x1.Comment;
                                    dr["FAI_ID"] = x1.FAI_ID;
                                    dr["Location"] = x1.Location;
                                    dr["S/N"] = x1.SerialNumber ?? "";
                                    dr["IsCancel"] = "0";
                                    dr["IsDownload"] = "N";


                                    dr["isPrices"] = checkConf_JOB_IS_PrintPrice(dc, txtJobID.Text.Trim(), "IN");//false,

                                    dr["ออกบิลในนาม"] = x1.ERP_StationCharge == '0' ? "สถานี" : "บริษัทน้ำมัน";

                                    dr["ERP_SPA_ID"] = null;//x1.ERP_SPA_ID;
                                    dr["ERP_PartName"] = null;//x1.ERP_PartName;  

                                    //==20141120==
                                    dr["อะไหล่ลูกค้า"] = "N";
                                    dr["ERP_orderline_id"] = "0";
                                    //20150612dr["ตำแหน่งที่เปลี่ยน"] = "99";
                                    //====

                                    //20161117
                                    dr["Page/Item"] = x1.ReferPage;

                                    logCancelSPA_ID.Add(x1.SPA_ID);
                                    dtPart.Rows.Add(dr);
                                }
                            }

                        }

                        #endregion

                    }

                    #endregion
                }*/
            }//ไม่มีเลขที่สัญญา

            /* //20181017 ย้ายไปใช้เฉพาะมีสัญญา
            if (dtPart.Rows.Count <= 0 && dataGridView_FailPoint.RowCount == 1)
            {//   dr["รหัสจุดเสีย"] = "000";
                if (dataGridView_FailPoint["รหัสจุดเสีย", 0].Value.ToString() == "000")
                {
                    setDtFailure();
                    FailureBinding();                    
                }
            }
             */

        }

        private bool checkClientDateTime()
        {
            DateTime dtOpenJOB = Convert.ToDateTime(_Jobopendate.ToString("yyyy/MM/dd HH:mm"));
            DateTime dtCloseJOB = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm"));

            if (DateTime.Compare(dtOpenJOB, dtCloseJOB) > 0)  //วันที่เปิด JOB มากกว่า วันที่ปิด JOB
                return true;
            return false;

        }


        private bool checkClient_StartEnd_DateTime(string jobID)
        {
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext(strConn))
            {
                var q = dc.JOB_Details.Where(t => t.JOB_ID == jobID)
                    .OrderByDescending(r => r.RecDate)
                    .FirstOrDefault();
                if (q != null)
                {
                    DateTime dtStart = q.StartDate;
                    DateTime dtEnd = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd HH:mm"));

                    if (DateTime.Compare(dtStart, dtEnd) > 0)  //วันที่เปิด JOB มากกว่า วันที่ปิด JOB
                        return true;
                }
            }
            return false;

        }


        private bool checkJobCont()
        {
            string JobCont = "";
            if (radioButton_A.Checked) JobCont = "A";//(A) ดี
            else if (radioButton_B.Checked) JobCont = "B";//(B) พอใช้
            else if (radioButton_C.Checked) JobCont = "C";//(C) ปรับปรุง
            else if (radioButton_D.Checked) JobCont = "D";//(D) ไม่ระบุ    

            if (JobCont == "") return false;
            else return true;
        }

        #region JOB Transaction

        private bool checkPartRemaiCloseJob()
        {
            for (int i = 0; i <= dataGridView_Part.Rows.Count - 1; i++)
            {
                if ((dataGridView_Part["สถานะ", i].Value.ToString().Trim() == "ค้าง" && dataGridView_Part["IsCancel", i].Value.ToString().Trim() == "0") && RadioButtonList_JobStatus2.Checked == true)
                    return false;
            }

            return true;
        }


        private void EditJOB()
        {
            string strPoint = "";
            DateTime tmpStartDate = DateTime.Now;
            DateTime tmpEndDate = DateTime.Now;

            string tmpVAN_ID = "";
            DateTime? srvTimeToSave = null;

            //strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;

            if (editMode == "job_edit")
                strConn = GetConnection.Connection;
            else
                strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;



            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn))
            {

                srvTimeToSave = dc
                   .ExecuteQuery<DateTime>("SELECT GETDATE()")
                   .First();

                //tmpEndDate = srvTimeToSave.Value;
                Server_date = srvTimeToSave.Value;
            }

            //===========



            using (DAL.SMSManage.SMSManageDataContext db = new DAL.SMSManage.SMSManageDataContext(strConn))
            {
                try
                {
                    db.Connection.Open();
                    db.Transaction = db.Connection.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted);


                    //======================================
                    // Close JOB
                    //======================================
                    #region Edit JOB
                    strPoint = "Update Close JOB"; //แก้ไขเฉพาะ Job ที่ค้าง และ ปิด

                    var qj = (from t in db.JOBs where t.JOB_ID == txtJobID.Text.Trim() select t).First();

                    if (qj == null)
                    {
                        closeProgress();
                        MessageBox.Show("ตรวจสอบไม่พบ JOB กรุณาติดต่อผู้ดูแลระบบ", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    if (RadioButtonList_JobStatus2.Checked == true)//CloseJOB
                    {
                        if (qj != null)
                        {
                            char IsSpecialCharge = '0';
                            if (UserInfo.UserId.ToLower().StartsWith("v"))
                                if (MessageBox.Show("JOB [" + txtJobID.Text.Trim() + "] นี้ ต้องเก็บเงินพิเศษ หรือไม่ ?...", "ยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                    IsSpecialCharge = '1';

                            //20200124 qj.CloseDate = tmpEndDate; //จะต้องไม่ไปแก้ไขเวลาการปิดงานเดิม
                            qj.JOBS_ID = "99";

                            /* 20200407 smsmini2 ผู้จัดการแก้ได้ แต่ ไม่ทับ user เจ้าของงาน ยกเลิกเงื่อนไขเก่า
                            if (UserInfo.Van_ID != UserInfo.SUP_VAN)//Sup แก้ไข JOB ไม่ต้อง Update VAN                                   
                                qj.EMP_ID3 = UserInfo.UserId;
                             
                             */

                            //20200407
                            //qj.EMP_ID3 = UserInfo.UserId; //บันทึก user ที่แก้ไข ทับ user เจ้าของงาน

                            qj.PMT_ID = cobProblemType.SelectedValue.ToString();
                            qj.TCO_ID = "04";//ประเภทการปิด JOB {04 = เข้า Site (ผ่าน web)}
                            qj.PRI_ID = comboBox_Priority.SelectedValue.ToString();
                            qj.IsUpload = "9"[0];//20191126"0"[0];
                            qj.IsSpecialCharge = IsSpecialCharge;

                        }

                        db.SubmitChanges();


                    }
                    else//JOB ค้าง
                    {
                        if (qj != null)
                        {
                            if (qj.JOBS_ID != "99") //20210714
                            {
                                qj.JOBS_ID = "07";
                                qj.IsUpload = "9"[0];//20191126"0"[0];

                                /* 20200407 smsmini2 ผู้จัดการแก้ได้ แต่ ไม่ทับ user เจ้าของงาน ยกเลิกเงื่อนไขเก่า
                                if (UserInfo.Van_ID != UserInfo.SUP_VAN)//Sup แก้ไข JOB ไม่ต้อง Update VAN                                   
                                    qj.EMP_ID3 = UserInfo.UserId; */


                                qj.PMT_ID = cobProblemType.SelectedValue.ToString();
                                qj.PRI_ID = comboBox_Priority.SelectedValue.ToString();


                                ////if (isPartRement())//ตรวจสอบอะไหล่ค้าง
                                ////{
                                ////    //ตรวจสอบ ค้าง JOB เพื่อทำเสนอราคา
                                ////    //IsQuotation = null , 0= ไม่เสนอราคา, 1=ต้องทำใบเสนอราคา, 2=ทำใบเสนอราคาแล้ว
                                ////    if (isPartRement())
                                ////    {
                                ////        if (MessageBox.Show("JOB [" + txtJobID.Text.Trim() + "] นี้ ต้องการทำใบเสนอราคาหรือไม่?...", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                ////            qj.IsQuotation = "1"[0];
                                ////        else
                                ////            qj.IsQuotation = "0"[0];
                                ////    }
                                ////}

                                qj.IsQuotation = "0"[0];//0= ไม่เสนอราคา

                                //20160713
                                qj.TCO_ID = "04";//ประเภทการปิด JOB {04 = เข้า Site (ผ่าน web)}
                            }
                        }

                        db.SubmitChanges();
                    }

                    var sql_log = "";
                    sql_log = "Insert into [LogSystem] ([TableName] ,[DataID] ,[Detail] ,[UserID] ,[IPAddress]) " +
                        " values ('JOB'  ,'" + txtJobID.Text.Trim() + "' ,'EditJOB' ,'" + UserInfo.UserId + "' ,CONVERT(char(15), CONNECTIONPROPERTY('client_net_address')))";
                    db.ExecuteCommand(sql_log);


                    #endregion

                    /* คำสั่งยกลิกการประเมิน 20200508
                    //======================================
                    // JOB_ContentMent (บันทึกข้อมูลความพึงพอใจ)
                    //======================================
                    #region Insert JOB_ContentMent
                    strPoint = "Insert JOB_ContentMent";

             

                    string _jobremark = (tmpRemarkJobContentMent == "") ? null : tmpRemarkJobContentMent;
                    string JobCont = "";
                    if (radioButton_A.Checked) JobCont = "A";//(A) ดี
                    else if (radioButton_B.Checked) JobCont = "B";//(B) พอใช้
                    else if (radioButton_C.Checked) JobCont = "C";//(C) ปรับปรุง
                    else if (radioButton_D.Checked) JobCont = "D";//(D) ไม่ระบุ               

                    string _cmdStatus = "0";
                    if ((from t in db.JOB_ContentMents
                         where t.JOB_ID == txtJobID.Text.Trim() && t.ContentStatus == _cmdStatus[0] && t.CMD_ID == "00"

                         select t).Count() > 0)
                    {
                        var delq11 = from t in db.JOB_ContentMents
                                     where t.JOB_ID == txtJobID.Text.Trim() && t.ContentStatus == _cmdStatus[0] && t.CMD_ID == "00"
                                     select t;
                        db.JOB_ContentMents.DeleteAllOnSubmit(delq11);
                        db.SubmitChanges();
                    }

                    if (JobCont != "")
                    {
                        JOB_ContentMent jcm = new JOB_ContentMent()
                        {
                            JOB_ID = txtJobID.Text.Trim(),
                            CMD_ID = "00",//ความคิดเห็นของเจ้าหน้าที่ Flowco
                            ContentStatus = _cmdStatus[0],
                            JobContentMent = JobCont,
                            Remark = _jobremark
                        };
                        db.JOB_ContentMents.InsertOnSubmit(jcm);
                        db.SubmitChanges();
                    }
                    #endregion
                    */

                    //======================================
                    // Insert JOB_Failure_Detail
                    //======================================
                    #region Insert JOB_Failure_Detail
                    strPoint = "Insert JOB_Failure_Detail";

                    //20191119 //20150126
                    //string vProblem_Detail = null;

                    //if ((from t in db.JOB_Failure_Details where t.JOB_ID == txtJobID.Text.Trim() select t).Count() > 0)
                    //{
                    //    var delq11 = from t in db.JOB_Failure_Details
                    //                 where t.JOB_ID == txtJobID.Text.Trim()
                    //                 select t;
                    //    db.JOB_Failure_Details.DeleteAllOnSubmit(delq11);
                    //    db.SubmitChanges();

                    //}

                    ////20191129
                    //for (int i = 0; i <= dataGridView_FailPoint.Rows.Count - 1; i++)
                    //{
                    //    //---20150126
                    //    var qATF = (from t in db.Failure_Actions
                    //                where t.ATF_ID == dataGridView_FailPoint["ATF_ID", i].Value.ToString()
                    //                select t).FirstOrDefault();

                    //    if (qATF != null)
                    //    {

                    //        vProblem_Detail += "รหัสจุดเสีย " + dataGridView_FailPoint["รหัสจุดเสีย", i].Value.ToString()
                    //        + ": " + dataGridView_FailPoint["ATF_ID", i].Value.ToString()
                    //        + " " + qATF.Action_Failure + "  ";

                    //    }
                    //    //---20150126

                    //    string tmp_POI_ID = dataGridView_FailPoint["รหัสจุดเสีย", i].Value.ToString();
                    //    string tmp_Failure_Detail = dataGridView_FailPoint["อาการเสีย", i].Value.ToString();
                    //    string SerialNumber = dataGridView_FailPoint["S/N(Old)", i].Value.ToString();
                    //    string fixAssetNo = dataGridView_FailPoint["FixAssetNo", i].Value.ToString();

                    //    string SerialNumber_new = dataGridView_FailPoint["S/N(New)", i].Value.ToString();
                    //    string fixAssetNo_New = dataGridView_FailPoint["FixAssetNo_New", i].Value.ToString();

                    //    string tmp_FAI_ID = dataGridView_FailPoint["FAI_ID", i].Value.ToString();
                    //    //string tmp_Location = dataGridView_FailPoint["Location", i].Value.ToString();
                    //    //string tmp_Location = dataGridView_FailPoint["Location", i].Value.ToString() == null ? "0" : dataGridView_FailPoint["Location", i].Value.ToString();//dataGridView_FailPoint["Location", i].Value.ToString();


                    //    /* //20150709
                    //    string tmp_Location = "";
                    //    if (string.IsNullOrEmpty(dataGridView_FailPoint["Location", i].Value.ToString()))
                    //    {
                    //        tmp_Location = "0";
                    //    }
                    //    else
                    //    {
                    //        tmp_Location = dataGridView_FailPoint["Location", i].Value.ToString();
                    //    }
                    //    */


                    //    //=============20150709==================
                    //    int _LOC_ID = 0;
                    //    string _vfLocation = dataGridView_FailPoint["Location", i].Value.ToString().Trim();

                    //    int _cfLocation = 0;

                    //    if (dataGridView_FailPoint["Location", i].Value != null)
                    //        _cfLocation = dataGridView_FailPoint["Location", i].Value.ToString().Trim().Length;

                    //    if ((_cfLocation > 2 || _vfLocation == null))
                    //    {
                    //        //_vLocation
                    //        _LOC_ID = 0;
                    //    }
                    //    else
                    //    {
                    //        var qfLocate = (from t in db.Locations
                    //                        where Convert.ToString(t.LOC_ID) == _vfLocation //dataGridView_Part["SPA_ID", i].Value.ToString()
                    //                        select t).FirstOrDefault();
                    //        if (qfLocate == null)
                    //        {
                    //            _LOC_ID = 0;
                    //        }
                    //        else
                    //        {
                    //            _LOC_ID = qfLocate.LOC_ID;
                    //        }
                    //    }
                    //    //===============20150709================


                    //    string tmp_Model = dataGridView_FailPoint["Model", i].Value.ToString();

                    //    decimal tmp_StartLiter = Convert.ToDecimal(dataGridView_FailPoint["StartLiter", i].Value.ToString() == "&nbsp;" ? "0" : dataGridView_FailPoint["StartLiter", i].Value.ToString());
                    //    decimal tmp_EndLiter = Convert.ToDecimal(dataGridView_FailPoint["EndLiter", i].Value.ToString() == "&nbsp;" ? "0" : dataGridView_FailPoint["EndLiter", i].Value.ToString());
                    //    decimal tmp_LiterTest = Convert.ToDecimal(dataGridView_FailPoint["LiterTest", i].Value.ToString() == "&nbsp;" ? "0" : dataGridView_FailPoint["LiterTest", i].Value.ToString());

                    //    int _fcatID = 0;
                    //    int _fbaID = 0;
                    //    int _fmdID = 0;

                    //    //20141204
                    //   //20150709 ใช้ process แปลง int _LOC_ID = 0;

                    //    if (dataGridView_FailPoint["FCAT_ID", i].Value != DBNull.Value)
                    //        _fcatID = Convert.ToInt32(dataGridView_FailPoint["FCAT_ID", i].Value);

                    //    if (dataGridView_FailPoint["FBA_ID", i].Value != DBNull.Value)
                    //        _fbaID = Convert.ToInt32(dataGridView_FailPoint["FBA_ID", i].Value);

                    //    if (dataGridView_FailPoint["FMD_ID", i].Value != DBNull.Value)
                    //        _fmdID = Convert.ToInt32(dataGridView_FailPoint["FMD_ID", i].Value);

                    //    //20141204
                    //    //20150709 ใช้ process แปลง _LOC_ID = Convert.ToInt32(tmp_Location);


                    //    //===20141022
                    //    int _fPRT_ID = 0;
                    //    //20141127ยกเลิก int _fEQP_ID = 0;
                    //    int _fFAS_ID = 0;

                    //    if (dataGridView_FailPoint["PRT_ID", i].Value != DBNull.Value)
                    //        _fPRT_ID = Convert.ToInt32(dataGridView_FailPoint["PRT_ID", i].Value);

                    //    //20141127ยกเลิกif (dataGridView_FailPoint["EQP_ID", i].Value != DBNull.Value)
                    //         //20141127ยกเลิก _fEQP_ID = Convert.ToInt32(dataGridView_FailPoint["EQP_ID", i].Value);

                    //    string _fATF_ID = dataGridView_FailPoint["ATF_ID", i].Value.ToString();

                    //    if (dataGridView_FailPoint["FAS_ID", i].Value != DBNull.Value)
                    //        _fFAS_ID = Convert.ToInt32(dataGridView_FailPoint["FAS_ID", i].Value);

                    //    string SolvingByWI = dataGridView_FailPoint["SolvingByWI", i].Value.ToString();

                    //    //20150126

                    //    //===20180806
                    //    int _fcatCH = 0;
                    //    if (dataGridView_FailPoint["FCAT_CH", i].Value != DBNull.Value)
                    //        _fcatCH = Convert.ToInt32(dataGridView_FailPoint["FCAT_CH", i].Value);
                    //    //============

                    //    JOB_Failure_Detail jf = new JOB_Failure_Detail()
                    //    {
                    //        NO = i + 1,
                    //        JOB_ID = txtJobID.Text.Trim(),
                    //        POI_ID = tmp_POI_ID,
                    //        FAI_ID = tmp_FAI_ID == "" ? "0000000" : tmp_FAI_ID,
                    //        Failure_Detail = tmp_Failure_Detail == "" ? null : tmp_Failure_Detail.Trim(),
                    //        trandate = DateTime.Now,
                    //        SerialNumber = SerialNumber,
                    //        FixAssetNo = fixAssetNo,
                    //        SerialNumber_New = SerialNumber_new,
                    //        FixAssetNo_New = fixAssetNo_New,
                    //        FCAT_ID = _fcatID,
                    //        FBA_ID = _fbaID,
                    //        FMD_ID = _fmdID,
                    //        StartLiter = tmp_StartLiter,
                    //        EndLiter = tmp_EndLiter,
                    //        LiterTest = tmp_LiterTest,
                    //        LOC_ID = _LOC_ID,//20141204
                    //        Location = null,//20141204 tmp_Location.Trim() == "" ? null : tmp_Location.Trim(),
                    //        Model = tmp_Model.Trim() == "" ? null : tmp_Model.Trim(),
                    //        IsCancel = dataGridView_FailPoint["IsCancel", i].Value.ToString()[0],
                    //        IsDownload = dataGridView_FailPoint["IsDownload", i].Value.ToString()[0],
                    //        //20141022
                    //        PRT_ID = _fPRT_ID,
                    //         //20141127ยกเลิก EQP_ID = _fEQP_ID,
                    //        ATF_ID = _fATF_ID,
                    //        FAS_ID = _fFAS_ID,
                    //        SolvingByWI = SolvingByWI

                    //        ,FCAT_CH = _fcatCH //20180806 
                    //    };

                    //    db.JOB_Failure_Details.InsertOnSubmit(jf);
                    //    db.SubmitChanges();
                    //}// for (

                    //====20191129 
                    #region Insert JOB_Failure_Detail online
                    strPoint = "Insert JOB_Failure_Detail";
                    string vProblem_Detail = null;

                    int tmp_NO = 0;
                    for (int i = 0; i <= dataGridView_FailPoint.Rows.Count - 1; i++)
                    {
                        //----20150126
                        var qATF = (from t in db.Failure_Actions
                                    where t.ATF_ID == dataGridView_FailPoint["ATF_ID", i].Value.ToString()
                                    select t).FirstOrDefault();

                        if (qATF != null)
                        {

                            vProblem_Detail += "รหัสจุดเสีย " + dataGridView_FailPoint["รหัสจุดเสีย", i].Value.ToString()
                            + ": " + dataGridView_FailPoint["ATF_ID", i].Value.ToString()
                            + " " + qATF.Action_Failure + "  ";

                        }
                        //----20150126
                        tmp_NO = int.Parse(dataGridView_FailPoint["NO.", i].Value.ToString());
                        string tmp_POI_ID = dataGridView_FailPoint["รหัสจุดเสีย", i].Value.ToString();
                        string tmp_Failure_Detail = dataGridView_FailPoint["อาการเสีย", i].Value.ToString();
                        //string SerialNumber = dataGridView_FailPoint["S/N(Old)", i].Value.ToString();
                        //string SerialNumber_new = dataGridView_FailPoint["S/N(New)", i].Value.ToString();
                        string SerialNumber = dataGridView_FailPoint["S/N(Old)", i].Value.ToString();
                        string fixAssetNo = dataGridView_FailPoint["FixAssetNo", i].Value.ToString();

                        string SerialNumber_new = dataGridView_FailPoint["S/N(New)", i].Value.ToString();
                        string fixAssetNo_New = dataGridView_FailPoint["FixAssetNo_New", i].Value.ToString();


                        //20150709 ใช้ process แปลง string tmp_Location = dataGridView_FailPoint["Location", i].Value.ToString();


                        //=============20150709==================
                        int _LOC_ID = 0;
                        string _vfLocation = dataGridView_FailPoint["Location", i].Value.ToString().Trim();

                        int _cfLocation = 0;

                        if (dataGridView_FailPoint["Location", i].Value != null)
                            _cfLocation = dataGridView_FailPoint["Location", i].Value.ToString().Trim().Length;

                        if ((_cfLocation > 2 || _vfLocation == null))
                        {
                            //_vLocation
                            _LOC_ID = 0;
                        }
                        else
                        {
                            var qfLocate = (from t in db.Locations
                                            where Convert.ToString(t.LOC_ID) == _vfLocation //dataGridView_Part["SPA_ID", i].Value.ToString()
                                            select t).FirstOrDefault();
                            if (qfLocate == null)
                            {
                                _LOC_ID = 0;
                            }
                            else
                            {
                                _LOC_ID = qfLocate.LOC_ID;
                            }
                        }
                        //===============20150709================
                        string tmp_FAI_ID = dataGridView_FailPoint["FAI_ID", i].Value.ToString();
                        string tmp_Model = dataGridView_FailPoint["Model", i].Value.ToString();

                        decimal tmp_StartLiter = Convert.ToDecimal(dataGridView_FailPoint["StartLiter", i].Value.ToString() == "&nbsp;" ? "0" : dataGridView_FailPoint["StartLiter", i].Value.ToString());
                        decimal tmp_EndLiter = Convert.ToDecimal(dataGridView_FailPoint["EndLiter", i].Value.ToString() == "&nbsp;" ? "0" : dataGridView_FailPoint["EndLiter", i].Value.ToString());
                        decimal tmp_LiterTest = Convert.ToDecimal(dataGridView_FailPoint["LiterTest", i].Value.ToString() == "&nbsp;" ? "0" : dataGridView_FailPoint["LiterTest", i].Value.ToString());

                        int _fcatID = 0;
                        int _fbaID = 0;
                        int _fmdID = 0;

                        //20141204
                        //20150709 ใช้ process แปลง int _LOC_ID =0;

                        if (dataGridView_FailPoint["FCAT_ID", i].Value != DBNull.Value)
                            _fcatID = Convert.ToInt32(dataGridView_FailPoint["FCAT_ID", i].Value);

                        if (dataGridView_FailPoint["FBA_ID", i].Value != DBNull.Value)
                            _fbaID = Convert.ToInt32(dataGridView_FailPoint["FBA_ID", i].Value);

                        if (dataGridView_FailPoint["FMD_ID", i].Value != DBNull.Value)
                            _fmdID = Convert.ToInt32(dataGridView_FailPoint["FMD_ID", i].Value);

                        //20141204
                        //20150709 ใช้ process แปลง _LOC_ID = Convert.ToInt32(tmp_Location);

                        //===20141021
                        int _fPRT_ID = 0;
                        //20141127ยกเลิก int _fEQP_ID =0;
                        int _fFAS_ID = 0;

                        if (dataGridView_FailPoint["PRT_ID", i].Value != DBNull.Value)
                            _fPRT_ID = Convert.ToInt32(dataGridView_FailPoint["PRT_ID", i].Value);

                        //20141127ยกเลิก if (dataGridView_FailPoint["EQP_ID", i].Value != DBNull.Value)
                        //20141127ยกเลิก    _fEQP_ID = Convert.ToInt32(dataGridView_FailPoint["EQP_ID", i].Value);

                        string _fATF_ID = dataGridView_FailPoint["ATF_ID", i].Value.ToString();

                        if (dataGridView_FailPoint["FAS_ID", i].Value != DBNull.Value)
                            _fFAS_ID = Convert.ToInt32(dataGridView_FailPoint["FAS_ID", i].Value);

                        string SolvingByWI = dataGridView_FailPoint["SolvingByWI", i].Value.ToString();


                        //===20180806
                        int _fcatCH = 0;
                        if (dataGridView_FailPoint["FCAT_CH", i].Value != DBNull.Value)
                            _fcatCH = Convert.ToInt32(dataGridView_FailPoint["FCAT_CH", i].Value);
                        //============


                        //==ตรวจสอบ insert update  
                        string vtmp_FAI_ID = tmp_FAI_ID == "" ? "0000000" : tmp_FAI_ID;
                        var qF = (from j in db.JOB_Failure_Details
                                  where //20191212(j.NO == tmp_NO) &&
                                 (j.JOB_ID == txtJobID.Text.Trim()) &&
                                 (j.POI_ID == tmp_POI_ID) &&
                                 (j.FAI_ID == vtmp_FAI_ID) &&
                                 (j.SerialNumber == SerialNumber) &&
                                 (j.LOC_ID == _LOC_ID) //&&
                                  //(j.IsCancel == IsCancelF) 
                                  select j).FirstOrDefault();


                        if (qF != null)
                        {
                            //qF.NO = Convert.ToInt32(dataGridView_JOB_Failure_Detail["NO", x].Value);
                            //qF.JOB_ID = dataGridView_JOB_Failure_Detail["JOB_ID", x].Value.ToString().Trim();
                            //qF.POI_ID = dataGridView_JOB_Failure_Detail["POI_ID", x].Value.ToString().Trim();
                            qF.FAI_ID = tmp_FAI_ID == "" ? "0000000" : tmp_FAI_ID;
                            qF.Failure_Detail = tmp_Failure_Detail == "" ? null : tmp_Failure_Detail.Trim();

                            qF.SerialNumber = SerialNumber;
                            qF.SerialNumber_New = SerialNumber_new;

                            qF.FixAssetNo = fixAssetNo;
                            qF.FixAssetNo_New = fixAssetNo_New;

                            qF.FCAT_ID = _fcatID;

                            qF.FBA_ID = _fbaID;

                            qF.FMD_ID = _fmdID;

                            //20200124 qF.trandate = tmpEndDate;
                            qF.StartLiter = tmp_StartLiter;
                            qF.EndLiter = tmp_EndLiter;
                            qF.LiterTest = tmp_LiterTest;
                            qF.Location = null;

                            //20141222
                            qF.LOC_ID = _LOC_ID;

                            qF.Model = tmp_Model.Trim() == "" ? null : tmp_Model.Trim();
                            qF.IsCancel = dataGridView_FailPoint["IsCancel", i].Value.ToString()[0];

                            //20141027
                            qF.PRT_ID = _fPRT_ID;
                            //20141201 EQP_ID = Convert.ToInt32(dataGridView_JOB_Failure_Detail["EQP_ID", x].Value),
                            qF.ATF_ID = _fATF_ID;
                            qF.FAS_ID = _fFAS_ID;
                            qF.SolvingByWI = SolvingByWI;

                            //20180806 
                            qF.FCAT_CH = _fcatCH;

                            qF.IsDownload = dataGridView_FailPoint["IsDownload", i].Value.ToString()[0];

                            db.SubmitChanges();

                        }
                        else
                        {
                            //เพิ่มใหม่
                            JOB_Failure_Detail jf = new JOB_Failure_Detail()
                            {
                                NO = tmp_NO,
                                JOB_ID = txtJobID.Text.Trim(),
                                POI_ID = tmp_POI_ID,
                                FAI_ID = tmp_FAI_ID == "" ? "0000000" : tmp_FAI_ID,
                                Failure_Detail = tmp_Failure_Detail == "" ? null : tmp_Failure_Detail.Trim(),
                                //20200124 trandate = tmpEndDate,//getServerTime(),

                                //20100127 เพิ่มจุดเสียใหม่ในกรณี edit ให้ใช้เวลาของ job ที่เลือก
                                trandate = tmpEndDateTimeEditJOB,
                                //=====
                                //SerialNumber = SerialNumber,
                                //SerialNumber_New = SerialNumber_new,
                                SerialNumber = SerialNumber,
                                FixAssetNo = fixAssetNo,
                                SerialNumber_New = SerialNumber_new,
                                FixAssetNo_New = fixAssetNo_New,

                                FCAT_ID = _fcatID,
                                FBA_ID = _fbaID,
                                FMD_ID = _fmdID,

                                StartLiter = tmp_StartLiter,
                                EndLiter = tmp_EndLiter,
                                LiterTest = tmp_LiterTest,
                                LOC_ID = _LOC_ID,//20141204
                                Location = null,//20141204 tmp_Location.Trim() == "" ? null : tmp_Location.Trim(),
                                Model = tmp_Model.Trim() == "" ? null : tmp_Model.Trim(),
                                IsCancel = dataGridView_FailPoint["IsCancel", i].Value.ToString()[0],
                                IsDownload = dataGridView_FailPoint["IsDownload", i].Value.ToString()[0],

                                //20141021
                                PRT_ID = _fPRT_ID,
                                //20141127ยกเลิก EQP_ID = _fEQP_ID,
                                ATF_ID = _fATF_ID,
                                FAS_ID = _fFAS_ID,
                                SolvingByWI = SolvingByWI

                                ,
                                FCAT_CH = _fcatCH //20180806 

                            };

                            db.JOB_Failure_Details.InsertOnSubmit(jf);
                            db.SubmitChanges();
                        }
                        //======================

                    }//for
                    #endregion




                    #endregion

                    //======================================
                    // Update JOB_Detail
                    //======================================
                    #region Get StartDate
                    strPoint = "get JOB_Detail StartDate";
                    DateTime _EndFndStart = DateTime.Parse(_pEnddate); //20200618 

                    var qjd = (from t in db.JOB_Details
                               where t.JOB_ID == txtJobID.Text.Trim() && t.StatusDetail == true
                               //===20200618
                               && t.EndDate.Value.Date == _EndFndStart.Date
                               && t.EndDate.Value.Hour == _EndFndStart.Hour
                               && t.EndDate.Value.Minute == _EndFndStart.Minute
                               && t.EndDate.Value.Second == _EndFndStart.Second
                               //===
                               //20200618 orderby t.RecDate descending
                               select t).FirstOrDefault();

                    if (qjd != null)
                    {
                        //20200124 tmpStartDate = qjd.StartDate;
                        //tmpEndDate = qjd.EndDate ?? DateTime.Now;
                        tmpVAN_ID = qjd.EMP_ID ?? UserInfo.UserId;
                        tmpStartDate = qjd.StartDate;//20200618

                    }

                    bool TypeJobRemain = false;

                    #endregion


                    //======================================
                    // Insert JOB_Detail_Spare
                    //======================================
                    //====20191129===
                    //#region Insert JOB_Detail_Spare Offline
                    //strPoint = "Insert JOB_Detail_Spare";

                    ////string 
                    ////Clear Old part
                    //if ((from t in db.JOB_Detail_Spares where t.JOB_ID == txtJobID.Text.Trim() select t).Count() > 0)
                    //{
                    //    var delq2 = from t in db.JOB_Detail_Spares where t.JOB_ID == txtJobID.Text.Trim() select t;
                    //    db.JOB_Detail_Spares.DeleteAllOnSubmit(delq2);
                    //    db.SubmitChanges();
                    //}

                    //DateTime _StartDate = tmpStartDate;
                    //for (int i = 0; i <= dataGridView_Part.Rows.Count - 1; i++)
                    //{


                    //    string _SPA_ID = dataGridView_Part["Part no.", i].Value.ToString();

                    //    //Write Part cancel
                    //    #region Write Part cancel

                    //    //if (dataGridView_Part["IsDownload", i].Value.ToString() == "Y" && dataGridView_Part["IsCancel", i].Value.ToString() == "1")
                    //    //{
                    //    //    foreach (var partID in logCancelSPA_ID)
                    //    //    {
                    //    //        if (partID == dataGridView_Part["Part no.", i].Value.ToString())
                    //    //            partLogCancel(
                    //    //                dataGridView_Part["Part no.", i].Value.ToString(),
                    //    //                dataGridView_Part["Location", i].Value.ToString(),
                    //    //                dataGridView_Part["รหัสจุดเสีย", i].Value.ToString(),
                    //    //                dataGridView_Part["FAI_ID", i].Value.ToString(),
                    //    //                dataGridView_Part["Type", i].Value.ToString()
                    //    //                );
                    //    //    }
                    //    //}
                    //    #endregion

                    //    _StartDate = checkStartDate(_StartDate, _SPA_ID);

                    //    bool _StatusSpare = (dataGridView_Part["สถานะ", i].Value.ToString() == "ค้าง") ? false : true;//7dr["สถานะ"]
                    //    //อะไหล่ลูกค้า
                    //    string _StatusSpareCus = (dataGridView_Part["อะไหล่ลูกค้า", i].Value.ToString() == "Y") ? "9" : "0";


                    //    int xNO = int.Parse(dataGridView_Part["NO.", i].Value.ToString());

                    //    char stCharge = dataGridView_Part["ออกบิลในนาม", i].Value.ToString() == "สถานี" ? '0' : '1';
                    //    //string vtxtJobID = txtJobID.Text.Trim();

                    //    if ((from j in db.JOB_Detail_Spares
                    //         where j.JOB_ID == txtJobID.Text.Trim()&&
                    //         j.StartDate == _StartDate &&
                    //         j.SPA_ID == _SPA_ID &&
                    //          j.NO == xNO
                    //         select j).Count() > 0)
                    //    {

                    //        var sp = (from j in db.JOB_Detail_Spares
                    //                  where j.JOB_ID == txtJobID.Text.Trim() &&
                    //                      j.StartDate == _StartDate &&
                    //                      j.SPA_ID == _SPA_ID &&
                    //                      j.NO == xNO
                    //                  select j).FirstOrDefault();

                    //        sp.POI_ID = dataGridView_Part["รหัสจุดเสีย", i].Value.ToString();

                    //        sp.Quantity = decimal.Parse(dataGridView_Part["จำนวน", i].Value.ToString());
                    //        sp.PricesList = decimal.Parse(dataGridView_Part["PricesList", i].Value.ToString());
                    //        sp.Prices = decimal.Parse(dataGridView_Part["ราคา/หน่วย", i].Value.ToString());
                    //        sp.SumPrices = decimal.Parse(dataGridView_Part["รวม", i].Value.ToString());
                    //        sp.SumVAT = decimal.Parse(dataGridView_Part["ภาษี", i].Value.ToString());
                    //        sp.SumTotalPrices = decimal.Parse(dataGridView_Part["รวมสุทธิ", i].Value.ToString());


                    //        sp.Discount = decimal.Parse(dataGridView_Part["Discount", i].Value.ToString());
                    //        sp.VAT = decimal.Parse(dataGridView_Part["VAT", i].Value.ToString());
                    //        sp.ControlFixAssetNo = dataGridView_Part["FixAssetNo", i].Value.ToString();

                    //        sp.StatusSpare = _StatusSpare;
                    //        sp.TRA_ID = dataGridView_Part["Type", i].Value.ToString();
                    //        sp.Comment = dataGridView_Part["Comment", i].Value == null ? null + (IsQT == false ? "#QT=None" : "") : dataGridView_Part["Comment", i].Value.ToString() + (IsQT == false ? "#QT=None" : "");//dr["Comment"]                         
                    //        sp.Enddate = tmpEndDate;
                    //        sp.EMP_ID = tmpVAN_ID;//UserInfo.UserId;
                    //        sp.FAI_ID = dataGridView_Part["FAI_ID", i].Value == null ? null : dataGridView_Part["FAI_ID", i].Value.ToString();

                    //        //20150709 ใช้ process แปลง sp.Location = dataGridView_Part["Location", i].Value == null ? null : dataGridView_Part["Location", i].Value.ToString();
                    //        //===========20150709=============================
                    //        string _vLocation = dataGridView_Part["Location", i].Value.ToString().Trim();

                    //        int _cLocation = 0;

                    //        if (dataGridView_Part["Location", i].Value != null)
                    //            _cLocation = dataGridView_Part["Location",i].Value.ToString().Trim().Length;

                    //        if ((_cLocation > 2 || _vLocation == null))
                    //        {
                    //            //_vLocation
                    //            sp.Location = "0";
                    //        }
                    //        else
                    //        {
                    //            var qLocate = (from t in db.Locations
                    //                           where Convert.ToString(t.LOC_ID) == _vLocation 
                    //                           select t).FirstOrDefault();
                    //            if (qLocate == null)
                    //            {
                    //                sp.Location = "0";
                    //            }
                    //            else
                    //            {
                    //                sp.Location = Convert.ToString(qLocate.LOC_ID);
                    //            }
                    //        }
                    //        //===========20150709=============================


                    //        sp.SerialNumber = dataGridView_Part["S/N", i].Value == null ? null : dataGridView_Part["S/N", i].Value.ToString();

                    //        sp.IsCancel = dataGridView_Part["IsCancel", i].Value.ToString()[0];
                    //        sp.IsDownload = dataGridView_Part["IsDownload", i].Value.ToString()[0];
                    //        sp.IsPrices = dataGridView_Part["IsPrices", i].Value.ToString() == "0" ? false : true;
                    //        sp.ERP_StationCharge = stCharge;

                    //        if (dataGridView_Part["ERP_SPA_ID", i].Value != DBNull.Value && !string.IsNullOrEmpty(dataGridView_Part["ERP_SPA_ID", i].Value.ToString()))
                    //            sp.ERP_SPA_ID = dataGridView_Part["ERP_SPA_ID", i].Value.ToString();
                    //        else
                    //            sp.ERP_SPA_ID = null;

                    //        if (dataGridView_Part["ERP_PartName", i].Value != DBNull.Value && !string.IsNullOrEmpty(dataGridView_Part["ERP_PartName", i].Value.ToString()))
                    //            sp.ERP_PartName = dataGridView_Part["ERP_PartName", i].Value.ToString();
                    //        else
                    //            sp.ERP_PartName = null;


                    //        //อะไหล่ลูกค้า
                    //        sp.IsCustomer = Convert.ToChar(_StatusSpareCus);

                    //        if (dataGridView_Part["ERP_orderline_id", i].Value != DBNull.Value && !string.IsNullOrEmpty(dataGridView_Part["ERP_orderline_id", i].Value.ToString()))
                    //            sp.ERP_orderline_id = int.Parse (dataGridView_Part["ERP_orderline_id", i].Value.ToString());
                    //        else
                    //            sp.ERP_orderline_id = null;

                    //        //20140905
                    //        sp.SerialNumber_Old = dataGridView_Part["S/N Old", i].Value.ToString();

                    //        //20141022
                    //        //20141127ยกเลิก  sp.EQP_ID = int.Parse(dataGridView_Part["ตำแหน่งที่เปลี่ยน", i].Value.ToString());

                    //        //20161116
                    //        sp.ReferPage = dataGridView_Part["Page/Item", i].Value.ToString();




                    //        db.SubmitChanges();

                    //    }
                    //    else
                    //    {
                    //        JOB_Detail_Spare jPart = new JOB_Detail_Spare();


                    //        jPart.NO = xNO;
                    //        jPart.JOB_ID = txtJobID.Text.Trim();
                    //        jPart.POI_ID = dataGridView_Part["รหัสจุดเสีย", i].Value.ToString();//dr["รหัสจุดเสีย"]
                    //        jPart.SPA_ID = dataGridView_Part["Part no.", i].Value.ToString();//dr["Part no."]
                    //        jPart.StartDate = _StartDate;
                    //        jPart.Quantity = decimal.Parse(dataGridView_Part["จำนวน", i].Value.ToString());//dr["จำนวน"]
                    //        jPart.PricesList = decimal.Parse(dataGridView_Part["PricesList", i].Value.ToString());
                    //        jPart.Prices = decimal.Parse(dataGridView_Part["ราคา/หน่วย", i].Value.ToString());
                    //        jPart.SumPrices = decimal.Parse(dataGridView_Part["รวม", i].Value.ToString());
                    //        jPart.SumVAT = decimal.Parse(dataGridView_Part["ภาษี", i].Value.ToString());
                    //        jPart.SumTotalPrices = decimal.Parse(dataGridView_Part["รวมสุทธิ", i].Value.ToString());

                    //        jPart.Discount = decimal.Parse(dataGridView_Part["Discount", i].Value.ToString());
                    //        jPart.VAT = decimal.Parse(dataGridView_Part["VAT", i].Value.ToString());
                    //        jPart.ControlFixAssetNo = dataGridView_Part["FixAssetNo", i].Value.ToString();

                    //        jPart.StatusSpare = _StatusSpare;//dr["สถานะ"]
                    //        jPart.TRA_ID = dataGridView_Part["Type", i].Value.ToString();//dr["Type"]
                    //        jPart.Comment = dataGridView_Part["Comment", i].Value == null ? null + (IsQT == false ? "#QT=None" : "") : dataGridView_Part["Comment", i].Value.ToString() + (IsQT == false ? "#QT=None" : "");//dr["Comment"]                         
                    //        jPart.Enddate = tmpEndDate;
                    //        jPart.EMP_ID = tmpVAN_ID;// UserInfo.UserId;

                    //        jPart.FAI_ID = dataGridView_Part["FAI_ID", i].Value == null ? null : dataGridView_Part["FAI_ID", i].Value.ToString();

                    //        //20150709 ใช้ process แปลง jPart.Location = dataGridView_Part["Location", i].Value == null ? null : dataGridView_Part["Location", i].Value.ToString();
                    //        //===========20150709=============================
                    //        string _vLocation = dataGridView_Part["Location", i].Value.ToString().Trim();

                    //        int _cLocation = 0;

                    //        if (dataGridView_Part["Location", i].Value != null)
                    //            _cLocation = dataGridView_Part["Location", i].Value.ToString().Trim().Length;

                    //        if ((_cLocation > 2 || _vLocation == null))
                    //        {
                    //            //_vLocation
                    //            jPart.Location = "0";
                    //        }
                    //        else
                    //        {
                    //            var qLocate = (from t in db.Locations
                    //                           where Convert.ToString(t.LOC_ID) == _vLocation 
                    //                           select t).FirstOrDefault();
                    //            if (qLocate == null)
                    //            {
                    //                jPart.Location = "0";
                    //            }
                    //            else
                    //            {
                    //                jPart.Location = Convert.ToString(qLocate.LOC_ID);
                    //            }
                    //        }
                    //        //===========20150709=============================


                    //        jPart.SerialNumber = dataGridView_Part["S/N", i].Value == null ? null : dataGridView_Part["S/N", i].Value.ToString();


                    //        jPart.IsCancel = dataGridView_Part["IsCancel", i].Value.ToString()[0];
                    //        jPart.IsDownload = dataGridView_Part["IsDownload", i].Value.ToString()[0];
                    //        jPart.IsPrices = dataGridView_Part["IsPrices", i].Value.ToString() == "0" ? false : true;
                    //        jPart.ERP_StationCharge = stCharge;

                    //        if (dataGridView_Part["ERP_SPA_ID", i].Value != DBNull.Value && !string.IsNullOrEmpty(dataGridView_Part["ERP_SPA_ID", i].Value.ToString()))
                    //            jPart.ERP_SPA_ID = dataGridView_Part["ERP_SPA_ID", i].Value.ToString();
                    //        else
                    //            jPart.ERP_SPA_ID = null;

                    //        if (dataGridView_Part["ERP_PartName", i].Value != DBNull.Value && !string.IsNullOrEmpty(dataGridView_Part["ERP_PartName", i].Value.ToString()))
                    //            jPart.ERP_PartName = dataGridView_Part["ERP_PartName", i].Value.ToString();
                    //        else
                    //            jPart.ERP_PartName = null;

                    //        //jPart.ERP_SPA_ID = dataGridView_Part["ERP_SPA_ID", i].Value == DBNull.Value ? null : dataGridView_Part["ERP_SPA_ID", i].Value.ToString();
                    //        //jPart.ERP_PartName = dataGridView_Part["ERP_PartName", i].Value == DBNull.Value? null: dataGridView_Part["ERP_PartName", i].Value.ToString();

                    //        //อะไหล่ลูกค้า

                    //        jPart.IsCustomer = Convert.ToChar(_StatusSpareCus);

                    //        if (dataGridView_Part["ERP_orderline_id", i].Value != DBNull.Value && !string.IsNullOrEmpty(dataGridView_Part["ERP_orderline_id", i].Value.ToString()))
                    //            jPart.ERP_orderline_id = int.Parse(dataGridView_Part["ERP_orderline_id", i].Value.ToString());
                    //        else
                    //            jPart.ERP_orderline_id = null;


                    //        //string a = dataGridView_Part["S/N Old", i].Value.ToString();
                    //        ////20140905
                    //        //jPart.SerialNumber_Old = dataGridView_Part["S/N Old", i].Value.ToString();


                    //        if (dataGridView_Part["S/N Old", i].Value != DBNull.Value && !string.IsNullOrEmpty(dataGridView_Part["S/N Old", i].Value.ToString()))
                    //            jPart.SerialNumber_Old = dataGridView_Part["S/N Old", i].Value.ToString();
                    //        else
                    //            jPart.SerialNumber_Old = null;


                    //        //20141022
                    //         //20141127ยกเลิก jPart.EQP_ID = int.Parse(dataGridView_Part["ตำแหน่งที่เปลี่ยน", i].Value.ToString());

                    //        //20161116
                    //        jPart.ReferPage = dataGridView_Part["Page/Item", i].Value.ToString();


                    //        db.JOB_Detail_Spares.InsertOnSubmit(jPart);
                    //        db.SubmitChanges();


                    //        PartCount++;//ตรวจสอบการเพิ่ม Part เพื่อนำไปตรวจสอบ การออกรายงาน Services Report
                    //        if (_StatusSpare == true)
                    //            TypeJobRemain = true;

                    //    }//  if ((from j in db.JOB_Detail_Spares

                    //}//for (int i = 0; i <= dataGridView_Part.Rows.Count - 1; i++)

                    //#endregion




                    #region Insert JOB_Detail_Spare Online
                    strPoint = "Insert JOB_Detail_Spare";

                    //20200916 DateTime _StartDate = tmpStartDate;
                    var log_part = "";
                    for (int i = 0; i <= dataGridView_Part.Rows.Count - 1; i++)
                    {
                        DateTime _StartDate = tmpStartDate;//20200916
                        //===20200626  log check add part
                        string _vxLlocat = dataGridView_Part["Location", i].Value.ToString();
                        string _vxLpoi = dataGridView_Part["รหัสจุดเสีย", i].Value.ToString();
                        string _vxLfai = dataGridView_Part["FAI_ID", i].Value.ToString();
                        string _vxLsn = dataGridView_Part["S/N", i].Value.ToString();
                        string _vxLno = dataGridView_Part["NO.", i].Value.ToString();
                        string _vxLspa = dataGridView_Part["Part No.", i].Value.ToString();
                        string _vxLtra = dataGridView_Part["Type", i].Value.ToString();
                        string _vxLprice = dataGridView_Part["รวมสุทธิ", i].Value.ToString();




                        DateTime _xLend = DateTime.Parse(_pEnddate);
                        var qchkLogAdd = (from jd in db.JOB_Detail_Spares
                                          where
                                          jd.JOB_ID == txtJobID.Text.Trim() &&
                                          jd.POI_ID == _vxLpoi &&
                                          jd.Location == _vxLlocat &&
                                          jd.SerialNumber == _vxLsn &&
                                          jd.FAI_ID == _vxLfai &&
                                          jd.SPA_ID == _vxLspa &&
                                          jd.TRA_ID == _vxLtra &&
                                          jd.SumTotalPrices == decimal.Parse(_vxLprice) &&
                                          jd.Enddate.Value.Date == _xLend.Date &&
                                          jd.Enddate.Value.Hour == _xLend.Hour &&
                                          jd.Enddate.Value.Minute == _xLend.Minute &&
                                          jd.Enddate.Value.Second == _xLend.Second

                                          select jd).FirstOrDefault();

                        if (qchkLogAdd == null)
                        {
                            string wordingLog = "ADD_spa" + _vxLspa + "_poi" + _vxLpoi + "_fai" + _vxLfai + "_loc" + _vxLlocat + "_SN" + _vxLsn + "_tra" + _vxLtra + "_Price" + _vxLprice + "_enddate" + _pEnddate + "";

                            var sql_logadd = "";
                            sql_logadd = "Insert into [LogSystem] ([TableName] ,[DataID] ,[Detail] ,[UserID] ,[IPAddress]) " +
                                       " values ('JOB_Detail_Spare'  ,'" + txtJobID.Text.Trim() + "' ,'" + wordingLog + "' ,'" + UserInfo.UserId + "' ,CONVERT(char(15), CONNECTIONPROPERTY('client_net_address')))";
                            db.ExecuteCommand(sql_logadd);

                        }

                        //====



                        string _SPA_ID = dataGridView_Part["Part no.", i].Value.ToString();

                        log_part = _SPA_ID + ",";

                        //Write Part cancel
                        #region Write Part cancel

                        //if (dataGridView_Part["IsDownload", i].Value.ToString() == "Y" && dataGridView_Part["IsCancel", i].Value.ToString() == "1")
                        //{
                        //    foreach (var partID in logCancelSPA_ID)
                        //    {
                        //        if (partID == dataGridView_Part["Part no.", i].Value.ToString())
                        //            partLogCancel(
                        //                dataGridView_Part["Part no.", i].Value.ToString(),
                        //                dataGridView_Part["Location", i].Value.ToString(),
                        //                dataGridView_Part["รหัสจุดเสีย", i].Value.ToString(),
                        //                dataGridView_Part["FAI_ID", i].Value.ToString(),
                        //                dataGridView_Part["Type", i].Value.ToString()
                        //                );
                        //    }
                        //}
                        #endregion

                        //ดึง StartDate ของเดิมมาใช้
                        //_StartDate = checkStartDate(i, _StartDate, _SPA_ID);
                        _StartDate = checkStartDate(_StartDate, _SPA_ID);

                        bool _StatusSpare = (dataGridView_Part["สถานะ", i].Value.ToString() == "ค้าง") ? false : true;//7dr["สถานะ"]

                        //อะไหล่ลูกค้า
                        string _StatusSpareCus = (dataGridView_Part["อะไหล่ลูกค้า", i].Value.ToString() == "Y") ? "9" : "0";


                        int xNO = int.Parse(dataGridView_Part["NO.", i].Value.ToString());

                        char _stCharge = dataGridView_Part["ออกบิลในนาม", i].Value.ToString() == "สถานี" ? '0' : '1';

                        if ((from j in db.JOB_Detail_Spares
                             where j.JOB_ID == txtJobID.Text.Trim() && j.StartDate == _StartDate &&
                                 j.SPA_ID == _SPA_ID && j.NO == xNO
                             select j).Count() > 0)
                        { //เคยมี  part นี้แล้ว

                            #region Edite JOB Part
                            var sp = (from j in db.JOB_Detail_Spares
                                      where j.JOB_ID == txtJobID.Text.Trim() && j.StartDate == _StartDate &&
                                          j.SPA_ID == _SPA_ID && j.NO == xNO
                                      select j).FirstOrDefault();

                            sp.POI_ID = dataGridView_Part["รหัสจุดเสีย", i].Value.ToString();//dr["รหัสจุดเสีย"]

                            //Quantity
                            sp.Quantity = decimal.Parse(dataGridView_Part["จำนวน", i].Value.ToString());//dr["จำนวน"]      
                            sp.Prices = decimal.Parse(dataGridView_Part["ราคา/หน่วย", i].Value.ToString());
                            //PricesList
                            sp.PricesList = decimal.Parse(dataGridView_Part["PricesList", i].Value.ToString());
                            sp.SumPrices = decimal.Parse(dataGridView_Part["รวม", i].Value.ToString());
                            sp.SumVAT = decimal.Parse(dataGridView_Part["ภาษี", i].Value.ToString());
                            sp.Prices = decimal.Parse(dataGridView_Part["ราคา/หน่วย", i].Value.ToString());
                            sp.SumTotalPrices = decimal.Parse(dataGridView_Part["รวมสุทธิ", i].Value.ToString());
                            //รวมสุทธิ
                            sp.Discount = decimal.Parse(dataGridView_Part["Discount", i].Value.ToString());
                            sp.VAT = decimal.Parse(dataGridView_Part["VAT", i].Value.ToString());
                            sp.ControlFixAssetNo = dataGridView_Part["FixAssetNo", i].Value.ToString();

                            sp.StatusSpare = _StatusSpare;//dr["สถานะ"]
                            sp.TRA_ID = dataGridView_Part["Type", i].Value.ToString();//dr["TranType"]
                            sp.Comment = dataGridView_Part["Comment", i].Value == null ? null + (IsQT == false ? "#QT=None" : "") : dataGridView_Part["Comment", i].Value.ToString() + (IsQT == false ? "#QT=None" : "");//dr["Comment"]                         
                            //20200124 sp.Enddate = tmpEndDate; //ไม่บันทึกเวลาที่แก้ ทับ เวลาที่เคยปิด
                            //20200407 sp.EMP_ID = UserInfo.UserId; //ไม่บันทึก ผู้แก้  ทับ เจ้าของงาน
                            sp.FAI_ID = dataGridView_Part["FAI_ID", i].Value == null ? null : dataGridView_Part["FAI_ID", i].Value.ToString();
                            //20150709 ใช้ process แปลง sp.Location = dataGridView_Part["Location", i].Value == null ? null : dataGridView_Part["Location", i].Value.ToString();
                            //===========20150709=============================
                            string _vLocation = dataGridView_Part["Location", i].Value.ToString().Trim();

                            int _cLocation = 0;

                            if (dataGridView_Part["Location", i].Value != null)
                                _cLocation = dataGridView_Part["Location", i].Value.ToString().Trim().Length;

                            if ((_cLocation > 2 || _vLocation == null))
                            {
                                //_vLocation
                                sp.Location = "0";
                            }
                            else
                            {
                                var qLocate = (from t in db.Locations
                                               where Convert.ToString(t.LOC_ID) == _vLocation
                                               select t).FirstOrDefault();
                                if (qLocate == null)
                                {
                                    sp.Location = "0";
                                }
                                else
                                {
                                    sp.Location = Convert.ToString(qLocate.LOC_ID);
                                }
                            }
                            //===========20150709=============================


                            sp.SerialNumber = dataGridView_Part["S/N", i].Value == null ? null : dataGridView_Part["S/N", i].Value.ToString();

                            sp.IsCancel = dataGridView_Part["IsCancel", i].Value.ToString()[0];
                            sp.IsDownload = dataGridView_Part["IsDownload", i].Value.ToString()[0];
                            sp.IsPrices = (dataGridView_Part["isPrices", i].Value.ToString() == "0" ? false : true);

                            sp.ERP_StationCharge = _stCharge;

                            //if (dataGridView_Part["ERP_SPA_ID", i].Value != null)
                            //    sp.ERP_SPA_ID = dataGridView_Part["ERP_SPA_ID", i].Value.ToString();

                            //if (dataGridView_Part["ERP_PartName", i].Value != null)
                            //    sp.ERP_PartName = dataGridView_Part["ERP_PartName", i].Value.ToString();

                            if (dataGridView_Part["ERP_SPA_ID", i].Value != DBNull.Value && !string.IsNullOrEmpty(dataGridView_Part["ERP_SPA_ID", i].Value.ToString()))
                                sp.ERP_SPA_ID = dataGridView_Part["ERP_SPA_ID", i].Value.ToString();
                            else
                                sp.ERP_SPA_ID = null;

                            if (dataGridView_Part["ERP_PartName", i].Value != DBNull.Value && !string.IsNullOrEmpty(dataGridView_Part["ERP_PartName", i].Value.ToString()))
                                sp.ERP_PartName = dataGridView_Part["ERP_PartName", i].Value.ToString();
                            else
                                sp.ERP_PartName = null;

                            //อะไหล่ลูกค้า
                            sp.IsCustomer = Convert.ToChar(_StatusSpareCus);

                            //ERP_orderline_id
                            if (dataGridView_Part["ERP_orderline_id", i].Value != DBNull.Value && !string.IsNullOrEmpty(dataGridView_Part["ERP_orderline_id", i].Value.ToString()))
                                sp.ERP_orderline_id = int.Parse(dataGridView_Part["ERP_orderline_id", i].Value.ToString());
                            else
                                sp.ERP_orderline_id = null;


                            //20140905
                            sp.SerialNumber_Old = dataGridView_Part["S/N Old", i].Value.ToString();

                            //20141021 
                            //20141127ยกเลิก sp.EQP_ID = int.Parse(dataGridView_Part["ตำแหน่งที่เปลี่ยน", i].Value.ToString());

                            //20161116
                            sp.ReferPage = dataGridView_Part["Page/Item", i].Value.ToString();

                            db.SubmitChanges();

                            #endregion
                        }
                        else
                        {
                            #region New JOB Part
                            JOB_Detail_Spare jNew = new JOB_Detail_Spare();


                            jNew.NO = xNO;//i + 1,
                            jNew.JOB_ID = txtJobID.Text.Trim();
                            jNew.POI_ID = dataGridView_Part["รหัสจุดเสีย", i].Value.ToString();//dr["รหัสจุดเสีย"]
                            jNew.SPA_ID = dataGridView_Part["Part no.", i].Value.ToString();//dr["Part no."]

                            //20100127 jNew.StartDate = _StartDate;
                            jNew.StartDate = _StartDate; //20200613 ต้องได้เวบาเดิมที่ปิดงานไปแล้ว tmpStartDateTimeEditJOB;//ถ้ายังไม่เคยมี แสดงว่าจะเพิ่มใหม่ต้องเพิ่มเข้าเวลา startdate ล่าสุดเท่าน้ัน

                            //======

                            jNew.Quantity = decimal.Parse(dataGridView_Part["จำนวน", i].Value.ToString());//dr["จำนวน"]   
                            jNew.PricesList = decimal.Parse(dataGridView_Part["PricesList", i].Value.ToString());
                            jNew.Prices = decimal.Parse(dataGridView_Part["ราคา/หน่วย", i].Value.ToString());
                            jNew.SumPrices = decimal.Parse(dataGridView_Part["รวม", i].Value.ToString());
                            jNew.SumVAT = decimal.Parse(dataGridView_Part["ภาษี", i].Value.ToString());
                            jNew.SumTotalPrices = decimal.Parse(dataGridView_Part["รวมสุทธิ", i].Value.ToString());


                            jNew.Discount = decimal.Parse(dataGridView_Part["Discount", i].Value.ToString());
                            jNew.VAT = decimal.Parse(dataGridView_Part["VAT", i].Value.ToString());
                            jNew.ControlFixAssetNo = dataGridView_Part["FixAssetNo", i].Value.ToString();

                            jNew.StatusSpare = _StatusSpare;//dr["สถานะ"]
                            jNew.TRA_ID = dataGridView_Part["Type", i].Value.ToString();//dr["TranType"]
                            jNew.Comment = dataGridView_Part["Comment", i].Value == null ? null + (IsQT == false ? "#QT=None" : "") : dataGridView_Part["Comment", i].Value.ToString() + (IsQT == false ? "#QT=None" : "");//dr["Comment"]                         

                            //20200127 jNew.Enddate = tmpEndDate;
                            jNew.Enddate = tmpEndDateTimeEditJOB;
                            //=======
                            //20200407 jNew.EMP_ID = UserInfo.UserId; //ไม่บันทึก ผู้แก้  ทับ เจ้าของงาน
                            //20200407
                            jNew.EMP_ID = tmpVAN_ID;

                            jNew.FAI_ID = dataGridView_Part["FAI_ID", i].Value == null ? null : dataGridView_Part["FAI_ID", i].Value.ToString();
                            //20150709 ใช้ process แปลง jNew.Location = dataGridView_Part["Location", i].Value == null ? null : dataGridView_Part["Location", i].Value.ToString();

                            //===========20150709=============================
                            string _vLocation = dataGridView_Part["Location", i].Value.ToString().Trim();

                            int _cLocation = 0;

                            if (dataGridView_Part["Location", i].Value != null)
                                _cLocation = dataGridView_Part["Location", i].Value.ToString().Trim().Length;

                            if ((_cLocation > 2 || _vLocation == null))
                            {
                                //_vLocation
                                jNew.Location = "0";
                            }
                            else
                            {
                                var qLocate = (from t in db.Locations
                                               where Convert.ToString(t.LOC_ID) == _vLocation
                                               select t).FirstOrDefault();
                                if (qLocate == null)
                                {
                                    jNew.Location = "0";
                                }
                                else
                                {
                                    jNew.Location = Convert.ToString(qLocate.LOC_ID);
                                }
                            }
                            //===========20150709=============================




                            jNew.SerialNumber = dataGridView_Part["S/N", i].Value == null ? null : dataGridView_Part["S/N", i].Value.ToString();

                            jNew.IsCancel = dataGridView_Part["IsCancel", i].Value.ToString()[0];
                            jNew.IsDownload = dataGridView_Part["IsDownload", i].Value.ToString()[0];
                            jNew.IsPrices = (dataGridView_Part["isPrices", i].Value.ToString() == "0" ? false : true);
                            jNew.ERP_StationCharge = _stCharge;

                            //if (dataGridView_Part["ERP_SPA_ID", i].Value != null)
                            //    jNew.ERP_SPA_ID = dataGridView_Part["ERP_SPA_ID", i].Value.ToString();

                            //if (dataGridView_Part["ERP_PartName", i].Value != null)
                            //    jNew.ERP_PartName = dataGridView_Part["ERP_PartName", i].Value.ToString();
                            if (dataGridView_Part["ERP_SPA_ID", i].Value != DBNull.Value && !string.IsNullOrEmpty(dataGridView_Part["ERP_SPA_ID", i].Value.ToString()))
                                jNew.ERP_SPA_ID = dataGridView_Part["ERP_SPA_ID", i].Value.ToString();
                            else
                                jNew.ERP_SPA_ID = null;

                            if (dataGridView_Part["ERP_PartName", i].Value != DBNull.Value && !string.IsNullOrEmpty(dataGridView_Part["ERP_PartName", i].Value.ToString()))
                                jNew.ERP_PartName = dataGridView_Part["ERP_PartName", i].Value.ToString();
                            else
                                jNew.ERP_PartName = null;

                            //อะไหล่ลูกค้า
                            jNew.IsCustomer = Convert.ToChar(_StatusSpareCus);

                            //ERP_orderline_id
                            if (dataGridView_Part["ERP_orderline_id", i].Value != DBNull.Value && !string.IsNullOrEmpty(dataGridView_Part["ERP_orderline_id", i].Value.ToString()))
                                jNew.ERP_orderline_id = int.Parse(dataGridView_Part["ERP_orderline_id", i].Value.ToString());
                            else
                                jNew.ERP_orderline_id = null;



                            //S/N Old
                            if (dataGridView_Part["S/N Old", i].Value != DBNull.Value && !string.IsNullOrEmpty(dataGridView_Part["S/N Old", i].Value.ToString()))
                                jNew.SerialNumber_Old = dataGridView_Part["S/N Old", i].Value.ToString();
                            else
                                jNew.SerialNumber_Old = null;


                            //ตำแหน่งที่เปลี่ยน 
                            /* if (dataGridView_Part["ตำแหน่งที่เปลี่ยน", i].Value != DBNull.Value && !string.IsNullOrEmpty(dataGridView_Part["ตำแหน่งที่เปลี่ยน", i].Value.ToString()))
                                 jNew.EQP_ID = int.Parse(dataGridView_Part["ตำแหน่งที่เปลี่ยน", i].Value.ToString());
                             else
                                 jNew.EQP_ID = 99;
                             */

                            //20161116
                            jNew.ReferPage = dataGridView_Part["Page/Item", i].Value.ToString();

                            db.JOB_Detail_Spares.InsertOnSubmit(jNew);
                            db.SubmitChanges();

                            PartCount++;//ตรวจสอบการเพิ่ม Part เพื่อนำไปตรวจสอบ การออกรายงาน Services Report
                            if (_StatusSpare == true)
                                TypeJobRemain = true;

                            #endregion

                        }//  if ((from j in db.JOB_Detail_Spares

                    }//for (int i = 0; i <= dataGridView_Part.Rows.Count - 1; i++)


                    ////20200626 เปลี่ยน flow การเก็บ log
                    ////var sql_log = "";
                    //sql_log = "Insert into [LogSystem] ([TableName] ,[DataID] ,[Detail] ,[UserID] ,[IPAddress]) " +
                    //    " values ('JOB_Detail_Spare'  ,'" + txtJobID.Text.Trim() + "' ,'" + log_part + "' ,'" + UserInfo.UserId + "' ,CONVERT(char(15), CONNECTIONPROPERTY('client_net_address')))";
                    //db.ExecuteCommand(sql_log);
                    ////=====
                    #endregion





                    //======================================
                    // Insert JOB_Detail
                    //======================================
                    #region Insert JOB_Detail
                    strPoint = "Insert JOB_Detail";
                    if (qjd != null)
                    {
                        //tmpEndDate
                        //20200127 ไม่ต้องทำอะไรทั้งสิ้น qjd.EndDate = tmpStartDateTimeEditJOB;//20100127 tmpEndDate;
                        qjd.Problem_Detail = vProblem_Detail;// txtProblem_Detail.Text.Trim() == "" ? null : txtProblem_Detail.Text.Trim(); //สาเหตุ
                        qjd.Resole_Detail = txtResolve_Detail.Text.Trim() == "" ? null : txtResolve_Detail.Text.Trim();//วิธีการแก้ไข

                        if (TypeJobRemain == true)
                            qjd.TRE_ID = "02";//02	ค้างอะไหล่
                        else
                            qjd.TRE_ID = "03";// Job ค้าง


                        if (RadioButtonList_JobStatus2.Checked)//CloseJOB
                            qjd.JOBS_ID = "99";//ปิด JOB 
                        else
                            qjd.JOBS_ID = "07";//ค้าง JOB

                        //20160713
                        qjd.TCO_ID = "04";//ประเภทการปิด JOB {04 = เข้า Site (ผ่าน web)}

                        //20170215
                        qjd.RecDate_Endate = Server_date;//เวลา server


                        db.SubmitChanges();
                    }

                    #endregion


                    tmpjobID = txtJobID.Text.Trim();


                    ////===20200313===
                    //var qUserVers = (from t in db.Users where t.EMP_ID == UserInfo.UserId select t).First();
                    //if (qUserVers != null)
                    //{
                    //    qUserVers.SMSVersion = UserInfo.MySMSMINIVersion;
                    //    qUserVers.SMSVersionDate = tmpEndDate;
                    //    db.SubmitChanges();

                    //}
                    ////===============





                    //20200626//20191129 
                    //updateSN_byStation(db);

                    //20200626
                    //using (DAL.SMSManage.SMSManageDataContext db = new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn))
                    //{
                    //updateSN_byStation(db);
                    //}
                    //===

                    //20210217 updateSN_byStation(db);
                    updateSN_byStation(db, DateTime.Parse(_pEnddate));

                    //updateSN_byStation();

                    db.Transaction.Commit();



                    setDtFailure();
                    setDtPart();

                    FailureBinding();
                    PartBindind();

                    ClearControl();

                    ClearTextFail();
                    ClearPartText();

                    butttonEnable(false, false, true, true, true);

                    //20150629
                    //chkEnableSVReport(txtJobID.Text.Trim());
                    toolStripButton_Print.Enabled = true;


                    //20191224
                    DateTime dt1 = tmpEndDateTimeEditJOB.Value;
                    stmpEndDate = dt1.ToString("yyyyMMdd", new System.Globalization.CultureInfo("en-US"));
                    stmpEndTime = dt1.ToString("HH:mm:ss", new System.Globalization.CultureInfo("en-US"));

                    //stmpEndDate = dt1.ToString("yyyyMMdd");
                    //stmpEndTime = dt1.ToString("HH:mm:ss");
                }

                catch (Exception ex)
                {
                    db.Transaction.Rollback();
                    PartCount = 0;
                    closeProgress();
                    MessageBox.Show("เกิดข้อผิดพลาด Error: ในการบันทึกข้อมูล  หมายเลข JOB: " + txtJobID.Text.Trim() +
                        Environment.NewLine + " จุด " + strPoint +
                        Environment.NewLine + " Error: " + ex.Message, " กรุณาติดต่อผู้ดูแลระบบ...",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }

        }

        private bool isPartRement()
        {
            for (int i = 0; i <= dtPart.Rows.Count - 1; i++)
            {
                if (dtPart.Rows[i]["สถานะ"].ToString() == "ค้าง")
                    return true;
            }

            return false;
        }


        private void InsertExitCloseJOB()
        {
            string strPoint = "";
            DateTime tmpStartDate = DateTime.Now;
            DateTime tmpEndDate = DateTime.Now;

            //20191219
            DateTime tmpEndTime = DateTime.Now;



            //===20200527 
            DateTime? srvTimeToSave = null;
            strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn))
            {

                srvTimeToSave = dc
                   .ExecuteQuery<DateTime>("SELECT GETDATE()")
                   .First();

                tmpEndDate = srvTimeToSave.Value;
                Server_date = srvTimeToSave.Value;
            }

            //===========






            //if (UserInfo.ConnectMode == "0")
            //    strConn = Properties.Settings.Default["ServicesMSDBConnectionString"].ToString();
            //else
            //    strConn = Properties.Settings.Default["ServicesMSDBConnectionString"].ToString();


            //strConn = Properties.Settings.Default["ServicesMSDBConnectionString"].ToString();
            strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;

            try
            {
                //20191202 using (DAL.SMSManage.SMSManageDataContext db = new DAL.SMSManage.SMSManageDataContext(strConn))
                //20191129using (DAL.SMSManage.SMSManageDataContext db = new DAL.SMSManage.SMSManageDataContext(strConn))//เปลี่ยนไปใช้โครงสร้าง server    
                //using (DAL.SMSManage.SMSManageDataContext db = new DAL.SMSManage.SMSManageDataContext(strConn))
                using (DAL.SMSManage.SMSManageDataContext db = new DAL.SMSManage.SMSManageDataContext(strConn))
                {
                    try
                    {
                        db.Connection.Open();
                        db.Transaction = db.Connection.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted);

                        //======================================
                        // Close JOB
                        //======================================
                        #region Close JOB
                        strPoint = "Update Close JOB";
                        var qj = (from t in db.JOBs where t.JOB_ID == txtJobID.Text.Trim() select t).First();

                        if (RadioButtonList_JobStatus2.Checked == true)//CloseJOB
                        {
                            //เก็บเงินพิเศษ
                            char IsSpecialCharge = '0';
                            if (UserInfo.UserId.ToLower().StartsWith("v"))
                                if (MessageBox.Show("JOB [" + txtJobID.Text.Trim() + "] นี้ ต้องเก็บเงินพิเศษ หรือไม่ ?...", "ยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                    IsSpecialCharge = '1';

                            if (qj != null)
                            {
                                qj.CloseDate = tmpEndDate;
                                qj.JOBS_ID = "99";
                                qj.EMP_ID1 = UserInfo.UserId;//20200401 เตรียม qj.EMP_ID3 กรณีปิดงาน ไม่ต้องเปลี่ยนแล้ว

                                qj.EMP_ID3 = UserInfo.UserId;//20200401 เตรียม qj.EMP_ID3 กรณีปิดงาน ไม่ต้องเปลี่ยนแล้ว
                                qj.PMT_ID = cobProblemType.SelectedValue.ToString();
                                qj.TCO_ID = "04";//ประเภทการปิด JOB {04 = เข้า Site (ผ่าน web)}
                                qj.PRI_ID = comboBox_Priority.SelectedValue.ToString();
                                qj.IsSpecialCharge = IsSpecialCharge;
                                qj.IsUpload = "9"[0];
                            }

                            db.SubmitChanges();
                        }
                        else//JOB ค้าง
                        {
                            if (qj != null)
                            {
                                qj.JOBS_ID = "07";
                                qj.EMP_ID1 = UserInfo.UserId;//20200401 เตรียม qj.EMP_ID3 กรณีปิดงาน ไม่ต้องเปลี่ยนแล้ว
                                qj.EMP_ID3 = UserInfo.UserId;//20200401 เตรียม qj.EMP_ID3 กรณีปิดงาน ไม่ต้องเปลี่ยนแล้ว
                                qj.PMT_ID = cobProblemType.SelectedValue.ToString();
                                qj.PRI_ID = comboBox_Priority.SelectedValue.ToString();
                                qj.IsUpload = "9"[0];

                                if (isPartRement())//ตรวจสอบอะไหล่ค้าง
                                {
                                    //ตรวจสอบ ค้าง JOB เพื่อทำเสนอราคา
                                    //IsQuotation = null , 0= ไม่เสนอราคา, 1=ต้องทำใบเสนอราคา, 2=ทำใบเสนอราคาแล้ว
                                    if (MessageBox.Show("JOB [" + txtJobID.Text.Trim() + "] นี้ ต้องการทำใบเสนอราคาหรือไม่?...", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                        qj.IsQuotation = "1"[0];
                                    else
                                        qj.IsQuotation = "0"[0];
                                }
                                //20160713
                                qj.TCO_ID = "04";//ประเภทการปิด JOB {04 = เข้า Site (ผ่าน web)}

                            }
                            db.SubmitChanges();
                        }

                        #endregion
                        ////20200124 
                        //stmpEndDate = Convert.ToString(tmpEndDate);

                        /* คำสั่งยกลิกการประเมิน 20200508
                       //======================================
                       // JOB_ContentMent (บันทึกข้อมูลความพึงพอใจ)
                       //======================================
                       #region Insert JOB_ContentMent
                       strPoint = "Insert JOB_ContentMent";

                       string _jobremark = (tmpRemarkJobContentMent == "") ? null : tmpRemarkJobContentMent;
                       string JobCont = "";
                       if (radioButton_A.Checked) JobCont = "A";//(A) ดี
                       else if (radioButton_B.Checked) JobCont = "B";//(B) พอใช้
                       else if (radioButton_C.Checked) JobCont = "C";//(C) ปรับปรุง
                       else if (radioButton_D.Checked) JobCont = "D";//(D) ไม่ระบุ               

                       string _cmdStatus = "0";
                       if ((from t in db.JOB_ContentMents
                            where t.JOB_ID == txtJobID.Text.Trim() && t.ContentStatus == _cmdStatus[0] && t.CMD_ID == "00"

                            select t).Count() > 0)
                       {
                           var delq11 = from t in db.JOB_ContentMents
                                        where t.JOB_ID == txtJobID.Text.Trim() && t.ContentStatus == _cmdStatus[0] && t.CMD_ID == "00"
                                        select t;
                           db.JOB_ContentMents.DeleteAllOnSubmit(delq11);
                           db.SubmitChanges();
                       }

                       if (JobCont != "")
                       {
                           JOB_ContentMent jcm = new JOB_ContentMent()
                           {
                               JOB_ID = txtJobID.Text.Trim(),
                               CMD_ID = "00",//ความคิดเห็นของเจ้าหน้าที่ Flowco
                               ContentStatus = _cmdStatus[0],
                               JobContentMent = JobCont,
                               Remark = _jobremark
                           };
                           db.JOB_ContentMents.InsertOnSubmit(jcm);
                          db.SubmitChanges();
                       }
                       #endregion
                       */

                        //======================================
                        // Insert JOB_Failure_Detail
                        //======================================
                        #region Insert JOB_Failure_Detail Offline
                        ////strPoint = "Insert JOB_Failure_Detail";
                        //////20150126
                        ////string vProblem_Detail = null;

                        ////if ((from t in db.JOB_Failure_Details where t.JOB_ID == txtJobID.Text.Trim() select t).Count() > 0)
                        ////{
                        ////    var delq11 = from t in db.JOB_Failure_Details
                        ////                 where t.JOB_ID == txtJobID.Text.Trim()
                        ////                 select t;
                        ////    db.JOB_Failure_Details.DeleteAllOnSubmit(delq11);
                        ////    db.SubmitChanges();

                        ////}


                        ////int tmp_NO = 0;
                        ////for (int i = 0; i <= dataGridView_FailPoint.Rows.Count - 1; i++)
                        ////{
                        ////    //----20150126
                        ////    var qATF = (from t in db.Failure_Actions
                        ////                where t.ATF_ID == dataGridView_FailPoint["ATF_ID", i].Value.ToString()
                        ////                select t).FirstOrDefault();

                        ////    if (qATF != null)
                        ////    {

                        ////        vProblem_Detail += "รหัสจุดเสีย " + dataGridView_FailPoint["รหัสจุดเสีย", i].Value.ToString()
                        ////        + ": " + dataGridView_FailPoint["ATF_ID", i].Value.ToString()
                        ////        + " " + qATF.Action_Failure + "  ";

                        ////    }
                        ////    //----20150126

                        ////    tmp_NO = int.Parse(dataGridView_FailPoint["NO.", i].Value.ToString());
                        ////    string tmp_POI_ID = dataGridView_FailPoint["รหัสจุดเสีย", i].Value.ToString();
                        ////    string tmp_Failure_Detail = dataGridView_FailPoint["อาการเสีย", i].Value.ToString();
                        ////    //string SerialNumber = dataGridView_FailPoint["S/N(Old)", i].Value.ToString();
                        ////    //string SerialNumber_new = dataGridView_FailPoint["S/N(New)", i].Value.ToString();
                        ////    string SerialNumber = dataGridView_FailPoint["S/N(Old)", i].Value.ToString();
                        ////    string fixAssetNo = dataGridView_FailPoint["FixAssetNo", i].Value.ToString();

                        ////    string SerialNumber_new = dataGridView_FailPoint["S/N(New)", i].Value.ToString();
                        ////    string fixAssetNo_New = dataGridView_FailPoint["FixAssetNo_New", i].Value.ToString();


                        ////    //20150709 ใช้ process แปลง string tmp_Location = dataGridView_FailPoint["Location", i].Value.ToString();


                        ////    //=============20150709==================
                        ////    int _LOC_ID = 0;
                        ////    string _vfLocation = dataGridView_FailPoint["Location", i].Value.ToString().Trim();

                        ////    int _cfLocation = 0;

                        ////    if (dataGridView_FailPoint["Location", i].Value != null)
                        ////        _cfLocation = dataGridView_FailPoint["Location", i].Value.ToString().Trim().Length;

                        ////    if ((_cfLocation > 2 || _vfLocation == null))
                        ////    {
                        ////        //_vLocation
                        ////        _LOC_ID = 0;
                        ////    }
                        ////    else
                        ////    {
                        ////        var qfLocate = (from t in db.Locations
                        ////                        where Convert.ToString(t.LOC_ID) == _vfLocation //dataGridView_Part["SPA_ID", i].Value.ToString()
                        ////                        select t).FirstOrDefault();
                        ////        if (qfLocate == null)
                        ////        {
                        ////            _LOC_ID = 0;
                        ////        }
                        ////        else
                        ////        {
                        ////            _LOC_ID = qfLocate.LOC_ID;
                        ////        }
                        ////    }
                        ////    //===============20150709================
                        ////    string tmp_FAI_ID = dataGridView_FailPoint["FAI_ID", i].Value.ToString();
                        ////    string tmp_Model = dataGridView_FailPoint["Model", i].Value.ToString();

                        ////    decimal tmp_StartLiter = Convert.ToDecimal(dataGridView_FailPoint["StartLiter", i].Value.ToString() == "&nbsp;" ? "0" : dataGridView_FailPoint["StartLiter", i].Value.ToString());
                        ////    decimal tmp_EndLiter = Convert.ToDecimal(dataGridView_FailPoint["EndLiter", i].Value.ToString() == "&nbsp;" ? "0" : dataGridView_FailPoint["EndLiter", i].Value.ToString());
                        ////    decimal tmp_LiterTest = Convert.ToDecimal(dataGridView_FailPoint["LiterTest", i].Value.ToString() == "&nbsp;" ? "0" : dataGridView_FailPoint["LiterTest", i].Value.ToString());

                        ////    int _fcatID = 0;
                        ////    int _fbaID = 0;
                        ////    int _fmdID = 0;

                        ////    //20141204
                        ////    //20150709 ใช้ process แปลง int _LOC_ID =0;







                        ////    if (dataGridView_FailPoint["FCAT_ID", i].Value != DBNull.Value)
                        ////        _fcatID = Convert.ToInt32(dataGridView_FailPoint["FCAT_ID", i].Value);

                        ////    if (dataGridView_FailPoint["FBA_ID", i].Value != DBNull.Value)
                        ////        _fbaID = Convert.ToInt32(dataGridView_FailPoint["FBA_ID", i].Value);

                        ////    if (dataGridView_FailPoint["FMD_ID", i].Value != DBNull.Value)
                        ////        _fmdID = Convert.ToInt32(dataGridView_FailPoint["FMD_ID", i].Value);

                        ////    //20141204
                        ////    //20150709 ใช้ process แปลง _LOC_ID = Convert.ToInt32(tmp_Location);

                        ////    //===20141021
                        ////    int _fPRT_ID =0;
                        ////    //20141127ยกเลิก int _fEQP_ID =0;
                        ////    int _fFAS_ID =0;

                        ////    if (dataGridView_FailPoint["PRT_ID", i].Value != DBNull.Value)
                        ////        _fPRT_ID = Convert.ToInt32(dataGridView_FailPoint["PRT_ID", i].Value);

                        ////    //20141127ยกเลิก if (dataGridView_FailPoint["EQP_ID", i].Value != DBNull.Value)
                        ////    //20141127ยกเลิก    _fEQP_ID = Convert.ToInt32(dataGridView_FailPoint["EQP_ID", i].Value);

                        ////    string _fATF_ID = dataGridView_FailPoint["ATF_ID", i].Value.ToString();

                        ////    if (dataGridView_FailPoint["FAS_ID", i].Value != DBNull.Value)
                        ////        _fFAS_ID = Convert.ToInt32(dataGridView_FailPoint["FAS_ID", i].Value);

                        ////    string SolvingByWI = dataGridView_FailPoint["SolvingByWI", i].Value.ToString();


                        ////    //===20180806
                        ////    int _fcatCH = 0;
                        ////    if (dataGridView_FailPoint["FCAT_CH", i].Value != DBNull.Value)
                        ////        _fcatCH = Convert.ToInt32(dataGridView_FailPoint["FCAT_CH", i].Value);
                        ////    //============





                        ////    JOB_Failure_Detail jf = new JOB_Failure_Detail()
                        ////    {
                        ////        NO = tmp_NO,
                        ////        JOB_ID = txtJobID.Text.Trim(),
                        ////        POI_ID = tmp_POI_ID,
                        ////        FAI_ID = tmp_FAI_ID == "" ? "0000000" : tmp_FAI_ID,
                        ////        Failure_Detail = tmp_Failure_Detail == "" ? null : tmp_Failure_Detail.Trim(),
                        ////        trandate = tmpEndDate,//getServerTime(),
                        ////        //SerialNumber = SerialNumber,
                        ////        //SerialNumber_New = SerialNumber_new,
                        ////        SerialNumber = SerialNumber,
                        ////        FixAssetNo = fixAssetNo,
                        ////        SerialNumber_New = SerialNumber_new,
                        ////        FixAssetNo_New = fixAssetNo_New,

                        ////        FCAT_ID = _fcatID,
                        ////        FBA_ID = _fbaID,
                        ////        FMD_ID = _fmdID,

                        ////        StartLiter = tmp_StartLiter,
                        ////        EndLiter = tmp_EndLiter,
                        ////        LiterTest = tmp_LiterTest,
                        ////        LOC_ID = _LOC_ID,//20141204
                        ////        Location = null,//20141204 tmp_Location.Trim() == "" ? null : tmp_Location.Trim(),
                        ////        Model = tmp_Model.Trim() == "" ? null : tmp_Model.Trim(),
                        ////        IsCancel = dataGridView_FailPoint["IsCancel", i].Value.ToString()[0],
                        ////        IsDownload = dataGridView_FailPoint["IsDownload", i].Value.ToString()[0],

                        ////        //20141021
                        ////        PRT_ID = _fPRT_ID,
                        ////        //20141127ยกเลิก EQP_ID = _fEQP_ID,
                        ////        ATF_ID = _fATF_ID,
                        ////        FAS_ID = _fFAS_ID,
                        ////        SolvingByWI = SolvingByWI

                        ////        ,FCAT_CH = _fcatCH //20180806 

                        ////    };

                        ////    db.JOB_Failure_Details.InsertOnSubmit(jf);
                        ////    db.SubmitChanges();
                        ////}
                        #endregion
                        //========20191120========================
                        #region Insert JOB_Failure_Detail online
                        strPoint = "Insert JOB_Failure_Detail";
                        string vProblem_Detail = null;

                        int tmp_NO = 0;
                        for (int i = 0; i <= dataGridView_FailPoint.Rows.Count - 1; i++)
                        {
                            //----20150126
                            var qATF = (from t in db.Failure_Actions
                                        where t.ATF_ID == dataGridView_FailPoint["ATF_ID", i].Value.ToString()
                                        select t).FirstOrDefault();

                            if (qATF != null)
                            {

                                vProblem_Detail += "รหัสจุดเสีย " + dataGridView_FailPoint["รหัสจุดเสีย", i].Value.ToString()
                                + ": " + dataGridView_FailPoint["ATF_ID", i].Value.ToString()
                                + " " + qATF.Action_Failure + "  ";

                            }
                            //----20150126
                            tmp_NO = int.Parse(dataGridView_FailPoint["NO.", i].Value.ToString());
                            string tmp_POI_ID = dataGridView_FailPoint["รหัสจุดเสีย", i].Value.ToString();
                            string tmp_Failure_Detail = dataGridView_FailPoint["อาการเสีย", i].Value.ToString();
                            //string SerialNumber = dataGridView_FailPoint["S/N(Old)", i].Value.ToString();
                            //string SerialNumber_new = dataGridView_FailPoint["S/N(New)", i].Value.ToString();
                            string SerialNumber = dataGridView_FailPoint["S/N(Old)", i].Value.ToString();
                            string fixAssetNo = dataGridView_FailPoint["FixAssetNo", i].Value.ToString();

                            string SerialNumber_new = dataGridView_FailPoint["S/N(New)", i].Value.ToString();
                            string fixAssetNo_New = dataGridView_FailPoint["FixAssetNo_New", i].Value.ToString();


                            //20150709 ใช้ process แปลง string tmp_Location = dataGridView_FailPoint["Location", i].Value.ToString();


                            //=============20150709==================
                            int _LOC_ID = 0;
                            string _vfLocation = dataGridView_FailPoint["Location", i].Value.ToString().Trim();

                            int _cfLocation = 0;

                            if (dataGridView_FailPoint["Location", i].Value != null)
                                _cfLocation = dataGridView_FailPoint["Location", i].Value.ToString().Trim().Length;

                            if ((_cfLocation > 2 || _vfLocation == null))
                            {
                                //_vLocation
                                _LOC_ID = 0;
                            }
                            else
                            {
                                var qfLocate = (from t in db.Locations
                                                where Convert.ToString(t.LOC_ID) == _vfLocation //dataGridView_Part["SPA_ID", i].Value.ToString()
                                                select t).FirstOrDefault();
                                if (qfLocate == null)
                                {
                                    _LOC_ID = 0;
                                }
                                else
                                {
                                    _LOC_ID = qfLocate.LOC_ID;
                                }
                            }
                            //===============20150709================
                            string tmp_FAI_ID = dataGridView_FailPoint["FAI_ID", i].Value.ToString();
                            string tmp_Model = dataGridView_FailPoint["Model", i].Value.ToString();

                            decimal tmp_StartLiter = Convert.ToDecimal(dataGridView_FailPoint["StartLiter", i].Value.ToString() == "&nbsp;" ? "0" : dataGridView_FailPoint["StartLiter", i].Value.ToString());
                            decimal tmp_EndLiter = Convert.ToDecimal(dataGridView_FailPoint["EndLiter", i].Value.ToString() == "&nbsp;" ? "0" : dataGridView_FailPoint["EndLiter", i].Value.ToString());
                            decimal tmp_LiterTest = Convert.ToDecimal(dataGridView_FailPoint["LiterTest", i].Value.ToString() == "&nbsp;" ? "0" : dataGridView_FailPoint["LiterTest", i].Value.ToString());

                            int _fcatID = 0;
                            int _fbaID = 0;
                            int _fmdID = 0;

                            //20141204
                            //20150709 ใช้ process แปลง int _LOC_ID =0;

                            if (dataGridView_FailPoint["FCAT_ID", i].Value != DBNull.Value)
                                _fcatID = Convert.ToInt32(dataGridView_FailPoint["FCAT_ID", i].Value);

                            if (dataGridView_FailPoint["FBA_ID", i].Value != DBNull.Value)
                                _fbaID = Convert.ToInt32(dataGridView_FailPoint["FBA_ID", i].Value);

                            if (dataGridView_FailPoint["FMD_ID", i].Value != DBNull.Value)
                                _fmdID = Convert.ToInt32(dataGridView_FailPoint["FMD_ID", i].Value);

                            //20141204
                            //20150709 ใช้ process แปลง _LOC_ID = Convert.ToInt32(tmp_Location);

                            //===20141021
                            int _fPRT_ID = 0;
                            //20141127ยกเลิก int _fEQP_ID =0;
                            int _fFAS_ID = 0;

                            if (dataGridView_FailPoint["PRT_ID", i].Value != DBNull.Value)
                                _fPRT_ID = Convert.ToInt32(dataGridView_FailPoint["PRT_ID", i].Value);

                            //20141127ยกเลิก if (dataGridView_FailPoint["EQP_ID", i].Value != DBNull.Value)
                            //20141127ยกเลิก    _fEQP_ID = Convert.ToInt32(dataGridView_FailPoint["EQP_ID", i].Value);

                            string _fATF_ID = dataGridView_FailPoint["ATF_ID", i].Value.ToString();

                            if (dataGridView_FailPoint["FAS_ID", i].Value != DBNull.Value)
                                _fFAS_ID = Convert.ToInt32(dataGridView_FailPoint["FAS_ID", i].Value);

                            string SolvingByWI = dataGridView_FailPoint["SolvingByWI", i].Value.ToString();


                            //===20180806
                            int _fcatCH = 0;
                            if (dataGridView_FailPoint["FCAT_CH", i].Value != DBNull.Value)
                                _fcatCH = Convert.ToInt32(dataGridView_FailPoint["FCAT_CH", i].Value);
                            //============


                            //==ตรวจสอบ insert update  
                            string vtmp_FAI_ID = tmp_FAI_ID == "" ? "0000000" : tmp_FAI_ID;
                            var qF = (from j in db.JOB_Failure_Details
                                      where //20191212(j.NO == tmp_NO) &&
                                     (j.JOB_ID == txtJobID.Text.Trim()) &&
                                     (j.POI_ID == tmp_POI_ID) &&
                                     (j.FAI_ID == vtmp_FAI_ID) &&
                                     (j.SerialNumber == SerialNumber) &&
                                     (j.LOC_ID == _LOC_ID) //&&
                                      //(j.IsCancel == IsCancelF) 
                                      select j).FirstOrDefault();


                            if (qF != null)
                            {
                                //qF.NO = Convert.ToInt32(dataGridView_JOB_Failure_Detail["NO", x].Value);
                                //qF.JOB_ID = dataGridView_JOB_Failure_Detail["JOB_ID", x].Value.ToString().Trim();
                                //qF.POI_ID = dataGridView_JOB_Failure_Detail["POI_ID", x].Value.ToString().Trim();
                                qF.FAI_ID = tmp_FAI_ID == "" ? "0000000" : tmp_FAI_ID;
                                qF.Failure_Detail = tmp_Failure_Detail == "" ? null : tmp_Failure_Detail.Trim();

                                qF.SerialNumber = SerialNumber;
                                qF.SerialNumber_New = SerialNumber_new;

                                qF.FixAssetNo = fixAssetNo;
                                qF.FixAssetNo_New = fixAssetNo_New;

                                qF.FCAT_ID = _fcatID;

                                qF.FBA_ID = _fbaID;

                                qF.FMD_ID = _fmdID;

                                qF.trandate = tmpEndDate;
                                qF.StartLiter = tmp_StartLiter;
                                qF.EndLiter = tmp_EndLiter;
                                qF.LiterTest = tmp_LiterTest;
                                qF.Location = null;

                                //20141222
                                qF.LOC_ID = _LOC_ID;

                                qF.Model = tmp_Model.Trim() == "" ? null : tmp_Model.Trim();
                                qF.IsCancel = dataGridView_FailPoint["IsCancel", i].Value.ToString()[0];

                                //20141027
                                qF.PRT_ID = _fPRT_ID;
                                //20141201 EQP_ID = Convert.ToInt32(dataGridView_JOB_Failure_Detail["EQP_ID", x].Value),
                                qF.ATF_ID = _fATF_ID;
                                qF.FAS_ID = _fFAS_ID;
                                qF.SolvingByWI = SolvingByWI;

                                //20180806 
                                qF.FCAT_CH = _fcatCH;

                                qF.IsDownload = dataGridView_FailPoint["IsDownload", i].Value.ToString()[0];

                                db.SubmitChanges();

                            }
                            else
                            {
                                //เพิ่มใหม่
                                JOB_Failure_Detail jf = new JOB_Failure_Detail()
                                {
                                    NO = tmp_NO,
                                    JOB_ID = txtJobID.Text.Trim(),
                                    POI_ID = tmp_POI_ID,
                                    FAI_ID = tmp_FAI_ID == "" ? "0000000" : tmp_FAI_ID,
                                    Failure_Detail = tmp_Failure_Detail == "" ? null : tmp_Failure_Detail.Trim(),
                                    trandate = tmpEndDate,//getServerTime(),
                                    //SerialNumber = SerialNumber,
                                    //SerialNumber_New = SerialNumber_new,
                                    SerialNumber = SerialNumber,
                                    FixAssetNo = fixAssetNo,
                                    SerialNumber_New = SerialNumber_new,
                                    FixAssetNo_New = fixAssetNo_New,

                                    FCAT_ID = _fcatID,
                                    FBA_ID = _fbaID,
                                    FMD_ID = _fmdID,

                                    StartLiter = tmp_StartLiter,
                                    EndLiter = tmp_EndLiter,
                                    LiterTest = tmp_LiterTest,
                                    LOC_ID = _LOC_ID,//20141204
                                    Location = null,//20141204 tmp_Location.Trim() == "" ? null : tmp_Location.Trim(),
                                    Model = tmp_Model.Trim() == "" ? null : tmp_Model.Trim(),
                                    IsCancel = dataGridView_FailPoint["IsCancel", i].Value.ToString()[0],
                                    IsDownload = dataGridView_FailPoint["IsDownload", i].Value.ToString()[0],

                                    //20141021
                                    PRT_ID = _fPRT_ID,
                                    //20141127ยกเลิก EQP_ID = _fEQP_ID,
                                    ATF_ID = _fATF_ID,
                                    FAS_ID = _fFAS_ID,
                                    SolvingByWI = SolvingByWI

                                    ,
                                    FCAT_CH = _fcatCH //20180806 

                                };

                                db.JOB_Failure_Details.InsertOnSubmit(jf);
                                db.SubmitChanges();
                            }
                            //======================




                        }//for
                        #endregion
                        //==================================
                        //======================================
                        // Update JOB_Detail
                        //======================================
                        #region Get StartDate
                        strPoint = "get JOB_Detail StartDate";

                        var qjd = (from t in db.JOB_Details
                                   where t.JOB_ID == txtJobID.Text.Trim() && t.StatusDetail == true
                                   orderby t.RecDate descending
                                   select t).FirstOrDefault();

                        if (qjd != null)
                        {
                            tmpStartDate = qjd.StartDate;
                        }

                        bool TypeJobRemain = false;

                        #endregion


                        //======================================
                        // Insert JOB_Detail_Spare
                        //======================================
                        #region Insert JOB_Detail_Spare Offline
                        ////strPoint = "Insert JOB_Detail_Spare";

                        //////Clear Old Part
                        ////if ((from t in db.JOB_Detail_Spares where t.JOB_ID == txtJobID.Text.Trim() select t).Count() > 0)
                        ////{
                        ////    var delq2 = from t in db.JOB_Detail_Spares 
                        ////                where t.JOB_ID == txtJobID.Text.Trim() 
                        ////                select t;
                        ////    db.JOB_Detail_Spares.DeleteAllOnSubmit(delq2);
                        ////    db.SubmitChanges();
                        ////}


                        ////DateTime _StartDate = tmpStartDate;
                        ////for (int i = 0; i <= dataGridView_Part.Rows.Count - 1; i++)
                        ////{
                        ////    string _SPA_ID = dataGridView_Part["Part no.", i].Value.ToString();

                        ////    //Write Part cancel
                        ////    #region Write Part cancel

                        ////    //if (dataGridView_Part["IsDownload", i].Value.ToString() == "Y" && dataGridView_Part["IsCancel", i].Value.ToString() == "1")
                        ////    //{
                        ////    //    foreach (var partID in logCancelSPA_ID)
                        ////    //    {
                        ////    //        if (partID == dataGridView_Part["Part no.", i].Value.ToString())
                        ////    //            partLogCancel(
                        ////    //                dataGridView_Part["Part no.", i].Value.ToString(),
                        ////    //                dataGridView_Part["Location", i].Value.ToString(),
                        ////    //                dataGridView_Part["รหัสจุดเสีย", i].Value.ToString(),
                        ////    //                dataGridView_Part["FAI_ID", i].Value.ToString(),
                        ////    //                dataGridView_Part["Type", i].Value.ToString()
                        ////    //                );
                        ////    //    }
                        ////    //}
                        ////    #endregion

                        ////    //ดึง StartDate ของเดิมมาใช้
                        ////    //_StartDate = checkStartDate(i, _StartDate, _SPA_ID);
                        ////    _StartDate = checkStartDate(_StartDate, _SPA_ID);

                        ////    bool _StatusSpare = (dataGridView_Part["สถานะ", i].Value.ToString() == "ค้าง") ? false : true;//7dr["สถานะ"]

                        ////    //อะไหล่ลูกค้า
                        ////    string _StatusSpareCus = (dataGridView_Part["อะไหล่ลูกค้า", i].Value.ToString() == "Y") ? "9" : "0";


                        ////    int xNO = int.Parse(dataGridView_Part["NO.", i].Value.ToString());

                        ////    char _stCharge = dataGridView_Part["ออกบิลในนาม", i].Value.ToString() == "สถานี" ? '0' : '1';

                        ////    if ((from j in db.JOB_Detail_Spares
                        ////         where j.JOB_ID == txtJobID.Text.Trim() && j.StartDate == _StartDate &&
                        ////             j.SPA_ID == _SPA_ID && j.NO == xNO
                        ////         select j).Count() > 0)
                        ////    {

                        ////        #region Edite JOB Part
                        ////        var sp = (from j in db.JOB_Detail_Spares
                        ////                  where j.JOB_ID == txtJobID.Text.Trim() && j.StartDate == _StartDate &&
                        ////                      j.SPA_ID == _SPA_ID && j.NO == xNO
                        ////                  select j).FirstOrDefault();

                        ////        sp.POI_ID = dataGridView_Part["รหัสจุดเสีย", i].Value.ToString();//dr["รหัสจุดเสีย"]

                        ////        //Quantity
                        ////        sp.Quantity = decimal.Parse(dataGridView_Part["จำนวน", i].Value.ToString());//dr["จำนวน"]      
                        ////        sp.Prices = decimal.Parse(dataGridView_Part["ราคา/หน่วย", i].Value.ToString());
                        ////        //PricesList
                        ////        sp.PricesList = decimal.Parse(dataGridView_Part["PricesList", i].Value.ToString());
                        ////        sp.SumPrices = decimal.Parse(dataGridView_Part["รวม", i].Value.ToString());
                        ////        sp.SumVAT = decimal.Parse(dataGridView_Part["ภาษี", i].Value.ToString());
                        ////        sp.Prices = decimal.Parse(dataGridView_Part["ราคา/หน่วย", i].Value.ToString());
                        ////        sp.SumTotalPrices = decimal.Parse(dataGridView_Part["รวมสุทธิ", i].Value.ToString());
                        ////        //รวมสุทธิ
                        ////        sp.Discount = decimal.Parse(dataGridView_Part["Discount", i].Value.ToString());
                        ////        sp.VAT = decimal.Parse(dataGridView_Part["VAT", i].Value.ToString());
                        ////        sp.ControlFixAssetNo = dataGridView_Part["FixAssetNo", i].Value.ToString();

                        ////        sp.StatusSpare = _StatusSpare;//dr["สถานะ"]
                        ////        sp.TRA_ID = dataGridView_Part["Type", i].Value.ToString();//dr["TranType"]
                        ////        sp.Comment = dataGridView_Part["Comment", i].Value == null ? null + (IsQT == false ? "#QT=None" : "") : dataGridView_Part["Comment", i].Value.ToString() + (IsQT == false ? "#QT=None" : "");//dr["Comment"]                         
                        ////        sp.Enddate = tmpEndDate;
                        ////        sp.EMP_ID = UserInfo.UserId;
                        ////        sp.FAI_ID = dataGridView_Part["FAI_ID", i].Value == null ? null : dataGridView_Part["FAI_ID", i].Value.ToString();
                        ////        //20150709 ใช้ process แปลง sp.Location = dataGridView_Part["Location", i].Value == null ? null : dataGridView_Part["Location", i].Value.ToString();
                        ////        //===========20150709=============================
                        ////        string _vLocation = dataGridView_Part["Location", i].Value.ToString().Trim();

                        ////        int _cLocation = 0;

                        ////        if (dataGridView_Part["Location", i].Value != null)
                        ////            _cLocation = dataGridView_Part["Location", i].Value.ToString().Trim().Length;

                        ////        if ((_cLocation > 2 || _vLocation == null))
                        ////        {
                        ////            //_vLocation
                        ////            sp.Location = "0";
                        ////        }
                        ////        else
                        ////        {
                        ////            var qLocate = (from t in db.Locations
                        ////                           where Convert.ToString(t.LOC_ID) == _vLocation
                        ////                           select t).FirstOrDefault();
                        ////            if (qLocate == null)
                        ////            {
                        ////                sp.Location = "0";
                        ////            }
                        ////            else
                        ////            {
                        ////                sp.Location = Convert.ToString(qLocate.LOC_ID);
                        ////            }
                        ////        }
                        ////        //===========20150709=============================


                        ////        sp.SerialNumber = dataGridView_Part["S/N", i].Value == null ? null : dataGridView_Part["S/N", i].Value.ToString();

                        ////        sp.IsCancel = dataGridView_Part["IsCancel", i].Value.ToString()[0];
                        ////        sp.IsDownload = dataGridView_Part["IsDownload", i].Value.ToString()[0];
                        ////        sp.IsPrices = (dataGridView_Part["isPrices", i].Value.ToString() == "0" ? false : true);

                        ////        sp.ERP_StationCharge = _stCharge;

                        ////        //if (dataGridView_Part["ERP_SPA_ID", i].Value != null)
                        ////        //    sp.ERP_SPA_ID = dataGridView_Part["ERP_SPA_ID", i].Value.ToString();

                        ////        //if (dataGridView_Part["ERP_PartName", i].Value != null)
                        ////        //    sp.ERP_PartName = dataGridView_Part["ERP_PartName", i].Value.ToString();

                        ////        if (dataGridView_Part["ERP_SPA_ID", i].Value != DBNull.Value && !string.IsNullOrEmpty(dataGridView_Part["ERP_SPA_ID", i].Value.ToString()))
                        ////            sp.ERP_SPA_ID = dataGridView_Part["ERP_SPA_ID", i].Value.ToString();
                        ////        else
                        ////            sp.ERP_SPA_ID = null;

                        ////        if (dataGridView_Part["ERP_PartName", i].Value != DBNull.Value && !string.IsNullOrEmpty(dataGridView_Part["ERP_PartName", i].Value.ToString()))
                        ////            sp.ERP_PartName = dataGridView_Part["ERP_PartName", i].Value.ToString();
                        ////        else
                        ////            sp.ERP_PartName = null;

                        ////        //อะไหล่ลูกค้า
                        ////        sp.IsCustomer = Convert.ToChar(_StatusSpareCus);

                        ////        //ERP_orderline_id
                        ////        if (dataGridView_Part["ERP_orderline_id", i].Value != DBNull.Value && !string.IsNullOrEmpty(dataGridView_Part["ERP_orderline_id", i].Value.ToString()))
                        ////            sp.ERP_orderline_id = int.Parse(dataGridView_Part["ERP_orderline_id", i].Value.ToString());
                        ////        else
                        ////            sp.ERP_orderline_id = null;


                        ////        //20140905
                        ////        sp.SerialNumber_Old = dataGridView_Part["S/N Old", i].Value.ToString();

                        ////        //20141021 
                        ////        //20141127ยกเลิก sp.EQP_ID = int.Parse(dataGridView_Part["ตำแหน่งที่เปลี่ยน", i].Value.ToString());

                        ////        //20161116
                        ////        sp.ReferPage = dataGridView_Part["Page/Item", i].Value.ToString();

                        ////       db.SubmitChanges();

                        ////        #endregion
                        ////    }
                        ////    else
                        ////    {
                        ////        #region New JOB Part
                        ////        JOB_Detail_Spare jNew = new JOB_Detail_Spare();


                        ////        jNew.NO = xNO;//i + 1,
                        ////        jNew.JOB_ID = txtJobID.Text.Trim();
                        ////        jNew.POI_ID = dataGridView_Part["รหัสจุดเสีย", i].Value.ToString();//dr["รหัสจุดเสีย"]
                        ////        jNew.SPA_ID = dataGridView_Part["Part no.", i].Value.ToString();//dr["Part no."]
                        ////        jNew.StartDate = _StartDate;

                        ////        jNew.Quantity = decimal.Parse(dataGridView_Part["จำนวน", i].Value.ToString());//dr["จำนวน"]   
                        ////        jNew.PricesList = decimal.Parse(dataGridView_Part["PricesList", i].Value.ToString());
                        ////        jNew.Prices = decimal.Parse(dataGridView_Part["ราคา/หน่วย", i].Value.ToString());
                        ////        jNew.SumPrices = decimal.Parse(dataGridView_Part["รวม", i].Value.ToString());
                        ////        jNew.SumVAT = decimal.Parse(dataGridView_Part["ภาษี", i].Value.ToString());
                        ////        jNew.SumTotalPrices = decimal.Parse(dataGridView_Part["รวมสุทธิ", i].Value.ToString());


                        ////        jNew.Discount = decimal.Parse(dataGridView_Part["Discount", i].Value.ToString());
                        ////        jNew.VAT = decimal.Parse(dataGridView_Part["VAT", i].Value.ToString());
                        ////        jNew.ControlFixAssetNo = dataGridView_Part["FixAssetNo", i].Value.ToString();

                        ////        jNew.StatusSpare = _StatusSpare;//dr["สถานะ"]
                        ////        jNew.TRA_ID = dataGridView_Part["Type", i].Value.ToString();//dr["TranType"]
                        ////        jNew.Comment = dataGridView_Part["Comment", i].Value == null ? null + (IsQT == false ? "#QT=None" : "") : dataGridView_Part["Comment", i].Value.ToString() + (IsQT == false ? "#QT=None" : "");//dr["Comment"]                         
                        ////        jNew.Enddate = tmpEndDate;
                        ////        jNew.EMP_ID = UserInfo.UserId;

                        ////        jNew.FAI_ID = dataGridView_Part["FAI_ID", i].Value == null ? null : dataGridView_Part["FAI_ID", i].Value.ToString();

                        ////        //20150709 ใช้ process แปลง jNew.Location = dataGridView_Part["Location", i].Value == null ? null : dataGridView_Part["Location", i].Value.ToString();

                        ////        //===========20150709=============================
                        ////        string _vLocation = dataGridView_Part["Location", i].Value.ToString().Trim();

                        ////        int _cLocation = 0;

                        ////        if (dataGridView_Part["Location", i].Value != null)
                        ////            _cLocation = dataGridView_Part["Location", i].Value.ToString().Trim().Length;

                        ////        if ((_cLocation > 2 || _vLocation == null))
                        ////        {
                        ////            //_vLocation
                        ////            jNew.Location = "0";
                        ////        }
                        ////        else
                        ////        {
                        ////            var qLocate = (from t in db.Locations
                        ////                           where Convert.ToString(t.LOC_ID) == _vLocation
                        ////                           select t).FirstOrDefault();
                        ////            if (qLocate == null)
                        ////            {
                        ////                jNew.Location = "0";
                        ////            }
                        ////            else
                        ////            {
                        ////                jNew.Location = Convert.ToString(qLocate.LOC_ID);
                        ////            }
                        ////        }
                        ////        //===========20150709=============================




                        ////        jNew.SerialNumber = dataGridView_Part["S/N", i].Value == null ? null : dataGridView_Part["S/N", i].Value.ToString();

                        ////        jNew.IsCancel = dataGridView_Part["IsCancel", i].Value.ToString()[0];
                        ////        jNew.IsDownload = dataGridView_Part["IsDownload", i].Value.ToString()[0];
                        ////        jNew.IsPrices = (dataGridView_Part["isPrices", i].Value.ToString() == "0" ? false : true);
                        ////        jNew.ERP_StationCharge = _stCharge;

                        ////        //if (dataGridView_Part["ERP_SPA_ID", i].Value != null)
                        ////        //    jNew.ERP_SPA_ID = dataGridView_Part["ERP_SPA_ID", i].Value.ToString();

                        ////        //if (dataGridView_Part["ERP_PartName", i].Value != null)
                        ////        //    jNew.ERP_PartName = dataGridView_Part["ERP_PartName", i].Value.ToString();
                        ////        if (dataGridView_Part["ERP_SPA_ID", i].Value != DBNull.Value && !string.IsNullOrEmpty(dataGridView_Part["ERP_SPA_ID", i].Value.ToString()))
                        ////            jNew.ERP_SPA_ID = dataGridView_Part["ERP_SPA_ID", i].Value.ToString();
                        ////        else
                        ////            jNew.ERP_SPA_ID = null;

                        ////        if (dataGridView_Part["ERP_PartName", i].Value != DBNull.Value && !string.IsNullOrEmpty(dataGridView_Part["ERP_PartName", i].Value.ToString()))
                        ////            jNew.ERP_PartName = dataGridView_Part["ERP_PartName", i].Value.ToString();
                        ////        else
                        ////            jNew.ERP_PartName = null;

                        ////        //อะไหล่ลูกค้า
                        ////        jNew.IsCustomer = Convert.ToChar(_StatusSpareCus);

                        ////        //ERP_orderline_id
                        ////        if (dataGridView_Part["ERP_orderline_id", i].Value != DBNull.Value && !string.IsNullOrEmpty(dataGridView_Part["ERP_orderline_id", i].Value.ToString()))
                        ////            jNew.ERP_orderline_id = int.Parse(dataGridView_Part["ERP_orderline_id", i].Value.ToString());
                        ////        else
                        ////            jNew.ERP_orderline_id = null;



                        ////        //S/N Old
                        ////        if (dataGridView_Part["S/N Old", i].Value != DBNull.Value && !string.IsNullOrEmpty(dataGridView_Part["S/N Old", i].Value.ToString()))
                        ////            jNew.SerialNumber_Old = dataGridView_Part["S/N Old", i].Value.ToString();
                        ////        else
                        ////            jNew.SerialNumber_Old = null;


                        ////        //ตำแหน่งที่เปลี่ยน 
                        ////       /* if (dataGridView_Part["ตำแหน่งที่เปลี่ยน", i].Value != DBNull.Value && !string.IsNullOrEmpty(dataGridView_Part["ตำแหน่งที่เปลี่ยน", i].Value.ToString()))
                        ////            jNew.EQP_ID = int.Parse(dataGridView_Part["ตำแหน่งที่เปลี่ยน", i].Value.ToString());
                        ////        else
                        ////            jNew.EQP_ID = 99;
                        ////        */

                        ////        //20161116
                        ////        jNew.ReferPage = dataGridView_Part["Page/Item", i].Value.ToString();

                        ////        db.JOB_Detail_Spares.InsertOnSubmit(jNew);
                        ////        db.SubmitChanges();

                        ////        PartCount++;//ตรวจสอบการเพิ่ม Part เพื่อนำไปตรวจสอบ การออกรายงาน Services Report
                        ////        if (_StatusSpare == true)
                        ////            TypeJobRemain = true;

                        ////        #endregion

                        ////    }//  if ((from j in db.JOB_Detail_Spares

                        ////}//for (int i = 0; i <= dataGridView_Part.Rows.Count - 1; i++)

                        #endregion

                        #region Insert JOB_Detail_Spare Online
                        strPoint = "Insert JOB_Detail_Spare";

                        //20200915 DateTime _StartDate = tmpStartDate;
                        for (int i = 0; i <= dataGridView_Part.Rows.Count - 1; i++)
                        {
                            DateTime _StartDate = tmpStartDate;//20200915

                            string _SPA_ID = dataGridView_Part["Part no.", i].Value.ToString();

                            //Write Part cancel
                            #region Write Part cancel

                            //if (dataGridView_Part["IsDownload", i].Value.ToString() == "Y" && dataGridView_Part["IsCancel", i].Value.ToString() == "1")
                            //{
                            //    foreach (var partID in logCancelSPA_ID)
                            //    {
                            //        if (partID == dataGridView_Part["Part no.", i].Value.ToString())
                            //            partLogCancel(
                            //                dataGridView_Part["Part no.", i].Value.ToString(),
                            //                dataGridView_Part["Location", i].Value.ToString(),
                            //                dataGridView_Part["รหัสจุดเสีย", i].Value.ToString(),
                            //                dataGridView_Part["FAI_ID", i].Value.ToString(),
                            //                dataGridView_Part["Type", i].Value.ToString()
                            //                );
                            //    }
                            //}
                            #endregion

                            //ดึง StartDate ของเดิมมาใช้
                            //_StartDate = checkStartDate(i, _StartDate, _SPA_ID);
                            _StartDate = checkStartDate(_StartDate, _SPA_ID);

                            bool _StatusSpare = (dataGridView_Part["สถานะ", i].Value.ToString() == "ค้าง") ? false : true;//7dr["สถานะ"]

                            //อะไหล่ลูกค้า
                            string _StatusSpareCus = (dataGridView_Part["อะไหล่ลูกค้า", i].Value.ToString() == "Y") ? "9" : "0";


                            int xNO = int.Parse(dataGridView_Part["NO.", i].Value.ToString());

                            char _stCharge = dataGridView_Part["ออกบิลในนาม", i].Value.ToString() == "สถานี" ? '0' : '1';

                            if ((from j in db.JOB_Detail_Spares
                                 where j.JOB_ID == txtJobID.Text.Trim() && j.StartDate == _StartDate &&
                                     j.SPA_ID == _SPA_ID && j.NO == xNO
                                 select j).Count() > 0)
                            {

                                #region Edite JOB Part
                                var sp = (from j in db.JOB_Detail_Spares
                                          where j.JOB_ID == txtJobID.Text.Trim() && j.StartDate == _StartDate &&
                                              j.SPA_ID == _SPA_ID && j.NO == xNO
                                          select j).FirstOrDefault();

                                sp.POI_ID = dataGridView_Part["รหัสจุดเสีย", i].Value.ToString();//dr["รหัสจุดเสีย"]

                                //Quantity
                                sp.Quantity = decimal.Parse(dataGridView_Part["จำนวน", i].Value.ToString());//dr["จำนวน"]      
                                sp.Prices = decimal.Parse(dataGridView_Part["ราคา/หน่วย", i].Value.ToString());
                                //PricesList
                                sp.PricesList = decimal.Parse(dataGridView_Part["PricesList", i].Value.ToString());
                                sp.SumPrices = decimal.Parse(dataGridView_Part["รวม", i].Value.ToString());
                                sp.SumVAT = decimal.Parse(dataGridView_Part["ภาษี", i].Value.ToString());
                                sp.Prices = decimal.Parse(dataGridView_Part["ราคา/หน่วย", i].Value.ToString());
                                sp.SumTotalPrices = decimal.Parse(dataGridView_Part["รวมสุทธิ", i].Value.ToString());
                                //รวมสุทธิ
                                sp.Discount = decimal.Parse(dataGridView_Part["Discount", i].Value.ToString());
                                sp.VAT = decimal.Parse(dataGridView_Part["VAT", i].Value.ToString());
                                sp.ControlFixAssetNo = dataGridView_Part["FixAssetNo", i].Value.ToString();

                                sp.StatusSpare = _StatusSpare;//dr["สถานะ"]
                                sp.TRA_ID = dataGridView_Part["Type", i].Value.ToString();//dr["TranType"]
                                sp.Comment = dataGridView_Part["Comment", i].Value == null ? null + (IsQT == false ? "#QT=None" : "") : dataGridView_Part["Comment", i].Value.ToString() + (IsQT == false ? "#QT=None" : "");//dr["Comment"]                         
                                sp.Enddate = tmpEndDate;
                                sp.EMP_ID = UserInfo.UserId;
                                sp.FAI_ID = dataGridView_Part["FAI_ID", i].Value == null ? null : dataGridView_Part["FAI_ID", i].Value.ToString();
                                //20150709 ใช้ process แปลง sp.Location = dataGridView_Part["Location", i].Value == null ? null : dataGridView_Part["Location", i].Value.ToString();
                                //===========20150709=============================
                                string _vLocation = dataGridView_Part["Location", i].Value.ToString().Trim();

                                int _cLocation = 0;

                                if (dataGridView_Part["Location", i].Value != null)
                                    _cLocation = dataGridView_Part["Location", i].Value.ToString().Trim().Length;

                                if ((_cLocation > 2 || _vLocation == null))
                                {
                                    //_vLocation
                                    sp.Location = "0";
                                }
                                else
                                {
                                    var qLocate = (from t in db.Locations
                                                   where Convert.ToString(t.LOC_ID) == _vLocation
                                                   select t).FirstOrDefault();
                                    if (qLocate == null)
                                    {
                                        sp.Location = "0";
                                    }
                                    else
                                    {
                                        sp.Location = Convert.ToString(qLocate.LOC_ID);
                                    }
                                }
                                //===========20150709=============================


                                sp.SerialNumber = dataGridView_Part["S/N", i].Value == null ? null : dataGridView_Part["S/N", i].Value.ToString();

                                sp.IsCancel = dataGridView_Part["IsCancel", i].Value.ToString()[0];
                                sp.IsDownload = dataGridView_Part["IsDownload", i].Value.ToString()[0];
                                sp.IsPrices = (dataGridView_Part["isPrices", i].Value.ToString() == "0" ? false : true);

                                sp.ERP_StationCharge = _stCharge;

                                //if (dataGridView_Part["ERP_SPA_ID", i].Value != null)
                                //    sp.ERP_SPA_ID = dataGridView_Part["ERP_SPA_ID", i].Value.ToString();

                                //if (dataGridView_Part["ERP_PartName", i].Value != null)
                                //    sp.ERP_PartName = dataGridView_Part["ERP_PartName", i].Value.ToString();

                                if (dataGridView_Part["ERP_SPA_ID", i].Value != DBNull.Value && !string.IsNullOrEmpty(dataGridView_Part["ERP_SPA_ID", i].Value.ToString()))
                                    sp.ERP_SPA_ID = dataGridView_Part["ERP_SPA_ID", i].Value.ToString();
                                else
                                    sp.ERP_SPA_ID = null;

                                if (dataGridView_Part["ERP_PartName", i].Value != DBNull.Value && !string.IsNullOrEmpty(dataGridView_Part["ERP_PartName", i].Value.ToString()))
                                    sp.ERP_PartName = dataGridView_Part["ERP_PartName", i].Value.ToString();
                                else
                                    sp.ERP_PartName = null;

                                //อะไหล่ลูกค้า
                                sp.IsCustomer = Convert.ToChar(_StatusSpareCus);

                                //ERP_orderline_id
                                if (dataGridView_Part["ERP_orderline_id", i].Value != DBNull.Value && !string.IsNullOrEmpty(dataGridView_Part["ERP_orderline_id", i].Value.ToString()))
                                    sp.ERP_orderline_id = int.Parse(dataGridView_Part["ERP_orderline_id", i].Value.ToString());
                                else
                                    sp.ERP_orderline_id = null;


                                //20140905
                                sp.SerialNumber_Old = dataGridView_Part["S/N Old", i].Value.ToString();

                                //20141021 
                                //20141127ยกเลิก sp.EQP_ID = int.Parse(dataGridView_Part["ตำแหน่งที่เปลี่ยน", i].Value.ToString());

                                //20161116
                                sp.ReferPage = dataGridView_Part["Page/Item", i].Value.ToString();

                                db.SubmitChanges();

                                #endregion


                            }
                            else
                            {
                                #region New JOB Part
                                JOB_Detail_Spare jNew = new JOB_Detail_Spare();


                                jNew.NO = xNO;//i + 1,
                                jNew.JOB_ID = txtJobID.Text.Trim();
                                jNew.POI_ID = dataGridView_Part["รหัสจุดเสีย", i].Value.ToString();//dr["รหัสจุดเสีย"]
                                jNew.SPA_ID = dataGridView_Part["Part no.", i].Value.ToString();//dr["Part no."]
                                jNew.StartDate = _StartDate;

                                jNew.Quantity = decimal.Parse(dataGridView_Part["จำนวน", i].Value.ToString());//dr["จำนวน"]   
                                jNew.PricesList = decimal.Parse(dataGridView_Part["PricesList", i].Value.ToString());
                                jNew.Prices = decimal.Parse(dataGridView_Part["ราคา/หน่วย", i].Value.ToString());
                                jNew.SumPrices = decimal.Parse(dataGridView_Part["รวม", i].Value.ToString());
                                jNew.SumVAT = decimal.Parse(dataGridView_Part["ภาษี", i].Value.ToString());
                                jNew.SumTotalPrices = decimal.Parse(dataGridView_Part["รวมสุทธิ", i].Value.ToString());


                                jNew.Discount = decimal.Parse(dataGridView_Part["Discount", i].Value.ToString());
                                jNew.VAT = decimal.Parse(dataGridView_Part["VAT", i].Value.ToString());
                                jNew.ControlFixAssetNo = dataGridView_Part["FixAssetNo", i].Value.ToString();

                                jNew.StatusSpare = _StatusSpare;//dr["สถานะ"]
                                jNew.TRA_ID = dataGridView_Part["Type", i].Value.ToString();//dr["TranType"]
                                jNew.Comment = dataGridView_Part["Comment", i].Value == null ? null + (IsQT == false ? "#QT=None" : "") : dataGridView_Part["Comment", i].Value.ToString() + (IsQT == false ? "#QT=None" : "");//dr["Comment"]                         
                                jNew.Enddate = tmpEndDate;
                                jNew.EMP_ID = UserInfo.UserId;

                                jNew.FAI_ID = dataGridView_Part["FAI_ID", i].Value == null ? null : dataGridView_Part["FAI_ID", i].Value.ToString();

                                //20150709 ใช้ process แปลง jNew.Location = dataGridView_Part["Location", i].Value == null ? null : dataGridView_Part["Location", i].Value.ToString();

                                //===========20150709=============================
                                string _vLocation = dataGridView_Part["Location", i].Value.ToString().Trim();

                                int _cLocation = 0;

                                if (dataGridView_Part["Location", i].Value != null)
                                    _cLocation = dataGridView_Part["Location", i].Value.ToString().Trim().Length;

                                if ((_cLocation > 2 || _vLocation == null))
                                {
                                    //_vLocation
                                    jNew.Location = "0";
                                }
                                else
                                {
                                    var qLocate = (from t in db.Locations
                                                   where Convert.ToString(t.LOC_ID) == _vLocation
                                                   select t).FirstOrDefault();
                                    if (qLocate == null)
                                    {
                                        jNew.Location = "0";
                                    }
                                    else
                                    {
                                        jNew.Location = Convert.ToString(qLocate.LOC_ID);
                                    }
                                }
                                //===========20150709=============================




                                jNew.SerialNumber = dataGridView_Part["S/N", i].Value == null ? null : dataGridView_Part["S/N", i].Value.ToString();

                                jNew.IsCancel = dataGridView_Part["IsCancel", i].Value.ToString()[0];
                                jNew.IsDownload = dataGridView_Part["IsDownload", i].Value.ToString()[0];
                                jNew.IsPrices = (dataGridView_Part["isPrices", i].Value.ToString() == "0" ? false : true);
                                jNew.ERP_StationCharge = _stCharge;

                                //if (dataGridView_Part["ERP_SPA_ID", i].Value != null)
                                //    jNew.ERP_SPA_ID = dataGridView_Part["ERP_SPA_ID", i].Value.ToString();

                                //if (dataGridView_Part["ERP_PartName", i].Value != null)
                                //    jNew.ERP_PartName = dataGridView_Part["ERP_PartName", i].Value.ToString();
                                if (dataGridView_Part["ERP_SPA_ID", i].Value != DBNull.Value && !string.IsNullOrEmpty(dataGridView_Part["ERP_SPA_ID", i].Value.ToString()))
                                    jNew.ERP_SPA_ID = dataGridView_Part["ERP_SPA_ID", i].Value.ToString();
                                else
                                    jNew.ERP_SPA_ID = null;

                                if (dataGridView_Part["ERP_PartName", i].Value != DBNull.Value && !string.IsNullOrEmpty(dataGridView_Part["ERP_PartName", i].Value.ToString()))
                                    jNew.ERP_PartName = dataGridView_Part["ERP_PartName", i].Value.ToString();
                                else
                                    jNew.ERP_PartName = null;

                                //อะไหล่ลูกค้า
                                jNew.IsCustomer = Convert.ToChar(_StatusSpareCus);

                                //ERP_orderline_id
                                if (dataGridView_Part["ERP_orderline_id", i].Value != DBNull.Value && !string.IsNullOrEmpty(dataGridView_Part["ERP_orderline_id", i].Value.ToString()))
                                    jNew.ERP_orderline_id = int.Parse(dataGridView_Part["ERP_orderline_id", i].Value.ToString());
                                else
                                    jNew.ERP_orderline_id = null;



                                //S/N Old
                                if (dataGridView_Part["S/N Old", i].Value != DBNull.Value && !string.IsNullOrEmpty(dataGridView_Part["S/N Old", i].Value.ToString()))
                                    jNew.SerialNumber_Old = dataGridView_Part["S/N Old", i].Value.ToString();
                                else
                                    jNew.SerialNumber_Old = null;


                                //ตำแหน่งที่เปลี่ยน 
                                /* if (dataGridView_Part["ตำแหน่งที่เปลี่ยน", i].Value != DBNull.Value && !string.IsNullOrEmpty(dataGridView_Part["ตำแหน่งที่เปลี่ยน", i].Value.ToString()))
                                     jNew.EQP_ID = int.Parse(dataGridView_Part["ตำแหน่งที่เปลี่ยน", i].Value.ToString());
                                 else
                                     jNew.EQP_ID = 99;
                                 */

                                //20161116
                                jNew.ReferPage = dataGridView_Part["Page/Item", i].Value.ToString();

                                db.JOB_Detail_Spares.InsertOnSubmit(jNew);
                                db.SubmitChanges();

                                PartCount++;//ตรวจสอบการเพิ่ม Part เพื่อนำไปตรวจสอบ การออกรายงาน Services Report
                                if (_StatusSpare == true)
                                    TypeJobRemain = true;

                                #endregion

                            }//  if ((from j in db.JOB_Detail_Spares


                            ////20200915 ===== set dataGridView_tmpPart
                            //dataGridView_tmpPart.DataSource = null;
                            //dataGridView_tmpPart.Refresh(); 
                            //var part = from t in db.JOB_Detail_Spares where t.JOB_ID.Trim() == txtJobID.Text.Trim() select t;
                            //if (part.Count() > 0)
                            //{
                            //    dataGridView_tmpPart.DataSource = part.ToList();
                            //    dataGridView_tmpPart.Refresh();
                            //    cobEndDate.Items.Clear();
                            //}// var part 
                            //else
                            //{
                            //    dataGridView_tmpPart.DataSource = null;
                            //    dataGridView_tmpPart.Refresh();
                            //}
                            ////==============================

                        }//for (int i = 0; i <= dataGridView_Part.Rows.Count - 1; i++)




                        #endregion

                        //======================================
                        // Insert JOB_Detail
                        //======================================
                        #region Insert JOB_Detail
                        strPoint = "Insert JOB_Detail";
                        if (qjd != null)
                        {
                            qjd.Problem_Detail = vProblem_Detail;// txtProblem_Detail.Text.Trim() == "" ? null : txtProblem_Detail.Text.Trim(); //สาเหตุ
                            qjd.Resole_Detail = txtResolve_Detail.Text.Trim() == "" ? null : txtResolve_Detail.Text.Trim();//วิธีการแก้ไข

                            if (TypeJobRemain == true)
                                qjd.TRE_ID = "02";//02	ค้างอะไหล่
                            else
                                qjd.TRE_ID = "03";// Job ค้าง


                            if (RadioButtonList_JobStatus2.Checked)//CloseJOB
                                qjd.JOBS_ID = "99";//ปิด JOB 
                            else
                                qjd.JOBS_ID = "07";//ค้าง JOB

                            qjd.EMP_ID = UserInfo.UserId;
                            qjd.EndDate = tmpEndDate;

                            //20150713
                            qjd.TCO_ID = "04";//ประเภทการปิด JOB {04 = เข้า Site (ผ่าน web)}

                            //20170215
                            qjd.RecDate_Endate = Server_date;//เวลา server


                            db.SubmitChanges();
                        }

                        #endregion

                        //= hold this process (edit)==20200626
                        //if (LstChecklogEditDel.Count > 0 )//20200707
                        if (LstChecklogEditDel != null)
                        {
                            if (LstChecklogEditDel.Count > 0)
                            {
                                foreach (var itemLS in LstChecklogEditDel)
                                {
                                    var qchkPartOnServer = (from jd in db.JOB_Detail_Spares
                                                            where
                                                            jd.JOB_ID == itemLS._LJOBID &&
                                                            jd.POI_ID == itemLS._Lpoi &&
                                                            jd.Location == itemLS._Llocat &&
                                                            jd.SerialNumber == itemLS._Lsn &&
                                                            jd.FAI_ID == itemLS._Lfai &&
                                                            jd.SPA_ID == itemLS._Lspa &&
                                                            jd.TRA_ID == itemLS._Ltra &&
                                                            jd.SumTotalPrices == decimal.Parse(itemLS._Lprice) &&
                                                            jd.Enddate.Value.Date == itemLS._Lend.Date &&
                                                            jd.Enddate.Value.Hour == itemLS._Lend.Hour &&
                                                            jd.Enddate.Value.Minute == itemLS._Lend.Minute &&
                                                            jd.Enddate.Value.Second == itemLS._Lend.Second

                                                            select jd).FirstOrDefault();

                                    if (qchkPartOnServer != null)
                                    {
                                        string wordingLog = "Edit_spa" + itemLS._Lspa + "_poi" + itemLS._Lpoi + "_fai" + itemLS._Lfai + "_loc" + itemLS._Llocat + "_SN" + itemLS._Lsn + "_tra" + itemLS._Ltra + "_Price" + itemLS._Lprice + "_enddate" + itemLS._Lend + "";

                                        var sql_logEdit = "";
                                        sql_logEdit = "Insert into [LogSystem] ([TableName] ,[DataID] ,[Detail] ,[UserID] ,[IPAddress]) " +
                                                   " values ('JOB_Detail_Spare'  ,'" + itemLS._LJOBID + "' ,'" + wordingLog + "' ,'" + UserInfo.UserId + "' ,CONVERT(char(15), CONNECTIONPROPERTY('client_net_address')))";
                                        db.ExecuteCommand(sql_logEdit);


                                    }


                                }
                            }
                        }

                        //===========


                        ////===20200313===
                        //var qUserVers = (from t in db.Users where t.EMP_ID == UserInfo.UserId select t).First();
                        //if (qUserVers != null)
                        //{
                        //    qUserVers.SMSVersion = UserInfo.MySMSMINIVersion;
                        //    qUserVers.SMSVersionDate = tmpEndDate;
                        //    db.SubmitChanges();

                        //}
                        ////===============


                        DateTime dtxc = tmpEndDate;


                        // S/N Tracking
                        //20191129 //updateSN_byStation(db);
                        //updateSN_byStation();
                        updateSN_byStation(db, tmpEndDate);

                        db.Transaction.Commit();


                        setDtFailure();
                        setDtPart();

                        FailureBinding();
                        PartBindind();

                        ClearControl();

                        ClearTextFail();
                        ClearPartText();

                        butttonEnable(false, false, true, true, true);

                        //20150629
                        //chkEnableSVReport(txtJobID.Text.Trim());
                        toolStripButton_Print.Enabled = true;

                        //20191219 print SV ล่าสุดเหมือน ปิดงาน Offline
                        //string at = tmpEndDate;
                        //DateTime dt1 = DateTime.Parse(cobEndDate.SelectedItem.ToString());//Convert.ToDateTime(tmpEndDate);

                        DateTime dt1 = tmpEndDate;//Convert.ToDateTime(tmpEndDate);

                        //DateTime dt1 = Convert.ToDateTime(tmpEndDate); //tmpEndDate;//Convert.ToDateTime(tmpEndDate.ToString());

                        stmpEndDate = dt1.ToString("yyyyMMdd", new System.Globalization.CultureInfo("en-US"));
                        stmpEndTime = dt1.ToString("HH:mm:ss", new System.Globalization.CultureInfo("en-US"));

                        //stmpEndDate = dt1.ToString("yyyyMMdd");
                        //stmpEndTime = dt1.ToString("HH:mm:ss");



                        //DateTime dt1 = Convert.ToDateTime(cobEndDate.SelectedItem.ToString());

                        //tmpEndDate = dt1.ToString("yyyyMMdd", new System.Globalization.CultureInfo("en-US"));
                        //tmpEndTime = dt1.ToString("HH:mm:ss", new System.Globalization.CultureInfo("en-US"));
                        //panel_selectEndDate.Visible = false;
                    }

                    catch (Exception ex)
                    {
                        closeProgress();
                        db.Transaction.Rollback();
                        PartCount = 0;
                        MessageBox.Show("เกิดข้อผิดพลาด Error: ในการบันทึกข้อมูล  หมายเลข JOB: " + txtJobID.Text.Trim() +
                            Environment.NewLine + " จุด " + strPoint +
                            Environment.NewLine + " Error: " + ex.Message, " กรุณาติดต่อผู้ดูแลระบบ...",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                }
            }

            catch (System.Exception ex)
            {
                closeProgress();
                MessageBox.Show("เกิดข้อผิดพลาด Error:ในการบันทึกข้อมูล  หมายเลข JOB: " + txtJobID.Text.Trim() +
                    Environment.NewLine + " จุด " + strPoint, " กรุณาติดต่อผู้ดูแลระบบ..." +
                    Environment.NewLine + "Error:= " + ex.Message,
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        //private void updateSN_byStation(DAL.SMSManage.SMSManageDataContext db)
        //20191129 
        //private void updateSN_byStation(DAL.SMSManage.SMSManageDataContext dc)
        //private void updateSN_byStation()
        //20210217 private void updateSN_byStation(DAL.SMSManage.SMSManageDataContext dc)
        private void updateSN_byStation(DAL.SMSManage.SMSManageDataContext dc, DateTime dtToCFFFA)
        {
            //using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            //{
            string _msg = "";
            string stOld = "00000000";
            string stNew = "00000000";
            string vanID = "0000";

            for (int i = 0; i <= dataGridView_FailPoint.Rows.Count - 1; i++)
            {

                string SerialNumber = dataGridView_FailPoint["S/N(Old)", i].Value.ToString();
                string SerialNumber_new = dataGridView_FailPoint["S/N(New)", i].Value.ToString();

                int vFCAT_ID = int.Parse(dataGridView_FailPoint["FCAT_ID", i].Value.ToString());
                int vFBA_ID = int.Parse(dataGridView_FailPoint["FBA_ID", i].Value.ToString());
                int vFMD_ID = int.Parse(dataGridView_FailPoint["FMD_ID", i].Value.ToString());
                //20200507 int vLOC_ID = int.Parse(dataGridView_FailPoint["Location", i].Value.ToString());
                //20200512 int vLOC_ID = dataGridView_FailPoint["Location", i].Value.ToString() == "None"?0:int.Parse(dataGridView_FailPoint["Location", i].Value.ToString());

                if ((SerialNumber != "None" && SerialNumber_new != "None") && (SerialNumber != "ไม่ระบุ..." && SerialNumber_new != "ไม่ระบุ..."))
                {
                    //==20200512 
                    int vLOC_ID = 0;
                    //vLOC_ID = dataGridView_FailPoint["Location", i].Value.ToString() == "None" ? 0 : int.Parse(dataGridView_FailPoint["Location", i].Value.ToString());
                    vLOC_ID = dataGridView_FailPoint["Location", i].Value.ToString() == "None" ? 0 : int.Parse(dataGridView_FailPoint["Location", i].Value.ToString());
                    //=======
                    //---process upload               
                    DateTime getdate = dc.ExecuteQuery<DateTime>("select getdate() ")
                    .FirstOrDefault();

                    #region FFixedAsset_SerialNumber_Tracking

                    //ดึงข้อมูลสน. เก่า (STA_ID_Old)
                    _msg = "ดึงข้อมูลสน. เก่า (STA_ID_Old)";
                    string _jobID = txtJobID.Text.Trim();


                    var staold = dc.JOB_Failure_Details
                        .Where(t => t.SerialNumber_New == SerialNumber_new)
                        .OrderByDescending(t => t.trandate).FirstOrDefault();

                    if (staold != null)
                        _jobID = staold.JOB_ID;
                    var _staold = dc.JOBs.Where(t => t.JOB_ID == _jobID).FirstOrDefault();

                    if (_staold != null)
                        stOld = _staold.STA_ID;
                    //string stOld = string.Empty;
                    //string stNew = string.Empty;

                    //ดึงข้อมูลสน. ใหม่ (STA_ID_New)
                    _msg = "ดึงข้อมูลสน. ใหม่ (STA_ID_New)";
                    var _staNew = dc.JOBs
                        .Where(t => t.JOB_ID == txtJobID.Text.Trim())
                        .FirstOrDefault();

                    if (_staNew != null)
                    {
                        stNew = _staNew.STA_ID;
                        vanID = _staNew.EMP_ID3;
                    }


                    //if (item.FCAT_ID != 0)//20150114
                    //{
                    string _traStatus = "92";//มีการเปลี่ยน SN  //20210121 "12";//มีการเปลี่ยน SN
                    if (SerialNumber == SerialNumber_new)
                        _traStatus = "91";//ไม่มีการเปลี่ยน SN  //20210121 "11";//ไม่มีการเปลี่ยน SN

                    var snMas = dc.FFixedAsset_SerialNumber_Pools
                        .Where(t => t.SerialNumber == SerialNumber ||
                            t.SerialNumber == SerialNumber_new).FirstOrDefault();
                    if (snMas == null)
                        _traStatus = "93";//เปลี่ยน SN ที่ไม่อยู่ใน SN Master //20210121 "88";//เปลี่ยน SN ที่ไม่อยู่ใน SN Master


                    if ((dc.FFixedAsset_SerialNumber_Trackings
                        .Where(t =>
                            t.SerialNumber_Old == SerialNumber &&
                            t.SerialNumber_New == SerialNumber_new &&
                            t.JOB_ID == txtJobID.Text.Trim() &&
                            t.STA_ID_Old == stOld &&
                            t.STA_ID_New == stNew
                        )).Count() <= 0)
                    {
                        var newSNTrack = new DAL.SMSManage.FFixedAsset_SerialNumber_Tracking//DAL.SMSManage.FFixedAsset_SerialNumber_Tracking
                        {
                            TrackingDate = getdate,
                            SerialNumber_Old = SerialNumber,
                            SerialNumber_New = SerialNumber_new,
                            JOB_ID = txtJobID.Text.Trim(),
                            STA_ID_Old = stOld,
                            STA_ID_New = stNew,
                            VAN_ID = vanID,
                            SerialNumber_Status = _traStatus,//"12",
                        };

                        _msg = "Insert New S/N Tracking";
                        dc.FFixedAsset_SerialNumber_Trackings.InsertOnSubmit(newSNTrack);
                        dc.SubmitChanges();

                    }

                    //}//if cat_id
                    #endregion



                    /* //20191127 flow เก่า offline ปิดงานที่เครื่องช่าง
                //20141223 if ((SerialNumber != "None" && SerialNumber != "None") && (SerialNumber != "ไม่ระบุ..." && SerialNumber != "ไม่ระบุ..."))
                 //20191126 ย้ายขึ้นข้างบน if ((SerialNumber != "None" && SerialNumber_new != "None") && (SerialNumber != "ไม่ระบุ..." && SerialNumber_new != "ไม่ระบุ..."))
                 //20191126 ย้ายขึ้นข้างบน    {

                    
                        //20141224 เช็คข้อมุล SN ที่ user key เองมีใน pool หรือไม่
                       var qchkPool = from a in db.FFixedAsset_SerialNumber_Pools
                                      where a.SerialNumber == SerialNumber_new
                                    select a;

                       if (qchkPool.Count() > 0)
                       {

                           //ถ้ามีการเปลี่ยน Serial
                           if (SerialNumber != SerialNumber_new)
                           {

                               #region FFixedAsset_Station_Details


                               var q = from t in db.FFixedAsset_Station_Details
                                       where t.STA_ID == tmpSTAID &&
                                               t.SerialNumber == SerialNumber
                                       select t;

                               if (q.Count() > 0)
                               {
                                   var delSN = q.Where(t => t.SerialNumber == SerialNumber
                                              
                                                 &&
                                               t.FCAT_ID == t.FCAT_ID &&
                                              t.FMD_ID == t.FMD_ID &&
                                              t.STA_ID == t.STA_ID
                                                
                                             //  &&
                                             //  t.FCAT_ID == tmpSN.FCAT_ID) &&
                                             // t.FMD_ID == tmpSN.FMD_ID &&
                                             // t.STA_ID == tmpSN.STA_ID

                                             ).FirstOrDefault();
                                   if (delSN != null)
                                   {
                                       var tmpSN = delSN;
                                       db.FFixedAsset_Station_Details.DeleteOnSubmit(delSN);

                                       var newSN = new FFixedAsset_Station_Detail
                                       {
                                           SerialNumber = SerialNumber_new,
                                           FCAT_ID = tmpSN.FCAT_ID,
                                           FBA_ID = tmpSN.FBA_ID,
                                           FMD_ID = tmpSN.FMD_ID,
                                           STA_ID = tmpSN.STA_ID,
                                           OwnerShip = tmpSN.OwnerShip,
                                           InstallDate = tmpSN.InstallDate,
                                           Detail = tmpSN.Detail,
                                           //20141208
                                           LOC_ID = tmpSN.LOC_ID
                                       };

                                       db.FFixedAsset_Station_Details.InsertOnSubmit(newSN);
                                       db.SubmitChanges();
                                   }
                             
                               //เปลี่ยน 20150105   }

                               #endregion

                               #region Update SN Pool


                               var qpool = db.FFixedAsset_SerialNumber_Pools
                                   .Where(t => t.SerialNumber == SerialNumber_new).FirstOrDefault();

                               if (qpool != null)
                               {
                                   qpool.StatusUse = "11";//ใช้งานแล้ว
                                   db.SubmitChanges();
                               }
                               //20140814 ก่อนตรวจสอบเรื่อง ตู้จ่าย
                               var qpool2 = db.FFixedAsset_SerialNumber_Pools
                                  .Where(t => t.SerialNumber == SerialNumber).FirstOrDefault();

                               if (qpool2 != null)
                               {
                                   //20140814 ก่อนตรวจสอบเรื่อง ตู้จ่าย
                                   //qpool2.StatusUse = "12";//รอซ่อม
                                   //db.SubmitChanges();                          

                                   var chkpool = ((from p in db.FFixedAsset_SerialNumber_Pools
                                                   join m in db.FFixedAsset_Models on p.FMD_ID equals m.FMD_ID
                                                   join b in db.FFixedAsset_Brands on m.FBA_ID equals b.FBA_ID
                                                   where m.FMD_ID.ToString().Contains(qpool2.FMD_ID.ToString())
                                                   && p.SerialNumber == qpool2.SerialNumber
                                                   && (b.FBA_ID.ToString() == "1"
                                                   || b.FBA_ID.ToString() == "2"
                                                   || b.FBA_ID.ToString() == "3"
                                                   || b.FBA_ID.ToString() == "4")
                                                   select p).FirstOrDefault());

                                   if (chkpool != null)
                                   {
                                       chkpool.StatusUse = "00";//พร้อมใช้
                                       db.SubmitChanges();
                                   }
                                   else
                                   {
                                       qpool2.StatusUse = "12";//รอซ่อม
                                       db.SubmitChanges();
                                   }
                               }
                              }//เปลี่ยน 20150105
                               //คืนค่า ถ้ามีการยกเลิก
                               foreach (var item in lsSNCancel)
                               {
                                   var resetSNCanOld = db.FFixedAsset_SerialNumber_Pools
                                  .Where(t => t.SerialNumber == item.SNold).FirstOrDefault();
                                   if (resetSNCanOld != null)
                                   {
                                       resetSNCanOld.StatusUse = "11";
                                       db.SubmitChanges();
                                   }

                                   var resetSNCanNew = db.FFixedAsset_SerialNumber_Pools
                                 .Where(t => t.SerialNumber == item.SNNew).FirstOrDefault();
                                   if (resetSNCanNew != null)
                                   {
                                       resetSNCanNew.StatusUse = "00";
                                       db.SubmitChanges();
                                   }
                               }
                               #endregion
                           }
                           else
                           {
                               var qpool = db.FFixedAsset_SerialNumber_Pools
                                      .Where(t => t.SerialNumber == SerialNumber_new).FirstOrDefault();

                               if (qpool != null)
                               {
                                   qpool.StatusUse = "11";//ใช้งานแล้ว
                                   db.SubmitChanges();
                               }
                           }
                       }//end chkpool */


                    var qchkPool = from a in dc.FFixedAsset_SerialNumber_Pools
                                   where a.SerialNumber == SerialNumber_new
                                   select a;

                    if (qchkPool.Count() > 0)
                    {
                        //ถ้ามีการเปลี่ยน Serial
                        if (SerialNumber != SerialNumber_new)
                        {

                            #region Delete SN Old

                            int catID = vFCAT_ID;//item.FCAT_ID.Value;
                            int fbaID = vFBA_ID;//item.FBA_ID.Value;
                            int fmdID = vFMD_ID;//item.FMD_ID.Value;
                            string staID = stOld;

                            //20150113
                            int fLoc_id = vLOC_ID;//item.LOC_ID.Value;



                            //1. Delete SN Old 
                            _msg = "Delete SN Old";
                            var snOld = dc.FFixedAsset_Station_Details
                                .Where(t => t.SerialNumber == SerialNumber)
                                .FirstOrDefault();

                            if (snOld != null)
                            {
                                if (catID != 0)//ต้องมีอยู่จริงในระบบ ไม่ใช่การ key //20150114
                                {
                                    //===20210216
                                    //string ax = tmpEndDate;
                                    DateTime _vEnddateToCfFFA;
                                    _vEnddateToCfFFA = dtToCFFFA;

                                    //if (editMode != "job_edit")
                                    //{
                                    //    _vEnddateToCfFFA = DateTime.Parse(tmpEndDate);
                                    //}
                                    //else
                                    //{
                                    //    _vEnddateToCfFFA = DateTime.Parse(_pEnddate);
                                    //}


                                    ////DateTime dtxc = tmpEndDate;

                                    //if (editMode != "job_edit")
                                    //{
                                    //    _vEnddateToCfFFA = tmpEndDate;
                                    //}
                                    //else
                                    //{
                                    //    _vEnddateToCfFFA = DateTime.Parse(_pEnddate);
                                    //}
                                    //===========
                                    //===20210129========================================                             
                                    _msg = " Insert SN New";
                                    if (catID != 0)
                                    {
                                        if ((dc.FFixedAsset_Station_Details
                                           .Where(t => t.SerialNumber == SerialNumber_new &&
                                            /*20210309
                                             t.FCAT_ID == catID &&
                                             t.FBA_ID == fbaID &&
                                             t.FMD_ID == fmdID &&
                                             */
                                            //20150113 t.STA_ID == staID)).Count() <= 0)
                                            t.STA_ID == stNew)).Count() <= 0)
                                        {

                                            var snNewFmdID = (from t in dc.FFixedAsset_SerialNumber_Pools
                                                              join m in dc.FFixedAsset_Models on t.FMD_ID equals m.FMD_ID
                                                              join b in dc.FFixedAsset_Brands on m.FBA_ID equals b.FBA_ID
                                                              where t.SerialNumber == SerialNumber_new
                                                              select new
                                                              {
                                                                  t.FCAT_ID,
                                                                  m.FMD_ID,
                                                                  b.FBA_ID
                                                              })
                                              .FirstOrDefault();
                                            if (snNewFmdID != null)
                                            {
                                                var snNew = new DAL.SMSManage.FFixedAsset_Station_Detail()
                                                {
                                                    SerialNumber = SerialNumber_new,
                                                    /*20210309 FCAT_ID = snNewFmdID.FCAT_ID,//catID,
                                                    FBA_ID = snNewFmdID.FBA_ID,//fbaID,
                                                    FMD_ID = snNewFmdID.FMD_ID,//fmdID,*/
                                                    //20150113 STA_ID = staID,
                                                    STA_ID = stNew,
                                                    OwnerShip = "FLO",
                                                    InstallDate = snOld.InstallDate,//20210201
                                                    WarrantDate = snOld.WarrantDate,//20210201
                                                    LOC_ID = fLoc_id,
                                                    //20210216 Detail = snOld.Detail,//20210202

                                                    //==20210216
                                                    TranDate = _vEnddateToCfFFA,
                                                    TranEMP = UserInfo.UserId,
                                                    Detail = string.IsNullOrEmpty(snOld.Detail) ? "ยังไม่มีรายละเอียด" : snOld.Detail,
                                                    IsConfirm = true,
                                                    ConfirmDate = _vEnddateToCfFFA,
                                                    ConfirmEMP = UserInfo.UserId,
                                                    //====
                                                };

                                                dc.FFixedAsset_Station_Details.InsertOnSubmit(snNew);
                                                dc.SubmitChanges();
                                            }
                                        }
                                    } //if (catID != 0) insert new

                                    //======================================================

                                    //==20190516
                                    var ld = new DAL.SMSManage.Log_DeleteDatabyProgram
                                    {
                                        Trandate = getdate,
                                        DataID = SerialNumber,//txtJOB_ID.Text.Trim(),
                                        TableName = "FFixedAsset_Station_Detail",
                                        Menu = "Close JOB ",
                                        Detail = "Delete SN_Old " + SerialNumber + " STA_ID " + snOld.STA_ID + " for SN_New " + stNew + " JOB " + txtJobID.Text.Trim() + " Change FixedAsset",
                                        UserID = UserInfo.UserId,
                                        Apps = "SMSMINI2"

                                    };

                                    dc.Log_DeleteDatabyPrograms.InsertOnSubmit(ld);
                                    dc.SubmitChanges();


                                    //select หาข้อมูลว่ามีอยู่จริงที่ที่สถานีเก่า
                                    //var vchkD = dc.FFixedAsset_Station_Details
                                    //   .Where(t => t.SerialNumber == item.SerialNumber &&
                                    //           t.FCAT_ID == catID &&
                                    //           t.FMD_ID == item2.FMD_ID
                                    //           && t.STA_ID != item2.STA_ID)
                                    //   .FirstOrDefault();

                                    //if (vchkD != null)
                                    //{


                                    ////==20210309
                                    var vchkD1 = (from p in dc.FFixedAsset_SerialNumber_Pools
                                                  join m in dc.FFixedAsset_Models on p.FMD_ID equals m.FMD_ID
                                                  join b in dc.FFixedAsset_Brands on m.FBA_ID equals b.FBA_ID
                                                  join c in dc.FFixedAsset_Categories on b.FCAT_ID equals c.FCAT_ID
                                                  where p.SerialNumber == SerialNumber
                                                  select new
                                                  {
                                                      c.FCAT_ID,
                                                      b.FBA_ID,
                                                      m.FMD_ID,

                                                  })
                                              .FirstOrDefault();
                                    //==

                                    ////เก็บ log Fixed Asset ของสถานีเก่า
                                    var newLog = new DAL.SMSManage.FFixedAsset_Station_Detail_logMove
                                    {

                                        SerialNumber = snOld.SerialNumber,
                                        /*20210309 FCAT_ID = snOld.FCAT_ID,
                                        FBA_ID = snOld.FBA_ID,
                                        FMD_ID = snOld.FMD_ID,*/
                                        //--20210309 
                                        FCAT_ID = vchkD1.FCAT_ID,
                                        FBA_ID = vchkD1.FBA_ID,
                                        FMD_ID = vchkD1.FMD_ID,
                                        //------
                                        STA_ID = snOld.STA_ID,
                                        OwnerShip = snOld.OwnerShip,
                                        InstallDate = snOld.InstallDate,
                                        WarrantDate = snOld.WarrantDate,
                                        Location = snOld.Location,
                                        LOC_ID = snOld.LOC_ID,
                                        Detail = snOld.Detail,
                                        TranDate = snOld.TranDate,
                                        PartType1 = snOld.PartType1,
                                        PartType2 = snOld.PartType2,
                                        PartType3 = snOld.PartType3,
                                        CheckGauge = snOld.CheckGauge,
                                        TranEMP = snOld.TranEMP,
                                        ControlFixAssetNo = snOld.ControlFixAssetNo,
                                        IsConfirm = snOld.IsConfirm,
                                        ConfirmEMP = snOld.ConfirmEMP,
                                        ConfirmDate = snOld.ConfirmDate,
                                        STA_ID_MoveTo = stNew,
                                        TranDate_MoveTo = getdate,
                                        TranEMP_MoveTo = UserInfo.UserId
                                    };

                                    dc.FFixedAsset_Station_Detail_logMoves.InsertOnSubmit(newLog);
                                    dc.SubmitChanges();
                                    //}

                                    //==========

                                    dc.FFixedAsset_Station_Details.DeleteOnSubmit(snOld);
                                    dc.SubmitChanges();

                                } //if (catID != 0)//ต้องมีอยู่จริงในระบบ ไม่ใช่การ key //20150114
                            }

                            #region Insert SN New/reduce SN_Old in table FFixedAsset_Station_Categorie

                            /* 20210308 ยกเลิกการใช้งาน
                            //2.ลบจำนวน Cat ใน FFixedAsset_Station_Category
                            var _catQtyNew = dc
                                .FFixedAsset_Station_Categories
                                //20150113.Where(t => t.FCAT_ID == catID && t.STA_ID == staID)
                                .Where(t => t.FCAT_ID == catID && t.STA_ID == stNew)
                                .FirstOrDefault();

                            if (_catQtyNew == null)
                            {
                                if (catID != 0)
                                {
                                    var newCat = new DAL.SMSManage.FFixedAsset_Station_Category()
                                    {
                                        FCAT_ID = catID,
                                        //20150113 STA_ID = staID,
                                        STA_ID = stNew,
                                        FQuantity = 1
                                    };
                                    dc.FFixedAsset_Station_Categories.InsertOnSubmit(newCat);
                                    dc.SubmitChanges();
                                }
                            }
                            else
                            {
                                _catQtyNew.FQuantity = _catQtyNew.FQuantity + 1;
                                dc.SubmitChanges();
                            }
                             */

                            /*// 20210129 ย้าย บันทึกก่อนลบ 
                            _msg = " Insert SN New";

                            if (catID != 0)
                            {
                                if ((dc.FFixedAsset_Station_Details
                                    .Where(t => t.SerialNumber == SerialNumber_new &&
                                    t.FCAT_ID == catID &&
                                    t.FBA_ID == fbaID &&
                                    t.FMD_ID == fmdID &&
                                        //20150113 t.STA_ID == staID)).Count() <= 0)
                                   t.STA_ID == stNew)).Count() <= 0)
                                {
                                    //==20200722
                                    var chkInv = dc.FFixedAsset_SerialNumber_Pools
                                    .Where(t => t.SerialNumber == SerialNumber_new
                                        //&& t.FCAT_ID == catID &&
                                        //t.FMD_ID == fmdID 
                                    ).FirstOrDefault();
                                    //==========
                                    var snNew = new DAL.SMSManage.FFixedAsset_Station_Detail()
                                    {
                                        SerialNumber = SerialNumber_new,
                                        FCAT_ID = catID,
                                        FBA_ID = fbaID,
                                        FMD_ID = fmdID,
                                        //20150113 STA_ID = staID,
                                        STA_ID = stNew,
                                        OwnerShip = "FLO",
                                        InstallDate = chkInv.INVDate != null ? chkInv.INVDate : getdate,//20200722 ให้ไปใช้วันที่ inv แทน getdate,
                                        LOC_ID = fLoc_id,//20150113 

                                    };

                                    dc.FFixedAsset_Station_Details.InsertOnSubmit(snNew);
                                    dc.SubmitChanges();



                                    ////20210120                                  
                                    //    var newSNTrack = new DAL.SMSManage.FFixedAsset_SerialNumber_Tracking
                                    //    {
                                    //        TrackingDate = getdate,
                                    //        SerialNumber_Old = SerialNumber,
                                    //        SerialNumber_New = SerialNumber_new,
                                    //        JOB_ID = txtJobID.Text.Trim(),
                                    //        STA_ID_Old = stOld,
                                    //        STA_ID_New = stNew,
                                    //        VAN_ID = vanID,
                                    //        SerialNumber_Status = "11",
                                    //        Remark = "Replace FixedAsset Tracking",
                                    //        RegistDate = getdate
                                    //    };

                                    //    dc.FFixedAsset_SerialNumber_Trackings.InsertOnSubmit(newSNTrack);
                                    //    dc.SubmitChanges();

                                    ////=== 



                                }
                            }*/

                            #endregion

                            /*20210308 ยกเลิกการใช้งาน
                            //2. ลบจำนวน Cat ใน FFixedAsset_Station_Category
                            var _catQty = dc.FFixedAsset_Station_Categories
                                .Where(t => t.FCAT_ID == catID && t.STA_ID == staID)
                                .FirstOrDefault();

                            if (_catQty != null)
                            {
                                _catQty.FQuantity = _catQty.FQuantity - 1;
                                dc.SubmitChanges();
                            }
                             */

                            #endregion


                            #region Update SN Old รอซ่อม
                            ////_msg = "Update SN Old รอซ่อม";

                            ////var snOldPool = dc.FFixedAsset_SerialNumber_Pools
                            ////    .Where(t => t.SerialNumber == item.SerialNumber)
                            ////    .FirstOrDefault();

                            ////if (snOldPool != null)
                            ////{
                            ////    snOldPool.StatusUse = "12";//รอซ่อม
                            ////    dc.SubmitChanges();

                            ////}

                            //20150113 ก่อนตรวจสอบเรื่อง ตู้จ่าย
                            var qpool2 = dc.FFixedAsset_SerialNumber_Pools
                               .Where(t => t.SerialNumber == SerialNumber).FirstOrDefault();

                            if (qpool2 != null)
                            {
                                //20140814 ก่อนตรวจสอบเรื่อง ตู้จ่าย
                                //qpool2.StatusUse = "12";//รอซ่อม
                                //db.SubmitChanges();        


                                if (catID != 0)//20150127
                                {
                                    //20210115 ประเภทงานส่งคืน
                                    if (tmpTYP_ID1 == "RF")
                                    {
                                        qpool2.StatusUse = "00";//พร้อมใช้
                                        dc.SubmitChanges();
                                        //20210120  
                                        DateTime Trackgetdate = dc.ExecuteQuery<DateTime>("select getdate() ")
                                        .FirstOrDefault();
                                        var newSNTrack = new DAL.SMSManage.FFixedAsset_SerialNumber_Tracking
                                        {
                                            TrackingDate = Trackgetdate,
                                            SerialNumber_Old = SerialNumber,
                                            SerialNumber_New = SerialNumber,
                                            JOB_ID = txtJobID.Text.Trim(),
                                            STA_ID_Old = stOld,
                                            STA_ID_New = stNew,
                                            VAN_ID = vanID,
                                            SerialNumber_Status = "00",
                                            Remark = "Ready (RF) FixedAsset Tracking",
                                            RegistDate = getdate
                                        };

                                        dc.FFixedAsset_SerialNumber_Trackings.InsertOnSubmit(newSNTrack);
                                        dc.SubmitChanges();

                                        //=== 




                                        #region Update SN new to use 1
                                        _msg = "Update SN new to use1";
                                        var snNewPool = dc.FFixedAsset_SerialNumber_Pools
                                           .Where(t => t.SerialNumber == SerialNumber_new)
                                           .FirstOrDefault();

                                        if (snNewPool != null)
                                        {
                                            snNewPool.StatusUse = "11";//ใช้แล้ว
                                            dc.SubmitChanges();

                                            //20210120      
                                            DateTime Trackgetdate1 = dc.ExecuteQuery<DateTime>("select getdate() ")
                                            .FirstOrDefault();
                                            var newSNTrack1 = new DAL.SMSManage.FFixedAsset_SerialNumber_Tracking
                                            {
                                                TrackingDate = Trackgetdate1,
                                                SerialNumber_Old = SerialNumber_new,
                                                SerialNumber_New = SerialNumber_new,
                                                JOB_ID = txtJobID.Text.Trim(),
                                                STA_ID_Old = stOld,
                                                STA_ID_New = stNew,
                                                VAN_ID = vanID,
                                                SerialNumber_Status = "11",
                                                Remark = "Replace FixedAsset Tracking",
                                                RegistDate = getdate
                                            };

                                            dc.FFixedAsset_SerialNumber_Trackings.InsertOnSubmit(newSNTrack1);
                                            dc.SubmitChanges();

                                            //=== 




                                        }
                                        #endregion
                                    }//RF
                                    else
                                    {

                                        var chkpool = ((from p in dc.FFixedAsset_SerialNumber_Pools
                                                        join m in dc.FFixedAsset_Models on p.FMD_ID equals m.FMD_ID
                                                        join b in dc.FFixedAsset_Brands on m.FBA_ID equals b.FBA_ID
                                                        where m.FMD_ID.ToString().Contains(qpool2.FMD_ID.ToString())
                                                        && p.SerialNumber == qpool2.SerialNumber
                                                        && (b.FBA_ID.ToString() == "1"
                                                        || b.FBA_ID.ToString() == "2"
                                                        || b.FBA_ID.ToString() == "3"
                                                        || b.FBA_ID.ToString() == "4")
                                                        select p).FirstOrDefault());

                                        if (chkpool != null)
                                        {
                                            chkpool.StatusUse = "00";//พร้อมใช้
                                            dc.SubmitChanges();

                                            //20210120 
                                            DateTime Trackgetdate2 = dc.ExecuteQuery<DateTime>("select getdate() ")
                                               .FirstOrDefault();
                                            var newSNTrack = new DAL.SMSManage.FFixedAsset_SerialNumber_Tracking
                                            {
                                                TrackingDate = Trackgetdate2,
                                                SerialNumber_Old = SerialNumber,
                                                SerialNumber_New = SerialNumber,
                                                JOB_ID = txtJobID.Text.Trim(),
                                                STA_ID_Old = stOld,
                                                STA_ID_New = stNew,
                                                VAN_ID = vanID,
                                                SerialNumber_Status = "00",
                                                Remark = "Dispenser Ready FixedAsset Tracking",
                                                RegistDate = getdate
                                            };

                                            dc.FFixedAsset_SerialNumber_Trackings.InsertOnSubmit(newSNTrack);
                                            dc.SubmitChanges();

                                            //=== 
                                        }
                                        else
                                        {
                                            qpool2.StatusUse = "12";//รอซ่อม
                                            dc.SubmitChanges();

                                            //20210120   
                                            DateTime Trackgetdate3 = dc.ExecuteQuery<DateTime>("select getdate() ")
                                               .FirstOrDefault();
                                            var newSNTrack = new DAL.SMSManage.FFixedAsset_SerialNumber_Tracking
                                            {
                                                TrackingDate = Trackgetdate3,
                                                SerialNumber_Old = SerialNumber,
                                                SerialNumber_New = SerialNumber,
                                                JOB_ID = txtJobID.Text.Trim(),
                                                STA_ID_Old = stOld,
                                                STA_ID_New = stNew,
                                                VAN_ID = vanID,
                                                SerialNumber_Status = "12",
                                                Remark = "Waiting FixedAsset Tracking",
                                                RegistDate = getdate
                                            };

                                            dc.FFixedAsset_SerialNumber_Trackings.InsertOnSubmit(newSNTrack);
                                            dc.SubmitChanges();

                                            //=== 
                                        }
                                        //20150127} if (qpool2 != null)


                                        #endregion

                                        #region Update SN new to use 1
                                        _msg = "Update SN new to use1";
                                        var snNewPool = dc.FFixedAsset_SerialNumber_Pools
                                           .Where(t => t.SerialNumber == SerialNumber_new)
                                           .FirstOrDefault();

                                        if (snNewPool != null)
                                        {
                                            snNewPool.StatusUse = "11";//ใช้แล้ว
                                            dc.SubmitChanges();

                                            //20210120 
                                            DateTime Trackgetdate4 = dc.ExecuteQuery<DateTime>("select getdate() ")
                                               .FirstOrDefault();
                                            var newSNTrack = new DAL.SMSManage.FFixedAsset_SerialNumber_Tracking
                                            {
                                                TrackingDate = Trackgetdate4,
                                                SerialNumber_Old = SerialNumber_new,
                                                SerialNumber_New = SerialNumber_new,
                                                JOB_ID = txtJobID.Text.Trim(),
                                                STA_ID_Old = stOld,
                                                STA_ID_New = stNew,
                                                VAN_ID = vanID,
                                                SerialNumber_Status = "11",
                                                Remark = "Replace FixedAsset Tracking",
                                                RegistDate = getdate
                                            };

                                            dc.FFixedAsset_SerialNumber_Trackings.InsertOnSubmit(newSNTrack);
                                            dc.SubmitChanges();

                                        }
                                        #endregion
                                    }//iftypis1
                                }//catID !=0//20150127
                            }// if (qpool2 != null)//20150127

                        }// if (SerialNumber != SerialNumber_new)
                        else
                        {
                            #region Update SN new to use 2
                            _msg = "Update SN new to use2";
                            var snNewPool = dc.FFixedAsset_SerialNumber_Pools
                               .Where(t => t.SerialNumber == SerialNumber_new)
                               .FirstOrDefault();

                            if (snNewPool != null)
                            {
                                snNewPool.StatusUse = "11";//ใช้แล้ว
                                dc.SubmitChanges();

                                //20210120  
                                DateTime Trackgetdate5 = dc.ExecuteQuery<DateTime>("select getdate() ")
                                           .FirstOrDefault();
                                var newSNTrack1 = new DAL.SMSManage.FFixedAsset_SerialNumber_Tracking
                                {
                                    TrackingDate = Trackgetdate5,
                                    SerialNumber_Old = SerialNumber_new,
                                    SerialNumber_New = SerialNumber_new,
                                    JOB_ID = txtJobID.Text.Trim(),
                                    STA_ID_Old = stOld,
                                    STA_ID_New = stNew,
                                    VAN_ID = vanID,
                                    SerialNumber_Status = "11",
                                    Remark = "Used FixedAsset Tracking",
                                    RegistDate = getdate
                                };

                                dc.FFixedAsset_SerialNumber_Trackings.InsertOnSubmit(newSNTrack1);
                                dc.SubmitChanges();

                            }
                            #endregion
                        }

                        //?? แล้วถ้ากรณีคืนค่า เช่น ยกเลิกจุดเสีย



                    }
                    //--end-process upload


                    /* flow ปิดงาน offline
                    //20141223 if ((SerialNumber != "None" && SerialNumber != "None") && (SerialNumber != "ไม่ระบุ..." && SerialNumber != "ไม่ระบุ..."))
                     //20191126 ย้ายขึ้นข้างบน if ((SerialNumber != "None" && SerialNumber_new != "None") && (SerialNumber != "ไม่ระบุ..." && SerialNumber_new != "ไม่ระบุ..."))
                     //20191126 ย้ายขึ้นข้างบน    {


                            //20141224 เช็คข้อมุล SN ที่ user key เองมีใน pool หรือไม่
                           var qchkPool = from a in db.FFixedAsset_SerialNumber_Pools
                                          where a.SerialNumber == SerialNumber_new
                                        select a;

                           if (qchkPool.Count() > 0)
                           {

                               //ถ้ามีการเปลี่ยน Serial
                               if (SerialNumber != SerialNumber_new)
                               {

                                   #region FFixedAsset_Station_Details


                                   var q = from t in db.FFixedAsset_Station_Details
                                           where t.STA_ID == tmpSTAID &&
                                                   t.SerialNumber == SerialNumber
                                           select t;

                                   if (q.Count() > 0)
                                   {
                                       var delSN = q.Where(t => t.SerialNumber == SerialNumber
                                              
                                                     &&
                                                   t.FCAT_ID == t.FCAT_ID &&
                                                  t.FMD_ID == t.FMD_ID &&
                                                  t.STA_ID == t.STA_ID
                                                
                                                 //  &&
                                                 //  t.FCAT_ID == tmpSN.FCAT_ID) &&
                                                 // t.FMD_ID == tmpSN.FMD_ID &&
                                                 // t.STA_ID == tmpSN.STA_ID

                                                 ).FirstOrDefault();
                                       if (delSN != null)
                                       {
                                           var tmpSN = delSN;
                                           db.FFixedAsset_Station_Details.DeleteOnSubmit(delSN);

                                           var newSN = new FFixedAsset_Station_Detail
                                           {
                                               SerialNumber = SerialNumber_new,
                                               FCAT_ID = tmpSN.FCAT_ID,
                                               FBA_ID = tmpSN.FBA_ID,
                                               FMD_ID = tmpSN.FMD_ID,
                                               STA_ID = tmpSN.STA_ID,
                                               OwnerShip = tmpSN.OwnerShip,
                                               InstallDate = tmpSN.InstallDate,
                                               Detail = tmpSN.Detail,
                                               //20141208
                                               LOC_ID = tmpSN.LOC_ID
                                           };

                                           db.FFixedAsset_Station_Details.InsertOnSubmit(newSN);
                                           db.SubmitChanges();
                                       }
                             
                                   //เปลี่ยน 20150105   }

                                   #endregion

                                   #region Update SN Pool


                                   var qpool = db.FFixedAsset_SerialNumber_Pools
                                       .Where(t => t.SerialNumber == SerialNumber_new).FirstOrDefault();

                                   if (qpool != null)
                                   {
                                       qpool.StatusUse = "11";//ใช้งานแล้ว
                                       db.SubmitChanges();
                                   }
                                   //20140814 ก่อนตรวจสอบเรื่อง ตู้จ่าย
                                   var qpool2 = db.FFixedAsset_SerialNumber_Pools
                                      .Where(t => t.SerialNumber == SerialNumber).FirstOrDefault();

                                   if (qpool2 != null)
                                   {
                                       //20140814 ก่อนตรวจสอบเรื่อง ตู้จ่าย
                                       //qpool2.StatusUse = "12";//รอซ่อม
                                       //db.SubmitChanges();                          

                                       var chkpool = ((from p in db.FFixedAsset_SerialNumber_Pools
                                                       join m in db.FFixedAsset_Models on p.FMD_ID equals m.FMD_ID
                                                       join b in db.FFixedAsset_Brands on m.FBA_ID equals b.FBA_ID
                                                       where m.FMD_ID.ToString().Contains(qpool2.FMD_ID.ToString())
                                                       && p.SerialNumber == qpool2.SerialNumber
                                                       && (b.FBA_ID.ToString() == "1"
                                                       || b.FBA_ID.ToString() == "2"
                                                       || b.FBA_ID.ToString() == "3"
                                                       || b.FBA_ID.ToString() == "4")
                                                       select p).FirstOrDefault());

                                       if (chkpool != null)
                                       {
                                           chkpool.StatusUse = "00";//พร้อมใช้
                                           db.SubmitChanges();
                                       }
                                       else
                                       {
                                           qpool2.StatusUse = "12";//รอซ่อม
                                           db.SubmitChanges();
                                       }
                                   }
                                  }//เปลี่ยน 20150105


                                   //คืนค่า ถ้ามีการยกเลิก
                                   foreach (var item in lsSNCancel)
                                   {
                                       var resetSNCanOld = db.FFixedAsset_SerialNumber_Pools
                                      .Where(t => t.SerialNumber == item.SNold).FirstOrDefault();
                                       if (resetSNCanOld != null)
                                       {
                                           resetSNCanOld.StatusUse = "11";
                                           db.SubmitChanges();
                                       }

                                       var resetSNCanNew = db.FFixedAsset_SerialNumber_Pools
                                     .Where(t => t.SerialNumber == item.SNNew).FirstOrDefault();
                                       if (resetSNCanNew != null)
                                       {
                                           resetSNCanNew.StatusUse = "00";
                                           db.SubmitChanges();
                                       }
                                   }
                                   #endregion



                               }
                               else
                               {
                                   var qpool = db.FFixedAsset_SerialNumber_Pools
                                          .Where(t => t.SerialNumber == SerialNumber_new).FirstOrDefault();

                                   if (qpool != null)
                                   {
                                       qpool.StatusUse = "11";//ใช้งานแล้ว
                                       db.SubmitChanges();
                                   }
                               }
                           }//end chkpool

                         */

                }//  if (SerialNumber != "None" && SerialNumber != "None")

            }// for (int i ...
            //}//dal
        }

        private DateTime checkStartDate(DateTime _StartDate, string _SPA_ID)
        {
            for (int i = 0; i <= dataGridView_tmpPart.Rows.Count - 1; i++)
            {
                //20200911 ปิดงานคนละวัน รายการอะไหล่ตัวเดียวกันมันทับข้อมูลเดิม 
                //if (_SPA_ID == dataGridView_tmpPart["SPA_ID", i].Value.ToString() )
                //{
                //    _StartDate = DateTime.Parse(dataGridView_tmpPart["StartDate", i].Value.ToString());
                //    return _StartDate;
                //}
                //if (_SPA_ID == dataGridView_tmpPart["SPA_ID", i].Value.ToString() && dataGridView_tmpPart["StatusSpare", i].Value.ToString() == "0")

                string d = dataGridView_tmpPart["StatusSpare", i].Value.ToString();
                //string d = dataGridView_tmpPart["SPA_ID", i].Value.ToString();
                if (_SPA_ID == dataGridView_tmpPart["SPA_ID", i].Value.ToString() && bool.Parse(dataGridView_tmpPart["StatusSpare", i].Value.ToString()) == false)
                {

                    _StartDate = DateTime.Parse(dataGridView_tmpPart["StartDate", i].Value.ToString());
                    return _StartDate;
                }
            }
            return _StartDate;
        }

        #endregion

        private void toolStripButton_Edit_Click(object sender, EventArgs e)
        {
            editMode = "job_edit";
            disableControl(false);
        }

        private void toolStripButton_Cancel_Click(object sender, EventArgs e)
        {
            JobClose_Load(null, null);


            tmpEndDate = "";
            tmpEndTime = "";

            lblIsContract.Visible = false;
            txtContract.Visible = false;
            btEditContractNo.Visible = false;
            btUpdatePart.Visible = false;
            btUpdateWorkType.Enabled = false;
            btUpdateWorkType.BackColor = Color.Transparent;

            label_1.Visible = false;
            label_2.Visible = false;
            label_3.Visible = false;
        }

        private void toolStripButton_Exit_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Abort;
            this.Close();
        }

        private void toolStripButton_Print_Click(object sender, EventArgs e)
        {
            if (editMode == "job_edit")
                strConn = GetConnection.Connection;
            else
                strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;



            if (tmpjobID == string.Empty)
            {
                closeProgress();

                MessageBox.Show("ยังไม่เลือกข้อมูล JOB", "ผลการตรวจสอบ");
                return;
            }

            using (DAL.SMSManage.SMSManageDataContext dc = new DAL.SMSManage.SMSManageDataContext(strConn))
            {
                var jchkU = dc.JOBs .Where(t => t.JOB_ID == tmpjobID && new string[] { "99", "07" }.Contains(t.JOBS_ID)) .FirstOrDefault();

                if (jchkU != null) chkUpload = "1"; else chkUpload = "0";

                //string vJOBChkTypFFA = null;
                var qJOBChkStaM = dc.JOBs.Where(t => t.JOB_ID == tmpjobID.Trim()).FirstOrDefault();


                if (qJOBChkStaM != null)
                {
                    //vJOBChkTypFFA = qJOBChkTypFFA.TYP_ID.Trim();
                    if (qJOBChkStaM.JOB_ID.Contains("M") && qJOBChkStaM.STA_ID.Trim() == "10007810")
                    {
                        closeProgress();

                        MessageBox.Show("คุณไม่สามารถสั่งพิมพ์ Service Report..." + Environment.NewLine +
                            "เนื่องจาก เป็น JOB งานขาย...สถานีงานขาย-ลูกค้าทั่วไป..." + Environment.NewLine +
                            "กรุณา ตรวจสอบข้อมูล อีกครั้ง...", "ผลการตรวจสอบ...", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
            }

            //==20191022 ตรวจสอบ
            // upload รูป IN,WA
            // ต้องมีเลข Job กรณีงานสำรวจสถานี
            // ยืนยัน FFA ที่สถานีตามประเภทjobที่เปิด              
            // ต้อง stop ML

            //20200212 ======= upload รูป IN,WA ============

            if (!strConn.Contains("ServicesMSDB_"))//Database Backup...ไม่ต้องเช็ค JOB Photo Upload 
            {
                if (checkJOBPhotoUpload())
                {
                    closeProgress();
                    MessageBox.Show("คุณไม่สามารถสั่งพิมพ์ Service Report..." + Environment.NewLine + "กรุณา Upload รูปภาพอะไหล่...",
                                           "ผลการตรวจสอบ...", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    try
                    {

                        Cursor.Current = Cursors.AppStarting;
                        Transaction.JobUploadPhoto f = new JobUploadPhoto();
                        f.StartPosition = FormStartPosition.CenterScreen;

                        f._jOBNo = _job.JOB_ID;
                        f._MODE = "0";//20151001
                        f._failureDetail = _job.JobFailure_Detail;
                        f._startDate = _jobd.StartDate;
                        f._endDate = _jobd.Enddate.Value;
                        f._workID = (_wid == null ? "" : _wid.Work_ID);
                        f._stationNo = _station.STA_ID;
                        f._stationName = _station.StationSys;

                        f.lsJPart = lsJSP.ToList();

                        //20201022
                        f._LConfTRA = LConfTRA.ToList();


                        f.ShowDialog();

                    }
                    catch (System.Exception ex)
                    {
                        closeProgress();
                        MessageBox.Show("เกิดผิดพลาดในการใช้งานเมนู (4) Upload JOB Error " + ex.Message, "ข้อผิดพลาด");
                        //this.Show();
                    }

                    ////this.Close();
                    return;
                }
            }

            //=======================================
            //20200205 ยืนยัน FFA ที่สถานีตามประเภทjobที่เปิด
            //tmpJobType

            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn))
            {
                string vJOBChkTypFFA = null;

                var qJOBChkTypFFA = dc.JOBs.Where(t => t.JOB_ID == tmpjobID.Trim()).FirstOrDefault();


                if (qJOBChkTypFFA != null)
                {
                    vJOBChkTypFFA = qJOBChkTypFFA.TYP_ID.Trim();

                }


                List<string> LJOBChkTypFFA = new List<string>();

                if (vJOBChkTypFFA == "30")
                {


                    var a = dc.FFixedAsset_Categories
                       .Where(t => t.TYPD == '1' &&
                                  t.IsCancel == false)
                                  .Select(t => t.FCAT_ID.ToString())
                                  .ToList();


                    LJOBChkTypFFA = a;



                    //20170807
                    var fIsConf = (from t in dc.FFixedAsset_Station_Details
                                   join s in dc.Stations on t.STA_ID equals s.STA_ID
                                   //--20210308--
                                   join p in dc.FFixedAsset_SerialNumber_Pools on t.SerialNumber equals p.SerialNumber
                                   join m in dc.FFixedAsset_Models on p.FMD_ID equals m.FMD_ID
                                   join b in dc.FFixedAsset_Brands on m.FBA_ID equals b.FBA_ID
                                   join c in dc.FFixedAsset_Categories on b.FCAT_ID equals c.FCAT_ID
                                   //------------

                                   //where strStaIDBinding.ToList().Contains(t.STA_ID)
                                   where t.STA_ID == qJOBChkTypFFA.STA_ID
                                   //20171122 && (t.IsConfirm == false || t.IsConfirm == null)
                                    && (
                                    (t.IsConfirm == false || t.IsConfirm == null) || ((t.IsConfirm == true && t.LOC_ID == null) || (t.IsConfirm == true && t.LOC_ID == 0))
                                    )
                                   //20171122 เพิ่มเงื่อนไขห้ามระบุ loc เป็น null,0
                                   //20210308 && LJOBChkTypFFA.Contains(t.FCAT_ID.ToString())
                                   && LJOBChkTypFFA.Contains(c.FCAT_ID.ToString())
                                   select new
                                   {
                                       STA_ID = t.STA_ID,
                                       StationSys = s.StationSys
                                   }).FirstOrDefault();

                    if (fIsConf != null)
                    {
                        closeProgress();
                        if (MessageBox.Show("สถานี: " + fIsConf.STA_ID.ToString() + " " + fIsConf.StationSys.ToString() + Environment.NewLine +
                          //20171122"ยังไม่บันทึก ยืนยัน ความถูกต้องของ Fixed Asset" + Environment.NewLine +
                          //20171122"กรุณา เข้าไป บันทึกยืนยันความถูกต้อง", "ผลการตรวจสอบ...ยืนยัน Fixed Asset", MessageBoxButtons.OK, MessageBoxIcon.Warning) == DialogResult.OK)
                          "ยังไม่บันทึก ยืนยัน ความถูกต้องของ Fixed Asset" + Environment.NewLine +
                          "หรือ ยังมีรายการที่เลือก Location เป็น ไม่ระบุ..." + Environment.NewLine +
                          "กรุณา เข้าไป บันทึกยืนยันความถูกต้องใหม่", "ผลการตรวจสอบ...ยืนยัน Fixed Asset", MessageBoxButtons.OK, MessageBoxIcon.Warning) == DialogResult.OK)
                        {
                            try
                            {
                                Cursor.Current = Cursors.AppStarting;
                                FFA.Input_FFA f = new SMSMINI.FFA.Input_FFA();
                                f.IsConfirmFFA = true;
                                f.statID = fIsConf.STA_ID;//vSTAConf;//vSTAConf;
                                //20180328
                                f.vLJOBChkTypFFA = LJOBChkTypFFA;
                                f.vFrmUpload = "1";

                                f.WindowState = FormWindowState.Maximized;
                                if (f.ShowDialog() == DialogResult.Abort)
                                    Cursor.Current = Cursors.Default;
                            }
                            catch (System.Exception ex)
                            {
                                MessageBox.Show("เกิดผิดพลาดในการใช้งานเมนู ยืนยัน ข้อมูล  Fixed Asset " + Environment.NewLine
                                    + ex.Message, "ข้อผิดพลาด");
                                this.Show();
                            }
                            //=====================
                            return;

                        }//if (MessageBox
                    }//if (fIsConf.Count() > 0)
                }// if (vJOBChkTypFFA == "30")
                else if (vJOBChkTypFFA == "31")
                {
                    var a = dc.FFixedAsset_Categories
                              .Where(t => t.TYPS == '1' &&
                                         t.IsCancel == false)
                                         .Select(t => t.FCAT_ID.ToString())
                                         .ToList();


                    LJOBChkTypFFA = a;



                    //20170807
                    var fIsConf = (from t in dc.FFixedAsset_Station_Details
                                   join s in dc.Stations on t.STA_ID equals s.STA_ID
                                   //--20210308--
                                   join p in dc.FFixedAsset_SerialNumber_Pools on t.SerialNumber equals p.SerialNumber
                                   join m in dc.FFixedAsset_Models on p.FMD_ID equals m.FMD_ID
                                   join b in dc.FFixedAsset_Brands on m.FBA_ID equals b.FBA_ID
                                   join c in dc.FFixedAsset_Categories on b.FCAT_ID equals c.FCAT_ID
                                   //------------

                                   //where strStaIDBinding.ToList().Contains(t.STA_ID)
                                   where t.STA_ID == qJOBChkTypFFA.STA_ID

                                   //20171122 && (t.IsConfirm == false || t.IsConfirm == null)
                                    && (

                                    (t.IsConfirm == false || t.IsConfirm == null)
                                    || ((t.IsConfirm == true && t.LOC_ID == null) || (t.IsConfirm == true && t.LOC_ID == 0))

                                    )
                                   //20171122 เพิ่มเงื่อนไขห้ามระบุ loc เป็น null,0

                                   //20210308 && LJOBChkTypFFA.Contains(t.FCAT_ID.ToString())
                                   && LJOBChkTypFFA.Contains(c.FCAT_ID.ToString())
                                   select new
                                   {
                                       STA_ID = t.STA_ID,
                                       StationSys = s.StationSys
                                   }).FirstOrDefault();

                    if (fIsConf != null)
                    {

                        if (MessageBox.Show("สถานี: " + fIsConf.STA_ID.ToString() + " " + fIsConf.StationSys.ToString() + Environment.NewLine +
                      //20171122"ยังไม่บันทึก ยืนยัน ความถูกต้องของ Fixed Asset" + Environment.NewLine +
                      //20171122"กรุณา เข้าไป บันทึกยืนยันความถูกต้อง", "ผลการตรวจสอบ...ยืนยัน Fixed Asset", MessageBoxButtons.OK, MessageBoxIcon.Warning) == DialogResult.OK)
                      "ยังไม่บันทึก ยืนยัน ความถูกต้องของ Fixed Asset" + Environment.NewLine +
                      "หรือ ยังมีรายการที่เลือก Location เป็น ไม่ระบุ..." + Environment.NewLine +
                      "กรุณา เข้าไป บันทึกยืนยันความถูกต้องใหม่", "ผลการตรวจสอบ...ยืนยัน Fixed Asset", MessageBoxButtons.OK, MessageBoxIcon.Warning) == DialogResult.OK)
                        {
                            //20200305 this.Close();
                            //20170808 IsFFAOK = true;
                            //20200206 statID = fIsConf.STA_ID;//vSTAConf;//vSTAConf;
                            //20200206 checkUpload = false;
                            //==20170626 default ให้ยืนยัน====
                            try
                            {
                                Cursor.Current = Cursors.AppStarting;
                                FFA.Input_FFA f = new SMSMINI.FFA.Input_FFA();
                                f.IsConfirmFFA = true;
                                f.statID = fIsConf.STA_ID;//vSTAConf;//vSTAConf;
                                //20180328
                                f.vLJOBChkTypFFA = LJOBChkTypFFA;
                                f.vFrmUpload = "1";

                                f.WindowState = FormWindowState.Maximized;
                                if (f.ShowDialog() == DialogResult.Abort)
                                    Cursor.Current = Cursors.Default;
                            }
                            catch (System.Exception ex)
                            {
                                MessageBox.Show("เกิดผิดพลาดในการใช้งานเมนู ยืนยัน ข้อมูล  Fixed Asset " + Environment.NewLine
                                    + ex.Message, "ข้อผิดพลาด");
                                this.Show();
                            }
                            //=====================
                            return;

                        }//if (MessageBox
                    }//if (fIsConf.Count() > 0)                                    
                } // else if  (vJOBChkTypFFA == "31")

            }

            //=====================================

            //20200205 ต้อง stop ML
            string vJobIDML = "";
            string vEMP_IDML = "";
            vJobIDML = tmpjobID.Trim();
            vEMP_IDML = tmpVANID.Trim();

            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn))
            {
                var qJobIDML = (from t in dc.JOB_Detail_GPS_Mileages
                                where (t.JOB_ID == vJobIDML) &&
                                       (t.Start_Date != null) &&
                                       (t.Stop_Date == null) &&
                                       (t.VAN == vEMP_IDML)
                                select t).FirstOrDefault();
                if (qJobIDML != null)
                {

                    if (MessageBox.Show("คุณยังไม่ STOP ไมล์เลจ...JOB : " + vJobIDML + Environment.NewLine +
                          "คุณต้องการ STOP ไมล์เลจ ใช่หรือไม่", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {


                        var url = GetURL.smsURL("JML");
                        if (url != "")
                            System.Diagnostics.Process.Start(url);
                        else
                            System.Diagnostics.Process.Start("http://sms.flowco.co.th/jml");

                        return;

                    }
                }
            }
            //=======================================


            if (stmpEndDate == "" && editMode == "job_edit")
            {
                panel_selectEndDate.Enabled = true;
                panel_selectEndDate.Visible = true;
                panel_selectEndDate.BringToFront();

            }
            else
            {


                //20200129if (tmpEndDate != "")
                if (stmpEndDate != "")
                {
                    //==20200521
                    string yyyy = stmpEndDate.Substring(0, 4).ToString();
                    string mm = stmpEndDate.Substring(4, 2).ToString();
                    string dd = stmpEndDate.Substring(6, 2).ToString();
                    string dtp = yyyy + "/" + mm + "/" + dd;
                    string b = DateTime.Parse(stmpEndTime).ToString("HH:mm:ss", new System.Globalization.CultureInfo("en-US"));
                    string a = DateTime.Parse(dtp).ToString("yyyy/MM/dd", new System.Globalization.CultureInfo("en-US"));

                    string c = a + " " + b;

                    string d = DateTime.Parse(c).ToString("yyyy/MM/dd HH:mm:ss", new System.Globalization.CultureInfo("en-US"));
 
                    DateTime dtpnt = Convert.ToDateTime(d);

                    DateTime dtCobEndAuthPrint = Convert.ToDateTime(dtpnt.ToString("yyyy/MM/dd HH:mm:ss"));

                    using (DAL.SMSManage.SMSManageDataContext dc = new DAL.SMSManage.SMSManageDataContext(strConn))
                    {
                        //DateTime dtCobEndAuthPrint = d;//Convert.ToDateTime(d);//Convert.ToDateTime(dt1.ToString("yyyy/MM/dd HH:mm:ss"));

                        var iLAuthPrint = (from j in dc.JOBs
                                           join jd in dc.JOB_Details on j.JOB_ID equals jd.JOB_ID
                                           where new string[] { "07", "99" }.Contains(jd.JOBS_ID)
                                           && j.JOB_ID == tmpjobID.Trim()
                                           && jd.EndDate.Value.Date == dtCobEndAuthPrint.Date
                                           && jd.EndDate.Value.Hour == dtCobEndAuthPrint.Hour
                                           && jd.EndDate.Value.Minute == dtCobEndAuthPrint.Minute
                                           && jd.EndDate.Value.Second == dtCobEndAuthPrint.Second
                                           && jd.StatusDetail == true //20201208
                                           select jd).First();

                        if (iLAuthPrint != null)
                        {
                            if (iLAuthPrint.EndDate != null)
                            {
                                //====20200519==เงื่อนไข auth printSV 
                                if (!new string[] { "09", "12", "13" }.Contains(UserInfo.UserLevID))
                                {
                                    if (iLAuthPrint.IsPrintSV == true)
                                    {
                                        closeProgress();
                                        MessageBox.Show("สามารถสั่งพิมพ์รายงานย้อนหลังได้ เฉพาะ สิทธิ์ผู้จัดการเท่านั้น ", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        if (editMode == "job_edit")
                                        {
                                            stmpEndDate = "";
                                            stmpEndTime = "";
                                        }
                                        return;
                                    }

                                }
                            }
                            else
                            {
                                closeProgress();

                                MessageBox.Show("ข้อมูลการปิดงานไม่สมบูรณ์ รบกวนตรวจสอบ ", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return;

                            }
                        }
                    }

                    //===========
                    try
                    {
                        if (UserInfo.IsSVPrintOnline == 1)
                        {
                            var xuser = new xUserInfo();

                            xuser.UserId =  UserInfo.UserId ;
                            xuser.UserName = UserInfo.UserName;
                            xuser.Password = UserInfo.Password;
                            xuser.UserLevID = UserInfo.UserLevID;
                            xuser.UserLevel = UserInfo.UserLevel;
                            xuser.FullName = UserInfo.FullName;
                            xuser.Van_ID = UserInfo.Van_ID;
                            xuser.SUP_VAN = UserInfo.SUP_VAN;
                            xuser.Email = UserInfo.Email;

                            xuser.Van_Type = UserInfo.Van_Type;
                            xuser.VehicleRegis = UserInfo.VehicleRegis;


                            xuser.ConnectMode = UserInfo.ConnectMode;
                             
                            xuser.eDepID = UserInfo.eDepID;
                            xuser.IsSVPrintOnline = UserInfo.IsSVPrintOnline;

                            var json = JsonConvert.SerializeObject(xuser);


                            //http://sms.flowco.co.th/smssvprint/

                            //var mainUrl = "http://localhost:1069/";
                            var mainUrl = "http://sms.flowco.co.th/smssvprint/";

                            var svprint = mainUrl+"SVPrint.aspx?JOB_ID=" + tmpjobID.Trim() + "&EndDate="+ stmpEndDate + "&EndTime="+ stmpEndTime + "&chkUpload="+ chkUpload + "&xUserInfo="+ json + "";
                            System.Diagnostics.Process.Start(svprint) ;
                        }
                        else
                        {
                           

                            Cursor.Current = Cursors.AppStarting;
                            this.Hide();
                            ServiceReport f = new ServiceReport();
                            f.WindowState = FormWindowState.Maximized;
                            f.TmpJobID = tmpjobID.Trim();
                            f.PartCount = PartCount;

                            f.TmpEndDate = stmpEndDate;
                            f.TmpEndTime = stmpEndTime;
                            f.vchkUpload = chkUpload;//20180423

                            f.strConn = strConn;

                            if (f.ShowDialog() == DialogResult.Abort)
                            {
                                this.Show();

                                chkAuthAfterPrintSV(tmpjobID.Trim(), _pEnddate);

                            }


                            PartCount = 0;// re set การนับจำนวนอะไหล่
                            Cursor.Current = Cursors.Default;

                            //===20200128
                            if (editMode == "job_edit")
                            {
                                stmpEndDate = "";
                                stmpEndTime = "";
                            }
                            //===20200128


                            //cobEndDate.Items.Clear();
                        }
                    }
                    catch (System.Exception ex)
                    {
                        closeProgress();
                        MessageBox.Show("ข้อผิดพลาด " + ex.Message, "เกิดข้อผิดพลาด");
                    }



                }
                //===== 20191224
               
                //===== 20191224
                if (stmpEndDate == "" && editMode == "job_add")
                {
                    closeProgress();
                    MessageBox.Show("คุณยังไม่ทำการ บันทึกการปิดงาน ", "ผลการตรวจสอบ");
                    return;
                }

                //===20200128
                if (editMode == "job_edit")
                {
                    stmpEndDate = "";
                    stmpEndTime = "";
                }
                //===20200128
            }


            chkEnableSVReport(tmpjobID.Trim());

        }



        private void toolStripBtSVAdmin_Click(object sender, EventArgs e)
        {
            if (UserInfo.ConnectMode == "1")
            {
                if (tmpEndDate == "")
                {
                    panel_selectEndDate.Visible = true;
                }
                else
                {
                    try
                    {
                        Cursor.Current = Cursors.AppStarting;
                        this.Hide();
                        ServiceReport f = new ServiceReport();
                        f.WindowState = FormWindowState.Maximized;
                        f.TmpJobID = tmpjobID.Trim();
                        f.PartCount = PartCount;
                        f.TmpEndDate = tmpEndDate;
                        f.TmpEndTime = tmpEndTime;
                        f.vchkUpload = chkUpload;//20180423

                        if (f.ShowDialog() == DialogResult.Abort)
                            this.Show();
                        PartCount = 0;// re set การนับจำนวนอะไหล่
                        Cursor.Current = Cursors.Default;

                        tmpEndDate = "";
                        tmpEndTime = "";
                    }
                    catch (System.Exception ex)
                    {
                        closeProgress();
                        MessageBox.Show("ข้อผิดพลาด " + ex.Message, "เกิดข้อผิดพลาด");
                    }
                }

            }
        }

        private void JobClose_FormClosed(object sender, FormClosedEventArgs e)
        {
            DialogResult = DialogResult.Abort;
        }


        #region Private Method

        private void disableControl(bool isDisable)
        {

            //20200128 groupBox1.Enabled = !isDisable;
            groupBox1.Enabled = isDisable;

            btContact_Detail.Enabled = !isDisable;

            groupBox2.Enabled = !isDisable;
            groupBox3.Enabled = !isDisable;
            groupBox4.Enabled = !isDisable;


            cobProblemType.Enabled = !isDisable;
            radioButton_A.Enabled = !isDisable;
            radioButton_B.Enabled = !isDisable;
            radioButton_C.Enabled = !isDisable;
            radioButton_D.Enabled = !isDisable;

            label33.Enabled = !isDisable;
            label32.Enabled = !isDisable;

            //====20202002 
            dataGridView_Part.Enabled = !isDisable;
            //dataGridView_Part.ReadOnly = true;

            btSelectPointFail.Enabled = !isDisable;
            btBtPart_select.Enabled = !isDisable;

            txtResolve_Detail.ReadOnly = isDisable;
            //==========


        }
        private void disableControlInv(bool isDisable)
        {
            toolStripButton_Save.Enabled = !isDisable;
            //20200128 groupBox1.Enabled = !isDisable;
            groupBox1.Enabled = isDisable;

            btContact_Detail.Enabled = !isDisable;

            groupBox2.Enabled = isDisable;
            groupBox3.Enabled = isDisable;
            groupBox4.Enabled = isDisable;

            //dataGridView_FailPoint.ScrollBars




            cobProblemType.Enabled = !isDisable;
            radioButton_A.Enabled = !isDisable;
            radioButton_B.Enabled = !isDisable;
            radioButton_C.Enabled = !isDisable;
            radioButton_D.Enabled = !isDisable;

            label33.Enabled = !isDisable;
            label32.Enabled = !isDisable;

            btEditContractNo.Visible = !isDisable;
            btUpdatePart.Visible = !isDisable;
            btUpdateWorkType.Enabled = !isDisable;
            btUpdateWorkType.BackColor = Color.Lime;

            label_1.Visible = !isDisable;
            label_2.Visible = !isDisable;
            label_3.Visible = !isDisable;



            dataGridView_FailPoint.Enabled = true;
            //20201006 dataGridView_FailPoint.ReadOnly = true;



            dataGridView_Part.Enabled = true;
            //20201006 dataGridView_Part.ReadOnly = true;

            btSelectPointFail.Enabled = false;
            btBtPart_select.Enabled = false;

            txtResolve_Detail.ReadOnly = true;

            

            }

        private void setComboBox()
        {
            //if (UserInfo.ConnectMode == "0")
            //    strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
            //else

            strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;

            try
            {
                string _typeID = UserInfo.Van_Type;
                string sql = "";

                //20210201 sql = " SELECT POI_ID, PointFailure FROM [PointFailure] WHERE IsCancel = 'Y' Order by OrderNo;  " +
                //20210201     " SELECT POI_ID as 'POI_ID1', PointFailure as 'PointFailure1'  FROM [PointFailure]  WHERE IsCancel = 'Y'  Order by OrderNo;  " +
                sql = " SELECT POI_ID, PointFailure FROM [PointFailure] WHERE IsCancel = 'Y' Order by OrderNo;  " +
                    " SELECT POI_ID as 'POI_ID1', PointFailure as 'PointFailure1'  FROM [PointFailure]  WHERE IsCancel = 'Y'  Order by OrderNo;  " +
                    " SELECT PMT_ID, '['+PMT_ID+'] '+Problem as Detail  FROM JOB_ProblemType where (IsCancel is null or IsCancel = 0)   ORDER BY Detail ;" +
                    " SELECT TRA_ID, '[' + TRA_ID + '] ' +  TransferName as 'Detail' FROM SparePart_TranType where TRA_ID<>'00' AND IsCancel = '0' ORDER BY TRA_ID; " +
                    " SELECT PRI_ID, PriorityDetail from Priority; " +

                    " SELECT [PRT_ID],Convert(varchar,[PRT_ID] ) +': '+[Product_Type] as  [Product_Type] FROM  [Product_Type]; " +
                    //20210129 " SELECT [LOC_ID], Convert(varchar,LOC_ID)+ ': ' + [Location]  as [Location] FROM  [Location] Where (( [LocType] ='D') or ([LOC_ID] = 0) ) Order By [LOC_ID]; " +
                    " SELECT [LOC_ID], Convert(varchar,LOC_ID)+ ': ' + [Location]  as [Location] FROM  [Location] Where IsCancel ='0' Order By [LOC_ID]; " +
                    " SELECT [FAS_ID] ,Convert(varchar,[FAS_ID])+': '+[Failure_Action_Solving] as [Failure_Action_Solving] FROM  [Failure_Action_Solving]; " +
                    " SELECT [FSWI_ID] , [WICode]+': '+[WIDetail] as [WICode] FROM [Failure_Solving_WI] WHERE (JOBType in ('30','31')) ORDER BY [FSWI_ID]; " + //20141224
                                                                                                                                                               //20180731 เพิ่ม
                    " SELECT [FCAT_ID] , Convert(varchar,[FCAT_ID])+': '+[FixedAsset_CateName] as [FCateName] FROM [FFixedAsset_Category] WHERE (IsCancel = 0) ORDER BY [FCAT_ID]; "; //20141224

                SqlDataReader dr = JaSqlHelper.ExecuteReader(strConn, CommandType.Text, sql);
                DataTable dt;

                if (dr.HasRows)
                {
                    //comboBox_PoitFail
                    dt = new DataTable();
                    dt.Load(dr);
                    comboBox_PoitFail.BeginUpdate();
                    comboBox_PoitFail.ValueMember = "POI_ID";
                    comboBox_PoitFail.DisplayMember = "PointFailure";
                    comboBox_PoitFail.DataSource = dt;
                    comboBox_PoitFail.EndUpdate();
                    comboBox_PoitFail.SelectedIndex = -1;

                    //cob_PoitFail0
                    dt = new DataTable();
                    dt.Load(dr);
                    cob_PoitFail0.BeginUpdate();
                    cob_PoitFail0.ValueMember = "POI_ID1";
                    cob_PoitFail0.DisplayMember = "PointFailure1";
                    cob_PoitFail0.DataSource = dt;
                    cob_PoitFail0.EndUpdate();
                    cob_PoitFail0.SelectedIndex = -1;


                    //comProbType
                    dt = new DataTable();
                    dt.Load(dr);
                    cobProblemType.BeginUpdate();
                    cobProblemType.ValueMember = "PMT_ID";
                    cobProblemType.DisplayMember = "Detail";
                    cobProblemType.DataSource = dt;
                    cobProblemType.EndUpdate();
                    cobProblemType.SelectedIndex = -1;

                    //comboBox_SpareTranType
                    dt = new DataTable();
                    dt.Load(dr);
                    cob_TrnType.BeginUpdate();
                    cob_TrnType.ValueMember = "TRA_ID";
                    cob_TrnType.DisplayMember = "Detail";
                    cob_TrnType.DataSource = dt;
                    cob_TrnType.EndUpdate();
                    cob_TrnType.SelectedValue = "IN";

                    //comboBox_Priority
                    dt = new DataTable();
                    dt.Load(dr);
                    comboBox_Priority.BeginUpdate();
                    comboBox_Priority.ValueMember = "PRI_ID";
                    comboBox_Priority.DisplayMember = "PriorityDetail";
                    comboBox_Priority.DataSource = dt;
                    comboBox_Priority.EndUpdate();
                    comboBox_Priority.SelectedIndex = -1;


                    /* //20141127ยกเลิก
                    //cob_Equipment_Position
                    dt = new DataTable();
                    dt.Load(dr);
                    cob_Equipment_Position.BeginUpdate();
                    cob_Equipment_Position.ValueMember = "EQP_ID";
                    cob_Equipment_Position.DisplayMember = "Equipment_Position";
                    cob_Equipment_Position.DataSource = dt;
                    cob_Equipment_Position.EndUpdate();
                    cob_Equipment_Position.SelectedIndex = -1;
                    */

                    //cobOilType
                    dt = new DataTable();
                    dt.Load(dr);
                    cobOilType.BeginUpdate();
                    cobOilType.ValueMember = "PRT_ID";
                    cobOilType.DisplayMember = "Product_Type";
                    cobOilType.DataSource = dt;
                    cobOilType.EndUpdate();
                    cobOilType.SelectedIndex = -1;


                    //cbxLocation
                    dt = new DataTable();
                    dt.Load(dr);
                    cbxLocation.BeginUpdate();
                    cbxLocation.ValueMember = "LOC_ID";
                    cbxLocation.DisplayMember = "Location";
                    cbxLocation.DataSource = dt;
                    cbxLocation.EndUpdate();
                    cbxLocation.SelectedIndex = -1;

                    //cob_Failure_Action_Solving
                    dt = new DataTable();
                    dt.Load(dr);
                    cob_Failure_Action_Solving.BeginUpdate();
                    cob_Failure_Action_Solving.ValueMember = "FAS_ID";
                    cob_Failure_Action_Solving.DisplayMember = "Failure_Action_Solving";
                    cob_Failure_Action_Solving.DataSource = dt;
                    cob_Failure_Action_Solving.EndUpdate();
                    cob_Failure_Action_Solving.SelectedIndex = -1;

                    //cob_SolvingByWI
                    dt = new DataTable();
                    dt.Load(dr);
                    cob_SolvingByWI.BeginUpdate();
                    cob_SolvingByWI.ValueMember = "FSWI_ID";
                    cob_SolvingByWI.DisplayMember = "WICode";
                    cob_SolvingByWI.DataSource = dt;
                    cob_SolvingByWI.EndUpdate();
                    cob_SolvingByWI.SelectedIndex = -1;


                    //cbxChFCAT
                    dt = new DataTable();
                    dt.Load(dr);
                    cbxChFCAT.BeginUpdate();
                    cbxChFCAT.ValueMember = "FCAT_ID";
                    cbxChFCAT.DisplayMember = "FCateName";
                    cbxChFCAT.DataSource = dt;
                    cbxChFCAT.EndUpdate();
                    cbxChFCAT.SelectedIndex = -1;

                }
                dr.Close();
            }
            catch (System.Exception ex)
            {
                closeProgress();
                MessageBox.Show("เกิดข้อผิดพลาด " + ex.Message, "เกิดข้อผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }


        private void ClearControl(Control.ControlCollection objContrl)
        {
            IEnumerator eContrl = null;
            try
            {
                eContrl = objContrl.GetEnumerator();
                while (eContrl.MoveNext())
                {
                    Control cont = (Control)eContrl.Current;

                    if (cont.HasChildren)
                        this.ClearControl(cont.Controls);
                    else
                    {
                        if (cont is TextBox) ((TextBox)cont).Text = "";
                        if (cont is CheckBox) ((CheckBox)cont).Checked = false;
                        if (cont is RadioButton) ((RadioButton)cont).Checked = false;
                        if (cont is ComboBox) ((ComboBox)cont).SelectedIndex = -1;
                        if (cont is ListBox) ((ListBox)cont).Items.Clear();
                        if (cont is NumericUpDown) ((NumericUpDown)cont).Value = ((NumericUpDown)cont).Minimum;
                    }
                }
            }
            //20200218 
            //catch (System.Exception ex)
            //{
            //    MessageBox.Show("เกิดข้อผิดพลาดในการ clear control  " + ex.Message, "เกิดข้อผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
            //
            //==
            catch { }//20200304
            finally
            {
                if (eContrl is IDisposable)
                {
                    ((IDisposable)eContrl).Dispose();
                }
            }
        }



        private void searchData(string txtID, string connectionMode)
        {



            setDtFailure();
            setDtPart();


            //strConn = GetConnection.CheckConnectionString(toolStripText_txtFind.Text.Trim());

            if (editMode == "job_edit")
            {
                strConn = GetConnection.CheckConnectionString(toolStripText_txtFind.Text.Trim());
            }
            else
                strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;

            if (strConn == "")
            {
                closeProgress();
                MessageBox.Show("ไม่มีข้อมูล JOB [" + toolStripText_txtFind.Text + "] กรุณาตรวจสอบเลขที่ JOB ... ป้อนถูกต้องหรือไม่", "ผลการตรวจสอบ",
                      MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            string _typeID = UserInfo.Van_Type;

            lblJobInCount.Text = "";

            Finding.FindJobClose f = new SMSMINI.Transaction.Finding.FindJobClose();

            //if (checkBox_SMS_BAK.Checked == true)
            //    f.isSMSbackup = true;
            //else
            //    f.isSMSbackup = false;

            string vTJobID = "";
            string vTJobOpenDate = "";
            string vTStation = "";

            if (toolStripLabel_cobby.Text == "ตาม JOB ID")
            {
                invoke_Progress("ตรวจสอบข้อมูล JOB ตาม JOB ID");
                f.TmpJobID = toolStripText_txtFind.Text.Trim();
                vTJobID = toolStripText_txtFind.Text.Trim();
                //20191015
                if (editMode == "job_edit" && string.IsNullOrEmpty(toolStripText_txtFind.Text.Trim()))
                {
                    closeProgress();
                    MessageBox.Show("กรุณาระบุ ข้อมูล JOB ที่ต้องการค้นหา", "ผลการตรวจสอบ",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }


            else
            {


                //ทั้งหมด
                if (editMode == "job_edit" && string.IsNullOrEmpty(toolStripText_txtFind.Text.Trim()))
                {
                    closeProgress();

                    MessageBox.Show("กรุณาระบุ ข้อมูล JOB และ เงื่อนไข ที่ต้องการค้นหา", "ผลการตรวจสอบ",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }


                if (editMode == "job_add")
                {
                    f.TmpJobID = "";
                    f.TmpJobOpenDate = "";
                    f.TmpStation = "";

                }


            }

            f.TmpEditMode = editMode;
            f.TmpSearchMode = toolStripLabel_cobby.Text.Trim();
            string _jobID = "";
            closeProgress();


            _pEnddate = null;
            if (f.ShowDialog() == DialogResult.OK)
            {
                _jobID = f.TmpJobID;
                _pEnddate = f.TmpEndDate;//Convert.ToDateTime(f.TmpEndDate);


                if (_jobID != "")
                {

                    //====20200611 ต้อง stop ML ก่อนดึง job มาปิดงาน
                    if (editMode != "job_edit")
                    {
                        string vJobIDML = "";
                        //string vEMP_IDML = "";
                        vJobIDML = _jobID.Trim();// tmpjobID.Trim();
                        //vEMP_IDML = _job.EMP_ID3.Trim(); //tmpVANID.Trim();
                        //strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
                        using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn))
                        {
                            var qJobIDML = (from t in dc.JOB_Detail_GPS_Mileages
                                            where (t.JOB_ID == vJobIDML) &&
                                                   (t.Start_Date != null) &&
                                                   (t.Stop_Date == null) //&&
                                            //(t.VAN == vEMP_IDML)
                                            select t).FirstOrDefault();
                            if (qJobIDML != null)
                            {

                                if (MessageBox.Show("คุณยังไม่ STOP ไมล์เลจ...JOB : " + vJobIDML + Environment.NewLine +
                                      "คุณต้องทำการ STOP ไมล์เลจ ก่อน", "คำยืนยัน", MessageBoxButtons.OK, MessageBoxIcon.Warning) == DialogResult.OK)//, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                {
                                    var url = GetURL.smsURL("JML");
                                    if (url != "")
                                        System.Diagnostics.Process.Start(url);
                                    else
                                        System.Diagnostics.Process.Start("http://sms.flowco.co.th/jml");

                                    return;

                                }
                            }
                        }
                    }
                    //=======================================
                    //try
                    //{
                    setJob_Detail_hist(_jobID, _pEnddate);
                    //}
                    //catch (System.Exception ex) { 
                    //   MessageBox.Show("เกิดข้อผิดพลาด การเข้างานไม่สมบูรณ์ กรุณาตรวจสอบ...", "เกิดข้อผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //   return;
                    //}

                    checkPartRemain();

                    //20191022 disableControl(false);
                    DateTime? srvTime = null;

                    //strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
                    using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn))
                    {

                        srvTime = dc
                           .ExecuteQuery<DateTime>("SELECT GETDATE()")
                           .First();
                    }

                    using (DAL.SMSManage.SMSManageDataContext dc = new DAL.SMSManage.SMSManageDataContext(strConn))
                    {

                        //var qEdit = (from t in dc.JOB_Details
                        //             where t.JOB_ID == txtJobID.Text.Trim()
                        //             group t by t.JOB_ID
                        //                 into gjob
                        //                 select new { 
                        //                     mChkEnd = gjob.Max(t => t.EndDate ) ,
                        //                     jobs_id = t.

                        //                 }).FirstOrDefault();
                        //if (conMA != null && conMA.PriceList > 0)
                        //{ }


                        /* //20200331
                         * var itemList = (from t in dc.JOB_Details
                                        where t.JOB_ID == txtJobID.Text.Trim()
                                        select t).OrderByDescending(c => c.StartDate).First();
                       
                        //if (itemList != null)
                        //{
                        //    if (itemList.EndDate != null)
                        //    {
                        //        //20200127 get ค่าวันที่เวลาล่าสุด ที่บันทึกไปแล้วแล้วดึงมาแก้ไข
                        //        tmpEndDateTimeEditJOB = itemList.EndDate;
                        //        tmpStartDateTimeEditJOB = itemList.StartDate;
                        //        //======

                        //        if (itemList.EndDate.Value.Date >= srvTime.Value.Date)
                        //        {
                        //            disableControl(false);
                        //            toolStripButton_Save.Enabled = true;
                        //            groupBox1.Enabled = true;
                        //            panel_selectEndDate.Visible = false;
                                    
                        //        }
                        //        else 
                        //        {
                        //            disableControl(true);
                        //            panel_selectEndDate.Visible = true;
                        //            toolStripButton_Save.Enabled = false;
                        //            panel_selectEndDate.Enabled = true;
                        //        }
                        //    }
                         */

                        /* //20200331
                        * var itemList = (from t in dc.JOB_Details
                                       where t.JOB_ID == txtJobID.Text.Trim()
                                       select t).OrderByDescending(c => c.StartDate).First();
                        */
                        /* 20200610 ก่อนเลือกเวลาปิดงาน

                        var itemList = (from j in dc.JOBs
                                        join jd in dc.JOB_Details on j.JOB_ID equals jd.JOB_ID
                                        //where new string[] { "07", "99" }.Contains(jd.JOBS_ID)
                                        where j.JOB_ID == txtJobID.Text.Trim()
                                        select jd).OrderByDescending(c => c.StartDate).First();
                        if (itemList != null)
                        {
                            //if (new string[] { "", "99" }.Contains(jd.JOBS_ID))
                            if (itemList.EndDate != null)
                            {
                                //20200127 get ค่าวันที่เวลาล่าสุด ที่บันทึกไปแล้วแล้วดึงมาแก้ไข
                                tmpEndDateTimeEditJOB = itemList.EndDate;
                                tmpStartDateTimeEditJOB = itemList.StartDate;
                                //======

                                //====20200331==เงื่อนไข printSV ,editJOB
                                if (!new string[] { "09", "12", "13" }.Contains(UserInfo.UserLevID))
                                {
                                    if (itemList.EndDate.Value.Date >= srvTime.Value.Date && itemList.IsPrintSV != true)
                                    {
                                        disableControl(false);
                                        toolStripButton_Save.Enabled = true;
                                        groupBox1.Enabled = true;
                                        panel_selectEndDate.Visible = false;

                                    }
                                    else if (itemList.EndDate.Value.Date < srvTime.Value.Date && itemList.IsPrintSV == true)
                                    {//เลยวัน และพิมพ์แล้ว
                                        disableControl(true);
                                        panel_selectEndDate.Visible = true;
                                        toolStripButton_Save.Enabled = false;
                                        panel_selectEndDate.Enabled = true;
                                    }
                                    else {
                                        disableControl(true);
                                        panel_selectEndDate.Visible = true;
                                        toolStripButton_Save.Enabled = false;
                                        panel_selectEndDate.Enabled = true;
                                    }
                                }
                                else {//

                                    disableControl(false);
                                    toolStripButton_Save.Enabled = true;
                                    groupBox1.Enabled = true;
                                    panel_selectEndDate.Visible = false;
                                                           
                                }
                                //====

                                //if (itemList.EndDate.Value.Date >= srvTime.Value.Date)
                                //{
                                //    disableControl(false);
                                //    toolStripButton_Save.Enabled = true;
                                //    groupBox1.Enabled = true;
                                //    panel_selectEndDate.Visible = false;

                                //}
                                //else
                                //{
                                //    disableControl(true);
                                //    panel_selectEndDate.Visible = true;
                                //    toolStripButton_Save.Enabled = false;
                                //    panel_selectEndDate.Enabled = true;
                                //}

                            }
                            else {
                                if(itemList.JOBS_ID == "05" || itemList.JOBS_ID == "06" )
                                {
                                    if (editMode == "job_edit")
                                    {
                                        disableControl(true);
                                        panel_selectEndDate.Visible = false;
                                        toolStripButton_Save.Enabled = false;
                                        panel_selectEndDate.Enabled = false;
                                        toolStripButton_Print.Enabled = true;

                                        // var itemListPast = (from t in dc.JOB_Details
                                        //where t.JOB_ID == txtJobID.Text.Trim() && t.EndDate != null
                                        //select t).OrderByDescending(c => c.StartDate).First();
                                        // if (itemListPast != null)
                                        // {
                                        //     tmpEndDateTimeEditJOB = itemListPast.EndDate;
                                        //     tmpStartDateTimeEditJOB = itemListPast.StartDate;
                                        // }

                                        MessageBox.Show("ไม่สามารถแก้ไข ข้อมูลการปิดงานย้อนหลังได้ ", "ผลการตรวจสอบ",
                                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        return;
                                    }
                                    else {
                                        disableControl(false);
                                        toolStripButton_Save.Enabled = true;
                                    }

                                }

                               
                            }
                        
                        }*/
                        var itemList = (from j in dc.JOBs
                                        join jd in dc.JOB_Details on j.JOB_ID equals jd.JOB_ID
                                        //where new string[] { "07", "99" }.Contains(jd.JOBS_ID)
                                        where j.JOB_ID == txtJobID.Text.Trim()
                                        select jd).OrderByDescending(c => c.StartDate).First();
                        if (itemList != null)
                        {
                            //if (new string[] { "", "99" }.Contains(jd.JOBS_ID))
                            if (itemList.EndDate != null)
                            {
                                ////20200610 เปลี่ยนไปใช้ตามวันที่ปิดงานที่เลือก //20200127 get ค่าวันที่เวลาล่าสุด ที่บันทึกไปแล้วแล้วดึงมาแก้ไข
                                //tmpEndDateTimeEditJOB = itemList.EndDate;
                                //tmpStartDateTimeEditJOB = itemList.StartDate;
                                ////======


                                try
                                {//20201007
                                 //======20200610
                                    DateTime _authEnd = DateTime.Parse(_pEnddate);

                                    var itemListChEndate = (from j in dc.JOBs
                                                            join jd in dc.JOB_Details on j.JOB_ID equals jd.JOB_ID
                                                            //where new string[] { "07", "99" }.Contains(jd.JOBS_ID)
                                                            where j.JOB_ID == txtJobID.Text.Trim()
                                                             && jd.EndDate.Value.Date == _authEnd.Date
                                                             && jd.EndDate.Value.Hour == _authEnd.Hour
                                                             && jd.EndDate.Value.Minute == _authEnd.Minute
                                                             && jd.EndDate.Value.Second == _authEnd.Second
                                                             && jd.StatusDetail == true //20201208
                                                            select jd).First();

                                    tmpEndDateTimeEditJOB = itemListChEndate.EndDate;
                                    tmpStartDateTimeEditJOB = itemListChEndate.StartDate;
                                    //=============


                                    //====20200331==เงื่อนไข printSV ,editJOB
                                    if (!new string[] { "09", "12", "13" }.Contains(UserInfo.UserLevID))
                                    {
                                        //20200610 if (itemList.EndDate.Value.Date >= srvTime.Value.Date && itemList.IsPrintSV != true)
                                        if (itemListChEndate.EndDate.Value.Date >= srvTime.Value.Date && itemListChEndate.IsPrintSV != true)
                                        {
                                            disableControl(false);
                                            toolStripButton_Save.Enabled = true;
                                            groupBox1.Enabled = true;
                                            panel_selectEndDate.Visible = false;

                                        }
                                        //20200610 else if (itemList.EndDate.Value.Date < srvTime.Value.Date && itemList.IsPrintSV == true)
                                        else if (itemListChEndate.EndDate.Value.Date < srvTime.Value.Date && itemListChEndate.IsPrintSV == true)
                                        {//เลยวัน และพิมพ์แล้ว
                                            disableControl(true);
                                            panel_selectEndDate.Visible = true;
                                            toolStripButton_Save.Enabled = false;
                                            panel_selectEndDate.Enabled = true;
                                        }
                                        else
                                        {
                                            disableControl(true);
                                            panel_selectEndDate.Visible = true;
                                            toolStripButton_Save.Enabled = false;
                                            panel_selectEndDate.Enabled = true;
                                        }
                                    }
                                    else
                                    {//

                                        disableControl(false);
                                        toolStripButton_Save.Enabled = true;
                                        groupBox1.Enabled = true;
                                        panel_selectEndDate.Visible = false;

                                    }

                                }
                                catch (System.Exception ex)//20201007
                                {

                                    MessageBox.Show("เกิดข้อผิดพลาด ข้อมูลการเข้างานและข้อมูลการปิดงาน กรุณาตรวจสอบ...", "เกิดข้อผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    return;
                                }
                                //====

                                //if (itemList.EndDate.Value.Date >= srvTime.Value.Date)
                                //{
                                //    disableControl(false);
                                //    toolStripButton_Save.Enabled = true;
                                //    groupBox1.Enabled = true;
                                //    panel_selectEndDate.Visible = false;

                                //}
                                //else
                                //{
                                //    disableControl(true);
                                //    panel_selectEndDate.Visible = true;
                                //    toolStripButton_Save.Enabled = false;
                                //    panel_selectEndDate.Enabled = true;
                                //}

                            }
                            else
                            {
                                if (itemList.JOBS_ID == "05" || itemList.JOBS_ID == "06")
                                {
                                    if (editMode == "job_edit")
                                    {
                                        disableControl(true);
                                        panel_selectEndDate.Visible = false;
                                        toolStripButton_Save.Enabled = false;
                                        panel_selectEndDate.Enabled = false;
                                        toolStripButton_Print.Enabled = true;

                                        // var itemListPast = (from t in dc.JOB_Details
                                        //where t.JOB_ID == txtJobID.Text.Trim() && t.EndDate != null
                                        //select t).OrderByDescending(c => c.StartDate).First();
                                        // if (itemListPast != null)
                                        // {
                                        //     tmpEndDateTimeEditJOB = itemListPast.EndDate;
                                        //     tmpStartDateTimeEditJOB = itemListPast.StartDate;
                                        // }
                                        closeProgress();
                                        //20201208 MessageBox.Show("ไม่สามารถแก้ไข ข้อมูลการปิดงานย้อนหลังได้ ", "ผลการตรวจสอบ",
                                        MessageBox.Show("ไม่สามารถแก้ไข ข้อมูลการปิดงานย้อนหลังได้ " + Environment.NewLine +
                                        " สถานะ JOB ปัจจุบัน : เข้างาน ", "ผลการตรวจสอบ",
                                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        return;
                                    }
                                    else
                                    {
                                        disableControl(false);
                                        toolStripButton_Save.Enabled = true;
                                    }
                                }
                            }//05,06
                        }//if (itemList != null)
                    }//dal

                    //==================


                    //20210322เปลี่ยนเงื่อนไขอีกแล้วถ้ามี เก็บเงินสด ให้แก้ได้ //====20200708===มี invoice แล้ว ห้ามแก้ไข job========
                    if (editMode == "job_edit")
                    {
                        using (DAL.SMSManage.SMSManageDataContext dc = new DAL.SMSManage.SMSManageDataContext(strConn))
                        {

                            //-- model 1
                            //var qJEnd = (from x in dc.JOB_Detail_Spares
                            //             where x.JOB_ID == txtJobID.Text.Trim()
                            //             select x).OrderByDescending(c => c.Enddate).FirstOrDefault();

                            //if (qJEnd != null)
                            //{
                            //    var qNEdit = dc.JOB_Invoices
                            //               .Where(t => t.JOB_ID == txtJobID.Text.Trim() && t.Invoice_Date >= qJEnd.Enddate).FirstOrDefault();
                            //    if (qNEdit != null)
                            //    {

                            //==============================
                            //--model 2
                            //var qJDEnd = (from x in dc.JOB_Detail_Spares
                            //            where x.JOB_ID == txtJobID.Text.Trim()
                            //            select x).OrderByDescending(c => c.Enddate).FirstOrDefault();

                            //var qJIDate = (from t in dc.JOB_Invoices
                            //            where t.JOB_ID == txtJobID.Text.Trim()
                            //            select t).OrderByDescending(c => c.Invoice_Date).FirstOrDefault();

                            //   if (qJDEnd != null && qJIDate != null)
                            //   {
                            //       if(qJDEnd.Enddate.Value.Date <= qJIDate.Invoice_Date.Date)
                            //       {
                            //           disableControl(true);
                            //           panel_selectEndDate.Visible = true;
                            //           toolStripButton_Save.Enabled = false;
                            //           panel_selectEndDate.Enabled = true;
                            //           MessageBox.Show("ไม่สามารถแก้ไข ข้อมูลการปิดงานย้อนหลังได้ เนื่องจาก ออก Invoice แล้ว ", "ผลการตรวจสอบ",
                            //                       MessageBoxButtons.OK, MessageBoxIcon.Information);
                            //           return;
                            //       }
                            //   }   
                            //===============================================
                            //--model 3

                            /* //20210322 ก่อนแก้ไขเงื่อนไขเรื่อง เก็บเงินสด
                                DateTime _vpEnddate = DateTime.Parse(_pEnddate);
                                var qJDEnd = (from t in dc.JOB_Detail_Spares
                                              where t.JOB_ID.Trim() == txtJobID.Text.Trim()
                                              && t.Enddate.Value.Date == _vpEnddate.Date
                                              && t.Enddate.Value.Hour == _vpEnddate.Hour
                                              && t.Enddate.Value.Minute == _vpEnddate.Minute
                                              && t.Enddate.Value.Second == _vpEnddate.Second                                        
                                              select t).OrderByDescending(c => c.Enddate).FirstOrDefault();

                                var qJIDate = (from t in dc.JOB_Invoices
                                               where t.JOB_ID == txtJobID.Text.Trim()
                                               select t).OrderByDescending(c => c.Invoice_Date).FirstOrDefault();

                                if (qJDEnd != null && qJIDate != null)
                                {
                                    if (qJDEnd.Enddate.Value.Date <= qJIDate.Invoice_Date.Date)
                                    {
                                        //20202001 disableControl(true);
                                        disableControlInv(true);
                                        //panel_selectEndDate.Visible = true;
                                        //20201001 toolStripButton_Save.Enabled = false;
                                        //panel_selectEndDate.Enabled = true;
                                        MessageBox.Show("ไม่สามารถแก้ไข ข้อมูลการปิดงานย้อนหลังได้ เนื่องจาก ออก Invoice แล้ว ", "ผลการตรวจสอบ",
                                                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        return;
                                    }
                                }
                            }
                        }
                     */



                            //20210322 แก้ไขเงื่อนไขเรื่อง เก็บเงินสด
                            DateTime _vpEnddate = DateTime.Parse(_pEnddate);

                            var qIsPrice = (from t in dc.JOB_Detail_Spares
                                            where t.JOB_ID.Trim() == txtJobID.Text.Trim()
                                            && t.Enddate.Value.Date == _vpEnddate.Date
                                            && t.Enddate.Value.Hour == _vpEnddate.Hour
                                            && t.Enddate.Value.Minute == _vpEnddate.Minute
                                            && t.Enddate.Value.Second == _vpEnddate.Second
                                            && t.IsPrices == true //20210322 เก็บเงินสด
                                            select t).OrderByDescending(c => c.Enddate).FirstOrDefault();
                            if (qIsPrice != null)
                            {
                                // disableControlInv(false);
                            }
                            else
                            {
                                var qJDEnd = (from t in dc.JOB_Detail_Spares
                                              where t.JOB_ID.Trim() == txtJobID.Text.Trim()
                                              && t.Enddate.Value.Date == _vpEnddate.Date
                                              && t.Enddate.Value.Hour == _vpEnddate.Hour
                                              && t.Enddate.Value.Minute == _vpEnddate.Minute
                                              && t.Enddate.Value.Second == _vpEnddate.Second
                                              select t).OrderByDescending(c => c.Enddate).FirstOrDefault();

                                var qJIDate = (from t in dc.JOB_Invoices
                                               where t.JOB_ID == txtJobID.Text.Trim()
                                               select t).OrderByDescending(c => c.Invoice_Date).FirstOrDefault();

                                if (qJDEnd != null && qJIDate != null)
                                {
                                    //เงื่อนไขนี้เช็คตอน isprice = 0 ทั้งหมด
                                    if (qJDEnd.Enddate.Value.Date <= qJIDate.Invoice_Date.Date)
                                    {
                                        //20202001 disableControl(true);
                                        disableControlInv(true);
                                        //panel_selectEndDate.Visible = true;
                                        //20201001 toolStripButton_Save.Enabled = false;
                                        //panel_selectEndDate.Enabled = true;
                                        MessageBox.Show("ไม่สามารถแก้ไข ข้อมูลการปิดงานย้อนหลังได้ เนื่องจาก ออก Invoice แล้ว ", "ผลการตรวจสอบ",
                                                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        return;

                                    }
                                }
                            }
                            ////=====20210817=====จ่าย comm แล้วห้ามแก้====ก่อนเปลี่ยนไปใช้ sp ======

                            //var qComm = (from t in dc.JOB_Statement_Commissions
                            //             where t.JOB_ID == txtJobID.Text.Trim() && t.PayDate != null
                            //             select t).FirstOrDefault();
                            //if (qComm != null)
                            //{
                            //    disableControlInv(true);
                            //    MessageBox.Show("ไม่สามารถแก้ไข ข้อมูลการปิดงานย้อนหลังได้ เนื่องจาก จ่ายค่า Commission แล้ว ", "ผลการตรวจสอบ",
                            //                MessageBoxButtons.OK, MessageBoxIcon.Information);
                            //    return;
                            //}    

                        }//DAL                    
                        //================================

                        using (System.Data.SqlClient.SqlConnection Conn = new System.Data.SqlClient.SqlConnection(strConn))
                        {
                            if (Conn.State == ConnectionState.Closed)
                                Conn.Open();
                            //============================
                            System.Data.SqlClient.SqlDataReader dr = null;

                            string sqlC = "";
                            sqlC = " Declare @IsCommission INT " +
                                  " Exec @IsCommission = JobIsCommission '" + txtJobID.Text.Trim() + "'" +
                                  " Select @IsCommission ";


                            using (dr = new System.Data.SqlClient.SqlCommand(sqlC, Conn).ExecuteReader())
                            {
                                dr.Read();
                                if (dr.HasRows)
                                {
                                    
                                        disableControlInv(true);
                                        MessageBox.Show("ไม่สามารถแก้ไข ข้อมูลการปิดงานย้อนหลังได้ เนื่องจาก จ่ายค่า Commission แล้ว ", "ผลการตรวจสอบ",
                                                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        return;
                                    
                                }

                            }
                        }



                    }










                        //==============================================

                        /*//20191219 smsmini2 online ยกเลิก process upload แล้ว

                        //20151105 ป้องกัน ถ้า upload job แล้วห้ามเลือกจุดเสียและเลือกอะไหล่

                        //strConn = Properties.Settings.Default["ServicesMSDBConnectionString"].ToString();
                        using (DAL.SMSManage.SMSManageDataContext dc = new DAL.SMSManage.SMSManageDataContext(strConn))
                        {
                            var j = dc.JOBs
                                .Where(t => t.JOB_ID == txtJobID.Text.Trim() && new string[] { "99", "07" }.Contains(t.JOBS_ID) && t.IsUpload == '9')
                                .FirstOrDefault();

                            if (j != null)
                            {
                                btSelectPointFail.Enabled = false;
                                btBtPart_select.Enabled = false;
                                toolStripButton_Save.Enabled = false;

                            }

                        }
                         */


                    }

            }



        }


        private void setComboBox_PoitFail(string vJOBType)
        {
            //strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;

            if (editMode == "job_edit")
                strConn = GetConnection.Connection;
            else
                strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;



            try
            {
                string _typeID = UserInfo.Van_Type;
                string sql = "";
                /*//20150212  sql = "SELECT POI_ID, PointFailure FROM [PointFailure] WHERE (JOBType = '" + vJOBType + "')  Order by OrderNo; " +
                          " SELECT [FSWI_ID] , [WICode] FROM [Failure_Solving_WI] WHERE (JOBType = '" + vJOBType + "' or FSWI_ID = '0' )   ORDER BY [FSWI_ID]; ";
                 */
                sql = "SELECT POI_ID, PointFailure FROM [PointFailure] WHERE IsCancel = 'Y' Order by OrderNo; " +
                         //" SELECT [FSWI_ID] , [WICode] FROM [Failure_Solving_WI] WHERE (JOBType = '" + vJOBType + "' or FSWI_ID = '0' )   ORDER BY [FSWI_ID]; ";
                         " SELECT [FSWI_ID] , [WICode]+': '+[WIDetail] as [WICode] FROM [Failure_Solving_WI] WHERE (JOBType = '" + vJOBType + "' or FSWI_ID = '0' )   ORDER BY [FSWI_ID]; ";


                SqlDataReader dr = JaSqlHelper.ExecuteReader(strConn, CommandType.Text, sql);
                DataTable dt;

                if (dr.HasRows)
                {
                    //comboBox_PoitFail
                    dt = new DataTable();
                    dt.Load(dr);
                    comboBox_PoitFail.BeginUpdate();
                    comboBox_PoitFail.ValueMember = "POI_ID";
                    comboBox_PoitFail.DisplayMember = "PointFailure";
                    comboBox_PoitFail.DataSource = dt;
                    comboBox_PoitFail.EndUpdate();
                    comboBox_PoitFail.SelectedIndex = -1;

                    //cob_SolvingByWI
                    dt = new DataTable();
                    dt.Load(dr);
                    cob_SolvingByWI.BeginUpdate();
                    cob_SolvingByWI.ValueMember = "FSWI_ID";
                    cob_SolvingByWI.DisplayMember = "WICode";
                    cob_SolvingByWI.DataSource = dt;
                    cob_SolvingByWI.EndUpdate();
                    cob_SolvingByWI.SelectedIndex = -1;//0;//-1;

                }
                dr.Close();
            }
            catch (System.Exception ex)
            {
                closeProgress();
                MessageBox.Show("เกิดข้อผิดพลาด " + ex.Message, "เกิดข้อผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        decimal _ActualDistance = 0;
        decimal _DistanceFromCenter = 0;
        string _TYP_ID1 = "";


        //20200608 private void setJob_Detail_hist(string jobid)
        //20200609 
        private void setJob_Detail_hist(string jobid, string _pEnddate)
        //private void setJob_Detail_hist(string jobid, DateTime _pEnddate)
        {
            //strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
            if (editMode == "job_edit")
                strConn = GetConnection.Connection;
            else
                strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;

            using (DAL.SMSManage.SMSManageDataContext db = new DAL.SMSManage.SMSManageDataContext(strConn))
            {

                #region GetVAT
                var _v = db.VATs.Where(t => t.VAT_ID == "1"[0]).FirstOrDefault();
                if (_v != null)
                    _vat = _v.VAT1.Value;

                #endregion

                #region JOB
                string tmpQuarter = "";

                var job = db.JOBs
                    .Where(t => t.JOB_ID.Trim() == jobid.Trim())
                    .FirstOrDefault();

                _job = job;

                //lsJOB

                JOB_IS_PrintPrice(db);
                tmpTYP_ID1 = job.TYP_ID1;


                if (!string.IsNullOrEmpty(_job.ERP_documentno))
                    txtJobSO.Text = _job.ERP_documentno;

                //==20151106 เพิ่ม workOrder
                txtWorkID.Text = "";
                var jobWO = db.JOB_WorkIDs
                   .Where(t => t.JOB_ID.Trim() == job.JOB_ID)
                   .FirstOrDefault();

                if (jobWO != null)
                {
                    if (!string.IsNullOrEmpty(jobWO.Work_ID))
                        txtWorkID.Text = jobWO.Work_ID;
                }


                //==

                _contractNo = string.IsNullOrEmpty(job.ContractNo) ? string.Empty : job.ContractNo;

                comboBox_Priority.SelectedValue = job.PRI_ID.Trim();


                //====20141201
                setComboBox_PoitFail(job.TYP_ID.Trim());



                //RadioButtonList_JobStatus
                if (editMode == "job_edit")//แก้ไข
                {
                    /*20210713
                  if (job.JOBS_ID.Trim() != "99")
                      RadioButtonList_JobStatus1.Checked = true;
                  else
                      RadioButtonList_JobStatus2.Checked = true;//Close JOB
                   */

                    //=======20210713===================
                    DateTime _vpEnddateEdit = DateTime.Parse(_pEnddate);
                    var jobEdit = ((from t in db.JOB_Details
                                    where t.JOB_ID.Trim() == jobid.Trim()
                                   && t.StatusDetail == true
                                   ////&& Convert.ToDateTime(t.EndDate) == _pEnddate//_vpEnddate
                                   && t.EndDate.Value.Date == _vpEnddateEdit.Date
                                   && t.EndDate.Value.Hour == _vpEnddateEdit.Hour
                                   && t.EndDate.Value.Minute == _vpEnddateEdit.Minute
                                   && t.EndDate.Value.Second == _vpEnddateEdit.Second
                                    select t).FirstOrDefault());
                    if (jobEdit.JOBS_ID.Trim() != "99")
                    {
                        RadioButtonList_JobStatus1.Checked = true;
                    }
                    else
                    {
                        RadioButtonList_JobStatus2.Checked = true;//Close JOB

                    }

                    //=====================
                }

                string uopenJob = "...";

                if ((from t in db.Employees where t.EMP_ID.Trim() == job.EMP_ID0.Trim() select t).Count() > 0)
                {
                    var u = (from t in db.Employees where t.EMP_ID.Trim() == job.EMP_ID0.Trim() select t).FirstOrDefault();
                    uopenJob = u.FName ?? "";
                }

                var sGopID = (from t in db.Stations where t.STA_ID.Trim() == job.STA_ID.Trim() select t).FirstOrDefault();

                var xStation_Group = db.Station_Groups.Where(t => t.GOP_ID == sGopID.GOP_ID).FirstOrDefault();

                /*20170503  SVSAM_Offline ต้อง comment  if gop  */
                if (sGopID != null)
                {
                    tmpGOPId = sGopID.GOP_ID;//ใช้ map คำนวญ Price list
                    tmpSTAID = sGopID.STA_ID;
                    tmpGopName = xStation_Group.GroupStation ?? sGopID.GOP_ID;
                }

                var s = (from t in db.Stations where t.STA_ID.Trim() == job.STA_ID.Trim() select t).FirstOrDefault();
                var spro = db.Provinces.Where(t => t.PRO_ID == s.PRO_ID).FirstOrDefault();

                var jt = (from t in db.Types where t.TYP_ID.Trim() == job.TYP_ID.Trim() select t).FirstOrDefault();

                /*//20200609
                 * var inc = (from t in db.JOB_Details where t.JOB_ID.Trim() == job.JOB_ID.Trim() && t.StatusDetail == true select t).Count();
                //ดึงเวลาเข้าครั้งล่าสุด
                var dateIn = ((from t in db.JOB_Details where t.JOB_ID.Trim() == job.JOB_ID.Trim() && t.StatusDetail == true orderby t.RecDate descending select t).FirstOrDefault());
                */
                //-==20200609
                DateTime _vpEnddate;
                if (editMode == "job_edit")//แก้ไข
                {
                    //_vpEnddate = Convert.ToDateTime(_pEnddate); //ส่งลงมาเป็น string
                    //string _xpEnddate = _pEnddate+"999";
                    _vpEnddate = DateTime.Parse(_pEnddate);

                    var inc = (from t in db.JOB_Details
                               where t.JOB_ID.Trim() == job.JOB_ID.Trim()
                               && t.StatusDetail == true
                               //           //ไม่ได้ && t.EndDate.ToString("dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("en-US")) <= _pEnddate//_vpEnddate
                               //           //ไม่ได้  && Convert.ToDateTime(t.EndDate) < _pEnddate
                               //           // 3 ได้ 2 && System.Convert.ToDateTime(t.EndDate.Value) <= System.Convert.ToDateTime(_pEnddate)  
                               //           //ไม่ได้ && t.EndDate.Value.ToString("dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("en-US")) <= System.Convert.ToDateTime(_pEnddate).ToString("dd/MM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("en-US"))
                               //ไม่ได้&& t.EndDate.Value  <=  System.Convert.ToDateTime(xc)
                               && t.EndDate.Value <= _vpEnddate.AddSeconds(+1)
                               select t).Count();



                    //ดึงเวลาเข้าครั้งที่เลือก
                    var dateIn = ((from t in db.JOB_Details
                                   where t.JOB_ID.Trim() == job.JOB_ID.Trim()
                                   && t.StatusDetail == true
                                   ////&& Convert.ToDateTime(t.EndDate) == _pEnddate//_vpEnddate
                                   && t.EndDate.Value.Date == _vpEnddate.Date
                                   && t.EndDate.Value.Hour == _vpEnddate.Hour
                                   && t.EndDate.Value.Minute == _vpEnddate.Minute
                                   && t.EndDate.Value.Second == _vpEnddate.Second

                                   select t).FirstOrDefault());
                    tmpVANID = dateIn.EMP_ID;//20200205 for chk ML
                    txtStartDateIn.Text = dateIn == null ? "" : dateIn.StartDate.ToString("dd/MM/yyyy HH:mm:sss", new System.Globalization.CultureInfo("th-TH"));
                    lblJobInCount.Text = inc.ToString();// เข้าครั้งที่
                }
                else
                {
                    //20201008 
                    var inc = (from t in db.JOB_Details where t.JOB_ID.Trim() == job.JOB_ID.Trim() && t.StatusDetail == true select t).Count();
                    //var inc = (from t in db.JOB_Details 
                    //           where t.JOB_ID.Trim() == job.JOB_ID.Trim() 
                    //           && t.StatusDetail == true 
                    //           && new string[] { "06", "05" }.Contains(t.JOBS_ID) 
                    //           select t).Count();
                    //ดึงเวลาเข้าครั้งล่าสุด
                    try
                    {
                        //20201008  var dateIn = ((from t in db.JOB_Details where t.JOB_ID.Trim() == job.JOB_ID.Trim() && t.StatusDetail == true orderby t.RecDate descending select t).FirstOrDefault());
                        var dateIn = ((from t in db.JOB_Details
                                       where t.JOB_ID.Trim() == job.JOB_ID.Trim()
                                       && t.StatusDetail == true
                                       && new string[] { "06", "05" }.Contains(t.JOBS_ID)
                                       orderby t.RecDate descending
                                       select t).FirstOrDefault());
                        tmpVANID = dateIn.EMP_ID;//20200205 for chk ML
                        txtStartDateIn.Text = dateIn == null ? "" : dateIn.StartDate.ToString("dd/MM/yyyy HH:mm:sss", new System.Globalization.CultureInfo("th-TH"));
                        lblJobInCount.Text = inc.ToString();// เข้าครั้งที่
                    }
                    catch (System.Exception ex)//20201007
                    {

                        MessageBox.Show("เกิดข้อผิดพลาด การเข้างานไม่สมบูรณ์ กรุณาตรวจสอบ...", "เกิดข้อผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        disableControl(true);
                        return;
                    }
                }
                //-==========

                //ระยะทาง
                var sd = (from t in db.Station_Distances where t.STA_ID.Trim() == job.STA_ID.Trim() && t.TYP_ID == job.TYP_ID select t).FirstOrDefault();
                if (sd != null)
                {
                    _ActualDistance = sd.ActualDistance;
                    _DistanceFromCenter = sd.DistanceFromCenter ?? 0;
                }
                //*******************************************************************
                //รายละเอียดใบ JOB
                //*******************************************************************                
                txtJobID.Text = job.JOB_ID ?? "";
                tmpjobID = txtJobID.Text.Trim();//เก็ยไว้นำไปพิมพ์ SV
                tmpJobType = job.TYP_ID; //เก็บประเภท JOB
                //20200609 tmpVANID = dateIn.EMP_ID;//20200205 for chk ML

                txtProjectNO.Text = job.PROJ_ID ?? "";
                _Jobopendate = job.Opendate.Value;

                tmpQuarter = ConnectionManager.dates_Quarter(Convert.ToDateTime(job.Opendate.Value));
                toolStripLabel_QUARTER.Text = "QUARTER: " + tmpQuarter;



                txtOpenJOB.Text = job.Opendate.ToString();
                //20200609 txtStartDateIn.Text = dateIn == null ? "" : dateIn.StartDate.ToString("dd/MM/yyyy HH:mm:sss", new System.Globalization.CultureInfo("th-TH"));
                //txtPointFaile.Text =  txtPointFaile.Text = job.PointFailure.PointFailure1 ?? "";
                txtPointFaile.Text = job.PointFailure.PointFailure1 ?? "";
                txtFialureCode.Text = job.FAI_ID ?? "";
                FailureDetail.Text = job.JobFailure_Detail ?? "";
                txtInformer.Text = (job.Informer ?? "") + " " + (job.InPhone ?? "");
                txtUserOpenJob.Text = uopenJob ?? "";

                txtStation.Text = (s.STA_ID1 ?? s.STA_ID) + ": " +
                    (s.StationSys ?? "") + " ที่อยู่:" +
                    (s.Address ?? "") + " จ." +
                    (spro == null ? "<ยังไม่ระบุ ข้อมูล>" : (spro.Province1)) + " ระยะทาง:" +
                    _ActualDistance.ToString() + " กม. ห่างจากศูนย์: " +
                    _DistanceFromCenter.ToString(); // StationSys

                lbl_JobType.Text = jt.Type1 ?? "ไม่ระบุ";//ประเภท JOB
                //20200609lblJobInCount.Text = inc.ToString();// เข้าครั้งที่

                var workType = (from t in db.Types where t.TYP_ID == job.TYP_ID1 select t).FirstOrDefault();
                txtWorkType.Text = workType == null ? "" : workType.TYP_ID + ":" + workType.Type1;
                _TYP_ID1 = workType == null ? "" : workType.TYP_ID;


                //20130926
                if (job.IsStationCharge == true)
                {
                    closeProgress();
                    MessageBox.Show("เลขที่ Job." + job.JOB_ID.ToString() + " เก็บเงินสดที่สถานีบริการ " + Environment.NewLine +
                                "กรุณาตรวจสอบ...", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }



                //เก็บเงินสดหน้าสถานี
                if (job.IsStationCharge == true)
                {
                    _IsStationCharge = true;
                }







                #region ล็อก TranType ตามเงื่อนไขสัญญา
                var _jobid = txtJobID.Text.Substring(4, 1);

                if (new string[] { "D", "S" }.Contains(_jobid))
                {

                    var qlt = db.Conf_JOB_IsLockTranTypes
                        .Where(t => t.GOP_ID == tmpGOPId &&
                                t.TYP_ID == _job.TYP_ID &&
                                t.TYP_ID1 == _job.TYP_ID1).FirstOrDefault();


                    if (qlt != null)
                        isLockTranType = true;
                    else
                        isLockTranType = false;


                }
                else
                {
                    isLockTranType = false;
                }

                lockTranType();

                if (workType.TYP_ID == "OU")// OU = งาน นอกสัญญาทั่วไป
                {
                    //cob_TrnType_SetNewItem(db);
                }

                #endregion


                //get Contract
                try
                {
                    lblIsContract.Visible = true;
                    txtContract.Visible = true;

                    var smg = new string[] { "09", "12", "13" }.Contains(UserInfo.UserLevID);
                    if (smg && !strConn.Contains("ServicesMSDB_") && editMode == "job_edit")
                    {
                        btEditContractNo.Visible = true;
                        btUpdatePart.Visible = true;
                        btUpdateWorkType.Enabled = true;
                        btUpdateWorkType.BackColor = Color.Lime;

                        label_1.Visible = true;
                        label_2.Visible = true;
                        label_3.Visible = true;
                    }
                    else
                    {
                        btEditContractNo.Visible = false;
                        btUpdatePart.Visible = false;
                        btUpdateWorkType.Enabled = false;
                        btUpdateWorkType.BackColor = Color.Transparent;

                        label_1.Visible = false;
                        label_2.Visible = false;
                        label_3.Visible = false;
                    }


                    if (s.IsContract != null)//if (s.xContract != null)
                    {
                        lblIsContract.Text = (s.xContract.Value == "0"[0] || s.xContract.Value.Equals(null)) ? "สถานีในสัญญา: NO" : "สถานีในสัญญา: YES";


                        txtContract.Text =
                       "สถานี: " + (s.StationSys ?? "") + Environment.NewLine +
                       "กลุ่มบริษัท: " + xStation_Group.GroupStation + Environment.NewLine +
                       "หมดประกัน: " + (s.WarrantyDate ?? null) + Environment.NewLine +
                       "ติดตั้ง: " + (s.InstallDate ?? null) + Environment.NewLine +
                       "เลขที่สัญญา: " + _contractNo;
                    }
                    else
                    {
                        lblIsContract.Text = "สถานีในสัญญา: NO";
                        txtContract.Text =
                       "สถานี: " + (s.StationSys ?? "") + Environment.NewLine +
                       "กลุ่มบริษัท: " + xStation_Group.GroupStation + Environment.NewLine +
                       "หมดประกัน:-- " + Environment.NewLine +
                       "ติดตั้ง:-- " + Environment.NewLine +
                       "เลขที่สัญญา: " + _contractNo;
                    }
                }
                catch (Exception)
                {

                    //throw;
                }

                #endregion


                #region dataGridView_tmpPart

                var part = from t in db.JOB_Detail_Spares where t.JOB_ID.Trim() == jobid.Trim() select t;
                if (part.Count() > 0)
                {
                    dataGridView_tmpPart.DataSource = part.ToList();
                    dataGridView_tmpPart.Refresh();

                    ////---เปลี่ยนแปลงเงื่อนไขให้ printได้ทุกวันที่ปิดงานค้างและปิดงาน 20151217
                    //var _endDate = (from t in db.JOB_Detail_Spares where t.JOB_ID.Trim() == jobid.Trim() select t.Enddate).Distinct();
                    ////--20150813 เพิ่ม clear  วันที่ f8 เดิม
                    //cobEndDate.Items.Clear();

                    ////cobEndDate.SelectedItem= "";

                    ////cobEndDate.Items.Add("");
                    ////------------------------------------
                    //foreach (var endDate in _endDate)
                    //{
                    //    if (endDate.Value != null)
                    //    {
                    //        cobEndDate.Items.Add(endDate.Value);

                    //    }
                    //}
                    ////---เปลี่ยนแปลงเงื่อนไขให้ printได้ทุกวันที่ปิดงานค้างและปิดงาน

                    //===== 20151217
                    cobEndDate.Items.Clear();




                }// var part 
                else
                {
                    dataGridView_tmpPart.DataSource = null;
                    dataGridView_tmpPart.Refresh();
                    //cobEndDate.Items.Clear();


                    ////=== 20150831 ====
                    cobEndDate.Items.Clear();



                }


                if (cobEndDate.Items.Count < 1)
                {
                    var _endDate = db.JOB_Details
                        .Where(t => t.JOB_ID.Trim() == jobid.Trim() && t.EndDate != null && new string[] { "99", "07" }.Contains(t.JOBS_ID))
                        .Distinct();

                    if (_endDate.Count() > 0)
                    {
                        //--20150813 เพิ่ม clear  วันที่ f8 เดิม
                        cobEndDate.Items.Clear();
                        //cobEndDate.SelectedItem = "";
                        //cobEndDate.Items.Add("");
                        //cobEndDate.SelectedValue = "";
                        //------------------------------------
                        foreach (var endDate in _endDate)
                        {
                            if (endDate.EndDate.Value != null)
                            {
                                cobEndDate.Items.Add(endDate.EndDate.Value);
                            }
                        }
                    }
                }

                #endregion


                logCancelSPA_ID = new List<string>();


                if (editMode == "job_edit")//20200608 เปลี่ยนเป็นดึงข้อมูลตามเวลาปิดงานที่เลือก //แบบเดิม ถ้าเป็นการแก้ไขต้องดึงอะใหล่มาแสดงทั้งหมดใน Job นั้น
                {
                    #region job_edit

                    //20200608
                    _vpEnddate = Convert.ToDateTime(_pEnddate);

                    var q1 = from t in db.JOB_Detail_Spares
                             from p in db.PointFailures
                             from sp in db.SpareParts
                             where t.JOB_ID.Trim() == jobid.Trim() &&
                                t.POI_ID.Trim() == p.POI_ID.Trim() &&
                                t.SPA_ID == sp.SPA_ID
                              //20200608 
                              //&& t.Enddate == _vpEnddate
                              && t.Enddate.Value.Date == _vpEnddate.Date
                              && t.Enddate.Value.Hour == _vpEnddate.Hour
                              && t.Enddate.Value.Minute == _vpEnddate.Minute
                              && t.Enddate.Value.Second == _vpEnddate.Second
                             //=======
                             select new
                             {
                                 POI_ID = p.POI_ID,
                                 PoiFailure = p.PointFailure1,
                                 t.NO,
                                 SPA_ID = t.SPA_ID,
                                 PartName = sp.SparePart1,

                                 VAT = t.VAT ?? 0,
                                 t.ControlFixAssetNo,
                                 PricesList = t.PricesList ?? 0,
                                 Prices = t.Prices ?? 0,
                                 SumPrices = t.SumPrices ?? 0,
                                 SumVAT = t.SumVAT ?? 0,
                                 SumTotalPrices = t.SumTotalPrices ?? 0,
                                 Discount = t.Discount ?? 0,

                                 Quantity = t.Quantity ?? 0,
                                 StatusSpare = t.StatusSpare,
                                 TRA_ID = t.TRA_ID,
                                 Comment = t.Comment,
                                 FAI_ID = t.FAI_ID ?? "0000000",
                                 Location = t.Location ?? "",
                                 t.SerialNumber,
                                 t.IsCancel,
                                 t.IsDownload,
                                 IsPrices = (t.IsPrices ?? false),
                                 t.ERP_StationCharge,

                                 t.ERP_SPA_ID,
                                 t.ERP_PartName,
                                 //อะไหล่ลูกค้า
                                 t.IsCustomer

                                 //20140908
                                 ,
                                 t.SerialNumber_Old

                                 //20141022
                                 //20141127ยกเลิก ,t.EQP_ID


                                 //20161114
                                 ,
                                 t.ReferPage
                                 ,
                                 t.ERP_LoadTime

                             };



                    foreach (var x1 in q1)
                    {

                        DataRow dr = dtPart.NewRow();
                        dr["รหัสจุดเสีย"] = x1.POI_ID;
                        dr["จุดเสีย"] = x1.PoiFailure == "&nbsp;" ? "" : x1.PoiFailure;

                        dr["NO."] = x1.NO;

                        //20140908
                        dr["S/N Old"] = x1.SerialNumber_Old;

                        dr["Part no."] = x1.SPA_ID;
                        dr["Part"] = x1.PartName;
                        dr["Type"] = x1.TRA_ID;

                        dr["จำนวน"] = x1.Quantity;
                        dr["PricesList"] = x1.PricesList.ToString("#,##0.00");
                        dr["ราคา/หน่วย"] = x1.Prices.ToString("#,##0.00");
                        dr["รวม"] = x1.SumPrices.ToString("#,##0.00");
                        dr["ภาษี"] = x1.SumVAT.ToString("#,##0.00");

                        dr["รวมสุทธิ"] = x1.SumTotalPrices.ToString("#,##0.00");

                        dr["VAT"] = x1.VAT.ToString("#,##0.00");
                        dr["FixAssetNo"] = x1.ControlFixAssetNo;
                        dr["Discount"] = x1.Discount.ToString("#,##0.00");

                        dr["สถานะ"] = x1.StatusSpare == true ? "เปลี่ยน" : "ค้าง";

                        dr["Comment"] = x1.Comment == "&nbsp;" ? "" : x1.Comment;
                        dr["FAI_ID"] = x1.FAI_ID;
                        dr["Location"] = x1.Location;
                        dr["S/N"] = x1.SerialNumber ?? "";
                        dr["IsCancel"] = x1.IsCancel ?? "0"[0];
                        dr["IsDownload"] = x1.IsDownload ?? "Y"[0];

                        dr["isPrices"] = (x1.IsPrices == false ? "0" : "1");
                        dr["ออกบิลในนาม"] = (x1.ERP_StationCharge == null ? "สถานี" : (x1.ERP_StationCharge == '0' ? "สถานี" : "บริษัทน้ำมัน"));


                        dr["ERP_SPA_ID"] = x1.ERP_SPA_ID;
                        dr["ERP_PartName"] = x1.ERP_PartName;
                        //อะไหล่ลูกค้า
                        dr["อะไหล่ลูกค้า"] = x1.IsCustomer == '9' ? "Y" : "N";

                        //20141022
                        //20141127ยกเลิก dr["ตำแหน่งที่เปลี่ยน"] = x1.EQP_ID;

                        //20161114
                        dr["Page/Item"] = x1.ReferPage;

                        dr["ERP_LoadTime"] = x1.ERP_LoadTime;


                        logCancelSPA_ID.Add(x1.SPA_ID);

                        dtPart.Rows.Add(dr);



                    }

                    #endregion
                }
                else//ถ้าไม่ใช่แก้ไข ดึงเฉพาะอะไหล่ที่ค้างมาแสดง
                {

                    #region job_add

                    var q1 = from t in db.JOB_Detail_Spares
                             from p in db.PointFailures
                             from sp in db.SpareParts
                             where t.JOB_ID.Trim() == jobid.Trim() &&
                                t.POI_ID.Trim() == p.POI_ID.Trim() &&
                                t.SPA_ID == sp.SPA_ID &&
                                t.StatusSpare == false //ดึงเฉพาะอะไหล่ที่ค้างมาแสดง
                             select new
                             {
                                 //Poi_NO = t.Poi_NO,
                                 POI_ID = p.POI_ID,
                                 PoiFailure = p.PointFailure1,
                                 t.NO,
                                 t.SPA_ID,
                                 PartName = sp.SparePart1,

                                 VAT = t.VAT ?? 0,
                                 t.ControlFixAssetNo,
                                 PricesList = t.PricesList ?? 0,
                                 Prices = t.Prices ?? 0,
                                 Discount = t.Discount ?? 0,
                                 SumPrices = t.SumPrices ?? 0,
                                 SumTotalPrices = t.SumTotalPrices ?? 0,
                                 SumVAT = t.SumVAT ?? 0,
                                 Quantity = t.Quantity ?? 0,
                                 StatusSpare = t.StatusSpare,
                                 TRA_ID = t.TRA_ID,
                                 Comment = t.Comment,
                                 FAI_ID = t.FAI_ID ?? "0000000",
                                 Location = t.Location ?? "",
                                 t.SerialNumber,
                                 IsPrices = (t.IsPrices ?? false),
                                 t.ERP_StationCharge,

                                 t.ERP_SPA_ID,
                                 t.ERP_PartName,
                                 t.IsCustomer

                                 //20140908
                                 ,
                                 t.SerialNumber_Old

                                 //20141022
                                 //20141127ยกเลิก ,t.EQP_ID


                                 //20161114
                                 ,
                                 t.ReferPage
                             };


                    foreach (var x1 in q1)
                    {
                        DataRow dr = dtPart.NewRow();
                        dr["รหัสจุดเสีย"] = x1.POI_ID;
                        dr["จุดเสีย"] = x1.PoiFailure == "&nbsp;" ? "" : x1.PoiFailure;

                        dr["NO."] = x1.NO;

                        //20140908
                        dr["S/N Old"] = x1.SerialNumber_Old;

                        dr["Part no."] = x1.SPA_ID;
                        dr["Part"] = x1.PartName;
                        dr["Type"] = x1.TRA_ID;

                        dr["จำนวน"] = x1.Quantity;

                        dr["PricesList"] = x1.PricesList.ToString("#,##0.00");

                        dr["ราคา/หน่วย"] = x1.Prices.ToString("#,##0.00");
                        dr["รวม"] = x1.SumPrices.ToString("#,##0.00");
                        dr["ภาษี"] = x1.SumVAT.ToString("#,##0.00");

                        dr["รวมสุทธิ"] = x1.SumTotalPrices.ToString("#,##0.00");

                        dr["VAT"] = x1.VAT.ToString("#,##0.00");
                        dr["FixAssetNo"] = x1.ControlFixAssetNo;
                        dr["Discount"] = x1.Discount.ToString("#,##0.00");

                        dr["สถานะ"] = x1.StatusSpare == true ? "เปลี่ยน" : "ค้าง";

                        dr["Comment"] = x1.Comment == "&nbsp;" ? "" : x1.Comment;
                        dr["FAI_ID"] = x1.FAI_ID;
                        dr["Location"] = x1.Location;
                        dr["S/N"] = x1.SerialNumber ?? "";
                        dr["IsCancel"] = "0";
                        dr["IsDownload"] = "Y";

                        dr["isPrices"] = (x1.IsPrices == false ? "0" : "1");

                        dr["ออกบิลในนาม"] = (x1.ERP_StationCharge == null ? "สถานี" : (x1.ERP_StationCharge == '0' ? "สถานี" : "บริษัทน้ำมัน"));

                        dr["ERP_SPA_ID"] = x1.ERP_SPA_ID;
                        dr["ERP_PartName"] = x1.ERP_PartName;
                        //อะไหล่ลูกค้า
                        dr["อะไหล่ลูกค้า"] = x1.IsCustomer == '9' ? "Y" : "N";

                        //20141022
                        //20141127ยกเลิก dr["ตำแหน่งที่เปลี่ยน"] = x1.EQP_ID;

                        //20161114
                        dr["Page/Item"] = x1.ReferPage;


                        logCancelSPA_ID.Add(x1.SPA_ID);

                        dtPart.Rows.Add(dr);

                    }

                    #endregion
                }

                PartBindind();


                #region JOB_Failure_Details

                var q = from t in db.JOB_Failure_Details
                        where t.JOB_ID.Trim() == jobid.Trim()
                        select t;

                //var dd = db.PointFailures .Where(t=>t.POI_ID == "dddd").FirstOrDefault

                if (q.Count() > 0)
                {
                    foreach (var x in q)
                    {
                        var xPointFailure = db.PointFailures.Where(t => t.POI_ID == x.POI_ID).FirstOrDefault();

                        DataRow dr = dtFailure.NewRow();
                        dr["NO."] = x.NO;
                        dr["รหัสจุดเสีย"] = x.POI_ID;
                        // dr["จุดเสีย"] = x.PointFailure.PointFailure1 == "&nbsp;" ? "" : x.PointFailure.PointFailure1;
                        dr["จุดเสีย"] = (xPointFailure == null ? "" : xPointFailure.PointFailure1);
                        dr["อาการเสีย"] = x.Failure_Detail == "&nbsp;" ? "" : x.Failure_Detail;
                        dr["S/N(Old)"] = x.SerialNumber == "&nbsp;" ? "" : x.SerialNumber;
                        dr["FixAssetNo"] = x.SerialNumber == "&nbsp;" ? "" : x.FixAssetNo;

                        dr["S/N(New)"] = x.SerialNumber_New == "&nbsp;" ? "" : x.SerialNumber_New;
                        dr["FixAssetNo_New"] = x.SerialNumber_New == "&nbsp;" ? "" : x.FixAssetNo_New;

                        dr["FCAT_ID"] = x.FCAT_ID ?? 0;
                        dr["FBA_ID"] = x.FBA_ID ?? 0;
                        dr["FMD_ID"] = x.FMD_ID ?? 0;

                        dr["Location"] = x.LOC_ID == null ? "0" : Convert.ToString(x.LOC_ID);//20141215 x.Location == "&nbsp;" ? "" : x.Location;
                        dr["Model"] = x.Model == "&nbsp;" ? "" : x.Model;
                        dr["StartLiter"] = x.StartLiter ?? 0;
                        dr["EndLiter"] = x.EndLiter ?? 0;
                        dr["LiterTest"] = x.LiterTest ?? 0;
                        dr["FAI_ID"] = x.FAI_ID ?? "0000000";

                        dr["IsCancel"] = x.IsCancel ?? "0"[0];
                        //20200527 dr["IsDownload"] = x.IsDownload ?? "Y"[0];
                        dr["IsDownload"] = "Y"; //20200527

                        //20141022
                        dr["PRT_ID"] = x.PRT_ID;
                        //20141127ยกเลิก dr["EQP_ID"] = x.EQP_ID;
                        dr["ATF_ID"] = x.ATF_ID;
                        dr["FAS_ID"] = x.FAS_ID;
                        dr["SolvingByWI"] = x.SolvingByWI;

                        dr["FCAT_CH"] = x.FCAT_CH ?? 0;//20180806

                        dtFailure.Rows.Add(dr);

                    }
                    FailureBinding();
                }
                else
                {
                    if (dtPart.Rows.Count > 0)
                    {

                        DataRow dr = dtFailure.NewRow();
                        dr["NO."] = "1";
                        dr["รหัสจุดเสีย"] = "000";
                        dr["จุดเสีย"] = "ไม่ระบุ";
                        dr["อาการเสีย"] = "ไม่ระบุ";
                        dr["S/N(Old)"] = "None";
                        dr["S/N(New)"] = "None";

                        dr["FCAT_ID"] = 0;
                        dr["FBA_ID"] = 0;
                        dr["FMD_ID"] = 0;

                        dr["Location"] = "None";
                        dr["Model"] = "None";
                        dr["StartLiter"] = 0;
                        dr["EndLiter"] = 0;
                        dr["LiterTest"] = 0;
                        dr["FAI_ID"] = "0000000";

                        dr["IsCancel"] = "0"[0];
                        dr["IsDownload"] = "N"[0];

                        //20141022
                        dr["PRT_ID"] = 0;
                        //20141127ยกเลิก dr["EQP_ID"] = 0;
                        dr["ATF_ID"] = null;
                        dr["FAS_ID"] = 0;
                        dr["SolvingByWI"] = null;

                        dr["FCAT_CH"] = 0;//20180806

                        dtFailure.Rows.Add(dr);

                        FailureBinding();
                    }
                    //20150831
                    FailureBinding();
                }



                #endregion


                #region Problem_Detail, Resolve_Detail

                //if ((from t in db.JOB_Details where t.JOB_ID == jobid orderby t.RecDate descending select t).Count() > 0)
                //{
                //    var q2 = (from t in db.JOB_Details where t.JOB_ID == jobid orderby t.RecDate descending select t).FirstOrDefault();
                //    //20150126 txtProblem_Detail.Text = q2.Problem_Detail ?? "";
                //   txtResolve_Detail.Text = q2.Resole_Detail ?? "";
                //}

                if (editMode == "job_edit")
                {
                    _vpEnddate = Convert.ToDateTime(_pEnddate);
                    var q2 = (from t in db.JOB_Details
                              where t.JOB_ID == jobid &&
                                  //t.EndDate == _vpEnddate
                                  t.EndDate.Value.Date == _vpEnddate.Date
                                  && t.EndDate.Value.Hour == _vpEnddate.Hour
                                  && t.EndDate.Value.Minute == _vpEnddate.Minute
                                  && t.EndDate.Value.Second == _vpEnddate.Second
                                  && t.StatusDetail == true //20201207
                              select t).FirstOrDefault();
                    txtResolve_Detail.Text = q2.Resole_Detail ?? "";
                }
                else
                {
                    if ((from t in db.JOB_Details where t.JOB_ID == jobid orderby t.RecDate descending select t).Count() > 0)
                    {
                        var q2 = (from t in db.JOB_Details where t.JOB_ID == jobid orderby t.RecDate descending select t).FirstOrDefault();
                        //20150126 txtProblem_Detail.Text = q2.Problem_Detail ?? "";
                        txtResolve_Detail.Text = q2.Resole_Detail ?? "";
                    }
                }

                #endregion


                #region cobProblemType
                //cobProblemType

                if (job.PMT_ID == null)
                {
                    if (lbl_JobType.Text.Trim() == "ตู้จ่าย(D)")
                        cobProblemType.SelectedValue = "AO";//"BW";
                    else
                        cobProblemType.SelectedIndex = -1;
                }
                else
                    cobProblemType.SelectedValue = job.PMT_ID;

                cobProblemType.Enabled = true;

                #endregion


                #region ความพึงพอใจ:


                //ความพึงพอใจ:
                if ((from t in db.JOB_ContentMents where t.JOB_ID == jobid select t).Count() > 0)
                {
                    var q2 = (from t in db.JOB_ContentMents where t.JOB_ID == jobid select t).FirstOrDefault();

                    switch (q2.JobContentMent.Trim())
                    {
                        case "A": radioButton_A.Checked = true; break;
                        case "B": radioButton_B.Checked = true; break;
                        case "C": radioButton_C.Checked = true; break;
                        case "D": radioButton_D.Checked = true; break;

                    }
                }

                #endregion



                if (_TYP_ID1 == "SS" && txtJobSO.Text != string.Empty)
                {
                    closeProgress();
                    MessageBox.Show("งานติดตั้ง (S5)..." + Environment.NewLine +
                    "บริษัท จ่ายค่าคอมมิชั่น" + Environment.NewLine +
                    "ขอบคุณเจ้าหน้าที่ โฟลว์โก้ ที่ช่วยกันดูแล ลูกค้าเป็นอย่างดี", "งานติดตั้ง (S5)",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
            }
        } //end setJob_Detail_hist



        private decimal getPricesList_AutoLoadPart(SMSManageDataContext dc, string partNo)
        {

            _PricesList = 0;
            _Prices = 0;
            _Discount = 0;
            _SumPrices = 0;
            _SumTotalPrices = 0;
            _SumVAT = 0;
            _Quantity = 1;


            //Check ข้อมูลในตาราง SpareParts_IsTravel
            var iTv = (from t in dc.SpareParts_IsTravels
                       where t.GOP_ID == tmpGOPId &&
                       t.SPA_ID == partNo
                       select t).FirstOrDefault();

            if (iTv != null)//Part Travel
            {
                #region Check price Conf_Conditions
                decimal acDistance = 0;
                decimal dFrom = 0;
                decimal perKM = 0;


                var conP = dc.Conf_Conditions
                    .Where(t => t.SPA_ID == partNo &&
                    t.GOP_ID == tmpGOPId &&
                   //t.STA_ID == "" &&
                   t.TYP_ID == tmpJobType &&
                   t.TYP_ID1 == tmpTYP_ID1 &&
                   //20190702
                   t.ContractNo == _contractNo &&
                   t.IsAutoPart == true
                    //==
                   ).FirstOrDefault();
                if (conP != null && conP.PriceList > 0)
                {
                    perKM = conP.PriceList;
                }
                else
                {

                    #region Check Price list
                    //===20160929====เอาออก ไม่ใช้ pricelist
                    //Check Price list
                    //var pp = (from t in dc.SparePart_Prices_Lists
                    //          from t1 in dc.SpareParts_IsPrices
                    //          where ((t.SPA_ID == t1.SPA_ID) &&
                    //          t.SPA_ID == partNo && t1.GOP_ID == tmpGOPId)
                    //          select t).FirstOrDefault();

                    //if (pp != null)
                    //    perKM = pp.Prices;
                    //else
                    //{

                    var pp1 = dc.SpareParts.Where(t => t.SPA_ID == partNo)
                        .FirstOrDefault();
                    if (pp1 != null)
                        perKM = pp1.PricePerUnit.Value;
                    //} //===20160929====เอาออก ไม่ใช้ pricelist



                    #endregion
                }

                _PricesList = perKM;
                _Prices = perKM;
                _Discount = 0;

                #endregion



                var st = dc.Station_Distances
                  .Where(t => t.STA_ID == tmpSTAID && t.TYP_ID == _job.TYP_ID)
                  .FirstOrDefault();

                if (st != null)
                {
                    acDistance = st.ActualDistance;
                    dFrom = st.DistanceFromCenter ?? 0;

                    _SumPrices = ((acDistance - dFrom) * perKM);
                    double _xvat = Convert.ToDouble(_vat) > 0 ? Convert.ToDouble(_SumPrices) * (Convert.ToDouble(_vat) / 100.00) : 0;
                    _SumVAT = Convert.ToDecimal(_xvat);

                    _SumTotalPrices = ((acDistance - dFrom) * perKM) + _SumVAT;
                    _Quantity = (acDistance - dFrom);

                }
                else
                    _Quantity = 0;

            }
            else//ไม่ใช่ Part Travel
            {
                #region Part ไม่อ้างอิงโปรเจ็กต์

                var quantity = 1;
                double _xvat = 0;
                // --where contract เพิ่ม
                var conP = dc.Conf_Conditions
                      .Where(t => t.SPA_ID == partNo &&
                      t.GOP_ID == tmpGOPId &&
                     t.TYP_ID == tmpJobType &&
                     t.TYP_ID1 == tmpTYP_ID1 &&
                     //20190702
                     t.ContractNo == _contractNo &&
                     t.IsAutoPart == true
                     //==
                     ).FirstOrDefault();
                if (conP != null && conP.PriceList > 0)
                {
                    #region Check on Conf_Conditions

                    _PricesList = conP.PriceList;
                    _Prices = conP.PriceList * quantity;



                    _SumPrices = _Prices;
                    _xvat = Convert.ToDouble(_vat) > 0 ? Convert.ToDouble(_SumPrices) * (Convert.ToDouble(_vat) / 100.00) : 0;
                    _SumVAT = Convert.ToDecimal(_xvat);


                    _SumTotalPrices = _Prices + _SumVAT;

                    _Quantity = quantity;




                    #endregion
                }
                else
                {

                    #region Check Price list

                    var q = (from t in dc.SpareParts where t.SPA_ID == partNo select t).FirstOrDefault();

                    //var qIsPrice = (from t in dc.SpareParts_IsPrices where t.SPA_ID == partNo && t.GOP_ID == tmpGOPId select t).FirstOrDefault();
                    var stg = (from t in dc.Station_Groups where t.GOP_ID == tmpGOPId select t).FirstOrDefault();
                    var stGroupName = stg == null ? string.Empty : stg.GroupStation;

                    DateTime openjob = Convert.ToDateTime(Convert.ToDateTime(txtOpenJOB.Text).ToShortDateString());

                    ////var pri = (from pl in dc.SparePart_Prices_Lists
                    ////           from isp in dc.SpareParts_IsPrices
                    ////           from pq in dc.SparePart_Quarters
                    ////           from tg in dc.Station_Groups
                    ////           where (pl.PRI_ID == pq.PRI_ID) &&
                    ////                (pl.SPA_ID == isp.SPA_ID && tg.GOP_ID == isp.GOP_ID) &&
                    ////                (pq.GOP_ID == tg.GOP_ID) &&
                    ////                (pl.SPA_ID == partNo) &&
                    ////                 tg.GOP_ID == tmpGOPId &&
                    ////                 pq.StartDate <= openjob && pq.EndDate >= openjob
                    ////           select new
                    ////           {
                    ////               pl.SPA_ID,
                    ////               pq.localDiscount,
                    ////               pq.globalDiscount,
                    ////               pq.decRoundUp,//ปัดเศษ
                    ////               pl.Prices,
                    ////               pq.Quarter

                    ////           }).FirstOrDefault();


                    ////var pri = null;

                    ////if (pri != null)
                    ////{
                    ////    var _discount = 0.0;
                    ////    //var quantity = 1;
                    ////    var roundup = Convert.ToInt32(pri.decRoundUp ?? 0);//ปัดเศษ
                    ////    tmpRounup = roundup;

                    ////    //txtPriceList.Text = pri.Prices.ToString("#,##0.00");
                    ////    _PricesList = pri.Prices;
                    ////    var _prices = Convert.ToDouble(pri.Prices);//Price list

                    ////    if (qIsPrice != null)
                    ////    {
                    ////        if (qIsPrice.IsGlobalDiscount.Value != '2')
                    ////        {
                    ////            if (qIsPrice.IsGlobalDiscount.Value == '1')
                    ////                _discount = Convert.ToDouble(pri.globalDiscount);//ใช้ส่วนลดต่างประเทศ
                    ////            else
                    ////                _discount = Convert.ToDouble(pri.localDiscount);//ใช้ส่วนลดในประเทศ

                    ////            //txtDiscQuarter.Text = _discount.ToString(); //ส่วนลด
                    ////            _Discount = decimal.Parse(_discount.ToString());
                    ////            if (_discount <= 0)
                    ////                _Prices = Math.Round(pri.Prices * quantity, tmpRounup);
                    ////            else
                    ////                _Prices = decimal.Parse(Math.Round((_prices - ((_discount / 100) * _prices)) * quantity, tmpRounup).ToString());


                    ////        }
                    ////        else
                    ////        {
                    ////            //txtDiscQuarter.Text = "0";
                    ////            if (tmpRounup > 0)
                    ////                _Prices = decimal.Parse(Math.Round(Convert.ToDouble(_prices) * quantity, tmpRounup).ToString());
                    ////            else
                    ////                _Prices = decimal.Parse((Convert.ToDouble(_prices) * quantity).ToString());

                    ////        }// if (q.IsDiscount == true)

                    ////        if (qIsPrice.IsPrices != null)
                    ////        {
                    ////            chIsPrices.Checked = qIsPrice.IsPrices.Value;
                    ////            //chIsPrices.Enabled = false;
                    ////        }
                    ////        else
                    ////        {
                    ////            chIsPrices.Checked = false;
                    ////            //chIsPrices.Enabled = false;
                    ////        }

                    ////        _SumPrices = _Prices;
                    ////        _SumTotalPrices = _Prices;
                    ////        _Quantity = quantity;
                    ////    }
                    ////    else
                    ////    {

                    ////        _prices = Convert.ToDouble(q.PricePerUnit.Value == 0 ? 0 : q.PricePerUnit.Value);
                    ////        //txtDiscQuarter.Text = "0";

                    ////        _SumPrices = _Prices;
                    ////        _SumTotalPrices = _Prices;
                    ////        _Quantity = quantity;


                    ////    }
                    ////}
                    ////else
                    ////{
                    ////    //txtPriceList.Text = "0";
                    ////    // txtDiscQuarter.Text = "0";

                    ////    var _prices = Convert.ToDouble(q.PricePerUnit.Value == 0 ? 0 : q.PricePerUnit.Value);

                    ////    _PricesList = 0;
                    ////    _Prices = 0;
                    ////    _Discount = 0;
                    ////    _SumPrices = 0;
                    ////    _SumTotalPrices = 0;
                    ////    _SumVAT = 0;
                    ////    _Quantity = 1;
                    ////}


                    var _prices = Convert.ToDouble(q.PricePerUnit.Value == 0 ? 0 : q.PricePerUnit.Value);

                    _PricesList = 0;
                    _Prices = 0;
                    _Discount = 0;
                    _SumPrices = 0;
                    _SumTotalPrices = 0;
                    _SumVAT = 0;
                    _Quantity = 1;
                    #endregion

                }


                #endregion
            }

            return _PricesList;
        }

        //string strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
        private void SparepartIsPriceChange(string partNo)
        {
            //=====20181122============
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext(strConn))
            {
                var q = dc.SpareParts
                        .Where(t => t.SPA_ID == partNo
                            && t.IsPriceChange == true
                            )
                        .FirstOrDefault();

                if (q != null)
                {

                    txtPriceList.ReadOnly = false;
                    txtDiscQuarter.ReadOnly = false;
                    txtVAT.ReadOnly = false;
                    txtPriceUnit.ReadOnly = false;

                }

            }
            //===========================
        }

        private void lockTranType()
        {
            var smg = new string[] { "09", "12", "13" }.Contains(UserInfo.UserLevID);
            var backup = GetConnection.Connection.Contains("ServicesMSDB_");

            //if (!smg && !backup)
            if (!smg)
            {

                if (isLockTranType)//true คือ lock
                {
                    cob_TrnType.Enabled = false; //20180810 เปลี่ยนกลับไป lock ตาม config Conf_JOB_IsLockTranTypes cob_TrnType.Enabled = true;//เอากลับคืน cob_TrnType.Enabled = false;//20171016 cob_TrnType.Enabled = true;//20160829 false;
                    txtPriceList.ReadOnly = true; //แก้ไม่ได้
                    txtDiscQuarter.ReadOnly = true;
                    txtVAT.ReadOnly = true;

                    txtPriceUnit.ReadOnly = true; //20181122 เขียนกันเพิ่มเพราะเงื่อนไขเปลี่ยนราคาค่าเดินทางได้
                }
                else //false คือ ไม่ lock
                {
                    cob_TrnType.Enabled = true;//เอากลับคืนcob_TrnType.Enabled = false;////20171016เปลี่ยนเป็น lock ทั้งหมด cob_TrnType.Enabled = true;

                    txtPriceList.ReadOnly = false;
                    txtDiscQuarter.ReadOnly = false;
                    txtVAT.ReadOnly = false;

                    txtPriceUnit.ReadOnly = false; //20181122 เขียนกันเพิ่มเพราะเงื่อนไขเปลี่ยนราคาค่าเดินทางได้
                }
            }
        }

        //private void toolStripLabel_cobby_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    Cursor.Current = Cursors.AppStarting;
        //    if (toolStripLabel_cobby.Text == "ทั้งหมด")
        //    {
        //        toolStripText_txtFind.Enabled = false;
        //        toolStripText_txtFind.Text = "";
        //        toolStripButton_FindStation_Click(null, null);
        //    }
        //    else
        //        toolStripText_txtFind.Enabled = true;

        //    Cursor.Current = Cursors.Default;
        //}




        #endregion

        private void toolStripText_txtFind_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {


                toolStripButton_FindStation_Click(null, null);
            }
        }

        private void toolStripComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {


            if (toolStripComboBox1.Text == "บันทึก ออก/ปิดงาน")
                editMode = "job_add";
            else
                editMode = "job_edit";


            butttonEnable(true, true, true, false, true);

            //20150629
            //chkEnableSVReport(txtJobID.Text.Trim());


        }



        #region เลือกจุดเสีย:


        private bool CheckFailureData()
        {

            for (int i = 0; i <= dtFailure.Rows.Count - 1; i++)
            {
                if (comboBox_PoitFail.SelectedValue.ToString() == dtFailure.Rows[i]["รหัสจุดเสีย"].ToString().Trim() &&
                    txtFailure.Text == dtFailure.Rows[i]["อาการเสีย"].ToString().Trim() &&
                    txtSNOld.Text == dtFailure.Rows[i]["S/N(Old)"].ToString().Trim())
                    return true;
            }

            return false;
        }
        private void ClearTextFail()
        {
            comboBox_PoitFail.SelectedIndex = -1;


            txtStartLite.Text = "0";
            txtEndtLite.Text = "0";
            txtLiterTest.Text = "0";


        }

        // string strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
        private void btSelectPointFail_Click(object sender, EventArgs e)
        {

            try
            {
                //==20200512 เพิ่มแจ้งเตือนให้ระบุจุดเสีย
                if (comboBox_PoitFail.SelectedValue == null)
                {
                    closeProgress();
                    MessageBox.Show(" กรุณาเลือก จุดเสีย... ", "ผลการตรวจสอบ");
                    comboBox_PoitFail.Focus();
                    return;
                }
                if (cbxLocation.SelectedValue == null)
                {
                    MessageBox.Show(" กรุณาเลือก Location ของ Fixed Asset ", "ผลการตรวจสอบ");
                    cbxLocation.Focus();
                    return;
                }
                //============================

                //==20210128
                var chkKeyDownSNOld = txtSNOld.Text.Trim();
                var chkKeyDownSNNew = txtSNNew.Text.Trim();
                if (chkKeyDownSNOld == "")
                {
                    closeProgress();
                    MessageBox.Show("กรุณาระบุข้อมูล S/N Old...[ห้ามใส่ None]", "ผลการตรวจสอบ");
                    txtSNOld.Focus();
                    txtSNOld.SelectAll();
                    return;
                }

                if (chkKeyDownSNNew == "")
                {
                    closeProgress();
                    MessageBox.Show("กรุณาระบุข้อมูล S/N New...[ห้ามใส่ None]", "ผลการตรวจสอบ");
                    txtSNNew.Focus();
                    txtSNNew.SelectAll();
                    return;
                }


                if (txtSNOld.Text == string.Empty || txtSNOld.Text == "None" || txtSNOld.Text == "NONE" || txtSNOld.Text == "none")
                {
                    closeProgress();
                    MessageBox.Show("กรุณาระบุข้อมูล S/N Old...[ห้ามใส่ None]", "ผลการตรวจสอบ");
                    txtSNOld.Focus();
                    txtSNOld.SelectAll();
                    return;
                }

                if (txtSNNew.Text == string.Empty || txtSNNew.Text == "None" || txtSNOld.Text == "NONE" || txtSNOld.Text == "none")
                {
                    closeProgress();
                    MessageBox.Show("กรุณาระบุข้อมูล S/N New...[ห้ามใส่ None]", "ผลการตรวจสอบ");
                    txtSNNew.Focus();
                    txtSNNew.SelectAll();
                    return;
                }
                //==================


                //20210128 if ((txtSNOld.Text != "None" && txtSNOld.Text != "ไม่ระบุ...") || (txtSNNew.Text != "None" && txtSNNew.Text != "ไม่ระบุ..."))
                if ((txtSNOld.Text != "ไม่ระบุ...") || (txtSNNew.Text != "ไม่ระบุ..."))
                {
                    ////--=====20180504 ต้องเลือกจากระบบเท่านั้น  ===========================
                    //SNNew
                    using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn))
                    {
                        var qF = (from t in dc.FFixedAsset_SerialNumber_Pools
                                  from c in dc.FFixedAsset_Categories
                                  from m in dc.FFixedAsset_Models
                                  from b in dc.FFixedAsset_Brands
                                  where (m.FBA_ID == b.FBA_ID &&
                                          t.FCAT_ID == c.FCAT_ID &&
                                          t.FMD_ID == m.FMD_ID) &&
                                          t.SerialNumber == txtSNNew.Text.ToString() //&&
                                  //t.StatusUse =="11" 
                                  select t).FirstOrDefault();


                        if (qF == null)
                        {

                            //if (txtSNOld.Text != txtSNNew.Text)
                            //{
                            if (vFCAT_ID_NEW == 0)// key new เอง ไม่เลือก
                            {
                                closeProgress();

                                MessageBox.Show(" ไม่พบ SN : " + txtSNNew.Text.ToString() + " นี้ในระบบ " + Environment.NewLine +
                                    "รบกวนแจ้ง admin ส่วนกลางเพื่อบันทึกข้อมูล Fixed Asset เข้าระบบ" + Environment.NewLine +
                                    "หรือบันทึกข้อมูลเองที่ เมนูปรับปรุงข้อมูล >> บันทึกข้อมูล Fixed Asset  ",
                                    "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                txtSNNew.Focus();
                                return;
                            }

                            //}
                        }



                        //SNOld
                        var qF11 = (from t in dc.FFixedAsset_SerialNumber_Pools
                                    from c in dc.FFixedAsset_Categories
                                    from m in dc.FFixedAsset_Models
                                    from b in dc.FFixedAsset_Brands
                                    where (m.FBA_ID == b.FBA_ID &&
                                            t.FCAT_ID == c.FCAT_ID &&
                                            t.FMD_ID == m.FMD_ID) &&
                                            t.SerialNumber == txtSNOld.Text.ToString() &&
                                    t.StatusUse == "11"
                                    select t).FirstOrDefault();


                        if (qF11 == null)
                        {
                            closeProgress();
                            MessageBox.Show(" ไม่พบ SN : " + txtSNOld.Text.ToString() + " นี้ในสถานี" + Environment.NewLine +
                                    "รบกวนแจ้ง admin ส่วนกลางเพื่อบันทึกข้อมูล Fixed Asset เข้าระบบ",
                                    "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);


                            var qFRecheck = (from t in dc.FFixedAsset_SerialNumber_Pools
                                             from c in dc.FFixedAsset_Categories
                                             from m in dc.FFixedAsset_Models
                                             from b in dc.FFixedAsset_Brands
                                             where (m.FBA_ID == b.FBA_ID &&
                                                     t.FCAT_ID == c.FCAT_ID &&
                                                     t.FMD_ID == m.FMD_ID) &&
                                                     t.SerialNumber == txtSNOld.Text.ToString()

                                             select t).FirstOrDefault();

                            if (qFRecheck == null)
                            {
                                closeProgress();

                                MessageBox.Show(" ไม่พบ SN : " + txtSNOld.Text.ToString() + " นี้ในระบบ " + Environment.NewLine +
                                "รบกวนแจ้ง admin ส่วนกลางเพื่อบันทึกข้อมูล Fixed Asset เข้าระบบ",
                                "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                txtSNOld.Focus();
                                return;
                            }

                        }
                    }
                } //if
                //====================================================================================
                //==20180801
                if (cbxChFCAT.SelectedIndex == -1)
                {
                    cbxChFCAT.Focus();
                    MessageBox.Show("กรุณาเลือก ประเภทอุปกรณ์", "ผลการตรวจสอบ");
                    return;
                }
                //==20180801

                //====20180802 ประเภทที่เลือก เทียบ ประเภท SN
                using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn))
                {
                    if (int.Parse(cbxChFCAT.SelectedValue.ToString()) != FCAT_ID)
                    {
                        if (FCAT_ID == 0)//keyมือหรือเลือกไม่ระบุ
                        {
                            var chkForceF = dc.FFixedAsset_Categories
                                                     .Where(t => t.FCAT_ID == int.Parse(cbxChFCAT.SelectedValue.ToString()) && t.IsForce == '1')
                                                     .FirstOrDefault();

                            if (chkForceF != null)//บังคับ
                            {
                                closeProgress();

                                MessageBox.Show(" ประเภทอุปกรณ์นี้ ผู้ใช้ต้องเลือก SN จากระบบเท่านั้น",
                                     "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                txtSNOld.Focus();
                                return;

                            }
                        }
                        else //เลือกจากระบบแต่เลือกประเภทไม่ตรงกัน 
                        {
                            var chkForceF = dc.FFixedAsset_Categories
                                                    .Where(t => t.FCAT_ID == int.Parse(cbxChFCAT.SelectedValue.ToString()) && t.IsForce == '1')
                                                    .FirstOrDefault();

                            if (chkForceF != null)//บังคับ
                            {
                                closeProgress();
                                MessageBox.Show(" ประเภทอุปกรณ์นี้ ผู้ใช้ต้องเลือก SN จากระบบเท่านั้น " + Environment.NewLine +
                                "ประเภทอุปกรณ์ที่เลือก ไม่ตรงกับ ประเภทอุปกรณ์ของ SN", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                txtSNOld.Focus();
                                return;
                            }
                            else
                            {

                                closeProgress();
                                MessageBox.Show(" ประเภทอุปกรณ์ที่เลือก ไม่ตรงกับ ประเภทอุปกรณ์ของ SN ",
                                               "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                txtSNOld.Focus();
                                return;
                            }

                        }
                    }
                }
                //====20180802
                //====================================================================================



                ////--=====20180504 ต้องเลือกจากระบบเท่านั้น===========================

                //---20180426--บังคับเลือกจากระบบ ?
                //if (txtSNOld.Text != txtSNNew.Text)
                //{
                using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn))
                {
                    var qF = (from t in dc.FFixedAsset_SerialNumber_Pools
                              from c in dc.FFixedAsset_Categories
                              from m in dc.FFixedAsset_Models
                              from b in dc.FFixedAsset_Brands
                              where (m.FBA_ID == b.FBA_ID &&
                                      t.FCAT_ID == c.FCAT_ID &&
                                      t.FMD_ID == m.FMD_ID) &&
                                      t.SerialNumber == txtSNOld.Text.ToString() &&
                                      c.IsForce == '1'
                              select t).FirstOrDefault();


                    if (qF != null)
                    {
                        //if (vFCAT_ID_NEW == 0)// key new เอง ไม่เลือก
                        //{
                        //    MessageBox.Show(" ประเภทอุปกรณ์นี้ ผู้ใช้ต้องเลือก SN จากระบบเท่านั้น",
                        //        "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //    txtSNNew.Focus();
                        //    return;
                        //}

                        //====20180507=====
                        //if (txtSNOld.Text != txtSNNew.Text)
                        //{
                        //    if (vFCAT_ID_NEW == 0)// key new เอง ไม่เลือก
                        //    {
                        //        MessageBox.Show(" ประเภทอุปกรณ์นี้ ผู้ใช้ต้องเลือก SN จากระบบเท่านั้น",
                        //            "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //        txtSNNew.Focus();
                        //        return;
                        //    }
                        //================

                        //====20180507=====
                        if (FCAT_ID == 0 && vFCAT_ID_NEW == 0)// key new เอง ไม่เลือก
                        { //จะเท่ากัน หรือไม่เท่ากัน ก็ไม่ให้ผ่าน

                            //if (FCAT_ID != 0)
                            //{
                            // ===20180507 key มือมาเท่ากันเป๊ะ
                            closeProgress();
                            MessageBox.Show(" ประเภทอุปกรณ์นี้ ผู้ใช้ต้องเลือก SN จากระบบเท่านั้น",
                               "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtSNNew.Focus();
                            return;
                            //}
                            //============

                        }
                        else if (FCAT_ID != 0 && vFCAT_ID_NEW == 0)// เลือกจากระบบ 
                        {
                            if (txtSNOld.Text != txtSNNew.Text)
                            {
                                //if (vFCAT_ID_NEW == 0)// key new เอง ไม่เลือก
                                //{
                                closeProgress();
                                MessageBox.Show(" ประเภทอุปกรณ์นี้ ผู้ใช้ต้องเลือก SN จากระบบเท่านั้น",
                                    "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                txtSNNew.Focus();
                                return;
                                //}
                            }
                        }
                        //====20180507=====

                        ////case binding ตู้เดิม
                        //if (FCAT_ID != 0 && vFCAT_ID_NEW == 0)// key new เอง ไม่เลือก
                        //{
                        //    //if (FCAT_ID != 0)
                        //    //{
                        //    // ===20180507 key มือมาเท่ากันเป๊ะ
                        //    MessageBox.Show(" ประเภทอุปกรณ์นี้ ผู้ใช้ต้องเลือก SN จากระบบเท่านั้น",
                        //       "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //    txtSNNew.Focus();
                        //    return;
                        //    //}
                        //    //============

                        //}

                        //if (txtSNOld.Text == txtSNNew.Text)
                        //{
                        //    //if (FCAT_ID != vFCAT_ID_NEW)//ไม่เคยเลือก เพราะ key เองตั้งแต่ต้น 
                        //    //{
                        //    //    if (vFCAT_ID_NEW != 0)
                        //    //    {
                        //    //    MessageBox.Show(" ประเภทอุปกรณ์นี้ ผู้ใช้ต้องเลือก SN จากระบบเท่านั้น",
                        //    //            "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //    //    txtSNNew.Focus();
                        //    //    return;
                        //    //    }
                        //    //}

                        //    //ไม่เคยเลือก เพราะ key เองตั้งแต่ต้น 

                        //    //sn เดียวกันแต่ vnew =x เคยเลือกไว้ก่อนหน้า
                        //    //แล้วลบทิ้ง ไม่ได้เลือกจากระบบ ผู้ key เอง
                        //}


                    }
                }

                //}
                //------------------------------------------------------------------

                //====20191212=======================================
                using (DAL.SMSManage.SMSManageDataContext db = new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn))
                {
                    var qFDup = (from j in db.JOB_Failure_Details
                                 where
                                (j.JOB_ID == txtJobID.Text.Trim()) &&
                                (j.POI_ID == comboBox_PoitFail.SelectedValue.ToString()) &&
                                (j.FAI_ID == lblFailureCode.Text) &&
                                (j.SerialNumber == txtSNOld.Text.Trim()) &&
                                (j.SerialNumber_New == txtSNNew.Text.Trim())
                                 select j).FirstOrDefault();


                    if (qFDup != null)
                    {
                        closeProgress();
                        MessageBox.Show(" มี จุดเสีย และ Serialnumber นี้ในระบบอยู่แล้ว  " + Environment.NewLine +
                                        " ผู้ใช้สามารถ เพิ่มรายการอะไหล่ เข้าไปใน จุดเสีย เดิมได้เลย  " + Environment.NewLine +
                                        " รบกวนตรวจสอบข้อมูลอีกครั้ง",
                                           "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }

                }
                //====20191212=======================================


                if (comboBox_PoitFail.SelectedIndex == -1)
                {
                    comboBox_PoitFail.Focus();
                    MessageBox.Show("กรุณาเลือก จุดเสีย", "ผลการตรวจสอบ");
                    return;
                }

                if (lbl_JobType.Text.Trim() == "ตู้จ่าย(D)")
                {
                    //if (txtModel.Text == string.Empty || txtModel.Text == "None")
                    //{
                    //    MessageBox.Show("กรุณาป้อนข้อมูล Location...[ห้ามใส่ None]", "ผลการตรวจสอบ");
                    //    txtModel.Focus();
                    //    txtModel.SelectAll();
                    //    return;
                    //}


                    //txtSNOld.Text = (txtSNOld.Text == "" ? "None" : txtSNOld.Text);
                    //txtSNNew.Text = (txtSNNew.Text == "" ? "None" : txtSNNew.Text);


                    ////20141224 เอาไว้ป้องการการไม่ระบุ SN
                    //if ((txtSNOld.Text != string.Empty || txtSNOld.Text != "None") && (txtSNNew.Text == string.Empty || txtSNNew.Text == "None" || txtSNNew.Text == "ไม่ระบุ..."))
                    //{
                    //    MessageBox.Show("กรุณาป้อนข้อมูล S/N New ", "ผลการตรวจสอบ");
                    //    txtSNNew.Focus();
                    //    txtSNNew.SelectAll();
                    //    return;
                    //}
                    //if ((txtSNNew.Text != string.Empty || txtSNNew.Text != "None") && (txtSNOld.Text == string.Empty || txtSNOld.Text == "None" || txtSNOld.Text == "ไม่ระบุ..."))
                    //{
                    //    MessageBox.Show("กรุณาป้อนข้อมูล S/N New ", "ผลการตรวจสอบ");
                    //    txtSNOld.Focus();
                    //    txtSNOld.SelectAll();
                    //    return;
                    //}


                    //==20210128 ย้าย ไปใช้รวม
                    //if (txtSNOld.Text == string.Empty || txtSNOld.Text == "None")
                    //{
                    //    closeProgress();
                    //    MessageBox.Show("กรุณาป้อนข้อมูล S/N Old...[ห้ามใส่ None]", "ผลการตรวจสอบ");
                    //    txtSNOld.Focus();
                    //    txtSNOld.SelectAll();
                    //    return;
                    //}

                    //if (txtSNNew.Text == string.Empty || txtSNNew.Text == "None")
                    //{
                    //    closeProgress();
                    //    MessageBox.Show("กรุณาป้อนข้อมูล S/N New...[ห้ามใส่ None]", "ผลการตรวจสอบ");
                    //    txtSNNew.Focus();
                    //    txtSNNew.SelectAll();
                    //    return;
                    //}
                    //========


                    if (txtModel.Text == string.Empty || txtSNNew.Text == "None")
                    {
                        if (txtSNOld.Text != "ไม่ระบุ..." && txtSNNew.Text != "ไม่ระบุ...")//20141216
                        {
                            closeProgress();
                            MessageBox.Show("กรุณาป้อนข้อมูล Model...[ห้ามใส่ None]", "ผลการตรวจสอบ");
                            txtModel.Focus();
                            return;
                        }

                    }

                    //20141225
                    //if (cob_Failure_Action_Solving.SelectedValue.ToString() != "99" )//&& string.IsNullOrEmpty(cob_SolvingByWI.SelectedValue.ToString()))
                    //20150318  if (string.IsNullOrEmpty(cob_SolvingByWI.SelectedValue.ToString()) )
                    if (cob_SolvingByWI.SelectedIndex == -1)
                    {
                        closeProgress();
                        MessageBox.Show("กรุณาระบุ WI", "ผลการตรวจสอบ");
                        cob_SolvingByWI.Focus();
                        return;
                    }


                    ////เลขรวมลิตร:

                    ///*ไม่ลอค
                    //    39	งาน วิเคราะห์ ATG
                    //    BD	งาน โปรเจ็ค (ENS) 
                    // */
                    //if (txtWorkType.Text.Split(':')[0] != "39" && cob_TrnType.SelectedValue.ToString() != "BD")
                    //{
                    //    //20141223
                    //    //ไม่ล็อค งานตู้จ่าย ที่เป็น 015 เครื่องเติมลม,F22 other
                    //    if (comboBox_PoitFail.SelectedValue.ToString() != "015" && comboBox_PoitFail.SelectedValue.ToString() != "F22")
                    //    {

                    //        var ff = new PopStartLiteMessage();
                    //        ff.ShowDialog();

                    //        if (txtStartLite.Text == string.Empty || txtStartLite.Text == "0")
                    //        {

                    //            MessageBox.Show("กรุณาป้อนข้อมูล เลขรวมลิตร [ก่อน]...", "ผลการตรวจสอบ");
                    //            txtStartLite.Focus();
                    //            return;
                    //        }

                    //        if (txtEndtLite.Text == string.Empty || txtEndtLite.Text == "0")
                    //        {

                    //            MessageBox.Show("กรุณาป้อนข้อมูล เลขรวมลิตร [หลัง]...", "ผลการตรวจสอบ");
                    //            txtEndtLite.Focus();
                    //            return;
                    //        }
                    //    }
                    //    else
                    //    {
                    //        txtStartLite.Text = "0";
                    //        txtEndtLite.Text = "0";
                    //    }
                    //}

                    //=======================================================
                    //เลขรวมลิตร:
                    //ล็อค งาน A1-B4

                    if (new string[] { "016", "017", "018", "019", "020", "021", "022", "023" }.Contains(comboBox_PoitFail.SelectedValue.ToString()))
                    {

                        var ff = new PopStartLiteMessage();
                        ff.ShowDialog();

                        if (txtStartLite.Text == string.Empty || txtStartLite.Text == "0")
                        {
                            closeProgress();
                            MessageBox.Show("กรุณาป้อนข้อมูล เลขรวมลิตร [ก่อน]...", "ผลการตรวจสอบ");
                            txtStartLite.Focus();
                            return;
                        }

                        if (txtEndtLite.Text == string.Empty || txtEndtLite.Text == "0")
                        {
                            closeProgress();
                            MessageBox.Show("กรุณาป้อนข้อมูล เลขรวมลิตร [หลัง]...", "ผลการตรวจสอบ");
                            txtEndtLite.Focus();
                            return;
                        }
                    }
                    else
                    {
                        txtStartLite.Text = "0";
                        txtEndtLite.Text = "0";
                    }
                    //}
                    //else
                    //{ 
                    //    txtStartLite.Text = "0";
                    //    txtEndtLite.Text = "0";                    
                    //}

                }// if (lbl_JobType.Text.Trim() == "ตู้จ่าย(D)")
                else
                {

                    if (txtSNOld.Text == string.Empty)
                    {
                        closeProgress();
                        MessageBox.Show("กรุณาป้อนข้อมูล S/N...", "ผลการตรวจสอบ");
                        txtSNOld.Focus();
                        return;
                    }

                    if (txtModel.Text == string.Empty)
                    {
                        closeProgress();
                        MessageBox.Show("กรุณาป้อนข้อมูล txtModel...", "ผลการตรวจสอบ");
                        txtModel.Focus();
                        return;
                    }
                }


                if (lblFailureCode.Text == string.Empty || lblFailureCode.Text == "FailureCode")
                {
                    closeProgress();
                    MessageBox.Show("กรุณาป้อนข้อมูล อาการเสีย..." + Environment.NewLine, "ผลการตรวจสอบ");
                    txtFailure.Focus();
                    txtFailure.SelectAll();
                    return;
                }

                //ห้ามป้อนข้อมูลจุดเสีย+failureCode+Location ซ้ำ 
                if (checkFailureCode(txtModel.Text, lblFailureCode.Text, comboBox_PoitFail.SelectedValue.ToString()))
                {
                    closeProgress();
                    MessageBox.Show("คุณป้อนข้อมูล {Location:,FailureCode และ จุดเสีย:} ซ้ำ กรุณาป้อนใหม่...",
                        "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtModel.Focus();
                    txtModel.SelectAll();
                    return;
                }
                if (Convert.ToDouble(txtStartLite.Text) > Convert.ToDouble(txtEndtLite.Text))
                {
                    closeProgress();
                    txtEndtLite.Focus();
                    MessageBox.Show("เลขรวมลิตร: หลัง ต้องมากกว่า ก่อน", "ผลการตรวจสอบ",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                txtStartLite.Text = txtStartLite.Text == "" ? "0" : txtStartLite.Text;
                txtEndtLite.Text = txtEndtLite.Text == "" ? "0" : txtEndtLite.Text;
                txtEndtLite.Text = txtEndtLite.Text == "" ? "0" : txtEndtLite.Text;

                if (txtFailure.Text == "")
                {
                    closeProgress();
                    txtFailure.Focus();
                    MessageBox.Show("กรุณาป้อนข้อมูล อาการเสีย...", "ผลการตรวจสอบ");
                    return;
                }

                if (lsSNCancel.Count() > 0)
                {
                    var snCal = lsSNCancel
                        .Where(t => t.POI_ID == comboBox_PoitFail.SelectedValue.ToString() &&
                        t.SNNew == txtSNNew.Text.Trim() &&
                        t.SNold == txtSNOld.Text.Trim()).FirstOrDefault();
                    if (snCal != null)
                        lsSNCancel.Remove(snCal);
                }



                int xrow = 0;

                DataRow dr = dtFailure.NewRow();
                dr["NO."] = xrow;/*0*/
                dr["รหัสจุดเสีย"] = comboBox_PoitFail.SelectedValue.ToString();/*0*/
                dr["จุดเสีย"] = comboBox_PoitFail.Text.Trim();/*0*/
                dr["อาการเสีย"] = txtFailure.Text.Trim();/*0*/
                dr["S/N(Old)"] = txtSNOld.Text.Trim();/*0*/
                dr["FixAssetNo"] = txtFixAssetNoOld.Text.Trim();/*0*/

                dr["S/N(New)"] = txtSNNew.Text.Trim();/*0*/
                dr["FixAssetNo_New"] = txtFixAsetNoNew.Text.Trim();/*0*/

                dr["FCAT_ID"] = FCAT_ID;/*0*/
                dr["FBA_ID"] = FBA_ID;/*9*/
                dr["FMD_ID"] = FMD_ID;/*10*/

                dr["Location"] = cbxLocation.SelectedValue;//20141204  txtModel.Text.Trim();/*11*/
                dr["Model"] = txtModel.Text.Trim();/*12*/
                dr["StartLiter"] = txtStartLite.Text.Trim();/*13*/
                dr["EndLiter"] = txtEndtLite.Text.Trim();/*14*/
                dr["LiterTest"] = txtLiterTest.Text.Trim();/*15*/
                dr["FAI_ID"] = lblFailureCode.Text;

                //20141020
                dr["PRT_ID"] = cobOilType.SelectedValue; //"";
                //20141127ยกเลิก dr["EQP_ID"] = cob_Equipment_Position.SelectedValue; //"";
                dr["ATF_ID"] = cob_Failure_Action.SelectedValue;  //"";
                dr["FAS_ID"] = cob_Failure_Action_Solving.SelectedValue; //"";
                dr["SolvingByWI"] = cob_SolvingByWI.SelectedValue;//20141224 txtSolvingWI.Text;//"";


                dr["IsCancel"] = "0";
                dr["IsDownload"] = "N";

                dr["FCAT_CH"] = cbxChFCAT.SelectedValue;//20180803


                dtFailure.Rows.Add(dr);

                lsSNSelect.Add(new SNCancel
                {
                    POI_ID = comboBox_PoitFail.SelectedValue.ToString(),
                    SNold = txtSNOld.Text.Trim(),
                    SNNew = txtSNNew.Text.Trim()
                });

                FailureBinding();


                ClearTextFail();
                dataGridView_FailPoint.Enabled = true;

                countFailureNO();

                comboBox_PoitFail.Focus();

                vFCAT_ID_NEW = 0;//20180427 เคลียร์ค่า
                FCAT_ID = 0;//20180507 เคลียร์ค่า

                //20180822 เพิ่มเคลียร์ค่า SN บังคับให้ผู้ใช้เลือกใหม่เสมอ
                txtSNOld.Text = "";
                txtSNNew.Text = "";
            }

            catch (System.Exception ex)
            {
                MessageBox.Show("ข้อผิดพลาด " + ex.Message, "เกิดข้อผิดพลาด");
            }
        }

        private bool checkFailureCode(string txtLocation, string lblFailureCode, string comboBox_PoitFail)
        {
            for (int i = 0; i <= dataGridView_FailPoint.RowCount - 1; i++)
            {
                string _Location = dataGridView_FailPoint["Location", i].Value.ToString();
                string _FailureCode = dataGridView_FailPoint["FAI_ID", i].Value.ToString();
                string _cobPoitFail = dataGridView_FailPoint["รหัสจุดเสีย", i].Value.ToString();

                if ((txtLocation == _Location) && (lblFailureCode == _FailureCode) && (comboBox_PoitFail == _cobPoitFail))
                    return true;

            }

            return false;
        }


        private void countFailureNO()
        {
            for (int i = 0; i <= dataGridView_FailPoint.RowCount - 1; i++)
            {
                dataGridView_FailPoint["NO.", i].Value = i + 1;
            }
        }


        #endregion


        #region เลือกอะไหล่

        private bool CheckPartData()
        {
            string radTranType = "";
            if (radTranType0.Checked)
                radTranType = "ค้าง";
            else if (radTranType1.Checked)
                radTranType = "เปลี่ยน";

            for (int i = 0; i <= dtPart.Rows.Count - 1; i++)
            {
                if (cob_PoitFail0.SelectedText.Trim() == dtPart.Rows[i]["รหัสจุดเสีย"].ToString().Trim() &&
                    //txtPartNo.Text == dtPart.Rows[i]["Part no."].ToString() &&
                    tmpSPA_ID == dtPart.Rows[i]["Part no."].ToString() &&
                    radTranType == dtPart.Rows[i]["สถานะ"].ToString().Trim())
                    return true;
            }
            return false;
        }

        private void ClearPartText()
        {
            tmpSPA_ID = string.Empty;
            txtPartNo.Text = "";
            txtPartName.Text = "";
            var v = _vat;//20200128
            //txtVAT.Text = "0.00";  // _vat.ToString("#0.00");

            /*//20200318
            //====20200128
             string  kkk = _vat.ToString().Trim();
              if (kkk.Length > 0)
              {
                  try
                  {
                      txtVAT.Text = "0.00"; 
                  }
                  catch { }
              }
              //=========*/

            //20203018 txtVAT.Text = _vat.ToString("#0.00");

            try
            {
                txtVAT.Text = _vat.ToString("#0.00");
            }
            catch { }

            txtPriceList.Text = "0.00";
            txtDiscQuarter.Text = "0.00";
            txtPriceUnit.Text = "0.00";


            //txtPriceList.Text = Convert.ToDouble("0.00").ToString("#,##0.00");
            //txtDiscQuarter.Text = Convert.ToDouble("0.00").ToString("#,##0.00");
            //txtPriceUnit.Text = Convert.ToDouble("0.00").ToString("#,##0.00");

            txtPartQT.Text = "1";


            chIsPrices.Checked = false;
            chIsPrices.Enabled = true;

            radTranType0.Checked = false;
            radTranType1.Checked = false;

            txtPartComment.Text = "";
            txtFixAssetNo.Text = "";

            //cob_TrnType.SelectedIndex = -1;
            cob_TrnType.SelectedValue = "IN";

            con_ERP_StationCharge.SelectedIndex = -1;
            con_ERP_StationCharge.Enabled = true;

            //อะไหล่ลูกค้า
            cbIsCustomer.Checked = false;
            cbIsCustomer.Enabled = true;

            //20140908
            txtPartSerialNumberOld.Text = "";

            //2016115
            txtReferPage.Text = "";
        }



        private bool CheckProjectCountPart(string tmpJOBID, string tmpProjectNo, string tmpPartNo, ref bool notConnec)
        {
            int cCout = 0;
            int xQuantity = 0;
            try
            {
                //strConn = ConnectionManager.GetConnectionString("1");
                strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;

                using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
                {
                    var q = (from t in dc.v_JOB_Project_Part_Details
                             where t.PROJ_ID == tmpProjectNo && t.SPA_ID == tmpPartNo
                             select t);

                    foreach (var cPart in q)
                        cCout += cPart.sumQuantity.Value;

                    cCout += int.Parse(txtPartQT.Text);


                    var pro = from t in dc.v_JOB_Project_Part_Detail_mas
                              where (t.JOB_ID == tmpJOBID) && (t.PROJ_ID == t.PROJ_ID) && (t.SPA_ID == tmpPartNo)
                              select t;


                    foreach (var x in pro)
                        xQuantity += x.sumQuantity.Value;

                    xQuantity = xQuantity == 0 ? 1 : xQuantity;
                }

                notConnec = false;
                if (cCout > xQuantity)
                    return false;
            }
            catch (System.Exception)
            {
                closeProgress();
                MessageBox.Show("สำหรับ JOB Project การใช้งานหน้าจอ บันทึกข้อมูลออกงาน/ปิดงาน ต้องต่อ Internet" + Environment.NewLine +
                "เพื่อตรวจสอบจำนวนอะไหล่ที่เปลี่ยน", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                notConnec = true;
                return false;
            }

            return true;
        }


        bool checkPartProject(string spaID)
        {
            //20181123using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn))
            {
                if ((from d in dc.JOB_Project_Details
                     from p in dc.JOB_Projects
                     where p.PROJ_ID == d.PROJ_ID &&
                        d.PROJ_ID.Trim() == txtProjectNO.Text.Trim() &&
                        d.SPA_ID == spaID
                     select d).Count() > 0)
                {
                    //return false;
                    return true;
                }
                else
                {
                    //return true;
                    return false;
                }
            }

        }


        private void btBtPart_select_Click(object sender, EventArgs e)
        {


            #region Validate data

            if (cob_PoitFail0.SelectedIndex == -1 || cob_PoitFail0.Items.Count <= 0)
            {
                closeProgress();
                MessageBox.Show("กรุณาเลือกจุดเสีย ", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            //tmpSPA_ID txtPartNo.Text
            if (!checkPartInDB(tmpSPA_ID))
            {
                closeProgress();
                MessageBox.Show("กรุณาระบุอะไหล่ โดยการค้นหาจากฐานข้อมูล ", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (txtPartQT.Text == "" || txtPartQT.Text == "0")
            {
                closeProgress();
                txtPartQT.Focus();
                MessageBox.Show("กรุณาระบุจำนวนที่เปลี่ยน...", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (cob_PoitFail0.SelectedIndex == -1)
            {
                closeProgress();
                MessageBox.Show("กรุณาเลือกจุดเสียก่อนป้อน อะไหล่", "ผลการตรวจสอบ");
                return;
            }

            if (txtPartNo.Text == "" || tmpSPA_ID == string.Empty)
            {
                closeProgress();
                txtPartNo.Focus();
                MessageBox.Show("กรุณาป้อนข้อมูลรหัสอะไหล่...", "ผลการตรวจสอบ");
                return;
            }

            if (!radTranType0.Checked && !radTranType1.Checked)
            {
                closeProgress();
                MessageBox.Show("กรุณาเลือกข้อมูล สถานะ (ค้าง หรือเปลี่ยน)...", "ผลการตรวจสอบ");
                return;
            }

            //con_ERP_StationCharge
            if (con_ERP_StationCharge.SelectedIndex == -1)
            {
                closeProgress();
                MessageBox.Show("กรุณาเลือกข้อมูล ออกบิลในนาม...", "ผลการตรวจสอบ");
                return;
            }

            if (cob_TrnType.SelectedIndex == -1)
            {
                closeProgress();
                MessageBox.Show("กรุณาเลือกข้อมูล TranType...", "ผลการตรวจสอบ");
                return;
            }

            //20201202 ก่อน ปิดงาน 99 แล้วห้ามแก้เป็นค้างอะไหล่
            if (radTranType0.Checked)//ค้างอะไหล่
            {
                using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn))
                {
                    var js = dc.JOBs.Where(t => t.JOBS_ID == "99" && t.JOB_ID == txtJobID.Text.Trim()).FirstOrDefault();
                    if (js != null)
                    {
                        closeProgress();
                        MessageBox.Show("JOB : " + txtJobID.Text.Trim() + " ปิด JOB ไปแล้ว  " + Environment.NewLine + " ไม่สามารถเลือก สถานะ ของรายการอะไหล่เป็น ค้าง ได้", "ผลการตรวจสอบ");
                        return;
                    }
                }
            }

            //====


            //if (lbEQP_ID.Text == "")
            //{
            //    MessageBox.Show("กรุณาเลือกข้อมูล TranType...", "ผลการตรวจสอบ");
            //    return;
            //}

            #endregion


            newProgressbar();
            invoke_Progress("กำลังตรวจสอบข้อมูล...");

            try
            {
                DataRow dr = null;

                string _poid = cob_PoitFail0.SelectedValue == null ? "000" : cob_PoitFail0.SelectedValue.ToString().Trim();
                string _pode = cob_PoitFail0.Text.Trim() == "" ? "Other" : cob_PoitFail0.Text.Trim();

                //20140905
                string _SNOld = txtPartSerialNumberOld.Text.Trim() == "" ? "" : txtPartSerialNumberOld.Text.Trim();

                string radPartStatus = "";
                if (radTranType0.Checked)
                    radPartStatus = "ค้าง";
                else if (radTranType1.Checked)
                    radPartStatus = "เปลี่ยน";


                //อะไหล่ลูกค้า
                string _cbIsCustomer = "";
                if (this.cbIsCustomer.Checked == true)
                {
                    _cbIsCustomer = "Y";
                }
                else
                {
                    _cbIsCustomer = "N";
                }


                if (checkSPA_ID(tmpSPA_ID, radPartStatus, _poid, tmpFAIID, tmpLocation, cob_TrnType.SelectedValue.ToString()))
                {
                    /* //20200924 
                    closeProgress();
                    MessageBox.Show("คุณป้อนข้อมูล อะไหล่ ซ้ำ กรุณาเลือกใหม่", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtPartNo.Focus();
                    txtPartNo.SelectAll();
                    return;*/
                    using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext(strConn))
                    {
                        var j = dc.Conf_Part_Dups.Where(t => t.SPA_ID == tmpSPA_ID).FirstOrDefault();
                        if (j == null)
                        {
                            closeProgress();
                            MessageBox.Show("คุณป้อนข้อมูล อะไหล่ ซ้ำ กรุณาเลือกใหม่", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            txtPartNo.Focus();
                            txtPartNo.SelectAll();
                            return;
                        }
                    }
                }
                decimal number;

                invoke_Progress("กำลังตรวจสอบ Price List...");
                decimal priceList = 0;
                decimal vat = 0;
                decimal dis = 0;
                priceList = Decimal.TryParse(txtPriceList.Text, out number) ? number : 0;
                vat = Decimal.TryParse(txtVAT.Text, out number) ? number : 0;
                dis = Decimal.TryParse(txtDiscQuarter.Text, out number) ? number : 0;

                //จำนวนอะใหล่
                decimal qt = 0;
                qt = Decimal.TryParse(txtPartQT.Text, out number) ? number : 0;

                if (dis > 0)
                    priceList = priceList - (priceList * (dis / 100));

                //ราคา/หน่วย
                decimal pricePerUnit = 0;// Math.Round(priceList, tmpRounup);

                if (tmpRounup > 0)
                    pricePerUnit = Math.Round(priceList, tmpRounup);
                else
                    pricePerUnit = priceList;

                //dtPart
                int xrow = 0;

                if (tmpPA_ID_NO != 0)
                {
                    xrow = tmpPA_ID_NO - 1;
                    tmpPA_ID_NO = 0;
                }
                else
                {
                    for (int r = 0; r <= dtPart.Rows.Count - 1; r++)
                    {
                        if (Convert.ToInt32(dtPart.Rows[r]["NO."].ToString()) > xrow)
                            xrow = Convert.ToInt32(dtPart.Rows[r]["NO."].ToString());
                    }
                }


                //รวมสุทธิ
                decimal _xtotal = pricePerUnit * qt;

                //ภาษี
                decimal _xvat = vat > 0 ? _xtotal * (vat / 100) : 0;


                //20161114
                string _vReferPage = txtReferPage.Text.Trim();

                //20141127ยกเลิก int _EQP_ID = int.Parse(lbEQP_ID.Text.ToString());
                using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext(strConn))
                {
                    //dr = setPartDetail(dc, dr, _poid, _pode, radPartStatus, vat, dis, qt, pricePerUnit, xrow, _xtotal, _xvat, _cbIsCustomer);
                    //20141127ยกเลิก dr = setPartDetail(dc, dr, _poid, _pode, radPartStatus, vat, dis, qt, pricePerUnit, xrow, _xtotal, _xvat, _cbIsCustomer, _SNOld, _EQP_ID);

                    //20161114 dr = setPartDetail(dc, dr, _poid, _pode, radPartStatus, vat, dis, qt, pricePerUnit, xrow, _xtotal, _xvat, _cbIsCustomer, _SNOld);
                    dr = setPartDetail(dc, dr, _poid, _pode, radPartStatus, vat, dis, qt, pricePerUnit, xrow, _xtotal, _xvat, _cbIsCustomer, _SNOld, _vReferPage);

                    //dr = setPartDetail(dc, dr, _poid, _pode, radPartStatus, vat, dis, qt, priceP
                    //Split Part {Load Part to ERP}
                    if (radPartStatus == "เปลี่ยน")
                    {
                        if (!string.IsNullOrEmpty(_contractNo))
                        {
                            var q = (from ct in dc.Conf_Conditions
                                     from sp in dc.SpareParts

                                     where (ct.SPA_ID == sp.SPA_ID) &&
                                         (ct.SPA_ID == tmpSPA_ID && ct.GOP_ID == tmpGOPId && ct.ContractNo == _contractNo && ct.TYP_ID == _job.TYP_ID) &&
                                         (ct.ERP_SPA_ID != null && ct.ERP_TRA_ID != null)
                                     select new SPLIT_Part_ERP
                                     {
                                         ERP_SPA_ID = ct.ERP_SPA_ID,
                                         ERP_PartName = sp.SparePart1,
                                         ERP_TRA_ID = ct.ERP_TRA_ID
                                     }).FirstOrDefault();

                            if (q != null)
                            {
                                //dr = setPartDetil_to_ERP(dc, dr, _poid, _pode, radPartStatus, vat, dis, qt, pricePerUnit, xrow, _xtotal, _xvat, q);
                                //20140908
                                //20141127ยกเลิก dr = setPartDetil_to_ERP(dc, dr, _poid, _pode, radPartStatus, vat, dis, qt, pricePerUnit, xrow, _xtotal, _xvat, q,_SNOld,_EQP_ID);

                                dr = setPartDetil_to_ERP(dc, dr, _poid, _pode, radPartStatus, vat, dis, qt, pricePerUnit, xrow, _xtotal, _xvat, q, _SNOld);


                            }

                        }

                    }

                }

                PartBindind();
                ClearPartText();

                invoke_Progress("กำลังตรวจสอบ Lock TranType...");
                lockTranType();

                invoke_Progress("กำลังตรวจสอบ ค้างอะไหล่...");
                checkPartRemain();

                closeProgress();
            }
            catch (System.Exception ex)
            {
                closeProgress();
                MessageBox.Show("ข้อผิดพลาด " + ex.Message, "เกิดข้อผิดพลาด");
            }

        }

        //20141127ยกเลิก private DataRow setPartDetil_to_ERP(DAL.SMSManage.SMSManageDataContext dc, DataRow dr, string _poid, string _pode, string radPartStatus, decimal vat, decimal dis, decimal qt, decimal pricePerUnit, int xrow, decimal _xtotal, decimal _xvat, SPLIT_Part_ERP q, string _SNOld ,int _EQP_ID)
        private DataRow setPartDetil_to_ERP(DAL.SMSManage.SMSManageDataContext dc, DataRow dr, string _poid, string _pode, string radPartStatus, decimal vat, decimal dis, decimal qt, decimal pricePerUnit, int xrow, decimal _xtotal, decimal _xvat, SPLIT_Part_ERP q, string _SNOld)
        {
            dr = dtPart.NewRow();
            dr["รหัสจุดเสีย"] = _poid;
            dr["จุดเสีย"] = _pode;
            dr["NO."] = xrow + 1;

            //20140908
            dr["S/N Old"] = _SNOld;
            dr["Part no."] = q.ERP_SPA_ID;
            dr["Part"] = q.ERP_PartName;
            dr["จำนวน"] = qt.ToString();

            dr["VAT"] = vat.ToString("#,##0.00");
            dr["FixAssetNo"] = txtFixAssetNo.Text.Trim();

            dr["PricesList"] = decimal.Parse(txtPriceList.Text).ToString("#,##0.00");

            dr["ราคา/หน่วย"] = pricePerUnit.ToString("#,##0.00");
            dr["รวม"] = _xtotal.ToString("#,##0.00");
            dr["ภาษี"] = Math.Round(_xvat, 2).ToString("#,##0.00");

            dr["รวมสุทธิ"] = Convert.ToDecimal(_xtotal + _xvat).ToString("#,##0.00");

            dr["Discount"] = dis.ToString("#,##0.00");

            dr["สถานะ"] = radPartStatus;
            dr["Type"] = q.ERP_TRA_ID;
            dr["Comment"] = "Auto Load to ERP";

            dr["FAI_ID"] = tmpFAIID;
            dr["Location"] = tmpLocation;
            dr["S/N"] = tmpSN;
            dr["IsCancel"] = "0";
            dr["IsDownload"] = "Y";

            dr["isPrices"] = checkConf_JOB_IS_PrintPrice(dc, txtJobID.Text.Trim(), cob_TrnType.SelectedValue.ToString());
            dr["ออกบิลในนาม"] = con_ERP_StationCharge.Text.Trim().ToString();


            dr["ERP_SPA_ID"] = null;
            dr["ERP_PartName"] = null;

            dr["ERP_orderline_id"] = lblERP_orderline_id.Text;

            //20141127ยกเลิก dr["ตำแหน่งที่เปลี่ยน"] = _EQP_ID;

            dtPart.Rows.Add(dr);
            return dr;
        }



        //20141127ยกเลิก private DataRow setPartDetail(DAL.SMSManage.SMSManageDataContext dc, DataRow dr, string _poid, string _pode, string radPartStatus, decimal vat, decimal dis, decimal qt, decimal pricePerUnit, int xrow, decimal _xtotal, decimal _xvat, string _cbIsCustomer,string _SNOld, int _EQP_ID)
        //20161114 private DataRow setPartDetail(DAL.SMSManage.SMSManageDataContext dc, DataRow dr, string _poid, string _pode, string radPartStatus, decimal vat, decimal dis, decimal qt, decimal pricePerUnit, int xrow, decimal _xtotal, decimal _xvat, string _cbIsCustomer, string _SNOld)
        private DataRow setPartDetail(DAL.SMSManage.SMSManageDataContext dc, DataRow dr, string _poid, string _pode, string radPartStatus, decimal vat, decimal dis, decimal qt, decimal pricePerUnit, int xrow, decimal _xtotal, decimal _xvat, string _cbIsCustomer, string _SNOld, string vReferPage)
        {
            dr = dtPart.NewRow();
            dr["รหัสจุดเสีย"] = _poid;
            dr["จุดเสีย"] = _pode;
            dr["NO."] = xrow + 1;
            //20140905
            dr["S/N Old"] = _SNOld;
            dr["Part no."] = tmpSPA_ID;//txtPartNo.Text;
            dr["Part"] = txtPartName.Text.Trim();
            dr["จำนวน"] = qt.ToString();

            dr["VAT"] = vat.ToString("#,##0.00");
            dr["FixAssetNo"] = txtFixAssetNo.Text.Trim();

            dr["PricesList"] = decimal.Parse(txtPriceList.Text).ToString("#,##0.00");

            dr["ราคา/หน่วย"] = pricePerUnit.ToString("#,##0.00");
            dr["รวม"] = _xtotal.ToString("#,##0.00");
            dr["ภาษี"] = Math.Round(_xvat, 2).ToString("#,##0.00");

            dr["รวมสุทธิ"] = Convert.ToDecimal(_xtotal + _xvat).ToString("#,##0.00");

            dr["Discount"] = dis.ToString("#,##0.00");

            dr["สถานะ"] = radPartStatus;
            dr["Type"] = cob_TrnType.SelectedValue.ToString();
            dr["Comment"] = txtPartComment.Text.Trim();

            dr["FAI_ID"] = tmpFAIID;
            dr["Location"] = tmpLocation;
            dr["S/N"] = tmpSN;
            dr["IsCancel"] = "0";
            dr["IsDownload"] = "N";

            dr["isPrices"] = (chIsPrices.Checked == true && cob_TrnType.SelectedValue.ToString() == "IN") ? "1" :
                checkConf_JOB_IS_PrintPrice(dc, txtJobID.Text.Trim(), cob_TrnType.SelectedValue.ToString());

            dr["ออกบิลในนาม"] = con_ERP_StationCharge.Text.Trim().ToString();


            bool ERP_SPA_IDSend = false;
            string ERP_SPA_ID = "";



            #region Part แทน

            if (!string.IsNullOrEmpty(_contractNo))
            {
                var q1 = (from ct in dc.Conf_Conditions
                          from sp in dc.SpareParts

                          where (ct.SPA_ID == sp.SPA_ID) &&
                              (ct.SPA_ID == tmpSPA_ID && ct.GOP_ID == tmpGOPId && ct.ContractNo == _contractNo && ct.TYP_ID == _job.TYP_ID) &&
                              (ct.ERP_SPA_ID != null && ct.ERP_TRA_ID == null)
                          select new SPLIT_Part_ERP
                          {
                              ERP_SPA_ID = ct.ERP_SPA_ID,
                              ERP_PartName = sp.SparePart1,
                              ERP_TRA_ID = ct.ERP_TRA_ID
                          }).FirstOrDefault();

                if (q1 != null)
                {
                    ERP_SPA_IDSend = true;
                    ERP_SPA_ID = q1.ERP_SPA_ID;
                }

                if (ERP_SPA_IDSend)
                {
                    dr["ERP_SPA_ID"] = q1.ERP_SPA_ID;
                    dr["ERP_PartName"] = txtPartName.Text.Trim();
                }
                else
                {
                    dr["ERP_SPA_ID"] = null;
                    dr["ERP_PartName"] = null;
                }
            }

            #endregion

            //อะไหล่ลูกค้า

            dr["อะไหล่ลูกค้า"] = _cbIsCustomer;

            //ERP_orderline_id
            dr["ERP_orderline_id"] = lblERP_orderline_id.Text;

            //20141021
            //20141127ยกเลิก dr["ตำแหน่งที่เปลี่ยน"] = _EQP_ID;

            //20161114
            dr["Page/Item"] = vReferPage;

            dtPart.Rows.Add(dr);
            return dr;
        }

        private void deletePartTRANVELandSERVICE()
        {
            for (int i = 0; i <= dataGridView_Part.RowCount - 1; i++)
            {
                string item = dataGridView_Part["Part no.", i].Value.ToString();

                if (item == "SERVICE3" || item == "TRAVEL")
                {
                    dtPart.Rows[i].Delete();
                    PartBindind();
                    return;

                }
            }
        }

        private void deletePartTRANVELandSERVICE_WA()//20181022
        {
            for (int i = 0; i <= dataGridView_Part.RowCount - 1; i++)
            {
                string item = dataGridView_Part["Part no.", i].Value.ToString();

                if (item == "20SERA01LC" || item == "20SERD02LC")
                {
                    dtPart.Rows[i].Delete();
                    PartBindind();
                    //return;

                }
            }

        }

        private void deletePartTRANVEL(string spa_id)
        {
            for (int i = 0; i <= dataGridView_Part.RowCount - 1; i++)
            {
                string item = dataGridView_Part["Part no.", i].Value.ToString();

                if (item == spa_id)//"TRAVEL")
                {
                    dtPart.Rows[i].Delete();
                    PartBindind();
                    return;

                }
            }
        }



        private string checkConf_JOB_IS_PrintPrice(DAL.SMSManage.SMSManageDataContext dc, string jobID, string tranType)
        {
            //chIsPrices

            if (tranType == "IN")
            {
                var j = dc.JOBs.Where(t => t.JOB_ID == jobID).FirstOrDefault();
                if (j != null)
                {
                    var s = dc.Stations.Where(t => t.STA_ID == j.STA_ID).FirstOrDefault();
                    if (s != null)
                    {
                        var q = dc.Conf_JOB_IS_PrintPrices
                            .Where(t => t.GOP_ID == s.GOP_ID && t.TYP_ID == j.TYP_ID)
                            .FirstOrDefault();
                        if (q != null)
                        {
                            return (q.SVIsPrintPrice == true ? "1" : "0");
                        }
                    }
                }
            }

            return "0";
        }

        private bool checkPartInDB(string p)
        {

            strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext(strConn))
            {
                if ((from t in dc.SpareParts where t.SPA_ID == p select t).Count() > 0)
                    return true;
                else
                    return false;
            }
        }

        private void checkPartRemain()
        {
            for (int i = 0; i <= dataGridView_Part.RowCount - 1; i++)
            {

                if (dataGridView_Part["สถานะ", i].Value.ToString() == "ค้าง")// && dataGridView_Part["IsCancel", i].Value.ToString()=="0")
                {
                    RadioButtonList_JobStatus1.Checked = true;

                    RadioButtonList_JobStatus1.Enabled = false;
                    RadioButtonList_JobStatus2.Enabled = false;

                    tabControl1.Enabled = true;
                    return;
                }
            }

            if (RadioButtonList_JobStatus2.Checked != true)
            {


                //20210714  RadioButtonList_JobStatus1.Enabled = true;
                //20210714  RadioButtonList_JobStatus2.Enabled = true;

                //=====20210714=====
                if (editMode == "job_edit")
                {
                    RadioButtonList_JobStatus1.Enabled = false;
                    RadioButtonList_JobStatus2.Enabled = false;
                }
                else
                {
                    RadioButtonList_JobStatus1.Enabled = true;
                    RadioButtonList_JobStatus2.Enabled = true;
                }
                //===================
            }
            else
            {
                RadioButtonList_JobStatus1.Enabled = false;
                RadioButtonList_JobStatus2.Enabled = false;
            }
        }

        private void countPartNO()
        {
            for (int i = 0; i <= dataGridView_Part.RowCount - 1; i++)
            {
                dataGridView_Part["NO.", i].Value = i + 1;
            }
        }



        private bool checkSPA_ID(string pID, string radPartStatus, string cobPoitFail0, string faiid, string local, string tranType)
        {
            for (int i = 0; i <= dataGridView_Part.Rows.Count - 1; i++)
            {
                if (pID == dataGridView_Part["Part no.", i].Value.ToString() &&
                    radPartStatus == dataGridView_Part["สถานะ", i].Value.ToString() &&
                    cobPoitFail0.Trim() == dataGridView_Part["รหัสจุดเสีย", i].Value.ToString() &&
                    faiid == dataGridView_Part["FAI_ID", i].Value.ToString() &&
                    local == dataGridView_Part["Location", i].Value.ToString() &&
                    (tranType == dataGridView_Part["Type", i].Value.ToString()) &&
                    //Convert.ToInt32(dataGridView_Part["จำนวน", i].Value.ToString() == "0.00" ? "0" : dataGridView_Part["จำนวน", i].Value.ToString()) == Convert.ToInt32(txtPartQT.Text == "0.00" ? "0" : txtPartQT.Text))
                    decimal.Parse(dataGridView_Part["จำนวน", i].Value.ToString()) == decimal.Parse(txtPartQT.Text))
                    return true;
            }
            return false;
        }



        #endregion

        private void txtEndtLite_TextChanged(object sender, EventArgs e)
        {
            if (txtEndtLite.Text != "")
            {

                if (Char.IsNumber(txtEndtLite.Text, txtEndtLite.Text.Length - 1))
                {
                    txtLiterTest.Text = (Convert.ToDouble((txtEndtLite.Text == "") ? "0" : txtEndtLite.Text) -
                        Convert.ToDouble((txtStartLite.Text == "") ? "0" : txtStartLite.Text)).ToString("#0.00");
                }
            }
        }
        private void txtStartLite_TextChanged(object sender, EventArgs e)
        {
            if (txtStartLite.Text != "")
            {
                if (Char.IsNumber(txtStartLite.Text, txtStartLite.Text.Length - 1))
                {
                    txtLiterTest.Text = (Convert.ToDouble((txtEndtLite.Text == "") ? "0" : txtEndtLite.Text) -
                      Convert.ToDouble((txtStartLite.Text == "") ? "0" : txtStartLite.Text)).ToString("#0.00");
                }
            }
        }



        private void txtStartLite_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < "0"[0] || e.KeyChar > "9"[0]) && e.KeyChar != ControlChars.Back && e.KeyChar != "."[0])
                e.Handled = true;
        }

        private void txtEndtLite_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < "0"[0] || e.KeyChar > "9"[0]) && e.KeyChar != ControlChars.Back && e.KeyChar != "."[0])
                e.Handled = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Finding.FindFailureCode f = new SMSMINI.Transaction.Finding.FindFailureCode();
                string fail = "";
                f.TmpFailureCode = txtFailure.Text.Trim();
                f.TmpFaiIDOpanJOB = txtFialureCode.Text.Trim();
                f.TmpjobType = lbl_JobType.Text.Trim();
                if (f.ShowDialog() == DialogResult.OK)
                {
                    fail = f.TmpFailureCode;
                    if (fail != "")
                    {
                        lblFailureCode.Text = f.TmpFCodeID;
                        txtFailure.Text = fail;
                        setcob_Failure_Action(lblFailureCode.Text);

                        comboBox_PoitFail.Focus();
                    }
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("ข้อผิดพลาด " + ex.Message, "เกิดข้อผิดพลาด");
            }
        }

        private void setcob_PointFailure(int fFCATID)//20210201
        {
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn))
            {
                var chkPFFCAT = dc.PointFailures
                                    .Where(t => (t.FCAT_ID == fFCATID || t.FCAT_ID == null) && t.IsCancel == "Y")

                                    .Select(t => new
                                    {
                                        POI_ID = t.POI_ID,
                                        PointFailure = t.PointFailure1,
                                        OrderNo = t.OrderNo
                                    }).OrderBy(t => t.OrderNo)//20210204
                                    .ToList();
                comboBox_PoitFail.DataSource = chkPFFCAT;
                comboBox_PoitFail.DisplayMember = "PointFailure";
                comboBox_PoitFail.ValueMember = "POI_ID";
            }
        }


        private void setcob_Failure_Action(string FCodeID)
        {
            //if (UserInfo.ConnectMode == "0")
            //    strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
            //else
            //    strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;

            strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;

            try
            {
                string _typeID = UserInfo.Van_Type;
                string sql = "";
                sql = " SELECT [ATF_ID],[ATF_ID] +': '+[Action_Failure] as [Action_Failure] FROM [Failure_Action] Where [FAI_ID] = '" + FCodeID + "' or [FAI_ID]='0000000' Order by [Priority]";

                SqlDataReader dr = JaSqlHelper.ExecuteReader(strConn, CommandType.Text, sql);
                DataTable dt;

                if (dr.HasRows)
                {
                    //comboBox_PoitFail
                    dt = new DataTable();
                    dt.Load(dr);
                    cob_Failure_Action.BeginUpdate();
                    cob_Failure_Action.ValueMember = "ATF_ID";
                    cob_Failure_Action.DisplayMember = "Action_Failure";
                    cob_Failure_Action.DataSource = dt;
                    cob_Failure_Action.EndUpdate();
                    cob_Failure_Action.SelectedIndex = -1;



                }
                dr.Close();
            }
            catch (System.Exception ex)
            {
                closeProgress();
                MessageBox.Show("เกิดข้อผิดพลาด " + ex.Message, "เกิดข้อผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }



        private void dataGridView_FailPoint_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                //====== 20201002 =============================
                //if (e.ColumnIndex != -1)
                //{
                    if (editMode == "job_edit")
                    {
                        using (DAL.SMSManage.SMSManageDataContext dc = new DAL.SMSManage.SMSManageDataContext(strConn))
                        {
                            /*20210323
                             DateTime _vpEnddate = DateTime.Parse(_pEnddate);
                             var qJDEnd = (from t in dc.JOB_Detail_Spares
                                           where t.JOB_ID.Trim() == txtJobID.Text.Trim()
                                           && t.Enddate.Value.Date == _vpEnddate.Date
                                           && t.Enddate.Value.Hour == _vpEnddate.Hour
                                           && t.Enddate.Value.Minute == _vpEnddate.Minute
                                           && t.Enddate.Value.Second == _vpEnddate.Second
                                           select t).OrderByDescending(c => c.Enddate).FirstOrDefault();

                             var qJIDate = (from t in dc.JOB_Invoices
                                            where t.JOB_ID == txtJobID.Text.Trim()
                                            select t).OrderByDescending(c => c.Invoice_Date).FirstOrDefault();

                             if (qJDEnd != null && qJIDate != null)
                             {
                                     if (qJDEnd.Enddate.Value.Date <= qJIDate.Invoice_Date.Date)
                                     {
                                         return;
                                     }
                             }*/

                            //20210323 แก้ไขเงื่อนไขเรื่อง เก็บเงินสด
                            DateTime _vpEnddate = DateTime.Parse(_pEnddate);

                            var qIsPrice = (from t in dc.JOB_Detail_Spares
                                            where t.JOB_ID.Trim() == txtJobID.Text.Trim()
                                            && t.Enddate.Value.Date == _vpEnddate.Date
                                            && t.Enddate.Value.Hour == _vpEnddate.Hour
                                            && t.Enddate.Value.Minute == _vpEnddate.Minute
                                            && t.Enddate.Value.Second == _vpEnddate.Second
                                            && t.IsPrices == true //20210322 เก็บเงินสด
                                            select t).OrderByDescending(c => c.Enddate).FirstOrDefault();
                            if (qIsPrice != null)
                            {
                                //disableControlInv(false);
                            }
                            else
                            {
                                var qJDEnd = (from t in dc.JOB_Detail_Spares
                                              where t.JOB_ID.Trim() == txtJobID.Text.Trim()
                                              && t.Enddate.Value.Date == _vpEnddate.Date
                                              && t.Enddate.Value.Hour == _vpEnddate.Hour
                                              && t.Enddate.Value.Minute == _vpEnddate.Minute
                                              && t.Enddate.Value.Second == _vpEnddate.Second
                                              select t).OrderByDescending(c => c.Enddate).FirstOrDefault();

                                var qJIDate = (from t in dc.JOB_Invoices
                                               where t.JOB_ID == txtJobID.Text.Trim()
                                               select t).OrderByDescending(c => c.Invoice_Date).FirstOrDefault();

                                if (qJDEnd != null && qJIDate != null)
                                {
                                    //เงื่อนไขนี้เช็คตอน isprice = 0 ทั้งหมด
                                    if (qJDEnd.Enddate.Value.Date <= qJIDate.Invoice_Date.Date)
                                    {
                                        // disableControlInv(true);
                                        return;
                                    }
                                }
                            }
                            //==
                        }//dal
                         //=20210921==จ่ายคอมแล้วห้ามยกเลิก====
                        using (System.Data.SqlClient.SqlConnection Conn = new System.Data.SqlClient.SqlConnection(strConn))
                        {
                            if (Conn.State == ConnectionState.Closed)
                                Conn.Open();
                            //============================
                            System.Data.SqlClient.SqlDataReader dr = null;

                            string sqlC = "";
                            sqlC = " Declare @IsCommission INT " +
                                  " Exec @IsCommission = JobIsCommission '" + txtJobID.Text.Trim() + "'" +
                                  " Select @IsCommission ";


                            using (dr = new System.Data.SqlClient.SqlCommand(sqlC, Conn).ExecuteReader())
                            {
                                dr.Read();
                                if (dr.HasRows)
                                {

                                    //disableControlInv(true);
                                    return;

                                }

                            }
                        }
                        //================================

                    }

                //}
                //=============================================
                if (e.ColumnIndex == 0)//ยกเลิกข้อมูล
                {
                    #region ยกเลิกข้อมูล
                    try
                    {

                      
                        if (disableCalFNO(e.RowIndex))
                        {
                            closeProgress();
                            MessageBox.Show("คุณไม่สามารถยกเลิกรายการจุดเสียนี้ได้" + Environment.NewLine +
                            "เนื่องจากมีอะไหล่อยู่ กรุณายกเลิกรายการอะไหล่ก่อน", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }



                        if (dataGridView_FailPoint["IsDownload", e.RowIndex].Value.ToString() == "Y")
                        {
                            if (dataGridView_FailPoint["IsCancel", e.RowIndex].Value.ToString() == "1")
                            {
                                dataGridView_FailPoint["IsCancel", e.RowIndex].Value = "0";
                            }
                            else//0
                            {
                                /*20200529 if (MessageBox.Show("ไม่สามารถยกเลิกเพื่อแก้ไขข้อมูลได้ เนื่องจาก Download จาก Server" + Environment.NewLine +
                                    "คุณต้องการระบุสถานะยกเลิก ใช่หรือไม่", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                    dataGridView_FailPoint["IsCancel", e.RowIndex].Value = dataGridView_FailPoint["IsCancel", e.RowIndex].Value.ToString() == "0" ? "1" : "0";
                                setDataGridViewFailPointStyle();*/

                                //=====20200601 
                                string cFa = dataGridView_FailPoint["FAI_ID", e.RowIndex].Value.ToString();
                                string cSN = dataGridView_FailPoint["S/N(Old)", e.RowIndex].Value.ToString();
                                string cLc = dataGridView_FailPoint["Location", e.RowIndex].Value.ToString();
                                string cPo = dataGridView_FailPoint["รหัสจุดเสีย", e.RowIndex].Value.ToString();
                                if (chkSpaCurrentCanc(e.RowIndex))
                                {
                                    if (MessageBox.Show("ไม่สามารถยกเลิกเพื่อแก้ไขข้อมูลได้ เนื่องจาก Download จาก Server" + Environment.NewLine +
                                      "คุณต้องการระบุสถานะยกเลิก ใช่หรือไม่", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                    {

                                        /* 20200602 //if (dataGridView_FailPoint["IsCancel", e.RowIndex].Value.ToString() == "0")
                                        //{
                                        //    dataGridView_FailPoint["IsCancel", e.RowIndex].Value = "1";
                                        //}
                                         */
                                        if (LPartUsedPOI.Count() > 0)
                                        {
                                            //20200624 strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
                                            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn))
                                            {

                                                var checkserv = (from t in dc.JOB_Detail_Spares
                                                                 where t.JOB_ID.Trim() == txtJobID.Text.Trim()
                                                                 && t.POI_ID == cPo
                                                                 && t.Location == cLc
                                                                 && t.FAI_ID == cFa
                                                                 && t.SerialNumber == cSN
                                                                 && t.IsCancel == '0'
                                                                 && !LPartUsedPOI.Select(x => x._Lspa.ToString()).ToList().Contains(t.SPA_ID)
                                                                 select t).FirstOrDefault();
                                                if (checkserv != null)
                                                {
                                                    closeProgress();
                                                    MessageBox.Show("ไม่สามารถ ยกเลิกจุดเสีย " + checkserv.SerialNumber.ToString() + "ได้ เนื่องจาก มีอะไหล่ที่กำลังใช้งาน จุดเสียนี้ อยู่บน server", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                                    return;
                                                }
                                                else
                                                {
                                                    if (dataGridView_FailPoint["IsCancel", e.RowIndex].Value.ToString() == "0")
                                                    {
                                                        dataGridView_FailPoint["IsCancel", e.RowIndex].Value = "1";
                                                    }
                                                }
                                            }//dal
                                        }//if count
                                    }
                                }
                                else
                                {
                                    //20200529==check มีอะไหล่ที่กำลังใช้งาน จุดเสียนี้อยู่บน server หรือไม่
                                    if (MessageBox.Show("ไม่สามารถยกเลิกเพื่อแก้ไขข้อมูลได้ เนื่องจาก Download จาก Server" + Environment.NewLine +
                                       "คุณต้องการระบุสถานะยกเลิก ใช่หรือไม่", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                    {

                                        //string cFa = dataGridView_FailPoint["FAI_ID", e.RowIndex].Value.ToString();
                                        //string cSN = dataGridView_FailPoint["S/N(Old)", e.RowIndex].Value.ToString();
                                        //string cLc = dataGridView_FailPoint["Location", e.RowIndex].Value.ToString();
                                        //string cPo = dataGridView_FailPoint["รหัสจุดเสีย", e.RowIndex].Value.ToString();

                                        //20200624 strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
                                        using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn))
                                        {

                                            var checkserv = (from t in dc.JOB_Detail_Spares
                                                             where t.JOB_ID.Trim() == txtJobID.Text.Trim()
                                                             && t.POI_ID == cPo
                                                             && t.Location == cLc
                                                             && t.FAI_ID == cFa
                                                             && t.SerialNumber == cSN
                                                             && t.IsCancel == '0'

                                                             select t).FirstOrDefault();
                                            if (checkserv != null)
                                            {
                                                closeProgress();
                                                MessageBox.Show("ไม่สามารถ ยกเลิกจุดเสีย " + checkserv.SerialNumber.ToString() + "ได้ เนื่องจาก มีอะไหล่ที่กำลังใช้งาน จุดเสียนี้ อยู่บน server", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                                return;
                                            }
                                            else
                                            {
                                                dataGridView_FailPoint["IsCancel", e.RowIndex].Value = dataGridView_FailPoint["IsCancel", e.RowIndex].Value.ToString() == "0" ? "1" : "0";
                                            }
                                        }
                                    }
                                }

                                //=============

                            }
                            setDataGridViewFailPointStyle();//20200529
                        }
                        else
                        {


                            if (MessageBox.Show("คุณต้องการยกเลิกข้อมูล ใช่หรือไม่???...", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {

                                tmpCalPoitFailID = dataGridView_FailPoint["รหัสจุดเสีย", e.RowIndex].Value.ToString();
                                tmpCalSN = dataGridView_FailPoint["S/N(New)", e.RowIndex].Value.ToString();
                                //tmpCalSN = dataGridView_FailPoint["S/N(Old)", e.RowIndex].Value.ToString();

                                tmpCalFAI_ID = dataGridView_FailPoint["FAI_ID", e.RowIndex].Value.ToString();


                                tmpFAIID = dataGridView_FailPoint["FAI_ID", e.RowIndex].Value.ToString();
                                lblFailureCode.Text = tmpFAIID;

                                if (dataGridView_FailPoint["FCAT_ID", e.RowIndex].Value != DBNull.Value)
                                    FCAT_ID = Convert.ToInt32(dataGridView_FailPoint["FCAT_ID", e.RowIndex].Value);

                                if (dataGridView_FailPoint["FBA_ID", e.RowIndex].Value != DBNull.Value)
                                    FBA_ID = Convert.ToInt32(dataGridView_FailPoint["FBA_ID", e.RowIndex].Value);

                                if (dataGridView_FailPoint["FMD_ID", e.RowIndex].Value != DBNull.Value)
                                    FMD_ID = Convert.ToInt32(dataGridView_FailPoint["FMD_ID", e.RowIndex].Value);

                                string _snNew = string.Empty;
                                string _snOld = string.Empty;

                                if (dataGridView_FailPoint["S/N(New)", e.RowIndex].Value != null)
                                    _snNew = dataGridView_FailPoint["S/N(New)", e.RowIndex].Value.ToString();

                                if (dataGridView_FailPoint["S/N(Old)", e.RowIndex].Value != null)
                                    _snOld = dataGridView_FailPoint["S/N(Old)", e.RowIndex].Value.ToString();

                                lsSNCancel.Add(new SNCancel
                                {
                                    POI_ID = tmpCalPoitFailID,
                                    SNNew = _snNew,
                                    SNold = _snOld
                                }
                                );



                                if (lsSNSelect.Count() > 0)
                                {
                                    var canSNSele = lsSNSelect
                                        .Where(t => t.POI_ID == tmpCalPoitFailID &&
                                                t.SNNew == _snNew &&
                                                t.SNold == _snOld).FirstOrDefault();
                                    if (canSNSele != null)
                                        lsSNSelect.Remove(canSNSele);
                                }

                                //===20180806


                                //if (dataGridView_FailPoint["FCAT_CH", e.RowIndex].Value != DBNull.Value)
                                //    FCAT_CH = Convert.ToInt32(dataGridView_FailPoint["FCAT_CH", e.RowIndex].Value);

                                //===20180806

                                GridViewFailtoTextBox(e);
                                dtFailure.Rows[e.RowIndex].Delete();


                                FailureBinding();

                                countFailureNO();
                            }

                        }
                    }
                    catch (System.Exception ex)
                    {
                        MessageBox.Show("ข้อผิดพลาด " + ex.Message, "เกิดข้อผิดพลาด");
                    }

                    if (dataGridView_FailPoint.RowCount == 0)
                        groupBox3.Enabled = false;

                    #endregion

                }
                else if (e.ColumnIndex == 1)//เพิ่มอะไหล่
                {
                    string _pf = dataGridView_FailPoint["รหัสจุดเสีย", e.RowIndex].Value.ToString();

                    //tmpSN = dataGridView_FailPoint["S/N(New)", e.RowIndex].Value.ToString();
                    //S/N(Old)

                    //20210129 tmpSN = dataGridView_FailPoint["S/N(Old)", e.RowIndex].Value.ToString();

                    //===20210129===
                    if (editMode == "job_edit")
                    {
                        using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn))
                        {
                            var checkserv = (from t in dc.FFixedAsset_Station_Details
                                             where t.SerialNumber == dataGridView_FailPoint["S/N(New)", e.RowIndex].Value.ToString()
                                             select t).FirstOrDefault();
                            if (checkserv != null)
                            {

                                tmpSN = dataGridView_FailPoint["S/N(New)", e.RowIndex].Value.ToString();
                            }
                            else
                            {
                                tmpSN = dataGridView_FailPoint["S/N(Old)", e.RowIndex].Value.ToString();
                            }
                        }
                    }
                    else
                    {
                        tmpSN = dataGridView_FailPoint["S/N(Old)", e.RowIndex].Value.ToString();
                    }
                    tmpSNOLD = dataGridView_FailPoint["S/N(Old)", e.RowIndex].Value.ToString();
                    //==============
                    tmpLocation = dataGridView_FailPoint["Location", e.RowIndex].Value.ToString();
                    tmpFAIID = dataGridView_FailPoint["FAI_ID", e.RowIndex].Value.ToString();

                    //20141021
                    //20141127ยกเลิก lbEQP_ID.Text = dataGridView_FailPoint["EQP_ID", e.RowIndex].Value.ToString();
                    //20141127ยกเลิก int vEQP_ID = int.Parse(dataGridView_FailPoint["EQP_ID", e.RowIndex].Value.ToString());

                    //20141127ยกเลิก
                    /*string sqlEQ = "";
                    sqlEQ = " SELECT EQP_ID,Equipment_Position FROM [Equipment_Position] WHERE  EQP_ID =" + vEQP_ID + " ";
                    SqlDataReader drEQ = JaSqlHelper.ExecuteReader(strConn, CommandType.Text, sqlEQ);
                   
                        if (drEQ.HasRows)
                        {
                            while (drEQ.Read())
                            {
                                
                                //20141127ยกเลิก  txtEquipment_Position.Text = drEQ["Equipment_Position"].ToString();
                            }
                        }
                     */



                    //using (DAL.SMSManage.SMSManageDataContext db = new SMSManageDataContext())
                    //{
                    //    var q2 = (from t in db.equ where t.JOB_ID == jobid orderby t.RecDate descending select t).FirstOrDefault();

                    //    txtProblem_Detail.Text = q2.Problem_Detail ?? "";
                    //}

                    this.tabControl1.SelectedTab = tabPage2;
                    cob_PoitFail0.SelectedValue = _pf.Trim();

                    groupBox3.Enabled = true;
                    setDataGridViewStyle();
                }
                //==20200624
                else if (e.ColumnIndex == 23)//ยกเลิกหรือคืนสถานะจุดเสีย
                {
                    if (dataGridView_FailPoint["IsCancel", e.RowIndex].Value.ToString() == "0")
                    {
                        if (MessageBox.Show("คุณต้องการ ยกเลิก จุดเสีย ใช่หรือไม่???...", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            if (disableCalFNO(e.RowIndex))
                            {
                                closeProgress();
                                MessageBox.Show("คุณไม่สามารถยกเลิกรายการจุดเสียนี้ได้" + Environment.NewLine +
                                "เนื่องจากมีอะไหล่อยู่ กรุณายกเลิกรายการอะไหล่ก่อน", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                return;
                            }


                            string cFa = dataGridView_FailPoint["FAI_ID", e.RowIndex].Value.ToString();
                            string cSN = dataGridView_FailPoint["S/N(Old)", e.RowIndex].Value.ToString();
                            string cLc = dataGridView_FailPoint["Location", e.RowIndex].Value.ToString();
                            string cPo = dataGridView_FailPoint["รหัสจุดเสีย", e.RowIndex].Value.ToString();
                            if (chkSpaCurrentCanc(e.RowIndex))
                            {
                                //if (MessageBox.Show("ไม่สามารถยกเลิกเพื่อแก้ไขข้อมูลได้ เนื่องจาก Download จาก Server" + Environment.NewLine +
                                //  "คุณต้องการระบุสถานะยกเลิก ใช่หรือไม่", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                //{

                                /* 20200602 //if (dataGridView_FailPoint["IsCancel", e.RowIndex].Value.ToString() == "0")
                                //{
                                //    dataGridView_FailPoint["IsCancel", e.RowIndex].Value = "1";
                                //}
                                 */
                                if (LPartUsedPOI.Count() > 0)
                                {
                                    //20200624 strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
                                    using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn))
                                    {

                                        var checkserv = (from t in dc.JOB_Detail_Spares
                                                         where t.JOB_ID.Trim() == txtJobID.Text.Trim()
                                                         && t.POI_ID == cPo
                                                         && t.Location == cLc
                                                         && t.FAI_ID == cFa
                                                         && t.SerialNumber == cSN
                                                         && t.IsCancel == '0'
                                                         && !LPartUsedPOI.Select(x => x._Lspa.ToString()).ToList().Contains(t.SPA_ID)
                                                         select t).FirstOrDefault();
                                        if (checkserv != null)
                                        {
                                            closeProgress();
                                            MessageBox.Show("ไม่สามารถ ยกเลิกจุดเสีย " + checkserv.SerialNumber.ToString() + "ได้ เนื่องจาก มีอะไหล่ที่กำลังใช้งาน จุดเสียนี้ อยู่บน server", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                            return;
                                        }
                                        else
                                        {
                                            if (dataGridView_FailPoint["IsCancel", e.RowIndex].Value.ToString() == "0")
                                            {
                                                dataGridView_FailPoint["IsCancel", e.RowIndex].Value = "1";
                                            }
                                        }
                                    }//dal
                                }//if count
                                //}
                            }


                        }
                    }
                    else if (dataGridView_FailPoint["IsCancel", e.RowIndex].Value.ToString() == "1")
                    {
                        if (MessageBox.Show("คุณต้องการ ใช้งาน จุดเสีย  ใช่หรือไม่???...", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            dataGridView_FailPoint["IsCancel", e.RowIndex].Value = "0";

                        }
                    }
                    setDataGridViewFailPointStyle();
                }
                //==

            }
        }

        private bool disableCalFNO(int rowIndex)
        {


            string _locat;
            string _pf;

            for (int i = 0; i <= dataGridView_Part.RowCount - 1; i++)
            {
                if (dataGridView_Part["IsCancel", i].Value.ToString() == "0")
                {
                    _locat = dataGridView_Part["Location", i].Value.ToString();
                    _pf = dataGridView_Part["รหัสจุดเสีย", i].Value.ToString();

                    for (int x = 0; x <= dataGridView_FailPoint.RowCount - 1; x++)
                    {
                        if (x == rowIndex &&
                            _locat == dataGridView_FailPoint["Location", x].Value.ToString() &&
                            _pf == dataGridView_FailPoint["รหัสจุดเสีย", x].Value.ToString())
                            return true;
                    }
                }
            }

            return false;

        }
        List<LPartUsedPOI> LPartUsedPOI = null;

        private bool chkSpaCurrentCanc(int rowIndex)
        {
            string _locat;
            string _pf;
            //string _stat;
            string _fai;
            string _sn;
            string _spa;


            //dataGridView_Part["IsCancel", r].Value.ToString();
            //dataGridView_Part["สถานะ", r]
            //dataGridView_Part["รหัสจุดเสีย", r].
            //dataGridView_Part["FAI_ID", r].
            //dataGridView_Part["Location", r]
            //dataGridView_Part["S/N", i].Value

            //List<LPartUsedPOI> LPartUsedPOI = null;
            LPartUsedPOI = new List<LPartUsedPOI>();

            for (int i = 0; i <= dataGridView_Part.RowCount - 1; i++)
            {
                if (dataGridView_Part["IsCancel", i].Value.ToString() == "1")
                {//มีรายการอะไหล่ที่ถูกยกเลิก
                    _locat = dataGridView_Part["Location", i].Value.ToString();
                    _pf = dataGridView_Part["รหัสจุดเสีย", i].Value.ToString();
                    //_stat = dataGridView_Part["สถานะ", i].Value.ToString();
                    _fai = dataGridView_Part["FAI_ID", i].Value.ToString();
                    _sn = dataGridView_Part["S/N", i].Value.ToString();
                    _spa = dataGridView_Part["Part no.", i].Value.ToString();

                    //List<string> LPartUsedPOI = new List<string>();



                    for (int x = 0; x <= dataGridView_FailPoint.RowCount - 1; x++)
                    {//วนหารายการอะไหล่ที่ยกเลิกตรงกับจุดเสีย ที่ต้องการยกเลิกหรือไม่ 
                        if (x == rowIndex &&
                            _locat == dataGridView_FailPoint["Location", x].Value.ToString() &&
                            _pf == dataGridView_FailPoint["รหัสจุดเสีย", x].Value.ToString() &&
                            _fai == dataGridView_FailPoint["FAI_ID", x].Value.ToString() &&
                            _sn == dataGridView_FailPoint["S/N(Old)", x].Value.ToString())
                        {  //ถ้าตรงเก็บใส่ list

                            ////LPartUsedPOI.Add(tmpjobID.ToString(), _locat, _pf, _fai, _sn);
                            ////LPartUsedPOI.Add(tmpjobID.ToString());
                            LPartUsedPOI.Add(new LPartUsedPOI
                            {
                                _LJOBID = tmpjobID,
                                _Llocat = _locat,
                                _Lpf = _pf,
                                _Lfai = _fai,
                                _Lsn = _sn,
                                _Lspa = _spa
                            }
                             );



                            //return true;
                        }
                    }//for

                    if (LPartUsedPOI.Count() > 0)//ถ้า list > 0 
                    {
                        ////เอา list ไป query หาบน server ว่ามีรายการอะไหล่นอกเหนือจากใน list ที่กำลังใช้งานจุดเสียนี้อยู่มั้ย iscancel = 0  
                        //strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
                        //using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn))
                        //{


                        //    ////var qPartOnServer = (from jd in dc.JOB_Detail_Spares
                        //    ////                     join ls in LPartUsedPOI on jd.JOB_ID equals ls._LJOBID
                        //    ////                     where 
                        //    ////                     //jd.POI_ID == ls._Lpf &&
                        //    ////                     //jd.Location == ls._Llocat &&
                        //    ////                     //jd.SerialNumber == ls._Lsn &&
                        //    ////                     //jd.FAI_ID == ls._Lfai &&
                        //    ////                     jd.IsCancel == '0' //&&


                        //    ////                     //var a = dc.FFixedAsset_Categories
                        //    ////                     //   .Where(t => t.TYPD == '1' &&
                        //    ////                     //              t.IsCancel == false)
                        //    ////                     //              .Select(t=>t.FCAT_ID.ToString())
                        //    ////                     //              .ToList();


                        //    ////                     //LJOBChkTypFFA = a;

                        //    ////                     //(!new string [] {LPartUsedPOI.Select(t=>t._Lspa.ToString()).ToList()}.Contains(jd.SPA_ID))
                        //    ////         //!LPartUsedPOI.Select(t => t._Lspa.ToString()).ToList().Contains(jd.SPA_ID)



                        //    ////                     //!LPartUsedPOI.Contains(jd.SPA_ID) 
                        //    ////                     //LPartUsedPOI.Select(t=>t._Lspa).Contains(jd.SPA_ID));

                        //    ////                     //&& new string[] { "IN", "WA" }.Contains(t.TRA_ID)) &&
                        //    ////                     //       (!new string[] { "TRAVEL", "SERVICE" }.Contains(t.SPA_ID)
                        //    ////                     //       && !LConfNoPart.Contains(t.SPA_ID)

                        //    ////                     select jd).FirstOrDefault();

                        //    var qPartOnServer = from jd in dc.JOB_Detail_Spares
                        //                        where jd.JOB_ID == tmpjobID 
                        //                        && !LPartUsedPOI.Select(t => t._Lspa.ToString()).ToList().Contains(jd.SPA_ID)
                        //                        select jd;

                        //    if (qPartOnServer != null)
                        //    {
                        //        return false; //ถ้ามีอะไหล่อื่น return false ห้ามยกเลิก

                        //    }
                        //    else
                        //    {
                        //        return true;
                        //    }
                        //}

                        return true; //ปิดงานปัจจุบันมีรายการอะไหล่ที่ยกเลิกในจุดเสียนี้ สามารถยกเลิกจุเสียนี้ได้  แต่ ก่อนยกเลิกต้องตรวจดูว่า มีรายการอื่นที่ไม่ใช่อะไหล่ พวกนี้ที่ใช้งานอยู่บน server หรือไม่ 
                    }
                    else
                    { //ไม่มีรายการยกเลิกตรงกับจุดเสียนี้
                        return false;
                    }
                }//1
            }//for
            return false;

        }



        private void GridViewFailtoTextBox(DataGridViewCellEventArgs e)
        {
            setcob_PointFailure(FCAT_ID);//20210201
            comboBox_PoitFail.SelectedValue = dataGridView_FailPoint.Rows[e.RowIndex].Cells["รหัสจุดเสีย"].Value.ToString();

            txtSNOld.Text = dataGridView_FailPoint.Rows[e.RowIndex].Cells["S/N(Old)"].Value.ToString();
            txtFixAssetNoOld.Text = dataGridView_FailPoint.Rows[e.RowIndex].Cells["FixAssetNo"].Value.ToString();


            txtSNNew.Text = dataGridView_FailPoint.Rows[e.RowIndex].Cells["S/N(New)"].Value.ToString();
            txtFixAsetNoNew.Text = dataGridView_FailPoint.Rows[e.RowIndex].Cells["FixAssetNo_New"].Value.ToString();

            //20141214  txtModel.Text = dataGridView_FailPoint.Rows[e.RowIndex].Cells["Location"].Value.ToString();
            string tmpLOC = "";
            // if (string.IsNullOrEmpty(dataGridView_FailPoint.Rows[e.RowIndex].Cells["Location"].Value.ToString()))
            if (string.IsNullOrEmpty(dataGridView_FailPoint.Rows[e.RowIndex].Cells["Location"].Value.ToString()) || dataGridView_FailPoint.Rows[e.RowIndex].Cells["Location"].Value.ToString() == "None")
            {
                tmpLOC = "0";
            }
            else
            {
                tmpLOC = dataGridView_FailPoint.Rows[e.RowIndex].Cells["Location"].Value.ToString();
            }
            cbxLocation.SelectedValue = Convert.ToUInt32(tmpLOC);//Convert.ToInt32(dataGridView_FailPoint.Rows[e.RowIndex].Cells["Location"].Value.ToString() == null ? "0" : dataGridView_FailPoint.Rows[e.RowIndex].Cells["Location"].Value.ToString());

            //20141215 Convert.ToInt32(dataGridView_FailPoint.Rows[e.RowIndex].Cells["Location"].Value.ToString());
            txtModel.Text = dataGridView_FailPoint.Rows[e.RowIndex].Cells["Model"].Value.ToString();
            txtFailure.Text = dataGridView_FailPoint.Rows[e.RowIndex].Cells["อาการเสีย"].Value.ToString();

            txtStartLite.Text = dataGridView_FailPoint.Rows[e.RowIndex].Cells["StartLiter"].Value.ToString();
            txtEndtLite.Text = dataGridView_FailPoint.Rows[e.RowIndex].Cells["EndLiter"].Value.ToString();

            txtLiterTest.Text = dataGridView_FailPoint.Rows[e.RowIndex].Cells["LiterTest"].Value.ToString();

            //20141021
            cobOilType.SelectedValue = dataGridView_FailPoint.Rows[e.RowIndex].Cells["PRT_ID"].Value;
            //20141127ยกเลิก cob_Equipment_Position.SelectedValue = dataGridView_FailPoint.Rows[e.RowIndex].Cells["EQP_ID"].Value;
            //lblFailureCode
            setcob_Failure_Action(lblFailureCode.Text);

            cob_Failure_Action.SelectedValue = dataGridView_FailPoint.Rows[e.RowIndex].Cells["ATF_ID"].Value;
            cob_Failure_Action_Solving.SelectedValue = dataGridView_FailPoint.Rows[e.RowIndex].Cells["FAS_ID"].Value;
            //20141224 txtSolvingWI.Text = dataGridView_FailPoint.Rows[e.RowIndex].Cells["SolvingByWI"].Value.ToString();
            cob_SolvingByWI.SelectedValue = dataGridView_FailPoint.Rows[e.RowIndex].Cells["SolvingByWI"].Value;

            //===20180806
            string tmpChFCAT = "";
            if (string.IsNullOrEmpty(dataGridView_FailPoint.Rows[e.RowIndex].Cells["FCAT_CH"].Value.ToString()) || dataGridView_FailPoint.Rows[e.RowIndex].Cells["FCAT_CH"].Value.ToString() == "0")
            {
                tmpChFCAT = "0";
            }
            else
            {
                tmpChFCAT = dataGridView_FailPoint.Rows[e.RowIndex].Cells["FCAT_CH"].Value.ToString();
            }
            cbxChFCAT.SelectedValue = Convert.ToUInt32(tmpChFCAT);
            //===20180806
        }

        private void txtPartNo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    DateTime openjob = Convert.ToDateTime(Convert.ToDateTime(txtOpenJOB.Text).ToShortDateString());

                    Finding.FindSparePart f = new SMSMINI.Transaction.Finding.FindSparePart();

                    f.TmpParameter = txtPartNo.Text.Trim();

                    f.TmpDopenJOB = openjob;
                    f.TmpGopID = tmpGOPId;
                    f.TmpGopName = tmpGopName;

                    f.tmpJobType = tmpJobType;
                    f.tmpTYP_ID1 = tmpTYP_ID1;

                    f.tmpContractNo = _contractNo;

                    //f.SO_NO = txtJobSO.Text;
                    f.JOBNo = txtJobID.Text;

                    if (txtProjectNO.Text != "")
                        f.TmpProjectNo = txtProjectNO.Text.Trim();

                    string partNo = "";

                    string TmpPartName = "";
                    if (f.ShowDialog() == DialogResult.OK)
                    {
                        partNo = f.TmpPartNo;
                        tmpSPA_ID = f.TmpPartNo;
                        TmpPartName = f.TmpPartName;

                        lblERP_orderline_id.Text = f.ERP_orderline_id.ToString();

                        if (partNo != "")
                        {
                            setPartDetail(partNo, TmpPartName);



                            //=====20161114======

                            using (var dc = new SMSManageDataContext(strConn))
                            {
                                var qReferPage = (from j in dc.JOBs
                                                  from s in dc.Stations
                                                      ///from jds in dc.JOB_Detail_Spares
                                                  from con in dc.Conf_Conditions
                                                  where j.STA_ID == s.STA_ID &&
                                                  //j.JOB_ID == jds.JOB_ID &&
                                                  //jds.SPA_ID == con.SPA_ID  &&
                                                  j.ContractNo == con.ContractNo &&
                                                  s.GOP_ID == con.GOP_ID &&
                                                  j.TYP_ID == con.TYP_ID &&
                                                  // j.TYP_ID1 == con.TYP_ID1 &&
                                                  j.JOB_ID == txtJobID.Text.Trim() &&
                                                  con.SPA_ID == partNo
                                                  select con).FirstOrDefault();

                                if (qReferPage != null)
                                {
                                    txtReferPage.Text = qReferPage.ReferPage;
                                }
                                else
                                {
                                    txtReferPage.Text = "";
                                }


                            }
                            //=================
                        }

                        //20141121 
                        con_ERP_StationCharge.SelectedIndex = con_ERP_StationCharge.FindString("บริษัทน้ำมัน");

                        //เก็บเงินสดหน้าสถานี
                        if (_IsStationCharge == true)
                        {

                            con_ERP_StationCharge.SelectedIndex = con_ERP_StationCharge.FindString("สถานี");
                            //cob_TrnType.SelectedValue = "IN";
                            cob_TrnType.SelectedIndex = cob_TrnType.FindString("[IN] Invoice");

                            chIsPrices.Checked = true;//พิมพ์ราคาออก SV
                        }



                        using (var dc = new SMSManageDataContext(strConn))
                        {
                            var q = dc.SpareParts.Where(t => t.SPA_ID == txtPartNo.Text.Trim() && t.IsCloseJobCheckSN == true)
                                .FirstOrDefault();

                            if (q != null)
                            {
                                //popUp_SN
                                string tmpSNPart;
                                //Finding.Find_SNPart fprt = new SMSMINI.Transaction.Finding.Find_SNPart();
                                var fprt = new SMSMINI.Transaction.Finding.Find_SNPart();

                                fprt.fTmpPartNo = txtPartNo.Text.Trim();

                                var result = fprt.ShowDialog();
                                if (result == DialogResult.OK)
                                {
                                    //=====
                                    tmpSNPart = fprt.SNpart;
                                    var qSN = dc.SpareParts_SerialNumbers.Where(t => t.SPA_ID == txtPartNo.Text.Trim() && t.SerialNumber == tmpSNPart)
                                    .FirstOrDefault();
                                    if (qSN != null)
                                    {
                                        txtPartSerialNumberOld.Text = qSN.SerialNumber.ToString();

                                        ////20140925
                                        //txtPartSerialNumberOld.Enabled = false;

                                        //20141112
                                        txtPartSerialNumberOld.Enabled = true;

                                        //20140925 if (DateTime.Now <= qSN.ExpiryDate)//อยู่ในประกัน
                                        DateTime chkGetDate = dc.ExecuteQuery<DateTime>("SELECT GETDATE()").First();
                                        if (chkGetDate <= qSN.ExpiryDate)
                                        {
                                            cob_TrnType.SelectedValue = "WA";
                                        }

                                    }
                                }//if (f.ShowDialog() == DialogResult.OK)
                                else
                                {//20140925

                                    txtPartSerialNumberOld.Text = "";
                                    //20141112 txtPartSerialNumberOld.Enabled = false;
                                    txtPartSerialNumberOld.Enabled = true;
                                }

                            }//if (q != null)

                            //20141112
                            else
                            {
                                txtPartSerialNumberOld.Text = "";
                                txtPartSerialNumberOld.Enabled = true;
                            }

                        }


                    }//if (f.ShowDialog() == DialogResult.OK)


                }
                catch (System.Exception ex)
                {
                    MessageBox.Show("ข้อผิดพลาด " + ex.Message, "เกิดข้อผิดพลาด");
                }


                txtPartQT.Focus();
                //txtVAT.Text = _vat.ToString();
                sumVAT();
            }



            // con_ERP_StationCharge.SelectedIndex = con_ERP_StationCharge.FindString("บริษัทน้ำมัน");

            SMG_Edit();
        }

        private void SMG_Edit()
        {
            var smg = new string[] { "09", "12", "13" }.Contains(UserInfo.UserLevID);
            var backup = GetConnection.Connection.Contains("ServicesMSDB_");

            //if (smg && backup)
            if (smg)
            {

                txtPriceList.Enabled = true;
                txtPriceList.ReadOnly = false;
                con_ERP_StationCharge.Enabled = true;
                cob_TrnType.Enabled = true;
                cob_TrnType.Enabled = true;
            }

        }

        private void setPartDetail(string partNo, string TmpPartName)
        {
            ClearPartText();

            //***********************************//
            //เก็บเงินทุก Part ดีฟอลต์ TranType = IN
            cob_TrnType.SelectedValue = "IN";

            getPricesList(partNo, TmpPartName);

            //string workTypID = "";//20200420
            //if (!string.IsNullOrEmpty(txtWorkType.Text)) //20200420 
            //        workTypID = txtWorkType.Text.Split(':')[0]; //20200420 


            var jobID = txtJobID.Text.Substring(4, 1).ToString();
            // if (jobID == "D") //งานตู้จ่าย //20160405 ไม่ lock ประเภท job เกี่ยวเนื่องงาน I
            // {//20160405 ไม่ lock ประเภท job เกี่ยวเนื่องงาน I


            //20200420 ก่อนเปลี่ยน การ check warranty
            //20200420 check_FFA_Warrant(tmpSN, partNo);//เช็คอะไหล่ในประกัน

            //20200420 check warranty เฉพาะ ประเภทงาน WA 
            if (tmpTYP_ID1 == "WA")
            {
                check_FFA_Warrant(tmpSN, partNo);//เช็คอะไหล่ในประกัน

            }
            //===================================

            var Trantype = cob_TrnType.SelectedValue.ToString();

            //20151104check_FFA_Price_Charges(tmpSN, partNo);//เช็คอะไหล่ในประกัน เก็บเงิน Part พิเศษ   

            //20181129 20181129 เอาออกเพราะเจอ bug เคลียร์ค่า wa ที่ต้องการเก็บเงินอะไหล่ เพราะ process ซ้ำกัน 
            //check_FFA_Price_Charges(tmpSN, partNo, Trantype);

            //}//20160405 ไม่ lock ประเภท job เกี่ยวเนื่องงาน I

            //เช็คอะไหล่ ตามเงื่อนไขสัญญา
            string workTypID = "";
            //if (new string[] { "D", "S" }.Contains(jobID)) //20160405 ไม่ lock ประเภท job เกี่ยวเนื่องงาน I
            //{//เฉพาะ JOB ระบบ และ JOB ตู้จ่าย //20160405 ไม่ lock ประเภท job เกี่ยวเนื่องงาน I

            if (!string.IsNullOrEmpty(txtWorkType.Text))
                workTypID = txtWorkType.Text.Split(':')[0];

            /*//20200420 ก่อนเปลี่ยน การ check warranty
             * 
            //20181129 เอาออกเพราะเจอ bug เคลียร์ค่า wa ที่ต้องการเก็บเงินอะไหล่ เพราะ process ซ้ำกัน                    
            //==20181129
            if (!check_FFA_Price_Charges(tmpSN, partNo, Trantype))//ตู้จ่ายไม่อยู่ในประกัน
            {
            //==20181129   
 

                if (!check_Conf_Conditions(partNo, workTypID))//อะไหล่ไม่มีในเงื่อนไข conf_config
                {
                    // SMS MINI v.17.0.9.2014
                    // ปรับปรุง
                    //  งานนอกสัญญา Default TranType ตาม Part master
                    DefaultTranTypeByPartMaster(partNo);

                }

                //20190114 ตู้ไม่อยู่ในประกัน check Lifetime
                setLifetime(tmpSN, partNo);
                //==================================
            }
        */

            //======20200420 check warranty เฉพาะ ประเภทงาน WA 
            if (tmpTYP_ID1 == "WA")
            {
                if (!check_FFA_Price_Charges(tmpSN, partNo, Trantype))//ตู้จ่ายไม่อยู่ในประกัน
                {
                    if (!check_Conf_Conditions(partNo, workTypID))//อะไหล่ไม่มีในเงื่อนไข conf_config
                    {
                        // SMS MINI v.17.0.9.2014
                        // ปรับปรุง
                        //  งานนอกสัญญา Default TranType ตาม Part master
                        DefaultTranTypeByPartMaster(partNo);
                    }
                    //20190114 ตู้ไม่อยู่ในประกัน check Lifetime
                    setLifetime(tmpSN, partNo);
                    //==================================
                }
            }
            else
            {
                check_FFA_Price_ChargesExceptWA(tmpSN, partNo, Trantype);

                //ต้อง check เงื่อนไขสัญญา อีกรอบ เพราะความสำคัญใหญ่กว่า
                if (!check_Conf_Conditions(partNo, workTypID))//อะไหล่ไม่มีในเงื่อนไข conf_config
                {
                    // SMS MINI v.17.0.9.2014
                    // ปรับปรุง
                    //  งานนอกสัญญา Default TranType ตาม Part master
                    DefaultTranTypeByPartMaster(partNo);
                }

                //20190114 ตู้ไม่อยู่ในประกัน check Lifetime
                setLifetime(tmpSN, partNo);
                //==================================
            }
            //======20200420 check warranty เฉพาะ ประเภทงาน WA 




            //-==20190114==เช็คราคาพิเศษ=========
            setSpecialPrice(tmpSN, partNo);
            //-==============================

            //-==20190725==เช็คงานอุบัติเหตุ=========
            //20200420 ยกเลิกการ check warranty  if (check_job_type_trantype(tmpSN, partNo))//ตู้จ่ายในประกัน 
            //20200420 ยกเลิกการ check warranty {
            var _setTrantype = cob_TrnType.SelectedValue.ToString();

            using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext(strConn))
            {
                var sConfTrType = (from t in dc.Conf_JOB_Type_TranTypes
                                   where t.GOP_ID == tmpGOPId &&
                                         t.TYP_ID == tmpJobType &&
                                         t.TYP_ID1 == tmpTYP_ID1
                                   select t).FirstOrDefault();

                if (sConfTrType != null)//ถ้ามี งาน AC ...
                {

                    if (sConfTrType.TRA_ID != null)
                        _setTrantype = sConfTrType.TRA_ID;

                    try
                    {
                        cob_TrnType.SelectedValue = _setTrantype;
                    }
                    catch (System.Exception)
                    {
                    }
                }
            }
            //20200420 ยกเลิกการ check warranty }
            //-==============================




            //--20181107 -
            if (txtProjectNO.Text.Trim() != "" && !string.IsNullOrEmpty(txtProjectNO.Text.Trim().ToString()))
            {
                //if (checkPartProject(partNo))
                //{
                setPartProject(partNo);
                //}
            }

            //--20181107 -

            check_SpareParts_IsTravel(partNo, workTypID);
            //} //20160405 ไม่ lock ประเภท job เกี่ยวเนื่องงาน I

            //==20160622==part 19==IS==

            check_partIS(partNo, workTypID);
            //==20160622==part 19==IS==


            lockTranType();

            SparepartIsPriceChange(partNo);//20181122
        }

        //--20181107 -
        private void setPartProject(string partNo)
        {
            //20181123 using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn))
            {

                int quantity = 0;
                quantity = int.Parse(txtPartQT.Text);

                var setPriTRA = (from d in dc.JOB_Project_Details
                                 from p in dc.JOB_Projects
                                 where p.PROJ_ID == d.PROJ_ID &&
                                 d.PROJ_ID.Trim() == txtProjectNO.Text.Trim() &&
                                 d.SPA_ID == partNo

                                 select d).FirstOrDefault();
                if (setPriTRA != null)
                {
                    txtPriceList.Text = Convert.ToDouble(setPriTRA.Part_Prices).ToString("#,##0.00");
                    txtDiscQuarter.Text = "0";
                    txtPriceUnit.Text = (Convert.ToDouble(setPriTRA.Part_Prices) * quantity).ToString("#,##0.00");
                    cob_TrnType.SelectedValue = setPriTRA.TRA_ID;
                }

            }//using
        }



        //--20160622
        private void check_partIS(string partNo, string TypID1)
        {

            var vPartNo = partNo.Substring(0, 2).ToString();

            if (vPartNo == "19" && TypID1 != "OU")
            {
                cob_TrnType.SelectedValue = "IS";//ไม่เก็บเงิน
                con_ERP_StationCharge.SelectedIndex = con_ERP_StationCharge.FindString("บริษัทน้ำมัน");
                chIsPrices.Checked = false;
            }
            else if (vPartNo == "19" && TypID1 == "OU")
            {
                //cob_TrnType.SelectedValue = "IN"; //รอคำสั่งอนุมัติเรื่องการจ่ายคอม แล้วให้ใช้ตัวนี้//20160722
                cob_TrnType.SelectedValue = "IS";
                con_ERP_StationCharge.SelectedIndex = con_ERP_StationCharge.FindString("บริษัทน้ำมัน");
                chIsPrices.Checked = false;
            }

        }
        private void DefaultTranTypeByPartMaster(string partNo)
        {
            using (SMSManageDataContext dc = new SMSManageDataContext(strConn))
            {
                var q = dc.SpareParts
                                .Where(t => t.SPA_ID == partNo)
                                .FirstOrDefault();

                if (q != null && !string.IsNullOrEmpty(q.TranType) && q.TranType != "00")
                {
                    cob_TrnType.SelectedValue = q.TranType;
                }
            }
        }


        private void check_FFA_Warrant(string tmpSN, string partNo)
        {
            //20151104 เพิ่มตัวแปรเข้า fn
            string vTrantype = null;
            //0 ตรวจสอบสัญญา
            var dnow = DateTime.Today.Date;
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext(strConn))
            {
                var ffa = dc.FFixedAsset_Station_Details
                   .Where(t => t.STA_ID == tmpSTAID && t.SerialNumber == tmpSN && t.WarrantDate > dnow)
                    //.Where(t => t.SerialNumber == tmpSN && t.WarrantDate > dnow)
                    .FirstOrDefault();
                if (ffa != null)
                {

                    cob_TrnType.SelectedValue = "WA";// WA=อะไหล่อยู่ในประกัน
                    vTrantype = "WA";



                    var spw = dc.FFixedAsset_Part_Warranties
                        .Where(t => t.SPA_ID == partNo)
                        .FirstOrDefault();

                    string partMess = "";
                    if (spw != null)
                    {
                        int? partTypeWar = 0;
                        switch (spw.PartType)
                        {
                            case '1': //PartType 1=จำนวน ปี รับประกันของ มือจ่าย       
                                partTypeWar = ffa.PartType1;
                                partMess = "มือจ่าย";
                                break;

                            case '2': //PartType 2=จำนวน ปี รับประกันของ สายจ่าย
                                partTypeWar = ffa.PartType1;
                                partMess = "สายจ่าย";
                                break;

                            case '3': //PartType 3=จำนวน ปี รับประกันของ เบรคอะเว 
                                partTypeWar = ffa.PartType1;
                                partMess = "เบรคอะเว";
                                break;

                            default: break;
                        }

                        if (dnow > ffa.InstallDate.Value.AddYears(partTypeWar.Value))//Part หมดประกัน
                        {
                            cob_TrnType.SelectedValue = "IN";//เก็บเงิน
                            vTrantype = "IN";
                            MessageBox.Show(
                                //20210129 "Serial Number [" + tmpSN + "] นี้ อยู่ในประกัน..." + Environment.NewLine +
                                "Serial Number [" + tmpSNOLD + "] นี้ อยู่ในประกัน..." + Environment.NewLine +
                                "แต่ Part:" + partNo + "[" + partMess + "] หมดประกัน" + Environment.NewLine +
                                "TranType:=IN", "ผลการตรวจสอบ...", MessageBoxButtons.OK);
                        }
                        else
                        {

                            //20210129 MessageBox.Show("Serial Number [" + tmpSN + "] นี้" + Environment.NewLine +
                            MessageBox.Show("Serial Number [" + tmpSNOLD + "] นี้" + Environment.NewLine +
                               "อยู่ในประกัน TranType:=WA", "ผลการตรวจสอบ...", MessageBoxButtons.OK);
                        }

                    }
                    else
                    {
                        //20210129 MessageBox.Show("Serial Number [" + tmpSN + "] นี้" + Environment.NewLine +
                        MessageBox.Show("Serial Number [" + tmpSNOLD + "] นี้" + Environment.NewLine +
                            "อยู่ในประกัน TranType:=WA", "ผลการตรวจสอบ...", MessageBoxButtons.OK);
                    }
                }
            }

        }


        //20151104  

        // private void check_FFA_Price_Charges(string tmpSN, string partNo)
        //20181129 private void check_FFA_Price_Charges(string tmpSN, string partNo,string vTrantype )
        private bool check_FFA_Price_Charges(string tmpSN, string partNo, string vTrantype)
        {
            //1...
            //Check ตู้จ่ายอยู่ในประกัน
            //2...
            //Check Part เก็บเงิน
            con_ERP_StationCharge.SelectedIndex = con_ERP_StationCharge.FindString("บริษัทน้ำมัน");

            bool bPriceCharges = false;
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext(strConn))
            {

                //0 ตรวจสอบสัญญา
                var dnow = DateTime.Today.Date;

                var con0 = dc.Conf_Contracts
                    .Where(t => t.ContractNo.Contains(_contractNo))
                    .FirstOrDefault();
                if (con0 != null)
                {
                    var con = dc.Conf_Contracts
                        .Where(t => t.StartDate.Date <= dnow && t.EndDate >= dnow &&
                            t.ContractNo.Contains(_contractNo))
                            .FirstOrDefault();
                    if (con == null)
                    {//สัญญาหมด
                        //20181129 return;
                        return false; //20181129
                    }
                }

                //20151104ก่อนเปลี่ยนเงื่อนไขให้อิงตามตู้ในประกัน 
                //ยกเลิก var _Trantyp = "IN"; //จะยกไปไว้ใน flow ตู้ไม่อยู่ในประกัน
                var _Trantyp = vTrantype;

                //1... 
                //Check ตู้จ่ายอยู่ในประกัน
                var ffa = from t in dc.FFixedAsset_Station_Details
                          where t.STA_ID == tmpSTAID &&
                                t.SerialNumber == tmpSN &&
                                t.WarrantDate.Value > DateTime.Now.Date
                          select t;

                //20200420 if (ffa.Count() > 0)//<==| ถ้าตู้จ่ายอยู่ในประกัน
                if (ffa.Count() > 0)//<==| ถ้าตู้จ่ายอยู่ในประกัน
                {

                    # region ถ้าตู้จ่ายอยู่ในประกัน
                    /*
                    //Check Part เก็บเงิน,<==| ตามสถานี
                    var priceCh = (from t in dc.Conf_PartPrice_Charges
                                   where t.STA_ID == tmpSTAID &&//<==| ตามสถานี
                                         t.GOP_ID == tmpGOPId &&
                                         t.SPA_ID == partNo
                                   select t).FirstOrDefault();

                    if (priceCh != null)//ถ้ามี Part เก็บค่าใช้จ่าย...
                    {
                        //cob_TrnType.SelectedValue = "IN";
                        //txtPriceList.Text = priceCh.PriceList.ToString("#,##0.00");

                        //====20150408============
                        //20150420 var _Trantyp = "IN";
                        if (priceCh.TRA_ID != null)
                            _Trantyp = priceCh.TRA_ID;

                        try
                        {
                            cob_TrnType.SelectedValue =_Trantyp;
                            txtPriceList.Text = priceCh.PriceList.ToString("#,##0.00");
                        }
                        catch (System.Exception )
                        {
                        }
                        //========================
                    }
                    else
                    {
                        //Check Part เก็บเงิน,<==| ตามบริษัท
                        var priceCh_stg = (from t in dc.Conf_PartPrice_Charges
                                           where t.GOP_ID == tmpGOPId &&//<==| ตามบริษัท
                                                 t.SPA_ID == partNo
                                           select t).FirstOrDefault();

                        if (priceCh_stg != null)//ถ้ามี Part เก็บค่าใช้จ่าย...
                        {
                            //====20150408============

                            //cob_TrnType.SelectedValue = "IN";
                            //txtPriceList.Text = priceCh_stg.PriceList.ToString("#,##0.00");

                            //var _Trantyp2 = "IN";
                            if (priceCh_stg.TRA_ID != null)
                                _Trantyp = priceCh_stg.TRA_ID;

                            try
                            {
                                cob_TrnType.SelectedValue = _Trantyp;//_Trantyp2;
                                txtPriceList.Text = priceCh_stg.PriceList.ToString("#,##0.00");
                            }
                            catch (System.Exception)
                            {
                            }
                            //=======================

                            */

                    //20150422
                    var priceCh = (from t in dc.Conf_PartPrice_Charges
                                   where t.SPA_ID == partNo
                                   select t).FirstOrDefault();

                    if (priceCh != null)//ถ้ามี Part เก็บค่าใช้จ่าย...
                    {
                        //cob_TrnType.SelectedValue = "IN";
                        //txtPriceList.Text = priceCh.PriceList.ToString("#,##0.00");

                        //====20150408============
                        //20150420 var _Trantyp = "IN";
                        if (priceCh.TRA_ID != null)
                            _Trantyp = priceCh.TRA_ID;

                        try
                        {
                            cob_TrnType.SelectedValue = _Trantyp;
                            //20150504 ให้ใช้ราคา getPriceList txtPriceList.Text = priceCh.PriceList.ToString("#,##0.00");
                        }
                        catch (System.Exception)
                        {
                        }
                        //========================

                    }
                    //else
                    //{
                    //    cob_TrnType.SelectedValue = "CS";
                    //    //txtPriceList.Text = "0.00";

                    //    //2...
                    //    //Check Part เก็บเงิน
                    //    //ตรวจสอบเงื่อนไข Part เก็บเงินสด
                    //    //check_Conf_Conditions(partNo);
                    //}


                    //20150420 
                    else
                    { //ถ้าไม่มีใน Conf_PartPrice_Charges ให้ตรวจสอบใน condition



                        //var vChkCond = (from t in dc.Conf_Conditions
                        //       where t.STA_ID == tmpSTAID &&//<==| ตามสถานี
                        //             t.GOP_ID == tmpGOPId &&
                        //             t.SPA_ID == partNo
                        //       select t).FirstOrDefault();

                        //===========================================




                        //===========================================

                        ////20150427 เช็คงานในสัญญาและนอกสัญญา
                        ////if (tmpTYP_ID1 == "GR") //ในสัญญา
                        ////{
                        ////    var vChkCond = dc.Conf_Conditions
                        ////                                .Where(t =>
                        ////                                    t.GOP_ID == tmpGOPId &&
                        ////                                    t.TYP_ID == tmpJobType &&
                        ////                                    t.TYP_ID1 == tmpTYP_ID1 &&
                        ////                                            t.SPA_ID == partNo &&
                        ////        t.ContractNo.Contains(_contractNo)
                        ////                               ).FirstOrDefault();

                        ////    if (vChkCond != null)//ถ้ามี Part เก็บค่าใช้จ่าย...
                        ////    {
                        ////        if (vChkCond.TRA_ID != null)
                        ////            _Trantyp = vChkCond.TRA_ID;

                        ////        try
                        ////        {
                        ////            cob_TrnType.SelectedValue = _Trantyp;//_Trantyp2;
                        ////            txtPriceList.Text = vChkCond.PriceList.ToString("#,##0.00");
                        ////        }
                        ////        catch (System.Exception)
                        ////        {
                        ////        }
                        ////    }
                        ////}
                        ////else if (tmpTYP_ID1 == "OU") //งานนอกสัญญา
                        ////{ 


                        ////}


                        //===========================================


                        /* 
                        var vChkCond = dc.Conf_Conditions
                                                 .Where(t =>
                                                            //t.GOP_ID == tmpGOPId &&
                                                            //t.TYP_ID == tmpJobType &&
                                                            //t.TYP_ID1 == tmpTYP_ID1 &&
                                                             t.SPA_ID == partNo //&&
                                                            //t.ContractNo.Contains(_contractNo)
                                                ).FirstOrDefault();

                            if (vChkCond != null)//ถ้ามี Part เก็บค่าใช้จ่าย...
                            {
                                  if (vChkCond.TRA_ID != null)
                                _Trantyp = vChkCond.TRA_ID;

                                try
                                {
                                    cob_TrnType.SelectedValue = _Trantyp;//_Trantyp2;
                                    txtPriceList.Text = vChkCond.PriceList.ToString("#,##0.00");
                                }
                                catch (System.Exception)
                                {
                                }


                        }
                    else
                        {

                                var vConfConD = (from t in dc.Conf_Conditions
                                                 where (t.GOP_ID == tmpGOPId) &&    //<==| ตามบริษัท
                                                       t.SPA_ID == partNo
                                                 select t).FirstOrDefault();

                                if (vConfConD != null)//ถ้ามี Part เก็บค่าใช้จ่าย...
                                {
                                      if (vConfConD.TRA_ID != null)
                                _Trantyp = vConfConD.TRA_ID;

                                try
                                {
                                    cob_TrnType.SelectedValue = _Trantyp;//_Trantyp2;
                                    txtPriceList.Text = vConfConD.PriceList.ToString("#,##0.00");
                                }
                                catch (System.Exception)
                                {
                                }
                            }

                    }//else {   //Conf_Conditions 
                   */
                        //======================20150504==================================
                        //20151104ก่อนเปลี่ยนเงื่อนไขให้อิงตามตู้ในประกัน 
                        //ยกเลิก _Trantyp ="CS";
                        _Trantyp = vTrantype;


                        try
                        {
                            cob_TrnType.SelectedValue = _Trantyp;//_Trantyp2;

                        }
                        catch (System.Exception)
                        {
                        }
                        //========================================================
                        /* ยกเลิก เพราะหากไม่มีใน   Conf_PartPrice_Charges ให้กำหนดเป็น  CS
                          if (_contractNo == null) //ไม่มีเลขที่สัญญา
                          {
                              var vChkCond = dc.Conf_Conditions
                                                   .Where(t =>
                                                              t.GOP_ID == tmpGOPId &&
                                                             t.TYP_ID == tmpJobType &&
                                                              t.TYP_ID1 == tmpTYP_ID1 &&
                                                               t.SPA_ID == partNo &&
                                  t.TRA_ID.Contains("IN")
                                                  )//.FirstOrDefault();
                              .OrderByDescending (r=>r.PriceList )
                              .FirstOrDefault();

                              if (vChkCond != null)//ถ้ามี Part เก็บค่าใช้จ่าย...
                              {
                                  if (vChkCond.TRA_ID != null)
                                      _Trantyp = vChkCond.TRA_ID;

                                  try
                                  {
                                      cob_TrnType.SelectedValue = _Trantyp;//_Trantyp2;
                                     // txtPriceList.Text = vChkCond.PriceList.ToString("#,##0.00");
                                  }
                                  catch (System.Exception)
                                  {
                                  }


                              }


                          }//ไม่มีเลขที่สัญญา
                          else
                          {//มีเลขที่สัญญา
                              var vChkCond = dc.Conf_Conditions
                                                   .Where(t =>
                                                              t.GOP_ID == tmpGOPId &&
                                                             t.TYP_ID == tmpJobType &&
                                                              t.TYP_ID1 == tmpTYP_ID1 &&
                                                               t.SPA_ID == partNo &&
                                  t.ContractNo.Contains(_contractNo)
                                                  ).FirstOrDefault();

                              if (vChkCond != null)//ถ้ามี Part เก็บค่าใช้จ่าย...
                              {
                                  if (vChkCond.TRA_ID != null)
                                      _Trantyp = vChkCond.TRA_ID;

                                  try
                                  {
                                      cob_TrnType.SelectedValue = _Trantyp;//_Trantyp2;
                                      // txtPriceList.Text = vChkCond.PriceList.ToString("#,##0.00");
                                  }
                                  catch (System.Exception)
                                  {
                                  }


                              }

                          }//ไม่มีเลขที่สัญญา
                          */
                    }//ถ้าไม่มีใน Conf_PartPrice_Charges ให้ตรวจสอบใน condition
                    #endregion


                    bPriceCharges = true; //20181129
                } //if (ffa.Count() > 0)//<==| ถ้าตู้จ่ายอยู่ในประกัน
                else
                { //ถ้าตู้จ่ายไม่อยู่ในประกัน

                    //20150421
                    // cob_TrnType.SelectedValue = "IN";


                    #region ถ้าตู้จ่ายไม่อยู่ในประกัน
                    /*
                    ////2...
                    ////Check Part เก็บเงิน
                    ////ตรวจสอบเงื่อนไข Part เก็บเงินสด
                    ////check_Conf_Conditions(partNo);

                     
                     
                    //var ffa2 = from t in dc.FFixedAsset_Station_Details
                    //           from s in dc.Stations
                    //           from st in dc.Station_Groups
                    //           where (t.STA_ID == s.STA_ID &&
                    //                 s.GOP_ID == tmpGOPId) &&
                    //                 (t.SerialNumber == tmpSN &&
                    //                 t.WarrantDate.Value.Date >= DateTime.Now.Date)
                    //           select t;

                    //if (ffa2.Count() > 0)
                    //{
                    //    var _Trantyp = "IN";
                    //    //Check Part เก็บเงิน,<==| ตามสถานี
                    //    var priceCh = (from t in dc.Conf_PartPrice_Charges
                    //                   where t.STA_ID == tmpSTAID &&//<==| ตามสถานี
                    //                         t.GOP_ID == tmpGOPId &&
                    //                         t.SPA_ID == partNo
                    //                   select t).FirstOrDefault();

                    //    if (priceCh != null)//ถ้ามี Part เก็บค่าใช้จ่าย...
                    //    {

                    //       //20150408
                    //        //cob_TrnType.SelectedValue = "IN";
                    //       //txtPriceList.Text = priceCh.PriceList.ToString("#,##0.00");
                    //        //====20150408============
                    //        //20150420 var _Trantyp = "IN";
                    //        if (priceCh.TRA_ID != null)
                    //            _Trantyp = priceCh.TRA_ID;

                    //        try
                    //        {
                    //            cob_TrnType.SelectedValue = _Trantyp;
                    //            txtPriceList.Text = priceCh.PriceList.ToString("#,##0.00");
                    //        }
                    //        catch (System.Exception )
                    //        {
                    //        }
                    //        //========================
                    //    }
                    //    else
                    //    {
                    //        //Check Part เก็บเงิน,<==| ตามบริษัท
                    //        var priceCh_stg = (from t in dc.Conf_PartPrice_Charges
                    //                           where t.GOP_ID == tmpGOPId &&//<==| ตามบริษัท
                    //                                 t.SPA_ID == partNo
                    //                           select t).FirstOrDefault();

                    //        if (priceCh_stg != null)//ถ้ามี Part เก็บค่าใช้จ่าย...
                    //        {
                    //            //===20150408=============
                    //           //cob_TrnType.SelectedValue = "IN";
                    //            //txtPriceList.Text = priceCh_stg.PriceList.ToString("#,##0.00");

                    //            //20150420 var _Trantyp2 = "IN";
                    //            if (priceCh_stg.TRA_ID != null)
                    //                _Trantyp = priceCh_stg.TRA_ID;

                    //            try
                    //            {
                    //                cob_TrnType.SelectedValue = _Trantyp;//_Trantyp2;
                    //                txtPriceList.Text = priceCh_stg.PriceList.ToString("#,##0.00");
                    //            }
                    //            catch (System.Exception )
                    //            {
                    //            }
                    //            //==========================

                    //        }







                    //        //else
                    //        //{
                    //        //    cob_TrnType.SelectedValue = "CS";
                    //        //    //txtPriceList.Text = "0.00";

                    //        //    //2...
                    //        //    //Check Part เก็บเงิน
                    //        //    //ตรวจสอบเงื่อนไข Part เก็บเงินสด
                    //        //    //check_Conf_Conditions(partNo);
                    //        //}
                    //    }
                    //} //if (ffa2.Count() > 0)
                    ////else//หมดประกัน
                    ////{
                    ////    cob_TrnType.SelectedValue = "IN";
                    ////    //txtPriceList.Text = "0.00";
                    ////    //check_Conf_Conditions(partNo);
                    ////}
                    */

                    _Trantyp = "IN";//ตู้จ่ายไม่อยู่ในประกัน ต้องเก็บเงิน ยกลงมาจากด้านบน //20151104

                    //=============20150504=========================
                    if (!string.IsNullOrEmpty(_contractNo) && _contractNo != "null")
                    {
                        #region มีเลขที่สัญญา
                        var vConf = (from t in dc.Conf_Conditions
                                     from sp in dc.SpareParts
                                     where (t.SPA_ID == sp.SPA_ID &&
                                         t.SPA_ID == partNo &&
                                         t.GOP_ID == tmpGOPId &&
                                         t.TYP_ID == tmpJobType &&
                                         t.TYP_ID1 == tmpTYP_ID1) &&
                                         t.ContractNo == _contractNo
                                     select t).FirstOrDefault();

                        if (vConf != null)
                        {
                            if (vConf.TRA_ID != null)
                                _Trantyp = vConf.TRA_ID;

                            try
                            {
                                cob_TrnType.SelectedValue = _Trantyp;

                            }
                            catch (System.Exception)
                            {
                            }

                        }
                        else
                        {//อะไหล่ใน warranty มีเลขที่สัญญา แต่หาไม่เจอ
                            // _Trantyp = "IN";
                            //20181012 มีนโยบายให้ไปใช้ใน ตราราง sparepart แทน  cob_TrnType.SelectedValue = _Trantyp;
                            DefaultTranTypeByPartMaster(partNo);  //20181012

                        }
                        #endregion

                    }
                    else
                    {
                        /*
                       //อะไหล่นอกwarranty ถ้า ไม่มี เลขที่สัญญา ให้ Trantyp = IN ทั้งหมด
                       */
                        //_Trantyp = "IN";
                        //20181012 มีนโยบายให้ไปใช้ใน ตราราง sparepart แทน  cob_TrnType.SelectedValue = _Trantyp;
                        DefaultTranTypeByPartMaster(partNo);  //20181012
                    }








                    //============================================


                    #endregion


                    bPriceCharges = false; //20181129
                }//หมดประกัน
            }  //using (DAL

            return bPriceCharges;//20181129
        }

        private bool check_FFA_Price_ChargesExceptWA(string tmpSN, string partNo, string vTrantype)
        {
            //1...
            //Check ตู้จ่ายอยู่ในประกัน
            //2...
            //Check Part เก็บเงิน
            con_ERP_StationCharge.SelectedIndex = con_ERP_StationCharge.FindString("บริษัทน้ำมัน");

            bool bPriceCharges = false;
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext(strConn))
            {

                //0 ตรวจสอบสัญญา
                var dnow = DateTime.Today.Date;

                var con0 = dc.Conf_Contracts
                    .Where(t => t.ContractNo.Contains(_contractNo))
                    .FirstOrDefault();
                if (con0 != null)
                {
                    var con = dc.Conf_Contracts
                        .Where(t => t.StartDate.Date <= dnow && t.EndDate >= dnow &&
                            t.ContractNo.Contains(_contractNo))
                            .FirstOrDefault();
                    if (con == null)
                    {//สัญญาหมด
                        //20181129 return;
                        return false; //20181129
                    }
                }

                //20151104ก่อนเปลี่ยนเงื่อนไขให้อิงตามตู้ในประกัน 
                //ยกเลิก var _Trantyp = "IN"; //จะยกไปไว้ใน flow ตู้ไม่อยู่ในประกัน
                var _Trantyp = vTrantype;

                #region ถ้าตู้จ่ายไม่อยู่ในประกัน
                _Trantyp = "IN";//ตู้จ่ายไม่อยู่ในประกัน ต้องเก็บเงิน ยกลงมาจากด้านบน //20151104

                //=============20150504=========================
                if (!string.IsNullOrEmpty(_contractNo) && _contractNo != "null")
                {
                    #region มีเลขที่สัญญา
                    var vConf = (from t in dc.Conf_Conditions
                                 from sp in dc.SpareParts
                                 where (t.SPA_ID == sp.SPA_ID &&
                                     t.SPA_ID == partNo &&
                                     t.GOP_ID == tmpGOPId &&
                                     t.TYP_ID == tmpJobType &&
                                     t.TYP_ID1 == tmpTYP_ID1) &&
                                     t.ContractNo == _contractNo
                                 select t).FirstOrDefault();

                    if (vConf != null)
                    {
                        if (vConf.TRA_ID != null)
                            _Trantyp = vConf.TRA_ID;

                        try
                        {
                            cob_TrnType.SelectedValue = _Trantyp;

                        }
                        catch (System.Exception)
                        {
                        }

                    }
                    else
                    {//อะไหล่ใน warranty มีเลขที่สัญญา แต่หาไม่เจอ
                        // _Trantyp = "IN";
                        //20181012 มีนโยบายให้ไปใช้ใน ตราราง sparepart แทน  cob_TrnType.SelectedValue = _Trantyp;
                        DefaultTranTypeByPartMaster(partNo);  //20181012

                    }
                    #endregion

                }
                else
                {
                    //20181012 มีนโยบายให้ไปใช้ใน ตราราง sparepart แทน  cob_TrnType.SelectedValue = _Trantyp;
                    DefaultTranTypeByPartMaster(partNo);  //20181012
                }
                //============================================


                #endregion


                bPriceCharges = false; //20181129

            }  //using (DAL

            return bPriceCharges;//20181129
        }

        //===20190114 chkSetSpecialPrice 
        private void setLifetime(string tmpSN, string partNo)
        {
            var dnow = DateTime.Today.Date;
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext(strConn))
            {
                var ffaWD = dc.FFixedAsset_Station_Details
                   //20190116.Where(t => t.STA_ID == tmpSTAID && t.SerialNumber == tmpSN && t.WarrantDate.Value.Date < dnow)//WarrantDate < dnow , t.WarrantDate.Value.Date < dnow เทียบค่า null ไม่ได้
                   .Where(t => t.STA_ID == tmpSTAID && t.SerialNumber == tmpSN && t.WarrantDate > dnow)
                    //.Where(t => t.SerialNumber == tmpSN && t.WarrantDate > dnow)
                    .FirstOrDefault();
                //20190116 if (ffaWD != null)
                if (ffaWD == null)//ไม่อยู่ในประกัน
                {
                    var ffaWDc = dc.FFixedAsset_Station_Details
                    .Where(t => t.STA_ID == tmpSTAID && t.SerialNumber == tmpSN)
                    .FirstOrDefault();
                    if (ffaWDc != null)
                    {
                        if (ffaWDc.IsLifeTime == true)
                        {
                            var chkSLT = dc.Conf_Part_LifeTimes
                            .Where(t => t.SPA_ID == partNo && t.GOP_ID == tmpGOPId).FirstOrDefault();
                            //var chkSP = (from t in dc.Conf_Part_SpecialPrices
                            //           where t.GOP_ID == tmpGOPId &&
                            //           t.SPA_ID == partNo
                            //           select t).FirstOrDefault();
                            if (chkSLT != null)
                            {

                                txtPriceList.Text = "0";
                                txtDiscQuarter.Text = "0";
                                txtVAT.Text = "0";
                                txtPriceUnit.Text = "0";

                                cob_TrnType.SelectedValue = chkSLT.TRA_ID;

                            }
                        }
                    }
                }
            }



        }

        //===20190114 chkSetSpecialPrice 
        private void setSpecialPrice(string tmpSN, string partNo)
        {
            double _prices = 0;
            double _discount = 0;
            int quantity = 0;

            if (txtPartQT.Text == "0" || txtPartQT.Text == "")
                txtPartQT.Text = "1";

            quantity = int.Parse(txtPartQT.Text);

            txtPartNo.Text = partNo;
            tmpSPA_ID = partNo;
            txtPartQT.Focus();

            var dnow = DateTime.Today.Date;
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext(strConn))
            {
                var ffaW = dc.FFixedAsset_Station_Details
                    //20190116.Where(t => t.STA_ID == tmpSTAID && t.SerialNumber == tmpSN && t.WarrantDate < dnow )
                    //.Where(t => t.SerialNumber == tmpSN && t.WarrantDate > dnow)
                    .Where(t => t.STA_ID == tmpSTAID && t.SerialNumber == tmpSN && t.WarrantDate > dnow)
                    .FirstOrDefault();
                //20190116if (ffaW != null)
                if (ffaW == null)
                {
                    var ffaWc = dc.FFixedAsset_Station_Details
                    .Where(t => t.STA_ID == tmpSTAID && t.SerialNumber == tmpSN)
                    .FirstOrDefault();

                    if (ffaWc != null)
                    {
                        if (ffaWc.IsSpecialPrice == true)
                        {
                            var chkSP = dc.Conf_Part_SpecialPrices
                                .Where(t => t.SPA_ID == partNo && t.GOP_ID == tmpGOPId).FirstOrDefault();
                            //var chkSP = (from t in dc.Conf_Part_SpecialPrices
                            //           where t.GOP_ID == tmpGOPId &&
                            //           t.SPA_ID == partNo
                            //           select t).FirstOrDefault();
                            if (chkSP != null)
                            {
                                txtPriceList.Text = Convert.ToDouble(chkSP.Price).ToString("#,##0.00");//chkSP.Price.ToString("#,##0.00");
                                _prices = Convert.ToDouble(chkSP.Price);

                                txtPriceUnit.Text = (Convert.ToDouble(_prices) * quantity).ToString("#,##0.00");

                            }
                        }
                    }

                }
            }
        }
        //===20190114

        private void check_SpareParts_IsTravel(string partNo, string workTypID)
        {
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext(strConn))
            {
                //0 ตรวจสอบสัญญา
                var dnow = DateTime.Today.Date;

                var con0 = dc.Conf_Contracts
                   .Where(t => t.ContractNo.Contains(_contractNo))
                   .FirstOrDefault();
                if (con0 != null)//มีสัญญา จึงไปเช็ควันที่
                {
                    var con = dc.Conf_Contracts
                        //20180420 .Where(t => t.StartDate.Date <= dnow && t.EndDate >= dnow &&
                        .Where(t => t.StartDate.Date <= dnow && t.EndDate.Date >= dnow &&
                            t.TYP_ID == tmpJobType &&
                            t.ContractNo.Contains(_contractNo))
                        .FirstOrDefault();
                    if (con == null)//สัญญาหมด
                    {
                        //var conShow = dc.Conf_Contracts
                        //.Where(t =>
                        //    t.TYP_ID == tmpJobType &&
                        //    t.ContractNo.Contains(_contractNo))
                        //.FirstOrDefault();
                        //DateTime dtStart = Convert.ToDateTime(conShow.StartDate.ToString("dd/MM/yyyy HH:mm"));
                        //DateTime dtEnd = Convert.ToDateTime(conShow.EndDate.ToString("dd/MM/yyyy HH:mm"));

                        //string vStart = Convert.ToString(dtStart);
                        // string vEnd = Convert.ToString(dtEnd); 
                        if (!string.IsNullOrEmpty(_contractNo))
                            MessageBox.Show("เลขที่สัญญา " + _contractNo + " หมดอายุ" + Environment.NewLine +
                            // " วันที่เริ่มต้นสัญญา " + dtStart + " " + Environment.NewLine +
                            // " วันที่สิ้นสุดสัญญา " + dtEnd + " " + Environment.NewLine +
                            "กรุณาตรวจสอบ...", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        return;
                    }

                }

                //Check ข้อมูลในตาราง SpareParts_IsTravel
                var iTv = (from t in dc.SpareParts_IsTravels
                           where t.GOP_ID == tmpGOPId &&
                           t.SPA_ID == partNo
                           select t).FirstOrDefault();

                if (iTv != null)
                {
                    var st = dc.Stations
                        .Where(t => t.STA_ID == tmpSTAID)
                        .FirstOrDefault();
                    if (st != null)
                    {
                        decimal acDistance = _ActualDistance;// st.ActualDistance.Value;
                        decimal dFrom = _DistanceFromCenter;// st.DistanceFromCenter.Value;
                        decimal perKM = 0;// st.PerKM.Value == null ? 0 : st.PerKM.Value;

                        if (!string.IsNullOrEmpty(_contractNo))
                        {
                            #region Part only  _contractNo != ""

                            var qconf = dc.Conf_Conditions
                            .Where(t =>
                                       t.GOP_ID == tmpGOPId &&
                                       t.TYP_ID == tmpJobType &&
                                       t.TYP_ID1 == tmpTYP_ID1 &&
                                        t.SPA_ID == partNo &&
                                       t.ContractNo.Contains(_contractNo)
                           ).FirstOrDefault();

                            if (qconf != null)
                            {
                                perKM = qconf.PriceList;
                            }
                            else
                            {
                                //var pp = (from t in dc.SparePart_Prices_Lists
                                //          from t1 in dc.SpareParts_IsPrices
                                //          where ((t.SPA_ID == t1.SPA_ID) &&
                                //          t.SPA_ID == partNo && t1.GOP_ID == tmpGOPId)
                                //          select t).FirstOrDefault();

                                //if (pp != null)
                                //    perKM = pp.Prices;
                                //else
                                //{
                                //    var pp1 = dc.SpareParts.Where(t => t.SPA_ID == partNo)
                                //        .FirstOrDefault();
                                //    if (pp1 != null)
                                //        perKM = pp1.PricePerUnit.Value;
                                //}


                                var pp1 = dc.SpareParts.Where(t => t.SPA_ID == partNo)
                                    .FirstOrDefault();
                                if (pp1 != null)
                                    perKM = pp1.PricePerUnit.Value;
                            }

                            #endregion
                        }
                        else
                        {
                            #region Part only  _contractNo == ""

                            var qconf = dc.Conf_Conditions
                            .Where(t =>
                                       t.GOP_ID == tmpGOPId &&
                                       t.TYP_ID == tmpJobType &&
                                       t.TYP_ID1 == tmpTYP_ID1 &&
                                       t.SPA_ID == partNo
                           ).FirstOrDefault();

                            if (qconf != null)
                            {
                                perKM = qconf.PriceList;
                            }
                            else
                            {
                                //var pp = (from t in dc.SparePart_Prices_Lists
                                //          from t1 in dc.SpareParts_IsPrices
                                //          where ((t.SPA_ID == t1.SPA_ID) &&
                                //          t.SPA_ID == partNo && t1.GOP_ID == tmpGOPId)
                                //          select t).FirstOrDefault();

                                //if (pp != null)
                                //    perKM = pp.Prices;
                                //else
                                //{
                                //    var pp1 = dc.SpareParts.Where(t => t.SPA_ID == partNo)
                                //        .FirstOrDefault();
                                //    if (pp1 != null)
                                //        perKM = pp1.PricePerUnit.Value;
                                //}

                                var pp1 = dc.SpareParts.Where(t => t.SPA_ID == partNo)
                                       .FirstOrDefault();
                                if (pp1 != null)
                                    perKM = pp1.PricePerUnit.Value;

                            }

                            #endregion

                        }

                        txtPriceList.Text = perKM.ToString("#,##0.00");//(dFrom * perKM).ToString("#,##0.00");
                        txtStation.Text = txtStation.Text +
                            " [ระยะทางจริง " + acDistance.ToString("#,##0.00") +
                            " จากศูนย์ " + dFrom.ToString("#,##0.00") +
                            " กม.ละ " + perKM.ToString("#,##0.00") + "]";
                        txtPartQT.Text = (acDistance - dFrom).ToString("#,##0.00");
                        txtPriceUnit.Text = ((acDistance - dFrom) * perKM).ToString("#,##0.00");

                        //chIsPrices.Checked = true;
                        chIsPrices.Checked = false;
                        chIsPrices.Enabled = true;

                    }
                    else
                    {
                        txtStation.Text = txtStation.Text;
                        chIsPrices.Checked = false;
                        chIsPrices.Enabled = true;
                    }

                }

            }

        }


        //ตรวจสอบเงื่อนไข Part เก็บเงินสด
        //ตามเงื่อนไขสัญญา Conf_PartCash_Charges
        private bool check_Conf_Conditions(string partNo, string workTypeID)
        {
            bool isContract = false;

            using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext(strConn))
            {
                #region ตรวจสอบวันที่สัญญา

                //0 ตรวจสอบวันที่สัญญา
                var dnow = DateTime.Today.Date;

                if (!string.IsNullOrEmpty(_contractNo) && _contractNo != "null")
                {
                    var con0 = dc.Conf_Contracts
                                .Where(t => t.ContractNo.Contains(_contractNo))
                                .FirstOrDefault();
                    if (con0 != null)//มีสัญญาจึงไปเช็คเงื่อนไข
                    {
                        var con = dc.Conf_Contracts
                            .Where(t => (t.StartDate.Date <= dnow && t.EndDate >= dnow &&
                                t.TYP_ID == tmpJobType &&
                                t.GOP_ID == tmpGOPId &&
                                t.ContractNo.Contains(_contractNo)) //&&  t.IsCheckTranType == true  
                                )
                            .FirstOrDefault();

                        if (con == null)//สัญญาหมด
                        {
                            //isContract = false;
                            return false;
                        }
                        else
                        {
                            isContract = true;
                        }
                    }// if (con0 != null)
                }// if (!string.IsNullOrEmpty(_contractNo) && _contractNo != "null")


                else
                {//ไม่มีเลขที่สัญญา

                    var con0 = dc.Conf_Contracts
                              //20160506.Where(t => t.ContractNo.Contains(_contractNo))
                              .Where(t => t.ContractNo == _contractNo)
                              .FirstOrDefault();
                    if (con0 != null)//มีสัญญาจึงไปเช็คเงื่อนไข (ไม่เข้าอยู่แล้ว)
                    {
                        var con = dc.Conf_Contracts
                               .Where(t => (t.StartDate.Date <= dnow && t.EndDate >= dnow &&
                                   t.TYP_ID == tmpJobType &&
                                   t.GOP_ID == tmpGOPId)
                                   )
                               .FirstOrDefault();
                        if (con == null)//สัญญาหมด
                            return false;
                        else
                            isContract = true;

                    }

                }

                #endregion

                //Check เงื่อนไขสัญญาตามสถานี...
                var qst = (from t in dc.Conf_Contracts
                           from tc in dc.Conf_Conditions
                           from sp in dc.SpareParts
                           where  // t.IsCheckTranType == true && (
                                    t.ContractNo.Contains(_contractNo) &&
                                   (t.ContractNo == tc.ContractNo) &&
                                   (tc.SPA_ID == sp.SPA_ID) &&
                                   (t.GOP_ID == tmpGOPId) &&
                                   (t.STA_ID == tmpSTAID) &&//<==| ตามสถานี
                                                            //(tc.TYP_ID == tmpJobType) &&//ประเภท JOB
                                   (tc.TYP_ID == workTypeID) && //ประเภทงานในเงื่อนไขสัญญา ตรงกับ JOB
                                   ((tc.SPA_ID == partNo)) &&
                                   (tc.IsAutoPart == false)//<==| ดึงเฉพาะ Part ที่ไม่โหลด Auto
                           //)
                           select tc).FirstOrDefault();

                if (qst != null)
                {
                    con_ERP_StationCharge.SelectedIndex = con_ERP_StationCharge.FindString("สถานี");
                    chIsPrices.Checked = true;


                    cob_TrnType.SelectedValue = qst.TRA_ID;
                    isContract = true;
                }
                else
                {
                    if (!string.IsNullOrEmpty(_contractNo) && _contractNo != "null")
                    {
                        #region Check เงื่อนไขสัญญาตามกลุ่มบริษัท...
                        //Check เงื่อนไขสัญญาตามกลุ่มบริษัท...
                        var q = (from t in dc.Conf_Contracts
                                 from tc in dc.Conf_Conditions
                                 from sp in dc.SpareParts
                                 where  //t.IsCheckTranType == true && (
                                        t.ContractNo.Contains(_contractNo) &&
                                        ((t.ContractNo == tc.ContractNo) &&
                                         (tc.SPA_ID == sp.SPA_ID)) &&
                                         (t.GOP_ID == tmpGOPId) &&
                                         //(t.STA_ID == tmpSTAID) &&//<==| ตามสถานี
                                         //(tc.TYP_ID == tmpJobType) && //ประเภท JOB
                                         (tc.TYP_ID1 == workTypeID) && //ประเภทงานในเงื่อนไขสัญญา ตรงกับ JOB
                                         ((tc.SPA_ID == partNo)) &&
                                         (tc.IsAutoPart == false)//<==| ดึงเฉพาะ Part ที่ไม่โหลด Auto
                                 //)
                                 select tc).FirstOrDefault();

                        if (q != null)
                        {
                            if (q.IsPartPriceCharges)
                            {
                                con_ERP_StationCharge.SelectedIndex = con_ERP_StationCharge.FindString("สถานี");
                                chIsPrices.Checked = true;
                            }
                            else
                            {
                                con_ERP_StationCharge.SelectedIndex = con_ERP_StationCharge.FindString("บริษัทน้ำมัน");
                                chIsPrices.Checked = false;
                            }

                            cob_TrnType.SelectedValue = q.TRA_ID;
                            isContract = true;
                        }
                        else
                        {
                            chIsPrices.Checked = false;
                            //20151105 ทดสอบเปลี่ยนค่า isContract = false;
                            isContract = true;
                        }

                        #endregion

                    }
                    else //ไม่มีเลขที่สัญญา
                    {
                        /* 20200420 ยกเลิก  #region Check เงื่อนไขสัญญาตามกลุ่มบริษัท...
                         //Check เงื่อนไขสัญญาตามกลุ่มบริษัท...
                         var q = (from t in dc.Conf_Contracts
                                  from tc in dc.Conf_Conditions
                                  from sp in dc.SpareParts
                                  where  //t.IsCheckTranType == true && (
                                         ((t.ContractNo == tc.ContractNo) &&
                                          (tc.SPA_ID == sp.SPA_ID)) &&
                                          (t.GOP_ID == tmpGOPId) &&
                                          (tc.TYP_ID1 == workTypeID) && //ประเภทงานในเงื่อนไขสัญญา ตรงกับ JOB
                                          ((tc.SPA_ID == partNo)) &&
                                          (tc.IsAutoPart == false)//<==| ดึงเฉพาะ Part ที่ไม่โหลด Auto
                                  //)
                                  select tc).FirstOrDefault();

                         
                         if (q != null)
                         {
                             if (q.IsPartPriceCharges)
                             {
                                 con_ERP_StationCharge.SelectedIndex = con_ERP_StationCharge.FindString("สถานี");
                                 chIsPrices.Checked = true;
                             }
                             else
                             {
                                 con_ERP_StationCharge.SelectedIndex = con_ERP_StationCharge.FindString("บริษัทน้ำมัน");
                                 chIsPrices.Checked = false;
                             }

                             cob_TrnType.SelectedValue = q.TRA_ID;
                             isContract = true ;
                         }
                         else
                         {
                             chIsPrices.Checked = false;
                             //20151105 ทดสอบเปลี่ยนค่า isContract = false;
                             isContract = true;
                         }

                         #endregion
                         */
                        isContract = true;//20200420
                        chIsPrices.Checked = false; //20200420
                    }
                }

                #region พิมพ์ราคาบน Service Report
                JOB_IS_PrintPrice(dc);

                #endregion
            }

            return isContract;
        }

        private void JOB_IS_PrintPrice(DAL.SMSManage.SMSManageDataContext dc)
        {
            var isP = dc.Conf_JOB_IS_PrintPrices
                .Where(t => t.GOP_ID == tmpGOPId && t.TYP_ID == _job.TYP_ID)
                .FirstOrDefault();

            if (isP != null)
            {
                if (isP.SVIsPrintPrice == true)
                {
                    chIsPrices.Checked = true;
                    chIsPrices.Enabled = false;
                    _IsPrintPrice = true;
                }
                else
                {
                    chIsPrices.Checked = false;
                    //chIsPrices.Enabled = false;
                }
            }
            else
            {
                chIsPrices.Checked = false;
                //chIsPrices.Enabled = false;
            }
        }


        private void getPricesList(string partNo, string partName)
        {
            int roundup = 0;

            double _prices = 0;
            double _discount = 0;
            int quantity = 0;

            if (txtPartQT.Text == "0" || txtPartQT.Text == "")
                txtPartQT.Text = "1";

            quantity = int.Parse(txtPartQT.Text);

            txtPartNo.Text = partNo;
            tmpSPA_ID = partNo;
            txtPartName.Text = partName;
            txtPartQT.Focus();

            using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext(strConn))
            {
                string stGroupName = string.Empty;
                //ค้นหา Part ตามโปรเจ็กต์ ID

                if (!string.IsNullOrEmpty(_contractNo) && _contractNo != "null")
                {
                    #region Part ไม่อ้างอิงโปรเจ็กต์ มีเลขที่สัญญา

                    //---20180420-----------------
                    var qMA = (from m in dc.Conf_Contracts
                               where m.ContractNo == _contractNo &&
                                   m.PriceListNo != null
                               select m).FirstOrDefault();

                    if (qMA != null)
                    {
                        var conMA = (from ma in dc.Conf_Conditions_PriceLists
                                     where ma.PriceListNo == qMA.PriceListNo &&
                                 ma.TYP_ID == tmpJobType &&
                                 ma.TYP_ID1 == tmpTYP_ID1 &&
                                 ma.SPA_ID == partNo
                                     group ma by ma.SPA_ID
                                         into gMA
                                     select new { PriceList = gMA.Max(ma => ma.PriceList) }).FirstOrDefault();
                        if (conMA != null && conMA.PriceList > 0)
                        {
                            #region Check Price Conf_Conditions

                            txtPriceList.Text = conMA.PriceList.ToString("#,##0.00");
                            _prices = Convert.ToDouble(conMA.PriceList);

                            txtPriceUnit.Text = (Convert.ToDouble(_prices) * quantity).ToString("#,##0.00");

                            #endregion
                        }
                        else
                        { //ใช้ราคาใน sparePart
                            var priSP = (from sp in dc.SpareParts
                                         where sp.SPA_ID == partNo
                                         select new
                                         {
                                             sp.SPA_ID,
                                             sp.PricePerUnit
                                         }).FirstOrDefault();
                            if (priSP != null)
                            {

                                txtPriceList.Text = Convert.ToDouble(priSP.PricePerUnit).ToString("#,##0.00");
                                txtDiscQuarter.Text = "0";
                                txtPriceUnit.Text = (Convert.ToDouble(priSP.PricePerUnit) * quantity).ToString("#,##0.00");

                            }

                        }
                    }
                    else
                    {//ไม่มีสัญญา MA
                        //-------------------------------
                        //ดึงราคาในสัญญ ตาม ....SPA_ID,TYP_ID,TYP_ID1
                        var conP = (from t in dc.Conf_Conditions
                                    from sp in dc.SpareParts
                                    where (t.SPA_ID == sp.SPA_ID &&
                                        t.SPA_ID == partNo &&
                                        t.GOP_ID == tmpGOPId &&
                                        t.TYP_ID == tmpJobType &&
                                        t.TYP_ID1 == tmpTYP_ID1) &&
                                        t.ContractNo == _contractNo
                                    group t by t.SPA_ID
                                        into g
                                    select new { PriceList = g.Max(t => t.PriceList) }).FirstOrDefault();

                        if (conP != null && conP.PriceList > 0)
                        {
                            #region Check Price Conf_Conditions

                            txtPriceList.Text = conP.PriceList.ToString("#,##0.00");
                            _prices = Convert.ToDouble(conP.PriceList);

                            txtPriceUnit.Text = (Convert.ToDouble(_prices) * quantity).ToString("#,##0.00");

                            #endregion
                        }
                        else
                        {

                            #region OLD

                            ////////#region Check Price List

                            ////////var q = (from t in dc.SpareParts where t.SPA_ID == partNo select t).FirstOrDefault();

                            ////////var qIsPrice = (from t in dc.SpareParts_IsPrices where t.SPA_ID == partNo && t.GOP_ID == tmpGOPId select t).FirstOrDefault();
                            ////////var stg = (from t in dc.Station_Groups where t.GOP_ID == tmpGOPId select t).FirstOrDefault();
                            ////////stGroupName = stg == null ? string.Empty : stg.GroupStation;

                            ////////DateTime openjob = Convert.ToDateTime(Convert.ToDateTime(txtOpenJOB.Text).ToShortDateString());

                            ////////var pri = (from sp in dc.SpareParts
                            ////////             where sp.SPA_ID == partNo
                            ////////           select new
                            ////////           {
                            ////////             sp.SPA_ID,
                            ////////             sp.PricePerUnit
                            ////////           }).FirstOrDefault();
                            ////////  if (pri != null)
                            ////////  {
                            ////////       if (qIsPrice != null)
                            ////////             {
                            ////////              #region มี Price List

                            ////////              txtPriceList.Text = Convert.ToDouble(pri.PricePerUnit).ToString("#,##0.00");//pri.Prices.ToString("#,##0.00");

                            ////////              txtDiscQuarter.Text = "0";
                            ////////              txtPriceUnit.Text = (Convert.ToDouble(_prices) * quantity).ToString("#,##0.00");

                            ////////              if (qIsPrice.IsPrices != null)
                            ////////                    chIsPrices.Checked = qIsPrice.IsPrices.Value;

                            ////////                #endregion

                            ////////              }
                            ////////              else
                            ////////              {
                            ////////                  #region ไม่มี PriceList
                            ////////                  MessageBox.Show("Part: [" + partNo + " " + partName + "] ไม่มี Price list" + Environment.NewLine +
                            ////////                                             "กล่มบริษัท: " + stGroupName + Environment.NewLine +
                            ////////                                             "ใช้ราคาจากข้อมูลหลัก (Master data) ของ part" + Environment.NewLine +
                            ////////                                             "โดยหมายเหตุ ระบุเป็น \"#Part นอก Prices\" ", "Part ไม่มี Price list", MessageBoxButtons.OK);

                            ////////                  _prices = Convert.ToDouble(q.PricePerUnit.Value == 0 ? 0 : q.PricePerUnit.Value);
                            ////////                  txtDiscQuarter.Text = "0";


                            ////////                  //txtPriceList.Text = _prices.ToString("#,##0.00");
                            ////////                  txtPartComment.Text = "#Part นอก Prices";

                            ////////                  txtPriceList.Text = "0";
                            ////////                  txtDiscQuarter.Text = "0";
                            ////////                  txtVAT.Text = "0";
                            ////////                  txtPriceUnit.Text = "0";
                            ////////                  #endregion

                            ////////              }



                            ////////      /* 
                            ////////       // ยกเลิก 20150429  var pri = (from pl in dc.SparePart_Prices_Lists
                            ////////                    from isp in dc.SpareParts_IsPrices
                            ////////                    from pq in dc.SparePart_Quarters
                            ////////                    from tg in dc.Station_Groups
                            ////////                    where (pl.PRI_ID == pq.PRI_ID) &&
                            ////////                         (pl.SPA_ID == isp.SPA_ID && tg.GOP_ID == isp.GOP_ID) &&
                            ////////                         (pq.GOP_ID == tg.GOP_ID) &&
                            ////////                         (pl.SPA_ID == partNo) &&
                            ////////                          tg.GOP_ID == tmpGOPId &&
                            ////////                          pq.StartDate <= openjob && pq.EndDate >= openjob
                            ////////                    select new
                            ////////                    {
                            ////////                        pl.SPA_ID,
                            ////////                        pq.localDiscount,
                            ////////                        pq.globalDiscount,
                            ////////                        pq.decRoundUp,
                            ////////                        pl.Prices,
                            ////////                        pq.Quarter
                            ////////                    }).FirstOrDefault();




                            ////////         if (pri != null)
                            ////////         {
                            ////////             #region มี Price List

                            ////////             roundup = Convert.ToInt32(pri.decRoundUp ?? 0);//ปัดเศษ
                            ////////             tmpRounup = roundup;

                            ////////             txtPriceList.Text = pri.Prices.ToString("#,##0.00");
                            ////////             _prices = Convert.ToDouble(pri.Prices);//Price list

                            ////////             if (qIsPrice != null)
                            ////////             {
                            ////////                 #region มี Price List

                            ////////                 if (qIsPrice.IsGlobalDiscount.Value != '2')
                            ////////                 {
                            ////////                     if (qIsPrice.IsGlobalDiscount.Value == '1')
                            ////////                         _discount = Convert.ToDouble(pri.globalDiscount);//ใช้ส่วนลดต่างประเทศ
                            ////////                     else
                            ////////                         _discount = Convert.ToDouble(pri.localDiscount);//ใช้ส่วนลดในประเทศ

                            ////////                     txtDiscQuarter.Text = _discount.ToString(); //ส่วนลด

                            ////////                     if (_discount <= 0)
                            ////////                         txtPriceUnit.Text = Math.Round(pri.Prices * quantity, tmpRounup).ToString("#,##0.00");
                            ////////                     else
                            ////////                         txtPriceUnit.Text = Math.Round((_prices - ((_discount / 100) * _prices)) * quantity, tmpRounup).ToString("#,##0.00");

                            ////////                 }
                            ////////                 else
                            ////////                 {
                            ////////                     txtDiscQuarter.Text = "0";
                            ////////                     if (tmpRounup > 0)
                            ////////                         txtPriceUnit.Text = Math.Round(Convert.ToDouble(_prices) * quantity, tmpRounup).ToString("#,##0.00");
                            ////////                     else
                            ////////                         txtPriceUnit.Text = (Convert.ToDouble(_prices) * quantity).ToString("#,##0.00");

                            ////////                 }// if (q.IsDiscount == true)

                            ////////                 if (qIsPrice.IsPrices != null)
                            ////////                     chIsPrices.Checked = qIsPrice.IsPrices.Value;

                            ////////                 #endregion

                            ////////             }
                            ////////             else
                            ////////             {
                            ////////                 #region ไม่มี PriceList
                            ////////                 MessageBox.Show("Part: [" + partNo + " " + partName + "] ไม่มี Price list" + Environment.NewLine +
                            ////////                                            "กล่มบริษัท: " + stGroupName + Environment.NewLine +
                            ////////                                            "ใช้ราคาจากข้อมูลหลัก (Master data) ของ part" + Environment.NewLine +
                            ////////                                            "โดยหมายเหตุ ระบุเป็น \"#Part นอก Prices\" ", "Part ไม่มี Price list", MessageBoxButtons.OK);

                            ////////                 _prices = Convert.ToDouble(q.PricePerUnit.Value == 0 ? 0 : q.PricePerUnit.Value);
                            ////////                 txtDiscQuarter.Text = "0";


                            ////////                 //txtPriceList.Text = _prices.ToString("#,##0.00");
                            ////////                 txtPartComment.Text = "#Part นอก Prices";

                            ////////                 txtPriceList.Text = "0";
                            ////////                 txtDiscQuarter.Text = "0";
                            ////////                 txtVAT.Text = "0";
                            ////////                 txtPriceUnit.Text = "0";
                            ////////                 #endregion

                            ////////             }

                            ////////             #endregion

                            ////////         } //end pri 
                            ////////                */
                            ////////  }//end pri 
                            ////////  else
                            ////////  {
                            ////////      #region ไม่มี PriceList
                            ////////      txtPriceList.Text = "0";
                            ////////      txtDiscQuarter.Text = "0";

                            ////////      _prices = Convert.ToDouble(q.PricePerUnit.Value == 0 ? 0 : q.PricePerUnit.Value);

                            ////////      //txtPriceList.Text = _prices.ToString("#,##0.00");

                            ////////      MessageBox.Show("Part: [" + partNo + " " + partName + "] ไม่มี Price list" + Environment.NewLine +
                            ////////                                "กล่มบริษัท: " + stGroupName + Environment.NewLine +
                            ////////                                "ใช้ราคาจากข้อมูลหลัก (Master data) ของ part" + Environment.NewLine +
                            ////////                                "โดยหมายเหตุ ระบุเป็น \"#Part นอก Prices\" ", "Part ไม่มี Price list", MessageBoxButtons.OK);

                            ////////      txtPartComment.Text = "#Part นอก Prices";

                            ////////      txtPriceList.Text = "0";
                            ////////      txtDiscQuarter.Text = "0";
                            ////////      txtVAT.Text = "0";
                            ////////      txtPriceUnit.Text = "0";
                            ////////      txtPartComment.Text = "#Part นอก Prices";

                            ////////      #endregion

                            ////////  }

                            ////////#endregion

                            #endregion

                            //ดึงราคาในสัญญา ตาม ....SPA_ID
                            var conP1 = (from t in dc.Conf_Conditions
                                         from sp in dc.SpareParts
                                         where (t.SPA_ID == sp.SPA_ID &&
                                             t.SPA_ID == partNo &&
                                             t.GOP_ID == tmpGOPId
                                             //t.TYP_ID == tmpJobType &&
                                             //t.TYP_ID1 == tmpTYP_ID1
                                             ) &&
                                             t.ContractNo == _contractNo
                                         group t by t.SPA_ID
                                             into g
                                         select new { PriceList = g.Max(t => t.PriceList) }).FirstOrDefault();

                            if (conP1 != null && conP1.PriceList > 0)
                            {
                                #region Check Price Conf_Conditions

                                txtPriceList.Text = conP1.PriceList.ToString("#,##0.00");
                                _prices = Convert.ToDouble(conP1.PriceList);

                                txtPriceUnit.Text = (Convert.ToDouble(_prices) * quantity).ToString("#,##0.00");

                                #endregion
                            }
                            else
                            { //ใช้ราคาใน sparePart
                                var priSP = (from sp in dc.SpareParts
                                             where sp.SPA_ID == partNo
                                             select new
                                             {
                                                 sp.SPA_ID,
                                                 sp.PricePerUnit
                                             }).FirstOrDefault();
                                if (priSP != null)
                                {

                                    txtPriceList.Text = Convert.ToDouble(priSP.PricePerUnit).ToString("#,##0.00");
                                    txtDiscQuarter.Text = "0";
                                    txtPriceUnit.Text = (Convert.ToDouble(priSP.PricePerUnit) * quantity).ToString("#,##0.00");

                                }

                            }

                            #endregion
                        }//ไม่มีในสัญญา ,TYP_ID,TYP_ID1
                    }//--20180420 MA
                }//เลขที่สัญญา(ไม่)เป็น nulll
                else
                {  // if (!string.IsNullOrEmpty(_contractNo) && _contractNo != "null") เลขที่สัญญาเป็น nulll

                    #region Old

                    //////////  #region Part ไม่อ้างอิงโปรเจ็กต์ ไม่มี _contractNo

                    //////////  //20150504var conP = (from t in dc.Conf_Conditions
                    //////////  //            from sp in dc.SpareParts
                    //////////  //            where (t.SPA_ID == sp.SPA_ID &&
                    //////////  //                t.SPA_ID == partNo &&
                    //////////  //                t.GOP_ID == tmpGOPId &&
                    //////////  //                t.TYP_ID == tmpJobType &&
                    //////////  //                t.TYP_ID1 == tmpTYP_ID1)
                    //////////  //            select t).FirstOrDefault();
                    ////////// //=========================

                    //////////  //ใช้ราคามากสุดตามเงื่อนไข 20150504
                    ///////////*  var conP = (from t in dc.Conf_Conditions
                    //////////              from sp in dc.SpareParts
                    //////////              where (t.SPA_ID == sp.SPA_ID &&
                    //////////                  t.SPA_ID == partNo &&
                    //////////                  t.GOP_ID == tmpGOPId &&
                    //////////                  t.TYP_ID == tmpJobType &&
                    //////////                  t.TYP_ID1 == tmpTYP_ID1)
                    //////////              select
                    //////////                  t.PriceList
                    //////////              //t
                    //////////              ).FirstOrDefault();
                    //////////      //.Max(t => t.PriceList);
                    //////////             //.Max();*/

                    //////////  //var conP = (from t in dc.Conf_Conditions select t).Max();
                    //////////  //var conP = (from t in dc.Conf_Conditions
                    //////////  //            from sp in dc.SpareParts
                    //////////  //            where (t.SPA_ID == sp.SPA_ID &&
                    //////////  //                t.SPA_ID == partNo &&  
                    //////////  //                t.GOP_ID == tmpGOPId &&
                    //////////  //                t.TYP_ID == tmpJobType &&
                    //////////  //                t.TYP_ID1 == tmpTYP_ID1) 
                    //////////  //            //select t.PriceList
                    //////////  //            select new
                    //////////  //            {
                    //////////  //                   t.PriceList,
                    //////////  //                   t.SPA_ID,
                    //////////  //                   t.GOP_ID,
                    //////////  //                   t.TYP_ID,
                    //////////  //                   t.TYP_ID1
                    //////////  //               }

                    //////////  //            ).Max();


                    //////////  //var conP1 = from t in dc.Conf_Conditions
                    //////////  //            //join sp in dc.SpareParts on t.SPA_ID equals sp.SPA_ID
                    //////////  //            where //t.SPA_ID == sp.SPA_ID &&
                    //////////  //                t.SPA_ID == partNo &&
                    //////////  //                t.GOP_ID == tmpGOPId &&
                    //////////  //                t.TYP_ID == tmpJobType &&
                    //////////  //                t.TYP_ID1 == tmpTYP_ID1
                    //////////  //            select
                    //////////  //                t.PriceList;

                    //////////  //var conP = (from t in dc.Conf_Conditions
                    //////////  //            from sp in dc.SpareParts
                    //////////  //            where (t.SPA_ID == sp.SPA_ID &&
                    //////////  //                t.SPA_ID == partNo &&
                    //////////  //                t.GOP_ID == tmpGOPId &&
                    //////////  //                t.TYP_ID == tmpJobType &&
                    //////////  //                t.TYP_ID1 == tmpTYP_ID1)
                    //////////  //            select
                    //////////  //                t.PriceList
                    //////////  //    //t
                    //////////  //            )//.FirstOrDefault();
                    //////////  ////.Max(t => t.PriceList);
                    //////////  //.Max();



                    //////////  var conP = (from t in dc.Conf_Conditions
                    //////////              from sp in dc.SpareParts
                    //////////              where t.SPA_ID == sp.SPA_ID &&
                    //////////             t.SPA_ID == partNo &&
                    //////////             t.GOP_ID == tmpGOPId &&
                    //////////             t.TYP_ID == tmpJobType &&
                    //////////             t.TYP_ID1 == tmpTYP_ID1
                    //////////              group t by t.SPA_ID
                    //////////              into g
                    //////////               select new { PriceList = g.Max(t => t.PriceList) }).FirstOrDefault();


                    //////////  if (conP != null && conP.PriceList > 0)
                    //////////  {
                    //////////      #region Check Price Conf_Conditions

                    //////////      txtPriceList.Text = conP.PriceList.ToString("#,##0.00");
                    //////////      _prices = Convert.ToDouble(conP.PriceList);

                    //////////      txtPriceUnit.Text = (Convert.ToDouble(_prices) * quantity).ToString("#,##0.00");

                    //////////      #endregion
                    //////////  }
                    //////////  else
                    //////////  {
                    //////////      #region Check Price List

                    //////////      var q = (from t in dc.SpareParts where t.SPA_ID == partNo select t).FirstOrDefault();

                    //////////      //อะไหล่เก็บเงินหน้างาน
                    //////////      var qIsPrice = (from t in dc.SpareParts_IsPrices where t.SPA_ID == partNo && t.GOP_ID == tmpGOPId select t).FirstOrDefault();
                    //////////      var stg = (from t in dc.Station_Groups where t.GOP_ID == tmpGOPId select t).FirstOrDefault();
                    //////////      stGroupName = stg == null ? string.Empty : stg.GroupStation;

                    //////////      DateTime openjob = Convert.ToDateTime(Convert.ToDateTime(txtOpenJOB.Text).ToShortDateString());


                    //////////      var pri = (from sp in dc.SpareParts
                    //////////                 where sp.SPA_ID == partNo
                    //////////                 select new
                    //////////                 {
                    //////////                     sp.SPA_ID,
                    //////////                     sp.PricePerUnit
                    //////////                 }).FirstOrDefault();
                    //////////      if (pri != null)
                    //////////      {
                    //////////          if (qIsPrice != null)
                    //////////          {
                    //////////              #region มี Price List

                    //////////              txtPriceList.Text = Convert.ToDouble(pri.PricePerUnit).ToString("#,##0.00");//pri.Prices.ToString("#,##0.00");

                    //////////              txtDiscQuarter.Text = "0";
                    //////////              txtPriceUnit.Text = (Convert.ToDouble(_prices) * quantity).ToString("#,##0.00");

                    //////////              if (qIsPrice.IsPrices != null)
                    //////////                  chIsPrices.Checked = qIsPrice.IsPrices.Value;

                    //////////              #endregion

                    //////////          }
                    //////////          else
                    //////////          {
                    //////////              #region ไม่มี PriceList
                    //////////              MessageBox.Show("Part: [" + partNo + " " + partName + "] ไม่มี Price list" + Environment.NewLine +
                    //////////                                         "กล่มบริษัท: " + stGroupName + Environment.NewLine +
                    //////////                                         "ใช้ราคาจากข้อมูลหลัก (Master data) ของ part" + Environment.NewLine +
                    //////////                                         "โดยหมายเหตุ ระบุเป็น \"#Part นอก Prices\" ", "Part ไม่มี Price list", MessageBoxButtons.OK);

                    //////////              _prices = Convert.ToDouble(q.PricePerUnit.Value == 0 ? 0 : q.PricePerUnit.Value);
                    //////////              txtDiscQuarter.Text = "0";


                    //////////              //txtPriceList.Text = _prices.ToString("#,##0.00");
                    //////////              txtPartComment.Text = "#Part นอก Prices";

                    //////////              txtPriceList.Text = "0";
                    //////////              txtDiscQuarter.Text = "0";
                    //////////              txtVAT.Text = "0";
                    //////////              txtPriceUnit.Text = "0";
                    //////////              #endregion

                    //////////          }


                    //////////     /*

                    //////////     //201500429  ยกเลิก
                    //////////      var pri = (from pl in dc.SparePart_Prices_Lists
                    //////////                 from isp in dc.SpareParts_IsPrices
                    //////////                 from pq in dc.SparePart_Quarters
                    //////////                 from tg in dc.Station_Groups
                    //////////                 where (pl.PRI_ID == pq.PRI_ID) &&
                    //////////                      (pl.SPA_ID == isp.SPA_ID && tg.GOP_ID == isp.GOP_ID) &&
                    //////////                      (pq.GOP_ID == tg.GOP_ID) &&
                    //////////                      (pl.SPA_ID == partNo) &&
                    //////////                       tg.GOP_ID == tmpGOPId &&
                    //////////                       pq.StartDate <= openjob && pq.EndDate >= openjob
                    //////////                 select new
                    //////////                 {
                    //////////                     pl.SPA_ID,
                    //////////                     pq.localDiscount,
                    //////////                     pq.globalDiscount,
                    //////////                     pq.decRoundUp,
                    //////////                     pl.Prices,
                    //////////                     pq.Quarter
                    //////////                 }).FirstOrDefault();




                    //////////      if (pri != null)
                    //////////      {
                    //////////          roundup = Convert.ToInt32(pri.decRoundUp ?? 0);//ปัดเศษ
                    //////////          tmpRounup = roundup;

                    //////////          txtPriceList.Text = pri.Prices.ToString("#,##0.00");
                    //////////          _prices = Convert.ToDouble(pri.Prices);//Price list

                    //////////          if (qIsPrice != null)
                    //////////          {
                    //////////              if (qIsPrice.IsGlobalDiscount.Value != '2')
                    //////////              {
                    //////////                  if (qIsPrice.IsGlobalDiscount.Value == '1')
                    //////////                      _discount = Convert.ToDouble(pri.globalDiscount);//ใช้ส่วนลดต่างประเทศ
                    //////////                  else
                    //////////                      _discount = Convert.ToDouble(pri.localDiscount);//ใช้ส่วนลดในประเทศ

                    //////////                  txtDiscQuarter.Text = _discount.ToString(); //ส่วนลด

                    //////////                  if (_discount <= 0)
                    //////////                      txtPriceUnit.Text = Math.Round(pri.Prices * quantity, tmpRounup).ToString("#,##0.00");
                    //////////                  else
                    //////////                      txtPriceUnit.Text = Math.Round((_prices - ((_discount / 100) * _prices)) * quantity, tmpRounup).ToString("#,##0.00");

                    //////////              }
                    //////////              else
                    //////////              {
                    //////////                  txtDiscQuarter.Text = "0";
                    //////////                  if (tmpRounup > 0)
                    //////////                      txtPriceUnit.Text = Math.Round(Convert.ToDouble(_prices) * quantity, tmpRounup).ToString("#,##0.00");
                    //////////                  else
                    //////////                      txtPriceUnit.Text = (Convert.ToDouble(_prices) * quantity).ToString("#,##0.00");

                    //////////              }// if (q.IsDiscount == true)

                    //////////          }
                    //////////          else
                    //////////          {
                    //////////              MessageBox.Show("Part: [" + partNo + " " + partName + "] ไม่มี Price list" + Environment.NewLine +
                    //////////                                         "กล่มบริษัท: " + stGroupName + Environment.NewLine +
                    //////////                                         "ใช้ราคาจากข้อมูลหลัก (Master data) ของ part" + Environment.NewLine +
                    //////////                                         "โดยหมายเหตุ ระบุเป็น \"#Part นอก Prices\" ", "Part ไม่มี Price list", MessageBoxButtons.OK);

                    //////////              _prices = Convert.ToDouble(q.PricePerUnit.Value == 0 ? 0 : q.PricePerUnit.Value);
                    //////////              txtDiscQuarter.Text = "0";


                    //////////              //txtPriceList.Text = _prices.ToString("#,##0.00");
                    //////////              txtPartComment.Text = "#Part นอก Prices";

                    //////////              txtPriceList.Text = "0";
                    //////////              txtDiscQuarter.Text = "0";
                    //////////              txtVAT.Text = "0";
                    //////////              txtPriceUnit.Text = "0";

                    //////////          }
                    //////////      //} //end pri
                    //////////          */
                    //////////  }//end pri
                    //////////      else
                    //////////      {
                    //////////          txtPriceList.Text = "0";
                    //////////          txtDiscQuarter.Text = "0";

                    //////////          _prices = Convert.ToDouble(q.PricePerUnit.Value == 0 ? 0 : q.PricePerUnit.Value);

                    //////////          MessageBox.Show("Part: [" + partNo + " " + partName + "] ไม่มี Price list" + Environment.NewLine +
                    //////////             "กล่มบริษัท: " + stGroupName + Environment.NewLine +
                    //////////             "ใช้ราคาจากข้อมูลหลัก (Master data) ของ part" + Environment.NewLine +
                    //////////             "โดยหมายเหตุ ระบุเป็น \"#Part นอก Prices\" ", "Part ไม่มี Price list", MessageBoxButtons.OK);

                    //////////          txtPartComment.Text = "#Part นอก Prices";

                    //////////          txtPriceList.Text = "0";
                    //////////          txtDiscQuarter.Text = "0";
                    //////////          txtVAT.Text = "0";
                    //////////          txtPriceUnit.Text = "0";
                    //////////          txtPartComment.Text = "#Part นอก Prices";

                    //////////      }

                    //////////      #endregion
                    //////////  }

                    //////////  #endregion

                    #endregion

                    //ดึงราคากลาง ในตาราง SpareParts
                    var priSP = (from sp in dc.SpareParts
                                 where sp.SPA_ID == partNo
                                 select new
                                 {
                                     sp.SPA_ID,
                                     sp.PricePerUnit
                                 }).FirstOrDefault();
                    if (priSP != null)
                    {

                        txtPriceList.Text = Convert.ToDouble(priSP.PricePerUnit).ToString("#,##0.00");
                        txtDiscQuarter.Text = "0";
                        txtPriceUnit.Text = (Convert.ToDouble(priSP.PricePerUnit) * quantity).ToString("#,##0.00");

                    }

                }


            }//using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext(ConnectionManager.GetConnectionString("0")))

        }//fn


        private void txtPartName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                try
                {
                    DateTime openjob = Convert.ToDateTime(Convert.ToDateTime(txtOpenJOB.Text).ToShortDateString());
                    Finding.FindSparePart f = new SMSMINI.Transaction.Finding.FindSparePart();

                    //f.TmpParameter = txtPartName.Text.Trim();

                    //f.TmpDopenJOB = openjob;
                    //f.TmpGopID = tmpGOPId;
                    //f.TmpGopName = tmpGopName;

                    f.TmpParameter = txtPartNo.Text.Trim();

                    f.TmpDopenJOB = openjob;
                    f.TmpGopID = tmpGOPId;
                    f.TmpGopName = tmpGopName;

                    f.tmpJobType = tmpJobType;
                    f.tmpTYP_ID1 = tmpTYP_ID1;

                    f.tmpContractNo = _contractNo;


                    if (txtProjectNO.Text != "")
                        f.TmpProjectNo = txtProjectNO.Text.Trim();

                    string partNo = "";
                    string TmpPartName = "";

                    if (f.ShowDialog() == DialogResult.OK)
                    {


                        partNo = f.TmpPartNo;
                        tmpSPA_ID = f.TmpPartNo;
                        TmpPartName = f.TmpPartName;

                        if (partNo != "")
                        {
                            setPartDetail(partNo, TmpPartName);

                        }
                    }



                }
                catch (System.Exception ex)
                {
                    MessageBox.Show("ข้อผิดพลาด " + ex.Message, "เกิดข้อผิดพลาด");
                }

                txtPartQT.Focus();


            }

        }


        private void dataGridView_Part_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {

                //====== 20201002 =============================
                if (e.ColumnIndex != -1)
                {
                    if (editMode == "job_edit")
                    {
                        using (DAL.SMSManage.SMSManageDataContext dc = new DAL.SMSManage.SMSManageDataContext(strConn))
                        {
                            /* //20210323 ก่อนแก้ไขเงื่อนไขเรื่อง เก็บเงินสด
                            DateTime _vpEnddate = DateTime.Parse(_pEnddate);
                            var qJDEnd = (from t in dc.JOB_Detail_Spares
                                          where t.JOB_ID.Trim() == txtJobID.Text.Trim()
                                          && t.Enddate.Value.Date == _vpEnddate.Date
                                          && t.Enddate.Value.Hour == _vpEnddate.Hour
                                          && t.Enddate.Value.Minute == _vpEnddate.Minute
                                          && t.Enddate.Value.Second == _vpEnddate.Second
                                          select t).OrderByDescending(c => c.Enddate).FirstOrDefault();

                            var qJIDate = (from t in dc.JOB_Invoices
                                           where t.JOB_ID == txtJobID.Text.Trim()
                                           select t).OrderByDescending(c => c.Invoice_Date).FirstOrDefault();

                            if (qJDEnd != null && qJIDate != null)
                            {
                                if (qJDEnd.Enddate.Value.Date <= qJIDate.Invoice_Date.Date)
                                {
                                    return;
                                }
                            }
                             */
                            //20210323 แก้ไขเงื่อนไขเรื่อง เก็บเงินสด
                            DateTime _vpEnddate = DateTime.Parse(_pEnddate);

                            var qIsPrice = (from t in dc.JOB_Detail_Spares
                                            where t.JOB_ID.Trim() == txtJobID.Text.Trim()
                                            && t.Enddate.Value.Date == _vpEnddate.Date
                                            && t.Enddate.Value.Hour == _vpEnddate.Hour
                                            && t.Enddate.Value.Minute == _vpEnddate.Minute
                                            && t.Enddate.Value.Second == _vpEnddate.Second
                                            && t.IsPrices == true //20210322 เก็บเงินสด
                                            select t).OrderByDescending(c => c.Enddate).FirstOrDefault();
                            if (qIsPrice != null)
                            {
                                // disableControlInv(false);
                            }
                            else
                            {
                                var qJDEnd = (from t in dc.JOB_Detail_Spares
                                              where t.JOB_ID.Trim() == txtJobID.Text.Trim()
                                              && t.Enddate.Value.Date == _vpEnddate.Date
                                              && t.Enddate.Value.Hour == _vpEnddate.Hour
                                              && t.Enddate.Value.Minute == _vpEnddate.Minute
                                              && t.Enddate.Value.Second == _vpEnddate.Second
                                              select t).OrderByDescending(c => c.Enddate).FirstOrDefault();

                                var qJIDate = (from t in dc.JOB_Invoices
                                               where t.JOB_ID == txtJobID.Text.Trim()
                                               select t).OrderByDescending(c => c.Invoice_Date).FirstOrDefault();

                                if (qJDEnd != null && qJIDate != null)
                                {
                                    //เงื่อนไขนี้เช็คตอน isprice = 0 ทั้งหมด
                                    if (qJDEnd.Enddate.Value.Date <= qJIDate.Invoice_Date.Date)
                                    {
                                        // disableControlInv(true);
                                        return;
                                    }
                                }
                            }
                            //==
                        }//dal
                         //=20210921==จ่ายคอมแล้วห้ามยกเลิก====
                        using (System.Data.SqlClient.SqlConnection Conn = new System.Data.SqlClient.SqlConnection(strConn))
                        {
                            if (Conn.State == ConnectionState.Closed)
                                Conn.Open();
                            //============================
                            System.Data.SqlClient.SqlDataReader dr = null;

                            string sqlC = "";
                            sqlC = " Declare @IsCommission INT " +
                                  " Exec @IsCommission = JobIsCommission '" + txtJobID.Text.Trim() + "'" +
                                  " Select @IsCommission ";


                            using (dr = new System.Data.SqlClient.SqlCommand(sqlC, Conn).ExecuteReader())
                            {
                                dr.Read();
                                if (dr.HasRows)
                                {

                                    //disableControlInv(true);                                  
                                    return;

                                }

                            }
                        }
                        //================================
                    }

                }

                //============================================

                int xCol = dataGridView_Part.ColumnCount - 1;
                var smg = new string[] { "09", "12", "13" }.Contains(UserInfo.UserLevID);

                switch (e.ColumnIndex)
                {
                    case 0://ยกเลิกข้อมูล
                        #region ยกเลิกข้อมูล

                        try
                        {
                            if (dataGridView_Part["IsDownload", e.RowIndex].Value.ToString() == "Y" && !smg)// ถ้าเป็น SMG ไม่ต้องเช็ค IsDownload
                            {
                                #region IsDownload = Y
                                using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext(strConn))
                                {
                                    string _spaID = dataGridView_Part["Part no.", e.RowIndex].Value.ToString();
                                    int _spaNo = Convert.ToInt32(dataGridView_Part["NO.", e.RowIndex].Value.ToString());

                                    var q = dc.Conf_Conditions
                                        .Where(t => t.ERP_SPA_ID == _spaID && t.GOP_ID == tmpGOPId && t.ContractNo == _contractNo)
                                        .FirstOrDefault();

                                    if (q != null)
                                    {
                                        MessageBox.Show("Part Auto Load to ERP" + Environment.NewLine
                                            + "ไม่สามารถ เปลี่ยน สถานะ", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        return;
                                    }
                                    else
                                    {
                                        if (dataGridView_Part["IsCancel", e.RowIndex].Value.ToString() == "1")
                                        {
                                            dataGridView_Part["IsCancel", e.RowIndex].Value = "0";

                                            string _xNO = "";
                                            string erpNO = "";
                                            string erpPartNO = "";

                                            try
                                            {
                                                _xNO = dataGridView_Part["NO.", e.RowIndex].Value.ToString();
                                                erpNO = dataGridView_Part["NO.", e.RowIndex + 1].Value.ToString();
                                                erpPartNO = dataGridView_Part["Part no.", e.RowIndex + 1].Value.ToString();

                                            }
                                            catch (Exception)
                                            {
                                            }
                                            if (_xNO == erpNO && erpPartNO != "")
                                            {
                                                dataGridView_Part["IsCancel", e.RowIndex + 1].Value = "0";
                                                dataGridView_Part["สถานะ", e.RowIndex + 1].Value = "เปลี่ยน";
                                            }


                                            if (tmpStatusPart == "ค้าง" && tmpStatusPartID == dataGridView_Part["Part no.", e.RowIndex].Value.ToString())
                                                dataGridView_Part["สถานะ", e.RowIndex].Value = "เปลี่ยน";

                                        }
                                        else
                                        {
                                            #region ไม่สามารถยกเลิกเพื่อแก้ไขข้อมูลได้ เนื่องจาก Download จาก Server

                                            if (MessageBox.Show("ไม่สามารถยกเลิกเพื่อแก้ไขข้อมูลได้ เนื่องจาก Download จาก Server" + Environment.NewLine +
                                                "คุณต้องการระบุสถานะยกเลิก ใช่หรือไม่", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                            {
                                                dataGridView_Part["IsCancel", e.RowIndex].Value = dataGridView_Part["IsCancel", e.RowIndex].Value.ToString() == "0" ? "1" : "0";

                                                string _xNO = "";

                                                string erpNO = "";
                                                string erpPartNO = "";

                                                try
                                                {
                                                    _xNO = dataGridView_Part["NO.", e.RowIndex].Value.ToString();
                                                    erpNO = dataGridView_Part["NO.", e.RowIndex + 1].Value.ToString();
                                                    erpPartNO = dataGridView_Part["Part no.", e.RowIndex + 1].Value.ToString();

                                                }
                                                catch (Exception)
                                                {
                                                }
                                                if (_xNO == erpNO && erpPartNO != "")
                                                {
                                                    dataGridView_Part["IsCancel", e.RowIndex + 1].Value = dataGridView_Part["IsCancel", e.RowIndex + 1].Value.ToString() == "0" ? "1" : "0";
                                                    dataGridView_Part["สถานะ", e.RowIndex + 1].Value = "เปลี่ยน";
                                                }

                                                if (dataGridView_Part["สถานะ", e.RowIndex].Value.ToString() == "ค้าง")
                                                {
                                                    tmpStatusPart = "ค้าง";
                                                    tmpStatusPartID = dataGridView_Part["Part no.", e.RowIndex].Value.ToString();
                                                    dataGridView_Part["สถานะ", e.RowIndex].Value = "เปลี่ยน";
                                                }
                                            }

                                            #endregion
                                        }


                                        setDataGridViewStyle();
                                    }// if (q != null)
                                }// using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext())

                                #endregion
                            }
                            else
                            {
                                #region IsDownload = N
                                var xmess = "";
                                if (smg)
                                {
                                    xmess =
                                        Environment.NewLine + "" +
                                        Environment.NewLine + "#กรณี...SMG แก้ไข JOB ย้อนหลัง " +
                                        Environment.NewLine + "โปรดระมัดระวังในการยกเลิกรายการ อะไหล่ " +
                                        Environment.NewLine + "เนื่องจาก เป็นการลบรายการออก JOB";
                                }



                                //if (MessageBox.Show("คุณต้องการยกเลิก รายการอะไหล่ [" + dataGridView_Part["Part no.", e.RowIndex].Value.ToString() + "] ใช่หรือไม่???..." + Environment.NewLine +
                                //    "การยกเลิก รายการอะไหล่ จะเป็นการลบออกจาก JOB" + Environment.NewLine +
                                //    "" + Environment.NewLine +
                                //    "1.โปรดระมัดระวังในการยืนยัน...ตอบ   YES เพราะเป็นการลบออกจาก JOB" + Environment.NewLine +
                                //    "2.ถ้าต้องการแก้ไข...ให้ยกเลิกรายการอะไหล่...ขึ้นด้านบน...แล้วแก้ไข..." + Environment.NewLine +
                                //    "3.จากนั้นกดปุ่ม เลือกอะไหล่...เพื่อดึงรายการใส่ตาราง JOB อีกครั้ง" + Environment.NewLine +
                                //    "4.จบขั้นตอน" + xmess, "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.Yes)
                                //{

                                FlexibleMessageBox.FONT = new Font("Tahoma", 12, FontStyle.Bold);
                                if (FlexibleMessageBox.Show("คุณต้องการยกเลิก รายการอะไหล่ [" + dataGridView_Part["Part no.", e.RowIndex].Value.ToString() + "] ใช่หรือไม่???..." + Environment.NewLine +
                                    "การยกเลิก รายการอะไหล่ จะเป็นการลบออกจาก JOB" + Environment.NewLine +
                                    "" + Environment.NewLine +
                                    "1.โปรดระมัดระวังในการยืนยัน...ตอบ   YES เพราะเป็นการลบออกจาก JOB" + Environment.NewLine +
                                    "2.ถ้าต้องการแก้ไข...ให้ยกเลิกรายการอะไหล่...ขึ้นด้านบน...แล้วแก้ไข..." + Environment.NewLine +
                                    "3.จากนั้นกดปุ่ม เลือกอะไหล่...เพื่อดึงรายการใส่ตาราง JOB อีกครั้ง" + Environment.NewLine +
                                    "4.จบขั้นตอน" + xmess, "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.Yes)
                                {

                                    tmpCalSPA_ID = dataGridView_Part["Part no.", e.RowIndex].Value.ToString();
                                    tmpPA_ID_NO = Convert.ToInt32(dataGridView_Part["NO.", e.RowIndex].Value.ToString());

                                    setGridPart_ToText(e);

                                    LstChecklogEditDel = new List<LChecklogEditDel>();//20200626

                                    if (editMode == "job_edit")
                                        strConn = GetConnection.Connection;
                                    else
                                        strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;

                                    using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext(strConn))
                                    {
                                        //20200916 var q = dc.JOB_Detail_Spares.Where(t => t.JOB_ID == txtJobID.Text.Trim() && t.SPA_ID == tmpCalSPA_ID).FirstOrDefault();                                  

                                        //==20200917

                                        if (editMode == "job_edit")
                                        {
                                            DateTime EndCancel = DateTime.Parse(_pEnddate);

                                            var q = dc.JOB_Detail_Spares.Where(t => t.JOB_ID == txtJobID.Text.Trim()
                                               && t.SPA_ID == tmpCalSPA_ID
                                               && t.NO == tmpPA_ID_NO
                                               && t.Enddate.Value.Date == EndCancel.Date
                                               && t.Enddate.Value.Hour == EndCancel.Hour
                                               && t.Enddate.Value.Minute == EndCancel.Minute
                                               && t.Enddate.Value.Second == EndCancel.Second
                                               ).FirstOrDefault();
                                            if (q != null)
                                            {
                                                //20200626
                                                string _vLlocat = dataGridView_Part["Location", e.RowIndex].Value.ToString();
                                                string _vLpoi = dataGridView_Part["รหัสจุดเสีย", e.RowIndex].Value.ToString();
                                                string _vLfai = dataGridView_Part["FAI_ID", e.RowIndex].Value.ToString();
                                                string _vLsn = dataGridView_Part["S/N", e.RowIndex].Value.ToString();
                                                string _vLno = dataGridView_Part["NO.", e.RowIndex].Value.ToString();
                                                string _vLspa = dataGridView_Part["Part no.", e.RowIndex].Value.ToString();
                                                string _vLtra = dataGridView_Part["Type", e.RowIndex].Value.ToString();
                                                string _vLprice = dataGridView_Part["รวมสุทธิ", e.RowIndex].Value.ToString();
                                                LstChecklogEditDel.Add(new LChecklogEditDel
                                                {
                                                    _LJOBID = txtJobID.Text.Trim(),
                                                    _Llocat = _vLlocat,
                                                    _Lpoi = _vLpoi,
                                                    _Lfai = _vLfai,
                                                    _Lsn = _vLsn,
                                                    _Lno = _vLno,
                                                    _Lspa = _vLspa,
                                                    _Ltra = _vLtra,
                                                    _Lprice = _vLprice,
                                                    _Lend = DateTime.Parse(_pEnddate)
                                                }
                                                );
                                                //========

                                                dc.JOB_Detail_Spares.DeleteOnSubmit(q);
                                                dc.SubmitChanges();

                                                //==20200626
                                                string wordingLog = "Delete_spa" + _vLspa + "_poi" + _vLpoi + "_fai" + _vLfai + "_loc" + _vLlocat + "_SN" + _vLsn + "_tra" + _vLtra + "_Price" + _vLprice + "_enddate" + _pEnddate + "";

                                                var sql_logDel = "";
                                                sql_logDel = "Insert into [LogSystem] ([TableName] ,[DataID] ,[Detail] ,[UserID] ,[IPAddress]) " +
                                                           " values ('JOB_Detail_Spare'  ,'" + txtJobID.Text.Trim() + "' ,'" + wordingLog + "' ,'" + UserInfo.UserId + "' ,CONVERT(char(15), CONNECTIONPROPERTY('client_net_address')))";
                                                dc.ExecuteCommand(sql_logDel);

                                                //=========
                                            }
                                        }
                                        //else
                                        //{//mode บันทึก
                                        //    //DateTime StrCancel = DateTime.Parse(txtStartDateIn.Text.ToString()); 
                                        //    var q = dc.JOB_Detail_Spares.Where(t => t.JOB_ID == txtJobID.Text.Trim()
                                        //        && t.SPA_ID == tmpCalSPA_ID
                                        //        && t.NO == tmpPA_ID_NO
                                        //        //&& t.StartDate.Date == StrCancel.Date
                                        //        //&& t.StartDate.Hour == StrCancel.Hour
                                        //        //&& t.StartDate.Minute == StrCancel.Minute
                                        //        //&& t.StartDate.Second == StrCancel.Second
                                        //        ).FirstOrDefault();
                                        //    if (q != null)
                                        //    {   

                                        //        //20200626
                                        //        string _vLlocat = dataGridView_Part["Location", e.RowIndex].Value.ToString();
                                        //        string _vLpoi = dataGridView_Part["รหัสจุดเสีย", e.RowIndex].Value.ToString();
                                        //        string _vLfai = dataGridView_Part["FAI_ID", e.RowIndex].Value.ToString();
                                        //        string _vLsn = dataGridView_Part["S/N", e.RowIndex].Value.ToString();
                                        //        string _vLno = dataGridView_Part["NO.", e.RowIndex].Value.ToString();
                                        //        string _vLspa = dataGridView_Part["Part no.", e.RowIndex].Value.ToString();
                                        //        string _vLtra = dataGridView_Part["Type", e.RowIndex].Value.ToString();
                                        //        string _vLprice = dataGridView_Part["รวมสุทธิ", e.RowIndex].Value.ToString();
                                        //        LstChecklogEditDel.Add(new LChecklogEditDel
                                        //        {
                                        //            _LJOBID = txtJobID.Text.Trim(),
                                        //            _Llocat = _vLlocat,
                                        //            _Lpoi = _vLpoi,
                                        //            _Lfai = _vLfai,
                                        //            _Lsn = _vLsn,
                                        //            _Lno = _vLno,
                                        //            _Lspa = _vLspa,
                                        //            _Ltra = _vLtra,
                                        //            _Lprice = _vLprice,
                                        //            //20200917_Lend = DateTime.Parse(_pEnddate)
                                        //        }
                                        //        );
                                        //        //========
                                        //        /* 20200917
                                        //        //dc.JOB_Detail_Spares.DeleteOnSubmit(q);
                                        //        //dc.SubmitChanges();
                                        //         */

                                        //        //==20200626
                                        //        //string wordingLog = "Delete_spa" + _vLspa + "_poi" + _vLpoi + "_fai" + _vLfai + "_loc" + _vLlocat + "_SN" + _vLsn + "_tra" + _vLtra + "_Price" + _vLprice + "_enddate" + _pEnddate + "";
                                        //        string wordingLog = "Delete_spa" + _vLspa + "_poi" + _vLpoi + "_fai" + _vLfai + "_loc" + _vLlocat + "_SN" + _vLsn + "_tra" + _vLtra + "_Price" + _vLprice + "";

                                        //        var sql_logDel = "";
                                        //        sql_logDel = "Insert into [LogSystem] ([TableName] ,[DataID] ,[Detail] ,[UserID] ,[IPAddress]) " +
                                        //                   " values ('JOB_Detail_Spare'  ,'" + txtJobID.Text.Trim() + "' ,'" + wordingLog + "' ,'" + UserInfo.UserId + "' ,CONVERT(char(15), CONNECTIONPROPERTY('client_net_address')))";
                                        //        dc.ExecuteCommand(sql_logDel);
                                        //    }
                                        //    //=========
                                        //    //}
                                        //    //==
                                        //}
                                    }


                                    dtPart.Rows[e.RowIndex].Delete();
                                    PartBindind();


                                    //20181122
                                    SparepartIsPriceChange(tmpCalSPA_ID);

                                }
                                //countPartNO();

                                #endregion
                            }

                            checkPartRemain();

                            SMG_Edit();

                        }
                        catch (System.Exception ex)
                        {
                            MessageBox.Show("ข้อผิดพลาด " + ex.Message, "เกิดข้อผิดพลาด");
                        }
                        break;

                    #endregion

                    //20160817 case 15://สถานะ
                    case 16://สถานะ
                        #region การเปลี่ยน อะไหล่

                        if (dataGridView_Part["สถานะ", e.RowIndex].Value.ToString() == "ค้าง")
                        {
                            if (MessageBox.Show("คุณต้องการเปลี่ยน อะไหล่ ใช่หรือไม่???...", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {
                                using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext(strConn))
                                {

                                    DataRow dr = null;
                                    string _spaID = dataGridView_Part["Part no.", e.RowIndex].Value.ToString();
                                    int _spaNo = Convert.ToInt32(dataGridView_Part["NO.", e.RowIndex].Value.ToString());

                                    //20140908
                                    string _SNOld = dataGridView_Part["S/N Old", e.RowIndex].Value.ToString();

                                    var q = dc.SpareParts
                                         .Where(t => t.ERP_SPA_ID == _spaID && t.ERP_SPA_Split == true)
                                         .FirstOrDefault();
                                    if (q != null)
                                    {
                                        MessageBox.Show("Part Auto Load to ERP" + Environment.NewLine
                                            + "ไม่สามารถ เปลี่ยน สถานะ", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        return;
                                    }

                                    dataGridView_Part["สถานะ", e.RowIndex].Value = "เปลี่ยน";


                                    string _poid = dataGridView_Part["รหัสจุดเสีย", e.RowIndex].Value.ToString(); //cob_PoitFail0.SelectedValue == null ? "000" : cob_PoitFail0.SelectedValue.ToString().Trim();
                                    string _pode = dataGridView_Part["จุดเสีย", e.RowIndex].Value.ToString();
                                    string radPartStatus = "เปลี่ยน";

                                    decimal priceList = string.IsNullOrEmpty(dataGridView_Part["PricesList", e.RowIndex].Value.ToString()) ? 0 : decimal.Parse(dataGridView_Part["PricesList", e.RowIndex].Value.ToString());
                                    decimal vat = string.IsNullOrEmpty(dataGridView_Part["VAT", e.RowIndex].Value.ToString()) ? 0 : decimal.Parse(dataGridView_Part["VAT", e.RowIndex].Value.ToString());
                                    decimal dis = string.IsNullOrEmpty(dataGridView_Part["Discount", e.RowIndex].Value.ToString()) ? 0 : decimal.Parse(dataGridView_Part["Discount", e.RowIndex].Value.ToString());

                                    //จำนวนอะใหล่
                                    decimal qt = decimal.Parse(dataGridView_Part["จำนวน", e.RowIndex].Value.ToString());

                                    if (dis > 0)
                                        priceList = priceList - (priceList * (dis / 100));

                                    //ราคา/หน่วย
                                    decimal pricePerUnit = 0;// Math.Round(priceList, tmpRounup);
                                    if (tmpRounup > 0)
                                        pricePerUnit = Math.Round(priceList, tmpRounup);
                                    else
                                        pricePerUnit = priceList;


                                    //dtPart
                                    int xrow = 0;

                                    if (tmpPA_ID_NO != 0)
                                    {
                                        xrow = tmpPA_ID_NO - 1;
                                        tmpPA_ID_NO = 0;
                                    }

                                    //รวมสุทธิ
                                    decimal _xtotal = pricePerUnit * qt;

                                    //ภาษี
                                    decimal _xvat = vat > 0 ? _xtotal * (vat / 100) : 0;

                                    //20141021
                                    //20141127ยกเลิก  int _EQP_ID = int.Parse(dataGridView_Part["ตำแหน่งที่เปลี่ยน", e.RowIndex].Value.ToString());

                                    //var q2 = dc.SpareParts
                                    //      .Where(t => t.SPA_ID == _spaID && t.ERP_SPA_ID != null && t.ERP_SPA_Split == true)
                                    //      .FirstOrDefault();

                                    var q2 = (from ct in dc.Conf_Conditions
                                              from sp in dc.SpareParts

                                              where (ct.SPA_ID == sp.SPA_ID) &&
                                                  (ct.SPA_ID == tmpSPA_ID && ct.GOP_ID == tmpGOPId && ct.ContractNo == _contractNo) &&
                                                  (ct.ERP_SPA_ID != null && ct.ERP_TRA_ID != null)
                                              select new SPLIT_Part_ERP
                                              {
                                                  ERP_SPA_ID = ct.ERP_SPA_ID,
                                                  ERP_PartName = sp.SparePart1,
                                                  ERP_TRA_ID = ct.ERP_TRA_ID
                                              }).FirstOrDefault();

                                    if (q2 != null)
                                    {
                                        //dr = setPartDetil_to_ERP(dc, dr, _poid, _pode, radPartStatus, vat, dis, qt, pricePerUnit, _spaNo - 1, _xtotal, _xvat, q2);
                                        //20140908
                                        //20141127ยกเลิก dr = setPartDetil_to_ERP(dc, dr, _poid, _pode, radPartStatus, vat, dis, qt, pricePerUnit, _spaNo - 1, _xtotal, _xvat, q2, _SNOld,_EQP_ID);
                                        dr = setPartDetil_to_ERP(dc, dr, _poid, _pode, radPartStatus, vat, dis, qt, pricePerUnit, _spaNo - 1, _xtotal, _xvat, q2, _SNOld);
                                        PartBindind();
                                    }
                                }// using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext())

                            }
                        }
                        else
                        {
                            if (MessageBox.Show("คุณต้องการค้าง อะไหล่ ใช่หรือไม่???...", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {
                                string _spa_id = dataGridView_Part["Part no.", e.RowIndex].Value.ToString();
                                int _spaNo = Convert.ToInt32(dataGridView_Part["NO.", e.RowIndex].Value.ToString());

                                using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext(strConn))
                                {
                                    //--20201204 

                                    var js = dc.JOBs.Where(t => t.JOBS_ID == "99" && t.JOB_ID == txtJobID.Text.Trim()).FirstOrDefault();
                                    if (js != null)
                                    {
                                        closeProgress();
                                        MessageBox.Show("JOB : " + txtJobID.Text.Trim() + " ปิด JOB ไปแล้ว  " + Environment.NewLine + " ไม่สามารถเลือก สถานะ ของรายการอะไหล่เป็น ค้าง ได้", "ผลการตรวจสอบ");
                                        return;
                                    }

                                    //--========

                                    var q = dc.SpareParts
                                            .Where(t => t.ERP_SPA_ID == _spa_id && t.ERP_SPA_Split == true)
                                            .FirstOrDefault();
                                    if (q != null)
                                    {
                                        MessageBox.Show("Part Auto Load to ERP" + Environment.NewLine
                                           + "ไม่สามารถ เปลี่ยน สถานะ", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        return;
                                    }

                                    dataGridView_Part["สถานะ", e.RowIndex].Value = "ค้าง";


                                    try
                                    {


                                        var q2 = dc.SpareParts
                                            .Where(t => t.SPA_ID == _spa_id && t.ERP_SPA_ID != null)
                                            .FirstOrDefault();
                                        if (q2 != null)
                                        {
                                            foreach (DataGridViewRow row in dataGridView_Part.Rows)
                                            {
                                                if (int.Parse(row.Cells["NO."].Value.ToString()) == _spaNo &&
                                                    row.Cells["Part no."].Value.ToString() == q2.ERP_SPA_ID)
                                                    dataGridView_Part.Rows.Remove(row);
                                            }


                                        }
                                    }
                                    catch (Exception)
                                    {
                                    }
                                }

                            }
                        }

                        checkPartRemain();

                        break;
                    #endregion

                    //====20200624 
                    case 22:
                        #region Iscancel
                        if (smg)
                        {
                            if (dataGridView_Part["IsCancel", e.RowIndex].Value.ToString() == "0")
                            {
                                if (MessageBox.Show("คุณต้องการ ยกเลิก รายการอะไหล่ ใช่หรือไม่???...", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                {
                                    dataGridView_Part["IsCancel", e.RowIndex].Value = "1";

                                }
                            }
                            else if (dataGridView_Part["IsCancel", e.RowIndex].Value.ToString() == "1")
                            {
                                if (MessageBox.Show("คุณต้องการ ใช้งาน รายการอะไหล่  ใช่หรือไม่???...", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                {
                                    dataGridView_Part["IsCancel", e.RowIndex].Value = "0";

                                }
                            }
                            setDataGridViewStyle();
                        }

                        //checkPartRemain();

                        break;

                    #endregion
                    //=====================
                    //20160817 case 23://เก็บเงินหน้างาน
                    case 24://เก็บเงินหน้างาน
                        #region IsPrices
                        ////ห้ามแก้ไข Edit 2012-08-19 v.10.1.2012
                        if (!_IsPrintPrice)
                        {
                            if (dataGridView_Part["isPrices", e.RowIndex].Value.ToString() == "0")
                            {
                                if (MessageBox.Show("คุณต้องการ พิมพ์ราคา ใช่หรือไม่???...", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                {
                                    dataGridView_Part["isPrices", e.RowIndex].Value = "1";
                                    dataGridView_Part["ออกบิลในนาม", e.RowIndex].Value = "สถานี";
                                }
                            }
                            //20160817 else
                            else if (dataGridView_Part["isPrices", e.RowIndex].Value.ToString() == "1")
                            {
                                if (MessageBox.Show("คุณไม่ต้องการ พิมพ์ราคา ใช่หรือไม่???...", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                {
                                    dataGridView_Part["isPrices", e.RowIndex].Value = "0";
                                    dataGridView_Part["ออกบิลในนาม", e.RowIndex].Value = "บริษัทน้ำมัน";
                                }
                            }
                        }
                        //checkPartRemain();

                        break;

                    #endregion

                    //20160817 case 27://อะไหล่ลูกค้า
                    case 28://อะไหล่ลูกค้า
                        #region IsCustomer

                        if (dataGridView_Part["อะไหล่ลูกค้า", e.RowIndex].Value.ToString() == "N")
                        {
                            if (MessageBox.Show("คุณต้องการ เปลี่ยนสถานะเป็น อะไหล่ลูกค้า ใช่หรือไม่???...", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {
                                dataGridView_Part["อะไหล่ลูกค้า", e.RowIndex].Value = "Y";

                            }
                        }
                        else if (dataGridView_Part["อะไหล่ลูกค้า", e.RowIndex].Value.ToString() == "Y")
                        {
                            if (MessageBox.Show("คุณต้องการ เปลี่ยนสถานะเป็น ไม่ใช่อะไหล่ลูกค้า ใช่หรือไม่???...", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {
                                dataGridView_Part["อะไหล่ลูกค้า", e.RowIndex].Value = "N";

                            }
                        }

                        //checkPartRemain();

                        break;

                        #endregion

                }

            }
        }

        private void setDataGridViewStyle()
        {
            for (int r = 0; r <= dataGridView_Part.RowCount - 1; r++)
            {
                if (dataGridView_Part["IsCancel", r].Value.ToString() == "1")
                {
                    dataGridView_Part["IsCancel", r].Style.BackColor = System.Drawing.Color.Yellow;

                    for (int c = 0; c <= dataGridView_Part.ColumnCount - 1; c++)
                    {
                        dataGridView_Part[c, r].Style.Font = new Font(dataGridView_Part.Font, FontStyle.Bold);
                        dataGridView_Part[c, r].Style.ForeColor = System.Drawing.Color.Red;
                    }
                    dataGridView_Part["จำนวน", r].Style.BackColor = Color.Yellow;
                    dataGridView_Part["Comment", r].Style.BackColor = Color.White;
                    dataGridView_Part["สถานะ", r].Style.BackColor = Color.Yellow;
                    dataGridView_Part["รหัสจุดเสีย", r].Style.BackColor = Color.Yellow;
                    dataGridView_Part["จุดเสีย", r].Style.BackColor = Color.Yellow;

                    dataGridView_Part["NO.", r].Style.BackColor = Color.Yellow;
                    //20140908
                    dataGridView_Part["S/N Old", r].Style.BackColor = Color.Yellow;

                    dataGridView_Part["Part no.", r].Style.BackColor = Color.Yellow;
                    dataGridView_Part["Part", r].Style.BackColor = Color.Yellow;
                    dataGridView_Part["Type", r].Style.BackColor = Color.Yellow;
                    dataGridView_Part["FAI_ID", r].Style.BackColor = Color.Yellow;
                    dataGridView_Part["Location", r].Style.BackColor = Color.Yellow;

                    dataGridView_Part["IsDownload", r].Style.BackColor = Color.Yellow;
                    dataGridView_Part["IsPrices", r].Style.BackColor = Color.Yellow;
                    //อะไหล่ลูกค้า
                    //2013-02013 dataGridView_Part["อะไหล่ลูกค้า", r].Style.BackColor = Color.White;
                    dataGridView_Part["อะไหล่ลูกค้า", r].Style.BackColor = Color.Yellow;

                    //20141021
                    //20141215
                    //dataGridView_Part["ตำแหน่งที่เปลี่ยน", r].Style.BackColor = Color.Yellow;

                    //20161115
                    dataGridView_Part["Page/Item", r].Style.BackColor = Color.Yellow;
                    dataGridView_Part["ERP_LoadTime", r].Style.BackColor = Color.Yellow;


                }
                else
                {


                    dataGridView_Part["IsCancel", r].Style.BackColor = System.Drawing.Color.LightGray;
                    //===20200624 
                    var smg = new string[] { "09", "12", "13" }.Contains(UserInfo.UserLevID);
                    if (smg)
                    {
                        dataGridView_Part["IsCancel", r].Style.BackColor = System.Drawing.Color.White;
                    }
                    //======
                    for (int c = 0; c <= dataGridView_Part.ColumnCount - 1; c++)
                    {
                        dataGridView_Part[c, r].Style.Font = new Font(dataGridView_Part.Font, FontStyle.Regular);
                        dataGridView_Part[c, r].Style.ForeColor = Color.Black;
                    }

                    dataGridView_Part["จำนวน", r].Style.BackColor = Color.White;
                    dataGridView_Part["Comment", r].Style.BackColor = Color.White;
                    dataGridView_Part["สถานะ", r].Style.BackColor = Color.White;

                    dataGridView_Part["รหัสจุดเสีย", r].Style.BackColor = Color.LightGray;
                    dataGridView_Part["จุดเสีย", r].Style.BackColor = Color.LightGray;
                    dataGridView_Part["NO.", r].Style.BackColor = Color.LightGray;
                    //20140908
                    dataGridView_Part["S/N Old", r].Style.BackColor = Color.LightGray;

                    dataGridView_Part["Part no.", r].Style.BackColor = Color.LightGray;
                    dataGridView_Part["Part", r].Style.BackColor = Color.LightGray;
                    dataGridView_Part["Type", r].Style.BackColor = Color.LightGray;

                    dataGridView_Part["PricesList", r].Style.BackColor = Color.LightGray;
                    dataGridView_Part["ราคา/หน่วย", r].Style.BackColor = Color.LightGray;
                    dataGridView_Part["รวม", r].Style.BackColor = Color.LightGray;
                    dataGridView_Part["ภาษี", r].Style.BackColor = Color.LightGray;
                    dataGridView_Part["รวมสุทธิ", r].Style.BackColor = Color.LightGray;
                    dataGridView_Part["Discount", r].Style.BackColor = Color.LightGray;
                    dataGridView_Part["VAT", r].Style.BackColor = Color.LightGray;
                    //dataGridView_Part["FixAssetNo", r].Style.BackColor = Color.LightGray;

                    dataGridView_Part["FAI_ID", r].Style.BackColor = Color.LightGray;
                    dataGridView_Part["Location", r].Style.BackColor = Color.LightGray;
                    dataGridView_Part["IsDownload", r].Style.BackColor = Color.LightGray;

                    if (_IsPrintPrice)
                        dataGridView_Part["IsPrices", r].Style.BackColor = Color.LightGray;
                    else
                        dataGridView_Part["IsPrices", r].Style.BackColor = Color.White;

                    dataGridView_Part["ออกบิลในนาม", r].Style.BackColor = Color.LightGray;
                    dataGridView_Part["ERP_SPA_ID", r].Style.BackColor = Color.LightGray;
                    dataGridView_Part["ERP_PartName", r].Style.BackColor = Color.LightGray;
                    //อะไหล่ลูกค้า
                    dataGridView_Part["อะไหล่ลูกค้า", r].Style.BackColor = Color.White;


                    //20141021
                    //20141215
                    //dataGridView_Part["ตำแหน่งที่เปลี่ยน", r].Style.BackColor = Color.LightGray;

                    //20161115
                    dataGridView_Part["Page/Item", r].Style.BackColor = Color.LightGray;

                    dataGridView_Part["ERP_LoadTime", r].Style.BackColor = Color.LightGray;

                }
            }
        }



        private void setDataGridViewFailPointStyle()
        {
            for (int r = 0; r <= dataGridView_FailPoint.RowCount - 1; r++)
            {
                if (dataGridView_FailPoint["IsCancel", r].Value.ToString() == "1")
                {
                    dataGridView_FailPoint["IsCancel", r].Style.BackColor = System.Drawing.Color.Yellow;

                    for (int c = 0; c <= dataGridView_FailPoint.ColumnCount - 1; c++)
                    {
                        dataGridView_FailPoint[c, r].Style.Font = new Font(dataGridView_Part.Font, FontStyle.Bold);
                        dataGridView_FailPoint[c, r].Style.ForeColor = System.Drawing.Color.Red;
                    }


                    dataGridView_FailPoint["อาการเสีย", r].Style.BackColor = Color.Yellow;
                    dataGridView_FailPoint["NO.", r].Style.BackColor = Color.Yellow;
                    dataGridView_FailPoint["รหัสจุดเสีย", r].Style.BackColor = Color.Yellow;
                    dataGridView_FailPoint["จุดเสีย", r].Style.BackColor = Color.Yellow;

                    //20200318 dataGridView_FailPoint["S/N(Old)", r].Style.BackColor = Color.White;
                    //20200318 dataGridView_FailPoint["S/N(New)", r].Style.BackColor = Color.White;
                    dataGridView_FailPoint["S/N(Old)", r].Style.BackColor = Color.Yellow;
                    dataGridView_FailPoint["S/N(New)", r].Style.BackColor = Color.Yellow;
                    dataGridView_FailPoint["Location", r].Style.BackColor = Color.White;
                    dataGridView_FailPoint["Model", r].Style.BackColor = Color.White;


                    dataGridView_FailPoint["StartLiter", r].Style.BackColor = Color.White;
                    dataGridView_FailPoint["EndLiter", r].Style.BackColor = Color.White;
                    dataGridView_FailPoint["LiterTest", r].Style.BackColor = Color.Yellow;

                    dataGridView_FailPoint["FAI_ID", r].Style.BackColor = Color.Yellow;
                    dataGridView_FailPoint["IsCancel", r].Style.BackColor = Color.Yellow;
                    dataGridView_FailPoint["IsDownload", r].Style.BackColor = Color.Yellow;

                    dataGridView_FailPoint["FCAT_CH", r].Style.BackColor = Color.Yellow;//20180803



                }
                else
                {

                    dataGridView_FailPoint["IsCancel", r].Style.BackColor = System.Drawing.Color.LightGray;
                    //===20200624 
                    var smg = new string[] { "09", "12", "13" }.Contains(UserInfo.UserLevID);
                    if (smg)
                    {
                        dataGridView_FailPoint["IsCancel", r].Style.BackColor = System.Drawing.Color.White;
                    }
                    //======
                    for (int c = 0; c <= dataGridView_FailPoint.ColumnCount - 1; c++)
                    {
                        dataGridView_FailPoint[c, r].Style.Font = new Font(dataGridView_Part.Font, FontStyle.Regular);
                        dataGridView_FailPoint[c, r].Style.ForeColor = System.Drawing.Color.Black;
                    }


                    dataGridView_FailPoint["อาการเสีย", r].Style.BackColor = Color.White;
                    dataGridView_FailPoint["NO.", r].Style.BackColor = Color.LightGray;
                    dataGridView_FailPoint["รหัสจุดเสีย", r].Style.BackColor = Color.LightGray;
                    dataGridView_FailPoint["จุดเสีย", r].Style.BackColor = Color.LightGray;

                    //20200318 dataGridView_FailPoint["S/N(Old)", r].Style.BackColor = Color.White;
                    //20200318 dataGridView_FailPoint["S/N(New)", r].Style.BackColor = Color.White;
                    dataGridView_FailPoint["S/N(Old)", r].Style.BackColor = Color.LightGray;
                    dataGridView_FailPoint["S/N(New)", r].Style.BackColor = Color.LightGray;
                    dataGridView_FailPoint["Location", r].Style.BackColor = Color.White;
                    dataGridView_FailPoint["Model", r].Style.BackColor = Color.White;


                    dataGridView_FailPoint["StartLiter", r].Style.BackColor = Color.White;
                    dataGridView_FailPoint["EndLiter", r].Style.BackColor = Color.White;
                    dataGridView_FailPoint["LiterTest", r].Style.BackColor = Color.LightGray;

                    dataGridView_FailPoint["FAI_ID", r].Style.BackColor = Color.LightGray;
                    //20200624 dataGridView_FailPoint["IsCancel", r].Style.BackColor = Color.LightGray;
                    dataGridView_FailPoint["IsDownload", r].Style.BackColor = Color.LightGray;

                    dataGridView_FailPoint["FCAT_CH", r].Style.BackColor = Color.LightGray;//20180803
                    dataGridView_FailPoint["FCAT_ID", r].Style.BackColor = Color.LightGray;//20180803
                    dataGridView_FailPoint["FBA_ID", r].Style.BackColor = Color.LightGray;//20180803
                    dataGridView_FailPoint["FMD_ID", r].Style.BackColor = Color.LightGray;//20180803




                }
            }
        }


        private void clearOldPart(string _SPA_ID)
        {
            try
            {
                using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext(strConn))
                {
                    if ((from t in dc.JOB_Detail_Spares where t.JOB_ID.Trim() == txtJobID.Text.Trim() && t.SPA_ID.Trim() == _SPA_ID.Trim() select t).Count() > 0)
                    {
                        var q = (from t in dc.JOB_Detail_Spares where t.JOB_ID.Trim() == txtJobID.Text.Trim() && t.SPA_ID.Trim() == _SPA_ID.Trim() select t).FirstOrDefault();

                        dc.JOB_Detail_Spares.DeleteOnSubmit(q);
                        dc.SubmitChanges();
                    }
                }
            }
            catch (System.Exception)
            {
            }
        }



        private void setGridPart_ToText(DataGridViewCellEventArgs e)
        {

            tmpLocation = dataGridView_Part.Rows[e.RowIndex].Cells["Location"].Value.ToString();
            tmpFAIID = dataGridView_Part.Rows[e.RowIndex].Cells["FAI_ID"].Value.ToString();

            cob_PoitFail0.SelectedValue = dataGridView_Part.Rows[e.RowIndex].Cells["รหัสจุดเสีย"].Value.ToString();

            txtPartNo.Text = dataGridView_Part.Rows[e.RowIndex].Cells["Part no."].Value.ToString();
            tmpSPA_ID = dataGridView_Part.Rows[e.RowIndex].Cells["Part no."].Value.ToString();
            txtPartName.Text = dataGridView_Part.Rows[e.RowIndex].Cells["Part"].Value.ToString();

            //decimal price = dataGridView_Part.Rows[e.RowIndex].Cells["ราคา/หน่วย"].Value.ToString()
            txtVAT.Text = dataGridView_Part.Rows[e.RowIndex].Cells["VAT"].Value.ToString();
            //dr["PricesList"] = txtPriceList.Text; 
            txtPriceList.Text = dataGridView_Part.Rows[e.RowIndex].Cells["PricesList"].Value.ToString();
            txtDiscQuarter.Text = dataGridView_Part.Rows[e.RowIndex].Cells["Discount"].Value.ToString();

            txtPartQT.Text = dataGridView_Part.Rows[e.RowIndex].Cells["จำนวน"].Value.ToString();

            string radTranType = dataGridView_Part.Rows[e.RowIndex].Cells["สถานะ"].Value.ToString();

            if (radTranType == "ค้าง")
                radTranType0.Checked = true;
            else if (radTranType == "เปลี่ยน")
                radTranType1.Checked = true;

            txtPartComment.Text = dataGridView_Part.Rows[e.RowIndex].Cells["Comment"].Value.ToString();


            cob_TrnType.SelectedValue = dataGridView_Part.Rows[e.RowIndex].Cells["Type"].Value.ToString();

            chIsPrices.Checked = (dataGridView_Part.Rows[e.RowIndex].Cells["IsPrices"].Value.ToString() == "0" ? false : true);
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext(strConn))
            {
                JOB_IS_PrintPrice(dc);
            }

            con_ERP_StationCharge.Text = dataGridView_Part.Rows[e.RowIndex].Cells["ออกบิลในนาม"].Value.ToString();

            //20140908
            txtPartSerialNumberOld.Text = dataGridView_Part.Rows[e.RowIndex].Cells["S/N Old"].Value.ToString();

            //20141021
            //20141127ยกเลิก lbEQP_ID.Text = dataGridView_Part.Rows[e.RowIndex].Cells["ตำแหน่งที่เปลี่ยน"].Value.ToString();
            //20141127ยกเลิก int vEQP_ID = int.Parse(dataGridView_Part.Rows[e.RowIndex].Cells["ตำแหน่งที่เปลี่ยน"].Value.ToString());
            //20141127ยกเลิก 
            /*string sqlEQ = "";
            sqlEQ = " SELECT EQP_ID,Equipment_Position FROM [Equipment_Position] WHERE  EQP_ID =" + vEQP_ID + " ";
            SqlDataReader drEQ = JaSqlHelper.ExecuteReader(strConn, CommandType.Text, sqlEQ);

            if (drEQ.HasRows)
            {
                while (drEQ.Read())
                {

                    //20141127ยกเลิก txtEquipment_Position.Text = drEQ["Equipment_Position"].ToString();
                }
            }*/
            txtFixAssetNo.Text = dataGridView_Part.Rows[e.RowIndex].Cells["FixAssetNo"].Value.ToString();
            //20141024
            lblERP_orderline_id.Text = dataGridView_Part.Rows[e.RowIndex].Cells["ERP_orderline_id"].Value.ToString();

            //201611116
            txtReferPage.Text = dataGridView_Part.Rows[e.RowIndex].Cells["Page/Item"].Value.ToString();


        }

        private void dataGridView_Part_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            dataGridView_Part["รหัสจุดเสีย", e.RowIndex].ReadOnly = true;
            dataGridView_Part["จุดเสีย", e.RowIndex].ReadOnly = true;
            dataGridView_Part["NO.", e.RowIndex].ReadOnly = true;

            //20140908 
            dataGridView_Part["S/N Old", e.RowIndex].ReadOnly = false;
            dataGridView_Part["S/N Old", e.RowIndex].Style.BackColor = Color.White;

            dataGridView_Part["Part no.", e.RowIndex].ReadOnly = true;

            //แก้ไขได้-----------------------------------------------------------//
            dataGridView_Part["จำนวน", e.RowIndex].ReadOnly = false;
            dataGridView_Part["จำนวน", e.RowIndex].Style.BackColor = Color.White;
            //<----------------------------------------------------------->//

            dataGridView_Part["Part", e.RowIndex].ReadOnly = true;
            dataGridView_Part["Type", e.RowIndex].ReadOnly = true;

            //แก้ไขได้-----------------------------------------------------------//
            dataGridView_Part["Comment", e.RowIndex].ReadOnly = false;
            dataGridView_Part["Comment", e.RowIndex].Style.BackColor = Color.White;
            //<----------------------------------------------------------->//

            dataGridView_Part["สถานะ", e.RowIndex].ReadOnly = false;
            dataGridView_Part["สถานะ", e.RowIndex].Style.BackColor = Color.White;

            dataGridView_Part["FAI_ID", e.RowIndex].ReadOnly = true;
            dataGridView_Part["Location", e.RowIndex].ReadOnly = true;

            //==20200624
            dataGridView_Part["IsCancel", e.RowIndex].ReadOnly = true;
            dataGridView_Part["IsCancel", e.RowIndex].Style.BackColor = Color.LightGray;
            var smg = new string[] { "09", "12", "13" }.Contains(UserInfo.UserLevID);
            if (smg)
            {
                //แก้ไขได้-----------------------------------------------------------//

                dataGridView_Part["IsCancel", e.RowIndex].ReadOnly = false;//20200624
                dataGridView_Part["IsCancel", e.RowIndex].Style.BackColor = Color.White;
                //<----------------------------------------------------------->//
            }
            //==

            dataGridView_Part["IsDownload", e.RowIndex].ReadOnly = true;

            if (_IsPrintPrice)
            {
                //PrintPrice
                dataGridView_Part["isPrices", e.RowIndex].ReadOnly = true;
                dataGridView_Part["isPrices", e.RowIndex].Style.BackColor = Color.LightGray;
            }
            else
            {
                dataGridView_Part["isPrices", e.RowIndex].ReadOnly = false;
                dataGridView_Part["isPrices", e.RowIndex].Style.BackColor = Color.White;
            }

            dataGridView_Part["รหัสจุดเสีย", e.RowIndex].Style.BackColor = Color.LightGray;
            dataGridView_Part["จุดเสีย", e.RowIndex].Style.BackColor = Color.LightGray;
            dataGridView_Part["NO.", e.RowIndex].Style.BackColor = Color.LightGray;
            //20140908
            dataGridView_Part["S/N Old", e.RowIndex].Style.BackColor = Color.LightGray;

            dataGridView_Part["Part no.", e.RowIndex].Style.BackColor = Color.LightGray;
            dataGridView_Part["Part", e.RowIndex].Style.BackColor = Color.LightGray;
            dataGridView_Part["Type", e.RowIndex].Style.BackColor = Color.LightGray;
            dataGridView_Part["FAI_ID", e.RowIndex].Style.BackColor = Color.LightGray;
            dataGridView_Part["Location", e.RowIndex].Style.BackColor = Color.LightGray;
            //20200624dataGridView_Part["IsCancel", e.RowIndex].Style.BackColor = Color.LightGray;
            dataGridView_Part["IsDownload", e.RowIndex].Style.BackColor = Color.LightGray;


            dataGridView_Part["PricesList", e.RowIndex].Style.BackColor = Color.LightGray;
            dataGridView_Part["ราคา/หน่วย", e.RowIndex].Style.BackColor = Color.LightGray;
            dataGridView_Part["รวม", e.RowIndex].Style.BackColor = Color.LightGray;
            dataGridView_Part["ภาษี", e.RowIndex].Style.BackColor = Color.LightGray;
            dataGridView_Part["รวมสุทธิ", e.RowIndex].Style.BackColor = Color.LightGray;

            dataGridView_Part["Discount", e.RowIndex].Style.BackColor = Color.LightGray;
            dataGridView_Part["VAT", e.RowIndex].Style.BackColor = Color.LightGray;

            dataGridView_Part["PricesList", e.RowIndex].ReadOnly = true;
            dataGridView_Part["ราคา/หน่วย", e.RowIndex].ReadOnly = true;
            dataGridView_Part["รวม", e.RowIndex].ReadOnly = true;
            dataGridView_Part["ภาษี", e.RowIndex].ReadOnly = true;
            dataGridView_Part["รวมสุทธิ", e.RowIndex].ReadOnly = true;

            dataGridView_Part["Discount", e.RowIndex].ReadOnly = true;
            dataGridView_Part["VAT", e.RowIndex].ReadOnly = true;

            //20141021
            //20141215 dataGridView_Part["ตำแหน่งที่เปลี่ยน", e.RowIndex].ReadOnly = true;

            //20161115
            dataGridView_Part["Page/Item", e.RowIndex].ReadOnly = true;

        }

        private void dataGridView_FailPoint_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            dataGridView_FailPoint["NO.", e.RowIndex].ReadOnly = true;
            dataGridView_FailPoint["รหัสจุดเสีย", e.RowIndex].ReadOnly = true;
            dataGridView_FailPoint["จุดเสีย", e.RowIndex].ReadOnly = true;

            dataGridView_FailPoint["อาการเสีย", e.RowIndex].ReadOnly = false;
            //20200318 dataGridView_FailPoint["S/N(Old)", e.RowIndex].ReadOnly = false;
            //20200318 dataGridView_FailPoint["S/N(New)", e.RowIndex].ReadOnly = false;
            dataGridView_FailPoint["S/N(Old)", e.RowIndex].ReadOnly = true;
            dataGridView_FailPoint["S/N(New)", e.RowIndex].ReadOnly = true;
            dataGridView_FailPoint["Location", e.RowIndex].ReadOnly = false;
            dataGridView_FailPoint["Model", e.RowIndex].ReadOnly = false;

            dataGridView_FailPoint["อาการเสีย", e.RowIndex].Style.BackColor = Color.White;
            //dataGridView_FailPoint["S/N(Old)", e.RowIndex].Style.BackColor = Color.White;
            //dataGridView_FailPoint["S/N(New)", e.RowIndex].Style.BackColor = Color.White;
            //dataGridView_FailPoint["Location", e.RowIndex].Style.BackColor = Color.White;
            //dataGridView_FailPoint["Model", e.RowIndex].Style.BackColor = Color.White;

            dataGridView_FailPoint["StartLiter", e.RowIndex].ReadOnly = false;
            dataGridView_FailPoint["EndLiter", e.RowIndex].ReadOnly = false;
            dataGridView_FailPoint["LiterTest", e.RowIndex].ReadOnly = true;

            dataGridView_FailPoint["FAI_ID", e.RowIndex].ReadOnly = true;
            dataGridView_FailPoint["IsCancel", e.RowIndex].ReadOnly = true;
            dataGridView_FailPoint["IsCancel", e.RowIndex].Style.BackColor = Color.LightGray;
            //==20200624
            var smg = new string[] { "09", "12", "13" }.Contains(UserInfo.UserLevID);
            if (smg)
            {
                //แก้ไขได้-----------------------------------------------------------//

                dataGridView_FailPoint["IsCancel", e.RowIndex].ReadOnly = false;//20200624
                dataGridView_FailPoint["IsCancel", e.RowIndex].Style.BackColor = Color.White;
                //<----------------------------------------------------------->//
            }
            //==
            dataGridView_FailPoint["IsDownload", e.RowIndex].ReadOnly = true;

            dataGridView_FailPoint["NO.", e.RowIndex].Style.BackColor = Color.LightGray;
            dataGridView_FailPoint["รหัสจุดเสีย", e.RowIndex].Style.BackColor = Color.LightGray;
            dataGridView_FailPoint["จุดเสีย", e.RowIndex].Style.BackColor = Color.LightGray;

            //20200318 dataGridView_FailPoint["S/N(Old)", e.RowIndex].Style.BackColor = Color.White;
            //20200318 dataGridView_FailPoint["S/N(New)", e.RowIndex].Style.BackColor = Color.White;
            dataGridView_FailPoint["S/N(Old)", e.RowIndex].Style.BackColor = Color.LightGray;
            dataGridView_FailPoint["S/N(New)", e.RowIndex].Style.BackColor = Color.LightGray;
            dataGridView_FailPoint["Location", e.RowIndex].Style.BackColor = Color.White;
            dataGridView_FailPoint["Model", e.RowIndex].Style.BackColor = Color.White;


            dataGridView_FailPoint["StartLiter", e.RowIndex].Style.BackColor = Color.White;
            dataGridView_FailPoint["EndLiter", e.RowIndex].Style.BackColor = Color.White;
            dataGridView_FailPoint["LiterTest", e.RowIndex].Style.BackColor = Color.LightGray;

            dataGridView_FailPoint["FAI_ID", e.RowIndex].Style.BackColor = Color.LightGray;

            //20200624 dataGridView_FailPoint["IsCancel", e.RowIndex].Style.BackColor = Color.LightGray;
            dataGridView_FailPoint["IsDownload", e.RowIndex].Style.BackColor = Color.LightGray;
        }


        private void invoke_Progress(string strMsg)
        {
            try
            {
                m_fmProgress.lblDescription.Invoke(
                   (MethodInvoker)delegate ()
                   {
                       m_fmProgress.lblDescription.Text = strMsg;
                   }
                   );
            }
            catch (System.Exception)
            { }
        }

        private void newProgressbar()
        {
            m_fmProgress = new ProgressBar.FormProgress();
            backgroundWorker1.RunWorkerAsync();
            System.Threading.Thread.Sleep(3000);
        }

        private void closeProgress()
        {
            if (m_fmProgress != null)
            {
                m_fmProgress.Invoke(
                    (MethodInvoker)delegate ()
                    {
                        m_fmProgress.Hide();
                        m_fmProgress.Dispose();
                        m_fmProgress = null;
                    }
                );
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            m_fmProgress.Activate();
            m_fmProgress.BringToFront();
            m_fmProgress.TopMost = true;
            m_fmProgress.ShowDialog();
        }

        private void radioButton_C_MouseClick(object sender, MouseEventArgs e)
        {
            if (radioButton_C.Checked)
            {
                tmpRemarkJobContentMent = Interaction.InputBox("กรุณาป้อนหมายเหตุ", "หมายเหตุ", tmpRemarkJobContentMent, MousePosition.X - 200, MousePosition.Y - 180);
            }
        }


        private void comboBox_PoitFail_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tmpJobType == "31")
                txtModel.Text = comboBox_PoitFail.Text.Trim();

            txtStartLite.Focus();
        }

        private void btClearData_Click(object sender, EventArgs e)
        {
            txtModel.Text = string.Empty;
            txtSNOld.Text = string.Empty;
            txtModel.Text = string.Empty;
            txtFailure.Text = string.Empty;
            txtSNNew.Text = string.Empty;
        }

        private void txtLocation_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                txtSNOld.Focus();
        }

        DateTime WarrantDate;
        private void txtSN_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Enter)
            //{
            //    //txtModel.Focus();
            //    Finding.Find_SN f = new SMSMINI.Transaction.Finding.Find_SN();
            //    f.Text = "ค้นหา S/N Old...";
            //    f.SN_Old = txtSNOld.Text;
            //    f.STAID = tmpSTAID;
            //    f.SN_New = null;
            //    f.lsSNSelec = lsSNSelect;
            //    if (f.ShowDialog() == DialogResult.OK)
            //    {

            //            txtSNOld.Text = f.SN_Old;
            //            txtFixAssetNoOld.Text = f.FANoOld;

            //            FCAT_ID = f.FCAT_ID;
            //            FBA_ID = f.FBA_ID;
            //            FMD_ID = f.FMD_ID;

            //            if (f.Location != "--" && !string.IsNullOrEmpty(f.Location))
            //                txtModel.Text = f.Location;



            //            if (f.MODEL != "--" && !string.IsNullOrEmpty(f.MODEL))
            //                txtModel.Text = f.MODEL;

            //            WarrantDate = f.WarrantDate;
            //            lblWarranty.Visible = true;

            //            if (WarrantDate > DateTime.Now)
            //            {
            //                lblWarranty.Text = "(อยู่ในประกัน)";
            //                if (f.CheckGauge > 0)
            //                {
            //                    MessageBox.Show("S/N [" + txtSNOld.Text + "] นี้" + Environment.NewLine +
            //                        "รับประกันค่าตรวจสอบมาตรวัด",
            //                        "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //                }
            //            }
            //            else
            //            {
            //                lblWarranty.Text = "(หมดประกัน)";
            //            }

            //            txtSNNew.Focus();



            //    }
            //    else
            //    {
            //        txtSNOld.Text = "None";                   
            //    }
            //}
        }
        private void txtSNNew_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Enter)
            //{
            //    if (txtSNOld.Text == string.Empty)
            //    {
            //        MessageBox.Show("กรุณาป้อนข้อมูล S/N Old ก่อน..." + Environment.NewLine +
            //        "เนื่องจาก ต้องเปลี่ยน S/N ที่อยู่ในประเภทอุปกรณ์เดียวกันเท่านั้น..." + Environment.NewLine +
            //        "กรุณาตรวจสอบ...", "ผลการตรวจสอบ...", MessageBoxButtons.OK);
            //        txtSNOld.Focus();
            //        return;
            //    }

            //    FindSNNew();
            //}
        }

        private void FindSNNew()
        {

            Finding.Find_SN f = new SMSMINI.Transaction.Finding.Find_SN();
            f.Text = "ค้นหา S/N New...";
            f.SN_Old = null;
            f.FCAT_ID = FCAT_ID;
            f.SN_New = txtSNNew.Text;//20141223 "";//20141202 txtSNNew.Text;
            f.lsSNSelec = lsSNSelect;
            if (f.ShowDialog() == DialogResult.OK)
            {
                //txtSNNew.Text = f.SN_New;
                //txtFixAsetNoNew.Text = f.FANoNew;
                //txtModel.Text = f.MODEL;
                //txtModel.Focus();

                //--20161117
                if (f.SN_New != "ไม่ระบุ...")
                {
                    txtSNNew.Text = f.SN_New;
                    txtFixAsetNoNew.Text = f.FANoNew;
                    txtModel.Text = f.MODEL;
                    vFCAT_ID_NEW = f.FCAT_ID_New; //20180426
                    txtModel.Focus();

                }
                else
                {
                    txtSNNew.Text = "ไม่ระบุ...";
                    txtFixAsetNoNew.Text = f.FANoNew;
                    txtModel.Text = f.MODEL;
                    vFCAT_ID_NEW = f.FCAT_ID_New; //20180426

                    txtModel.Focus();
                }
                //---20161117
            }
            else
            {
                //20210128 txtSNNew.Text = "None";
                txtSNNew.Text = "ไม่ระบุ...";

            }
        }

        private void txtModel_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) txtFailure.Focus();
        }

        private void txtStartLite_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) txtEndtLite.Focus();
        }

        private void txtEndtLite_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) btSelectPointFail.Focus();
        }


        private void txtPartQT_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {
                if (txtPartQT.Text == "0") return;
                txtPriceList.Focus();

                supTotalPrices();
            }
        }

        private void cob_TrnType_SelectionChangeCommitted(object sender, EventArgs e)
        {
            txtPartComment.Focus();
            try
            {
                if (txtWorkType.Text.Split(':')[0] == "OU" && cob_TrnType.SelectedValue.ToString() != "IN")
                {
                    closeProgress();
                    MessageBox.Show("ประเภทงาน: " + txtWorkType.Text.Split(':')[1] + Environment.NewLine +
                        "Part เก็บเงินทุกกรณี" + Environment.NewLine +
                        "TranType ต้องเป็น IN" + Environment.NewLine +
                        "กรุณาตรวจสอบ"
                        , "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    cob_TrnType.Focus();
                }
            }
            catch (Exception)
            {

            }
        }

        private void txtPartComment_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) radTranType0.Focus();
        }

        private void RadioButtonList_JobStatus2_Click(object sender, EventArgs e)
        {
            for (int i = 0; i <= dataGridView_Part.Rows.Count - 1; i++)
            {
                if ((dataGridView_Part["IsCancel", i].Value.ToString().Trim() == "0" && dataGridView_Part["สถานะ", i].Value.ToString().Trim() == "ค้าง") && RadioButtonList_JobStatus2.Checked == true)
                {
                    closeProgress();
                    MessageBox.Show("มีอะไหล่ค้าง ไม่สามารถเลือกบันทึกปิดงานได้", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    RadioButtonList_JobStatus2.Checked = false;
                    return;
                }



            }

            tabControl1.Enabled = true;

            #region Autoload Part

            AutoAddPart();

            #endregion


        }

        private void AutoAddPart()
        {

            #region เพิ่ม Part Auto ตามเงื่อนไขสัญญา

            if (RadioButtonList_JobStatus2.Checked == true)//บันทึกปิด งาน
            {

                AddPartAutoByConfig();
                PartBindind();
                CancelPartTravel();
            }

            #region TranType = WA, ให้ระบบ Pop Up เพื่อยืนยันการลบ Part TRAVEL, SERVICE3

            if (dataGridView_Part.RowCount > 0)
            {
                var workTypr = txtWorkType.Text.Split(':');
                string _workTypr = "";
                if (workTypr.Length > 1)
                    _workTypr = workTypr[0].ToString();

                int waCount = 0;
                int irow = 0;
                for (int i = 0; i <= dataGridView_Part.Rows.Count - 1; i++)
                {
                    irow++;
                    if (dataGridView_Part["Type", i].Value.ToString() == "WA" && _workTypr == "OU")
                    {
                        waCount++;
                    }

                    string item = dataGridView_Part["Part no.", i].Value.ToString();
                    if (item == "SERVICE3" || item == "TRAVEL")
                        irow--;
                }

                if ((waCount > 0 && irow > 0) && (waCount == irow))//ลบ SERVICE3 และ TRAVEL
                {
                    string item = dataGridView_Part["Part no.", 0].Value.ToString();

                RemoveItem:
                    if (item == "SERVICE3" || item == "TRAVEL")
                        deletePartTRANVELandSERVICE();

                    for (int i = 0; i <= dataGridView_Part.RowCount - 1; i++)
                    {
                        item = dataGridView_Part["Part no.", i].Value.ToString();
                        if (item == "SERVICE3" || item == "TRAVEL")
                            goto RemoveItem;
                    }
                }

            }

            #endregion

            #region ถ้า SN ในประกันทั้งหมด ให้ลบค่าบริการและค่าเดินทาง 20181026
            //20181022
            //==chk งานตู้ในประกัน ==
            if (dataGridView_FailPoint.RowCount > 0)
            {
                int chkSNWA = 0;
                int vDispenser = 0; //check ตู้จ่ายมีผลต่อทุก SN แต่ถ้าไม่มีตู้จ่ายใช้เงื่อนไขเดิม
                for (int a = 0; a <= dataGridView_FailPoint.Rows.Count - 1; a++)
                {

                    if (dataGridView_FailPoint["FCAT_CH", a].Value.ToString() == "1")
                    {
                        vDispenser++;
                        string itemSN = dataGridView_FailPoint["S/N(Old)", a].Value.ToString();
                        using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext(strConn))
                        {
                            var q = dc.FFixedAsset_Station_Details
                                .Where(t => t.SerialNumber == itemSN && (t.WarrantDate > DateTime.Now.Date))//warrantdate == null ถือว่าหมดประกัน
                                    .FirstOrDefault();
                            if (q == null)
                            {
                                chkSNWA++; //หมดประกัน
                            }
                        }
                    }
                }

                //====
                //if (chkSNWA > 0)
                //if (chkSNWA == 0)//อยู่ในประกัน (มีประกัน)
                if (chkSNWA == 0 && vDispenser > 0) //check ตู้จ่ายมีผลต่อทุก SN แต่ถ้าไม่มีตู้จ่ายใช้เงื่อนไขเดิม
                {
                    if (dataGridView_Part.RowCount > 0)
                    {

                    RemoveItemWA:
                        int irowchk = 0;
                        for (int i = 0; i <= dataGridView_Part.Rows.Count - 1; i++)
                        {

                            string item = dataGridView_Part["Part no.", i].Value.ToString();
                            if (item == "20SERD02LC" || item == "20SERA01LC")
                                irowchk++;
                        }
                        if (irowchk > 0)//ลบค่าบริการ และ ค่าเดินทาง
                        {
                            deletePartTRANVELandSERVICE_WA();
                            goto RemoveItemWA;
                        }

                    }

                    #endregion

                }//chkSNWA==0
            } //failPoint
            //=====
            #endregion

        }

        private void RadioButtonList_JobStatus1_Click(object sender, EventArgs e)
        {
            tabControl1.Enabled = true;
        }


        private void txtPartQT_TextChanged(object sender, EventArgs e)
        {

        }

        private void partDiscount()
        {
            if (txtPartQT.Text == "" || txtPartQT.Text == "0") return;
            if (txtPriceList.Text == "" || txtPriceList.Text == "0") return;

            double discount = double.Parse(txtDiscQuarter.Text);
            int quantity = txtPartQT.Text == "" ? 1 : int.Parse(txtPartQT.Text);
            double pricelist = double.Parse(txtPriceList.Text);

            //txtVAT
            double vat = double.Parse(txtVAT.Text);

            if (discount <= 0)//ไม่คิดส่วนลด
            {
                if (vat > 0)//คิด VAT 
                {
                    if (tmpRounup > 0)
                        txtPriceUnit.Text = Math.Round(pricelist * quantity, tmpRounup).ToString("#,##0.00");
                    else
                        txtPriceUnit.Text = (pricelist * quantity).ToString("#,##0.00");
                }
                else
                {
                    if (tmpRounup > 0)
                        txtPriceUnit.Text = Math.Round(pricelist * quantity, tmpRounup).ToString("#,##0.00");
                    else
                        txtPriceUnit.Text = (pricelist * quantity).ToString("#,##0.00");
                }
            }
            else
            {
                //คิดส่วนลด
                double dis = (discount / 100) * pricelist;
                //txtPriceUnit.Text = (tmpRounup==0)?(((pricelist - dis) * quantity)).ToString("#,##0.00"): Math.Round(((pricelist - dis) * quantity), tmpRounup).ToString("#,##0.00");
                if (tmpRounup > 0)
                    txtPriceUnit.Text = Math.Round(((pricelist - dis) * quantity), tmpRounup).ToString("#,##0.00");
                else
                    txtPriceUnit.Text = ((pricelist - dis) * quantity).ToString("#,##0.00");
            }
        }

        private void supTotalPrices()
        {
            double _discount = double.Parse(txtDiscQuarter.Text == "" ? "0" : txtDiscQuarter.Text);
            double _prices = double.Parse(txtPriceList.Text == "0.00" ? "0" : txtPriceList.Text);
            double quantity = double.Parse(txtPartQT.Text == "" ? "0" : txtPartQT.Text);

            if (_discount <= 0)//ไม่คิดส่วนลด
            {
                if (string.IsNullOrEmpty(txtVAT.Text) || txtVAT.Text == "0.00")//ไม่คิด VAT
                {
                    //txtPriceUnit.Text = Math.Round(_prices * quantity, tmpRounup).ToString("#,##0.00");
                    if (tmpRounup > 0)
                        txtPriceUnit.Text = Math.Round(_prices * quantity, tmpRounup).ToString("#,##0.00");
                    else
                        txtPriceUnit.Text = (_prices * quantity).ToString("#,##0.00");
                }
                else//คิด VAT
                {
                    double _total = (_prices * quantity);
                    double _tvat = Math.Round((Convert.ToDouble(txtVAT.Text) / 100) * _total, 2);

                    if (tmpRounup > 0)
                        txtPriceUnit.Text = Math.Round((_total + _tvat), tmpRounup).ToString("#,##0.00");
                    else
                        txtPriceUnit.Text = (_total + _tvat).ToString("#,##0.00");
                }

            }
            else//คิดส่วนลด
            {
                if (string.IsNullOrEmpty(txtVAT.Text) || txtVAT.Text == "0.00")//ไม่คิด VAT
                {
                    if (tmpRounup > 0)
                        txtPriceUnit.Text = Math.Round((_prices - ((_discount / 100) * _prices)) * quantity, tmpRounup).ToString("#,##0.00");
                    else
                        txtPriceUnit.Text = ((_prices - ((_discount / 100) * _prices)) * quantity).ToString("#,##0.00");

                }
                else//คิด VAT
                {
                    double _total = ((_prices - ((_discount / 100) * _prices)) * quantity);
                    double _tvat = Math.Round((Convert.ToDouble(txtVAT.Text) / 100) * _total, 2);

                    if (tmpRounup > 0)
                        txtPriceUnit.Text = Math.Round((_total + _tvat), tmpRounup).ToString("#,##0.00");
                    else
                        txtPriceUnit.Text = (_total + _tvat).ToString("#,##0.00");
                }
            }
        }


        private void txtVAT_TextChanged(object sender, EventArgs e)
        {
            sumVAT();
        }

        private void sumVAT()
        {
            if (txtVAT.Text == "")
                txtVAT.Text = "0";
            supTotalPrices();
        }

        private void txtPriceList_TextChanged(object sender, EventArgs e)
        {

            if (txtPriceList.Text == "")
                txtPriceList.Text = "0.00";




            try
            {
                txtPriceList.Text = double.Parse(txtPriceList.Text).ToString("#,##0.00");
                txtPriceList.Select(txtPriceList.Text.Length - 3, 0);
            }
            catch (Exception)
            {
                closeProgress();
                MessageBox.Show("ป้อนเฉพาะตัวเลข กรุณาตรวจสอบ", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtPriceList.SelectAll();
                txtPriceList.Focus();

                return;
            }


            supTotalPrices();
        }

        private void dataGridView_FailPoint_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {

            //if (e.ColumnIndex == 9)
            //    dataGridView_FailPoint[9, e.RowIndex].Value =
            //        Convert.ToDouble(dataGridView_FailPoint[9, e.RowIndex].Value).ToString("#,##0.00");
            //if (e.ColumnIndex == 10)
            //    dataGridView_FailPoint[10, e.RowIndex].Value =
            //        Convert.ToDouble(dataGridView_FailPoint[10, e.RowIndex].Value).ToString("#,##0.00");


            if (e.ColumnIndex == 15)//StartLiter
                dataGridView_FailPoint[15, e.RowIndex].Value =
                    Convert.ToDouble(dataGridView_FailPoint[15, e.RowIndex].Value).ToString("#,##0.00");

            if (e.ColumnIndex == 16)//EndLiter
                dataGridView_FailPoint[16, e.RowIndex].Value =
                    Convert.ToDouble(dataGridView_FailPoint[16, e.RowIndex].Value).ToString("#,##0.00");




            //if (e.ColumnIndex == 10)// Cell "EndLiter"
            if (e.ColumnIndex == 16)// Cell "EndLiter"
            {

                if (Convert.ToDouble(dataGridView_FailPoint["StartLiter",
                    e.RowIndex].Value.ToString()) > Convert.ToDouble(dataGridView_FailPoint["EndLiter",
                    e.RowIndex].Value.ToString()))
                {
                    closeProgress();
                    MessageBox.Show("เลขรวมลิตร: หลังทดสอบ ต้องมากกว่า ก่อนทดสอบ", "ผลการตรวจสอบ", MessageBoxButtons.OK);
                    return;
                }

                if (Convert.ToDouble(dataGridView_FailPoint["StartLiter", e.RowIndex].Value.ToString()) > 0 && Convert.ToDouble(dataGridView_FailPoint["EndLiter", e.RowIndex].Value.ToString()) > 0)
                    dataGridView_FailPoint["LiterTest", e.RowIndex].Value =
                        (Convert.ToDouble(dataGridView_FailPoint["EndLiter", e.RowIndex].Value.ToString()) -
                        Convert.ToDouble(dataGridView_FailPoint["StartLiter", e.RowIndex].Value.ToString())).ToString("#,##0.00");
            }
        }

        private void toolStripButton_TAGPrimt_Click(object sender, EventArgs e)
        {

            try
            {
                if (UserInfo.IsSVPrintOnline == 1)//20210817
                {
                    var xuser = new xUserInfo();

                    xuser.UserId = UserInfo.UserId;
                    xuser.UserName = UserInfo.UserName;
                    xuser.Password = UserInfo.Password;
                    xuser.UserLevID = UserInfo.UserLevID;
                    xuser.UserLevel = UserInfo.UserLevel;
                    xuser.FullName = UserInfo.FullName;
                    xuser.Van_ID = UserInfo.Van_ID;
                    xuser.SUP_VAN = UserInfo.SUP_VAN;
                    xuser.Email = UserInfo.Email;

                    xuser.Van_Type = UserInfo.Van_Type;
                    xuser.VehicleRegis = UserInfo.VehicleRegis;


                    xuser.ConnectMode = UserInfo.ConnectMode;

                    xuser.eDepID = UserInfo.eDepID;
                    xuser.IsSVPrintOnline = UserInfo.IsSVPrintOnline;

                    var json = JsonConvert.SerializeObject(xuser);


                    //http://sms.flowco.co.th/smssvprint/

                    //var mainUrl = "http://localhost:1069/";
                    var mainUrl = "http://sms.flowco.co.th/smssvprint/";
                    var Tagprint = mainUrl + "TAGPrint.aspx?JOB_ID=" + tmpjobID.Trim() + "&EndDate=" + stmpEndDate + "&EndTime=" + stmpEndTime + "&chkUpload=" + chkUpload + "&xUserInfo=" + json + "";
                    System.Diagnostics.Process.Start(Tagprint);
                }
                else
                {
                    Cursor.Current = Cursors.AppStarting;
                    //this.Hide();
                    ReturnPartsRoutingTagReport f = new ReturnPartsRoutingTagReport();
                    f.TmpJobID = tmpjobID.Trim();
                    if (f.ShowDialog() == DialogResult.Abort)
                        this.Show();
                    PartCount = 0;// re set การนับจำนวนอะไหล่
                    Cursor.Current = Cursors.Default;
                }
            }
            catch (System.Exception ex)
            {
                closeProgress();
                MessageBox.Show("ข้อผิดพลาด " + ex.Message, "เกิดข้อผิดพลาด");
            }

        }

        private void btSelectDateOK_Click(object sender, EventArgs e)
        {
            DateTime dt1 = Convert.ToDateTime(cobEndDate.SelectedItem.ToString());

            tmpEndDate = dt1.ToString("yyyyMMdd", new System.Globalization.CultureInfo("en-US"));
            tmpEndTime = dt1.ToString("HH:mm:ss", new System.Globalization.CultureInfo("en-US"));

            //20191224
            stmpEndDate = dt1.ToString("yyyyMMdd", new System.Globalization.CultureInfo("en-US"));
            stmpEndTime = dt1.ToString("HH:mm:ss", new System.Globalization.CultureInfo("en-US"));

            panel_selectEndDate.Visible = false;

            //เปลี่ยนเป็น check ห้ามพิมพ์ซ้ำ//===20200519====
            //using (DAL.SMSManage.SMSManageDataContext dc = new DAL.SMSManage.SMSManageDataContext(strConn))
            //{
            //    var iLAuthPrint = (from j in dc.JOBs
            //                    join jd in dc.JOB_Details on j.JOB_ID equals jd.JOB_ID
            //                    //where new string[] { "07", "99" }.Contains(jd.JOBS_ID)
            //                    where new string[] { "07", "99" }.Contains(jd.JOBS_ID) && j.JOB_ID == txtJobID.Text.Trim()
            //                    select jd).OrderByDescending(c => c.EndDate).First();

            //    if (iLAuthPrint != null)
            //    {
            //        if (iLAuthPrint.EndDate != null)
            //        {
            //            DateTime dtEndAuthPrint = Convert.ToDateTime(iLAuthPrint.EndDate.Value.ToString("yyyy/MM/dd HH:mm:ss"));

            //            //====20200519==เงื่อนไข auth printSV 
            //            if (!new string[] { "09", "12", "13" }.Contains(UserInfo.UserLevID))
            //            {

            //                if (dt1 < dtEndAuthPrint)//v.1.0.21.2020 //ทีแรกเข้าใจว่า check แค่วันที่  โดย ช่าง พิมพ์รายงาน Service Report ของ  วันเวลาปิดงานปัจจุบัน ซ้ำกี่ครั้งก็ได้ แต่ไม่สามารถพิมพ์SV ของการปิดงานก่อนหน้า ได้
            //                //if (dt1 <= dtEndAuthPrint  && iLAuthPrint.IsPrintSV == true )//v.1.0.20.2020
            //                {
            //                    MessageBox.Show("สามารถสั่งพิมพ์รายงานย้อนหลังได้ เฉพาะ สิทธิ์ผู้จัดการเท่านั้น ", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //                     if (editMode == "job_edit")
            //                {
            //                    stmpEndDate = "";
            //                    stmpEndTime = "";
            //                }
            //                    return;
            //                }

            //            }
            //        }
            //        else
            //        {
            //            MessageBox.Show("ข้อมูลการปิดงานไม่สมบูรณ์ รบกวนตรวจสอบ ", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //            return;

            //        }
            //    }
            //}

            //=20200521 รายการปิดงานที่ เคยพิมพ์ไปแล้ว พิมพ์ซ้ำอีกไม่ได้===
            using (DAL.SMSManage.SMSManageDataContext dc = new DAL.SMSManage.SMSManageDataContext(strConn))
            {
                DateTime dtCobEndAuthPrint = Convert.ToDateTime(dt1.ToString("yyyy/MM/dd HH:mm:ss"));
                var iLAuthPrint = (from j in dc.JOBs
                                   join jd in dc.JOB_Details on j.JOB_ID equals jd.JOB_ID
                                   where new string[] { "07", "99" }.Contains(jd.JOBS_ID)
                                   && j.JOB_ID == txtJobID.Text.Trim()
                                   && jd.EndDate.Value.Date == dtCobEndAuthPrint.Date
                                   && jd.EndDate.Value.Hour == dtCobEndAuthPrint.Hour
                                   && jd.EndDate.Value.Minute == dtCobEndAuthPrint.Minute
                                   && jd.EndDate.Value.Second == dtCobEndAuthPrint.Second
                                   && jd.StatusDetail == true //20201208
                                   select jd).First();

                if (iLAuthPrint != null)
                {
                    if (iLAuthPrint.EndDate != null)
                    {
                        //====20200519==เงื่อนไข auth printSV 
                        if (!new string[] { "09", "12", "13" }.Contains(UserInfo.UserLevID))
                        {
                            if (iLAuthPrint.IsPrintSV == true)
                            {
                                closeProgress();
                                MessageBox.Show("สามารถสั่งพิมพ์รายงานย้อนหลังได้ เฉพาะ สิทธิ์ผู้จัดการเท่านั้น ", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                if (editMode == "job_edit")
                                {
                                    stmpEndDate = "";
                                    stmpEndTime = "";
                                }
                                return;
                            }

                        }
                    }
                    else
                    {
                        closeProgress();
                        MessageBox.Show("ข้อมูลการปิดงานไม่สมบูรณ์ รบกวนตรวจสอบ ", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;

                    }
                }
            }


            //====

            //===============
        }

        private void txtPriceList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtPriceList.Text == "0") return;
                txtVAT.Focus();
            }
        }

        private void txtPartQT_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < "0"[0] || e.KeyChar > "9"[0]) && e.KeyChar != ControlChars.Back && e.KeyChar != "."[0])
                e.Handled = true;
        }

        private void txtPriceList_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < "0"[0] || e.KeyChar > "9"[0]) && e.KeyChar != ControlChars.Back && e.KeyChar != "."[0])
                e.Handled = true;

        }

        private void txtVAT_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < "0"[0] || e.KeyChar > "9"[0]) && e.KeyChar != ControlChars.Back && e.KeyChar != "."[0])
                e.Handled = true;

        }

        private void txtDiscQuarter_TextChanged(object sender, EventArgs e)
        {
            if (txtDiscQuarter.Text == "")
                txtDiscQuarter.Text = "0.00";
            if (decimal.Parse(txtDiscQuarter.Text) > 100)
            {
                closeProgress();
                MessageBox.Show("ห้ามป้อนส่วนลดเกิน 100", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtDiscQuarter.SelectAll();
                txtDiscQuarter.Focus();
                return;
            }

            supTotalPrices();
        }

        private void chIsPrices_Click(object sender, EventArgs e)
        {
            if (chIsPrices.Checked)
            {
                txtVAT.Text = "7.00";
                con_ERP_StationCharge.SelectedIndex = con_ERP_StationCharge.FindString("สถานี");
                con_ERP_StationCharge.Enabled = false;
            }
            else
            {
                txtVAT.Text = "0.00";
                con_ERP_StationCharge.Enabled = true;
            }
        }

        private void dataGridView_Part_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {

            decimal _qty = Convert.ToDecimal(dataGridView_Part.Rows[e.RowIndex].Cells["จำนวน"].Value);
            decimal _PricesList = Convert.ToDecimal(dataGridView_Part.Rows[e.RowIndex].Cells["PricesList"].Value);
            decimal _priUnit = Convert.ToDecimal(dataGridView_Part.Rows[e.RowIndex].Cells["ราคา/หน่วย"].Value);
            decimal _sumPri = Convert.ToDecimal(dataGridView_Part.Rows[e.RowIndex].Cells["รวม"].Value);
            decimal _sumVat = Convert.ToDecimal(dataGridView_Part.Rows[e.RowIndex].Cells["ภาษี"].Value);
            decimal _sumTotal = Convert.ToDecimal(dataGridView_Part.Rows[e.RowIndex].Cells["รวมสุทธิ"].Value);
            decimal _discount = Convert.ToDecimal(dataGridView_Part.Rows[e.RowIndex].Cells["Discount"].Value);
            decimal _vat = Convert.ToDecimal(dataGridView_Part.Rows[e.RowIndex].Cells["VAT"].Value);


            decimal _priUnit2 = (_PricesList - (_PricesList * (_discount / 100)));
            decimal _sumPri2 = ((_PricesList - (_PricesList * (_discount / 100))) * _qty);
            decimal _sumVat2 = _sumPri2 * (_vat / 100);

            dataGridView_Part.Rows[e.RowIndex].Cells["จำนวน"].Value = _qty;
            dataGridView_Part.Rows[e.RowIndex].Cells["PricesList"].Value = _PricesList.ToString("#,##0.00");
            if (tmpRounup > 0)
                dataGridView_Part.Rows[e.RowIndex].Cells["ราคา/หน่วย"].Value = Math.Round(_priUnit2, this.tmpRounup).ToString("#,##0.00");
            else
                dataGridView_Part.Rows[e.RowIndex].Cells["ราคา/หน่วย"].Value = (_priUnit2).ToString("#,##0.00");

            dataGridView_Part.Rows[e.RowIndex].Cells["รวม"].Value = _sumPri2.ToString("#,##0.00");

            dataGridView_Part.Rows[e.RowIndex].Cells["ภาษี"].Value = Math.Round(_sumVat2, 2).ToString("#,##0.00");


            dataGridView_Part.Rows[e.RowIndex].Cells["รวมสุทธิ"].Value = (_sumPri2 + _sumVat2).ToString("#,##0.00");

            dataGridView_Part.Rows[e.RowIndex].Cells["Discount"].Value = _discount.ToString("#,##0.00");
            dataGridView_Part.Rows[e.RowIndex].Cells["VAT"].Value = _vat.ToString("#,##0.00");

            dataGridView_Part.Rows[e.RowIndex].ErrorText = String.Empty;

        }

        private void dataGridView_Part_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            dataGridView_Part.Rows[e.RowIndex].ErrorText = "";

            int newInteger;
            if (dataGridView_Part.Rows[e.RowIndex].IsNewRow)
                return;

            try
            {
                if (new string[] { "จำนวน" }.Contains(dataGridView_Part.Columns[e.ColumnIndex].Name))
                {
                    if (!int.TryParse(Convert.ToInt32(e.FormattedValue.ToString().Replace(".", "").Replace(",", "")).ToString(), out newInteger) || newInteger < 0)//newInteger < 1)
                    {
                        closeProgress();
                        MessageBox.Show("กรุณาป้อนเฉพาะตัวเลข", "ผลหารตรวจสอบ");
                        dataGridView_Part.Rows[e.RowIndex].ErrorText = "กรุณาป้อนเฉพาะตัวเลข";
                        e.Cancel = true;
                    }

                }
                else if (new string[] { "Discount", "VAT", "PricesList" }.Contains(dataGridView_Part.Columns[e.ColumnIndex].Name))
                {
                    if (!int.TryParse(Convert.ToInt32(e.FormattedValue.ToString().Replace(".", "").Replace(",", "")).ToString(), out newInteger) || newInteger < 0)
                    {
                        closeProgress();
                        MessageBox.Show("กรุณาป้อนเฉพาะตัวเลข", "ผลหารตรวจสอบ");
                        dataGridView_Part.Rows[e.RowIndex].ErrorText = "กรุณาป้อนเฉพาะตัวเลข";
                        e.Cancel = true;
                    }
                }
            }
            catch (System.Exception)
            {

                dataGridView_Part.Rows[e.RowIndex].ErrorText = "กรุณาป้อนเฉพาะตัวเลข";
                e.Cancel = true;
            }
        }


        private void showUpdatePriceDiscount()
        {
            //try
            //{
            //    Cursor.Current = Cursors.AppStarting;
            //    this.Hide();
            //    MasterData.frmUpdatePartPriceList f = new SMSMINI.MasterData.frmUpdatePartPriceList();
            //    f.StartPosition = FormStartPosition.CenterScreen;
            //    //f.toolStripLabel_cobby.SelectedIndex = 3;
            //    f.toolStripComboBox_PriceDiscount.SelectedIndex = 0;

            //    f.toolStripButton_FindStation.Enabled = false;
            //    f.toolStripLabel_cobby.Enabled = false;
            //    f.toolStripComboBox_PriceDiscount.Enabled = false;
            //    f.toolStripText_txtFind.Enabled = false;
            //    f.toolStripButton_FindStation_Click(null, null);

            //    if (f.ShowDialog() == DialogResult.Abort)
            //        this.Close();

            //    Cursor.Current = Cursors.Default;
            //}
            //catch (System.Exception ex)
            //{
            //    closeProgress();
            //    MessageBox.Show("เกิดผิดพลาดในการ Update PriceList/ส่วนลดตามไตรมาส" + ex.Message, "ข้อผิดพลาด");
            //}
        }

        private void txtSNNew_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void btJobContentMent_Click(object sender, EventArgs e)
        {
            //==คำสั่งยกลิกการประเมิน 20200508
            //toolStripButton_Save.Enabled = false;
            //JobClose_JobContentMent f = new JobClose_JobContentMent();
            //f.JOBID = this.txtJobID.Text.Trim();
            //if (f.ShowDialog() == DialogResult.OK)
            //{
            //    this.btJobContentMent.ForeColor = System.Drawing.Color.Green;
            //    this.btJobContentMent.Image = global::SMSMINI.Properties.Resources.pass;
            //    this.btJobContentMent.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;

            //    this.Close();

            //}
            //===

        }

        private void btFTRPrint_Click(object sender, EventArgs e)
        {
            this.Hide();

            FixAssetTFR.ViewFixAssetTFR f = new SMSMINI.Transaction.FixAssetTFR.ViewFixAssetTFR();
            f.TMPJobID = tmpjobID.Trim();
            f.WindowState = FormWindowState.Maximized;

            if (f.ShowDialog() == DialogResult.Abort)
                this.Show();
        }

        private void dataGridView_FailPoint_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dataGridView_FailPoint.CurrentCell is DataGridViewCheckBoxCell)
                dataGridView_FailPoint.CommitEdit(DataGridViewDataErrorContexts.Commit);
        }

        private void dataGridView_Part_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dataGridView_Part.CurrentCell is DataGridViewCheckBoxCell)
                dataGridView_Part.CommitEdit(DataGridViewDataErrorContexts.Commit);
        }

        private void txtContract_TextChanged(object sender, EventArgs e)
        {

        }

        private void cbIsCustomer_CheckedChanged(object sender, EventArgs e)
        {
            if (cbIsCustomer.Checked == true)
            {
                cob_TrnType.SelectedValue = "CO";
            }
        }

        private void btLoadSNNew_Click(object sender, EventArgs e)
        {
            //txtSNNew
            try
            {
                Cursor.Current = Cursors.AppStarting;
                var f = new SMSMINI.MasterData.FixedAsset_SerialNumber_Master();
                f.FFSN = txtSNNew.Text.Trim();

                if (f.ShowDialog() == DialogResult.OK)
                {
                    Cursor.Current = Cursors.Default;
                    FindSNNew();
                }
            }
            catch (System.Exception ex)
            {
                closeProgress();
                MessageBox.Show("เกิดผิดพลาดในการใช้งานเมนู ข้อมูล Fixed Asset SerialNumber Master " + Environment.NewLine
                    + ex.Message, "ข้อผิดพลาด");
                this.Show();
            }

        }

        private void btContact_Detail_Click(object sender, EventArgs e)
        {
            PopupConditions_Detail(txtJobID.Text);

            //if (IsInternetConnection())
            //{
            //    PopupConditions_Detail(txtJobID.Text);
            //}
            //else
            //{
            //    MessageBox.Show("กรุณาเชื่อมต่อ อินเตอร์เน็ต...","ผลการตรวจสอบ",
            //        MessageBoxButtons.OK,MessageBoxIcon.Information);
            //}
        }


        private void PopupConditions_Detail(string JOBID)
        {
            try
            {
                strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;

                using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn))
                {

                    var q = (from t in dc.JOBs
                             from s in dc.Stations
                             from v in dc.vw_mini_pop_Conf_Conditions_Details
                             where t.JOB_ID == JOBID &&
                             //(v.PopPage.Value == "1"[0] || v.PopPage.Value == "3"[0]) &&
                             v.GOP_ID == s.GOP_ID &&
                             v.TYP_ID == t.TYP_ID &&
                             v.TYP_ID1 == t.TYP_ID1 &&
                             (t.STA_ID == s.STA_ID)

                             select v).ToList();


                    if (q.Count() > 0)
                    {
                        var f = new Transaction.PopupConditions_Detail();
                        f.lsPopupC = q;
                        f.ShowDialog();
                    }
                    else
                    {
                        closeProgress();
                        MessageBox.Show("ไม่มีรายละเอียด เงื่อนไขสัญญา...", "ผลการตรวจสอบ",
                         MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }

            }
            catch (Exception)
            {
                closeProgress();
                MessageBox.Show("กรุณาเชื่อมต่อ อินเตอร์เน็ต...", "ผลการตรวจสอบ",
                     MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private bool IsInternetConnection()
        {
            using (Ping myPing = new Ping())
            {
                String host = "www.google.com";
                byte[] buffer = new byte[32];
                int timeout = 1000;
                PingOptions pingOptions = new PingOptions();
                PingReply reply = myPing.Send(host, timeout, buffer, pingOptions);

                if (reply.Status == IPStatus.Success)
                {
                    return true;
                }
                else
                    return false;

            }
        }

        private void pUpSNOld_Click(object sender, EventArgs e)
        {

            if (cbxChFCAT.SelectedIndex == -1)
            {
                closeProgress();
                cbxChFCAT.Focus();
                MessageBox.Show("กรุณาเลือก ประเภทอุปกรณ์", "ผลการตรวจสอบ");
                return;
            }


            //txtModel.Focus();
            Finding.Find_SN f = new SMSMINI.Transaction.Finding.Find_SN();
            f.Text = "ค้นหา S/N Old...";
            f.SN_Old = txtSNOld.Text;//20141223"";//20141202 txtSNOld.Text;
            f.STAID = tmpSTAID;
            f.SN_New = null;
            f.lsSNSelec = lsSNSelect;
            f.chFCAT_ID = int.Parse(cbxChFCAT.SelectedValue.ToString()); //20180801
            if (f.ShowDialog() == DialogResult.OK)
            {

                if (f.SN_Old != "ไม่ระบุ...")
                {
                    txtSNOld.Text = f.SN_Old;
                    txtFixAssetNoOld.Text = f.FANoOld;

                    FCAT_ID = f.FCAT_ID;
                    FBA_ID = f.FBA_ID;
                    FMD_ID = f.FMD_ID;

                    LOC_ID = f.LOC_ID;

                    cbxLocation.SelectedValue = LOC_ID;// f.LOC_ID;

                    if (f.MODEL != "--" && !string.IsNullOrEmpty(f.MODEL))
                        txtModel.Text = f.MODEL;

                    WarrantDate = f.WarrantDate;
                    lblWarranty.Visible = true;

                    txtSNNew.Text = f.SN_Old; //20141223 default S/N old ใน new

                    IsFound = f._IsFound;
                    if (IsFound == true)
                    {
                        cbxLocation.Enabled = false;
                    }/*else{
                        cbxLocation.Enabled = true;
                    }*/

                    if (WarrantDate > DateTime.Now)
                    {
                        lblWarranty.Text = "(อยู่ในประกัน)";
                        if (f.CheckGauge > 0)
                        {
                            closeProgress();
                            MessageBox.Show("S/N [" + txtSNOld.Text + "] นี้" + Environment.NewLine +
                                "รับประกันค่าตรวจสอบมาตรวัด",
                                "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    else
                    {
                        lblWarranty.Text = "(หมดประกัน)";
                    }

                    //---20180426--บังคับเลือกจากระบบ ?
                    if (FCAT_ID != 0)
                    {
                        using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn))
                        {
                            if (int.Parse(cbxChFCAT.SelectedValue.ToString()) != FCAT_ID)
                            {
                                closeProgress();
                                MessageBox.Show(" ประเภทอุปกรณ์ที่เลือก ไม่ตรงกับ ประเภทอุปกรณ์ของ SN ",
                                               "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                txtSNOld.Focus();
                                return;
                            }

                        }

                        setcob_PointFailure(f.FCAT_ID);
                    }


                }
                else
                {
                    txtSNOld.Text = "ไม่ระบุ...";
                    cbxLocation.Enabled = true;

                    ////20150114 ส่งค่ากลับ
                    FCAT_ID = 0;
                    FBA_ID = 0;
                    FMD_ID = 0;

                    //====20161117===
                    txtSNNew.Text = "ไม่ระบุ...";
                    txtFixAsetNoNew.Text = null;
                    txtModel.Text = null;
                    //====20161117===
                }
                txtSNNew.Focus();




            }
            else
            {
                FCAT_ID = 0;
                FBA_ID = 0;
                FMD_ID = 0;

                //20210128 txtSNOld.Text = "None";
                txtSNOld.Text = "ไม่ระบุ...";

                cbxLocation.Enabled = true;

            }
        }

        private void pUpSNNew_Click(object sender, EventArgs e)
        {
            if (txtSNOld.Text == string.Empty)
            {
                closeProgress();
                MessageBox.Show("กรุณาป้อนข้อมูล S/N Old ก่อน..." + Environment.NewLine +
                "เนื่องจาก ต้องเปลี่ยน S/N ที่อยู่ในประเภทอุปกรณ์เดียวกันเท่านั้น..." + Environment.NewLine +
                "กรุณาตรวจสอบ...", "ผลการตรวจสอบ...", MessageBoxButtons.OK);
                txtSNOld.Focus();
                return;
            }

            if (txtSNNew.Text == string.Empty)
            {
                closeProgress();
                MessageBox.Show("กรุณาป้อนข้อมูล S/N New ก่อน..." + Environment.NewLine +
                   "เนื่องจาก ต้องเปลี่ยน S/N ที่อยู่ในประเภทอุปกรณ์เดียวกันเท่านั้น..." + Environment.NewLine +
                   "กรุณาตรวจสอบ...", "ผลการตรวจสอบ...", MessageBoxButtons.OK);
                txtSNNew.Focus();
                return;
            }



            FindSNNew();
        }


        //20150626
        private void chkEnableSVReport(string jobID)
        {


            if (editMode == "job_edit")
                strConn = GetConnection.Connection;
            else
                strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;




            using (DAL.SMSManage.SMSManageDataContext dc = new DAL.SMSManage.SMSManageDataContext(strConn))
            {
                var j = dc.JOBs
                    .Where(t => t.JOB_ID == jobID && new string[] { "99", "07" }.Contains(t.JOBS_ID))//t.UploadDate != null)//t.JOBS_ID == "99")
                    .FirstOrDefault();

                if (j != null)
                {
                    toolStripButton_Print.Enabled = true;
                    //---20180423----
                    var jchkU = dc.JOBs
                    .Where(t => t.JOB_ID == jobID && new string[] { "99", "07" }.Contains(t.JOBS_ID))//t.UploadDate  != null )//t.JOBS_ID == "99")
                    .FirstOrDefault();

                    if (jchkU != null)
                    {
                        chkUpload = "1";
                    }
                    else
                    {
                        chkUpload = "0";
                    }
                    //-------------------
                }
                else
                {
                    //20200129 toolStripButton_Print.Enabled = false;

                    if (editMode == "job_edit")
                    {
                        toolStripButton_Print.Enabled = true;
                    }

                    else
                    {
                        toolStripButton_Print.Enabled = false;
                    }
                }
            }

        }

        private void checkBox_SMS_BAK_CheckedChanged(object sender, EventArgs e)
        {

        }

        //-==20190731
        private bool check_job_type_trantype(string tmpSN, string partNo)
        {
            bool isSNWarranty = false;
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSManageDataContext(strConn))
            {

                //0 ตรวจสอบสัญญา
                var dnow = DateTime.Today.Date;


                //1... 
                //Check ตู้จ่ายอยู่ในประกัน
                var ffa = from t in dc.FFixedAsset_Station_Details
                          where t.STA_ID == tmpSTAID &&
                                t.SerialNumber == tmpSN &&
                                t.WarrantDate.Value > DateTime.Now.Date
                          select t;

                if (ffa.Count() > 0)//<==| ถ้าตู้จ่ายอยู่ในประกัน
                {
                    isSNWarranty = true;
                }
            }//dal
            return isSNWarranty;
        }
        //=====        

        //20200212

        DAL.SMSManage.JOB _jobU = null;
        DAL.SMSManage.Station _station = null;
        DAL.SMSManage.JOB_WorkID _wid = null;
        DAL.SMSManage.JOB_Detail_Spare _jobd = null;

        DAL.SMSManage.Conf_Upload_JOB_Photo_Report _conf_jphot = null;
        List<DAL.SMSManage.JOB_PhotoReport_Detail> lsJSP = null;

        List<DAL.SMSManage.JOB_PhotoReport_Detail> lsJSPComp = null;

        List<string> jobListPart;
        List<string> LConfNoPart;

        //==20201022
        List<string> LConfTRA;
        //=========

        private bool checkJOBPhotoUpload()
        {
            bool IsPartIN = false;

            try
            {

                string strMaxPrice = "";
                decimal? maxPrice = 0;
                string _JOBId = "";

                lsJSP = new List<SMSMINI.DAL.SMSManage.JOB_PhotoReport_Detail>();
                lsJSPComp = new List<SMSMINI.DAL.SMSManage.JOB_PhotoReport_Detail>();


                strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;




                using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn))
                {
                    //====20200707==ยกเลิกบังคับ upload รูปตาม config ==
                    var qNoUpL = dc.Conf_Upload_JOB_Photo_Report_NoDEPs
                                 .Where(t => t.DEP_ID == UserInfo.eDepID).FirstOrDefault();
                    if (qNoUpL != null)
                    {
                        return false;
                    }

                    //============================================

                    var qitem = dc.JOBs
                        .Where(t => t.EMP_ID3 == UserInfo.Van_ID &&
                                    //20200212 (t.IsUpload == "0"[0] || t.IsUpload == null) &&
                                    new string[] { "07", "99" }.Contains(t.JOBS_ID)
                                 //online === 20200224 ===
                                 && t.JOB_ID == tmpjobID.Trim()
                        //=================
                                )
                        .FirstOrDefault();
                    if (qitem != null)//มี JOB พร้อม Upload
                    {
                        //==== 20200226
                        string yyyy = stmpEndDate.Substring(0, 4).ToString();
                        string mm = stmpEndDate.Substring(4, 2).ToString();
                        string dd = stmpEndDate.Substring(6, 2).ToString();
                        string dtp = yyyy + "/" + mm + "/" + dd;
                        string b = DateTime.Parse(stmpEndTime).ToString("HH:mm:ss", new System.Globalization.CultureInfo("en-US"));
                        string a = DateTime.Parse(dtp).ToString("yyyy/MM/dd", new System.Globalization.CultureInfo("en-US"));

                        string c = a + " " + b;

                        string d = DateTime.Parse(c).ToString("yyyy/MM/dd HH:mm:ss", new System.Globalization.CultureInfo("en-US"));

                        DateTime e = DateTime.Parse(d);

                        //DateTime e = DateTime.Parse("2020-01-27 11:37:01"); 

                        //====================
                        _jobU = qitem;
                        _JOBId = qitem.JOB_ID;

                        var st = dc.Stations.Where(t => t.STA_ID == qitem.STA_ID).FirstOrDefault();
                        _station = st;
                        var stg = dc.Station_Groups.Where(t => t.GOP_ID == st.GOP_ID).FirstOrDefault();
                        var _mini_jobd = dc.JOB_Details
                            .Where(t => t.JOB_ID == qitem.JOB_ID
                            ////==20200226 
                            //&& t.EndDate.Value.Date == e.Date
                            //&& t.EndDate.Value.Hour == e.Hour
                            //&& t.EndDate.Value.Minute == e.Minute
                            //&& t.EndDate.Value.Second == e.Second
                            ////==========
                            )
                            .OrderByDescending(t => t.StartDate)
                            .FirstOrDefault();

                        #region Conf_Upload_JOB_Photo_Report
                        //20200212using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn))
                        //20200212{
                        //-===20150930 ==ตัดออกเพราะยังไงก็ ต้องเช็ค part 
                        //var jphtot = dc.JOB_PhotoReports
                        //        .Where(t => t.JOB_ID == item.JOB_ID && t.Enddate >= _mini_jobd.EndDate)
                        //        .FirstOrDefault();

                        //if (jphtot != null)
                        //    return false;//Upload JOB Photo แล้ว
                        //-======================

                        //-===20151006 ==chk conf  no part 
                        LConfNoPart = new List<string>();
                        //LConfNoPart =( from np in dc.Conf_Upload_JOB_Photo_Report_NoParts select np.SPA_ID).ToList ();

                        LConfNoPart = dc.Conf_Upload_JOB_Photo_Report_NoParts.Select(t => t.SPA_ID).ToList();
                        //LConfNoPart.Add(confNoPart.)




                        LConfTRA = new List<string>();


                        _conf_jphot = dc.Conf_Upload_JOB_Photo_Reports
                            .Where(
                                    t => t.GOP_ID == st.GOP_ID &&
                                    t.TYP_ID == qitem.TYP_ID &&
                                    t.TYP_ID1 == qitem.TYP_ID1)
                            .FirstOrDefault();

                        if (_conf_jphot != null)
                        {
                            //บังคับ Upload รูป
                            strMaxPrice =
                                "กลุ่มบริษัท: " + stg.TradeName + "-" +
                                stg.GroupStation + Environment.NewLine +
                               "ราคา JOB เกิน: " + _conf_jphot.MaxPrice.Value.ToString("#,#00.00") +
                               " [บังคับ Upload รูปภาพอะไหล่] ";

                            //20201022 เก็บ trantype ลงใน list

                            LConfTRA = dc.Conf_Upload_JOB_Photo_Reports.Where(
                                    t => t.GOP_ID == st.GOP_ID &&
                                    t.TYP_ID == qitem.TYP_ID &&
                                    t.TYP_ID1 == qitem.TYP_ID1).Select(t => t.TRA_ID).ToList();

                        }
                        else
                        {
                            IsPartIN = false;
                            strMaxPrice = "";
                            //break;
                        }

                        //20200212 }// using (DAL.SMSManage.SMSManageDataContext 
                        #endregion



                        var wid = dc.JOB_WorkIDs.Where(t => t.JOB_ID == qitem.JOB_ID).FirstOrDefault();
                        _wid = wid;

                        // //==== 20200226

                        // string yyyy = stmpEndDate.Substring(0,4).ToString();
                        // string mm = stmpEndDate.Substring(4, 2).ToString();
                        // string dd = stmpEndDate.Substring(6, 2).ToString();
                        // string dtp = yyyy + "/" + mm + "/" + dd; 
                        // string b = DateTime.Parse(stmpEndTime).ToString("HH:mm:ss", new System.Globalization.CultureInfo("en-US"));
                        // string a = DateTime.Parse(dtp).ToString("yyyy/MM/dd", new System.Globalization.CultureInfo("en-US"));

                        // string c = a+" "+b;

                        // string d = DateTime.Parse(c).ToString("yyyy/MM/dd HH:mm:ss", new System.Globalization.CultureInfo("en-US"));

                        // DateTime e = DateTime.Parse(d);

                        // //DateTime e = DateTime.Parse("2020-01-27 11:37:01"); 

                        ////====================


                        var jsp = dc.JOB_Detail_Spares
                            .Where(
                             //20150915  t => (t.JOB_ID == item.JOB_ID && t.TRA_ID == "IN") &&
                             //20201021 t => (t.JOB_ID == qitem.JOB_ID && new string[] { "IN", "WA" }.Contains(t.TRA_ID)) &&
                             t => (t.JOB_ID == qitem.JOB_ID && LConfTRA.Contains(t.TRA_ID)) &&//อันนี้ต้องใส่ list trantype ที่ได้จาก
                            (!new string[] { "TRAVEL", "SERVICE" }.Contains(t.SPA_ID)
                            && !LConfNoPart.Contains(t.SPA_ID)
                            //20151006 ยกลิก && !t.SPA_ID.StartsWith("20")//20150930
                            && t.IsCancel != '1' //20190710

                            //==20200226 
                            && t.Enddate.Value.Date == e.Date
                            && t.Enddate.Value.Hour == e.Hour
                            && t.Enddate.Value.Minute == e.Minute
                            && t.Enddate.Value.Second == e.Second

                            //==========
                            )).OrderBy(m => m.StartDate); //20210408

                        // var xx = jsp.Where(t => t.SPA_ID == confNoPart.);

                        if (jsp.Count() > 0)//มี Part IN
                        {//ไม่มีใน nopart
                            int newGen = 0;
                            foreach (var itemx in jsp)
                            {
                                newGen++;
                                lsJSPComp.Add(new DAL.SMSManage.JOB_PhotoReport_Detail
                                {

                                    Item = newGen,//20190710 itemx.NO,
                                    JOB_ID = itemx.JOB_ID,
                                    StartDate = itemx.StartDate,
                                    Enddate = (itemx.Enddate == null ? DateTime.Now : itemx.Enddate.Value),
                                    ImgBefore = null,
                                    ImgAfter = null,
                                    Equipment = itemx.SPA_ID,
                                    Decription = qitem.JobFailure_Detail,
                                    Remark = "",
                                    ImgBeforeRepair = null,
                                    ImgAfterRepair = null,
                                    TranDate = itemx.StartDate
                                });
                                lsJSP.Add(new DAL.SMSManage.JOB_PhotoReport_Detail
                                {

                                    Item = newGen,//20190710 itemx.NO,
                                    JOB_ID = itemx.JOB_ID,
                                    StartDate = itemx.StartDate,
                                    Enddate = (itemx.Enddate == null ? DateTime.Now : itemx.Enddate.Value),
                                    ImgBefore = null,
                                    ImgAfter = null,
                                    Equipment = itemx.SPA_ID,
                                    Decription = qitem.JobFailure_Detail,
                                    Remark = "",
                                    ImgBeforeRepair = null,
                                    ImgAfterRepair = null,
                                    TranDate = itemx.StartDate
                                });
                            }





                            //ตรวจสอบ อะไหล่ IN
                            //var maxPrice = 0;
                            //////////////////////////

                            _jobd = dc.JOB_Detail_Spares
                                //20201022 .Where(t => t.JOB_ID == qitem.JOB_ID && new string[] { "IN", "WA" }.Contains(t.TRA_ID)
                                .Where(t => t.JOB_ID == qitem.JOB_ID && LConfTRA.Contains(t.TRA_ID)
                                //==20200226 
                                && t.Enddate.Value.Date == e.Date
                                && t.Enddate.Value.Hour == e.Hour
                                && t.Enddate.Value.Minute == e.Minute
                                && t.Enddate.Value.Second == e.Second
                                //==========
                                ) //20150930 && t.TRA_ID == "IN")
                                .OrderByDescending(t => t.StartDate)
                                .FirstOrDefault();

                            var _jdp0 = dc.JOB_Detail_Spares
                                //20201022 .Where(t => t.JOB_ID == qitem.JOB_ID && new string[] { "IN", "WA" }.Contains(t.TRA_ID)
                                .Where(t => t.JOB_ID == qitem.JOB_ID && LConfTRA.Contains(t.TRA_ID)
                                //==20200226 
                                && t.Enddate.Value.Date == e.Date
                                && t.Enddate.Value.Hour == e.Hour
                                && t.Enddate.Value.Minute == e.Minute
                                && t.Enddate.Value.Second == e.Second
                                //==========
                                );//&& t.TRA_ID == "IN");

                            if (_jdp0.Count() > 0)
                            {
                                maxPrice = _jdp0.Sum(t => t.SumTotalPrices).Value;
                            }

                            //==============================
                            if (_conf_jphot == null)//ไม่มีคอนฟิก dc.Conf_Upload_JOB_Photo_Report บังคับ uploadตามกลุ่มและประเภทงาน
                            {

                                return false;

                                //////==20170405 ไม่เพิ่ม process ดีกว่า
                                //////-=========20170405 --ตรวจ เรื่อง no part เพื่อ up รูปสถานี

                                //var jspChk = dcMini.JOB_Detail_Spares
                                //    .Where(t => (t.JOB_ID == item.JOB_ID)//20150930
                                //     && LConfNoPart.Contains(t.SPA_ID)
                                // );
                                //if (jspChk.Count() > 0)//มี Part no part
                                //{
                                //    using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
                                //    {
                                //        var jphtot = dc.JOB_PhotoReports
                                //                .Where(t => t.JOB_ID == item.JOB_ID)
                                //                .FirstOrDefault();

                                //        if (jphtot == null)
                                //        {

                                //            _jobd = dcMini.JOB_Detail_Spares
                                //               .Where(t => t.JOB_ID == item.JOB_ID && LConfNoPart.Contains(t.SPA_ID)) //20150930 && t.TRA_ID == "IN")
                                //              .OrderByDescending(t => t.StartDate)
                                //              .FirstOrDefault();

                                //            //=------------
                                //            return true;
                                //        }
                                //        else
                                //        {
                                //            return false;
                                //        }
                                //    } // using (DAL                                  
                                //}
                                ////===
                                //else
                                //{
                                //    return false;
                                //}   

                                //////-=========20170405 --


                            }
                            else//มีคอนฟิก บังคับ upload รูป
                            {
                                //check รูปบน server ว่ามีแล้วหรือยัง
                                //20200217 using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
                                //{
                                //20190715 foreach (var itemChkPhoto in jsp)
                                foreach (var itemChkPhoto in lsJSP)
                                {
                                    DateTime vStartDate = DateTime.Parse(itemChkPhoto.StartDate.ToString());
                                    DateTime vEnddate = DateTime.Parse(itemChkPhoto.Enddate.ToString());
                                    var oldjd = dc.JOB_PhotoReport_Details
                                               .Where(t => t.JOB_ID == itemChkPhoto.JOB_ID.ToString()
                                               //20190711 ยกเลิก && t.Equipment == itemChkPhoto.SPA_ID.ToString()
                                               //==20190711
                                               //20190715 && t.Item == int.Parse(itemChkPhoto.NO.ToString())
                                               && t.Item == int.Parse(itemChkPhoto.Item.ToString())
                                               //20190715 && t.StartDate.Date == Convert.ToDateTime(itemChkPhoto.StartDate).Date
                                               // no && ((t.StartDate.Date == Convert.ToDateTime(itemChkPhoto.StartDate).Date) && (t.StartDate.TimeOfDay == Convert.ToDateTime(itemChkPhoto.StartDate).TimeOfDay)) 
                                               && (t.StartDate == vStartDate)

                                               //20190715
                                               //เอาออกก่อนอยากรู้เงื่อนไขที่แท้จริง  กรณี อะไหล่ค้าง ว่าต้อง upload รูปขึ้นอีกรอบหรือไม่
                                               //อะไหล่ค้างจะกลายเป็น insert ถ้า where enddate 
                                               //20190715 && t.Enddate.Date == Convert.ToDateTime(itemChkPhoto.Enddate).Date
                                               // no && ((t.Enddate.Date == Convert.ToDateTime(itemChkPhoto.Enddate).Date) && (t.Enddate.TimeOfDay == Convert.ToDateTime(itemChkPhoto.Enddate).TimeOfDay)) 
                                               && (t.Enddate == vEnddate)

                                               //==       
                                               ).FirstOrDefault();
                                    if (oldjd == null) //ยังไม่เคยมีรูป
                                    {
                                        // return true; 
                                        if (maxPrice >= _conf_jphot.MaxPrice)//ราคา Job => ราคาที่คอนฟิก
                                        {
                                            return true;
                                        }
                                    }
                                    ////===20190715 เคยมีรูปแล้ว up ขึ้นไปใหม่ในกรณี อะไหล่ค้าง
                                    //else
                                    //{ //  if (oldjd == null)

                                    //    return true;

                                    //}//else   if (oldjd == null)
                                    ////================

                                }//for
                                //}//using  SMSManage



                            }


                            //===============================
                            /*//20151015
                             * if (_conf_jphot == null)//ไม่มีคอนฟิก
                            {
                                return false;
                            }
                            else//มีคอนฟิก
                            {
                                if (maxPrice >= _conf_jphot.MaxPrice)//ราคา Job => ราคาที่คอนฟิก
                                {
                                    return true;
                                }
                            }
                             */


                        }//if (jsp.Count() > 0)//มี Part IN

                        //===20150930 ไม่มี WA,IN 
                        else
                        {//มีใน nopart

                            /* //20170405 
                            
                              //---20151020
                              _jobd = dcMini.JOB_Detail_Spares
                                //20160912 .Where(t => t.JOB_ID == item.JOB_ID && new string[] { "IN", "WA" }.Contains(t.TRA_ID)) //20150930 && t.TRA_ID == "IN")
                                 .Where(t => t.JOB_ID == item.JOB_ID && !new string[] { "IN", "WA" }.Contains(t.TRA_ID)) //20150930 && t.TRA_ID == "IN")
                                .OrderByDescending(t => t.StartDate)
                                .FirstOrDefault();

                              //=------------




                              // ====chk 02

                              var jspChk = dcMini.JOB_Detail_Spares
                             .Where(
                                  //20151006ยกเลิกให้หาใน config แทน  t => (t.JOB_ID == item.JOB_ID && t.SPA_ID.StartsWith("20"))//20150930
                                  t => (t.JOB_ID == item.JOB_ID)//20150930
                                         && LConfNoPart.Contains(t.SPA_ID)

                                     );
                              if (jspChk.Count() > 0)//มี Part no part
                              {
                                  // foreach (var itemchk in jspChk)
                                  //{
                                  using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
                                  {
                                      //    var jspConFNoPart = dc.Conf_Upload_JOB_Photo_Report_NoParts
                                      //                       .Where(
                                      //        //20151006ยกเลิกให้หาใน config แทน  t => (t.JOB_ID == item.JOB_ID && t.SPA_ID.StartsWith("20"))//20150930
                                      //                            t => (t.SPA_ID == itemchk.SPA_ID)//20150930
                                      //                                   ).FirstOrDefault();
                                      //        if (jspConFNoPart != null)
                                      //        {
                                      //chk รูปสถานี 20151001

                                      var jphtot = dc.JOB_PhotoReports
                                              .Where(t => t.JOB_ID == item.JOB_ID)//20151006&& t.Enddate >= _mini_jobd.EndDate)
                                              .FirstOrDefault();

                                      if (jphtot == null)
                                      {
                                          return true;
                                      }
                                      else
                                      {
                                          return false;
                                      }
                                      //     }
                                  } // using (DAL
                                  //} //foreach (var itemchk in jspChk)
                              }//มี Part02
                              //else
                              // {
                              // return false;
                              // }

                              */






                            // ====chk 02
                            // ===20170405 เรียง process ใหม่
                            var jspChk = dc.JOB_Detail_Spares
                           .Where(
                                //20151006ยกเลิกให้หาใน config แทน  t => (t.JOB_ID == item.JOB_ID && t.SPA_ID.StartsWith("20"))//20150930
                                t => (t.JOB_ID == qitem.JOB_ID)//20150930
                                       && LConfNoPart.Contains(t.SPA_ID)

                                   );
                            if (jspChk.Count() > 0)//มี Part no part
                            {
                                // foreach (var itemchk in jspChk)
                                //{
                                //20200217 using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext())
                                //{
                                //    var jspConFNoPart = dc.Conf_Upload_JOB_Photo_Report_NoParts
                                //                       .Where(
                                //        //20151006ยกเลิกให้หาใน config แทน  t => (t.JOB_ID == item.JOB_ID && t.SPA_ID.StartsWith("20"))//20150930
                                //                            t => (t.SPA_ID == itemchk.SPA_ID)//20150930
                                //                                   ).FirstOrDefault();
                                //        if (jspConFNoPart != null)
                                //        {
                                //chk รูปสถานี 20151001

                                var jphtot = dc.JOB_PhotoReports
                                        .Where(t => t.JOB_ID == qitem.JOB_ID)//20151006&& t.Enddate >= _mini_jobd.EndDate)
                                        .FirstOrDefault();

                                if (jphtot == null)
                                {

                                    _jobd = dc.JOB_Detail_Spares
                                       //20160912 .Where(t => t.JOB_ID == item.JOB_ID && new string[] { "IN", "WA" }.Contains(t.TRA_ID)) //20150930 && t.TRA_ID == "IN")
                                       .Where(t => t.JOB_ID == qitem.JOB_ID && LConfNoPart.Contains(t.SPA_ID)) //20150930 && t.TRA_ID == "IN")
                                      .OrderByDescending(t => t.StartDate)
                                      .FirstOrDefault();

                                    //=------------
                                    return true;
                                }
                                else
                                {
                                    return false;
                                }
                                //     }
                                //} // using (DAL
                                //} //foreach (var itemchk in jspChk)
                            }//มี Part02
                            //else
                            // {
                            // return false;
                            // }



                        } //===20150930 ไม่มี WA,IN 







                    } // if (item != null)//มี JOB พร้อม Upload
                }//   using (DAL  server



            }
            catch (Exception)
            {

            }

            return IsPartIN;
        }

        private void cbxChFCAT_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void cbxLocation_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void cob_Failure_Action_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void cob_Failure_Action_Solving_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void comboBox_PoitFail_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void cobOilType_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void cob_SolvingByWI_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void con_ERP_StationCharge_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void cob_TrnType_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void cob_PoitFail0_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void cobProblemType_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }


        private void chkPrintSV(string vpJOBID, string dtEnddate)
        {
            DateTime dt1 = Convert.ToDateTime(dtEnddate.ToString());
            using (DAL.SMSManage.SMSManageDataContext dc = new DAL.SMSManage.SMSManageDataContext(strConn))
            {
                DateTime dtCobEndAuthPrint = Convert.ToDateTime(dt1.ToString("yyyy/MM/dd HH:mm:ss"));
                var iLAuthPrint = (from j in dc.JOBs
                                   join jd in dc.JOB_Details on j.JOB_ID equals jd.JOB_ID
                                   where new string[] { "07", "99" }.Contains(jd.JOBS_ID)
                                   && j.JOB_ID == vpJOBID
                                   && jd.EndDate.Value.Date == dtCobEndAuthPrint.Date
                                   && jd.EndDate.Value.Hour == dtCobEndAuthPrint.Hour
                                   && jd.EndDate.Value.Minute == dtCobEndAuthPrint.Minute
                                   && jd.EndDate.Value.Second == dtCobEndAuthPrint.Second
                                   && jd.StatusDetail == true //20201208
                                   select jd).First();

                if (iLAuthPrint != null)
                {
                    if (iLAuthPrint.EndDate != null)
                    {
                        //====20200519==เงื่อนไข auth printSV 
                        if (!new string[] { "09", "12", "13" }.Contains(UserInfo.UserLevID))
                        {
                            if (iLAuthPrint.IsPrintSV == true)
                            {
                                closeProgress();
                                MessageBox.Show("สามารถสั่งพิมพ์รายงานย้อนหลังได้ เฉพาะ สิทธิ์ผู้จัดการเท่านั้น ", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                if (editMode == "job_edit")
                                {
                                    stmpEndDate = "";
                                    stmpEndTime = "";
                                }
                                return;
                            }

                        }
                    }
                    else
                    {
                        closeProgress();
                        MessageBox.Show("ข้อมูลการปิดงานไม่สมบูรณ์ รบกวนตรวจสอบ ", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;

                    }
                }
            }
        }

        //20200610 private void chkAuthAfterPrintSV(string vpJOBID)
        private void chkAuthAfterPrintSV(string vpJOBID, string _pEnddate)
        {
            //strConn = Properties.Settings.Default["ServicesMSDBConnectionString"].ToString();
            DateTime? srvTime = null;

            //strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
            using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn))
            {

                srvTime = dc
                   .ExecuteQuery<DateTime>("SELECT GETDATE()")
                   .First();
            }
            using (DAL.SMSManage.SMSManageDataContext dc = new DAL.SMSManage.SMSManageDataContext(strConn))
            {
                var itemList = (from j in dc.JOBs
                                join jd in dc.JOB_Details on j.JOB_ID equals jd.JOB_ID
                                //where new string[] { "07", "99" }.Contains(jd.JOBS_ID)
                                where j.JOB_ID == vpJOBID //20200526 txtJobID.Text.Trim()
                                select jd).OrderByDescending(c => c.StartDate).First();

                if (itemList != null)
                {
                    //if (new string[] { "", "99" }.Contains(jd.JOBS_ID))
                    if (itemList.EndDate != null)
                    {
                        ////20200610//20200127 get ค่าวันที่เวลาล่าสุด ที่บันทึกไปแล้วแล้วดึงมาแก้ไข
                        //tmpEndDateTimeEditJOB = itemList.EndDate;
                        //tmpStartDateTimeEditJOB = itemList.StartDate;
                        ////======

                        //=== 20200709 solve error s ==add=
                        if (editMode == "job_add")
                        {
                            _pEnddate = itemList.EndDate.ToString();
                        }
                        //=================================

                        //======20200610
                        DateTime _authEnd = DateTime.Parse(_pEnddate);
                        var itemListChEndate = (from j in dc.JOBs
                                                join jd in dc.JOB_Details on j.JOB_ID equals jd.JOB_ID
                                                //where new string[] { "07", "99" }.Contains(jd.JOBS_ID)
                                                where j.JOB_ID == vpJOBID//20200612 txtJobID.Text.Trim()
                                                 && jd.EndDate.Value.Date == _authEnd.Date
                                                 && jd.EndDate.Value.Hour == _authEnd.Hour
                                                 && jd.EndDate.Value.Minute == _authEnd.Minute
                                                 && jd.EndDate.Value.Second == _authEnd.Second
                                                 && jd.StatusDetail == true //20201208
                                                select jd).First();

                        tmpEndDateTimeEditJOB = itemListChEndate.EndDate;
                        tmpStartDateTimeEditJOB = itemListChEndate.StartDate;
                        //=============

                        //====20200331==เงื่อนไข printSV ,editJOB
                        if (!new string[] { "09", "12", "13" }.Contains(UserInfo.UserLevID))
                        {
                            //20200610 if (itemList.EndDate.Value.Date >= srvTime.Value.Date && itemList.IsPrintSV != true)
                            if (itemListChEndate.EndDate.Value.Date >= srvTime.Value.Date && itemListChEndate.IsPrintSV != true)
                            {
                                disableControl(false);
                                toolStripButton_Save.Enabled = true;
                                groupBox1.Enabled = true;
                                panel_selectEndDate.Visible = false;

                            }
                            //20200610 else if (itemList.EndDate.Value.Date < srvTime.Value.Date && itemList.IsPrintSV == true)
                            else if (itemListChEndate.EndDate.Value.Date < srvTime.Value.Date && itemListChEndate.IsPrintSV == true)
                            {//เลยวัน และพิมพ์แล้ว
                                disableControl(true);
                                panel_selectEndDate.Visible = true;
                                toolStripButton_Save.Enabled = false;
                                panel_selectEndDate.Enabled = true;
                            }
                            else
                            {
                                disableControl(true);
                                panel_selectEndDate.Visible = true;
                                toolStripButton_Save.Enabled = false;
                                panel_selectEndDate.Enabled = true;
                            }
                        }
                        else
                        {//ไม่ใช่ผู้จัดการ

                            disableControl(false);
                            toolStripButton_Save.Enabled = true;
                            groupBox1.Enabled = true;
                            panel_selectEndDate.Visible = false;

                            //--===20200708
                            if (editMode == "job_edit")
                            {
                                

                                //20210322 แก้ไขเงื่อนไขเรื่อง เก็บเงินสด
                                DateTime _vpEnddate = DateTime.Parse(_pEnddate);

                                var qIsPrice = (from t in dc.JOB_Detail_Spares
                                                where t.JOB_ID.Trim() == txtJobID.Text.Trim()
                                                && t.Enddate.Value.Date == _vpEnddate.Date
                                                && t.Enddate.Value.Hour == _vpEnddate.Hour
                                                && t.Enddate.Value.Minute == _vpEnddate.Minute
                                                && t.Enddate.Value.Second == _vpEnddate.Second
                                                && t.IsPrices == true //20210322 เก็บเงินสด
                                                select t).OrderByDescending(c => c.Enddate).FirstOrDefault();
                                if (qIsPrice != null)
                                {
                                    // disableControlInv(false);
                                }
                                else
                                {
                                    var qJDEnd = (from t in dc.JOB_Detail_Spares
                                                  where t.JOB_ID.Trim() == txtJobID.Text.Trim()
                                                  && t.Enddate.Value.Date == _vpEnddate.Date
                                                  && t.Enddate.Value.Hour == _vpEnddate.Hour
                                                  && t.Enddate.Value.Minute == _vpEnddate.Minute
                                                  && t.Enddate.Value.Second == _vpEnddate.Second
                                                  select t).OrderByDescending(c => c.Enddate).FirstOrDefault();

                                    var qJIDate = (from t in dc.JOB_Invoices
                                                   where t.JOB_ID == txtJobID.Text.Trim()
                                                   select t).OrderByDescending(c => c.Invoice_Date).FirstOrDefault();

                                    if (qJDEnd != null && qJIDate != null)
                                    {
                                        //เงื่อนไขนี้เช็คตอน isprice = 0 ทั้งหมด
                                        if (qJDEnd.Enddate.Value.Date <= qJIDate.Invoice_Date.Date)
                                        {
                                            //20202001 disableControl(true);
                                            disableControlInv(true);
                                            //panel_selectEndDate.Visible = true;
                                            //20201001 toolStripButton_Save.Enabled = false;
                                            //panel_selectEndDate.Enabled = true;
                                            //MessageBox.Show("ไม่สามารถแก้ไข ข้อมูลการปิดงานย้อนหลังได้ เนื่องจาก ออก Invoice แล้ว ", "ผลการตรวจสอบ",
                                            //            MessageBoxButtons.OK, MessageBoxIcon.Information);

                                            return;

                                        }
                                    }
                                }
                                //====

                                //===20210817
                                 using (System.Data.SqlClient.SqlConnection Conn = new System.Data.SqlClient.SqlConnection(strConn))
                                {
                                    if (Conn.State == ConnectionState.Closed)
                                        Conn.Open();
                                    //============================
                                    System.Data.SqlClient.SqlDataReader dr = null;

                                    string sqlC = "";
                                    sqlC = " Declare @IsCommission INT " +
                                          " Exec @IsCommission = JobIsCommission '" + txtJobID.Text.Trim() + "'" +
                                          " Select @IsCommission ";


                                    using (dr = new System.Data.SqlClient.SqlCommand(sqlC, Conn).ExecuteReader())
                                    {
                                        dr.Read();
                                        if (dr.HasRows)
                                        {

                                            disableControlInv(true);
                                            //MessageBox.Show("ไม่สามารถแก้ไข ข้อมูลการปิดงานย้อนหลังได้ เนื่องจาก จ่ายค่า Commission แล้ว ", "ผลการตรวจสอบ",
                                            //            MessageBoxButtons.OK, MessageBoxIcon.Information);
                                            return;

                                        }

                                    }
                                }
                                //===========
                            }//edit
                            //--===========
                        }
                        //====
                    }
                    else
                    {
                        if (itemList.JOBS_ID == "05" || itemList.JOBS_ID == "06")
                        {
                            if (editMode == "job_edit")
                            {
                                disableControl(true);
                                panel_selectEndDate.Visible = false;
                                toolStripButton_Save.Enabled = false;
                                panel_selectEndDate.Enabled = false;
                                toolStripButton_Print.Enabled = true;

                                closeProgress();
                                //20201208 MessageBox.Show("ไม่สามารถแก้ไข ข้อมูลการปิดงานย้อนหลังได้ ", "ผลการตรวจสอบ",
                                MessageBox.Show("ไม่สามารถแก้ไข ข้อมูลการปิดงานย้อนหลังได้ " + Environment.NewLine +
                                "สถานะ JOB ปัจจุบัน : เข้างาน ", "ผลการตรวจสอบ",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return;
                            }
                            else
                            {
                                disableControl(false);
                                toolStripButton_Save.Enabled = true;
                            }
                        }
                    }
                }
            }
        }

        private void btEditContractNo_Click(object sender, EventArgs e)
        {
            if (strConn.Contains("ServicesMSDB_"))
                return;

            var smg = new string[] { "09", "12", "13" }.Contains(UserInfo.UserLevID);
            if (smg && editMode == "job_edit")
            {
                Finding.frmCloseJOB_EditContractNo f = new SMSMINI.Transaction.Finding.frmCloseJOB_EditContractNo();
                f.strConn = strConn;
                f.jobID = toolStripText_txtFind.Text.Trim();

                if (f.ShowDialog() == DialogResult.OK)
                {
                    toolStripButton_FindStation_Click(null, null);
                }



            }
            else
            {
                MessageBox.Show("สำหรับโหมดแก้ไข ของ ผู้จัดการส่วน เท่านั้น...", "ผลการตรวจสอบ...", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }

        }



        private void btUpdatePart_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("คุณต้องการแก้ไข TranType และ ราคาอะไหล่ ตามสัญญา ใช่หรือไม่?...", "คำยืนยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                return;
            if (_contractNo != string.Empty)
            {
                /*//===============================================================

                  invoke_Progress("กำลังตรวจสอบ Price List...");
                 decimal priceList = 0;
                 decimal vat = 0;
                 decimal dis = 0;
                 priceList = Decimal.TryParse(txtPriceList.Text, out number) ? number : 0;
                 vat = Decimal.TryParse(txtVAT.Text, out number) ? number : 0;
                 dis = Decimal.TryParse(txtDiscQuarter.Text, out number) ? number : 0;

                 //จำนวนอะใหล่
                 decimal qt = 0;
                 qt = Decimal.TryParse(txtPartQT.Text, out number) ? number : 0;

                 if (dis > 0)
                     priceList = priceList - (priceList * (dis / 100));

                 //ราคา/หน่วย
                 decimal pricePerUnit = 0;// Math.Round(priceList, tmpRounup);

                 if (tmpRounup > 0)
                     pricePerUnit = Math.Round(priceList, tmpRounup);
                 else
                     pricePerUnit = priceList;

                 //dtPart
                 int xrow = 0;

                 if (tmpPA_ID_NO != 0)
                 {
                     xrow = tmpPA_ID_NO - 1;
                     tmpPA_ID_NO = 0;
                 }
                 else
                 {
                     for (int r = 0; r <= dtPart.Rows.Count - 1; r++)
                     {
                         if (Convert.ToInt32(dtPart.Rows[r]["NO."].ToString()) > xrow)
                             xrow = Convert.ToInt32(dtPart.Rows[r]["NO."].ToString());
                     }
                 }


                 //รวมสุทธิ
                 decimal _xtotal = pricePerUnit * qt;

                 //ภาษี
                 decimal _xvat = vat > 0 ? _xtotal * (vat / 100) : 0;


                 //20161114
                 string _vReferPage = txtReferPage.Text.Trim();

                 //===============================================================
                 */

                var _partNo = "";
                using (SMSManageDataContext dc = new SMSManageDataContext(strConn))
                {
                    var _job = dc.JOBs.Where(t => t.JOB_ID == txtJobID.Text).FirstOrDefault();
                    var st = dc.Stations.Where(t => t.STA_ID == _job.STA_ID).FirstOrDefault();
                    var jPart = dc.JOB_Detail_Spares.Where(t => t.JOB_ID == txtJobID.Text).ToList();


                    var _contract = dc.Conf_Contracts.Where(t => t.ContractNo == _contractNo).FirstOrDefault();


                    if (jPart.Count > 0)
                    {
                        foreach (var item in jPart)
                        {
                            //20200827 if (_contract.PriceListNo == "" )
                            if (string.IsNullOrEmpty(_contract.PriceListNo))
                            {
                                ////======================
                                //   string vReferPage = null;
                                //   var qReferPage = (from j in dc.JOBs
                                //                     from s in dc.Stations
                                //                     from con in dc.Conf_Conditions
                                //                     where j.STA_ID == s.STA_ID &&
                                //                     j.ContractNo == con.ContractNo &&
                                //                     s.GOP_ID == con.GOP_ID &&
                                //                     j.TYP_ID == con.TYP_ID &&
                                //                     j.JOB_ID == _job.JOB_ID &&
                                //                     con.SPA_ID == item.SPA_ID
                                //                     select con).FirstOrDefault();

                                //   if (qReferPage != null)
                                //   {
                                //       vReferPage = qReferPage.ReferPage;
                                //   }
                                //   else
                                //   {
                                //       vReferPage = "";
                                //   }

                                //   //======================


                                //สัญญา เหมาจ่าย
                                var partCon = dc.Conf_Conditions
                                                .Where(t => t.ContractNo == _contractNo &&
                                                            t.GOP_ID == st.GOP_ID &&
                                                            t.TYP_ID == _job.TYP_ID &&
                                                            t.TYP_ID1 == _job.TYP_ID1 &&
                                                            t.SPA_ID == item.SPA_ID)
                                                .FirstOrDefault();



                                if (partCon != null)//ราคาในสัญญาเหมาจ่าย
                                {
                                    decimal vVAT = 0;
                                    vVAT = Decimal.Parse(item.VAT.ToString());

                                    decimal vPriceList = partCon.PriceList;
                                    decimal vPrices = partCon.PriceList;

                                    decimal vSumPrices = vPriceList * Decimal.Parse(item.Quantity.ToString());
                                    decimal vSumVat = vVAT > 0 ? vSumPrices * (vVAT / 100) : 0;
                                    decimal vSumTotalPrices = vSumPrices + vSumVat;




                                    item.PricesList = vPriceList;
                                    item.Prices = vPrices;
                                    item.SumPrices = vSumPrices;
                                    item.SumVAT = vSumVat;
                                    item.SumTotalPrices = vSumTotalPrices;
                                    item.ReferPage = partCon.ReferPage == null ? "" : partCon.ReferPage;

                                    item.TRA_ID = partCon.TRA_ID;
                                    //item.Prices = partCon.PriceList;
                                    //item.PricesList = partCon.PriceList;

                                    dc.SubmitChanges();

                                    //_partNo = ", " + _partNo;
                                    _partNo = _partNo + ", " + item.SPA_ID;
                                }
                                else
                                {
                                    //ตรวจตามสัญญาก่อน ถ้าไม่มีจริงๆค่อยใช้ราคากลาง
                                    var conP1x = (from t in dc.Conf_Conditions
                                                  from sp in dc.SpareParts
                                                  where (t.SPA_ID == sp.SPA_ID &&
                                                      t.SPA_ID == item.SPA_ID &&
                                                      t.GOP_ID == tmpGOPId
                                                      ) &&
                                                      t.ContractNo == _contractNo
                                                  group t by t.SPA_ID
                                            into g
                                                  select new
                                                  {
                                                      PriceList = g.Max(t => t.PriceList)
                                                      ,
                                                      ReferPage = g.Select(t => t.ReferPage.ToString())
                                                  }).FirstOrDefault();

                                    //if (conP1 != null && conP1.PriceList > 0)
                                    //decimal _vPrice = 0;
                                    if (conP1x != null)
                                    {
                                        //    _vPrice = conP1x.PriceList;
                                        //}
                                        //    #region Check Price Conf_Conditions

                                        //    //txtPriceList.Text = conP1.PriceList.ToString("#,##0.00");
                                        //    //_prices = Convert.ToDouble(conP1.PriceList);

                                        //    //txtPriceUnit.Text = (Convert.ToDouble(_prices) * quantity).ToString("#,##0.00");

                                        //    #endregion

                                        //    item.TRA_ID = partCon.TRA_ID;
                                        //    item.Prices = partCon.PriceList;
                                        //    item.PricesList = partCon.PriceList;

                                        //    dc.SubmitChanges();

                                        //    _partNo = ", " + _partNo;
                                        //}
                                        //else
                                        //{ 

                                        //ใช้ trantype ใน sparePart แต่ราคาต้องเช็คในสัญญาก่อน

                                        var _part = dc.SpareParts.Where(t => t.SPA_ID == item.SPA_ID).FirstOrDefault();
                                        if (_part != null)
                                        {
                                            string _vpartTRA = "IN";
                                            if (!string.IsNullOrEmpty(_part.TranType) && _part.TranType != "00")
                                            {
                                                _vpartTRA = _part.TranType;
                                            }


                                            decimal vVAT = 0;
                                            vVAT = Decimal.Parse(item.VAT.ToString());

                                            decimal vPriceList = conP1x.PriceList;
                                            decimal vPrices = conP1x.PriceList;


                                            decimal vSumPrices = vPriceList * Decimal.Parse(item.Quantity.ToString());
                                            decimal vSumVat = vVAT > 0 ? vSumPrices * (vVAT / 100) : 0;
                                            decimal vSumTotalPrices = vSumPrices + vSumVat;


                                            item.PricesList = vPriceList;
                                            item.Prices = vPrices;
                                            item.SumPrices = vSumPrices;
                                            item.SumVAT = vSumVat;
                                            item.SumTotalPrices = vSumTotalPrices;
                                            item.ReferPage = "";


                                            item.TRA_ID = _vpartTRA;//(_part.TranType == "" ? "IN" : _part.TranType);
                                                                    //item.Prices = (conP1x.PriceList == null ? _part.PricePerUnit : conP1x.PriceList);//_part.PricePerUnit;
                                                                    //item.PricesList = (conP1x.PriceList == null ? _part.PricePerUnit : conP1x.PriceList);//_part.PricePerUnit;

                                            dc.SubmitChanges();

                                            //_partNo = ", " + _partNo;
                                            _partNo = _partNo + ", " + item.SPA_ID;
                                        }
                                    }
                                    else
                                    {
                                        var _part = dc.SpareParts.Where(t => t.SPA_ID == item.SPA_ID).FirstOrDefault();
                                        if (_part != null)
                                        {
                                            string _vpartTRA = "IN";
                                            if (!string.IsNullOrEmpty(_part.TranType) && _part.TranType != "00")
                                            {
                                                _vpartTRA = _part.TranType;
                                            }

                                            decimal vVAT = 0;
                                            vVAT = Decimal.Parse(item.VAT.ToString());

                                            decimal vPriceList = Decimal.Parse(_part.PricePerUnit.ToString());
                                            decimal vPrices = Decimal.Parse(_part.PricePerUnit.ToString());


                                            decimal vSumPrices = vPriceList * Decimal.Parse(item.Quantity.ToString());
                                            decimal vSumVat = vVAT > 0 ? vSumPrices * (vVAT / 100) : 0;
                                            decimal vSumTotalPrices = vSumPrices + vSumVat;


                                            item.PricesList = vPriceList;
                                            item.Prices = vPrices;
                                            item.SumPrices = vSumPrices;
                                            item.SumVAT = vSumVat;
                                            item.SumTotalPrices = vSumTotalPrices;
                                            item.ReferPage = "";

                                            item.TRA_ID = _vpartTRA;
                                            //item.TRA_ID = (_part.TranType==""? "IN":_part.TranType);
                                            //item.Prices = _part.PricePerUnit;
                                            //item.PricesList = _part.PricePerUnit;

                                            dc.SubmitChanges();

                                            //_partNo = ", " + _partNo;
                                            _partNo = _partNo + ", " + item.SPA_ID;
                                        }
                                    }
                                }

                            }
                            else
                            {
                                //สัญญา MA
                                var partConMA = dc.Conf_Conditions_PriceLists
                                              .Where(t => t.PriceListNo == _contract.PriceListNo &&
                                                          //t.GOP_ID == st.GOP_ID &&
                                                          t.TYP_ID == _job.TYP_ID &&
                                                          t.TYP_ID1 == _job.TYP_ID1 &&
                                                          t.SPA_ID == item.SPA_ID)
                                              .FirstOrDefault();



                                if (partConMA != null)//ราคา ใน MA
                                {
                                    decimal vVAT = 0;
                                    vVAT = Decimal.Parse(item.VAT.ToString());

                                    decimal vPriceList = partConMA.PriceList;
                                    decimal vPrices = partConMA.PriceList;


                                    decimal vSumPrices = vPriceList * Decimal.Parse(item.Quantity.ToString());
                                    decimal vSumVat = vVAT > 0 ? vSumPrices * (vVAT / 100) : 0;
                                    decimal vSumTotalPrices = vSumPrices + vSumVat;


                                    item.PricesList = vPriceList;
                                    item.Prices = vPrices;
                                    item.SumPrices = vSumPrices;
                                    item.SumVAT = vSumVat;
                                    item.SumTotalPrices = vSumTotalPrices;
                                    item.ReferPage = partConMA.ReferPage == null ? "" : partConMA.ReferPage;

                                    item.TRA_ID = partConMA.TRA_ID;
                                    //item.Prices = partConMA.PriceList;
                                    //item.PricesList = partConMA.PriceList;

                                    dc.SubmitChanges();

                                    //_partNo = ", " + _partNo;
                                    _partNo = _partNo + ", " + item.SPA_ID;
                                }
                                else//ถ้าไม่มี ใช้ราคากลาง
                                {
                                    var _part = dc.SpareParts.Where(t => t.SPA_ID == item.SPA_ID).FirstOrDefault();
                                    if (_part != null)
                                    {
                                        string _vpartTRA = "IN";
                                        if (!string.IsNullOrEmpty(_part.TranType) && _part.TranType != "00")
                                        {
                                            _vpartTRA = _part.TranType;
                                        }

                                        decimal vVAT = 0;
                                        vVAT = Decimal.Parse(item.VAT.ToString());

                                        decimal vPriceList = Decimal.Parse(_part.PricePerUnit.ToString());
                                        decimal vPrices = Decimal.Parse(_part.PricePerUnit.ToString());


                                        decimal vSumPrices = vPriceList * Decimal.Parse(item.Quantity.ToString());
                                        decimal vSumVat = vVAT > 0 ? vSumPrices * (vVAT / 100) : 0;
                                        decimal vSumTotalPrices = vSumPrices + vSumVat;


                                        item.PricesList = vPriceList;
                                        item.Prices = vPrices;
                                        item.SumPrices = vSumPrices;
                                        item.SumVAT = vSumVat;
                                        item.SumTotalPrices = vSumTotalPrices;
                                        item.ReferPage = "";

                                        item.TRA_ID = _vpartTRA;
                                        //item.TRA_ID = (_part.TranType==""? "IN":_part.TranType);
                                        //item.Prices = _part.PricePerUnit;
                                        //item.PricesList = _part.PricePerUnit;

                                        dc.SubmitChanges();

                                        //_partNo = ", " + _partNo;
                                        _partNo = _partNo + ", " + item.SPA_ID;
                                    }
                                }
                            }

                        }
                        var cv = _partNo;

                        var sql_log = "";
                        sql_log = "Insert into [LogSystem] ([TableName] ,[DataID] ,[Detail] ,[UserID] ,[IPAddress]) " +
                            " values ('JOB'  ,'" + txtJobID.Text.Trim() + "' ,' EditContractNo[" + _contractNo + " => Part.No=" + _partNo + "]' ,'" + UserInfo.UserId + "' ,CONVERT(char(15), CONNECTIONPROPERTY('client_net_address')))";

                        dc.ExecuteCommand(sql_log);


                        MessageBox.Show("Auto Update TranType...ราคา...สัญญา " + _contractNo + " เรียบร้อย", "ผลการทำงาน", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        toolStripButton_FindStation_Click(null, null);
                    }
                }
            }
        }

        private void btUpdateWorkType_Click(object sender, EventArgs e)
        {
            if (strConn.Contains("ServicesMSDB_"))
                return;

            var smg = new string[] { "09", "12", "13" }.Contains(UserInfo.UserLevID);
            if (smg && editMode == "job_edit")
            {
                Finding.frmCloseJOB_EditWorkType f = new SMSMINI.Transaction.Finding.frmCloseJOB_EditWorkType();
                f.strConn = strConn;
                f.jobID = toolStripText_txtFind.Text.Trim();

                if (f.ShowDialog() == DialogResult.OK)
                {

                    toolStripButton_FindStation_Click(null, null);
                }



            }
            else
            {
                MessageBox.Show("สำหรับโหมดแก้ไข ของ ผู้จัดการส่วน เท่านั้น...", "ผลการตรวจสอบ...", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
        }

    }


    public class ComboBoxItem
    {
        public string Value;
        public string Text;

        public ComboBoxItem(string val, string text) { Value = val; Text = text; }

        public override string ToString() { return Text; }
    }

    public class SPLIT_Part_ERP
    {
        public string ERP_SPA_ID { get; set; }
        public string ERP_TRA_ID { get; set; }
        public string ERP_PartName { get; set; }
    }

    //20200602
    public class LPartUsedPOI
    {
        public string _LJOBID { get; set; }
        public string _Llocat { get; set; }
        public string _Lpf { get; set; }
        public string _Lfai { get; set; }
        public string _Lsn { get; set; }
        public string _Lspa { get; set; }

    }

    //20200626
    public class LChecklogEditDel
    {
        public string _LJOBID { get; set; }
        public string _Llocat { get; set; }
        public string _Lpoi { get; set; }
        public string _Lfai { get; set; }
        public string _Lsn { get; set; }
        public string _Lno { get; set; }
        public string _Lspa { get; set; }
        public string _Ltra { get; set; }
        public string _Lprice { get; set; }
        public DateTime _Lend { get; set; }

    }


}
