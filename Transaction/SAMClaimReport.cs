﻿using CrystalDecisions.Shared;
using System;
using System.Windows.Forms;

namespace SMSMINI.Transaction
{
    public partial class SAMClaimReport : Form
    {
        public SAMClaimReport()
        {
            InitializeComponent();
        }

        SMSMINI.Transaction.Report.rptSAMClaim rpt = new Report.rptSAMClaim();
        private void SAMClaimReport_Load(object sender, EventArgs e)
        {

            crystalReportViewer1.ReportSource = rpt;
        }

        private void btPrint_Click(object sender, EventArgs e)
        {
            Export2PDF("", rpt);
        }

        string sv = "";
        string tmpJobID = "";

        private void Export2PDF(string mode, Report.rptSAMClaim rpt)
        {
            string f = "";
            SaveFileDialog DialogSave = new SaveFileDialog();
            DialogSave.DefaultExt = "xls";
            DialogSave.Filter = "Adobe Acrobat  (*.pdf)|*.pdf|All files (*.*)|*.*";
            DialogSave.AddExtension = true;
            DialogSave.RestoreDirectory = true;
            DialogSave.Title = "Where do you want to save the file?";
            DialogSave.InitialDirectory = @"C:/";


            sv = "SAM_ClaimReport_" + tmpJobID + "_(" + mode + ")_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".pdf";

            DialogSave.FileName = sv;

            if (DialogSave.ShowDialog() == DialogResult.OK)
            {
                f = DialogSave.FileName.ToString();
                DialogSave.Dispose();
                DialogSave = null;

                try
                {
                    ExportOptions crOptions;
                    var ddOptions = new DiskFileDestinationOptions();
                    var fOptions = new PdfRtfWordFormatOptions();
                    ddOptions.DiskFileName = f;


                    crOptions = rpt.ExportOptions;
                    {
                        crOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                        crOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                        crOptions.DestinationOptions = ddOptions;
                        crOptions.FormatOptions = fOptions;

                    }
                    rpt.Export();
                    rpt.Close();

                    if (System.IO.File.Exists(f))
                    {
                        System.Diagnostics.Process.Start(f);
                    }


                }

                catch (Exception ex)
                {
                    MessageBox.Show("ข้อผิดพลาด " + ex.ToString(), "เกิดข้อผิดพลาดในการ Export ไฟล์ PDF SAM_ClaimReport");
                }
            }// if (DialogSave.ShowDialog() == DialogResult.OK)

        }


    }
}
