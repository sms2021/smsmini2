﻿namespace SMSMINI.Transaction
{
    partial class ServiceReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ServiceReport));
            this.crystalReportViewer1 = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.chkPrintCopy = new System.Windows.Forms.CheckBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.btPrint = new System.Windows.Forms.Button();
            this.checkBox_2pdf = new System.Windows.Forms.CheckBox();
            this.cbPrintIsCustomer = new System.Windows.Forms.CheckBox();
            this.cbxComp = new System.Windows.Forms.ComboBox();
            this.lblComp = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // crystalReportViewer1
            // 
            this.crystalReportViewer1.ActiveViewIndex = -1;
            this.crystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crystalReportViewer1.Cursor = System.Windows.Forms.Cursors.Default;
            this.crystalReportViewer1.DisplayBackgroundEdge = false;
            this.crystalReportViewer1.DisplayStatusBar = false;
            this.crystalReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.crystalReportViewer1.Location = new System.Drawing.Point(0, 0);
            this.crystalReportViewer1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.crystalReportViewer1.Name = "crystalReportViewer1";
            this.crystalReportViewer1.SelectionFormula = "";
            this.crystalReportViewer1.ShowExportButton = false;
            this.crystalReportViewer1.ShowGotoPageButton = false;
            this.crystalReportViewer1.ShowGroupTreeButton = false;
            this.crystalReportViewer1.ShowLogo = false;
            this.crystalReportViewer1.ShowPrintButton = false;
            this.crystalReportViewer1.ShowRefreshButton = false;
            this.crystalReportViewer1.ShowTextSearchButton = false;
            this.crystalReportViewer1.Size = new System.Drawing.Size(1056, 697);
            this.crystalReportViewer1.TabIndex = 0;
            this.crystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            this.crystalReportViewer1.ToolPanelWidth = 267;
            this.crystalReportViewer1.ViewTimeSelectionFormula = "";
            this.crystalReportViewer1.Load += new System.EventHandler(this.crystalReportViewer1_Load);
            // 
            // chkPrintCopy
            // 
            this.chkPrintCopy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkPrintCopy.AutoSize = true;
            this.chkPrintCopy.BackColor = System.Drawing.Color.Transparent;
            this.chkPrintCopy.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.chkPrintCopy.Location = new System.Drawing.Point(813, 38);
            this.chkPrintCopy.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkPrintCopy.Name = "chkPrintCopy";
            this.chkPrintCopy.Size = new System.Drawing.Size(119, 24);
            this.chkPrintCopy.TabIndex = 1;
            this.chkPrintCopy.Text = "เบิกค่าใช้จ่าย";
            this.chkPrintCopy.UseVisualStyleBackColor = false;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // btPrint
            // 
            this.btPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btPrint.Location = new System.Drawing.Point(940, 38);
            this.btPrint.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btPrint.Name = "btPrint";
            this.btPrint.Size = new System.Drawing.Size(100, 28);
            this.btPrint.TabIndex = 2;
            this.btPrint.Text = "พิมพ์ SV";
            this.btPrint.UseVisualStyleBackColor = true;
            this.btPrint.Click += new System.EventHandler(this.btPrint_Click);
            // 
            // checkBox_2pdf
            // 
            this.checkBox_2pdf.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox_2pdf.AutoSize = true;
            this.checkBox_2pdf.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.checkBox_2pdf.Location = new System.Drawing.Point(688, 38);
            this.checkBox_2pdf.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.checkBox_2pdf.Name = "checkBox_2pdf";
            this.checkBox_2pdf.Size = new System.Drawing.Size(118, 24);
            this.checkBox_2pdf.TabIndex = 3;
            this.checkBox_2pdf.Text = "Export PDF";
            this.checkBox_2pdf.UseVisualStyleBackColor = true;
            // 
            // cbPrintIsCustomer
            // 
            this.cbPrintIsCustomer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbPrintIsCustomer.AutoSize = true;
            this.cbPrintIsCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.cbPrintIsCustomer.Location = new System.Drawing.Point(555, 38);
            this.cbPrintIsCustomer.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbPrintIsCustomer.Name = "cbPrintIsCustomer";
            this.cbPrintIsCustomer.Size = new System.Drawing.Size(112, 24);
            this.cbPrintIsCustomer.TabIndex = 4;
            this.cbPrintIsCustomer.Text = "อะไหล่ลูกค้า";
            this.cbPrintIsCustomer.UseVisualStyleBackColor = true;
            // 
            // cbxComp
            // 
            this.cbxComp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbxComp.FormattingEnabled = true;
            this.cbxComp.Items.AddRange(new object[] {
            "กรุณาเลือก"});
            this.cbxComp.Location = new System.Drawing.Point(367, 32);
            this.cbxComp.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbxComp.Name = "cbxComp";
            this.cbxComp.Size = new System.Drawing.Size(160, 24);
            this.cbxComp.TabIndex = 5;
            this.cbxComp.SelectionChangeCommitted += new System.EventHandler(this.cbxComp_SelectionChangeCommitted);
            // 
            // lblComp
            // 
            this.lblComp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblComp.AutoSize = true;
            this.lblComp.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.lblComp.Location = new System.Drawing.Point(292, 37);
            this.lblComp.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblComp.Name = "lblComp";
            this.lblComp.Size = new System.Drawing.Size(63, 20);
            this.lblComp.TabIndex = 6;
            this.lblComp.Text = "บริษัท  :";
            // 
            // ServiceReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1056, 697);
            this.Controls.Add(this.lblComp);
            this.Controls.Add(this.cbxComp);
            this.Controls.Add(this.cbPrintIsCustomer);
            this.Controls.Add(this.chkPrintCopy);
            this.Controls.Add(this.checkBox_2pdf);
            this.Controls.Add(this.btPrint);
            this.Controls.Add(this.crystalReportViewer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MinimizeBox = false;
            this.Name = "ServiceReport";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ServiceReport";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ServiceReport_FormClosed);
            this.Load += new System.EventHandler(this.ServiceReport_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CrystalDecisions.Windows.Forms.CrystalReportViewer crystalReportViewer1;
        private System.Windows.Forms.CheckBox chkPrintCopy;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Button btPrint;
        private System.Windows.Forms.CheckBox checkBox_2pdf;
        private System.Windows.Forms.CheckBox cbPrintIsCustomer;
        private System.Windows.Forms.ComboBox cbxComp;
        private System.Windows.Forms.Label lblComp;
    }
}