﻿using System;
using System.Data;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace SMSMINI
{
    public partial class NetworkConnectionTest : Form
    {
        DataTable dtAddr = null;
        DataGridViewImageColumn imColumn = null;


        public NetworkConnectionTest()
        {
            InitializeComponent();

            object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
            if (attributes.Length == 0)
            {
                this.Text = "SMS MINI [v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "]";

            }
            this.Text = ((AssemblyProductAttribute)attributes[0]).Product + " [v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "]";

        }

        private void NetworkConnectionTest_Load(object sender, EventArgs e)
        {


            setDataSourde();

            imColumn = new DataGridViewImageColumn();
            imColumn.Name = "Ping";
            imColumn.HeaderText = "";
            imColumn.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            imColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView1.Columns.Insert(dataGridView1.ColumnCount, imColumn);


        }

        private void setDataSourde()
        {
            dtAddr = new DataTable();
            dtAddr.Columns.Add(new DataColumn("Status"));
            dtAddr.Columns.Add(new DataColumn("Host"));
            dtAddr.Columns.Add(new DataColumn("Address"));
            dtAddr.Columns.Add(new DataColumn("Message"));

        }

        private void btNetworkTest_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.AppStarting;
                ping("Google", "www.google.com");
                ping("Flowco", "sms.flowco.co.th");
                setImage2Column();

                Cursor.Current = Cursors.Default;
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("เกิดผิดพลาดใน ทดสอบการเชื่อมต่อ Network " + ex.Message, "ข้อผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }

        private void setImage2Column()
        {
            for (int i = 0; i <= dataGridView1.RowCount - 1; i++)
            {
                if (dataGridView1.Rows[i].Cells["Status"].Value.ToString() == "Fail")
                {
                    dataGridView1.Rows[i].Cells["Ping"].Value = Properties.Resources.fail;
                    dataGridView1.Rows[i].Cells["Ping"].ToolTipText = "Faliure";
                }
                else
                {
                    dataGridView1.Rows[i].Cells["Ping"].Value = Properties.Resources.pass;
                    dataGridView1.Rows[i].Cells["Ping"].ToolTipText = "Success";
                }

            }
        }

        private void ping(string address)
        {
            try
            {
                int timeout = 12000;
                Ping ping = new Ping();
                byte[] buffer = Encoding.ASCII.GetBytes("ooooooooooooooooooooooo");
                PingOptions options = new PingOptions(64, true);

                AutoResetEvent waiter = new AutoResetEvent(false);
                ping.PingCompleted += new PingCompletedEventHandler(ping_PingCompleted);
                ping.SendAsync(address, timeout, buffer, options, null);


            }
            catch (System.Exception)
            {


            }
        }

        void ping_PingCompleted(object sender, PingCompletedEventArgs e)
        {
            try
            {
                DataRow dr = dtAddr.NewRow();
                dr["Status"] = e.Reply.Status.ToString();
                dr["Address"] = e.Reply.Address.ToString();
                dr["Round Trip Time"] = e.Reply.RoundtripTime.ToString();
                dr["Buffer Length"] = e.Reply.Buffer.Length.ToString();
                dr["Time to live"] = e.Reply.Options.Ttl.ToString();
                dtAddr.Rows.Add(dr);

                dataGridView1.DataSource = dtAddr;
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("เกิดผิดพลาดใน ทดสอบการเชื่อมต่อ Network " + ex.Message, "ข้อผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }



        void ping(string host, string address)
        {
            byte[] buffer = Encoding.ASCII.GetBytes("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
            PingReply oReply = null;
            try
            {
                Ping pingSender = new Ping();
                PingOptions options = new PingOptions(64, true);
                int timeout = 12000;
                oReply = pingSender.Send(address, timeout, buffer, options);

                DataRow dr = dtAddr.NewRow();
                dr["Status"] = oReply.Status.ToString();
                dr["Host"] = host;
                dr["Address"] = oReply.Address.ToString();

                dr["Message"] = "RoundtripTime: " + oReply.RoundtripTime.ToString() + " Buffer Length: " + oReply.Buffer.Length.ToString() + " Options: " + oReply.Options.Ttl.ToString();
                dtAddr.Rows.Add(dr);

                dataGridView1.DataSource = dtAddr;
            }
            catch (System.Exception ex)
            {

                DataRow dr = dtAddr.NewRow();
                dr["Status"] = "Fail";
                dr["Host"] = host;
                dr["Address"] = address;
                dr["Message"] = "Time out Error: " + ex.Message;
                dtAddr.Rows.Add(dr);

                dataGridView1.DataSource = dtAddr;
            }

        }



    }
}
