﻿#pragma warning disable CS0246 // The type or namespace name 'Janawat' could not be found (are you missing a using directive or an assembly reference?)
using Janawat.Application.Data;
#pragma warning restore CS0246 // The type or namespace name 'Janawat' could not be found (are you missing a using directive or an assembly reference?)
using Microsoft.Win32;
using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Deployment.Application;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Windows.Forms;

namespace SMSMINI
{


    public partial class Login : Form
    {
        private ProgressBar.FormProgress m_fmProgress = null;

#pragma warning disable CS0246 // The type or namespace name 'Encryption' could not be found (are you missing a using directive or an assembly reference?)
        Encryption.Encryption dCryp = new Encryption.Encryption();
#pragma warning restore CS0246 // The type or namespace name 'Encryption' could not be found (are you missing a using directive or an assembly reference?)

        string strMode = string.Empty;
        int loginattempts = 0;

        bool isDownloadDBComplete = false;

        string strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;

        public Login()
        {
            InitializeComponent();

            object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
            if (attributes.Length == 0)
            {
                this.Text = "SMS MINI [v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "]";

            }
            this.Text = ((AssemblyProductAttribute)attributes[0]).Product + " [v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "]";

        }



        private void Login_Load(object sender, EventArgs e)
        {
            checkVersion();

            ////====แก้ 20160831=====//
            //strConn = SMSMINI.Properties.Settings.Default.ServicesMSDBConnectionString;
            strMode = "0";
            //==================

            ////if (!checkSQLService("MSSQL$SQLEXPRESS"))  //if (!checkSQLService("MSSQL$SQLEXPRESS"))
            ////    return;

            // updateScript();

            //radioButton_On.Enabled = true;
            //radioButton_Off.Checked = true;

            txtUserName.Text = "";
            txtPassword.Text = "";

            txtUserName.Focus();

            //Font size reduced after export the crystal report to pdf format
            //Fixed by update registry 
            //For Crystal Report Basic for Visual Studio 2008
            try
            {
                RegistryKey regKey = Registry.CurrentUser;
                regKey = regKey.CreateSubKey("Software\\Business Objects\\10.5\\Crystal Reports\\Export\\PDF");
                regKey.SetValue("ForceLargerFonts", 1);

            }
            catch (Exception)
            {

            }
        }


        public string AssemblyProduct
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyProductAttribute)attributes[0]).Product;
            }
        }

        Version myVersion = new Version();
        private void checkVersion()
        {

            var checkTestVersion = AssemblyProduct;

            if (!checkTestVersion.Contains("TEST"))
            {

                Version newVersion = new Version();

                if (ApplicationDeployment.IsNetworkDeployed)//ดึง Version จาก ClickOnce
                    newVersion = ApplicationDeployment.CurrentDeployment.CurrentVersion;
                
                string _newversion = String.Concat(newVersion);


                var prodVersion = "";
                //string _xnewversion = ""; //ดึง Version จาก  ServicesMSDB..SMSDBVersion.SMSVersion


                using (DAL.SMSManage.SMSManageDataContext dc = new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn))
                //20210813using (DAL.SMS21.ServicesMSDBEntities dc = new DAL.SMS21.ServicesMSDBEntities ())
                {


                    try
                    {
                        //20200311 prodVersion = dc.SMSDBVersions.FirstOrDefault().SMSVersion.Trim();
                        //20210813 prodVersion = dc.SMSMINI2DBVersion.FirstOrDefault().SMSVersion.Trim();
                        prodVersion = dc.SMSMINI2DBVersions.FirstOrDefault().SMSVersion.Trim();

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(
                            "ไม่สามารถ เชื่อมต่อ SMS Server..." + Environment.NewLine +
                            //20200311 "คุณไม่สามารถ Download และ Upload JOB..." + Environment.NewLine +
                            "----------------------------------------------------" + Environment.NewLine +
                            "กรุณา ตรวจสอบ การเชื่อมต่ออินเตอร์เนต" + Environment.NewLine +
                            "โดย:" + Environment.NewLine +
                            "1. Reboot Notebook 1 ครั้ง" + Environment.NewLine +
                            "2. Reboot มือถือ 1 ครั้ง" + Environment.NewLine +
                            "3. ใช้เน็ตมือถือ...ถ้าใช้เน็ต wifi ของสถานี " + Environment.NewLine +
                            "......บางครั้ง บล็อกไม่ให้เข้าถึง SMS Server..." + Environment.NewLine +
                            "4. จบขั้นตอน" + Environment.NewLine +
                            "----------------------------------------------------" + Environment.NewLine +
                            "ข้อผิดพลาด: " + Environment.NewLine + ex.Message + Environment.NewLine +
                            "", "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                        return;
                        //this.Close();
                    }


                }

                myVersion = Assembly.GetExecutingAssembly().GetName().Version;

                var version = string.Concat(myVersion);

                if (prodVersion != version)
                {
                    MessageBox.Show("ไม่สามารถใช้งานโปรแกรมได้" + Environment.NewLine +
                        "เนื่องจาก คุณยังไม่ได้ Update โปรแกรมเวอร์ชั่นใหม่" + Environment.NewLine +
                        "New Version = SMS MINI v." + _newversion + Environment.NewLine +
                        "กรุณา Update version ใหม่...." + Environment.NewLine + Environment.NewLine +
                         "โดยเข้าติดตั้งได้ที่: http://sms.flowco.co.th/smsmini2/", "ผลการตรวจสอบ...", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    System.Diagnostics.Process.Start("http://sms.flowco.co.th/smsmini2/");

                    this.Close();
                }
            }
        }


       

        private void btLogin_Click(object sender, EventArgs e)
        { 

            if (txtUserName.Text == "")
            {
                MessageBox.Show("กรุณาป้อนข้อมูล UserName", "ข้อผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtUserName.Focus();
                txtUserName.SelectAll();
                return;
            }

            if (txtPassword.Text == "")
            {
                MessageBox.Show("กรุณาป้อนข้อมูล Password", "ข้อผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtPassword.Focus();
                txtPassword.SelectAll();
                return;
            }


            LoginSystem(txtUserName.Text, txtPassword.Text);
        }



        private bool Authentication(string user, string pass)
        {
            string key = SMSMINI.Properties.Settings.Default.CryptionKey;
            string xpass = dCryp.Encrypt(pass, key);

            strConn = Properties.Settings.Default["ServicesMSDBConnectionString"].ToString();

            using (DAL.SMSManage.SMSManageDataContext dc = new DAL.SMSManage.SMSManageDataContext(strConn))
            {
                var u = (from t in dc.Users
                         where t.UserName.Trim() == user.Trim() &&
                             t.Password.Trim() == xpass
                         select t).FirstOrDefault();

                if (u == null)
                    return false;
                if (new string[] { "05", "11", "12", "13", "99" }.Contains(u.LEV_ID))
                    return true;
            }

            return false;
        }

        private void btExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }


        private void radioButton_On_Click(object sender, EventArgs e)
        {
            //groupBox1.Text = "{Management db}";
            txtUserName.Focus();
            txtUserName.SelectAll();
            strMode = "1";


        }


        private void radioButton_Off_Click(object sender, EventArgs e)
        {
            //groupBox1.Text = "{Mini db}";
            txtUserName.Focus();
            txtUserName.SelectAll();
            strMode = "0";


        }

        private void LoginSystem(string _UserName, string _Pass)
        {
            try
            {
                string key = SMSMINI.Properties.Settings.Default.CryptionKey;
                string xpass = dCryp.Encrypt(_Pass, key);


                strConn = Properties.Settings.Default["ServicesMSDBConnectionString"].ToString();


                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append("SELECT * from vUserLogon ");
                sb.Append(" Where UserName=@UserName and Password=@Password");

                string sql = sb.ToString();

                SqlParameter[] parameters = { new SqlParameter("@UserName", _UserName), new SqlParameter("@Password", xpass) };

                SqlDataReader dr = JaSqlHelper.ExecuteReader(strConn, System.Data.CommandType.Text, sql, parameters);

                string empID = "";
                string user = "";
                string pass = "";
                string userLevID = "";
                string userLeve = "";
                string empName = "";
                string jobType = "";
                string jobTypeID = "";
                string vanID = "";
                string sup_Van = "";
                bool isChnPwd = false;
                string email = "";
                string van_type = "";
                string _vehicleRegis = "";

                //20200707
                string eDEP = "";
                int IsSVPrintOnline = 0;
                //===

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {

                        empID = dr["EMP_ID"].ToString();
                        user = dr["UserName"].ToString();
                        pass = dr["Password"].ToString();
                        userLevID = dr["LEV_ID"].ToString();
                        userLeve = dr["levelName"].ToString();
                        empName = dr["FName"].ToString() + " " + dr["LName"].ToString();
                        jobType = dr["Type"].ToString();
                        jobTypeID = dr["TYP_ID"].ToString();
                        vanID = dr["VAN_ID"].ToString();
                        sup_Van = dr["SUP_ID"].ToString();
                        isChnPwd = (bool)dr["IsChangPwd"];
                        email = dr["EMail"].ToString();

                        van_type = dr["TYP_ID"].ToString();
                        _vehicleRegis = dr["LicensePlate"].ToString();

                        //20200707
                        eDEP = dr["DEP_ID"].ToString();
                        IsSVPrintOnline = int.Parse( dr["IsSVPrintOnline"].ToString());
                        //========
                    }
                    dr.Close();


                    UserInfo.UserId = empID;
                    UserInfo.UserName = user;
                    UserInfo.Password = _Pass;
                    UserInfo.UserLevID = userLevID;
                    UserInfo.UserLevel = userLeve;
                    UserInfo.FullName = empName;
                    UserInfo.Van_ID = vanID;
                    UserInfo.SUP_VAN = sup_Van;
                    UserInfo.Email = email;

                    UserInfo.Van_Type = van_type;
                    UserInfo.VehicleRegis = _vehicleRegis;


                    UserInfo.ConnectMode = "1";

                    //20200707
                    UserInfo.eDepID = eDEP;
                    UserInfo.IsSVPrintOnline = IsSVPrintOnline;

                    


                    try
                    {
                        string version = "SMSMINI2 [v." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "]";
                        using (var dc = new DAL.SMSManage.SMSManageDataContext())
                        {
                            sql = "UPDATE Users SET SMSVersion = '" + version + "' ,SMSVersionDate=Getdate() WHERE UserName = '" + txtUserName.Text.Trim() + "' ";
                            dc.ExecuteCommand(sql);
                        }
                    }
                    catch (Exception) { }


                    this.Hide();
                    Main f = new Main();
                    if (f.ShowDialog() == DialogResult.No)
                    {
                        this.Show();
                        Login_Load(null, null);
                    }

                }
                else
                {

                    loginattempts++;
                    if (loginattempts >= 3)
                    {
                        MessageBox.Show("UserName และ Password ไม่ถูกต้อง กรุณาระบุใหม่...Login ครั้งที่ " + loginattempts.ToString(), "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Application.Exit();
                    }
                    else
                    {
                        MessageBox.Show("UserName และ Password ไม่ถูกต้อง กรุณาระบุใหม่...Login ครั้งที่ " + loginattempts.ToString(), "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                }

            }
            catch (System.Data.SqlClient.SqlException ex0)
            {
                MessageBox.Show("ไม่พบฐานข้อมูล  Error " + ex0.Message, "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (System.Exception ex1)
            {
                MessageBox.Show("เกิดข้อผิดพลาด  Error " + ex1.Message, "ผลการตรวจสอบ", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }

        }


        private void txtUserName_TextChanged(object sender, EventArgs e)
        { 
        }


        private void txtPassword_TextChanged(object sender, EventArgs e)
        { 
        }


        private void Login_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }


        private void txtUserName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                txtPassword.Focus();
        }


        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                //btLogin.Focus();
                btLogin_Click(null, null);
            }
        }


        private void pictureBox1_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.AppStarting;
                About f = new About();
                f.ShowDialog();

                Cursor.Current = Cursors.Default;
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("เกิดผิดพลาดในการใช้งานเมนูDownload Job Error " + ex.Message, "ข้อผิดพลาด");
            }
        }


        private void btLoadVAN_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.AppStarting;
                this.Hide();
                MasterData.Van f = new SMSMINI.MasterData.Van();
                f.vMode = "unlog";
                if (f.ShowDialog() == DialogResult.Abort)
                    this.Show();

                Cursor.Current = Cursors.Default;
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("เกิดผิดพลาดในการใช้งานเมนู Upload Job Error " + ex.Message, "ข้อผิดพลาด");
            }
        }


        private void loadVANToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.AppStarting;
                this.Hide();
                MasterData.Van f = new SMSMINI.MasterData.Van();
                f.vMode = "unlog";
                if (f.ShowDialog() == DialogResult.Abort)
                    this.Show();

                Cursor.Current = Cursors.Default;
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("เกิดผิดพลาดในการใช้งานเมนู Upload Job Error " + ex.Message, "ข้อผิดพลาด");
            }
        }


        private void updateSMSMINIDatabaseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    Cursor.Current = Cursors.AppStarting;

            //    Download.DownloadManager f = new SMSMINI.Download.DownloadManager();
            //    f.StartPosition = FormStartPosition.CenterScreen;

            //    if (f.ShowDialog() == DialogResult.Abort)
            //    {
            //        if (f.isDownloadComplete)
            //        {
            //            isDownloadDBComplete = f.isDownloadComplete;
            //            Cursor.Current = Cursors.Default;

            //            Application.Exit();
            //        }
            //    }
            //}
            //catch (System.Exception ex)
            //{
            //    MessageBox.Show("เกิดผิดพลาดในการใช้งานเมนู Download ฐานข้อมูล" + ex.Message, "ข้อผิดพลาด");
            //}
        }


        private void exeBatfile(string sourceFileName)
        {
            System.Diagnostics.Process proc = null;
            System.Diagnostics.ProcessStartInfo processStartInfo;

            processStartInfo = new System.Diagnostics.ProcessStartInfo();

            processStartInfo.FileName = sourceFileName;


            if (System.Environment.OSVersion.Version.Major >= 6)  // Windows Vista or higher
            {
                processStartInfo.Verb = "RunAs";
                processStartInfo.Arguments = "/env /user:" + "Administrator";
            }

            processStartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Normal;
            processStartInfo.UseShellExecute = false;

            try
            {
                proc = System.Diagnostics.Process.Start(processStartInfo);
                proc.WaitForExit();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                if (proc != null)
                {
                    proc.Dispose();
                }
            }

        }



        private void label3_Click(object sender, EventArgs e)
        {
            Label lbl = (Label)sender;
            lbl.ContextMenuStrip.Show(lbl, new System.Drawing.Point(0, lbl.Height));
        }

        private void resetPasswordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (txtUserName.Text == "")
            {
                MessageBox.Show("กรุณาระบุ UserName", "ผลการตรวจสอบ",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtUserName.Focus();
                return;
            }

            newProgressbar();

            invoke_Progress("กำลัง Reset Password รอสักครู่...");
            Random random = new Random();
            int actual = random.Next(0000, 9999);

            string _Password = actual.ToString();
            string _User = txtUserName.Text.Trim();
            string emEmail = "";
            string FName = "";

            bool upPass = false;
            try
            {
                invoke_Progress("กำลัง Reset Password... โดยแจ้ง password ทางไป E-Mail...");

                using (DAL.SMSManage.SMSManageDataContext db = new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn))
                {
                    var q = (from u in db.Users
                             from em in db.Employees
                             where u.EMP_ID == em.EMP_ID &&
                             u.UserName == _User
                             select em).FirstOrDefault();
                    if (q != null)
                    {
                        emEmail = q.EMail;
                        FName = q.FName;


                        MailMessage mail = new MailMessage();
                        SmtpClient mailClient = new SmtpClient("mail.flowco.co.th");

                        mail.From = new MailAddress("SMSMileageStatement@flowco.co.th", "SMS Automail");

                        mail.To.Add(emEmail);
                        mail.Subject = "Reset Password";
                        mail.Body = "<h2>เรียนคุณ " + FName + "</h2>" +
                            "<h2>Reset Password</h2>User:=" + _User + "<BR/>Password:=" + _Password;
                        mail.IsBodyHtml = true;

                        mailClient.Port = 25;

                        mailClient.Credentials = new System.Net.NetworkCredential("SMSMileageStatement@flowco.co.th", "SMS@2013");
                        mailClient.EnableSsl = true;

                        ServicePointManager.ServerCertificateValidationCallback =
                            delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
                            { return true; };

                        mailClient.Send(mail);


                        upPass = true;
                    }

                }

            }
            catch (System.Exception ex)
            {
                closeProgress();
                MessageBox.Show(ex.Message, "ข้อผิดพลาด");
                return;
            }

            if (emEmail == "")
            {
                closeProgress();
                MessageBox.Show("ไม่สามารถ Reset password เนื่องจาก คุณไม่มี email ในระบบ", "ข้อผิดพลาด",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            try
            {
                invoke_Progress("กำลัง Reset Password... ปรับปรุงฐานข้อมูล User");
                string key = SMSMINI.Properties.Settings.Default["CryptionKey"].ToString();
                string xpass = dCryp.Encrypt(_Password, key);
                using (DAL.SMSManage.SMSManageDataContext db = new DAL.SMSManage.SMSManageDataContext(strConn))
                {
                    if ((from t in db.Users where t.UserName.Trim() == _User.Trim() select t).Count() > 0)
                    {
                        var u = (from t in db.Users where t.UserName.Trim() == _User.Trim() select t).FirstOrDefault();
                        u.Password = xpass;
                        db.SubmitChanges();
                        upPass = true;
                    }

                }

                if (upPass)
                {
                    using (DAL.SMSManage.SMSManageDataContext db = new SMSMINI.DAL.SMSManage.SMSManageDataContext(strConn))
                    {
                        if ((from t in db.Users where t.UserName.Trim() == _User.Trim() select t).Count() > 0)
                        {
                            var u = (from t in db.Users where t.UserName.Trim() == _User.Trim() select t).FirstOrDefault();
                            u.Password = xpass;
                            db.SubmitChanges();
                        }
                    }
                }

                closeProgress();
                MessageBox.Show("Reset password เรียบร้อย" + Environment.NewLine +
                    "กรุณาเช็ค Password ใน mail: " + emEmail, "ผลการทำงาน", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);

            }
            catch (System.Exception ex)
            {
                closeProgress();
                MessageBox.Show(ex.Message, "ผลการทำงาน");
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            m_fmProgress.Activate();
            m_fmProgress.BringToFront();
            m_fmProgress.TopMost = true;
            m_fmProgress.ShowDialog();
        }
        #region Progressbar

        private void newProgressbar()
        {
            m_fmProgress = new ProgressBar.FormProgress();
            backgroundWorker1.RunWorkerAsync();
            System.Threading.Thread.Sleep(3000);
        }

        private void invoke_Progress(string strMsg)
        {
            try
            {
                m_fmProgress.lblDescription.Invoke(
                   (MethodInvoker)delegate ()
                   {
                       m_fmProgress.lblDescription.Text = strMsg;
                   }
                   );
            }
            catch (System.Exception)
            {
            }
        }

        private void closeProgress()
        {
            if (m_fmProgress != null)
            {
                m_fmProgress.Invoke(
                    (MethodInvoker)delegate ()
                    {
                        m_fmProgress.Hide();
                        m_fmProgress.Dispose();
                        m_fmProgress = null;
                    }
                );
            }
        }

        #endregion


        #region OLD updateScript

        ////private void updateScript()
        ////{
        ////    string sql = "";

        ////    #region Old


        ////    ////string messErr = "";

        ////    //#region Old version

        ////    //////            #region SMS Mini v.1.0.51.2009

        ////    //////            ////==============================================
        ////    //////            ////Start SMS Mini v.1.0.51.2009
        ////    //////            ////==============================================
        ////    //////            //try
        ////    //////            //{
        ////    //////            //    sql = " ALTER TABLE Station " +
        ////    //////            //            " ALTER COLUMN STA_ID1  NVARCHAR(30) NULL" +

        ////    //////            //            " ALTER TABLE JOB_Detail_Spare " +
        ////    //////            //            " ADD Prices  decimal(18,2) NULL," +
        ////    //////            //            "	Discount int NULL";
        ////    //////            //    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);
        ////    //////            //}
        ////    //////            //catch (System.Exception ex) { }

        ////    //////            //try
        ////    //////            //{
        ////    //////            //    sql = " ALTER TABLE SpareParts " +
        ////    //////            //            " ADD IsDiscount  bit  NULL, " +
        ////    //////            //            " IsGlobalDiscount  bit  NULL";
        ////    //////            //    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);
        ////    //////            //}
        ////    //////            //catch (System.Exception ex) { }
        ////    //////            //==============================================
        ////    //////            //End SMS Mini v.1.0.51.2009
        ////    //////            //==============================================

        ////    //////            //==============================================
        ////    //////            //Start SMS Mini v.1.0.53.2009
        ////    //////            //==============================================
        ////    //////            //try
        ////    //////            //{
        ////    //////            //    sql = " delete Station where GOP_ID  in ('FLB01','FLR01','FLW01')";
        ////    //////            //    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);
        ////    //////            //}
        ////    //////            //catch (System.Exception ex) { }

        ////    //////            #endregion

        ////    //////            #region SMS Mini v.1.0.54.2009

        ////    //////            //////==============================================
        ////    //////            //////Start SMS Mini v.1.0.54.2009
        ////    //////            //////==============================================
        ////    //////            ////try
        ////    //////            ////{
        ////    //////            ////    sql = "" +
        ////    //////            ////        //IsCancel
        ////    //////            ////    " if Not Exists(select * from sys.columns where Name = N'IsCancel' and Object_ID = Object_ID(N'JOB_Failure_Detail'))" +
        ////    //////            ////    "   ALTER TABLE JOB_Failure_Detail  ADD IsCancel  char(1) NULL; " +
        ////    //////            ////        //IsDownload
        ////    //////            ////    " if Not Exists(select * from sys.columns where Name = N'IsDownload' and Object_ID = Object_ID(N'JOB_Failure_Detail'))" +
        ////    //////            ////    "   ALTER TABLE JOB_Failure_Detail ADD IsDownload char(1) NULL; ";
        ////    //////            ////    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);
        ////    //////            ////}
        ////    //////            ////catch (System.Exception ex)
        ////    //////            ////{
        ////    //////            ////    messErr += "ALTER TABLE JOB_Failure_Detail Error " + ex.Message + Environment.NewLine;
        ////    //////            ////}

        ////    //////            ////try
        ////    //////            ////{
        ////    //////            ////    sql = "" +
        ////    //////            ////        //IsDownload
        ////    //////            ////        " if Not Exists(select * from sys.columns where Name = N'IsDownload' and Object_ID = Object_ID(N'JOB_Detail_Spare'))" +
        ////    //////            ////        "   ALTER TABLE JOB_Detail_Spare ADD IsDownload  char(1) NULL;  " +
        ////    //////            ////        //SerialNumber
        ////    //////            ////        " if Not Exists(select * from sys.columns where Name = N'SerialNumber' and Object_ID = Object_ID(N'JOB_Detail_Spare'))" +
        ////    //////            ////        "	ALTER TABLE JOB_Detail_Spare ADD SerialNumber nvarchar(50) NULL;    " +
        ////    //////            ////        //VAT
        ////    //////            ////        " if Not Exists(select * from sys.columns where Name = N'VAT' and Object_ID = Object_ID(N'JOB_Detail_Spare'))" +
        ////    //////            ////        "	ALTER TABLE JOB_Detail_Spare ADD VAT numeric(18, 2) NULL;   " +
        ////    //////            ////        //Discount
        ////    //////            ////        " if Not Exists(select * from sys.columns where Name = N'Discount' and Object_ID = Object_ID(N'JOB_Detail_Spare'))" +
        ////    //////            ////        "	ALTER TABLE JOB_Detail_Spare ADD Discount int NULL; " +
        ////    //////            ////        //IsPrices
        ////    //////            ////        " if Not Exists(select * from sys.columns where Name = N'IsPrices' and Object_ID = Object_ID(N'JOB_Detail_Spare'))" +
        ////    //////            ////        "	ALTER TABLE JOB_Detail_Spare ADD IsPrices bit NULL; "+                    
        ////    //////            ////        //Prices
        ////    //////            ////        " if Not Exists(select * from sys.columns where Name = N'Prices' and Object_ID = Object_ID(N'JOB_Detail_Spare'))" +
        ////    //////            ////        "	ALTER TABLE JOB_Detail_Spare ADD Prices numeric(18, 2) NULL";

        ////    //////            ////    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);
        ////    //////            ////}
        ////    //////            ////catch (System.Exception ex)
        ////    //////            ////{
        ////    //////            ////    messErr += "ALTER TABLE JOB_Detail_Spare Error " + ex.Message + Environment.NewLine;
        ////    //////            ////}

        ////    //////            ////try
        ////    //////            ////{
        ////    //////            ////    sql = "" +
        ////    //////            ////        //TRA_ID
        ////    //////            ////    " if Not Exists(select * from sys.columns where Name = N'TRA_ID' and Object_ID = Object_ID(N'JOB_Detail_Spare_LogCabcel'))" +
        ////    //////            ////       "	ALTER TABLE JOB_Detail_Spare_LogCabcel ADD TRA_ID  char(1) NULL;    " +
        ////    //////            ////        //POI_ID
        ////    //////            ////    " if Not Exists(select * from sys.columns where Name = N'POI_ID' and Object_ID = Object_ID(N'JOB_Detail_Spare_LogCabcel'))" +
        ////    //////            ////       "	ALTER TABLE JOB_Detail_Spare_LogCabcel ADD POI_ID char(3) NULL; " +
        ////    //////            ////        //Location
        ////    //////            ////    " if Not Exists(select * from sys.columns where Name = N'Location' and Object_ID = Object_ID(N'JOB_Detail_Spare_LogCabcel'))" +
        ////    //////            ////       "	ALTER TABLE JOB_Detail_Spare_LogCabcel ADD Location  nvarchar(50) NULL; " +
        ////    //////            ////        //FAI_ID
        ////    //////            ////    " if Not Exists(select * from sys.columns where Name = N'FAI_ID' and Object_ID = Object_ID(N'JOB_Detail_Spare_LogCabcel'))" +
        ////    //////            ////       "	ALTER TABLE JOB_Detail_Spare_LogCabcel ADD FAI_ID nvarchar (50) NULL;   ";
        ////    //////            ////    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);
        ////    //////            ////}
        ////    //////            ////catch (System.Exception ex)
        ////    //////            ////{
        ////    //////            ////    messErr += "ALTER TABLE JOB_Detail_Spare_LogCabcel Error " + ex.Message + Environment.NewLine;
        ////    //////            ////}

        ////    //////            ////try
        ////    //////            ////{
        ////    //////            ////    sql = "" +
        ////    //////            ////        //UploadDate
        ////    //////            ////    " if Not Exists(select * from sys.columns where Name = N'UploadDate' and Object_ID = Object_ID(N'JOB_Detail'))" +
        ////    //////            ////     "	ALTER TABLE JOB_Detail ADD UploadDate datetime NULL; ";

        ////    //////            ////    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);
        ////    //////            ////}
        ////    //////            ////catch (System.Exception ex)
        ////    //////            ////{
        ////    //////            ////    messErr += "ALTER TABLE JOB_Detail Error " + ex.Message + Environment.NewLine;
        ////    //////            ////}

        ////    //////            ////if (!string.IsNullOrEmpty(messErr))
        ////    //////            ////{
        ////    //////            ////    MessageBox.Show("การปรับปรุงโครงสร้างฐานข้อมูลเกิดข้อผิดพลาด" + Environment.NewLine +
        ////    //////            ////    messErr + Environment.NewLine + "กรุณาติดต่อผู้พัฒนาระบบ", "ผลการตรวจสอบ การปรับปรุงโครงสร้างฐานข้อมูล", 
        ////    //////            ////    MessageBoxButtons.OK, MessageBoxIcon.Information);
        ////    //////            ////}
        ////    //////            #endregion

        ////    //////            #region SMS MINI PPL v.1.0.66.2009

        ////    //////            ////==============================================
        ////    //////            ////Start SMS Mini v.1.0.66.2009
        ////    //////            ////==============================================
        ////    //////            //try
        ////    //////            //{
        ////    //////            //    sql = "" +
        ////    //////            //        //IsCancel
        ////    //////            //    " if Not Exists(select * from sys.columns where Name = N'IsDelete' and Object_ID = Object_ID(N'SpareParts'))" +
        ////    //////            //    "   ALTER TABLE SpareParts  ADD IsDelete  bit NULL; "+

        ////    //////            //    " if Not Exists(select * from sys.columns where Name = N'DisSysAdmin' and Object_ID = Object_ID(N'Station'))" +
        ////    //////            //    "   ALTER TABLE Station  ADD DisSysAdmin  char(1) NULL; "+
        ////    //////            //    " if Not Exists(select * from sys.columns where Name = N'IsCancel' and Object_ID = Object_ID(N'Station'))" +
        ////    //////            //    "   ALTER TABLE Station  ADD IsCancel  bit NULL; ";

        ////    //////            //    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);
        ////    //////            //}
        ////    //////            //catch (System.Exception ex)
        ////    //////            //{
        ////    //////            //    messErr += "ALTER TABLE SpareParts Error " + ex.Message + Environment.NewLine;
        ////    //////            //}

        ////    //////            #endregion

        ////    //////            #region SMS MINI PPL v.1.0.69.2009
        ////    //////            //try
        ////    //////            //{
        ////    //////            //    sql = "" +
        ////    //////            //        //IsCancel
        ////    //////            //    " if Not Exists(select * from sys.columns where Name = N'IsCancel' and Object_ID = Object_ID(N'JOB_ProblemType'))" +
        ////    //////            //    "   ALTER TABLE JOB_ProblemType  ADD IsCancel  bit NULL; ";



        ////    //////            //    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);
        ////    //////            //}
        ////    //////            //catch (System.Exception ex)
        ////    //////            //{
        ////    //////            //    messErr += "ALTER TABLE SpareParts Error " + ex.Message + Environment.NewLine;
        ////    //////            //}
        ////    //////            #endregion

        ////    //////            #region SMS MINI PPL v.2.0.62.2010
        ////    //////            //try
        ////    //////            //{
        ////    //////            //    sql = "" +
        ////    //////            //        //PricesList
        ////    //////            //    " if Not Exists(select * from sys.columns where Name = N'PricesList' and Object_ID = Object_ID(N'JOB_Detail_Spare'))" +
        ////    //////            //    "   ALTER TABLE JOB_Detail_Spare  ADD PricesList  decimal (18, 2) NULL CONSTRAINT [DF_JOB_Detail_Spare_PricesList]  DEFAULT ((0));";



        ////    //////            //    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);
        ////    //////            //}
        ////    //////            //catch (System.Exception ex)
        ////    //////            //{
        ////    //////            //    messErr += "ALTER TABLE SpareParts Error " + ex.Message + Environment.NewLine;
        ////    //////            //}
        ////    //////            #endregion

        ////    //////            #region SMS MINI PPL v.4.0.4.2010
        ////    //////            try
        ////    //////            {
        ////    //////                sql = "" +
        ////    //////                    //DROP CONSTRAINT
        ////    //////                  " IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='dbo' AND CONSTRAINT_NAME='DF_SpareParts_BRA_ID' AND TABLE_NAME='SpareParts') " + Environment.NewLine +
        ////    //////                  "     ALTER TABLE dbo.SpareParts DROP CONSTRAINT DF_SpareParts_BRA_ID " + Environment.NewLine;
        ////    //////                JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////    //////                try
        ////    //////                {
        ////    //////                    sql = "" +
        ////    //////                          "ALTER TABLE dbo.SpareParts DROP CONSTRAINT DF_SpareParts_BRA_ID ";
        ////    //////                    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);
        ////    //////                }
        ////    //////                catch (Exception)
        ////    //////                {

        ////    //////                }


        ////    //////                sql = "" +
        ////    //////                    //FK_SpareParts_Brand
        ////    //////                  " IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='dbo' AND CONSTRAINT_NAME='FK_SpareParts_Brand' AND TABLE_NAME='Brand') " + Environment.NewLine +
        ////    //////                  "     ALTER TABLE Brand DROP CONSTRAINT FK_SpareParts_Brand " + Environment.NewLine;
        ////    //////                JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);
        ////    //////                sql = "" +

        ////    //////                    " IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='dbo' AND CONSTRAINT_NAME='FK_SpareParts_Brand' AND TABLE_NAME='SpareParts') " + Environment.NewLine +
        ////    //////               "     ALTER TABLE SpareParts DROP CONSTRAINT FK_SpareParts_Brand " + Environment.NewLine;
        ////    //////                JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);
        ////    //////                sql = "" +

        ////    //////                  " IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='dbo' AND CONSTRAINT_NAME='FK_Dispenser_Brand' AND TABLE_NAME='Dispenser') " + Environment.NewLine +
        ////    //////                  "     ALTER TABLE Dispenser DROP CONSTRAINT FK_Dispenser_Brand " + Environment.NewLine;
        ////    //////                JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);


        ////    //////                //PK_Brand
        ////    //////                try
        ////    //////                {
        ////    //////                    sql = "" +

        ////    //////                 " if Exists(select * from sys.columns where Name = N'BRA_ID' and Object_ID = Object_ID(N'Brand')) " + Environment.NewLine +
        ////    //////                 "     ALTER TABLE Brand DROP CONSTRAINT PK_Brand " + Environment.NewLine;
        ////    //////                    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);
        ////    //////                }
        ////    //////                catch (Exception)
        ////    //////                {


        ////    //////                }


        ////    //////                sql = "" +
        ////    //////                  " if Exists(select * from sys.columns where Name = N'BRA_ID' and Object_ID = Object_ID(N'SpareParts')) " + Environment.NewLine +
        ////    //////                  "    ALTER TABLE SpareParts  ALTER COLUMN BRA_ID int NULL" + Environment.NewLine;
        ////    //////                JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////    //////                //Brand
        ////    //////                sql = "" +
        ////    //////                " if Exists(select * from sys.columns where Name = N'BRA_ID' and Object_ID = Object_ID(N'Brand')) " + Environment.NewLine +
        ////    //////                "    ALTER TABLE Brand  ALTER COLUMN BRA_ID int NOT NULL" + Environment.NewLine;
        ////    //////                JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////    //////                // CONSTRAINT PK_Brand
        ////    //////                sql = "" +
        ////    //////                    "   ALTER TABLE Brand WITH NOCHECK  ADD CONSTRAINT PK_Brand PRIMARY KEY CLUSTERED (BRA_ID)  WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80)";
        ////    //////                JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);


        ////    //////                sql = "";
        ////    //////                sql = " " +
        ////    //////                    "ALTER VIEW [dbo].[v_rpt_ServicesReport_web_PartDetail_Reprint] " + Environment.NewLine +
        ////    //////                    " AS " + Environment.NewLine +
        ////    //////                    " SELECT    JD.JOB_ID, JD.POI_ID, JD.NO AS PartLineNo, JD.SPA_ID, JD.Quantity, JD.Comment, JD.StatusSpare, JD.TRA_ID, dbo.SpareParts.SparePart,  " + Environment.NewLine +
        ////    //////                    "   dbo.SpareParts.WH, JD.Location, JD.FAI_ID AS PFAI_ID, JD.IsCancel,  " +
        ////    //////                    " CASE WHEN JOB.PROJ_ID is null or JOB.PROJ_ID = '' then CASE WHEN JD.IsPrices = '1' THEN JD.SumPrices	        else 0 END else 0 END  AS 'SumPrices',  " + Environment.NewLine +
        ////    //////                    " CASE WHEN JOB.PROJ_ID is null or JOB.PROJ_ID = '' then CASE WHEN JD.IsPrices = '1' THEN JD.SumVAT			    else 0 END else 0 END  AS 'SumVAT',  " + Environment.NewLine +
        ////    //////                    " CASE WHEN JOB.PROJ_ID is null or JOB.PROJ_ID = '' then CASE WHEN JD.IsPrices = '1' THEN JD.SumTotalPrices     else 0 END else 0 END  AS 'SumTotalPrices',  " + Environment.NewLine +
        ////    //////                    " CASE WHEN JOB.PROJ_ID is null or JOB.PROJ_ID = '' then CASE WHEN JD.IsPrices = '1' THEN JD.Prices			    else 0 END else 0 END  AS 'Prices', " + Environment.NewLine +
        ////    //////                    " dbo.JOB_Failure_Detail.IsCancel AS FIsCancel, JD.StartDate,JD.Enddate " + Environment.NewLine +
        ////    //////                    " FROM  dbo.JOB_Detail INNER JOIN " + Environment.NewLine +
        ////    //////                    "       dbo.JOB_Detail_Spare AS JD ON dbo.JOB_Detail.JOB_ID = JD.JOB_ID AND dbo.JOB_Detail.StartDate = JD.StartDate INNER JOIN " + Environment.NewLine +
        ////    //////                    "       dbo.SpareParts ON JD.SPA_ID = dbo.SpareParts.SPA_ID INNER JOIN " + Environment.NewLine +
        ////    //////                    "       dbo.JOB_Failure_Detail ON JD.JOB_ID = dbo.JOB_Failure_Detail.JOB_ID AND JD.POI_ID = dbo.JOB_Failure_Detail.POI_ID AND  " + Environment.NewLine +
        ////    //////                    "       JD.Location = dbo.JOB_Failure_Detail.Location AND JD.FAI_ID = dbo.JOB_Failure_Detail.FAI_ID INNER JOIN " + Environment.NewLine +
        ////    //////                    "       dbo.JOB ON JD.JOB_ID = dbo.JOB.JOB_ID " + Environment.NewLine +
        ////    //////                    " WHERE     (JD.IsCancel = '0') OR  (JD.IsCancel IS NULL)" + Environment.NewLine;

        ////    //////                JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);
        ////    //////            }
        ////    //////            catch (System.Exception ex)
        ////    //////            {
        ////    //////                messErr += "ALTER TABLE SpareParts Error " + ex.Message + Environment.NewLine;
        ////    //////            }
        ////    //////            #endregion

        ////    //////            #region SMS MINI V.4.0.41.2010
        ////    //////            try
        ////    //////            {
        ////    //////                sql = "" +
        ////    //////                    //IsCancel
        ////    //////                " if Not Exists(select * from sys.columns where Name = N'GOP_ID_ERP' and Object_ID = Object_ID(N'Station_Group'))" +
        ////    //////                "   ALTER TABLE Station_Group  ADD GOP_ID_ERP  nvarchar(20) NULL; ";
        ////    //////                JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);
        ////    //////            }
        ////    //////            catch (Exception) { }


        ////    //////            #endregion

        ////    //////            #region SMS MINI V.4.0.42.2010
        ////    //////            //STA_ID_ERP
        ////    //////            try
        ////    //////            {
        ////    //////                sql = "" +
        ////    //////                " if Not Exists(select * from sys.columns where Name = N'STA_ID_ERP' and Object_ID = Object_ID(N'STATION'))" +
        ////    //////                "   ALTER TABLE STATION  ADD STA_ID_ERP  nvarchar(20) NULL; ";
        ////    //////                JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);
        ////    //////            }
        ////    //////            catch (Exception) { }

        ////    //////            //ERP_DOC_NO
        ////    //////            try
        ////    //////            {
        ////    //////                sql = "" +
        ////    //////                    //IsCancel
        ////    //////                " if Not Exists(select * from sys.columns where Name = N'ERP_DOC_NO' and Object_ID = Object_ID(N'job_detail_spare'))" +
        ////    //////                "   ALTER TABLE job_detail_spare  ADD ERP_DOC_NO  nvarchar(20) NULL; ";
        ////    //////                JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);
        ////    //////            }
        ////    //////            catch (Exception) { }

        ////    //////            //ERP_DOC_NO_VOID
        ////    //////            try
        ////    //////            {
        ////    //////                sql = "" +
        ////    //////                    //IsCancel
        ////    //////                " if Not Exists(select * from sys.columns where Name = N'ERP_DOC_NO_VOID' and Object_ID = Object_ID(N'job_detail_spare'))" +
        ////    //////                "   ALTER TABLE job_detail_spare  ADD ERP_DOC_NO_VOID  nvarchar(20) NULL; ";
        ////    //////                JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);
        ////    //////            }
        ////    //////            catch (Exception) { }

        ////    //////            //ERP_Message
        ////    //////            try
        ////    //////            {
        ////    //////                sql = "" +
        ////    //////                    //IsCancel
        ////    //////                " if Not Exists(select * from sys.columns where Name = N'ERP_Message' and Object_ID = Object_ID(N'job_detail_spare'))" +
        ////    //////                "   ALTER TABLE job_detail_spare  ADD ERP_Message  nvarchar(100) NULL; ";
        ////    //////                JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);
        ////    //////            }
        ////    //////            catch (Exception) { }

        ////    //////            //WH
        ////    //////            try
        ////    //////            {
        ////    //////                sql = "" +
        ////    //////                " if Not Exists(select * from sys.columns where Name = N'WH' and Object_ID = Object_ID(N'SparePart_TranType'))" +
        ////    //////                "   ALTER TABLE SparePart_TranType  ADD WH  nvarchar(10) NULL; ";
        ////    //////                JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);
        ////    //////            }
        ////    //////            catch (Exception) { }

        ////    //////            //WH
        ////    //////            try
        ////    //////            {
        ////    //////                sql = "" +
        ////    //////                " ALTER VIEW [dbo].[v_rpt_ServicesReport_web] " + Environment.NewLine +
        ////    //////"AS " + Environment.NewLine +
        ////    //////"SELECT DISTINCT  " + Environment.NewLine +
        ////    //////                     " dbo.JOB.JOB_ID, dbo.JOB.Opendate, dbo.JOB.FirstInDate, ISNULL(JOB_Detail_1.StartDate, dbo.JOB_Detail.StartDate) AS StartDate,  " + Environment.NewLine +
        ////    //////                     " dbo.JOB.CloseDate, dbo.JOB.EMP_ID3, ISNULL(dbo.Employees.FName, N'') + N' โทร. ' + dbo.Employees.Phone AS FName, dbo.Failure.Failure_th,  " + Environment.NewLine +
        ////    //////                     " dbo.Priority.Priority_GuestVip, PointFailure_1.PointFailure AS job_PointFailure, dbo.Type.Type, dbo.JOB_Status.JobStatus, Type_1.Type AS Type_1,  " + Environment.NewLine +
        ////    //////                     " dbo.JOB.Informer, dbo.JOB.InPhone, dbo.JOB.JobOutside, dbo.TypeJobClose.TypeCloseJob, dbo.JOB_ProblemType.Problem,  " + Environment.NewLine +
        ////    //////                     " dbo.JOB_Failure_Detail.NO AS fail_NO, dbo.JOB_Failure_Detail.POI_ID AS fail_POI_ID, PointFailure_2.PointFailure AS PointFailure2,  " + Environment.NewLine +
        ////    //////                     " dbo.JOB_Failure_Detail.Failure_Detail, dbo.JOB_Failure_Detail.SerialNumber, JOB_Detail_1.EndDate, JOB_Detail_1.Resole_Detail,  " + Environment.NewLine +
        ////    //////                     " ISNULL(dbo.Station.STA_ID1, dbo.Station.STA_ID) AS STA_ID1, dbo.Station.StationSys, dbo.JOB.FAI_ID, dbo.JOB.PRI_ID,  " + Environment.NewLine +
        ////    //////                     " dbo.JOB.POI_ID AS Job_POI_ID, dbo.JOB.TYP_ID, dbo.JOB.TYP_ID1, dbo.JOB.TCO_ID, dbo.JOB.PMT_ID, dbo.JOB.Contract,  " + Environment.NewLine +
        ////    //////                     " dbo.Station.Address + N' จ. ' + ISNULL(dbo.Province.Province, N'') + N'  ' + ISNULL(dbo.ZONE.Detail, N'') AS Address, dbo.Station.TelNo,  " + Environment.NewLine +
        ////    //////                     " dbo.Station.FaxNo, dbo.JOB.JobFailure_Detail, dbo.JOB_Failure_Detail.StartLiter, dbo.JOB_Failure_Detail.EndLiter, dbo.JOB_Failure_Detail.LiterTest,  " + Environment.NewLine +
        ////    //////                     " dbo.JOB_Failure_Detail.Location, dbo.JOB_Failure_Detail.Model, dbo.JOB_Failure_Detail.trandate, ISNULL(dbo.JOB_WorkID.Work_ID, N'')  " + Environment.NewLine +
        ////    //////                     " AS Work_ID, dbo.JOB_Failure_Detail.FAI_ID AS PFAI_ID, dbo.JOB_Failure_Detail.IsCancel, dbo.JOB.JOBS_ID, dbo.JOB_Detail.JOBS_ID AS JOBS_ID2,  " + Environment.NewLine +
        ////    //////                     " ISNULL(dbo.JOB_WorkID.UserWorkOrder, N'') AS UserWorkOrder " + Environment.NewLine +
        ////    //////" FROM         dbo.Station LEFT OUTER JOIN " + Environment.NewLine +
        ////    //////                     " dbo.ZONE ON dbo.Station.SUB_ZONE = dbo.ZONE.ZONE LEFT OUTER JOIN " + Environment.NewLine +
        ////    //////                     " dbo.Province LEFT OUTER JOIN " + Environment.NewLine +
        ////    //////                     " dbo.Province_Region ON dbo.Province.REG_ID = dbo.Province_Region.REG_ID ON dbo.Station.xPRO_ID = dbo.Province.PRO_ID RIGHT OUTER JOIN " + Environment.NewLine +
        ////    //////                     " dbo.JOB LEFT OUTER JOIN " + Environment.NewLine +
        ////    //////                     " dbo.JOB_WorkID ON dbo.JOB.JOB_ID = dbo.JOB_WorkID.JOB_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////    //////                     " dbo.PointFailure AS PointFailure_2 INNER JOIN " + Environment.NewLine +
        ////    //////                     " dbo.JOB_Failure_Detail ON PointFailure_2.POI_ID = dbo.JOB_Failure_Detail.POI_ID ON dbo.JOB.JOB_ID = dbo.JOB_Failure_Detail.JOB_ID ON  " + Environment.NewLine +
        ////    //////                     " dbo.Station.STA_ID = dbo.JOB.STA_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////    //////                     " dbo.Employees ON dbo.JOB.EMP_ID3 = dbo.Employees.EMP_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////    //////                     " dbo.JOB_ProblemType ON dbo.JOB.PMT_ID = dbo.JOB_ProblemType.PMT_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////    //////                     " dbo.TypeJobClose ON dbo.JOB.TCO_ID = dbo.TypeJobClose.TCO_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////    //////                     " dbo.Type AS Type_1 ON dbo.JOB.TYP_ID1 = Type_1.TYP_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////    //////                     " dbo.JOB_Status ON dbo.JOB.JOBS_ID = dbo.JOB_Status.JOBS_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////    //////                     " dbo.Type ON dbo.JOB.TYP_ID = dbo.Type.TYP_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////    //////                     " dbo.PointFailure AS PointFailure_1 ON dbo.JOB.POI_ID = PointFailure_1.POI_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////    //////                     " dbo.Priority ON dbo.JOB.PRI_ID = dbo.Priority.PRI_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////    //////                     " dbo.Failure ON dbo.JOB.FAI_ID = dbo.Failure.FAI_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////    //////                     " dbo.JOB_Detail AS JOB_Detail_1 ON dbo.JOB.JOB_ID = JOB_Detail_1.JOB_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////    //////                     " dbo.JOB_Detail ON dbo.JOB.JOB_ID = dbo.JOB_Detail.JOB_ID " + Environment.NewLine +
        ////    //////" WHERE     (dbo.JOB_Detail.JOBS_ID > '03') AND (dbo.JOB_Failure_Detail.IsCancel = '0' OR " + Environment.NewLine +
        ////    //////                     " dbo.JOB_Failure_Detail.IsCancel IS NULL) ";

        ////    //////                JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);
        ////    //////            }
        ////    //////            catch (Exception) { }

        ////    //////            #endregion

        ////    //////            #region SMS MINI V.4.0.43.2010

        ////    //////            try
        ////    //////            {
        ////    //////                sql = "";
        ////    //////                sql = " if Not Exists(select * from sys.columns where Name = N'VAN_ID_ERP' and Object_ID = Object_ID(N'VAN')) " + Environment.NewLine +
        ////    //////                      "     ALTER TABLE VAN  ADD VAN_ID_ERP nvarchar(20) NULL; ";

        ////    //////                JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);
        ////    //////            }
        ////    //////            catch (Exception)
        ////    //////            {
        ////    //////            }

        ////    //////            #endregion

        ////    //////            #region SMS MINI V.5.0.2.2011

        ////    //////            try
        ////    //////            {
        ////    //////                sql = "";
        ////    //////                sql = " if Not Exists(select * from sys.columns where Name = N'GOP_ID_ERP' and Object_ID = Object_ID(N'Station_Group')) " + Environment.NewLine +
        ////    //////                      "     ALTER TABLE Station_Group  ADD GOP_ID_ERP nvarchar(20) NULL; ";

        ////    //////                JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);
        ////    //////            }
        ////    //////            catch (Exception)
        ////    //////            {
        ////    //////            }

        ////    //////            #endregion

        ////    //////            #region SMS MINI V.5.0.3.2011

        ////    //////            //try
        ////    //////            //{
        ////    //////            //    sql = "";
        ////    //////            //    sql = " if Not Exists(select * from sys.columns where Name = N'OtherDiscount3' and Object_ID = Object_ID(N'SparePart_Quarter')) " + Environment.NewLine +
        ////    //////            //          "     ALTER TABLE SparePart_Quarter  ADD OtherDiscount3 numeric(18, 2) NULL; ";
        ////    //////            //    sql += " if Not Exists(select * from sys.columns where Name = N'OtherDiscount4' and Object_ID = Object_ID(N'SparePart_Quarter')) " + Environment.NewLine +
        ////    //////            //          "     ALTER TABLE SparePart_Quarter  ADD OtherDiscount4 numeric(18, 2) NULL; ";
        ////    //////            //    sql += " if Not Exists(select * from sys.columns where Name = N'OtherDiscount5' and Object_ID = Object_ID(N'SparePart_Quarter')) " + Environment.NewLine +
        ////    //////            //          "     ALTER TABLE SparePart_Quarter  ADD OtherDiscount5 numeric(18, 2) NULL; ";
        ////    //////            //    sql += " if Not Exists(select * from sys.columns where Name = N'OtherDiscount6' and Object_ID = Object_ID(N'SparePart_Quarter')) " + Environment.NewLine +
        ////    //////            //          "     ALTER TABLE SparePart_Quarter  ADD OtherDiscount6 numeric(18, 2) NULL; ";
        ////    //////            //    sql += " if Not Exists(select * from sys.columns where Name = N'OtherDiscount6' and Object_ID = Object_ID(N'SparePart_Quarter')) " + Environment.NewLine +
        ////    //////            //          "     ALTER TABLE SparePart_Quarter  ADD OtherDiscount6 numeric(18, 2) NULL; ";
        ////    //////            //    sql += " if Not Exists(select * from sys.columns where Name = N'OtherDiscount7' and Object_ID = Object_ID(N'SparePart_Quarter')) " + Environment.NewLine +
        ////    //////            //          "     ALTER TABLE SparePart_Quarter  ADD OtherDiscount7 numeric(18, 2) NULL; ";
        ////    //////            //    sql += " if Not Exists(select * from sys.columns where Name = N'OtherDiscount8' and Object_ID = Object_ID(N'SparePart_Quarter')) " + Environment.NewLine +
        ////    //////            //          "     ALTER TABLE SparePart_Quarter  ADD OtherDiscount8 numeric(18, 2) NULL; ";

        ////    //////            //    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////    //////            //}
        ////    //////            //catch (Exception)
        ////    //////            //{
        ////    //////            //}

        ////    //////            #endregion

        ////    //////            #region SMS MINI V.5.1.3.2011
        ////    //////            //IsSpecialCharge
        ////    //////            try
        ////    //////            {

        ////    //////                sql = "";
        ////    //////                sql = " if Not Exists(select * from sys.columns where Name = N'IsSpecialCharge' and Object_ID = Object_ID(N'JOB')) " + Environment.NewLine +
        ////    //////                      "     ALTER TABLE JOB  ADD IsSpecialCharge char(1) NULL; ";

        ////    //////                JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////    //////            }
        ////    //////            catch (Exception)
        ////    //////            {
        ////    //////            }
        ////    //////            #endregion

        ////    //////            #region  SMS MINI V.5.1.4.2011
        ////    //////            try
        ////    //////            {
        ////    //////                //v_rpt_PartPricesList
        ////    //////                sql = "";
        ////    //////                sql = " ALTER VIEW  v_rpt_PartPricesList " + Environment.NewLine +
        ////    //////                    " AS " + Environment.NewLine +
        ////    //////                    " SELECT      SparePart_Quarter.GOP_ID,  Station_Group.GroupStation,  SparePart_Quarter.StartDate,  SparePart_Quarter.EndDate,  " + Environment.NewLine +
        ////    //////                    "                        SparePart_Quarter.Quarter,  SpareParts_IsPrices.SPA_ID,  SpareParts.SparePart,  SparePart_Prices_List.Prices,  " + Environment.NewLine +
        ////    //////                    "                        SpareParts_IsPrices.IsPrices,  SpareParts_IsPrices.IsGlobalDiscount,  SparePart_Quarter.globalDiscount,  " + Environment.NewLine +
        ////    //////                    "                        SparePart_Quarter.localDiscount,  SparePart_Quarter.decRoundUp,  SparePart_Quarter.PRI_ID " + Environment.NewLine +
        ////    //////                    " FROM          Station_Group INNER JOIN " + Environment.NewLine +
        ////    //////                    "                        SpareParts_IsPrices ON  Station_Group.GOP_ID =  SpareParts_IsPrices.GOP_ID INNER JOIN " + Environment.NewLine +
        ////    //////                    "                        SparePart_Quarter ON  Station_Group.GOP_ID =  SparePart_Quarter.GOP_ID INNER JOIN " + Environment.NewLine +
        ////    //////                    "                        SpareParts ON  SpareParts_IsPrices.SPA_ID =  SpareParts.SPA_ID INNER JOIN " + Environment.NewLine +
        ////    //////                    "                        SparePart_Prices_List ON  SpareParts.SPA_ID =  SparePart_Prices_List.SPA_ID AND  " + Environment.NewLine +
        ////    //////                    "                        SparePart_Quarter.PRI_ID =  SparePart_Prices_List.PRI_ID " + Environment.NewLine;

        ////    //////                JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////    //////            }
        ////    //////            catch (Exception)
        ////    //////            {

        ////    //////                throw;
        ////    //////            }

        ////    //////            try
        ////    //////            {
        ////    //////                //vWait_DistributeJob
        ////    //////                sql = "";
        ////    //////                sql = " ALTER VIEW  vWait_DistributeJob  " + Environment.NewLine +
        ////    //////                    " AS  " + Environment.NewLine +
        ////    //////                    " SELECT DISTINCT   " + Environment.NewLine +
        ////    //////                    "                        JOB.JOB_ID,  JOB.Opendate,  JOB.Informer,  JOB.InPhone,  Failure.Failure_th AS Failure,  JOB.JobFailure_Detail,   " + Environment.NewLine +
        ////    //////                    "                        Station.STA_ID,  Station.StationSys,  JOB.JOBS_ID,  JOB_Status.JobStatus,  JOB.TYP_ID,  Type.Type,  JOB.PMT_ID,   " + Environment.NewLine +
        ////    //////                    "                        JOB.EMP_ID0,  JOB.EMP_ID1,  JOB.EMP_ID2,  JOB.EMP_ID3,  VAN.VAN_ID,  JOB.FAI_ID,  Employees.FName,   " + Environment.NewLine +
        ////    //////                    "                        Employees.Phone,  VAN.LicensePlate,  Station.STA_ID1,  JOB_Project.PROJ_ID, CONVERT(varchar,  JOB.Opendate, 112)   " + Environment.NewLine +
        ////    //////                    "                       AS Opendate1, ISNULL( JOB_WorkID.Work_ID, N'') AS Work_ID  " + Environment.NewLine +
        ////    //////                    " FROM          Failure RIGHT OUTER JOIN  " + Environment.NewLine +
        ////    //////                    "                        JOB INNER JOIN  " + Environment.NewLine +
        ////    //////                    "                        Station ON  JOB.STA_ID =  Station.STA_ID INNER JOIN  " + Environment.NewLine +
        ////    //////                    "                        JOB_Status ON  JOB.JOBS_ID =  JOB_Status.JOBS_ID INNER JOIN  " + Environment.NewLine +
        ////    //////                    "                        Type ON  JOB.TYP_ID1 =  Type.TYP_ID ON  Failure.FAI_ID =  JOB.FAI_ID LEFT OUTER JOIN  " + Environment.NewLine +
        ////    //////                    "                        JOB_WorkID ON  JOB.JOB_ID =  JOB_WorkID.JOB_ID LEFT OUTER JOIN  " + Environment.NewLine +
        ////    //////                    "                        VAN INNER JOIN  " + Environment.NewLine +
        ////    //////                    "                        Employees ON  VAN.EMP_ID =  Employees.EMP_ID ON  JOB.EMP_ID3 =  VAN.EMP_ID LEFT OUTER JOIN  " + Environment.NewLine +
        ////    //////                    "                        JOB_Project ON  JOB.PROJ_ID =  JOB_Project.PROJ_ID " + Environment.NewLine;
        ////    //////                JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);
        ////    //////            }
        ////    //////            catch (Exception)
        ////    //////            {

        ////    //////                throw;
        ////    //////            }

        ////    //////            try
        ////    //////            {
        ////    //////                //vWait_DistributeJob
        ////    //////                sql = "";
        ////    //////                sql = " ALTER VIEW  vWait_DistributeJob_Serch  " + Environment.NewLine +
        ////    //////                    " AS  " + Environment.NewLine +
        ////    //////                    " SELECT     JOB.JOB_ID, JOB.Opendate, JOB.Informer, JOB.InPhone, Failure.Failure_th AS Failure, JOB.JobFailure_Detail,   " + Environment.NewLine +
        ////    //////                    "                       Station.STA_ID, Station.StationSys, VAN.VAN_ID, Employees.FName, Employees.LName, Employees.Phone,   " + Environment.NewLine +
        ////    //////                    "                       JOB.JOBS_ID, VAN.LicensePlate  " + Environment.NewLine +
        ////    //////                    " FROM         Employees INNER JOIN  " + Environment.NewLine +
        ////    //////                    "                       VAN ON Employees.EMP_ID = VAN.EMP_ID RIGHT OUTER JOIN  " + Environment.NewLine +
        ////    //////                    "                       VAN_Support ON VAN.VAN_ID = VAN_Support.VAN_ID RIGHT OUTER JOIN  " + Environment.NewLine +
        ////    //////                    "                       JOB LEFT OUTER JOIN  " + Environment.NewLine +
        ////    //////                    "                       Station ON JOB.STA_ID = Station.STA_ID LEFT OUTER JOIN  " + Environment.NewLine +
        ////    //////                    "                       Failure ON JOB.FAI_ID = Failure.FAI_ID ON VAN_Support.STA_ID = Station.STA_ID   " + Environment.NewLine;

        ////    //////                JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);
        ////    //////            }
        ////    //////            catch (Exception)
        ////    //////            {

        ////    //////                throw;
        ////    //////            }

        ////    //////            #endregion

        ////    //////            #region SMS MINI V.5.1.3.2011
        ////    //////            try
        ////    //////            {

        ////    //////                sql = "";
        ////    //////                sql = " if Not Exists(select * from sys.columns where Name = N'OtherDiscount3' and Object_ID = Object_ID(N'SparePart_Quarter')) " + Environment.NewLine +
        ////    //////                      "     ALTER TABLE SparePart_Quarter  ADD OtherDiscount3 numeric(18, 2) NULL;";

        ////    //////                JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////    //////                sql = "";
        ////    //////                sql = " if Not Exists(select * from sys.columns where Name = N'OtherDiscount4' and Object_ID = Object_ID(N'SparePart_Quarter')) " + Environment.NewLine +
        ////    //////                      "     ALTER TABLE SparePart_Quarter  ADD OtherDiscount4 numeric(18, 2) NULL;";

        ////    //////                JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////    //////                sql = "";
        ////    //////                sql = " if Not Exists(select * from sys.columns where Name = N'OtherDiscount5' and Object_ID = Object_ID(N'SparePart_Quarter')) " + Environment.NewLine +
        ////    //////                      "     ALTER TABLE SparePart_Quarter  ADD OtherDiscount5 numeric(18, 2) NULL;";

        ////    //////                JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////    //////                sql = "";
        ////    //////                sql = " if Not Exists(select * from sys.columns where Name = N'OtherDiscount6' and Object_ID = Object_ID(N'SparePart_Quarter')) " + Environment.NewLine +
        ////    //////                      "     ALTER TABLE SparePart_Quarter  ADD OtherDiscount6 numeric(18, 2) NULL;";

        ////    //////                JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////    //////                sql = "";
        ////    //////                sql = " if Not Exists(select * from sys.columns where Name = N'OtherDiscount7' and Object_ID = Object_ID(N'SparePart_Quarter')) " + Environment.NewLine +
        ////    //////                      "     ALTER TABLE SparePart_Quarter  ADD OtherDiscount7 numeric(18, 2) NULL;";

        ////    //////                JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////    //////                sql = "";
        ////    //////                sql = " if Not Exists(select * from sys.columns where Name = N'OtherDiscount8' and Object_ID = Object_ID(N'SparePart_Quarter')) " + Environment.NewLine +
        ////    //////                      "     ALTER TABLE SparePart_Quarter  ADD OtherDiscount8 numeric(18, 2) NULL;";

        ////    //////                JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////    //////            }
        ////    //////            catch (Exception)
        ////    //////            {
        ////    //////            }
        ////    //////            #endregion
        ////    //////            //IsContractSys
        ////    //////            #region SMS MINI V.5.1.4.2011
        ////    //////            try
        ////    //////            {

        ////    //////                sql = "";
        ////    //////                sql = " if Not Exists(select * from sys.columns where Name = N'IsContractSys' and Object_ID = Object_ID(N'Station')) " + Environment.NewLine +
        ////    //////                      "     ALTER TABLE Station  ADD IsContractSys bit NULL;";

        ////    //////                JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);


        ////    //////                sql = "";
        ////    //////                sql = " if Not Exists(select * from sys.columns where Name = N'LastUpdated' and Object_ID = Object_ID(N'Station')) " + Environment.NewLine +
        ////    //////                      "     ALTER TABLE Station  ADD LastUpdated datetime NULL;";

        ////    //////                JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);


        ////    //////                sql = "";
        ////    //////                sql = " if Not Exists(select * from sys.columns where Name = N'LastUpdated' and Object_ID = Object_ID(N'Station_Group')) " + Environment.NewLine +
        ////    //////                      "     ALTER TABLE Station_Group  ADD LastUpdated datetime NULL;";

        ////    //////                JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);


        ////    //////                sql = "";
        ////    //////                sql = " if Not Exists(select * from sys.columns where Name = N'ERP_EMP_ID' and Object_ID = Object_ID(N'Employees')) " + Environment.NewLine +
        ////    //////                      "     ALTER TABLE Employees  ADD ERP_EMP_ID nvarchar(8) NULL;";

        ////    //////                JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////    //////                sql = "";
        ////    //////                sql = " if Not Exists(select * from sys.columns where Name = N'IsDelte' and Object_ID = Object_ID(N'Employees')) " + Environment.NewLine +
        ////    //////                      "     ALTER TABLE Employees  ADD IsDelte bit NULL;";

        ////    //////                JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////    //////                sql = "";
        ////    //////                sql = " if Not Exists(select * from sys.columns where Name = N'LastUpdated' and Object_ID = Object_ID(N'Employees')) " + Environment.NewLine +
        ////    //////                     "     ALTER TABLE Employees  ADD LastUpdated datetime NULL;";

        ////    //////                JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);




        ////    //////            }
        ////    //////            catch (Exception)
        ////    //////            {
        ////    //////            }
        ////    //////            #endregion

        ////    //////            #region SMS MINI V.5.4.1.2011
        ////    //////            //IsSpecialCharge
        ////    //////            try
        ////    //////            {

        ////    //////                sql = "";

        ////    //////                //JOBLimitPrices
        ////    //////                sql = " if Not Exists(select * from sys.columns where Name = N'JOBLimitPrices' and Object_ID = Object_ID(N'JOB')) " + Environment.NewLine +
        ////    //////                      "     ALTER TABLE JOB  ADD JOBLimitPrices decimal(18, 2) NULL; " +
        ////    //////                    //JCC_ID
        ////    //////                      " if Not Exists(select * from sys.columns where Name = N'JCC_ID' and Object_ID = Object_ID(N'JOB')) " + Environment.NewLine +
        ////    //////                      "     ALTER TABLE JOB  ADD JCC_ID int NULL; " +



        ////    //////                //ContractNo               
        ////    //////                      " if Not Exists(select * from sys.columns where Name = N'ContractNo' and Object_ID = Object_ID(N'JOB')) " + Environment.NewLine +
        ////    //////                      "     ALTER TABLE JOB  ADD ContractNo nvarchar(30) NULL; " +



        ////    //////                //Counselor                
        ////    //////                      " if Not Exists(select * from sys.columns where Name = N'Counselor' and Object_ID = Object_ID(N'JOB')) " + Environment.NewLine +
        ////    //////                      "     ALTER TABLE JOB  ADD Counselor nvarchar(80) NULL; " +

        ////    //////                //IsDelete
        ////    //////                    " if Not Exists(select * from sys.columns where Name = N'IsDelete' and Object_ID = Object_ID(N'SpareParts')) " + Environment.NewLine +
        ////    //////                    "     ALTER TABLE SpareParts  ADD IsDelete bit NULL; " +

        ////    //////                    " if Not Exists(select * from sys.columns where Name = N'IsDelete' and Object_ID = Object_ID(N'Employees')) " + Environment.NewLine +
        ////    //////                    "     ALTER TABLE Employees  ADD IsDelete bit NULL; " +

        ////    //////                //MType
        ////    //////                " if Not Exists(select * from sys.columns where Name = N'MType' and Object_ID = Object_ID(N'Type')) " + Environment.NewLine +
        ////    //////                "     ALTER TABLE Type  ADD MType char(2) NULL;" +

        ////    //////                    //BillingCost char (1) NULL,
        ////    //////                " if Not Exists(select * from sys.columns where Name = N'BillingCost' and Object_ID = Object_ID(N'JOB_Detail_Spare')) " + Environment.NewLine +
        ////    //////                "     ALTER TABLE JOB_Detail_Spare  ADD BillingCost char(1) NULL;" +

        ////    //////                    //ERP_DOC_NO nvarchar (40) NULL,
        ////    //////                " if Not Exists(select * from sys.columns where Name = N'ERP_DOC_NO' and Object_ID = Object_ID(N'JOB_Detail_Spare')) " + Environment.NewLine +
        ////    //////                "     ALTER TABLE JOB_Detail_Spare  ADD ERP_DOC_NO nvarchar (40) NULL;" +

        ////    //////                    //ERP_DOC_NO_VOID nvarchar (40) NULL,
        ////    //////                " if Not Exists(select * from sys.columns where Name = N'ERP_DOC_NO_VOID' and Object_ID = Object_ID(N'JOB_Detail_Spare')) " + Environment.NewLine +
        ////    //////                "     ALTER TABLE JOB_Detail_Spare  ADD ERP_DOC_NO_VOID nvarchar (40) NULL;" +

        ////    //////                    //ERP_Message nvarchar (100) NULL,
        ////    //////                " if Not Exists(select * from sys.columns where Name = N'ERP_Message' and Object_ID = Object_ID(N'JOB_Detail_Spare')) " + Environment.NewLine +
        ////    //////                "     ALTER TABLE JOB_Detail_Spare  ADD ERP_Message nvarchar (100) NULL;" +

        ////    //////                    //ERP_INVOICE_NO nvarchar (40) NULL,
        ////    //////                " if Not Exists(select * from sys.columns where Name = N'ERP_INVOICE_NO' and Object_ID = Object_ID(N'JOB_Detail_Spare')) " + Environment.NewLine +
        ////    //////                "     ALTER TABLE JOB_Detail_Spare  ADD ERP_INVOICE_NO nvarchar (40) NULL;" +

        ////    //////                    //ERP_DOCST_NO nvarchar (40) NULL,
        ////    //////                " if Not Exists(select * from sys.columns where Name = N'ERP_DOCST_NO' and Object_ID = Object_ID(N'JOB_Detail_Spare')) " + Environment.NewLine +
        ////    //////                "     ALTER TABLE JOB_Detail_Spare  ADD ERP_DOCST_NO nvarchar (40) NULL;" +

        ////    //////                    //ERP_LASTUPDATE datetime NULL,
        ////    //////                " if Not Exists(select * from sys.columns where Name = N'ERP_LASTUPDATE' and Object_ID = Object_ID(N'JOB_Detail_Spare')) " + Environment.NewLine +
        ////    //////                "     ALTER TABLE JOB_Detail_Spare  ADD ERP_LASTUPDATE datetime NULL;" +

        ////    //////                    //ERP_QTYORDER float NULL,
        ////    //////                " if Not Exists(select * from sys.columns where Name = N'ERP_QTYORDER' and Object_ID = Object_ID(N'JOB_Detail_Spare')) " + Environment.NewLine +
        ////    //////                "     ALTER TABLE JOB_Detail_Spare  ADD ERP_QTYORDER float NULL;" +

        ////    //////                    //ERP_bpartner nvarchar (20) NULL,
        ////    //////                " if Not Exists(select * from sys.columns where Name = N'ERP_bpartner' and Object_ID = Object_ID(N'JOB_Detail_Spare')) " + Environment.NewLine +
        ////    //////                "     ALTER TABLE JOB_Detail_Spare  ADD ERP_bpartner nvarchar (20) NULL;" +

        ////    //////                    //ERP_StationCharge char (1) NULL,             
        ////    //////                " if Not Exists(select * from sys.columns where Name = N'ERP_StationCharge' and Object_ID = Object_ID(N'JOB_Detail_Spare')) " + Environment.NewLine +
        ////    //////                "     ALTER TABLE JOB_Detail_Spare  ADD ERP_StationCharge char (1) NULL;" +

        ////    //////                    //ERP_LoadTime datetime NULL,
        ////    //////                " if Not Exists(select * from sys.columns where Name = N'ERP_LoadTime' and Object_ID = Object_ID(N'JOB_Detail_Spare')) " + Environment.NewLine +
        ////    //////                "     ALTER TABLE JOB_Detail_Spare  ADD ERP_LoadTime datetime NULL;";

        ////    //////                JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////    //////            }
        ////    //////            catch (Exception)
        ////    //////            {
        ////    //////            }


        ////    //////            try
        ////    //////            {

        ////    //////                sql = "";

        ////    //////                    //CSS_Score_Total
        ////    //////                sql = "  if Not Exists(select * from sys.columns where Name = N'CSS_Score_Total' and Object_ID = Object_ID(N'Station')) " + Environment.NewLine +
        ////    //////                    "     ALTER TABLE Station  ADD CSS_Score_Total decimal(18, 2) NULL;" +

        ////    //////                    //IsCancel
        ////    //////                    "  if Not Exists(select * from sys.columns where Name = N'IsCancel' and Object_ID = Object_ID(N'JOB_ProblemType')) " + Environment.NewLine +
        ////    //////                    "     ALTER TABLE JOB_ProblemType  ADD IsCancel bit NULL;"+

        ////    //////                //IsCancel
        ////    //////                "  if Not Exists(select * from sys.columns where Name = N'IsCancel' and Object_ID = Object_ID(N'Failure')) " + Environment.NewLine +
        ////    //////                "     ALTER TABLE Failure  ADD IsCancel bit NULL;";


        ////    //////                JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);


        ////    //////                //
        ////    //////            }
        ////    //////            catch (Exception)
        ////    //////            {

        ////    //////            }
        ////    //////            #endregion

        ////    //////            #region "SMS MINI V.5.5.0.2011"

        ////    //////            try
        ////    //////            {

        ////    //////                sql = "";

        ////    //////                //EndDate_GPS
        ////    //////                sql = "  if Not Exists(select * from sys.columns where Name = N'EndDate_GPS' and Object_ID = Object_ID(N'JOB_Detail')) " + Environment.NewLine +
        ////    //////                    "     ALTER TABLE JOB_Detail  ADD EndDate_GPS datetime NULL;"+

        ////    //////                //IsPopup
        ////    //////                "  if Not Exists(select * from sys.columns where Name = N'IsPopup' and Object_ID = Object_ID(N'Station_Group')) " + Environment.NewLine +
        ////    //////                "     ALTER TABLE Station_Group  ADD IsPopup bit NULL;";


        ////    //////                JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);               
        ////    //////            }
        ////    //////            catch (Exception)
        ////    //////            {

        ////    //////            }

        ////    //////            #endregion

        ////    //#endregion

        ////    ////STGOP_ID_ERP
        ////    //#region "SMS MINI V.5.5.0.2011"

        ////    ////try
        ////    ////{
        ////    ////    //STGOP_ID_ERP
        ////    ////    sql = "";
        ////    ////    //IsPopup
        ////    ////    sql = "   if Not Exists(select * from sys.columns where Name = N'STGOP_ID_ERP' and Object_ID = Object_ID(N'Station')) " + Environment.NewLine +
        ////    ////         "    ALTER TABLE Station  ADD STGOP_ID_ERP nvarchar(5) NULL;"; 
        ////    ////    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////    ////    sql = "   if Not Exists(select * from sys.columns where Name = N'IsJobInGPS' and Object_ID = Object_ID(N'JOB_Detail')) " + Environment.NewLine +
        ////    ////        "    ALTER TABLE JOB_Detail ADD IsJobInGPS nchar(1) NULL;";
        ////    ////    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);


        ////    ////}
        ////    ////catch (Exception)
        ////    ////{

        ////    ////}

        ////    //#endregion

        ////    //#region SMS MINI V.5.6.x.2011
        ////    //try
        ////    //{
        ////    //    sql = "";

        ////    //    sql = "   if Not Exists(select * from sys.columns where Name = N'FCAT_ID' and Object_ID = Object_ID(N'JOB_Failure_Detail')) " + Environment.NewLine +
        ////    //         "    ALTER TABLE JOB_Failure_Detail  ADD FCAT_ID int NULL;";
        ////    //    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////    //    sql = "   if Not Exists(select * from sys.columns where Name = N'FBA_ID' and Object_ID = Object_ID(N'JOB_Failure_Detail')) " + Environment.NewLine +
        ////    //        "    ALTER TABLE JOB_Failure_Detail  ADD FBA_ID int NULL;";
        ////    //    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////    //    sql = "   if Not Exists(select * from sys.columns where Name = N'FMD_ID' and Object_ID = Object_ID(N'JOB_Failure_Detail')) " + Environment.NewLine +
        ////    //      "    ALTER TABLE JOB_Failure_Detail  ADD FMD_ID int NULL;";
        ////    //    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);


        ////    //}
        ////    //catch (Exception)
        ////    //{

        ////    //}
        ////    //#endregion

        ////    //#region SMS MINI V.5.7.3.2011
        ////    //try
        ////    //{
        ////    //    sql = "";

        ////    //    sql = "   if Not Exists(select * from sys.columns where Name = N'InstallDate' and Object_ID = Object_ID(N'FFixedAsset_SerialNumber_Pool')) " + Environment.NewLine +
        ////    //         "    ALTER TABLE FFixedAsset_SerialNumber_Pool  ADD InstallDate datetime NULL;";
        ////    //    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////    //    sql = "   if Not Exists(select * from sys.columns where Name = N'ExpiryDate' and Object_ID = Object_ID(N'FFixedAsset_SerialNumber_Pool')) " + Environment.NewLine +
        ////    //        "    ALTER TABLE FFixedAsset_SerialNumber_Pool  ADD ExpiryDate datetime NULL;";
        ////    //    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////    //}
        ////    //catch (Exception)
        ////    //{

        ////    //}
        ////    //#endregion

        ////    #endregion

        ////    #region SMS MINI V.5.7.3.2011 > ALTER COLUMN
        ////    //try
        ////    //{
        ////    //    sql = "";

        ////    //    sql = "   ALTER TABLE  JOB_Detail_Spare ALTER COLUMN Quantity decimal(18,2) NULL;";
        ////    //    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////    //    sql = "   ALTER TABLE  JOB_Detail_REQ_Detail ALTER COLUMN Quantity_JOB decimal(18,2) NULL; ";
        ////    //    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////    //    sql = "   ALTER TABLE  JOB_Detail_REQ_Detail ALTER COLUMN Quantity_REQ decimal(18,2) NULL;";
        ////    //    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////    //}
        ////    //catch (Exception)
        ////    //{

        ////    //}
        ////    #endregion

        ////    #region SMS MINI V.6.7.0.2011 > ALTER COLUMN

        ////    //try
        ////    //{

        ////    //    sql = "";

        ////    //    sql = " IF NOT EXISTS (SELECT * FROM ContentMentDetail where CMD_ID='00') Insert into ContentMentDetail values('00','ความคิดเห็นของเจ้าหน้าที่ Flowco')";
        ////    //    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////    //    sql = " IF NOT EXISTS (SELECT * FROM ContentMentDetail where CMD_ID='01') Insert into ContentMentDetail values('01','การนัดหมายก่อนเข้าสถานี')";
        ////    //    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////    //    sql = " IF NOT EXISTS (SELECT * FROM ContentMentDetail where CMD_ID='02') Insert into ContentMentDetail values('02','การตรงต่อเวลาที่นัด')";
        ////    //    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////    //    sql = " IF NOT EXISTS (SELECT * FROM ContentMentDetail where CMD_ID='03') Insert into ContentMentDetail values('03','การแต่งกายและมารยาท')";
        ////    //    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////    //    sql = " IF NOT EXISTS (SELECT * FROM ContentMentDetail where CMD_ID='04') Insert into ContentMentDetail values('04','ความรู้ความสามารถในการซ่อม')";
        ////    //    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////    //    sql = " IF NOT EXISTS (SELECT * FROM ContentMentDetail where CMD_ID='05') Insert into ContentMentDetail values('05','ความพร้อมของอะไหล่')";
        ////    //    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);


        ////    //}
        ////    //catch (Exception)
        ////    //{

        ////    //}



        ////    //try
        ////    //{
        ////    //    sql = "  IF EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[v_Input_JOB_ContentMent]')) " +
        ////    //            " DROP VIEW [dbo].[v_Input_JOB_ContentMent] ";
        ////    //    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////    //    sql = "  CREATE VIEW [dbo].[v_Input_JOB_ContentMent] " +
        ////    //            " AS " +
        ////    //            " SELECT     CMD_ID, ContentMentDetail, CAST(0 AS bit) AS [(A+)ดีมาก], CAST(0 AS bit) AS [(A)ดี], CAST(0 AS bit) AS [(B)พอใจ], CAST(0 AS bit) AS [(C)ปรับปรุง] " +
        ////    //            " FROM         dbo.ContentMentDetail " +
        ////    //            " WHERE     (CMD_ID <> '00') ";
        ////    //    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////    //}
        ////    //catch (Exception)
        ////    //{


        ////    //}


        ////    #endregion

        ////    #region SMS MINI V.9.0.1.2012 > DROP TRIGGER trg_Add_Users_Public_Station
        ////    //try
        ////    //{
        ////    //    sql = "";

        ////    //    sql = " IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[trg_Add_Users_Public_Station]')) " +
        ////    //          " DROP TRIGGER [dbo].[trg_Add_Users_Public_Station]";
        ////    //    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////    //}
        ////    //catch (Exception)
        ////    //{

        ////    //}
        ////    #endregion

        ////    #region SMS MINI V.9.0.10.2012 > DROP TRIGGER trg_Add_Users_Public_Station
        ////    //try
        ////    //{
        ////    //    sql = "";

        ////    //    sql = "   if Not Exists(select * from sys.columns where Name = N'IsCheckTranType' and Object_ID = Object_ID(N'Conf_Contract')) " + Environment.NewLine +
        ////    //         "    ALTER TABLE Conf_Contract  ADD IsCheckTranType bit NULL;";


        ////    //    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////    //}
        ////    //catch (Exception)
        ////    //{

        ////    //}
        ////    #endregion

        ////    #region SMS MINI V.10.0.0.2012 > DROP TRIGGER trg_Add_Users_Public_Station
        ////    //try
        ////    //{
        ////    //    sql = "";

        ////    //    sql = "   if Not Exists(select * from sys.columns where Name = N'ERP_TRA_ID' and Object_ID = Object_ID(N'Conf_Conditions')) " + Environment.NewLine +
        ////    //         "    ALTER TABLE Conf_Conditions  ADD ERP_TRA_ID char(2) NULL;";
        ////    //    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////    //    sql = "   if Not Exists(select * from sys.columns where Name = N'ERP_SPA_ID' and Object_ID = Object_ID(N'Conf_Conditions')) " + Environment.NewLine +
        ////    //        "    ALTER TABLE Conf_Conditions  ADD ERP_SPA_ID nvarchar(30) NULL;";
        ////    //    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////    //}
        ////    //catch (Exception)
        ////    //{

        ////    //}
        ////    #endregion


        ////    #region SMS MINI V.10.0.0.2012 > DROP TRIGGER trg_Add_Users_Public_Station
        ////    //try
        ////    //{
        ////    //    sql = "";

        ////    //    sql = "   if Not Exists(select * from sys.columns where Name = N'Equipment' and Object_ID = Object_ID(N'JOB_PhotoReport_Detail')) " + Environment.NewLine +
        ////    //         "    ALTER TABLE JOB_PhotoReport_Detail  ADD Equipment nvarchar(80) NULL;";
        ////    //    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);
        ////    //}
        ////    //catch (Exception)
        ////    //{

        ////    //}
        ////    #endregion


        ////    #region SMS MINI V.12.0.6.2012 > DROP TRIGGER trg_Add_Users_Public_Station
        ////    //try
        ////    //{
        ////    //    sql = "";

        ////    //    sql =
        ////    //        " IF NOT EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ZONE_Province') " + Environment.NewLine +
        ////    //        "    CREATE TABLE [dbo].[ZONE_Province]( " + Environment.NewLine +
        ////    //        "    [ZONE] [nvarchar](2) NOT NULL, " + Environment.NewLine +
        ////    //        "    [LOC_PRNO] [nvarchar](2) NOT NULL, " + Environment.NewLine +
        ////    //        "    [Province] [nvarchar](100) NULL, " + Environment.NewLine +
        ////    //        "    [PRO_ID0] [nvarchar](10) NOT NULL " + Environment.NewLine +
        ////    //        "    );";
        ////    //    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);
        ////    //}
        ////    //catch (Exception)
        ////    //{

        ////    //}
        ////    #endregion

        ////    #region SMS MINI V.12.0.1.2012 > ALTER COLUMN

        ////    //try
        ////    //{

        ////    //    //sql = "   ALTER TABLE  JOB_Detail_Spare ALTER COLUMN IsCustomer nchar(1) NULL;";
        ////    //    //      JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////    //        sql = "   if Not Exists(select * from sys.columns where Name = N'IsCustomer' and Object_ID = Object_ID(N'JOB_Detail_Spare')) " + Environment.NewLine +
        ////    //             "    ALTER TABLE JOB_Detail_Spare  ADD IsCustomer nchar(1) NULL;";
        ////    //        JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);


        ////    //}
        ////    //catch (Exception)
        ////    //{

        ////    //}
        ////    #endregion

        ////    //ERP_documentno

        ////    #region ERP_documentno,Commision V.14.0.8.2013
        ////    //try
        ////    //{

        ////    //    //sql = "   ALTER TABLE  JOB_Detail_Spare ALTER COLUMN IsCustomer nchar(1) NULL;";
        ////    //    //      JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////    //    sql = "   if Not Exists(select * from sys.columns where Name = N'ERP_documentno' and Object_ID = Object_ID(N'JOB')) " + Environment.NewLine +
        ////    //         "    ALTER TABLE JOB  ADD ERP_documentno nvarchar(30) NULL;";
        ////    //    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);


        ////    //    sql = "   if Not Exists(select * from sys.columns where Name = N'ERP_orderline_id' and Object_ID = Object_ID(N'JOB_Detail_Spare')) " + Environment.NewLine +
        ////    //      "    ALTER TABLE JOB_Detail_Spare ADD ERP_orderline_id int NULL;";
        ////    //    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////    //    sql = "   if Not Exists(select * from sys.columns where Name = N'Commision' and Object_ID = Object_ID(N'SpareParts')) " + Environment.NewLine +
        ////    //   "    ALTER TABLE SpareParts ADD Commision [numeric](18, 2) NULL;";
        ////    //    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////    //    sql = "   if Not Exists(select * from sys.columns where Name = N'CaculateST' and Object_ID = Object_ID(N'Employees')) " + Environment.NewLine +
        ////    //  "    ALTER TABLE Employees ADD CaculateST nchar(1) NULL;";
        ////    //    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////    //    //CaculateST

        ////    //}
        ////    //catch (Exception)
        ////    //{

        ////    //}

        ////    #endregion

        ////    #region ERP_documentno,Commision V.15.0.0.2013
        ////    //try
        ////    //{

        ////    //    sql = "   if Not Exists(select * from sys.columns where Name = N'JdQTUse' and Object_ID = Object_ID(N'JOB_Detail_SO')) " + Environment.NewLine +
        ////    //   "    ALTER TABLE JOB_Detail_SO ADD JdQTUse [numeric](18, 2) NULL;";
        ////    //    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);



        ////    //}
        ////    //catch (Exception)
        ////    //{

        ////    //}

        ////    #endregion


        ////    #region ERP_documentno,Commision V.15.0.0.2013
        ////    //try
        ////    //{

        ////    //    sql = "   if Not Exists(select * from sys.columns where Name = N'LicensePlate' and Object_ID = Object_ID(N'Employees')) " + Environment.NewLine +
        ////    //   "    ALTER TABLE Employees ADD LicensePlate nvarchar(30) NULL;";
        ////    //    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);



        ////    //}
        ////    //catch (Exception)
        ////    //{

        ////    //}

        ////    #endregion

        ////    #region ERP_documentno,Commision V.16.0.2.2013
        ////    //try
        ////    //{

        ////    //    sql = 
        ////    //        " If Not Exists(select * from sys.columns where Name = N'Location' and Object_ID = Object_ID(N'FFixedAsset_Station_Detail')) " + Environment.NewLine +
        ////    //        "    ALTER TABLE FFixedAsset_Station_Detail ADD Location nvarchar(80) NULL;";
        ////    //    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////    //    sql =
        ////    //       " If Not Exists(select * from sys.columns where Name = N'TranDate' and Object_ID = Object_ID(N'FFixedAsset_Station_Detail')) " + Environment.NewLine +
        ////    //       "    ALTER TABLE FFixedAsset_Station_Detail ADD TranDate DateTime NULL;";
        ////    //    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);
        ////    //}
        ////    //catch (Exception)
        ////    //{

        ////    //}

        ////    #endregion


        ////    #region IsStationCharge
        ////    //try
        ////    //{
        ////    //    sql =
        ////    //            " If Not Exists(select * from sys.columns where Name = N'IsStationCharge' and Object_ID = Object_ID(N'JOB')) " + Environment.NewLine +
        ////    //            "    ALTER TABLE JOB ADD IsStationCharge bit NULL;";
        ////    //    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);
        ////    //}
        ////    //catch (Exception)
        ////    //{

        ////    //}

        ////    #endregion


        ////    #region Change Data Type, SMS MINI V.17.0.3.2013
        ////    //try
        ////    //{
        ////    //    sql =
        ////    //            " ALTER TABLE FFixedAsset_Station_Detail  ALTER COLUMN PartType1 int;"+
        ////    //            " ALTER TABLE FFixedAsset_Station_Detail  ALTER COLUMN PartType2 int;"+
        ////    //            " ALTER TABLE FFixedAsset_Station_Detail  ALTER COLUMN PartType3 int;";

        ////    //    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);
        ////    //}
        ////    //catch (Exception)
        ////    //{

        ////    //}

        ////    #endregion

        ////    #region Drop a foreign key constraint, SMS MINI V.17.0.3.2013

        ////    //try
        ////    //{
        ////    //    sql =
        ////    //            "   IF (OBJECT_ID('FK_FFixedAsset_Station_Detail_FFixedAsset_SerialNumber_Pool') IS NOT NULL) " +
        ////    //            "   BEGIN " +
        ////    //            "       ALTER TABLE [FFixedAsset_Station_Detail] " +
        ////    //            "       DROP CONSTRAINT FK_FFixedAsset_Station_Detail_FFixedAsset_SerialNumber_Pool " +
        ////    //            "   END";

        ////    //    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);
        ////    //}
        ////    //catch (Exception)
        ////    //{

        ////    //}
        ////    #endregion

        ////    //CheckGauge
        ////    #region CheckGauge, SMS MINI V.17.0.3.2013

        ////    //try
        ////    //{

        ////    //    sql =
        ////    //            " If Not Exists(select * from sys.columns where Name = N'CheckGauge' and Object_ID = Object_ID(N'FFixedAsset_Station_Detail')) " + Environment.NewLine +
        ////    //            "    ALTER TABLE FFixedAsset_Station_Detail ADD CheckGauge int NULL;";

        ////    //    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////    //    sql =
        ////    //         " If Not Exists(select * from sys.columns where Name = N'WADDate' and Object_ID = Object_ID(N'FFixedAsset_SerialNumber_Pool')) " + Environment.NewLine +
        ////    //         "    ALTER TABLE FFixedAsset_SerialNumber_Pool ADD WADDate DateTime NULL;" +

        ////    //         " If Not Exists(select * from sys.columns where Name = N'PDIDate' and Object_ID = Object_ID(N'FFixedAsset_SerialNumber_Pool')) " + Environment.NewLine +
        ////    //         "    ALTER TABLE FFixedAsset_SerialNumber_Pool ADD PDIDate DateTime NULL;";
        ////    //    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////    //    sql =
        ////    //        " If Not Exists(select * from sys.columns where Name = N'ContactName' and Object_ID = Object_ID(N'Station')) " + Environment.NewLine +
        ////    //        "    ALTER TABLE Station ADD [ContactName] [nvarchar](100) NULL;" +

        ////    //        " If Not Exists(select * from sys.columns where Name = N'ContactPhone' and Object_ID = Object_ID(N'Station')) " + Environment.NewLine +
        ////    //        "    ALTER TABLE Station ADD [ContactPhone] [nvarchar](100) NULL;" +

        ////    //        " If Not Exists(select * from sys.columns where Name = N'ManagerName' and Object_ID = Object_ID(N'Station')) " + Environment.NewLine +
        ////    //        "    ALTER TABLE Station ADD [ManagerName] [nvarchar](100) NULL;" +

        ////    //        " If Not Exists(select * from sys.columns where Name = N'ManagerPhone' and Object_ID = Object_ID(N'Station')) " + Environment.NewLine +
        ////    //        "    ALTER TABLE Station ADD [ManagerPhone] [nvarchar](100) NULL;";

        ////    //    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);


        ////    //}
        ////    //catch (Exception)
        ////    //{

        ////    //}
        ////    #endregion


        ////    #region Drop a foreign key constraint, SMS MINI V.17.0.4.2013

        ////   // try
        ////   // {
        ////   //     sql =
        ////   //     " If Not Exists(select * from sys.columns where Name = N'IsCloseJobCheckSN' and Object_ID = Object_ID(N'SpareParts')) " + Environment.NewLine +
        ////   //     "    ALTER TABLE SpareParts ADD [IsCloseJobCheckSN] Bit NULL;";

        ////   //     JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////   //     sql =
        ////   //     " If Not Exists(select * from sys.columns where Name = N'SerialNumber_New' and Object_ID = Object_ID(N'JOB_Detail_Spare')) " + Environment.NewLine +
        ////   //     "    ALTER TABLE JOB_Detail_Spare ADD [SerialNumber_New] [nvarchar](50) NULL;" + Environment.NewLine +

        ////   //     " If Not Exists(select * from sys.columns where Name = N'SerialNumber_Old' and Object_ID = Object_ID(N'JOB_Detail_Spare')) " + Environment.NewLine +
        ////   //     "    ALTER TABLE JOB_Detail_Spare ADD [SerialNumber_Old] [nvarchar](50) NULL;";


        ////   //     JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////   //     sql =
        ////   //            " IF not exists (select * from sysobjects where name='SpareParts_SerialNumber' and xtype='U') " + Environment.NewLine +
        ////   //             "    Begin " + Environment.NewLine +
        ////   //             "    CREATE TABLE [dbo].[SpareParts_SerialNumber]( " + Environment.NewLine +
        ////   //             "    	[SPA_ID] [nvarchar](30) NOT NULL, " + Environment.NewLine +
        ////   //             "    	[SerialNumber] [nvarchar](50) NOT NULL, " + Environment.NewLine +
        ////   //             "    	[ReceivedDate] [datetime] NULL, " + Environment.NewLine +
        ////   //             "    	[ExpiryDate] [datetime] NULL, " + Environment.NewLine +
        ////   //             "     CONSTRAINT [PK_SpareParts_SerialNumber] PRIMARY KEY CLUSTERED  " + Environment.NewLine +
        ////   //             "    ( " + Environment.NewLine +
        ////   //             "    	[SPA_ID] ASC, " + Environment.NewLine +
        ////   //             "    	[SerialNumber] ASC " + Environment.NewLine +
        ////   //             "    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY] " + Environment.NewLine +
        ////   //             "    ) ON [PRIMARY] " + Environment.NewLine +
        ////   //             "    end " + Environment.NewLine +

        ////   //             "";

        ////   //     JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);


        ////    //sql =
        ////    //      " ALTER VIEW [dbo].[v_rpt_ReturnPartsRoutingTag] " + Environment.NewLine +
        ////    //         "    AS " + Environment.NewLine +
        ////    //         "    SELECT     GOP_ID, GroupStation, STA_ID, STA_ID1, StationSys, Address, TelNo, Province, JOB_ID, JobFailure_Detail, SPA_ID, SparePart, FName, LName, Opendate, CloseDate,  " + Environment.NewLine +
        ////    //          "                         TRA_ID, StatusSpare, Resole_Detail, Problem_Detail, IsCancel, NO, EMP_ID, SerialNumber, SerialNumber_New, PartSerialNumber_Old, " + Environment.NewLine +
        ////    //          "                             (SELECT     ml.FModel " + Environment.NewLine +
        ////    //          "                               FROM          dbo.FFixedAsset_SerialNumber_Pool AS pl INNER JOIN " + Environment.NewLine +
        ////    //          "                                                      dbo.FFixedAsset_Model AS ml ON pl.FMD_ID = ml.FMD_ID " + Environment.NewLine +
        ////    //          "                               WHERE      (pl.SerialNumber = v.SerialNumber)) AS FModelSN, " + Environment.NewLine +
        ////    //          "                             (SELECT     ml.FModel " + Environment.NewLine +
        ////    //          "                               FROM          dbo.FFixedAsset_SerialNumber_Pool AS pl INNER JOIN " + Environment.NewLine +
        ////    //          "                                                      dbo.FFixedAsset_Model AS ml ON pl.FMD_ID = ml.FMD_ID " + Environment.NewLine +
        ////    //         "                                WHERE      (pl.SerialNumber = v.SerialNumber_New)) AS FModelSNNew " + Environment.NewLine +
        ////    //         "    FROM         (SELECT     dbo.Station_Group.GOP_ID, dbo.Station_Group.GroupStation, ISNULL(dbo.Station.STA_ID1, dbo.Station.STA_ID) AS STA_ID, dbo.Station.STA_ID1,  " + Environment.NewLine +
        ////    //         "                                                  dbo.Station.StationSys, dbo.Station.Address, dbo.Station.TelNo, dbo.ZONE_Province.Province, dbo.JOB.JOB_ID, dbo.JOB.JobFailure_Detail,  " + Environment.NewLine +
        ////    //         "                                                  dbo.SpareParts.SPA_ID, dbo.SpareParts.SparePart, dbo.Employees.FName, dbo.Employees.LName, dbo.JOB.Opendate, dbo.JOB.CloseDate,  " + Environment.NewLine +
        ////    //          "                                                 dbo.JOB_Detail_Spare.TRA_ID, dbo.JOB_Detail_Spare.StatusSpare, dbo.JOB_Detail.Resole_Detail, dbo.JOB_Detail.Problem_Detail,  " + Environment.NewLine +
        ////    //         "                                                  dbo.JOB_Detail_Spare.IsCancel, dbo.JOB_Detail_Spare.NO, dbo.Employees.EMP_ID, dbo.JOB_Failure_Detail.SerialNumber,  " + Environment.NewLine +
        ////    //         "                                                  dbo.JOB_Failure_Detail.SerialNumber_New, dbo.JOB_Detail_Spare.SerialNumber_Old AS PartSerialNumber_Old " + Environment.NewLine +
        ////    //         "                           FROM          dbo.JOB INNER JOIN " + Environment.NewLine +
        ////    //         "                                                  dbo.JOB_Detail ON dbo.JOB.JOB_ID = dbo.JOB_Detail.JOB_ID INNER JOIN " + Environment.NewLine +
        ////    //         "                                                  dbo.JOB_Detail_Spare ON dbo.JOB_Detail.JOB_ID = dbo.JOB_Detail_Spare.JOB_ID AND  " + Environment.NewLine +
        ////    //          "                                                 dbo.JOB_Detail.StartDate = dbo.JOB_Detail_Spare.StartDate INNER JOIN " + Environment.NewLine +
        ////    //          "                                                 dbo.SpareParts ON dbo.JOB_Detail_Spare.SPA_ID = dbo.SpareParts.SPA_ID INNER JOIN " + Environment.NewLine +
        ////    //          "                                                 dbo.Employees ON dbo.JOB.EMP_ID3 = dbo.Employees.EMP_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////    //          "                                                 dbo.JOB_Failure_Detail ON dbo.JOB_Detail_Spare.JOB_ID = dbo.JOB_Failure_Detail.JOB_ID AND  " + Environment.NewLine +
        ////    //          "                                                 dbo.JOB_Detail_Spare.POI_ID = dbo.JOB_Failure_Detail.POI_ID AND dbo.JOB_Detail_Spare.FAI_ID = dbo.JOB_Failure_Detail.FAI_ID AND  " + Environment.NewLine +
        ////    //         "                                                 dbo.JOB_Detail_Spare.Location = dbo.JOB_Failure_Detail.LOC_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////    //         "                                                  dbo.Station ON dbo.JOB.STA_ID = dbo.Station.STA_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////    //         "                                                     dbo.Station_Group ON dbo.Station.GOP_ID = dbo.Station_Group.GOP_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////    //         "                                                  dbo.ZONE_Province ON dbo.Station.PRO_ID0 = dbo.ZONE_Province.PRO_ID0) AS v " + Environment.NewLine +
        ////    //        "";

        ////    //JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////    /*
        ////    //---20151223=====เปลี่ยนการแสดงผล อาการ===========
        ////    //====20150914 == LOC_ID
        ////    sql =
        ////          "    ALTER VIEW [dbo].[v_rpt_ReturnPartsRoutingTag] " + Environment.NewLine +
        ////          "    AS " + Environment.NewLine +
        ////          "     SELECT     GOP_ID, GroupStation, STA_ID, STA_ID1, StationSys, Address, TelNo, Province, JOB_ID, JobFailure_Detail, SPA_ID  " + Environment.NewLine +
        ////          "     , SparePart, FName, LName, Opendate, CloseDate, TRA_ID, StatusSpare,  " + Environment.NewLine +
        ////          "     Resole_Detail, Problem_Detail, IsCancel, NO, EMP_ID, SerialNumber, SerialNumber_New, PartSerialNumber_Old, " + Environment.NewLine +
        ////          "        ( SELECT     ml.FModel " + Environment.NewLine +
        ////          "           FROM          dbo.FFixedAsset_SerialNumber_Pool AS pl INNER JOIN " + Environment.NewLine +
        ////          "                                dbo.FFixedAsset_Model AS ml ON pl.FMD_ID = ml.FMD_ID " + Environment.NewLine +
        ////          "          WHERE      (pl.SerialNumber = v.SerialNumber)) AS FModelSN, " + Environment.NewLine +
        ////          "      (   SELECT     ml.FModel " + Environment.NewLine +
        ////          "          FROM          dbo.FFixedAsset_SerialNumber_Pool AS pl INNER JOIN " + Environment.NewLine +
        ////          "                             dbo.FFixedAsset_Model AS ml ON pl.FMD_ID = ml.FMD_ID " + Environment.NewLine +
        ////          "          WHERE      (pl.SerialNumber = v.SerialNumber_New)) AS FModelSNNew  " + Environment.NewLine +
        ////          " FROM         (SELECT     dbo.Station_Group.GOP_ID, dbo.Station_Group.GroupStation, ISNULL(dbo.Station.STA_ID1, dbo.Station.STA_ID) AS STA_ID, dbo.Station.STA_ID1, dbo.Station.StationSys,  " + Environment.NewLine +
        ////                                      " dbo.Station.Address, dbo.Station.TelNo, dbo.ZONE_Province.Province, dbo.JOB.JOB_ID, dbo.JOB.JobFailure_Detail, dbo.SpareParts.SPA_ID, dbo.SpareParts.SparePart,  " + Environment.NewLine +
        ////                                      " dbo.Employees.FName, dbo.Employees.LName, dbo.JOB.Opendate, dbo.JOB.CloseDate, dbo.JOB_Detail_Spare.TRA_ID, dbo.JOB_Detail_Spare.StatusSpare,  " + Environment.NewLine +
        ////                                      " dbo.JOB_Detail.Resole_Detail, dbo.JOB_Detail.Problem_Detail, dbo.JOB_Detail_Spare.IsCancel, dbo.JOB_Detail_Spare.NO, dbo.Employees.EMP_ID,  " + Environment.NewLine +
        ////                                      " dbo.JOB_Failure_Detail.SerialNumber, dbo.JOB_Failure_Detail.SerialNumber_New, dbo.JOB_Detail_Spare.SerialNumber_Old AS PartSerialNumber_Old " + Environment.NewLine +
        ////                                      " FROM          dbo.JOB INNER JOIN " + Environment.NewLine +
        ////                                      " dbo.JOB_Detail ON dbo.JOB.JOB_ID = dbo.JOB_Detail.JOB_ID INNER JOIN " + Environment.NewLine +
        ////                                      " dbo.JOB_Detail_Spare ON dbo.JOB_Detail.JOB_ID = dbo.JOB_Detail_Spare.JOB_ID AND dbo.JOB_Detail.StartDate = dbo.JOB_Detail_Spare.StartDate INNER JOIN " + Environment.NewLine +
        ////                                      " dbo.SpareParts ON dbo.JOB_Detail_Spare.SPA_ID = dbo.SpareParts.SPA_ID INNER JOIN " + Environment.NewLine +
        ////                                      " dbo.Employees ON dbo.JOB.EMP_ID3 = dbo.Employees.EMP_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////                                      " dbo.JOB_Failure_Detail ON dbo.JOB_Detail_Spare.JOB_ID = dbo.JOB_Failure_Detail.JOB_ID AND dbo.JOB_Detail_Spare.POI_ID = dbo.JOB_Failure_Detail.POI_ID AND  " + Environment.NewLine +
        ////                                      " dbo.JOB_Detail_Spare.FAI_ID = dbo.JOB_Failure_Detail.FAI_ID AND dbo.JOB_Detail_Spare.Location = dbo.JOB_Failure_Detail.LOC_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////                                      " dbo.Station ON dbo.JOB.STA_ID = dbo.Station.STA_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////                                      " dbo.Station_Group ON dbo.Station.GOP_ID = dbo.Station_Group.GOP_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////                                      " dbo.ZONE_Province ON dbo.Station.PRO_ID0 = dbo.ZONE_Province.PRO_ID0) AS v  " + Environment.NewLine +                 
        ////                                      "";

        ////    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);
        ////    */
        ////    //---20151223=====เปลี่ยนการแสดงผล อาการ===========
        ////    sql =
        ////          "    ALTER VIEW [dbo].[v_rpt_ReturnPartsRoutingTag] " + Environment.NewLine +
        ////          "    AS " + Environment.NewLine +
        ////          "     SELECT     GOP_ID, GroupStation, STA_ID, STA_ID1, StationSys, Address, TelNo, Province, JOB_ID, JobFailure_Detail, SPA_ID, SparePart, FName, LName, Opendate, CloseDate, TRA_ID, StatusSpare,   " + Environment.NewLine +
        ////          "      Resole_Detail, Problem_Detail, IsCancel, NO, EMP_ID, SerialNumber, SerialNumber_New, PartSerialNumber_Old,  " + Environment.NewLine +
        ////          "      (SELECT     ml.FModel  " + Environment.NewLine +
        ////          "      FROM          dbo.FFixedAsset_SerialNumber_Pool AS pl INNER JOIN  " + Environment.NewLine +
        ////          "                             dbo.FFixedAsset_Model AS ml ON pl.FMD_ID = ml.FMD_ID  " + Environment.NewLine +
        ////          "      WHERE      (pl.SerialNumber = v.SerialNumber)) AS FModelSN,  " + Environment.NewLine +
        ////          "      (SELECT     ml.FModel  " + Environment.NewLine +
        ////          "      FROM          dbo.FFixedAsset_SerialNumber_Pool AS pl INNER JOIN  " + Environment.NewLine +
        ////          "                             dbo.FFixedAsset_Model AS ml ON pl.FMD_ID = ml.FMD_ID  " + Environment.NewLine +
        ////          "      WHERE      (pl.SerialNumber = v.SerialNumber_New)) AS FModelSNNew  " + Environment.NewLine +
        ////          "    ,DEP_ID ,Company" + Environment.NewLine +//20171208

        ////          "      FROM         (  " + Environment.NewLine +
        ////          "                             SELECT     dbo.JOB_Failure_Detail.ATF_ID, dbo.Station_Group.GOP_ID, dbo.Station_Group.GroupStation, ISNULL(dbo.Station.STA_ID1, dbo.Station.STA_ID) AS STA_ID, dbo.Station.STA_ID1,  " + Environment.NewLine +
        ////          "                            dbo.Station.StationSys, dbo.Station.Address, dbo.Station.TelNo, dbo.ZONE_Province.Province, dbo.JOB.JOB_ID, dbo.JOB.JobFailure_Detail, dbo.SpareParts.SPA_ID,   " + Environment.NewLine +
        ////          "                            dbo.SpareParts.SparePart, dbo.Employees.FName, dbo.Employees.LName, dbo.JOB.Opendate, dbo.JOB.CloseDate, dbo.JOB_Detail_Spare.TRA_ID, dbo.JOB_Detail_Spare.StatusSpare,   " + Environment.NewLine +
        ////          "                            dbo.JOB_Detail.Resole_Detail,  " + Environment.NewLine +
        ////          "                            CASE WHEN JOB_Failure_Detail.ATF_ID = '0000000' THEN Problem_Detail + ' ( ' + JobFailure_Detail + ' )' ELSE CASE WHEN JOB_Failure_Detail.ATF_ID IS NULL OR JOB_Failure_Detail.ATF_ID = ''   " + Environment.NewLine +
        ////          "                            THEN 'ไม่ระบุ... ( ' + JobFailure_Detail + ' )' ELSE Problem_Detail + ' ( ' + JobFailure_Detail + ' )' END END AS Problem_Detail, dbo.JOB_Detail_Spare.IsCancel, dbo.JOB_Detail_Spare.NO,   " + Environment.NewLine +
        ////          "                            dbo.Employees.EMP_ID, dbo.JOB_Failure_Detail.SerialNumber, dbo.JOB_Failure_Detail.SerialNumber_New, dbo.JOB_Detail_Spare.SerialNumber_Old AS PartSerialNumber_Old  " + Environment.NewLine +
        ////          "                            , dbo.Employees.DEP_ID,Employees.Company " + Environment.NewLine +//20171208
        ////          "                             FROM          dbo.JOB INNER JOIN  " + Environment.NewLine +
        ////          "                            dbo.JOB_Detail ON dbo.JOB.JOB_ID = dbo.JOB_Detail.JOB_ID INNER JOIN  " + Environment.NewLine +
        ////          "                            dbo.JOB_Detail_Spare ON dbo.JOB_Detail.JOB_ID = dbo.JOB_Detail_Spare.JOB_ID AND dbo.JOB_Detail.StartDate = dbo.JOB_Detail_Spare.StartDate INNER JOIN  " + Environment.NewLine +
        ////          "                            dbo.SpareParts ON dbo.JOB_Detail_Spare.SPA_ID = dbo.SpareParts.SPA_ID INNER JOIN  " + Environment.NewLine +
        ////          "                            dbo.Employees ON dbo.JOB.EMP_ID3 = dbo.Employees.EMP_ID LEFT OUTER JOIN  " + Environment.NewLine +
        ////          "                            dbo.JOB_Failure_Detail ON dbo.JOB_Detail_Spare.JOB_ID = dbo.JOB_Failure_Detail.JOB_ID AND dbo.JOB_Detail_Spare.POI_ID = dbo.JOB_Failure_Detail.POI_ID AND   " + Environment.NewLine +
        ////          "                            dbo.JOB_Detail_Spare.FAI_ID = dbo.JOB_Failure_Detail.FAI_ID AND dbo.JOB_Detail_Spare.Location = CAST(dbo.JOB_Failure_Detail.LOC_ID AS nvarchar) LEFT OUTER JOIN  " + Environment.NewLine +
        ////          "                            dbo.Station ON dbo.JOB.STA_ID = dbo.Station.STA_ID LEFT OUTER JOIN  " + Environment.NewLine +
        ////          "                            dbo.Station_Group ON dbo.Station.GOP_ID = dbo.Station_Group.GOP_ID LEFT OUTER JOIN  " + Environment.NewLine +
        ////          "                            dbo.ZONE_Province ON dbo.Station.PRO_ID0 = dbo.ZONE_Province.PRO_ID0 LEFT OUTER JOIN  " + Environment.NewLine +
        ////          "                            dbo.Failure_Action ON dbo.JOB_Failure_Detail.ATF_ID = dbo.Failure_Action.ATF_ID  " + Environment.NewLine +
        ////          "           ) AS v  " + Environment.NewLine +
        ////                                      "";

        ////    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);


        ////   //     //20141030
        ////   //sql =   " ALTER VIEW [dbo].[v_rpt_ServicesReport_web] " + Environment.NewLine +
        ////   //           " AS " + Environment.NewLine +
        ////   //           " SELECT DISTINCT " + Environment.NewLine +
        ////   //           " dbo.JOB.JOB_ID, dbo.JOB.Opendate, dbo.JOB.FirstInDate, ISNULL(JOB_Detail_1.StartDate, dbo.JOB_Detail.StartDate) AS StartDate, dbo.JOB.CloseDate, dbo.Employees.EMP_ID AS EMP_ID3, " + Environment.NewLine +
        ////   //           " ISNULL(dbo.Employees.FName, N'') + N' โทร. ' + ISNULL(dbo.Employees.Phone, N'') AS FName, dbo.Failure.Failure_th, dbo.Priority.Priority_GuestVip, " + Environment.NewLine +
        ////   //           " PointFailure_1.PointFailure AS job_PointFailure, dbo.Type.Type, dbo.JOB_Status.JobStatus, Type_1.Type AS Type_1, dbo.JOB.Informer, dbo.JOB.InPhone, dbo.JOB.JobOutside, " + Environment.NewLine +
        ////   //           " dbo.TypeJobClose.TypeCloseJob, dbo.JOB_ProblemType.Problem, ISNULL(dbo.JOB_Failure_Detail.NO, 0) AS fail_NO, ISNULL(dbo.JOB_Failure_Detail.POI_ID, N'000') AS fail_POI_ID, " + Environment.NewLine +
        ////   //           " PointFailure_2.PointFailure AS PointFailure2, dbo.JOB_Failure_Detail.Failure_Detail, dbo.JOB_Failure_Detail.SerialNumber, JOB_Detail_1.EndDate, JOB_Detail_1.Resole_Detail, " + Environment.NewLine +
        ////   //           " ISNULL(dbo.Station.STA_ID1, dbo.Station.STA_ID) AS STA_ID1, dbo.Station.StationSys, ISNULL(dbo.JOB.FAI_ID, N'00000') AS FAI_ID, dbo.JOB.PRI_ID, ISNULL(dbo.JOB.POI_ID, N'000') " + Environment.NewLine +
        ////   //           " AS Job_POI_ID, dbo.JOB.TYP_ID, dbo.JOB.TYP_ID1, dbo.JOB.TCO_ID, dbo.JOB.PMT_ID, dbo.JOB.Contract, dbo.Station.Address + N' จ. ' + ISNULL(dbo.Province.Province, N'') " + Environment.NewLine +
        ////   //           " + N' " + Environment.NewLine +
        ////   //           " โซน: ' + ISNULL(dbo.ZONE.Detail, N'') + N' [ระยะทาง: ' + ISNULL(CONVERT(nvarchar, dbo.Station.ActualDistance), N'0') + N' กม.]' AS Address, dbo.Station.TelNo, dbo.Station.FaxNo, " + Environment.NewLine +
        ////   //           " dbo.JOB.JobFailure_Detail, dbo.JOB_Failure_Detail.StartLiter, dbo.JOB_Failure_Detail.EndLiter, dbo.JOB_Failure_Detail.LiterTest, ISNULL(dbo.JOB_Failure_Detail.Location, N'None') AS Location, " + Environment.NewLine +
        ////   //           " dbo.JOB_Failure_Detail.Model, dbo.JOB_Failure_Detail.trandate, ISNULL(dbo.JOB_WorkID.Work_ID, N'') AS Work_ID, ISNULL(dbo.JOB_Failure_Detail.FAI_ID, N'0000000') AS PFAI_ID, " + Environment.NewLine +
        ////   //           " dbo.JOB_Failure_Detail.IsCancel, dbo.JOB.JOBS_ID, dbo.JOB_Detail.JOBS_ID AS JOBS_ID2, ISNULL(dbo.JOB_WorkID.UserWorkOrder, N'') AS UserWorkOrder, JOB_Detail_1.StatusDetail, " + Environment.NewLine +
        ////   //           " dbo.Product_Type.Product_Type AS Grade, ISNULL(dbo.JOB_Failure_Detail.Location, N'None') AS Locate, dbo.Equipment_Position.Equipment_Position AS PointFFA, " + Environment.NewLine +
        ////   //           " ISNULL(dbo.JOB_Failure_Detail.Location, N'None') + ' ' + dbo.JOB_Failure_Detail.Failure_Detail AS FDetail, dbo.Failure_Action.Action_Failure AS Cause, " + Environment.NewLine +
        ////   //           " dbo.Failure_Action_Solving.Failure_Action_Solving + ' ' + dbo.JOB_Failure_Detail.SolvingByWI AS Solve " + Environment.NewLine + 
        ////   //           " FROM         dbo.Failure RIGHT OUTER JOIN " + Environment.NewLine + 
        ////   //           " dbo.JOB LEFT OUTER JOIN " + Environment.NewLine +
        ////   //           " dbo.JOB_WorkID ON dbo.JOB.JOB_ID = dbo.JOB_WorkID.JOB_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////   //           " dbo.PointFailure AS PointFailure_2 RIGHT OUTER JOIN " + Environment.NewLine +
        ////   //           " dbo.JOB_Failure_Detail ON PointFailure_2.POI_ID = dbo.JOB_Failure_Detail.POI_ID ON dbo.JOB.JOB_ID = dbo.JOB_Failure_Detail.JOB_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////   //           " dbo.Station LEFT OUTER JOIN " + Environment.NewLine +
        ////   //           " dbo.Province ON dbo.Station.PRO_ID = dbo.Province.PRO_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////   //           " dbo.ZONE ON dbo.Station.ZONE = dbo.ZONE.ZONE ON dbo.JOB.STA_ID = dbo.Station.STA_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////   //           " dbo.JOB_ProblemType ON dbo.JOB.PMT_ID = dbo.JOB_ProblemType.PMT_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////   //           " dbo.TypeJobClose ON dbo.JOB.TCO_ID = dbo.TypeJobClose.TCO_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////   //           " dbo.Type AS Type_1 ON dbo.JOB.TYP_ID1 = Type_1.TYP_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////   //           " dbo.JOB_Status ON dbo.JOB.JOBS_ID = dbo.JOB_Status.JOBS_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////   //           " dbo.Type ON dbo.JOB.TYP_ID = dbo.Type.TYP_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////   //           " dbo.PointFailure AS PointFailure_1 ON dbo.JOB.POI_ID = PointFailure_1.POI_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////   //           " dbo.Priority ON dbo.JOB.PRI_ID = dbo.Priority.PRI_ID ON dbo.Failure.FAI_ID = dbo.JOB.FAI_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////   //           " dbo.JOB_Detail AS JOB_Detail_1 LEFT OUTER JOIN " + Environment.NewLine +
        ////   //           " dbo.Employees ON JOB_Detail_1.EMP_ID = dbo.Employees.EMP_ID ON dbo.JOB.JOB_ID = JOB_Detail_1.JOB_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////   //           " dbo.JOB_Detail ON dbo.JOB.JOB_ID = dbo.JOB_Detail.JOB_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////   //           " dbo.Province_Region ON dbo.Province.REG_ID = dbo.Province_Region.REG_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////   //           " dbo.Product_Type ON dbo.JOB_Failure_Detail.PRT_ID = dbo.Product_Type.PRT_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////   //           " dbo.Equipment_Position ON dbo.JOB_Failure_Detail.EQP_ID = dbo.Equipment_Position.EQP_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////   //           " dbo.Failure_Action ON dbo.JOB_Failure_Detail.ATF_ID = dbo.Failure_Action.ATF_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////   //           " dbo.Failure_Action_Solving ON dbo.JOB_Failure_Detail.FAS_ID = dbo.Failure_Action_Solving.FAS_ID " + Environment.NewLine +
        ////   //           "  WHERE     (dbo.JOB_Detail.JOBS_ID > '03') AND (dbo.JOB_Failure_Detail.IsCancel IS NULL OR " + Environment.NewLine +
        ////   //           " dbo.JOB_Failure_Detail.IsCancel = '0') " + Environment.NewLine +                       
        ////   //     "";
        ////   //     JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);


        ////   //    sql = " ALTER VIEW [dbo].[v_rpt_ServicesReport_web_PartDetail_Reprint] " + Environment.NewLine +    
        ////   //         " AS " + Environment.NewLine +    
        ////   //         " SELECT     JD.JOB_ID, ISNULL(JD.POI_ID, N'000') AS POI_ID, ISNULL(JD.NO, 0) AS PartLineNo, JD.SPA_ID, JD.Quantity, JD.Comment, JD.StatusSpare, JD.TRA_ID, dbo.SpareParts.SparePart, dbo.SpareParts.WH, ISNULL(JD.Location, N'None') AS Location, ISNULL(JD.FAI_ID,  " + Environment.NewLine +   
        ////   //         "                   N'0000000') AS PFAI_ID, JD.IsCancel, CASE WHEN JOB.PROJ_ID IS NULL OR " + Environment.NewLine +   
        ////   //         "                    JOB.PROJ_ID = '' THEN CASE WHEN JD.IsPrices = '1' THEN JD.SumPrices ELSE 0 END ELSE 0 END AS SumPrices, CASE WHEN JOB.PROJ_ID IS NULL OR " + Environment.NewLine +   
        ////   //         "                    JOB.PROJ_ID = '' THEN CASE WHEN JD.IsPrices = '1' THEN JD.SumVAT ELSE 0 END ELSE 0 END AS SumVAT, CASE WHEN JOB.PROJ_ID IS NULL OR " + Environment.NewLine +   
        ////   //         "                   JOB.PROJ_ID = '' THEN CASE WHEN JD.IsPrices = '1' THEN JD.SumTotalPrices ELSE 0 END ELSE 0 END AS SumTotalPrices, CASE WHEN JOB.PROJ_ID IS NULL OR " + Environment.NewLine +   
        ////   //         "                   JOB.PROJ_ID = '' THEN CASE WHEN JD.IsPrices = '1' THEN JD.Prices ELSE 0 END ELSE 0 END AS Prices, dbo.JOB_Failure_Detail.IsCancel AS FIsCancel, JD.StartDate, JD.Enddate, dbo.Equipment_Position.Equipment_Position AS LocatePoint,  " + Environment.NewLine +   
        ////   //         "                   dbo.Conf_Conditions.ReferPage " + Environment.NewLine +   
        ////   //         " FROM        dbo.Conf_Conditions INNER JOIN " + Environment.NewLine +   
        ////   //         "                   dbo.JOB_Detail_Spare AS JD ON dbo.Conf_Conditions.SPA_ID = JD.SPA_ID LEFT OUTER JOIN " + Environment.NewLine +   
        ////   //         "                   dbo.JOB_Detail ON JD.JOB_ID = dbo.JOB_Detail.JOB_ID AND JD.StartDate = dbo.JOB_Detail.StartDate LEFT OUTER JOIN " + Environment.NewLine +   
        ////   //         "                   dbo.SpareParts ON JD.SPA_ID = dbo.SpareParts.SPA_ID LEFT OUTER JOIN " + Environment.NewLine +   
        ////   //         "                   dbo.JOB ON JD.JOB_ID = dbo.JOB.JOB_ID LEFT OUTER JOIN " + Environment.NewLine +   
        ////   //         "                   dbo.JOB_Failure_Detail ON JD.JOB_ID = dbo.JOB_Failure_Detail.JOB_ID AND JD.POI_ID = dbo.JOB_Failure_Detail.POI_ID AND JD.Location = dbo.JOB_Failure_Detail.Location AND JD.FAI_ID = dbo.JOB_Failure_Detail.FAI_ID LEFT OUTER JOIN " + Environment.NewLine +   
        ////   //         "                   dbo.Equipment_Position ON dbo.JOB_Failure_Detail.EQP_ID = dbo.Equipment_Position.EQP_ID " + Environment.NewLine +   
        ////   //         " WHERE     (dbo.JOB_Failure_Detail.IsCancel IS NULL OR " + Environment.NewLine +   
        ////   //         "                   dbo.JOB_Failure_Detail.IsCancel = '0') AND (dbo.JOB_Detail.JOBS_ID > '03') AND (JD.IsCancel IS NULL OR " + Environment.NewLine +
        ////   //         "                   JD.IsCancel = 0)  " + Environment.NewLine +   
        ////   //           "";
        ////   //            JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);


        ////   //            sql =" ALTER TABLE JOB_Failure_Detail ALTER COLUMN ATF_ID nvarchar(10)  NULL";
        ////   //           JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////   //           sql = " if Exists(select * from sys.columns where Name = N'PRT_ID' and Object_ID = Object_ID(N'JOB_Detail_Spare'))  " + Environment.NewLine +
        ////   //                " EXEC sp_rename 'JOB_Detail_Spare.PRT_ID', 'EQP_ID', 'COLUMN' ";
        ////   //           JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////   // }
        ////   // catch (Exception ex)
        ////   // {
        ////   //     MessageBox.Show(ex.Message);
        ////   // }

        ////    /*//20151106 สร้างขึ้นเพื่อ support โครงสร้างเก่าของช่าง 
        ////     //เมื่อมีการ update โครงสร้างใหม่ที่เครื่องช่าง จึงต้องเปลี่ยนไปใช้ script เดียวกันกับบน server
        ////    //20141211
        ////    sql =  " ALTER VIEW [dbo].[v_rpt_ServicesReport_web] " + Environment.NewLine +
        ////              " AS " + Environment.NewLine +
        ////              " SELECT DISTINCT " + Environment.NewLine +
        ////              " dbo.JOB.JOB_ID, dbo.JOB.Opendate, dbo.JOB.FirstInDate, ISNULL(JOB_Detail_1.StartDate, dbo.JOB_Detail.StartDate) AS StartDate, dbo.JOB.CloseDate, dbo.Employees.EMP_ID AS EMP_ID3, " + Environment.NewLine +
        ////              " ISNULL(dbo.Employees.FName, N'') + N' โทร. ' + ISNULL(dbo.Employees.Phone, N'') AS FName, dbo.Failure.Failure_th, dbo.Priority.Priority_GuestVip, " + Environment.NewLine +
        ////              " PointFailure_1.PointFailure AS job_PointFailure, dbo.Type.Type, dbo.JOB_Status.JobStatus, Type_1.Type AS Type_1, dbo.JOB.Informer, dbo.JOB.InPhone, dbo.JOB.JobOutside, " + Environment.NewLine +
        ////              " dbo.TypeJobClose.TypeCloseJob, dbo.JOB_ProblemType.Problem, ISNULL(dbo.JOB_Failure_Detail.NO, 0) AS fail_NO, ISNULL(dbo.JOB_Failure_Detail.POI_ID, N'000') AS fail_POI_ID, " + Environment.NewLine +
        ////              " PointFailure_2.PointFailure AS PointFailure2, dbo.JOB_Failure_Detail.Failure_Detail, dbo.JOB_Failure_Detail.SerialNumber, JOB_Detail_1.EndDate, JOB_Detail_1.Resole_Detail, " + Environment.NewLine +
        ////              " ISNULL(dbo.Station.STA_ID1, dbo.Station.STA_ID) AS STA_ID1, dbo.Station.StationSys, ISNULL(dbo.JOB.FAI_ID, N'00000') AS FAI_ID, dbo.JOB.PRI_ID, ISNULL(dbo.JOB.POI_ID, N'000') " + Environment.NewLine +
        ////              " AS Job_POI_ID, dbo.JOB.TYP_ID, dbo.JOB.TYP_ID1, dbo.JOB.TCO_ID, dbo.JOB.PMT_ID, dbo.JOB.Contract, dbo.Station.Address + N' จ. ' + ISNULL(dbo.Province.Province, N'') " + Environment.NewLine +
        ////              //20150318" + N' โซน: ' + ISNULL(dbo.ZONE.Detail, N'') + N' [ระยะทาง: ' + ISNULL(CONVERT(nvarchar, dbo.Station.ActualDistance), N'0') + N' กม.]' AS Address, dbo.Station.TelNo, dbo.Station.FaxNo, " + Environment.NewLine +
        ////              " + N' โซน: ' + ISNULL(dbo.ZONE.Detail, N'') AS Address, dbo.Station.TelNo, dbo.Station.FaxNo, " + Environment.NewLine +
        ////              " dbo.JOB.JobFailure_Detail, dbo.JOB_Failure_Detail.StartLiter, dbo.JOB_Failure_Detail.EndLiter, dbo.JOB_Failure_Detail.LiterTest, dbo.JOB_Failure_Detail.Model, dbo.JOB_Failure_Detail.trandate, " + Environment.NewLine +
        ////              " ISNULL(dbo.JOB_WorkID.Work_ID, N'') AS Work_ID, ISNULL(dbo.JOB_Failure_Detail.FAI_ID, N'0000000') AS PFAI_ID, dbo.JOB_Failure_Detail.IsCancel, dbo.JOB.JOBS_ID, " + Environment.NewLine +
        ////              " dbo.JOB_Detail.JOBS_ID AS JOBS_ID2, ISNULL(dbo.JOB_WorkID.UserWorkOrder, N'') AS UserWorkOrder, JOB_Detail_1.StatusDetail, dbo.Product_Type.Product_Type AS Grade, " + Environment.NewLine +
        ////             //20150805 " dbo.Location.Location  AS Locate " + Environment.NewLine + 
        ////             " CASE WHEN dbo.Location.Location <> NULL THEN CASE WHEN LEN(dbo.Location.Location) > 2 THEN 'ไม่ระบุ...' ELSE dbo.Location.Location END ELSE 'ไม่ระบุ...' END AS Locate ," + Environment.NewLine +
        ////              "  ISNULL(dbo.JOB_Failure_Detail.Location, N'None') + ' ' + ISNULL(dbo.JOB_Failure_Detail.Failure_Detail,N'') AS FDetail, " + Environment.NewLine +
        ////              //" dbo.Failure_Action.Action_Failure AS Cause, dbo.Failure_Action_Solving.Failure_Action_Solving + ' ' + dbo.JOB_Failure_Detail.SolvingByWI AS Solve " + Environment.NewLine +
        ////              "  dbo.Failure_Action.Action_Failure AS Cause,ISNULL(dbo.Failure_Action_Solving.Failure_Action_Solving,N'') + ' ' + ISNULL(dbo.Failure_Solving_WI.WICode,N'')  AS Solve " + Environment.NewLine +
        ////    "  FROM         dbo.Priority RIGHT OUTER JOIN " + Environment.NewLine +
        ////              " dbo.PointFailure AS PointFailure_2 RIGHT OUTER JOIN " + Environment.NewLine +
        ////              " dbo.Location INNER JOIN " + Environment.NewLine +
        ////              " dbo.JOB_Failure_Detail ON dbo.Location.LOC_ID = dbo.JOB_Failure_Detail.LOC_ID ON PointFailure_2.POI_ID = dbo.JOB_Failure_Detail.POI_ID RIGHT OUTER JOIN " + Environment.NewLine +
        ////              " dbo.JOB LEFT OUTER JOIN " + Environment.NewLine +
        ////              " dbo.JOB_WorkID ON dbo.JOB.JOB_ID = dbo.JOB_WorkID.JOB_ID ON dbo.JOB_Failure_Detail.JOB_ID = dbo.JOB.JOB_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////              " dbo.Station LEFT OUTER JOIN " + Environment.NewLine +
        ////              " dbo.Province ON dbo.Station.PRO_ID = dbo.Province.PRO_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////              " dbo.ZONE ON dbo.Station.ZONE = dbo.ZONE.ZONE ON dbo.JOB.STA_ID = dbo.Station.STA_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////              " dbo.JOB_ProblemType ON dbo.JOB.PMT_ID = dbo.JOB_ProblemType.PMT_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////              " dbo.TypeJobClose ON dbo.JOB.TCO_ID = dbo.TypeJobClose.TCO_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////              " dbo.Type AS Type_1 ON dbo.JOB.TYP_ID1 = Type_1.TYP_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////              " dbo.JOB_Status ON dbo.JOB.JOBS_ID = dbo.JOB_Status.JOBS_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////              " dbo.Type ON dbo.JOB.TYP_ID = dbo.Type.TYP_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////              " dbo.PointFailure AS PointFailure_1 ON dbo.JOB.POI_ID = PointFailure_1.POI_ID ON dbo.Priority.PRI_ID = dbo.JOB.PRI_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////              " dbo.Failure ON dbo.JOB.FAI_ID = dbo.Failure.FAI_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////              " dbo.JOB_Detail AS JOB_Detail_1 LEFT OUTER JOIN " + Environment.NewLine +
        ////              " dbo.Employees ON JOB_Detail_1.EMP_ID = dbo.Employees.EMP_ID ON dbo.JOB.JOB_ID = JOB_Detail_1.JOB_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////              " dbo.JOB_Detail ON dbo.JOB.JOB_ID = dbo.JOB_Detail.JOB_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////              " dbo.Province_Region ON dbo.Province.REG_ID = dbo.Province_Region.REG_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////              " dbo.Product_Type ON dbo.JOB_Failure_Detail.PRT_ID = dbo.Product_Type.PRT_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////              " dbo.Failure_Action ON dbo.JOB_Failure_Detail.ATF_ID = dbo.Failure_Action.ATF_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////              " dbo.Failure_Action_Solving ON dbo.JOB_Failure_Detail.FAS_ID = dbo.Failure_Action_Solving.FAS_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////              " dbo.Failure_Solving_WI ON dbo.JOB_Failure_Detail.SolvingByWI = dbo.Failure_Solving_WI.FSWI_ID " + Environment.NewLine +

        ////        " WHERE     (dbo.JOB_Detail.JOBS_ID > '03') AND (dbo.JOB_Failure_Detail.IsCancel IS NULL OR " + Environment.NewLine +
        ////              " dbo.JOB_Failure_Detail.IsCancel = '0')  " + Environment.NewLine +
        ////              "";
        ////        JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);


        ////        sql = " ALTER VIEW [dbo].[v_rpt_ServicesReport_web_PartDetail_Reprint] " + Environment.NewLine +
        ////             " AS " + Environment.NewLine +
        ////             " SELECT     JD.JOB_ID, ISNULL(JD.POI_ID, N'000') AS POI_ID, ISNULL(JD.NO, 0) AS PartLineNo, JD.SPA_ID, JD.Quantity, JD.Comment, JD.StatusSpare, JD.TRA_ID, dbo.SpareParts.SparePart, dbo.SpareParts.WH,  " + Environment.NewLine + 
        ////             //20150805 "  ISNULL(JD.Location, N'None') AS LOC_ID,  " + Environment.NewLine +
        ////             "  CASE WHEN JD.Location <> NULL THEN CASE WHEN LEN(JD.Location) > 2 THEN '0' ELSE JD.Location END ELSE '0' END AS LOC_ID,  " + Environment.NewLine +
        ////             "  ISNULL(JD.FAI_ID,  " + Environment.NewLine +
        ////             "                   N'0000000') AS PFAI_ID, JD.IsCancel, CASE WHEN JOB.PROJ_ID IS NULL OR " + Environment.NewLine +
        ////             "                    JOB.PROJ_ID = '' THEN CASE WHEN JD.IsPrices = '1' THEN JD.SumPrices ELSE 0 END ELSE 0 END AS SumPrices, CASE WHEN JOB.PROJ_ID IS NULL OR " + Environment.NewLine +
        ////             "                    JOB.PROJ_ID = '' THEN CASE WHEN JD.IsPrices = '1' THEN JD.SumVAT ELSE 0 END ELSE 0 END AS SumVAT, CASE WHEN JOB.PROJ_ID IS NULL OR " + Environment.NewLine +
        ////             "                   JOB.PROJ_ID = '' THEN CASE WHEN JD.IsPrices = '1' THEN JD.SumTotalPrices ELSE 0 END ELSE 0 END AS SumTotalPrices, CASE WHEN JOB.PROJ_ID IS NULL OR " + Environment.NewLine +
        ////             "                   JOB.PROJ_ID = '' THEN CASE WHEN JD.IsPrices = '1' THEN JD.Prices ELSE 0 END ELSE 0 END AS Prices, dbo.JOB_Failure_Detail.IsCancel AS FIsCancel, JD.StartDate, JD.Enddate,  " + Environment.NewLine +
        ////             "                   dbo.Conf_Conditions.ReferPage, " + Environment.NewLine +
        ////             //20150805 "                   dbo.Location.Location " + Environment.NewLine +
        ////             "                  CASE WHEN dbo.Location.Location <> NULL THEN CASE WHEN LEN(dbo.Location.Location) > 2 THEN 'ไม่ระบุ...' ELSE dbo.Location.Location END ELSE 'ไม่ระบุ...' END AS Location " + Environment.NewLine +
        ////             "                   , fsd.WarrantDate " + Environment.NewLine +
        ////             "                   , dbo.JOB.CloseDate " + Environment.NewLine +
        ////             "                   , CASE WHEN WarrantDate <> NULL " + Environment.NewLine +
        ////             "                   THEN CASE WHEN WarrantDate > job.CloseDate THEN '[Y]' ELSE '[N]' END ELSE '[N]' END AS WA  " + Environment.NewLine +

        ////            // " FROM        dbo.Conf_Conditions INNER JOIN " + Environment.NewLine + //20141217
        ////            //" FROM        dbo.Conf_Conditions RIGHT OUTER JOIN" + Environment.NewLine + 
        ////            //"                   dbo.JOB_Detail_Spare AS JD ON dbo.Conf_Conditions.SPA_ID = JD.SPA_ID LEFT OUTER JOIN " + Environment.NewLine +

        ////            " FROM      dbo.JOB_Detail_Spare AS JD LEFT OUTER JOIN " + Environment.NewLine + 
        ////             "                   dbo.JOB_Detail ON JD.JOB_ID = dbo.JOB_Detail.JOB_ID AND JD.StartDate = dbo.JOB_Detail.StartDate LEFT OUTER JOIN " + Environment.NewLine +
        ////             "                   dbo.SpareParts ON JD.SPA_ID = dbo.SpareParts.SPA_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////             "                   dbo.JOB ON JD.JOB_ID = dbo.JOB.JOB_ID LEFT OUTER JOIN " + Environment.NewLine +
        ////             "                   dbo.JOB_Failure_Detail ON JD.JOB_ID = dbo.JOB_Failure_Detail.JOB_ID AND JD.POI_ID = dbo.JOB_Failure_Detail.POI_ID AND JD.Location = dbo.JOB_Failure_Detail.Location AND JD.FAI_ID = dbo.JOB_Failure_Detail.FAI_ID  " + Environment.NewLine +
        ////             "                  LEFT OUTER JOIN Location on  JD.Location = Location.LOC_ID " + Environment.NewLine +
        ////             "                  LEFT OUTER JOIN dbo.FFixedAsset_Station_Detail AS fsd ON JD.SerialNumber = fsd.SerialNumber " + Environment.NewLine +
        ////             "                  LEFT OUTER JOIN dbo.JOB AS j ON j.JOB_ID = JD.JOB_ID " + Environment.NewLine +
        ////             "                  LEFT OUTER JOIN dbo.Station AS st ON j.STA_ID = st.STA_ID " + Environment.NewLine +
        ////             "                  LEFT OUTER JOIN  dbo.Conf_Conditions ON JD.SPA_ID = dbo.Conf_Conditions.SPA_ID  and dbo.Conf_Conditions.GOP_ID = st.GOP_ID  and dbo.Conf_Conditions.TYP_ID = JOB.TYP_ID " + Environment.NewLine +
        ////             " WHERE     (dbo.JOB_Failure_Detail.IsCancel IS NULL OR " + Environment.NewLine +
        ////             "                   dbo.JOB_Failure_Detail.IsCancel = '0') AND (dbo.JOB_Detail.JOBS_ID > '03') AND (JD.IsCancel IS NULL OR " + Environment.NewLine +
        ////             "                   JD.IsCancel = 0) " + Environment.NewLine +
        ////               "";

        ////        JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);
        ////    */

        ////    /* 
        ////     * 20170607
        ////    //20151106 ใช้ script เดียวกันกับ บน server

        ////            sql = "  ALTER VIEW [dbo].[v_rpt_ServicesReport_web] " + Environment.NewLine + 
        ////             " AS  " + Environment.NewLine +
        ////             " SELECT DISTINCT  " + Environment.NewLine +
        ////             " dbo.JOB.JOB_ID, dbo.JOB.Opendate, dbo.JOB.FirstInDate, ISNULL(JOB_Detail_1.StartDate, dbo.JOB_Detail.StartDate) AS StartDate, dbo.JOB.CloseDate, dbo.Employees.EMP_ID AS EMP_ID3,  " + Environment.NewLine +
        ////             " ISNULL(dbo.Employees.FName, N'') + N' โทร. ' + ISNULL(dbo.Employees.Phone, N'') AS FName, dbo.Failure.Failure_th, dbo.Priority.Priority_GuestVip,  " + Environment.NewLine +
        ////             " PointFailure_1.PointFailure AS job_PointFailure, dbo.Type.Type, dbo.JOB_Status.JobStatus, Type_1.Type AS Type_1, dbo.JOB.Informer, dbo.JOB.InPhone, dbo.JOB.JobOutside,  " + Environment.NewLine +
        ////             " dbo.TypeJobClose.TypeCloseJob, dbo.JOB_ProblemType.Problem, ISNULL(dbo.JOB_Failure_Detail.NO, 0) AS fail_NO, ISNULL(dbo.JOB_Failure_Detail.POI_ID, N'000') AS fail_POI_ID,  " + Environment.NewLine +
        ////             " PointFailure_2.PointFailure AS PointFailure2, dbo.JOB_Failure_Detail.Failure_Detail, dbo.JOB_Failure_Detail.SerialNumber, JOB_Detail_1.EndDate, JOB_Detail_1.Resole_Detail,  " + Environment.NewLine +
        ////             " ISNULL(dbo.Station.STA_ID1, dbo.Station.STA_ID) AS STA_ID1, dbo.Station.StationSys, ISNULL(dbo.JOB.FAI_ID, N'00000') AS FAI_ID, dbo.JOB.PRI_ID, ISNULL(dbo.JOB.POI_ID, N'000')  " + Environment.NewLine +
        ////             " AS Job_POI_ID, dbo.JOB.TYP_ID, dbo.JOB.TYP_ID1, dbo.JOB.TCO_ID, dbo.JOB.PMT_ID, dbo.JOB.Contract, dbo.Station.Address + N' จ. ' + ISNULL(dbo.Province.Province, N'')  " + Environment.NewLine +
        ////             " + N' โซน: ' + ISNULL(dbo.ZONE.Detail, N'') AS Address, dbo.Station.TelNo, dbo.Station.FaxNo,  " + Environment.NewLine +
        ////             " dbo.JOB.JobFailure_Detail, dbo.JOB_Failure_Detail.StartLiter, dbo.JOB_Failure_Detail.EndLiter, dbo.JOB_Failure_Detail.LiterTest, dbo.JOB_Failure_Detail.Model, dbo.JOB_Failure_Detail.trandate,  " + Environment.NewLine +
        ////             " ISNULL(dbo.JOB_WorkID.Work_ID, N'') AS Work_ID, ISNULL(dbo.JOB_Failure_Detail.FAI_ID, N'0000000') AS PFAI_ID, dbo.JOB_Failure_Detail.IsCancel, dbo.JOB.JOBS_ID,  " + Environment.NewLine +
        ////             " dbo.JOB_Detail.JOBS_ID AS JOBS_ID2, ISNULL(dbo.JOB_WorkID.UserWorkOrder, N'') AS UserWorkOrder, JOB_Detail_1.StatusDetail, dbo.Product_Type.Product_Type AS Grade,  " + Environment.NewLine +
        ////             " dbo.Location.Location  AS Locate, ISNULL(dbo.JOB_Failure_Detail.Location, N'None') + ' ' + dbo.JOB_Failure_Detail.Failure_Detail AS FDetail,  " + Environment.NewLine +
        ////             " dbo.Failure_Action.Action_Failure AS Cause,dbo.Failure_Action_Solving.Failure_Action_Solving + ' ' + dbo.Failure_Solving_WI.WICode AS Solve  " + Environment.NewLine +
        ////             " , dbo.Station.InstallDate, dbo.Station.WarrantyDate  " + Environment.NewLine + //20160809

        ////             "  FROM         dbo.Priority RIGHT OUTER JOIN  " + Environment.NewLine +
        ////             " dbo.PointFailure AS PointFailure_2 RIGHT OUTER JOIN  " + Environment.NewLine +
        ////             " dbo.Location INNER JOIN  " + Environment.NewLine +
        ////             " dbo.JOB_Failure_Detail ON dbo.Location.LOC_ID = dbo.JOB_Failure_Detail.LOC_ID ON PointFailure_2.POI_ID = dbo.JOB_Failure_Detail.POI_ID RIGHT OUTER JOIN  " + Environment.NewLine +
        ////             " dbo.JOB LEFT OUTER JOIN  " + Environment.NewLine +
        ////             " dbo.JOB_WorkID ON dbo.JOB.JOB_ID = dbo.JOB_WorkID.JOB_ID ON dbo.JOB_Failure_Detail.JOB_ID = dbo.JOB.JOB_ID LEFT OUTER JOIN  " + Environment.NewLine +
        ////             " dbo.Station LEFT OUTER JOIN  " + Environment.NewLine +
        ////             " dbo.Province ON dbo.Station.PRO_ID = dbo.Province.PRO_ID LEFT OUTER JOIN  " + Environment.NewLine +
        ////             " dbo.ZONE ON dbo.Station.ZONE = dbo.ZONE.ZONE ON dbo.JOB.STA_ID = dbo.Station.STA_ID LEFT OUTER JOIN  " + Environment.NewLine +
        ////             " dbo.JOB_ProblemType ON dbo.JOB.PMT_ID = dbo.JOB_ProblemType.PMT_ID LEFT OUTER JOIN  " + Environment.NewLine +
        ////             " dbo.TypeJobClose ON dbo.JOB.TCO_ID = dbo.TypeJobClose.TCO_ID LEFT OUTER JOIN  " + Environment.NewLine +
        ////             " dbo.Type AS Type_1 ON dbo.JOB.TYP_ID1 = Type_1.TYP_ID LEFT OUTER JOIN  " + Environment.NewLine +
        ////             " dbo.JOB_Status ON dbo.JOB.JOBS_ID = dbo.JOB_Status.JOBS_ID LEFT OUTER JOIN  " + Environment.NewLine +
        ////             " dbo.Type ON dbo.JOB.TYP_ID = dbo.Type.TYP_ID LEFT OUTER JOIN  " + Environment.NewLine +
        ////             " dbo.PointFailure AS PointFailure_1 ON dbo.JOB.POI_ID = PointFailure_1.POI_ID ON dbo.Priority.PRI_ID = dbo.JOB.PRI_ID LEFT OUTER JOIN  " + Environment.NewLine +
        ////             " dbo.Failure ON dbo.JOB.FAI_ID = dbo.Failure.FAI_ID LEFT OUTER JOIN  " + Environment.NewLine +
        ////             " dbo.JOB_Detail AS JOB_Detail_1 LEFT OUTER JOIN  " + Environment.NewLine +
        ////             " dbo.Employees ON JOB_Detail_1.EMP_ID = dbo.Employees.EMP_ID ON dbo.JOB.JOB_ID = JOB_Detail_1.JOB_ID LEFT OUTER JOIN  " + Environment.NewLine +
        ////             " dbo.JOB_Detail ON dbo.JOB.JOB_ID = dbo.JOB_Detail.JOB_ID LEFT OUTER JOIN  " + Environment.NewLine +
        ////             " dbo.Province_Region ON dbo.Province.REG_ID = dbo.Province_Region.REG_ID LEFT OUTER JOIN  " + Environment.NewLine +
        ////             " dbo.Product_Type ON dbo.JOB_Failure_Detail.PRT_ID = dbo.Product_Type.PRT_ID LEFT OUTER JOIN  " + Environment.NewLine +
        ////             " dbo.Failure_Action ON dbo.JOB_Failure_Detail.ATF_ID = dbo.Failure_Action.ATF_ID LEFT OUTER JOIN  " + Environment.NewLine +
        ////             " dbo.Failure_Action_Solving ON dbo.JOB_Failure_Detail.FAS_ID = dbo.Failure_Action_Solving.FAS_ID LEFT OUTER JOIN  " + Environment.NewLine +
        ////             " dbo.Failure_Solving_WI ON dbo.JOB_Failure_Detail.SolvingByWI = dbo.Failure_Solving_WI.FSWI_ID  " + Environment.NewLine +
        ////             " WHERE     (dbo.JOB_Detail.JOBS_ID > '03') AND (dbo.JOB_Failure_Detail.IsCancel IS NULL OR  " + Environment.NewLine +
        ////             " dbo.JOB_Failure_Detail.IsCancel = '0')   " + Environment.NewLine +
        ////             "";
        ////            JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);
        ////    */
        ////    //20170607
        ////     sql =  "  ALTER VIEW [dbo].[v_rpt_ServicesReport_web] " + Environment.NewLine +
        ////              " AS  " + Environment.NewLine +
        ////              "  SELECT DISTINCT  " + Environment.NewLine + 
        ////              " dbo.JOB.JOB_ID, dbo.JOB.Opendate, dbo.JOB.FirstInDate, ISNULL(JOB_Detail_1.StartDate, dbo.JOB_Detail.StartDate) AS StartDate, dbo.JOB.CloseDate, dbo.Employees.EMP_ID AS EMP_ID3,   " + Environment.NewLine +
        ////              " ISNULL(dbo.Employees.FName, N'') + N' โทร. ' + ISNULL(dbo.Employees.Phone, N'') AS FName, dbo.Failure.Failure_th, dbo.Priority.Priority_GuestVip,   " + Environment.NewLine +
        ////              " PointFailure_1.PointFailure AS job_PointFailure, dbo.Type.Type, dbo.JOB_Status.JobStatus, Type_1.Type AS Type_1, dbo.JOB.Informer, dbo.JOB.InPhone, dbo.JOB.JobOutside,   " + Environment.NewLine +
        ////              " dbo.TypeJobClose.TypeCloseJob, dbo.JOB_ProblemType.Problem, ISNULL(dbo.JOB_Failure_Detail.NO, 0) AS fail_NO, ISNULL(dbo.JOB_Failure_Detail.POI_ID, N'000') AS fail_POI_ID,   " + Environment.NewLine +
        ////              " PointFailure_2.PointFailure AS PointFailure2, dbo.JOB_Failure_Detail.Failure_Detail, dbo.JOB_Failure_Detail.SerialNumber, JOB_Detail_1.EndDate, JOB_Detail_1.Resole_Detail,   " + Environment.NewLine +
        ////              " ISNULL(dbo.Station.STA_ID1, dbo.Station.STA_ID) AS STA_ID1, dbo.Station.StationSys, ISNULL(dbo.JOB.FAI_ID, N'00000') AS FAI_ID, dbo.JOB.PRI_ID, ISNULL(dbo.JOB.POI_ID, N'000')   " + Environment.NewLine +
        ////              " AS Job_POI_ID, dbo.JOB.TYP_ID, dbo.JOB.TYP_ID1, dbo.JOB.TCO_ID, dbo.JOB.PMT_ID, dbo.JOB.Contract, dbo.Station.Address + N' จ. ' + ISNULL(dbo.Province.Province, N'')   " + Environment.NewLine +
        ////              " + N' โซน: ' + ISNULL(dbo.ZONE.Detail, N'') AS Address, dbo.Station.TelNo, dbo.Station.FaxNo, dbo.JOB.JobFailure_Detail, dbo.JOB_Failure_Detail.StartLiter, dbo.JOB_Failure_Detail.EndLiter,   " + Environment.NewLine +
        ////              " dbo.JOB_Failure_Detail.LiterTest, dbo.JOB_Failure_Detail.Model, dbo.JOB_Failure_Detail.trandate, ISNULL(dbo.JOB_WorkID.Work_ID, N'') AS Work_ID, ISNULL(dbo.JOB_Failure_Detail.FAI_ID,   " + Environment.NewLine +
        ////              " N'0000000') AS PFAI_ID, dbo.JOB_Failure_Detail.IsCancel, dbo.JOB.JOBS_ID, dbo.JOB_Detail.JOBS_ID AS JOBS_ID2, ISNULL(dbo.JOB_WorkID.UserWorkOrder, N'') AS UserWorkOrder,   " + Environment.NewLine +
        ////              " JOB_Detail_1.StatusDetail, dbo.Product_Type.Product_Type AS Grade, dbo.Location.Location AS Locate, ISNULL(dbo.JOB_Failure_Detail.Location, N'None')   " + Environment.NewLine +
        ////              " + ' ' + dbo.JOB_Failure_Detail.Failure_Detail AS FDetail, dbo.Failure_Action.Action_Failure AS Cause,   " + Environment.NewLine +
        ////              " dbo.Failure_Action_Solving.Failure_Action_Solving + ' ' + dbo.Failure_Solving_WI.WICode AS Solve, dbo.Station.InstallDate, dbo.Station.WarrantyDate  " + Environment.NewLine +
        ////              " ,Employees.DEP_ID,Employees.Company " + Environment.NewLine +//20171207 SETS
        ////              " FROM         dbo.Priority RIGHT OUTER JOIN   " + Environment.NewLine +
        ////              " dbo.PointFailure AS PointFailure_2 RIGHT OUTER JOIN   " + Environment.NewLine +
        ////              " dbo.Location INNER JOIN   " + Environment.NewLine +
        ////              " dbo.JOB_Failure_Detail ON dbo.Location.LOC_ID = dbo.JOB_Failure_Detail.LOC_ID AND (dbo.JOB_Failure_Detail.IsCancel IS NULL OR   " + Environment.NewLine +
        ////              " dbo.JOB_Failure_Detail.IsCancel = '0') ON PointFailure_2.POI_ID = dbo.JOB_Failure_Detail.POI_ID AND (dbo.JOB_Failure_Detail.IsCancel IS NULL OR   " + Environment.NewLine +
        ////              " dbo.JOB_Failure_Detail.IsCancel = '0') RIGHT OUTER JOIN   " + Environment.NewLine +
        ////              " dbo.JOB LEFT OUTER JOIN   " + Environment.NewLine +
        ////              " dbo.JOB_WorkID ON dbo.JOB.JOB_ID = dbo.JOB_WorkID.JOB_ID ON dbo.JOB_Failure_Detail.JOB_ID = dbo.JOB.JOB_ID AND (dbo.JOB_Failure_Detail.IsCancel IS NULL OR   " + Environment.NewLine +
        ////              " dbo.JOB_Failure_Detail.IsCancel = '0') LEFT OUTER JOIN   " + Environment.NewLine +
        ////              " dbo.Station LEFT OUTER JOIN   " + Environment.NewLine +
        ////              " dbo.Province ON dbo.Station.PRO_ID = dbo.Province.PRO_ID LEFT OUTER JOIN   " + Environment.NewLine +
        ////              " dbo.ZONE ON dbo.Station.ZONE = dbo.ZONE.ZONE ON dbo.JOB.STA_ID = dbo.Station.STA_ID LEFT OUTER JOIN   " + Environment.NewLine +
        ////              " dbo.JOB_ProblemType ON dbo.JOB.PMT_ID = dbo.JOB_ProblemType.PMT_ID LEFT OUTER JOIN   " + Environment.NewLine +
        ////              " dbo.TypeJobClose ON dbo.JOB.TCO_ID = dbo.TypeJobClose.TCO_ID LEFT OUTER JOIN   " + Environment.NewLine +
        ////              " dbo.Type AS Type_1 ON dbo.JOB.TYP_ID1 = Type_1.TYP_ID LEFT OUTER JOIN   " + Environment.NewLine +
        ////              " dbo.JOB_Status ON dbo.JOB.JOBS_ID = dbo.JOB_Status.JOBS_ID LEFT OUTER JOIN   " + Environment.NewLine +
        ////              " dbo.Type ON dbo.JOB.TYP_ID = dbo.Type.TYP_ID LEFT OUTER JOIN   " + Environment.NewLine +
        ////              " dbo.PointFailure AS PointFailure_1 ON dbo.JOB.POI_ID = PointFailure_1.POI_ID ON dbo.Priority.PRI_ID = dbo.JOB.PRI_ID LEFT OUTER JOIN   " + Environment.NewLine +
        ////              " dbo.Failure ON dbo.JOB.FAI_ID = dbo.Failure.FAI_ID LEFT OUTER JOIN   " + Environment.NewLine +
        ////              " dbo.JOB_Detail AS JOB_Detail_1 LEFT OUTER JOIN   " + Environment.NewLine +
        ////              " dbo.Employees ON JOB_Detail_1.EMP_ID = dbo.Employees.EMP_ID ON dbo.JOB.JOB_ID = JOB_Detail_1.JOB_ID LEFT OUTER JOIN   " + Environment.NewLine +
        ////              " dbo.JOB_Detail ON dbo.JOB.JOB_ID = dbo.JOB_Detail.JOB_ID LEFT OUTER JOIN   " + Environment.NewLine +
        ////              " dbo.Province_Region ON dbo.Province.REG_ID = dbo.Province_Region.REG_ID LEFT OUTER JOIN   " + Environment.NewLine +
        ////              " dbo.Product_Type ON dbo.JOB_Failure_Detail.PRT_ID = dbo.Product_Type.PRT_ID AND (dbo.JOB_Failure_Detail.IsCancel IS NULL OR   " + Environment.NewLine +
        ////              " dbo.JOB_Failure_Detail.IsCancel = '0') LEFT OUTER JOIN   " + Environment.NewLine +
        ////              " dbo.Failure_Action ON dbo.JOB_Failure_Detail.ATF_ID = dbo.Failure_Action.ATF_ID AND (dbo.JOB_Failure_Detail.IsCancel IS NULL OR   " + Environment.NewLine +
        ////              " dbo.JOB_Failure_Detail.IsCancel = '0') LEFT OUTER JOIN   " + Environment.NewLine +
        ////              " dbo.Failure_Action_Solving ON dbo.JOB_Failure_Detail.FAS_ID = dbo.Failure_Action_Solving.FAS_ID AND (dbo.JOB_Failure_Detail.IsCancel IS NULL OR   " + Environment.NewLine +
        ////              " dbo.JOB_Failure_Detail.IsCancel = '0') LEFT OUTER JOIN   " + Environment.NewLine +
        ////              " dbo.Failure_Solving_WI ON dbo.JOB_Failure_Detail.SolvingByWI = dbo.Failure_Solving_WI.FSWI_ID AND (dbo.JOB_Failure_Detail.IsCancel IS NULL OR   " + Environment.NewLine +
        ////              " dbo.JOB_Failure_Detail.IsCancel = '0')   " + Environment.NewLine +
        ////              " WHERE     (dbo.JOB_Detail.JOBS_ID > '03')   " + Environment.NewLine +
        ////              "";
        ////                JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);



        ////                sql = " ALTER VIEW [dbo].[v_rpt_ServicesReport_web_PartDetail_Reprint]   " + Environment.NewLine +
        ////                 " AS   " + Environment.NewLine +
        ////                 " SELECT     JD.JOB_ID, ISNULL(JD.POI_ID, N'000') AS POI_ID, ISNULL(JD.NO, 0) AS PartLineNo, JD.SPA_ID, JD.Quantity, JD.Comment, JD.StatusSpare, JD.TRA_ID, dbo.SpareParts.SparePart, dbo.SpareParts.WH, ISNULL(JD.Location, N'None') AS LOC_ID, ISNULL(JD.FAI_ID,    " + Environment.NewLine +
        ////                " N'0000000') AS PFAI_ID, JD.IsCancel, CASE WHEN JOB.PROJ_ID IS NULL OR   " + Environment.NewLine +
        ////                " JOB.PROJ_ID = '' THEN CASE WHEN JD.IsPrices = '1' THEN JD.SumPrices ELSE 0 END ELSE 0 END AS SumPrices, CASE WHEN JOB.PROJ_ID IS NULL OR   " + Environment.NewLine +
        ////                " JOB.PROJ_ID = '' THEN CASE WHEN JD.IsPrices = '1' THEN JD.SumVAT ELSE 0 END ELSE 0 END AS SumVAT, CASE WHEN JOB.PROJ_ID IS NULL OR   " + Environment.NewLine + 
        ////                " JOB.PROJ_ID = '' THEN CASE WHEN JD.IsPrices = '1' THEN JD.SumTotalPrices ELSE 0 END ELSE 0 END AS SumTotalPrices, CASE WHEN JOB.PROJ_ID IS NULL OR    " + Environment.NewLine +
        ////                " JOB.PROJ_ID = '' THEN CASE WHEN JD.IsPrices = '1' THEN JD.Prices ELSE 0 END ELSE 0 END AS Prices, dbo.JOB_Failure_Detail.IsCancel AS FIsCancel, JD.StartDate, JD.Enddate,    " + Environment.NewLine +
        ////                " dbo.Conf_Conditions.ReferPage,   " + Environment.NewLine +
        ////                " dbo.Location.Location   " + Environment.NewLine +
        ////                " , fsd.WarrantDate   " + Environment.NewLine +
        ////                " , dbo.JOB.CloseDate   " + Environment.NewLine +
        ////                " , CASE WHEN WarrantDate <> NULL   " + Environment.NewLine +
        ////                " THEN CASE WHEN WarrantDate > job.CloseDate THEN '[Y]' ELSE '[N]' END ELSE '[N]' END AS WA    " + Environment.NewLine +
        ////                " FROM      dbo.JOB_Detail_Spare AS JD LEFT OUTER JOIN   " + Environment.NewLine +
        ////                " dbo.JOB_Detail ON JD.JOB_ID = dbo.JOB_Detail.JOB_ID AND JD.StartDate = dbo.JOB_Detail.StartDate LEFT OUTER JOIN   " + Environment.NewLine +
        ////                " dbo.SpareParts ON JD.SPA_ID = dbo.SpareParts.SPA_ID LEFT OUTER JOIN   " + Environment.NewLine +
        ////                " dbo.JOB ON JD.JOB_ID = dbo.JOB.JOB_ID LEFT OUTER JOIN   " + Environment.NewLine +
        ////                " dbo.JOB_Failure_Detail ON JD.JOB_ID = dbo.JOB_Failure_Detail.JOB_ID AND JD.POI_ID = dbo.JOB_Failure_Detail.POI_ID AND JD.Location = dbo.JOB_Failure_Detail.Location AND JD.FAI_ID = dbo.JOB_Failure_Detail.FAI_ID    " + Environment.NewLine +
        ////                " LEFT OUTER JOIN Location on  JD.Location = Location.LOC_ID   " + Environment.NewLine +
        ////                " LEFT OUTER JOIN dbo.FFixedAsset_Station_Detail AS fsd ON JD.SerialNumber = fsd.SerialNumber   " + Environment.NewLine +
        ////                " LEFT OUTER JOIN dbo.JOB AS j ON j.JOB_ID = JD.JOB_ID   " + Environment.NewLine +
        ////                " LEFT OUTER JOIN dbo.Station AS st ON j.STA_ID = st.STA_ID   " + Environment.NewLine +
        ////                " LEFT OUTER JOIN  dbo.Conf_Conditions ON JD.SPA_ID = dbo.Conf_Conditions.SPA_ID  and dbo.Conf_Conditions.GOP_ID = st.GOP_ID  and dbo.Conf_Conditions.TYP_ID = JOB.TYP_ID   " + Environment.NewLine +
        ////                "  AND dbo.Conf_Conditions.ContractNo = dbo.JOB.ContractNo  " + Environment.NewLine +//20160317
        ////                "  AND dbo.JOB.TYP_ID1 = dbo.Conf_Conditions.TYP_ID1  " + Environment.NewLine +//20180706
        ////                " WHERE     (dbo.JOB_Failure_Detail.IsCancel IS NULL OR   " + Environment.NewLine +
        ////                " dbo.JOB_Failure_Detail.IsCancel = '0') AND (dbo.JOB_Detail.JOBS_ID > '03') AND (JD.IsCancel IS NULL OR   " + Environment.NewLine +
        ////              //20160829// " JD.IsCancel = 0)   " + Environment.NewLine +
        ////                 " JD.IsCancel = 0)  AND (JD.TRA_ID NOT IN ('IC', 'RT'))  " + Environment.NewLine +
        ////                "";
        ////                JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////    #endregion

        ////    //20141128
        ////    #region PointFailure
        ////    try
        ////    {

        ////        sql = "   if Not Exists(select * from sys.columns where Name = N'OrderNo' and Object_ID = Object_ID(N'PointFailure')) " + Environment.NewLine +
        ////       "ALTER TABLE PointFailure ADD OrderNo int NULL";
        ////        JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);

        ////       //20150220// sql = "   if Not Exists(select * from sys.columns where Name = N'JOBType' and Object_ID = Object_ID(N'PointFailure')) " + Environment.NewLine +
        ////       //"ALTER TABLE PointFailure ADD JOBType char(2) NULL";
        ////       // JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);



        ////    }
        ////    catch (Exception)
        ////    {

        ////    }

        ////    #endregion


        ////    //////======v 18.2.3.2016=====           

        ////    ////    try
        ////    ////    {
        ////    ////        sql = "" +
        ////    ////            //PricesList
        ////    ////                               " if Not Exists(select * from sys.columns where Name = N'TCO_ID' and Object_ID = Object_ID(N'JOB_Detail'))" +
        ////    ////                               "   ALTER TABLE JOB_Detail  ADD TCO_ID  char (2) NULL CONSTRAINT [DF_JOB_Detail]  DEFAULT (('00'));";



        ////    ////        JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);
        ////    ////    }
        ////    ////    catch (Exception)
        ////    ////    {

        ////    ////    }


        ////    //////=====update==========
        ////    ////    try
        ////    ////    {
        ////    ////        sql = "" +
        ////    ////            //PricesList
        ////    ////                               " if Exists(select * from sys.columns where Name = N'TCO_ID' and Object_ID = Object_ID(N'JOB_Detail'))" +
        ////    ////                               "  UPDATE JOB_Detail SET TCO_ID ='04' WHERE JOBS_ID IN ('07','99') AND TCO_ID IS NULL;UPDATE JOB_Detail SET TCO_ID ='00' WHERE JOBS_ID NOT IN ('07','99') AND TCO_ID IS NULL;";



        ////    ////        JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);
        ////    ////    }
        ////    ////    catch (Exception)
        ////    ////    {

        ////    ////    }

        ////    //////====20161116=====
        ////    //////==========================================================

        ////    //// try
        ////    ////    {
        ////    ////    sql = "";
        ////    ////    sql = " if Not Exists(select * from sys.columns where Name = N'ReferPage' and Object_ID = Object_ID(N'JOB_Detail_Spare')) " + Environment.NewLine +
        ////    ////          "     ALTER TABLE JOB_Detail_Spare  ADD ReferPage nvarchar(10) NULL;";

        ////    ////    JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);
        ////    ////    }
        ////    //// catch (Exception)
        ////    //// {

        ////    //// }

        ////    //////==20181122========================================================

        ////    //// try
        ////    //// {
        ////    ////     sql = "";
        ////    ////     sql = " if Not Exists(select * from sys.columns where Name = N'IsPriceChange' and Object_ID = Object_ID(N'SpareParts')) " + Environment.NewLine +
        ////    ////           "     ALTER TABLE SpareParts  ADD IsPriceChange bit NULL;";

        ////    ////     JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);
        ////    //// }
        ////    //// catch (Exception)
        ////    //// {

        ////    //// }

        ////    //////==20181123=============================================================
        ////    //// try
        ////    //// {
        ////    ////     sql = "";
        ////    ////     sql = " if Not Exists(select * from sys.columns where Name = N'TRA_ID' and Object_ID = Object_ID(N'JOB_Project_Detail')) " + Environment.NewLine +
        ////    ////           "     ALTER TABLE JOB_Project_Detail  ADD TRA_ID char(2) NULL;";

        ////    ////     JaSqlHelper.ExecuteNonQuery(strConn, System.Data.CommandType.Text, sql);
        ////    //// }
        ////    //// catch (Exception)
        ////    //// {

        ////    //// }



        ////}//updateScript()

        #endregion


    }


}