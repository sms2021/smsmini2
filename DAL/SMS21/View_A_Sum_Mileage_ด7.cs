//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMSMINI.DAL.SMS21
{
    using System;
    using System.Collections.Generic;
    
    public partial class View_A_Sum_Mileage_ด7
    {
        public string JOB_ID { get; set; }
        public Nullable<System.DateTime> Opendate { get; set; }
        public Nullable<int> jmonth { get; set; }
        public Nullable<int> jyear { get; set; }
        public Nullable<System.DateTime> CloseDate { get; set; }
        public string ประเภท_JOB { get; set; }
        public string jType { get; set; }
        public string jWorkType { get; set; }
        public string EMP_ID { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string Department { get; set; }
        public System.DateTime StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public string Resole_Detail { get; set; }
        public Nullable<decimal> Mileage { get; set; }
        public Nullable<decimal> PriceKM { get; set; }
    }
}
