//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMSMINI.DAL.SMS21
{
    using System;
    using System.Collections.Generic;
    
    public partial class vw_JOB_Detail_CheckServiceReport_mail_1_TEST
    {
        public string JOB_ID { get; set; }
        public System.DateTime StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public string VANNo { get; set; }
        public string Station { get; set; }
        public string UserInput { get; set; }
        public string Remark { get; set; }
        public string JobFailure_Detail { get; set; }
        public string Resole_Detail { get; set; }
        public string ChekSV { get; set; }
    }
}
