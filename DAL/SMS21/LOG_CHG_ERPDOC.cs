//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMSMINI.DAL.SMS21
{
    using System;
    using System.Collections.Generic;
    
    public partial class LOG_CHG_ERPDOC
    {
        public string JOB_ID { get; set; }
        public System.DateTime RecDate { get; set; }
        public System.DateTime TranDate { get; set; }
        public string ERP_DOCUMENT { get; set; }
    }
}
