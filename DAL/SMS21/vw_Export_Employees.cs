//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMSMINI.DAL.SMS21
{
    using System;
    using System.Collections.Generic;
    
    public partial class vw_Export_Employees
    {
        public string VAN_No { get; set; }
        public string VAN_ERP_ID { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string SUP_ERP_ID { get; set; }
        public string SUP_Name { get; set; }
        public string SMG_ERP_ID { get; set; }
        public string SMG_Name { get; set; }
        public string DMG_ERP_ID { get; set; }
        public string DMG_Name { get; set; }
        public string DEP_ID { get; set; }
        public string Department { get; set; }
    }
}
