//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMSMINI.DAL.SMS21
{
    using System;
    using System.Collections.Generic;
    
    public partial class vw_Summary_Expense_Revenue_VAN_QI_5
    {
        public string VAN_ID { get; set; }
        public Nullable<System.DateTime> minDate { get; set; }
        public Nullable<System.DateTime> AuditDate { get; set; }
        public Nullable<decimal> vS1 { get; set; }
        public Nullable<decimal> vS2 { get; set; }
        public Nullable<decimal> vS3 { get; set; }
        public Nullable<decimal> vS4 { get; set; }
        public string mDate { get; set; }
        public Nullable<int> cS1 { get; set; }
        public Nullable<int> cS2 { get; set; }
        public Nullable<int> cS3 { get; set; }
        public Nullable<int> cS4 { get; set; }
        public Nullable<decimal> qS1 { get; set; }
        public Nullable<decimal> qS2 { get; set; }
        public Nullable<decimal> qS3 { get; set; }
        public Nullable<decimal> qS4 { get; set; }
    }
}
