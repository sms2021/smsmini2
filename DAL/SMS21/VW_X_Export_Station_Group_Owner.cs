//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMSMINI.DAL.SMS21
{
    using System;
    using System.Collections.Generic;
    
    public partial class VW_X_Export_Station_Group_Owner
    {
        public string STA_ID { get; set; }
        public string StationSys { get; set; }
        public string Address { get; set; }
        public string GOP_ID { get; set; }
        public string Group_Owner { get; set; }
        public string GroupStation { get; set; }
        public string TradeName { get; set; }
    }
}
