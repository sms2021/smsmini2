//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMSMINI.DAL.SMS21
{
    using System;
    using System.Collections.Generic;
    
    public partial class vw_JML_JOB_Detail____
    {
        public string JOB_ID { get; set; }
        public string StationSys { get; set; }
        public string TYP_ID1 { get; set; }
        public string VAN { get; set; }
        public Nullable<System.DateTime> CloseDate { get; set; }
        public Nullable<System.DateTime> Start_Date { get; set; }
        public string Remark_old { get; set; }
        public Nullable<System.DateTime> Stop_Date { get; set; }
        public string Remark { get; set; }
        public string JOBS_ID { get; set; }
        public decimal Distance { get; set; }
        public string Resole_Detail { get; set; }
    }
}
