//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMSMINI.DAL.SMS21
{
    using System;
    using System.Collections.Generic;
    
    public partial class vw_JOB_SO_Employee_View___0
    {
        public Nullable<bool> PayComm { get; set; }
        public string JOB_ID { get; set; }
        public System.DateTime CloseDate { get; set; }
        public string รห_สพน_กงาน { get; set; }
        public string ช__อ { get; set; }
        public string ฝ_าย { get; set; }
        public int COM_ID { get; set; }
        public string CommName { get; set; }
        public string ComType { get; set; }
        public string INV_NO { get; set; }
        public string INV_DATE { get; set; }
        public Nullable<decimal> ค_าคอมม_ชช__น1 { get; set; }
        public Nullable<decimal> ค_าคอมม_ชช__น { get; set; }
        public decimal CommSpecial { get; set; }
        public string Status0 { get; set; }
        public string Status { get; set; }
        public string Type { get; set; }
        public string หมายเหต_ { get; set; }
        public string ERP_EMP_ID { get; set; }
        public string SO_NO { get; set; }
        public string TYP_ID1 { get; set; }
        public string WorkType { get; set; }
        public Nullable<System.DateTime> SO_Date { get; set; }
        public Nullable<decimal> SO_Price { get; set; }
        public string ReceiveNo { get; set; }
        public Nullable<System.DateTime> ReceiveDate { get; set; }
        public Nullable<decimal> ReceiveAmt1 { get; set; }
        public decimal noAmount { get; set; }
        public string StationSys { get; set; }
        public string prodgroup_code { get; set; }
        public string prodgroup_name { get; set; }
        public decimal prodgroup_qty { get; set; }
        public Nullable<decimal> noAmount2 { get; set; }
        public Nullable<decimal> ReceiveAmt { get; set; }
        public Nullable<System.DateTime> PayDate { get; set; }
        public Nullable<decimal> CommAmount { get; set; }
        public decimal PercentComm { get; set; }
        public Nullable<decimal> PartSetSUR { get; set; }
        public Nullable<decimal> PartSetINT { get; set; }
        public Nullable<decimal> PartSetTRN { get; set; }
        public Nullable<decimal> PartSetTotal1 { get; set; }
        public Nullable<decimal> PartSetTotal { get; set; }
        public int SP_ID { get; set; }
        public Nullable<decimal> SPAmount { get; set; }
    }
}
