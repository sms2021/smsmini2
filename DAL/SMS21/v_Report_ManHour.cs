//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMSMINI.DAL.SMS21
{
    using System;
    using System.Collections.Generic;
    
    public partial class v_Report_ManHour
    {
        public string EMP_ID3 { get; set; }
        public string FName { get; set; }
        public string JOB_ID { get; set; }
        public string STA_ID { get; set; }
        public string StationSys { get; set; }
        public string GOP_ID { get; set; }
        public Nullable<System.DateTime> Opendate { get; set; }
        public Nullable<System.DateTime> FirstInDate { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public Nullable<System.DateTime> CloseDate { get; set; }
        public string Contract { get; set; }
        public string JobStatus { get; set; }
        public string Type { get; set; }
        public string Failure_code { get; set; }
        public string Failure { get; set; }
        public string Problem { get; set; }
    }
}
