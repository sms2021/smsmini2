//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMSMINI.DAL.SMS21
{
    using System;
    using System.Collections.Generic;
    
    public partial class v_rpt_ServicesReport_web_PloblemResolve
    {
        public string JOB_ID { get; set; }
        public System.DateTime StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public string Problem_Detail { get; set; }
        public string Resole_Detail { get; set; }
        public Nullable<System.DateTime> RecDate { get; set; }
        public string JOBS_ID { get; set; }
    }
}
