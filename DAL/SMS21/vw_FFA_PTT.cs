//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMSMINI.DAL.SMS21
{
    using System;
    using System.Collections.Generic;
    
    public partial class vw_FFA_PTT
    {
        public string STA_ID { get; set; }
        public string StationSys { get; set; }
        public string TradeName { get; set; }
        public string SerialNumber { get; set; }
        public string FModel { get; set; }
        public Nullable<System.DateTime> InstallDate { get; set; }
        public Nullable<System.DateTime> WarrantDate { get; set; }
        public string Detail { get; set; }
        public string FBrand { get; set; }
        public string FixedAsset_CateName { get; set; }
        public string เจ_าของ { get; set; }
        public string ม_อจ_าย { get; set; }
        public string สายจ_าย { get; set; }
        public string เบรคอะเว { get; set; }
    }
}
