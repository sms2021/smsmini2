//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMSMINI.DAL.SMS21
{
    using System;
    using System.Collections.Generic;
    
    public partial class v_COUNT_JOB
    {
        public string JOB_ID { get; set; }
        public string GOP_ID { get; set; }
        public string GroupStation { get; set; }
        public Nullable<int> cMONTH { get; set; }
        public Nullable<int> cYEAR { get; set; }
        public string TYP_ID { get; set; }
    }
}
