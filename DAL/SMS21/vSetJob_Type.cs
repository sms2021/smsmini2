//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMSMINI.DAL.SMS21
{
    using System;
    using System.Collections.Generic;
    
    public partial class vSetJob_Type
    {
        public string JOB_ID { get; set; }
        public string TYP_ID { get; set; }
        public string Type { get; set; }
        public string StatType { get; set; }
        public Nullable<System.DateTime> FirstInDate { get; set; }
    }
}
