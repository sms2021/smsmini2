//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMSMINI.DAL.SMS21
{
    using System;
    using System.Collections.Generic;
    
    public partial class v_FFA_SerialNumber_Pool
    {
        public string SerialNumber { get; set; }
        public string SerialNumberBarcode { get; set; }
        public int FCAT_ID { get; set; }
        public string FixedAsset_CateName { get; set; }
        public int FBA_ID { get; set; }
        public string FBrand { get; set; }
        public int FMD_ID { get; set; }
        public string FixedAssetName { get; set; }
        public string EMP_ID { get; set; }
        public Nullable<System.DateTime> RegisDate { get; set; }
        public string Detail { get; set; }
        public string FMDetail { get; set; }
        public string StatusUse { get; set; }
    }
}
