//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMSMINI.DAL.SMS21
{
    using System;
    using System.Collections.Generic;
    
    public partial class JOB_Statement_Commission_COM_1_INT_20210430____
    {
        public System.DateTime TranDate { get; set; }
        public System.DateTime CloseDate { get; set; }
        public string JOB_ID { get; set; }
        public string SO_NO { get; set; }
        public Nullable<System.DateTime> SO_Date { get; set; }
        public Nullable<decimal> SO_Price { get; set; }
        public string ERP_EMP_ID { get; set; }
        public int COM_ID { get; set; }
        public string INV_NO { get; set; }
        public string INV_DATE { get; set; }
        public Nullable<decimal> INV_Amt { get; set; }
        public Nullable<decimal> Commission { get; set; }
        public decimal PercentComm { get; set; }
        public string Status { get; set; }
        public string Type { get; set; }
        public string Remark { get; set; }
        public string ReceiveNo { get; set; }
        public Nullable<System.DateTime> ReceiveDate { get; set; }
        public Nullable<decimal> ReceiveAmt { get; set; }
        public Nullable<System.DateTime> PayDate { get; set; }
        public int SP_ID { get; set; }
        public Nullable<decimal> Cost_Amt { get; set; }
        public Nullable<decimal> PercentCOM { get; set; }
        public Nullable<decimal> PercentCOM2 { get; set; }
        public Nullable<decimal> sPercentComm { get; set; }
        public string DEP_ID { get; set; }
        public Nullable<System.DateTime> PrintPayroll { get; set; }
    }
}
