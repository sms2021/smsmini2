//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMSMINI.DAL.SMS21
{
    using System;
    using System.Collections.Generic;
    
    public partial class vw_report_SMSJOBMonitring_CountJOBClose_Header
    {
        public string JOB_ID { get; set; }
        public string EMP_ID3 { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string JobStatus { get; set; }
        public string STA_ID { get; set; }
        public string StationSys { get; set; }
        public string JOBType { get; set; }
        public string WorkType { get; set; }
        public string Informer { get; set; }
        public string InPhone { get; set; }
        public Nullable<System.DateTime> Opendate { get; set; }
        public Nullable<System.DateTime> FirstInDate { get; set; }
        public Nullable<System.DateTime> CloseDate { get; set; }
        public string JobFailure_Detail { get; set; }
        public Nullable<System.DateTime> UploadDate { get; set; }
        public string SUP_ID { get; set; }
        public string SMG_ID { get; set; }
        public string DMG_ID { get; set; }
    }
}
