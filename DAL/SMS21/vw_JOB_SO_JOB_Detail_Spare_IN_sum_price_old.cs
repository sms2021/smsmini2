//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMSMINI.DAL.SMS21
{
    using System;
    using System.Collections.Generic;
    
    public partial class vw_JOB_SO_JOB_Detail_Spare_IN_sum_price_old
    {
        public string JOB_ID { get; set; }
        public string EMP_ID { get; set; }
        public Nullable<decimal> SumPrices { get; set; }
        public Nullable<decimal> SumVAT { get; set; }
        public Nullable<decimal> SumTotalPrices { get; set; }
        public string TRA_ID { get; set; }
        public Nullable<System.DateTime> CloseDate { get; set; }
    }
}
