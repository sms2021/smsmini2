//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMSMINI.DAL.SMS21
{
    using System;
    using System.Collections.Generic;
    
    public partial class v_Monthly_Report_Part_NEW
    {
        public int NOx { get; set; }
        public Nullable<long> NO { get; set; }
        public string JOB_ID { get; set; }
        public System.DateTime StartDate { get; set; }
        public string SPA_ID { get; set; }
        public string SparePart { get; set; }
        public Nullable<System.DateTime> Enddate { get; set; }
        public Nullable<decimal> Quantity { get; set; }
        public string Comment { get; set; }
        public Nullable<bool> StatusSpare { get; set; }
        public string TRA_ID { get; set; }
        public Nullable<decimal> PricePerUnit { get; set; }
        public string IsCancel { get; set; }
        public string Location { get; set; }
        public Nullable<decimal> PricesList { get; set; }
        public Nullable<decimal> Prices { get; set; }
        public Nullable<decimal> SumPrices1 { get; set; }
        public Nullable<decimal> SumVAT { get; set; }
        public Nullable<decimal> SumTotalPrices { get; set; }
        public Nullable<decimal> VAT { get; set; }
        public Nullable<decimal> Discount { get; set; }
        public string SerialNumber { get; set; }
        public string FixAssetNo { get; set; }
        public string SGT_ID { get; set; }
    }
}
