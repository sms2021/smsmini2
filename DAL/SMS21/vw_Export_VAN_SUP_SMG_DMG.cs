//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMSMINI.DAL.SMS21
{
    using System;
    using System.Collections.Generic;
    
    public partial class vw_Export_VAN_SUP_SMG_DMG
    {
        public string van_No { get; set; }
        public string van_id { get; set; }
        public string vanFName { get; set; }
        public string vanLName { get; set; }
        public string sup_no { get; set; }
        public string sup_id { get; set; }
        public string supFName { get; set; }
        public string supLName { get; set; }
        public string smg_no { get; set; }
        public string smg_id { get; set; }
        public string smgFName { get; set; }
        public string smgLName { get; set; }
        public string dmg_no { get; set; }
        public string dmg_id { get; set; }
        public string dmgFName { get; set; }
        public string dmgLName { get; set; }
        public string org { get; set; }
    }
}
