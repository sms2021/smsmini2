//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMSMINI.DAL.SMS21
{
    using System;
    using System.Collections.Generic;
    
    public partial class vrtp_ReportSparepart
    {
        public string SPA_ID { get; set; }
        public string SparePart { get; set; }
        public Nullable<decimal> PricePerUnit { get; set; }
        public Nullable<System.DateTime> WartDate { get; set; }
        public Nullable<int> InStock { get; set; }
        public Nullable<int> Minimum { get; set; }
        public string Description { get; set; }
        public string BrandName { get; set; }
        public string SuplierName { get; set; }
        public string TelNo { get; set; }
    }
}
