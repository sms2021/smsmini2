//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMSMINI.DAL.SMS21
{
    using System;
    using System.Collections.Generic;
    
    public partial class vCloseJobGetSparePart_byJOB
    {
        public string JOB_ID { get; set; }
        public string EMP_ID { get; set; }
        public string SPA_ID { get; set; }
        public string SparePart { get; set; }
        public System.DateTime StartDate { get; set; }
        public Nullable<System.DateTime> Enddate { get; set; }
        public string Comment { get; set; }
        public Nullable<bool> StatusSpare { get; set; }
        public int NO { get; set; }
        public string JOBS_ID { get; set; }
        public string TRA_ID { get; set; }
        public decimal PricePerUnit { get; set; }
        public Nullable<decimal> VAT { get; set; }
        public Nullable<decimal> Prices { get; set; }
        public Nullable<decimal> Discount { get; set; }
        public Nullable<bool> IsPrices { get; set; }
        public Nullable<decimal> Quantity { get; set; }
    }
}
