//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMSMINI.DAL.SMS21
{
    using System;
    using System.Collections.Generic;
    
    public partial class Station_Group_Logo
    {
        public string GOP_ID { get; set; }
        public byte[] ImgLogo { get; set; }
        public Nullable<double> Width { get; set; }
        public Nullable<double> Height { get; set; }
    }
}
