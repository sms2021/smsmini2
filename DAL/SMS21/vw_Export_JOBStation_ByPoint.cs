//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMSMINI.DAL.SMS21
{
    using System;
    using System.Collections.Generic;
    
    public partial class vw_Export_JOBStation_ByPoint
    {
        public string JOB_ID { get; set; }
        public Nullable<System.DateTime> Opendate { get; set; }
        public Nullable<System.DateTime> FirstInDate { get; set; }
        public Nullable<System.DateTime> CloseDate { get; set; }
        public string GroupStation { get; set; }
        public string StationSys { get; set; }
        public string Address { get; set; }
        public string ZONE { get; set; }
        public string JobFailure_Detail { get; set; }
        public string Resole_Detail { get; set; }
        public string SPA_ID { get; set; }
        public decimal Quantity { get; set; }
        public string POI_ID { get; set; }
        public string PointFailure { get; set; }
        public string JobStatus { get; set; }
        public string Location { get; set; }
        public string SerialNumber { get; set; }
    }
}
