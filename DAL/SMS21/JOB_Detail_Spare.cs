//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMSMINI.DAL.SMS21
{
    using System;
    using System.Collections.Generic;
    
    public partial class JOB_Detail_Spare
    {
        public int NO { get; set; }
        public string JOB_ID { get; set; }
        public string SPA_ID { get; set; }
        public System.DateTime StartDate { get; set; }
        public Nullable<System.DateTime> Enddate { get; set; }
        public string EMP_ID { get; set; }
        public Nullable<decimal> Quantity { get; set; }
        public Nullable<decimal> PricesList { get; set; }
        public Nullable<decimal> Prices { get; set; }
        public Nullable<decimal> SumPrices { get; set; }
        public Nullable<decimal> SumVAT { get; set; }
        public Nullable<decimal> SumTotalPrices { get; set; }
        public Nullable<decimal> VAT { get; set; }
        public Nullable<decimal> Discount { get; set; }
        public string Comment { get; set; }
        public Nullable<bool> StatusSpare { get; set; }
        public string TRA_ID { get; set; }
        public Nullable<bool> AdminSendPart { get; set; }
        public Nullable<System.DateTime> SendPartDate { get; set; }
        public string SendPartRemark { get; set; }
        public string ADM_ID { get; set; }
        public string POI_ID { get; set; }
        public string Location { get; set; }
        public Nullable<int> EQP_ID { get; set; }
        public string FAI_ID { get; set; }
        public string SerialNumber { get; set; }
        public string IsCancel { get; set; }
        public string IsDownload { get; set; }
        public Nullable<bool> IsPrices { get; set; }
        public string ControlFixAssetNo { get; set; }
        public string BillingCost { get; set; }
        public string ERP_StationCharge { get; set; }
        public Nullable<System.DateTime> ERP_LoadTime { get; set; }
        public string ERP_SPA_ID { get; set; }
        public string ERP_PartName { get; set; }
        public Nullable<bool> IsSlipUpload { get; set; }
        public string IsCustomer { get; set; }
        public Nullable<int> ERP_orderline_id { get; set; }
        public string SerialNumber_New { get; set; }
        public string SerialNumber_Old { get; set; }
        public Nullable<int> PRT_ID { get; set; }
        public string ReferPage { get; set; }
        public Nullable<System.DateTime> LastEditDate { get; set; }
        public string LastEditBy { get; set; }
        public string LastEditByHOST_NAME { get; set; }
    
        public virtual JOB_Detail JOB_Detail { get; set; }
        public virtual SparePart_TranType SparePart_TranType { get; set; }
        public virtual SpareParts SpareParts { get; set; }
    }
}
