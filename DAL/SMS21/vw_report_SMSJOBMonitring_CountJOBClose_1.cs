//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMSMINI.DAL.SMS21
{
    using System;
    using System.Collections.Generic;
    
    public partial class vw_report_SMSJOBMonitring_CountJOBClose_1
    {
        public string VAN_No { get; set; }
        public int cCloseDate { get; set; }
        public int cJOB { get; set; }
        public Nullable<int> AVGDay { get; set; }
    }
}
