//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMSMINI.DAL.SMS21
{
    using System;
    using System.Collections.Generic;
    
    public partial class Conf_Conditions_PriceList
    {
        public string PriceListNo { get; set; }
        public string TYP_ID { get; set; }
        public string TYP_ID1 { get; set; }
        public string SPA_ID { get; set; }
        public string TRA_ID { get; set; }
        public decimal PriceList { get; set; }
        public Nullable<System.DateTime> Tran_Date { get; set; }
        public Nullable<bool> IsPartPriceCharges { get; set; }
        public Nullable<bool> IsAutoPart { get; set; }
        public string ERP_SPA_ID { get; set; }
        public string ERP_TRA_ID { get; set; }
        public string ReferPage { get; set; }
        public Nullable<System.DateTime> ERP_Loadtime { get; set; }
    }
}
