//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMSMINI.DAL.SMS21
{
    using System;
    using System.Collections.Generic;
    
    public partial class Company
    {
        public string COM_ID { get; set; }
        public string CompanyName { get; set; }
        public string Address { get; set; }
        public string PosOffice { get; set; }
        public string TelNo { get; set; }
        public string FaxNo { get; set; }
        public string Province { get; set; }
        public string Region { get; set; }
    }
}
