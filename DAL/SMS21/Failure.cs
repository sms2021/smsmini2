//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMSMINI.DAL.SMS21
{
    using System;
    using System.Collections.Generic;
    
    public partial class Failure
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Failure()
        {
            this.JOB = new HashSet<JOB>();
        }
    
        public string FAI_ID { get; set; }
        public string Failure_th { get; set; }
        public string Failure_en { get; set; }
        public string PRI_ID { get; set; }
        public string GRF_ID { get; set; }
        public string TYP_ID { get; set; }
        public Nullable<bool> IsCancel { get; set; }
        public Nullable<decimal> UseTime_SUM_MINUTE { get; set; }
        public string UseTime_SUM_HOUR { get; set; }
    
        public virtual GroupFailure GroupFailure { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JOB> JOB { get; set; }
    }
}
