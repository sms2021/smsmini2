//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMSMINI.DAL.SMS21
{
    using System;
    using System.Collections.Generic;
    
    public partial class vw_Export_JOB_Part_DMG
    {
        public string JOB_ID { get; set; }
        public string SPA_ID { get; set; }
        public string SparePart { get; set; }
        public string DMG_ID { get; set; }
        public Nullable<decimal> Quantity { get; set; }
        public System.DateTime StartDate { get; set; }
        public string Division { get; set; }
        public string DEP_ID { get; set; }
        public string Department { get; set; }
        public string EMP_ID { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string GOP_ID { get; set; }
        public string TradeName { get; set; }
        public string TRA_ID { get; set; }
    }
}
