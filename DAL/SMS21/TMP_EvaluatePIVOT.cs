//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMSMINI.DAL.SMS21
{
    using System;
    using System.Collections.Generic;
    
    public partial class TMP_EvaluatePIVOT
    {
        public string STA_ID { get; set; }
        public string StationSys { get; set; }
        public string JOB_ID { get; set; }
        public string VAN { get; set; }
        public Nullable<System.DateTime> Opendate { get; set; }
        public Nullable<System.DateTime> CloseDate { get; set; }
        public Nullable<System.DateTime> CustTranDate { get; set; }
        public string อาการเส_ย { get; set; }
        public string การซ_อม { get; set; }
        public string Informer { get; set; }
        public string ประเภทงาน { get; set; }
        public string ประเภทJOB { get; set; }
        public string ฝ_าย { get; set; }
        public Nullable<int> SumCustPoint { get; set; }
        public Nullable<int> C01 { get; set; }
        public Nullable<int> C02 { get; set; }
        public Nullable<int> C03 { get; set; }
        public Nullable<int> C04 { get; set; }
        public Nullable<int> C05 { get; set; }
        public Nullable<int> C06 { get; set; }
        public Nullable<int> C07 { get; set; }
        public Nullable<int> C08 { get; set; }
        public Nullable<int> C09 { get; set; }
        public Nullable<int> C10 { get; set; }
        public Nullable<int> C11 { get; set; }
    }
}
