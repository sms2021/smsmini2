//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMSMINI.DAL.SMS21
{
    using System;
    using System.Collections.Generic;
    
    public partial class JOB_PhotoReport
    {
        public string JOB_ID { get; set; }
        public System.DateTime StartDate { get; set; }
        public System.DateTime Enddate { get; set; }
        public System.DateTime ReportDate { get; set; }
        public string VAN_ID { get; set; }
        public string Equipment { get; set; }
        public string Failure_Detail { get; set; }
        public Nullable<System.DateTime> TranDate { get; set; }
        public byte[] ImgStation { get; set; }
    }
}
