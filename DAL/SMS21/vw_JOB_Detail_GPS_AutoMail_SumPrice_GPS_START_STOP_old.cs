//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMSMINI.DAL.SMS21
{
    using System;
    using System.Collections.Generic;
    
    public partial class vw_JOB_Detail_GPS_AutoMail_SumPrice_GPS_START_STOP_old
    {
        public string JOB_ID { get; set; }
        public string EMP_ID3 { get; set; }
        public Nullable<System.DateTime> EndDate1 { get; set; }
        public Nullable<System.DateTime> Start_Date { get; set; }
        public Nullable<System.DateTime> Stop_Date { get; set; }
        public string Latitude1 { get; set; }
        public string Longitude1 { get; set; }
        public string Latitude2 { get; set; }
        public string Longitude2 { get; set; }
        public Nullable<decimal> Distance { get; set; }
        public string Remark { get; set; }
        public Nullable<int> StatusStartStop { get; set; }
        public string ERRRemark { get; set; }
    }
}
