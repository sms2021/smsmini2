//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMSMINI.DAL.SMS21
{
    using System;
    using System.Collections.Generic;
    
    public partial class v_AppointmentFM
    {
        public string JOB_ID { get; set; }
        public Nullable<System.DateTime> Opendate { get; set; }
        public string Failure_th { get; set; }
        public string STA_ID { get; set; }
        public string StationSys { get; set; }
        public string Informer { get; set; }
        public string InPhone { get; set; }
        public string VAN_ID { get; set; }
        public string LicensePlate { get; set; }
        public string JOBS_ID { get; set; }
        public string JobStatus { get; set; }
        public string TYP_ID { get; set; }
        public string JobFailure_Detail { get; set; }
        public string STA_ID1 { get; set; }
        public string EMP_ID3 { get; set; }
        public string FName { get; set; }
        public string Phone { get; set; }
        public Nullable<int> JobINCount { get; set; }
        public string Type { get; set; }
        public string Address { get; set; }
        public string Province { get; set; }
        public string CusZone { get; set; }
        public string Zone { get; set; }
        public string WorkOrderNo { get; set; }
        public string RefNo { get; set; }
    }
}
