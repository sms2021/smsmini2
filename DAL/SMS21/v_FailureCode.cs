//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMSMINI.DAL.SMS21
{
    using System;
    using System.Collections.Generic;
    
    public partial class v_FailureCode
    {
        public string FAI_ID { get; set; }
        public string Failure_th { get; set; }
        public string POI_ID { get; set; }
        public string PointFailure { get; set; }
        public string PRI_ID { get; set; }
        public string PriorityDetail { get; set; }
        public Nullable<double> Pio_RespTime { get; set; }
        public string StatusPri { get; set; }
        public string StatusPrioriry { get; set; }
        public string GRF_ID { get; set; }
        public string TYP_ID { get; set; }
        public string Failure_en { get; set; }
        public string Resolve { get; set; }
        public string Introduce { get; set; }
        public Nullable<int> Priority_GuestVip { get; set; }
        public Nullable<double> Pio_DownTime { get; set; }
        public Nullable<bool> IsCancel { get; set; }
    }
}
