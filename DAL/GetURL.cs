﻿using System.Data.SqlClient;


namespace SMSMINI.DAL
{
    public static class GetURL
    {


        public static string smsURL(string mmn)
        {
            var url = "";

            string conString = SMSMINI.Properties.Settings.Default["ServicesMSDBConnectionString"].ToString();

            var sql = " SELECT  [MMU] ,[SMS_URL]   FROM [Conf_URL] where [MMU] = '" + mmn + "' ";

            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                using (SqlCommand command = new SqlCommand(sql, con))
                {


                    using (SqlDataReader dr = command.ExecuteReader())
                    {

                        if (dr.HasRows)
                        {
                            dr.Read();
                            url = dr["SMS_URL"].ToString();
                            //while (dr.Read())
                            //{
                            //    url = dr["SMS_URL"].ToString();
                            //}
                        }
                    }

                }
            }

            return url;

        }
    }
}
