﻿using System;
using System.Data.SqlClient;

namespace SMSMINI.DAL
{
    public static class GetConnection
    {
        private static string conn = "";
        public static string Connection { get { return conn; } set { conn = value; } }

        public static string CheckConnectionString(string job_id)
        {
            string connectionString = "";

            if (job_id == "")
            {
                connectionString = Properties.Settings.Default.ServicesMSDB.ToString();
            }
            else
            {
                try
                {
                    connectionString = Properties.Settings.Default.ServicesMSDB.ToString();
                    //============================================================================//
                    //ServicesMSDB
                    //============================================================================//
                    try
                    {


                        using (SqlConnection connection = new SqlConnection(connectionString))
                        {
                            var sql = "select top 1* from JOB where JOB_ID = '" + job_id + "'";
                            SqlCommand command = new SqlCommand(sql, connection);
                            connection.Open();

                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                if (reader.HasRows)
                                {
                                    conn = connectionString;
                                    return connectionString;
                                }
                                else
                                {
                                    goto ServicesMSDB_151617;
                                }
                            }
                        }
                    }
                    catch (Exception)
                    {
                    }

                ServicesMSDB_151617:
                    //============================================================================//
                    //ServicesMSDB_151617
                    //============================================================================//
                    connectionString = Properties.Settings.Default.ServicesMSDB_151617.ToString();

                    try
                    {


                        using (SqlConnection connection = new SqlConnection(connectionString))
                        {
                            var sql = "select  top 1*  from JOB where JOB_ID = '" + job_id + "'";
                            SqlCommand command = new SqlCommand(sql, connection);
                            connection.Open();

                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                if (reader.HasRows)
                                {
                                    conn = connectionString;
                                    return connectionString;
                                }
                                else
                                {
                                    goto ServicesMSDB_1314;
                                }
                            }
                        }
                    }
                    catch (Exception)
                    {
                    }
                ServicesMSDB_1314:
                    //============================================================================//
                    //ServicesMSDB_1314
                    //============================================================================//
                    connectionString = Properties.Settings.Default.ServicesMSDB_1314.ToString();

                    try
                    {

                        using (SqlConnection connection = new SqlConnection(connectionString))
                        {
                            var sql = "select  top 1*  from JOB where JOB_ID = '" + job_id + "'";
                            SqlCommand command = new SqlCommand(sql, connection);
                            connection.Open();

                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                if (reader.HasRows)
                                {
                                    conn = connectionString;
                                    return connectionString;
                                }
                                else
                                {
                                    return "";
                                }
                            }

                        }


                    }
                    catch (Exception)
                    {
                    }
                }
                catch (Exception)
                {
                    connectionString = Properties.Settings.Default.ServicesMSDBConnectionString;
                    return connectionString;
                }
            }
            return connectionString;

        }
    }
}
